% check_interp is a script to check the correctness of the interpolation
% basically generating the complete producer variable domain, and use it
% to generate the same interpolated points as the consumer variable domain


%% Generating the original coordinates (DATA) function
% xx = (0:5:125);
% yy = (-35:5:90);
% 
% % obtaining the mesh
% XX = xx'*ones(1,26);
% YY = ones(26,1)*yy;
xx = (2.5:5:122.5);
yy = (-32.5:5:87.5);

% obtaining the mesh
XX = xx'*ones(1,25);
YY = ones(25,1)*yy;

% Function values
coef = 0.6D-2;
wx = XX + YY;
wy = XX - 0.001*YY;
%% Generating the original coordinates for another domain
xx = (7.5:15:142.5);
yy = (-35:10:55);

% obtaining the mesh
XX = xx'*ones(1,10);
YY = ones(10,1)*yy;
% Function values
%ZZ = XX - 1.0D-2*YY;
ZZ = XX + YY;
%% vectorizing the values
xx = reshape( XX, prod(size(XX)), 1);
yy = reshape( YY, prod(size(YY)), 1);
zz = reshape( ZZ, prod(size(ZZ)), 1);



%% Generating the interpolated values

[X,Y,Z]=griddata(xx, yy, zz, x, y,'linear');

