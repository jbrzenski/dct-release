/*******************************************************************/
/*                 Distributed Coupling Toolkit (DCT)              */
/*                                                                 */
/*!
   \file dct_comm.h
  
   \brief Contains all definitions and functions headers related
          with the communication library used in the implementation.

    The file contains the header of the functions used internally in
    the DCT system that are built based on the communication library.
    
    \note In here, there are not the functions used during the
          the DCT Data Exchange Phase, for that see \c dct_comm_data.c

    \attention The implementation is based on MPI.
  
    \date Date of Creation: Sep 12, 2008: MPI version.  
  
    \author Dany De Cecchis: dcecchis@gmail.com
    \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.

*/
/*******************************************************************/

# ifndef DCT_COMM_H
#   define DCT_COMM_H

/* -----------------------------------------------  Include Files  */
#include <mpi.h>
#include <dct_commdat.h>
/* -----------------------------------------  Constants Definition  */

/*******************************************************************/
/*                      Get_Global_Communicator                    */
/*                                                                 */
/*!     Returns the Global Communicator, when using MPI returns
        the MPI_COMM_WORLD.
 
        \return The corresponding system wide Communicator.

*/
/*******************************************************************/
#define Get_Global_Communicator()  (DCT_Comm)MPI_COMM_WORLD

/* ------------------------------------------  Function Prototypes */
int DCT_Comm_GetRank( );

int DCT_Check_CommIni( );

int DCT_Create_Comm ( DCT_Comm );

int DCT_Sync( DCT_Comm );

int DCT_Comm_SetBrokerRank ( DCT_Rank );

int DCT_Free_Comm (  );

int DCT_Free_DType( DCT_Comm_Datatype *, DCT_Integer );

int DCT_Free_Requests ( DCT_Request  *, DCT_Integer );

int DCT_Create_Model_Comm( DCT_Model * );

int DCT_Destroy_Model_Comm ( DCT_Model * );

int DCT_Send_Couple( DCT_Couple ** );

int DCT_Post_RecvCouple(  );

int DCT_Get_Couple( DCT_Couple **, int *, DCT_Rank * );

int DCT_Rels_RecvCouple(  );

int DCT_Send_Tag( const DCT_Rank , const int, DCT_Tag *,
                  const DCT_Msg_Tags_Type );

int DCT_Get_Tag( DCT_Couple **, int * );

int DCT_Mutual_ModReg( DCT_Couple ** );

int DCT_Recv_ModReg ( DCT_Couple ** );

int DCT_Send_Var( DCT_Couple ** );

int DCT_Send_ModVar( DCT_Couple ** );

int DCT_Get_ModVar( DCT_Tag * );

int DCT_Get_Var( DCT_Tag * );

int DCT_Send_Var_Plan( DCT_Couple ** );

int DCT_GetSend_Var_Plan( DCT_Couple ** );

int DCT_Get_Var_Plan( DCT_Couple ** );

int DCT_Send_Var_Plan(  );

int DCT_ProduceVar_Comm( DCT_Var_Node * );

int DCT_ConsumeVar_Comm( DCT_Var_Node * );

# endif /* DCT_COMM_H */

