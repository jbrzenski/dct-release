/*******************************************************************/
/*                Distributed Coupling Toolkit (DCT)               */
/*                                                                 */
/*!
   \file dct.h

   \brief Header file containing the DCT definitions of
    data structures and functions available for the user.

    Header file where is defined the data type structures,
    constants and function prototypes available for
    the user. In here, the essential DCT data type structures;
    DCT_Couple, DCT_Model, DCT_Field, DCT_3d_Var and DCT_4d_Var
    are defined with among other DCT library structures.

    \date Date of Creation: Sep 12, 2005.
    
    \todo Remove the fields related with the file-based communication

    \author Dany De Cecchis: dcecchis@gmail.com
    \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.
*/
/*******************************************************************/

# ifndef DCT_H
#   define DCT_H

/* -----------------------------------------------  Include Files  */
#include <stdlib.h>
#include <string.h>
#include "macros.h"
#include "dct_commdat.h"

/*******************************************************************/
/*                          DCT Definitions                        */
/*******************************************************************/

/*! Constant for empty String. */
#define DCT_NULL_STRING   "\0" /*!< Character used as empty char string. */

/* Constant for immidiate data exchange */
//#define DCT_NOW   -1 /*!< */

/*! Constant to identify never updated. */
#define DCT_TIME_UNSET   -1.0 /*!< Value used to indicate that a timestepping
                                   variable has never been used.
                                   \see DCT_Model \see DCT_Field \see DCT_3d_Var  */

/*!  \brief Defines a boolean data type.

     DCT defines internally the boolean data type (architecture independent).
*/
typedef enum {
  DCT_FALSE=                      0,   /*!< Set False value. */
  DCT_TRUE=                       1    /*!< Set True value. */
} DCT_Boolean;

/*******************************************************************/
/*                        DCT BASIC DATA TYPES                     */
/*******************************************************************/
/*!   \brief Internal DCT double.

   The data type used to represent internally float point values.
*/
typedef double            DCT_Scalar;

/*!   \brief Internal DCT integer.

   The data type used to represent internally integer values.
*/
typedef int               DCT_Integer;

/*!   \brief Internal DCT char strings.

   The data type used to represent internally character strings.
*/
typedef char             *DCT_String;

/*!   \brief Internal DCT names.

   The data type used to represent internally the DCT structure names.
*/
typedef char             *DCT_Name;

/*!    \brief Internal DCT definition of void pointer.

   The data type used to represent internally generic pointers.
*/
typedef void             *DCT_VoidPointer;

/*******************************************************************/
/*                           DCT_Time_Types                        */
/*******************************************************************/
#define DCT_DEFINED_TIME_UNITS 7 /*!< Determines the maximum amount of values
                                      for the \c DCT_Time_Types enum.
                                      \see DCT_Time_Types  */
/*! \brief Type of model time units.

   Defines the different kind of time units
   in the model for time stepping. The constant
   \c DCT_DEFINED_TIME_UNITS defines how many different
   valid values are for the \c DCT_Time_Types enum.

   \see DCT_Units
*/
typedef enum{
  DCT_TIME_UNKNOWN=               0, /*!< The time units is not set. */
  DCT_TIME_NO_UNIT=               1, /*!< No unit time. */
  DCT_TIME_SECONDS=               2, /*!< Time is set to seconds. */
  DCT_TIME_MINUTES=               3, /*!< Time is set to minutes. */
  DCT_TIME_HOURS=                 4, /*!< Time is set to hours. */
  DCT_TIME_DAYS=                  5, /*!< Time is set to days. */
  DCT_TIME_WEEKS=                 6, /*!< Time is set to weeks. */
  DCT_TIME_YEARS=                 7  /*!< Time is set to years. */
}  DCT_Time_Types;


/*******************************************************************/
/*                            DCT_Distribution                     */
/*******************************************************************/
#define DCT_MAX_DISTRIBUTION_TYPES 3 /*!< Determines the maximum amount of values
                                          for the \c DCT_Distribution enum.
                                          \see DCT_Distribution */
/*!  \brief Type of different parallel layout.

   Defines the different Parallel Processor Layouts
   supported by DCT. The constant \c DCT_MAX_DISTRIBUTION_TYPES
   defines how many different valid values are for the \c DCT_Distribution enum.
*/
typedef enum   {
  DCT_DIST_NULL=                  0, /*!< The parallel layout is not set. */
  DCT_DIST_SEQUENTIAL=            1, /*!< The parallel layout is set to sequential. */
  DCT_DIST_RECTANGULAR=           2, /*!< The parallel layout is set to 2D rectangular. */
  DCT_DIST_3D_RECTANGULAR=        3  /*!< The parallel layout is set to 3D rectangular. */
} DCT_Distribution;


/*******************************************************************/
/*                         DCT_Data_Transformation                 */
/*******************************************************************/
#define DCT_DATA_TRANSFORMATION   3 /*!< Determines the maximum amount of values
                                          for the \c DCT_Data_Transformation enum.
                                          \see DCT_Data_Transformation. */

/*!  \brief Type of different model data operations.

   Defines the different Data Transformations (i.e. interpolation)
   supported by DCT. The constant \c DCT_DATA_TRANSFORMATION
   defines how many different valid values are for the
   \c DCT_Data_Transformation enum.

   \warning Only the linear interpolation in 2D has been implemented.
*/
typedef enum{
  DCT_NO_INTERPOLATION=           0, /*!< The interpolation is not set */
  DCT_LINEAR_INTERPOLATION=       1, /*!< Operation is set to linear interpolation.*/
  DCT_QUAD_INTERPOLATION=         2, /*!< Operation is set to Quadratic interpolation.*/
  DCT_CUBIC_INTERPOLATION=        3  /*!< Operation is set to Cubic interpolation.*/
}  DCT_Data_Transformation;


/*******************************************************************/
/*                       DCT_Object_Types                          */
/*******************************************************************/
#define DCT_DEFINED_OBJECT_TYPES 7  /*!< Determines the maximum amount of values
                                         for the \c DCT_Object_Types.
                                         \see DCT_Object_Types  */
#define DCT_DEFINED_VAR_OBJECT 3  /*!< Determines the maximum amount of values
                                       for \DCT_Vars types (2D, 3D, or 4D) defined in
                                       \c DCT_Object_Types.
                                       \see DCT_Object_Types     */
/*!  \brief Type of different DCT essential data estructures.

    Stats the different kind of DCT Data Structure Types defined in the
    DCT to represent a model entity for the coupling. This is used to
    identify an actual DCT structure, specially in a \c DCT_List
    structure. The constant \c DCT_DEFINED_OBJECT_TYPES defines how
    many valid values are for the \c DCT_Object_Types enum. Another
    constant, \c DCT_DEFINED_VAR_OBJECT defines how many valid values
    are for \DCT_Vars types. The difference between these two constants
    is because \c DCT_DEFINED_OBJECT_TYPES stats the total set of
    values in the DCT_Object_Types enum, on the other hand,
    \c DCT_DEFINED_VAR_OBJECT gives the number of data structures that represents
    fields or variables in a user model, specially differentiated by the dimension,
    and it is here defined as \DCT_Vars ( DCT_Field, DCT_3d_Var and DCT_4d_Var).

    \sa DCT_List, DCT_Field, DCT_3d_var, DCT_4d_var, DCT_Model, DCT_Couple,
    DCT_Hollow_CPLVar, DCT_CPL_Node
*/
typedef enum{
  DCT_TYPE_UNKNOWN=               0, /*!< The object type is not set. */
  DCT_FIELD_TYPE=                 1, /*!< The object is set to DCT_Field. */
  DCT_3D_VAR_TYPE=                2, /*!< The object is set to DCT_3d_Var. */
  DCT_4D_VAR_TYPE=                3, /*!< The object is set to DCT_4d_Var. */
  DCT_MODEL_TYPE=                 4, /*!< The object is set to DCT_Model. */
  DCT_COUPLE_TYPE=                5, /*!< The object is set to DCT_Couple. */
  DCT_HCPLVAR_TYPE=               6, /*!< The object is set to \c DCT_Hollow_CPLVar. */
  DCT_CPL_NODE_TYPE=              7  /*!< The object is set to \c DCT_CPL_Node. */
}  DCT_Object_Types;


/*******************************************************************/
/*                           DCT_Data_Types                        */
/*******************************************************************/
#define DCT_DEFINED_DATA_TYPES 4 /*!< Determines the maximum amount of valid values
                                      for the \c DCT_Data_Types enum.
                                      \see DCT_Data_Types  */
/*!  \brief Type of different data types used inside the library.

    Stats the basic user data types used with DCT. The constant
    \c DCT_DEFINED_DATA_TYPES defines the how many
    valid values are for the \c DCT_Data_Types type enumeration.

    \attention This definition is different to that used by the
    communication library defined in dct_commdat.h.
    \see dct_commdat.h
*/
typedef enum {
  DCT_DATA_TYPE_UNKNOWN=          0, /*!< The user data type is not set */
  DCT_INTEGER=                    1, /*!< The user data type is set to int */
  DCT_FLOAT=                      2, /*!< The user data type is set to float */
  DCT_DOUBLE=                     3, /*!< The user data type is set to double */
  DCT_LONG_DOUBLE=                4  /*!< The user data type is set to long double */
} DCT_Data_Types;


/*******************************************************************/
/*                           DCT_Error_Handler                     */
/*******************************************************************/
#define DCT_DEFINED_ERRORS 30 /*!< Determines the maximum amount of valid values
                                   for the \c DCT_Error_Handler enum.
                                   \see DCT_Error_Handler  */
/*!  \brief Type of different error code used in the library.

     Defines the different  Error Codes constants, to determine the
     type of error. The constant \c DCT_DEFINED_ERRORS
     defines how many valid values are for the \c DCT_Error_Handler enum.
*/
typedef enum {
  DCT_SUCCESS=                    0,   /*!< No error. */
  DCT_INVALID_ARGUMENT,                /*!< Invalid argument is passed. */
  DCT_INVALID_MODEL_DEFINITION,        /*!< Invalid model definition. */
  DCT_INVALID_MODEL_TAG,               /*!< Invalid model tag. */
  DCT_INVALID_COUPLING_VARIABLE,       /*!< Invalid coupling variable. */
  DCT_INVALID_COUPLING_DOMAIN,         /*!< Invalid coupling domain. */
  DCT_INVALID_UNITS,                   /*!< Invalid units. */
  DCT_FAILED_MALLOC,                   /*!< Malloc call failed. */
  DCT_FAILED_LIB_COMM,                 /*!< The communication library failed. */
  DCT_FAILED_TRANSFOR,                 /*!< The transformation failed. */
  DCT_INVALID_COMM,                    /*!< Invalid DCT communicator. */
  DCT_INVALID_DISTRIBUTION,            /*!< Invalid DCT distribution. */
  DCT_INVALID_LABELING,                /*!< Invalid labeling. */
  DCT_INVALID_DIMENSIONS,              /*!< Invalid dimension. */
  DCT_INVALID_FREQ,                    /*!< Invalid frequency. */
  DCT_INVALID_OPERATION,               /*!< Invalid operation. */
  DCT_INVALID_DATA_TYPE,               /*!< Invalid data type. */
  DCT_INVALID_DATA_TRANS,              /*!< Invalid data transformation. */
  DCT_INVALID_NUMB_CPLS,               /*!< Invalid number of couples. */
  DCT_FILE_OPEN_ERROR,                 /*!< Error open a file. */
  DCT_FILE_IO_ERROR,                   /*!< Error in a file I/O. */
  DCT_NAME_MATCH_ERROR,                /*!< Error matching DCT object name. */
  DCT_VALUE_MATCH_ERROR,               /*!< Error in a matching number. */
  DCT_OBJECT_DUPLICATED,               /*!< Error by duplicating an object. */
  DCT_COUPLE_LIST_ERROR,               /*!< Error in a DCT_Couple list. */
  DCT_MODEL_LIST_ERROR,                /*!< Error in a DCT_Model list. */
  DCT_VARS_LIST_ERROR,                 /*!< Error in a \DCT_Vars list. */
  DCT_COUPLE_TAG_EXHAUSTED,            /*!< No more DCT_Couple tag available. */
  DCT_MODEL_TAG_EXHAUSTED,             /*!< No more DCT_Model tag available. */
  DCT_VARS_TAG_EXHAUSTED,              /*!< No more \DCT_Vars tag available. */
  DCT_UNKNOWN_ERROR                    /*!< No previously cataloged error. */
} DCT_Error_Handler;

/*******************************************************************/
/*                              DCT_Error                          */
/*******************************************************************/
/*!    \brief The data type used to represent the error variable.

   To provide better error caching and control, DCT implements this error variable
   which consist in a error code, and a message associated with the error returned.
*/
typedef struct{
   DCT_Error_Handler   Error_code;/*!< Stores the error code. */
   DCT_String          Error_msg; /*!< Stores the char string with the error
                                       messages associated with Error_code. */
} DCT_Error;


/*******************************************************************/
/*                            DCT_ProdCons                         */
/*******************************************************************/
/*!  \brief Type to indicate a \DCT_Vars is to produce or consume.

     \DCT_Vars are defined to be consumed or produced during a model coupling.
     This enum stats the type of rol be defined (Production/Consumption).

     \sa DCT_Field, DCT_3d_var, DCT_4d_var
*/
typedef enum{
  DCT_PRODUCE=                    0, /*!< The attribute is set to produce. */
  DCT_CONSUME=                    1  /*!< The attribute is set to consume. */
} DCT_ProdCons;


/*******************************************************************/
/*                              DCT_Units                          */
/*******************************************************************/
#define DCT_DEFINED_UNITS  30 /*!< Determines the maximum amount of valid values
                                   for the \c DCT_Set_Field_Units.
                                   \see DCT_Set_Field_Units  */
/*!  \brief Defines the different kind of unit can be represented using
            \DCT_Vars.

     Using DCT, the \DCT_Vars can represent meaningful physical units.
     This enum defines the supported units by the DCT interface.
     The constant \c DCT_DEFINED_UNITS defines how many valid values
     are for a DCT_Units enum.

     \attention  Beware with the USER_DEFINED? data and the DCT_DEFINED_UNITS
                 constant; because it could be set a user defined number and
                 no user data was defined. See, for instance,
                 \c DCT_Set_Field_Units function.

   \sa DCT_Set_Field_Units, DCT_Set_3d_Var_Units
*/
typedef enum {
  DCT_UNIT_UNKNOWN=               0,   /*!< The unit is not set. */
  DCT_NO_UNITS,                        /*!< No unit is used. */
  CENTIMETER,                          /*!< The unit set to centimeters. */
  METER,                               /*!< The unit set to meters. */
  KILOMETER,                           /*!< The unit set to Kilometers. */
  INCH,                                /*!< The unit set to inch. */
  FEET,                                /*!< The unit set to feet. */
  MILES,                               /*!< The unit set to miles. */
  SECONDS,                             /*!< The unit set to seconds. */
  MINUTES,                             /*!< The unit set to minutes. */
  HOURS,                               /*!< The unit set to hours. */
  DAYS,                                /*!< The unit set to days. */
  KILOMETERS_HOUR,                     /*!< The unit set to km/h. */
  KILOMETERS_SECOND,                   /*!< The unit set to Km/s. */
  KILOMETERS_MINUTE,                   /*!< The unit set to Km/m. */
  MILES_HOUR,                          /*!< The unit set to Mph. */
  MILES_SECOND,                        /*!< The unit set to Miles per second. */
  MILES_MINUTE,                        /*!< The unit set to Miles per minute. */
  CELSIUS,                             /*!< The unit set to Celsius. */
  KELVIN,                              /*!< The unit set to Kelvin. */
  FAHRENHEIT,                           /*!< The unit set to Fahrenheit. */
  USER_DEFINED1,                       /*!< The unit set to user defined unit 1. */
  USER_DEFINED2,                       /*!< The unit set to user defined unit 2. */
  USER_DEFINED3,                       /*!< The unit set to user defined unit 3. */
  USER_DEFINED4,                       /*!< The unit set to user defined unit 4. */
  USER_DEFINED5,                       /*!< The unit set to user defined unit 5. */
  USER_DEFINED6,                       /*!< The unit set to user defined unit 6. */
  USER_DEFINED7,                       /*!< The unit set to user defined unit 7. */
  USER_DEFINED8,                       /*!< The unit set to user defined unit 8. */
  USER_DEFINED9,                       /*!< The unit set to user defined unit 9. */
  USER_DEFINED10                       /*!< The unit set to user defined unit 10. */
} DCT_Units;


/*******************************************************************/
/*                            DCT_Val_Loc                          */
/*******************************************************************/
#define DCT_DEFINED_VAL_LOC  5 /*!< Determines maximum valid value
                                   for the \c DCT_Val_Loc.
                                   \see DCT_Val_Loc  */
/*!   \brief Defines in a \DCT_Vars where the data is located in the Model domain.

      \c DCT_Val_Loc stats the different locations to describe how the actual
      user data values are located with respect the domain mesh. The constant
      \c DCT_DEFINED_VAL_LOC defines how many valid values are for a DCT_Val_Loc
      enum.

*/
typedef enum {
  DCT_LOC_UNSETS=                 0,   /*!< Data locations is not set. */
  DCT_LOC_CENTERED,                    /*!< Data locations is centered. */
  DCT_LOC_CORNERS,                     /*!< Data locations is in corners. */
  DCT_LOC_STRIDE1,                     /*!< Data locations is strode 1 space. */
  DCT_LOC_STRIDE2,                     /*!< Data locations is strode 2 spaces. */
  DCT_LOC_STRIDEN                      /*!< Data locations is strode _n_ spaces. */
} DCT_Val_Loc;


/*******************************************************************/
/*                         DCT_Domain_Type                         */
/*******************************************************************/
#define DCT_DEFINED_DOMTYPE  3 /*!< Determines maximum amount of valid values
                                   for the \c DCT_Domain_Type.
                                   \see DCT_Domain_Type  */
/*!   \brief Stats the description of how is the model
      domain discretization.

      A computational Model is defined on a discretized domain. This enum
      establishes the different types of model domain discretization supported
      by DCT. The constant \c DCT_DEFINED_DOMTYPE defines how many valid values
      are for a DCT_Domain_Type enum. The meaning of each value is:
         \li The value \c DCT_CARTESIAN represents a rectilinear
             domain with an equally spaced discretization. Using this type
             only the initial and ending values are stored, together with
             the number of point, saving some memory.

         \li The value \c DCT_RECTILINEAR represents a rectilinear
             domain with an arbitrary spaced discretization, saving
             each tick mark in it.

         \li The values \c DCT_GENERAL_CURV represents a structured
             general curvilinear domain with all its points given by
             its coordinates.

      \warning The DCT_GENERAL_CURV mesh type is not fully implemented yet,
               and the data structure used to represent this kind of mesh
               might be unappropriated.

    \see DCT_Model_Dom
*/
typedef enum {
  DCT_UNKNOWN_TYPE=   0, /*!< Domain discretization type is not set. */
  DCT_CARTESIAN  =    1, /*!< Domain discretization type is equally spaced
                              in the corresponding dimension. */
  DCT_RECTILINEAR=    2, /*!< Domain discretization type is not equally spaced
                              in the corresponding dimension. */
  DCT_GENERAL_CURV=   3  /*!< Domain discretization type is general curvilinear. */
} DCT_Domain_Type;


/*******************************************************************/
/*                          DCT_Data_SubDom                        */
/*******************************************************************/
/*!   \brief The data type used to represent a model subdomain.

      Model domain in parallel environments are usually split in parts
      called subdomains. \c DCT_Data_SubDom is a structured used to represent
      a piece of a  domain (subdomain) assigned to a processor.

      \sa DCT_Model, DCT_Model_Dom
*/
typedef struct {
  DCT_Integer      *IniPos;   /*!< Array of the subdomain starting indices
                                   of the label values where the subdomain
                                   starts in each dimension.          */
  DCT_Integer      *EndPos;   /*!< Array of the subdomain ending indices
                                   of the label values where the subdomain
                                   starts in each dimension.          */
  DCT_Rank          RankProc; /*!< Process element ID.  */
} DCT_Data_SubDom;


/*******************************************************************/
/*                            DCT_List                             */
/*******************************************************************/
/*!   \brief General structure to represent list for DCT.

      Internally, DCT needs to use different data type of lists.
      \c DCT_List is a structure to represent a general list node.
*/
typedef struct {
  DCT_VoidPointer   AddressData;/*!< Pointer to the node data. */
  DCT_VoidPointer   NextData;/*!< Pointer to the next list node. */
  DCT_Object_Types  DataType;/*!< Stores the kind of data is pointed to
                                  AddressData. */
} DCT_List;


/*************************************************************************/
/*                             DCT BASES STRUCTURES                      */
/*************************************************************************/

/*******************************************************************/
/*                           DCT_Model_Dom                         */
/*******************************************************************/
/*   Maximum and Minimum dimensions allowed to declare a
     model domain                                             */
#define MIN_DOMAIN_DIM 2 /*!< Determines the minimum domain dimension. */
#define MAX_DOMAIN_DIM 4 /*!< Determines the maximum domain dimension.  */

/*!   \brief Data structure to represent the computational model domain in \c DCT_Model.

      The data structure DCT_Model needs a representation of its computational
      domain. \c DCT_Model_Dom has different member data to representes the
      domain of the model.
*/
typedef struct {
  DCT_Integer       ModDomDim;   /*!< Domain Dimension of the Model (2, 3, 4) */
  DCT_Domain_Type  *ModDomType;  /*!< Array of Type of the model domain in each
                                      dimension. */
  DCT_Integer      *ModNumPts;   /*!< Array of the number of points in each direction,
                                      with a length of \c DCT_Model_Dom.ModDomDim  */
  DCT_Scalar      **ModValues;   /*!< Array of pointers to DCT_Scalar, with a length
                                      of \c DCT_Model_Dom.ModDomDim. The content in
                                      each array position depends on the value of the
                                      corresponding position in the array
                                      \c DCT_Model_Dom.ModDomType:
                                \li If the value is \c DCT_CARTESIAN, the entry points
                                    to an array of 3 positions indicating; initial
                                    domain value, final domain value and the discretization
                                    spacing (\a h). This option can be mixed with
                                    \c DCT_RECTILINEAR.

                                \li If the values is \c DCT_RECTILINEAR, the entry
                                    points to an array with a length of the corresponding
                                    entry in \c DCT_Model_Dom.ModNumPts, with the tick
                                    marks values of the domain discretization along the
                                    dimension. This option can be mixed with
                                    \c DCT_CARTESIAN.

                                \li If the values is \c DCT_GENERAL_CURV, all the
                                    array entries points to arrays with a length of
                                    the product of the array entries in
                                    DCT_Model_Dom.ModNumPts. Each array contains the
                                    coordinate value of a  point in the discretized
                                    domain. This option can not be used with any other. */

/******* The data structures used to represent subdomains (and probably domains)
         on General Curvilinear Domains representation might be unappropriated    *******/

  DCT_Distribution  ModSubDomParLayout;/*!< The subdomain arrange of the process elements */
  DCT_Integer      *ModSubDomLayoutDim;/*!< Array with the number of process elements
                                            in each dimension according with
                                            DCT_Model_Dom.ModSubDomParLayout. */
  DCT_Data_SubDom  *ModSubDom;/*!< Pointer to an array of \c DCT_Data_SubDom entries, with
                                   a length resulting of the product of the entries in
                                   DCT_Model_Dom.ModSubDomLayoutDim.    */
} DCT_Model_Dom;


/*******************************************************************/
/*        ===================================================      */
/*                             DCT_Model                           */
/*        ===================================================      */
/*******************************************************************/
/*!   \brief Data structure used by DCT to represent a computational model.

      DCT provides a set of data structures to represent the process of
      coupling. One of these key data structures is the \c DCT_Model,
      which is used to describe a computational model, about its relevant
      properties and some \DCT_Vars associated. The implementation of the
      functions used to handle DCT_Model data structure are in the file
      dct_model.c.

    \warning The data structures used to represent subdomains (and probably domains)
             on General Curvilinear Domains representation might be unappropriated.
*/
typedef struct {
  DCT_Name          ModName;           /*!< Character string containing the identifier
                                            name of the DCT_Model instance. This name
                                            identifies the DCT_Model instance in DCT
                                            system wide. It should be unique between
                                            all the DCT_Model declared.
                                            \see DCT_Create_Model */
  DCT_String        ModDescription;    /*!< Character string with an alternative longer
                                            description of the DCT_Model instance.
                                            \see DCT_Create_Model */
  DCT_Boolean       ModCommit;         /*!< Indicates if the DCT_Model data structure
                                            can accept changes. When a DCT_Model instance
                                            is asociated to a DCT_Couple, then it is locked
                                            and no more changes are allowed.
                                            \see DCT_Create_Couple */
  DCT_Tag           ModTag;            /*!< Tag used internally to indentify a DCT_Model
                                            structure, and is set DCT_TAG_UNDEF when is
                                            created.
                                            \sa DCT_Get_Tag,  DCT_Broker_RegCouple */
  DCT_Integer       ModNumProcs;       /*!< Number of process elements that run the
                                            represented model. It must agree the number
                                            of subdomain.
                                            \see DCT_Create_Model */
  DCT_Scalar        ModTimeIni;        /*!< Initial time of the described model.
                                            \see DCT_Set_Model_Time */
  DCT_Scalar        ModTime;           /*!< Time-step size of the described model.
                                            \see DCT_Set_Model_Time */
  DCT_Scalar        ModCurrentTime;    /*!< The current model time in the described model.
                                            \see DCT_Update_Model_Time */
  DCT_Time_Types    ModTimeUnits;      /*!< The time units used in the model.
                                            \see DCT_Set_Model_Time  */
  DCT_Model_Dom    *ModDomain;         /*!< Describes the model domain and its parallel
                                            subdomain layout.
                                            \sa DCT_Set_Model_Dom,
                                            DCT_Set_Model_RSpaced_Dom,
                                            DCT_Set_Model_GenCur_Dom,
                                            DCT_Set_Model_SubDom */
  DCT_Integer       ModTotVarConsumed; /*!< The number of variables declared for consumption
                                            associated to the described model.
                                            \see DCT_Set_Model_Consumption  */
  DCT_Integer       ModTotVarProduced; /*!< The number of variables declared for production
                                            associated to the described model.
                                            \see DCT_Set_Model_Production  */
  DCT_List         *ModConsumeVars;    /*!< The DCT_List of the \DCT_Vars for consumption
                                            associated to the described model.
                                            \see DCT_Set_Model_Consumption  */
  DCT_List         *ModProduceVars;    /*!< The DCT_List of the \DCT_Vars for production
                                            associated to the described model.
                                            \see DCT_Set_Model_Production  */
  DCT_Rank          ModLeadRank;       /*!< The <em>Model Coordinator</em> processes element
                                            ID.
                                            \see DCT_Broker_RegCouple, DCT_Get_Tag  */
  DCT_Comm          ModGroupComm;      /*!< The communication grouping handler of the process
                                            elements in the described model.
                                            \see DCT_Create_Model_Comm */
} DCT_Model;


/*******************************************************************/
/*                            DCT_SD_Consume                       */
/*******************************************************************/
/*!   \brief Data structure used to represent a generic subdomain \e slab for
             consumption.

      This is a internal data structure that represents a subdomain \e slab
      for a \DCT_Vars to be consumed (and be received from a producer).

    \warning The most likely is that, in the future, slab representations
             could be moved to associated domain type.
*/
typedef struct {
  DCT_Rank                Sendr;    /*!< Process element ID to send (produce)
                                         the slab. */
  DCT_Integer            *IniPos;   /*!< Array of indices of the position where
                                         the slab starts. The array length in
                                         given by the domain dimension. */
  DCT_Integer            *Length;   /*!< Array of the length along each dimension
                                         of a slab. The array length in given by
                                         the domain dimension. */
} DCT_SD_Consume;

/*******************************************************************/
/*                           DCT_SDConsume_Plan                    */
/*******************************************************************/
/*!   \brief Structure to represent the consumption plan for a
             process unit of a model for a \DCT_Vars to be
             consumed.

      As long as a producer process only serves the data, a consumer process
      has to have enough information about how the information is served in
      order to \e digest the data. This internal structure represents the
      consumption plan for a process unit belonging a model with a \DCT_Vars
      to be consumed. The firsts six members of the structure represent
      the whole chunk of data that this consumer (Recvr proc) subdomain lays
      on the Producer Model Domain using the producer resolution. The next 
      two members, represent the parallel subdomain decomposition of the
      producer model domain ONLY of the subdomains where the consumer
      subdomain lays on and their neighbors (just in case it needed to ask
      them data when the variables are processed. And the last two members
      represents the slab of each subdomain from the producer that intersects
      with the consumer subdomain.

      \sa DCT_Couple, DCT_Create_Couple, DCT_Destroy_Couple, DCT_ConsumeVar_Comm,
         DCT_BrokerArrange, DCT_BrokerArrangeGC, DCT_Arrange_SD, DCT_Local_Data_Delete
*/
typedef struct {
  DCT_Rank                Recvr;     /*!< Process element ID of the receiver. This
                                          is who this plan belongs to. */
  DCT_Scalar             *IniVal;    /*!< Is an array with length dim, with the
                                          initial values of the subdomain in the
                                          producer resolution. */
  DCT_Scalar             *EndVal;    /*!< Is an array with length dim, with the
                                          ending values of the subdomain in the
                                          producer resolution. */
  DCT_Integer            *GlbInd;    /*!< Is an array with length dim, with the
                                          producer domain initial indices in each
                                          direction corresponding for the whole
                                          overlapped region (coupling interface). */
  DCT_Integer            *Npts;      /*!< Is an array with length dim, with the
                                          number of points corresponding in each
                                          direction from IniVal to EndVal. */
  DCT_Integer            *SndrNpts;  /*!< Is an array with size of dim, and it is
                                          the total number of points in the entire
                                          producer domain for each dimension. */
  /********************************************************************************/
  DCT_Integer             SndrNSD;   /*!< An integer indicating the number of
                                          producer's subdomains intersecting the
                                          consumer subdomain and the neighbors. */
  DCT_Data_SubDom        *SndrSDom;  /*!< Is a pointer to an array with  SndNSD
                                          elements  with each entry is a producer's
                                          subdomains description. */
  /********************************************************************************/
  DCT_Integer             Nmsg;      /*!< Is the number of slabs to be received.  */
  DCT_SD_Consume         *SDSlab;    /*!< Is an array with length Nmsg with the
                                           corresponding producer subdomain slab. */
} DCT_SDConsume_Plan;

/*******************************************************************/
/*                          DCT_SD_Produce                         */
/*******************************************************************/
/*!   \brief Data structure used to represent a generic subdomain
             \e slab  for production as guide for the variables
             to be produce.

      This is a internal data structure that represents a subdomain
      \e slab for a \DCT_Vars to be produced (and be received from
      a consumer).

    \warning The most likely is that, in the future, slab representations
             could be moved to associated domain type.
*/
typedef struct {
  DCT_Rank                Recvr;   /*!< Process element ID to receive (consume)
                                        the slab. */
  DCT_Scalar             *IniVal;  /*!< Is an array with length dim, with the
                                        initial values of the subdomain that
                                        could be sent. */
  DCT_Scalar             *EndVal;  /*!< Is an array with length dim, with the
                                        ending values of the subdomain that
                                        could be sent. */
  DCT_Integer            *Length;  /*!< Is an array with length dim, with the
                                        number of elements in each dimension. */
} DCT_SD_Produce;

/*******************************************************************/
/*                       DCT_SDProduce_Plan                        */
/*******************************************************************/
/*!   \brief Structure to represent the production plan for a
             process unit of a model for a \DCT_Vars to be
             produced.

      As long as a producer process only serves the data, a
      consumer process has to have enough information about how
      the information is served in order to \e digest the data.
      This internal structure represents the production plan for
      a process unit belonging a model with a \DCT_Vars to be
      produced.

      \sa DCT_Couple, DCT_Create_Couple, DCT_Destroy_Couple, DCT_ProduceVar_Comm,
         DCT_BrokerArrange, DCT_BrokerArrangeGC, DCT_Arrange_SD, DCT_Local_Data_Delete
*/
typedef struct {
  DCT_Integer             Nmsg;     /*!< Is the number of messages the process could
                                         send from this subdomain. */
  DCT_Rank                Sendr;    /*!< Is the sender rank corresponding for this
                                         subdomain.  */
  DCT_Integer             RcvrNSD;  /*!< Is an integer indicating the number of
                                         consumer's subdomains. */
  DCT_SD_Produce         *Msend;    /*!< Is an array of produce \e slabs that could be
                                         send. */
} DCT_SDProduce_Plan;

/*******************************************************************/
/*                       DCT_Hollow_Model                          */
/*******************************************************************/
/*!   \brief Structure to represent a remote DCT_Model.

      A DCT_Couple structure needs to have the information of two model.
      However, the model user only declares the name of remote model, and during
      the registration phase, this internal structure is used to store non local
      DCT_Model instances.

      \sa DCT_Couple, DCT_Create_Couple, DCT_Destroy_Couple
*/
typedef struct {
  DCT_Name          ModelName;         /*!< Remote model name. Must match exactly with
                                            actual name intended to be coupled. */
  DCT_Scalar        ModelLastUpdate;   /*!< Simulation time that the model was last
                                            updated. */
  DCT_Scalar        ModelUpdateIntrv;  /*!< Time-step size of the described model. */
  DCT_Rank          ModLeadRank;       /*!< Process element ID of the designated Model
                                            Leader to coordinate the registration phase. */
  DCT_Model_Dom    *ModDomain;         /*!< Remore DCT_Model domain description. */
} DCT_Hollow_Model;

/*******************************************************************/
/*         ===================================================     */
/*                              DCT_Couple                         */
/*         ===================================================     */
/*******************************************************************/
/*!   \brief Structure that uses DCT to represent the coupling of two DCT_Model.

      DCT provides a set of data structures to represent the process of
      coupling. The one which represents the coupling is the DCT_Couple
      data structure. A DCT_Couple structure needs two computational
      model to couple. Inside of a model code, one is a DCT_Model (the local one),
      and the other is a \c DCT_Hollow_Model (the remote). The last structure
      is transparent to the user. And finally, beside the models, it is
      necessary to describe the \DCT_Vars that are planned to be used in
      the couple.
*/
typedef struct {
  DCT_Name            CoupleName;     /*!< Character string containing the identifier
                                            name of the DCT_Couple instance. This name
                                            identifies the DCT_Couple variable DCT system
                                            wide. It should be unique between all the
                                            DCT_Couple declared.
                                            \sa DCT_Create_Couple */
  DCT_Tag             CoupleTag;        /*!< Tag used internally to indentify a
                                             DCT_Couple structure, and is set
                                             DCT_TAG_UNDEF when is created.
                                             \sa DCT_Get_Tag,  DCT_Broker_RegCouple */
  DCT_String          CoupleDescription; /*!< Character string with an alternative longer
                                              description of the DCT_Couple instance. */
  DCT_Model          *CoupleModelA;       /*!< Pointer to a DCT_Model that represents
                                               the local computational model. */
  DCT_Model          *CoupleModelB;       /*!< Pointer to a DCT_Model that represents
                                               the remote computational model. This
                                               pointer is declared because the Registration
                                               Broker process could any process and handle
                                               avery coupling details across the DCT
                                               system. */
  DCT_Hollow_Model   *CoupleHModelB;      /*!< Pointer to a hollow model representing the
                                               remote DCT_Model in the coupling. This
                                               structure is used to every process after
                                               the Registration Phase. */
  DCT_Integer         CoupleTotNumVars;   /*!< Total number of \DCT_Vars to be paired
                                               during the coupling process. This amount
                                               is set before DCT_EndRegistration step, and 
                                               have to agree with the counter part DCT_Couple
                                               declaration. */
  DCT_Integer         CoupleNumVars;      /*!< Number of variables associated to the
                                               local model A used, internally by the 
                                               DCT_Broker. During the registration is
                                               used to count the number of variables
                                               registered by the broker from the model A. */
  DCT_Integer         CoupleNumVars2;     /*!< Number of variables associated to the
                                               remote model B, used internally by the 
                                               DCT_Broker. During the registration is
                                               used to count the number of variables
                                               registered by the broker from the model B. */ 
  DCT_Integer         CoupleFirstVarCPL;  /*!< The first coupled variable index in the
                                               DCT_CPL_Table, corresponding to this
                                               DCT_Couple instance. */
  DCT_VoidPointer     CouplingTable;      /*!< Pointer to a temporary list where the
                                               coupled variables are include by the user
                                               (before call DCT_EndRegsitration). After
                                               the registration phase this list will be
                                               deleted. */

  DCT_SDConsume_Plan  CoupleRcv;          /*!< Pointer to a subdomain structure
                                               DCT_SDConsume_Plan for the variables
                                               consumed in the couple. */
  DCT_SDProduce_Plan  CoupleSnd;          /*!< Pointer to a subdomain structure
                                               DCT_SDProduce_Plan for the variables
                                               consumed in the couple. */
} DCT_Couple;

/*******************************************************************/
/*         ===================================================     */
/*                               DCT_Field                         */
/*         ===================================================     */
/*******************************************************************/
/*!   \brief Structure that uses DCT to represent a model 2D variable.

      DCT provides a set of data structures to represent the process of
      coupling. There are 3 types of the \DCT_Vars. The DCT_Field describes
      a 2D model variable. This structure gives the information to
      tell the DCT system what data to use, when do it, where is sent to
      or received from, and if there is any unit translation and data process.
*/
typedef struct {
  DCT_Name          VarName;           /*!< \brief Identification Name of the DCT_Field.
                                            Character string containing the identifier
                                            name of the DCT_Field instance. This name
                                            identifies the DCT_Field variable in the DCT
                                            system wide. */
  DCT_String        VarDescription;    /*!< \brief Shot description of the DCT_Field.
                                            Character string with an alternative longer
                                            description of the DCT_Field instance. */
  DCT_Model        *VarModel;          /*!< \brief Pointer to the DCT_Model that the variable
                                            belongs to. */
  DCT_Boolean       VarCommit;         /*!< \brief Lock the DCT_Field instance (no more changes
                                            can be made) when is added to a DCT_Model. */
  DCT_Tag           VarTag;            /*!< \brief Tag id of the DCT_Field in the DCT system,
                                            and is set DCT_TAG_UNDEF when is created. */
  DCT_Integer       VarDim[2];         /*!< \brief Number of discretization points in each dim
                                            of a 2D sub-domain data. */
  DCT_Domain_Type   VarDomType[2];     /*!< \brief DCT_Domain_Type value to indicate if the mesh
                                            is structured, non-structured Cartesian
                                            rectangular, or general curvilinear
                                            corresponding to each direction. */
  DCT_Scalar       *VarLabels[2];      /*!< \brief Values of labels (tick marks), if it is non-
                                            structured; values of initial and end value,
                                            if it is structured; or the coordinates if it
                                            is general curvilinear. */
  DCT_Val_Loc       VarDataLoc;        /*!< \brief Type of DCT_Val_Loc that describes how the
                                            var data is located in the domain mesh. */
  DCT_Integer       VarStride[2];      /*!< \brief Indicates the stride (how many positions is
                                            located) to the data for each dimension . */
  DCT_Distribution  VarDistType;       /*!< \brief The type of the parallel layout.
                                            \see DCT_Distribution */
  DCT_Integer      *VarDistProcCounts; /*!< \brief Array of Process count in every direction of
                                            the layout (e.g., in 2x2 or 3X4X5 process
                                            layout, the values in the array are | 2 : 2 |
                                            or | 3 : 4 : 5 |). */
  DCT_Rank         *VarDistRank;       /*!< \brief Array with the rank of the processes involved
                                            in the DCT_Field and with a length equal to
                                            the multiplication of all the members of
                                            \c VarDistProcCounts. */
  DCT_Time_Types    VarTimeUnits;      /*!< \brief Time units of the DTC_Fiels (It should be an
                                            integer defined in DCT_Time_Types). */
  DCT_Scalar        VarTimeIni;        /*!< \brief Indicates the time when the DCT_Field becomes
                                            to be operational in the coupling sense. */
  DCT_ProdCons      VarProduced;       /*!< \brief Flags whether the variable is produced
                                            (DCT_PRODUCE) or is consumed (DCT_CONSUME)
                                            by the model in the coupling. */
  DCT_Scalar        VarFrequency;      /*!< \brief Frequency of production or consumption, the
                                            time step that determines when the operation
                                            is performed. */
  DCT_Boolean       VarMask;           /*!< \brief Boolean that indicates if the DCT_Field is
                                            masked, then the information in VarMaskMap will
                                            be used. */
  DCT_Boolean      *VarMaskMap;        /*!< \brief Points to a boolean matrix that masks the
                                            DCT_Field values. It's not fully implemented */
  DCT_Units         VarUnits;          /*!< \brief Meaningful physical units of the DCT_Field
                                            represents in the model. */
  DCT_Scalar        VarLastUpdate;     /*!< \brief Marks the last time the DCT_Field was produced
                                            or consumed. */
  DCT_String        VarFileName;       /*!< \brief \deprecated  Name of the file used to
                                            exchange values when no communication lib is used
                                            and file media is used instead (Used as matter of
                                            test). */
  DCT_Integer       VarNumCpl;         /*!< \brief Number of coupling variables involving this
                                            variable, i.e the length of the array VarCplIndx. */
  DCT_Integer      *VarCplIndx;        /*!< \brief Pointer to an array of indices indicating the
                                            entries in the array CPLMasterTable. */
  DCT_Data_Types    VarUserDataType;   /*!< \brief Indicates the data type used in the user data
                                            pointed by \c VarValues. */
  DCT_VoidPointer   VarValues;         /*!< \brief Pointer to the array of the user model data */
} DCT_Field;


/*******************************************************************/
/*         ===================================================     */
/*                              DCT_3d_Var                         */
/*         ===================================================     */
/*******************************************************************/
/*!   \brief Structure that uses DCT to represent a model 3D variable.

      DCT provides a set of data structures to represent the process of
      coupling.  There are 3 types of the \DCT_Vars. The DCT_3d_Var describes
      a 3D model variable. This structure gives the information to
      tell the DCT system what data to use, when do it, where is sent to
      or received from, and if there is any unit translation and data process.
*/
typedef struct {
  DCT_Name          VarName;           /*!< \brief Identification Name of the DCT_3d_Var.
                                            Character string containing the identifier
                                            name of the DCT_3d_Var instance. This name
                                            identifies the DCT_3d_Var variable in the DCT
                                            system wide. */
  DCT_String        VarDescription;    /*!< \brief Shot description of the DCT_3d_Var.
                                            Character string with an alternative longer
                                            description of the DCT_3d_Var instance. */
  DCT_Model        *VarModel;          /*!< \brief Pointer to the DCT_Model that the variable
                                            belongs to. */
  DCT_Boolean       VarCommit;         /*!< \brief Lock the DCT_3d_Var instance (no more changes
                                            can be made) when is added to a DCT_Model. */
  DCT_Tag           VarTag;            /*!< \brief Tag id of the DCT_3d_Var in the DCT system,
                                            and is set DCT_TAG_UNDEF when is created. */
  DCT_Integer       VarDim[3];         /*!< \brief Number of discretization points in each dim
                                            of a 2D sub-domain data. */
  DCT_Domain_Type   VarDomType[3];     /*!< \brief DCT_Domain_Type value to indicate if the mesh
                                            is structured, non-structured Cartesian
                                            rectangular, or general curvilinear
                                            corresponding to each direction. */
  DCT_Scalar       *VarLabels[3];      /*!< \brief Values of labels (tick marks), if it is non-
                                            structured; values of initial and end value,
                                            if it is structured; or the coordinates if it
                                            is general curvilinear. */
  DCT_Val_Loc       VarDataLoc;        /*!< \brief Type of DCT_Val_Loc that describes how the
                                            var data is located in the domain mesh. */
  DCT_Integer       VarStride[3];      /*!< \brief Indicates the stride (how many positions is
                                            located) to the data for each dimension . */
  DCT_Distribution  VarDistType;       /*!< \brief The type of the parallel layout.
                                            \see DCT_Distribution */
  DCT_Integer      *VarDistProcCounts; /*!< \brief Array of Process count in every direction of
                                            the layout (e.g., in 2x2 or 3X4X5 process
                                            layout, the values in the array are | 2 : 2 |
                                            or | 3 : 4 : 5 |). */
  DCT_Rank         *VarDistRank;       /*!< \brief Array with the rank of the processes involved
                                            in the DCT_3d_Var and with a length equal to
                                            the multiplication of all the members of
                                            \c VarDistProcCounts. */
  DCT_Time_Types    VarTimeUnits;      /*!< \brief Time units of the DTC_Fiels (It should be an
                                            integer defined in DCT_Time_Types). */
  DCT_Scalar        VarTimeIni;        /*!< \brief Indicates the time when the DCT_3d_Var becomes
                                            to be operational in the coupling sense. */
  DCT_ProdCons      VarProduced;       /*!< \brief Flags whether the variable is produced
                                            (DCT_PRODUCE) or is consumed (DCT_CONSUME)
                                            by the model in the coupling. */
  DCT_Scalar        VarFrequency;      /*!< \brief Frequency of production or consumption, the
                                            time step that determines when the operation
                                            is performed. */
  DCT_Boolean       VarMask;           /*!< \brief Boolean that indicates if the DCT_3d_Var is
                                            masked, then the information in VarMaskMap will
                                            be used. */
  DCT_Boolean      *VarMaskMap;        /*!< \brief Points to a boolean matrix that masks the
                                            DCT_3d_Var values. It's not fully implemented */
  DCT_Units         VarUnits;          /*!< \brief Meaningful physical units of the DCT_3d_Var
                                            represents in the model. */
  DCT_Scalar        VarLastUpdate;     /*!< \brief Marks the last time the DCT_3d_Var was produced
                                            or consumed. */
  DCT_String        VarFileName;       /*!< \brief \deprecated  Name of the file used to
                                            exchange values when no communication lib is used
                                            and file media is used instead (Used as matter of
                                            test). */
  DCT_Integer       VarNumCpl;         /*!< \brief Number of coupling variables involving this
                                            variable, i.e the length of the array VarCplIndx. */
  DCT_Integer      *VarCplIndx;        /*!< \brief Pointer to an array of indices indicating the
                                            entries in the array CPLMasterTable. */
  DCT_Data_Types    VarUserDataType;   /*!< \brief Indicates the data type used in the user data
                                            pointed by \c VarValues. */
  DCT_VoidPointer   VarValues;         /*!< \brief Pointer to the array of the user model data */
} DCT_3d_Var;

/*******************************************************************/
/*         ===================================================     */
/*                               DCT_4d_Var                        */
/*         ===================================================     */
/*******************************************************************/
/*!   \brief Structure that uses DCT to represent a model 4D variable.

      DCT provides a set of data structures to represent the process of
      coupling.  There are 3 types of the \DCT_Vars. DCT_4d_Var describes
      a 4D model variable. This structure gives the information to
      tell the DCT system what data to use, when do it, where is sent to
      or received from, and if there is any unit translation and data process.
*/
typedef struct {
  DCT_Name          VarName;           /*!< \brief Identification Name of the DCT_4d_Var.
                                            Character string containing the identifier
                                            name of the DCT_4d_Var instance. This name
                                            identifies the DCT_4d_Var variable in the DCT
                                            system wide. */
  DCT_String        VarDescription;    /*!< \brief Shot description of the DCT_4d_Var.
                                            Character string with an alternative longer
                                            description of the DCT_4d_Var instance. */
  DCT_Model        *VarModel;          /*!< \brief Pointer to the DCT_Model that the variable
                                            belongs to. */
  DCT_Boolean       VarCommit;         /*!< \brief Lock the DCT_4d_Var instance (no more changes
                                            can be made) when is added to a DCT_Model. */
  DCT_Tag           VarTag;            /*!< \brief Tag id of the DCT_4d_Var in the DCT system,
                                            and is set DCT_TAG_UNDEF when is created. */
  DCT_Integer       VarDim[4];         /*!< \brief Number of discretization points in each dim
                                            of a 2D sub-domain data. */
  DCT_Domain_Type   VarDomType[4];     /*!< \brief DCT_Domain_Type value to indicate if the mesh
                                            is structured, non-structured Cartesian
                                            rectangular, or general curvilinear
                                            corresponding to each direction. */
  DCT_Scalar       *VarLabels[4];      /*!< \brief Values of labels (tick marks), if it is non-
                                            structured; values of initial and end value,
                                            if it is structured; or the coordinates if it
                                            is general curvilinear. */
  DCT_Val_Loc       VarDataLoc;        /*!< \brief Type of DCT_Val_Loc that describes how the
                                            var data is located in the domain mesh. */
  DCT_Integer       VarStride[4];      /*!< \brief Indicates the stride (how many positions is
                                            located) to the data for each dimension . */
  DCT_Distribution  VarDistType;       /*!< \brief The type of the parallel layout.
                                            \see DCT_Distribution */
  DCT_Integer      *VarDistProcCounts; /*!< \brief Array of Process count in every direction of
                                            the layout (e.g., in 2x2 or 3X4X5 process
                                            layout, the values in the array are | 2 : 2 |
                                            or | 3 : 4 : 5 |). */
  DCT_Rank         *VarDistRank;       /*!< \brief Array with the rank of the processes involved
                                            in the DCT_4d_Var and with a length equal to
                                            the multiplication of all the members of
                                            \c VarDistProcCounts. */
  DCT_Time_Types    VarTimeUnits;      /*!< \brief Time units of the DTC_Fiels (It should be an
                                            integer defined in DCT_Time_Types). */
  DCT_Scalar        VarTimeIni;        /*!< \brief Indicates the time when the DCT_4d_Var becomes
                                            to be operational in the coupling sense. */
  DCT_ProdCons      VarProduced;       /*!< \brief Flags whether the variable is produced
                                            (DCT_PRODUCE) or is consumed (DCT_CONSUME)
                                            by the model in the coupling. */
  DCT_Scalar        VarFrequency;      /*!< \brief Frequency of production or consumption, the
                                            time step that determines when the operation
                                            is performed. */
  DCT_Boolean       VarMask;           /*!< \brief Boolean that indicates if the DCT_4d_Var is
                                            masked, then the information in VarMaskMap will
                                            be used. */
  DCT_Boolean      *VarMaskMap;        /*!< \brief Points to a boolean matrix that masks the
                                            DCT_4d_Var values. It's not fully implemented */
  DCT_Units         VarUnits;          /*!< \brief Meaningful physical units of the DCT_4d_Var
                                            represents in the model. */
  DCT_Scalar        VarLastUpdate;     /*!< \brief Marks the last time the DCT_4d_Var was produced
                                            or consumed. */
  DCT_String        VarFileName;       /*!< \brief \deprecated  Name of the file used to
                                            exchange values when no communication lib is used
                                            and file media is used instead (Used as matter of
                                            test). */
  DCT_Integer       VarNumCpl;         /*!< \brief Number of coupling variables involving this
                                            variable, i.e the length of the array VarCplIndx. */
  DCT_Integer      *VarCplIndx;        /*!< \brief Pointer to an array of indices indicating the
                                            entries in the array CPLMasterTable. */
  DCT_Data_Types    VarUserDataType;   /*!< \brief Indicates the data type used in the user data
                                            pointed by \c VarValues. */
  DCT_VoidPointer   VarValues;         /*!< \brief Pointer to the array of the user model data */
} DCT_4d_Var;



/********************************************************************************/
/* ------------------------------------------------------  Function Prototypes  */
/********************************************************************************/

/********************************************************************/
/*          ===================================================     */
/*           Functions about DCT_Field defined in dct_fields.c      */
/*          ===================================================     */
/********************************************************************/
DCT_Error DCT_Create_Field( DCT_Field *var, const DCT_Name,
                            const DCT_String, const DCT_Units,
                            const DCT_ProdCons);
DCT_Error DCT_Set_Field_Dims( DCT_Field *, const DCT_Integer,
                              const DCT_Integer );
DCT_Error DCT_Set_Field_Val_Location( DCT_Field *, const DCT_Val_Loc);
DCT_Error DCT_Set_Field_Strides( DCT_Field *, const DCT_Integer,
                                 const DCT_Integer);
DCT_Error DCT_Set_Field_Time( DCT_Field *, const DCT_Time_Types,
                              const DCT_Scalar);
DCT_Error DCT_Set_Field_Dist( DCT_Field *, const DCT_Distribution,
                              const DCT_Integer *);
DCT_Error DCT_Set_Field_Mask( DCT_Field *, const DCT_Boolean *);
DCT_Error DCT_Set_Field_Freq_Production( DCT_Field *, const DCT_Scalar );
DCT_Error DCT_Set_Field_Freq_Consumption( DCT_Field *, const DCT_Scalar );
DCT_Error DCT_Set_Field_Labels( DCT_Field *, const DCT_Domain_Type,
                                const DCT_Scalar *, const DCT_Integer,
                                const DCT_Domain_Type, const DCT_Scalar *,
                                const DCT_Integer);
DCT_Error DCT_Set_Field_Units( DCT_Field *, const DCT_Units);
DCT_Error DCT_Set_Field_Values( DCT_Field *, const DCT_Data_Types,
                                const DCT_VoidPointer);
DCT_Error DCT_Destroy_Field( DCT_Field * );

/********************************************************************/
/*          ===================================================     */
/*           Functions User Support Operations on Data Fields       */
/*          ===================================================     */
/********************************************************************/



/*******************************************************************/
/*         ===================================================     */
/*         Functions about DCT_3d_Var defined in dct_3d_var.c      */
/*         ===================================================     */
/*******************************************************************/
DCT_Error DCT_Create_3d_Var( DCT_3d_Var *, const DCT_Name,
                             const DCT_String, const DCT_Units,
                             DCT_ProdCons);
DCT_Error DCT_Set_3d_Var_Dims( DCT_3d_Var *, const DCT_Integer,
                                const DCT_Integer, const DCT_Integer);
DCT_Error DCT_Set_3d_Var_Val_Location( DCT_3d_Var *, const DCT_Val_Loc);
DCT_Error DCT_Set_3d_Var_Strides( DCT_3d_Var *, const DCT_Integer,
                                  const DCT_Integer, const DCT_Integer);
DCT_Error DCT_Set_3d_Var_Time( DCT_3d_Var *, const DCT_Time_Types,
                               const DCT_Scalar);
DCT_Error DCT_Set_3d_Var_Dist( DCT_3d_Var *, const DCT_Distribution,
                               const DCT_Integer *);
DCT_Error DCT_Set_3d_Var_Mask( DCT_3d_Var *, const DCT_Boolean *);
DCT_Error DCT_Set_3d_Var_Freq_Production( DCT_3d_Var *, const DCT_Scalar );
DCT_Error DCT_Set_3d_Var_Freq_Consumption( DCT_3d_Var *, const DCT_Scalar );
DCT_Error DCT_Set_3d_Var_Labels( DCT_3d_Var *, const DCT_Domain_Type,
                                 const DCT_Scalar *, const DCT_Integer,
                                 const DCT_Domain_Type, const DCT_Scalar *,
                                 const DCT_Integer, const DCT_Domain_Type,
                                 const DCT_Scalar *, const DCT_Integer);
DCT_Error DCT_Set_3d_Var_Units( DCT_3d_Var *, const DCT_Units);
DCT_Error DCT_Set_3d_Var_Values( DCT_3d_Var *, const DCT_Data_Types,
                                 const DCT_VoidPointer);
DCT_Error DCT_Destroy_3d_Var( DCT_3d_Var * );

/*******************************************************************/
/*         ===================================================     */
/*          Functions User Support Operations on Data 3d_Vars      */
/*         ===================================================     */
/*******************************************************************/



/*******************************************************************/
/*         ===================================================     */
/*         Functions about DCT_4d_Var defined in dct_4d_var.c      */
/*         ===================================================     */
/*******************************************************************/
DCT_Error DCT_Create_4d_Var( DCT_4d_Var *, const DCT_Name, const DCT_String,
                             const DCT_Units, DCT_ProdCons);
DCT_Error DCT_Set_4d_Var_Dims( DCT_4d_Var *, const DCT_Integer,
                               const DCT_Integer, const DCT_Integer,
                               const DCT_Integer);
DCT_Error DCT_Set_4d_Var_Val_Location( DCT_4d_Var *, const DCT_Val_Loc);
DCT_Error DCT_Set_4d_Var_Strides( DCT_4d_Var *, const DCT_Integer,
                                  const DCT_Integer, const DCT_Integer,
                                  const DCT_Integer);
DCT_Error DCT_Set_4d_Var_Time( DCT_4d_Var *, const DCT_Time_Types,
                               const DCT_Scalar);
DCT_Error DCT_Set_4d_Var_Dist( DCT_4d_Var *, const DCT_Distribution,
                               const DCT_Integer *);
DCT_Error DCT_Set_4d_Var_Mask( DCT_4d_Var *, const DCT_Boolean *);
DCT_Error DCT_Set_4d_Var_Freq_Production( DCT_4d_Var *, const DCT_Scalar );
DCT_Error DCT_Set_4d_Var_Freq_Consumption( DCT_4d_Var *, const DCT_Scalar );
DCT_Error DCT_Set_4d_Var_Labels( DCT_4d_Var *, const DCT_Domain_Type,
                                 const DCT_Scalar *, const DCT_Integer,
                                 const DCT_Domain_Type, const DCT_Scalar *,
                                 const DCT_Integer, const DCT_Domain_Type,
                                 const DCT_Scalar *, const DCT_Integer,
                                 const DCT_Domain_Type, const DCT_Scalar *,
                                 const DCT_Integer);
DCT_Error DCT_Set_4d_Var_Units( DCT_4d_Var *, const DCT_Units);
DCT_Error DCT_Set_4d_Var_Values( DCT_4d_Var *, const DCT_Data_Types,
                                 const DCT_VoidPointer);
DCT_Error DCT_Destroy_4d_Var( DCT_4d_Var * );

/*******************************************************************/
/*         ===================================================     */
/*          Functions User Support Operations on Data 4d_Vars      */
/*         ===================================================     */
/*******************************************************************/



/*******************************************************************/
/*         ===================================================     */
/*           Functions about DCT_Model defined in dct_model.c      */
/*         ===================================================     */
/*******************************************************************/
DCT_Error DCT_Create_Model( DCT_Model *, const DCT_Name, const DCT_String,
                              const DCT_Integer );

DCT_Error DCT_Set_Model_Time( DCT_Model *, const DCT_Scalar,
                              const DCT_Scalar, const DCT_Time_Types);

DCT_Error DCT_Set_Model_RSpaced_Dom( DCT_Model *, const DCT_Integer,
                                     const DCT_Scalar [], const DCT_Scalar [],
                                     const DCT_Integer []);

DCT_Error DCT_Set_Model_Dom( DCT_Model *, const DCT_Integer, const DCT_Integer,
                             const DCT_Domain_Type, const DCT_Scalar [],
                             const DCT_Integer );

DCT_Error DCT_Set_Model_GenCur_Dom( DCT_Model *, const DCT_Integer ,
                                    const DCT_Scalar *, const DCT_Scalar *,
                                    const DCT_Scalar *, const DCT_Integer [] );

DCT_Error DCT_Set_Model_ParLayout( DCT_Model *, const DCT_Distribution,
                                   const DCT_Integer *); /*, const DCT_Rank *);*/

DCT_Error DCT_Set_Model_SubDom( DCT_Model *, const DCT_Rank *,
                                const DCT_Integer *, const DCT_Integer *);

DCT_Error DCT_Set_Model_Production( DCT_Model *, DCT_VoidPointer, const DCT_Name,
                                    const DCT_Object_Types, const DCT_Units,
                                    const DCT_Time_Types, const DCT_Scalar,
                                    const DCT_Scalar);
DCT_Error DCT_Set_Model_Consumption( DCT_Model *, DCT_VoidPointer,
                                     const DCT_Name, const DCT_Object_Types,
                                     const DCT_Units, const DCT_Time_Types,
                                     const DCT_Scalar, DCT_Scalar);

DCT_Error DCT_Set_Model_Var( DCT_Model *, DCT_VoidPointer, const DCT_Object_Types );

DCT_Error DCT_Update_Model_Time( DCT_Model * );

DCT_Error DCT_Destroy_Model( DCT_Model * );

/*******************************************************************/
/*         ===================================================     */
/*           Functions User Support Operations on Data Model       */
/*         ===================================================     */
/*******************************************************************/



/*******************************************************************/
/*         ===================================================     */
/*         Functions about DCT_Couple defined in dct_couple.c      */
/*         ===================================================     */
/*******************************************************************/
DCT_Error DCT_Create_Couple( DCT_Couple *, const DCT_Name,
                             const DCT_String, DCT_Model *,
                             const DCT_Name );

DCT_Error DCT_Set_Coupling_Vars( DCT_Couple *, const DCT_VoidPointer,
                                 const DCT_Name, const DCT_Object_Types,
                                 const DCT_Data_Transformation );

DCT_Error DCT_Destroy_Couple( DCT_Couple * );

/*******************************************************************/
/*         ===================================================     */
/*          Functions User Support Operations on Data Couple       */
/*         ===================================================     */
/*******************************************************************/



/*******************************************************************/
/*         ===================================================     */
/*               Functions about DCT Registration Phase            */
/*                       defined in dct_broker.c                   */
/*         ===================================================     */
/*******************************************************************/
DCT_Error DCT_Assign_Broker( DCT_Rank, DCT_Comm );

DCT_Error DCT_BeginRegistration( DCT_Comm );

DCT_Error DCT_EndRegistration(  );

DCT_Error DCT_Finalized(  );


/*******************************************************************/
/*         ===================================================     */
/*            Functions about DCT Data Communication Phase         */
/*                     defined in dct_comm_data.c                  */
/*         ===================================================     */
/*******************************************************************/
DCT_Error DCT_Send_Field( DCT_Field * );

DCT_Error DCT_Send_3d_Var( DCT_3d_Var * );

DCT_Error DCT_Send_4d_Var( DCT_4d_Var * );

DCT_Error DCT_Recv_Field( DCT_Field * );

DCT_Error DCT_Recv_3d_Var( DCT_3d_Var * );

DCT_Error DCT_Recv_4d_Var( DCT_4d_Var * );

/*******************************************************************/
/*         ===================================================     */
/*                      Functions about data utils                 */
/*                     defined in dct_data_utils.c                 */
/*         ===================================================     */
/*******************************************************************/
DCT_Error DCT_Gen_Spatial_Labels ( const DCT_Scalar, const DCT_Scalar,
                                   const DCT_Scalar, DCT_Scalar **,
                                   DCT_Integer *);
DCT_Error DCT_Gen_Number_Labels( const DCT_Scalar, const DCT_Scalar,
                                 const DCT_Integer, DCT_Scalar **,
                                 DCT_Integer *);

DCT_Scalar DCT_Change_TimeUnit( DCT_Scalar, DCT_Time_Types, DCT_Time_Types );

# endif /* DCT_H */

