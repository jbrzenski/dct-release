/**************************************************************/
/*              Distributed Coupling Toolkit (DCT)            */
/*!
   \file macros.h
  
   \brief the header file contains all the DCT utility
    macros.
  
    This header file contains all the macros used in DCT, specially
    to handle error control.
  
    \date Date of Creation: Jul 2, 2008.  

    \author Dany De Cecchis: dcecchis@gmail.com
    \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.
 */
/**************************************************************/


# ifndef MACROS_H
#   define MACROS_H

/* -----------------------------------------------  Include Files  */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

/* ----------------------------------------------  Macros Defined  */

/*! \def DCTCHKERR( x )
    \brief Checks if the \c DCT_Error \a x is reporting an error, and exit.
   
    This macro handles the error checking of \c DCT_Error variable \a x.
    If an error exists, the macro prints the error code, the corresponding
    message and the file name and line where occurs.
*/
# ifdef DCTCHKERR
#    undef DCTCHKERR
# endif  /* DCTCHKERR */
# ifdef MPI_INCLUDED
# define DCTCHKERR( x ) {\
    if ( (x.Error_code != DCT_SUCCESS) ) {\
       fflush( NULL );\
       fprintf( stderr, "\nDCTCHKERR[%d]: %s", x.Error_code, x.Error_msg );\
       fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );\
       fflush( NULL);\
       MPI_Abort(MPI_COMM_WORLD, x.Error_code);\
    }\
  }
# else
# define DCTCHKERR( x ) {\
    if ( (x.Error_code != DCT_SUCCESS) ) {\
       fflush( NULL );\
       fprintf( stderr, "\nDCTCHKERR[%d]: %s", x.Error_code, x.Error_msg );\
       fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );\
       fflush( NULL);\
       exit(x.Error_code);\
    }\
  }
# endif /* End of ifdef MPI_INCLUDED */

/*! \def DCTERROR( x, y, w )
    \brief Checks if \a x is true. If it is, prints \a y, and execute \a w.
    
    Checks the assertion of the expression \a x. If it is true,
    the message given the string \a y is printed as error message, and
    the instruction \a w is executed.
   
    This macro handles errors that should not be convenient to use 
    the \c DCT_Error to handle the error. x is the expression to be evaluated
    together with errno, y the message to be displayed and w the action to
    take place to handle the error.
*/
# ifdef DCTERROR
#     undef DCTERROR
# endif  /* DCTERROR */
# define DCTERROR( x, y, w ) if ( x ) {\
       fflush( NULL );\
       fprintf( stderr, "DCT_ERROR[%s]: %s\n", #x, y );\
       fprintf( stderr, "ERROR Pe[%d]: in file %s (line: %d)\n", DCT_procinfo.ProcDCTRank, __FILE__ , __LINE__ );\
       fflush( NULL); w;\
  }

/*! \def DCTWARN( x )
    \brief Is a macro that handles warnings messages.
   
    This macro just prints the warning message in the string \a x, reporting
    the location where the warning is in the code.
*/
#   ifdef DCTWARN
#       undef DCTWARN
#   endif  /* DCTWARN */
#   define DCTWARN( x ) {\
          fprintf( stderr, "WARNING ( %s, line: %d):\n** %s\n", __FILE__ , __LINE__, x );\
          fflush(stderr);\
      }

# endif /*  MACROS_H */
