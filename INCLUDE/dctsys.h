/**************************************************************/
/*              Distributed Coupling Toolkit (DCT)            */
/*!
   \file dctsys.h
  
   \brief Contains all the DCT definitions that are not
          available for a DCT user (private to DCT).

    This header file contains all the definitions (data
    type structures, constants and function prototypes that 
    are not available for a DCT user. These definitions are used
    internally by the DCT system to carry out its purpose.

    \warning The global system variables are defined as \c extern
             to be included in all the DCT system, except in the place where
             they are declared, which is in the dct_broker.c file. For that
             the directive DCT_GLB_VARS is used to control that.

    \date Date of Creation: Jul 23, 2008.  

    \author Dany De Cecchis: dcecchis@gmail.com
    \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.
 */
/**************************************************************/

# ifndef DCTSYS_H
#   define DCTSYS_H

/* -----------------------------------------------  Include Files  */

/*********************  The subdomain internal structures ***************************/

/**************************************************/
/*             DCT_Intersect_SubDom               */
/*!  Structure which represents a piece of domain  
     when a subdomain is intersected with its      
     couterpart model. */
typedef struct {
  DCT_Scalar      *IniVal;   /*!< Array containing initial values of 
                                  the subdomain intersection between 
                                  models domains in each direction.  
                                  The length of the array is dim. */
  DCT_Scalar      *EndVal;   /*!< Array containing ending values of  
                                  the subdomain intersection between 
                                  models domains in each direction. */
  DCT_Rank         RankProc; /*!< Array of the processor assigned   
                                  to the correspondent subdomian. */
} DCT_Intersect_SubDom;

/*****************  END The subdomain internal structures ***************************/


/**********************************************************/
/*                   DCT_Produce_Plan                     */
/*!  Structure that represents the complete communication    
     of a producer variable in a process. */
typedef struct {
  DCT_Tag            VarMsgTag;   /*!< Message tag that identify the two         
                                       variables exchange process, is defined    
                                       using the producer and consumer tags. */
  DCT_Integer        Nmsg;        /*!< Number of massages to send or receive. */
  DCT_Comm_Datatype *DType;       /*!< Data type to be transmitted. */
  DCT_Request       *reqsts;      /*!< Array of transmission requests. */
} DCT_Produce_Plan;

/**********************************************************/
/*                   DCT_Consume_Plan                     */
/*!  Structure that represents the complete communication    
     of a producer variable in a process. */
typedef struct {
  DCT_Tag                   VarMsgTag;    /*!< Message tag that identify the two         
                                               variables exchange process, is defined    
                                               using the producer and consumer tags. */
  DCT_Integer               Nmsg;         /*!< Number of massages to send or receive. */
  DCT_Request              *reqsts;       /*!< Array of transmission requests. */
  DCT_Comm_Datatype        *DType;        /*!< Data type to be transmitted. */
  DCT_Data_Transformation   VarTransf;    /*!< Type of transformation between meshes. */
  DCT_Units                 VarChUnits;   /*!< Unit type defined on the producer side. */
  DCT_Integer              *VarIniPos;    /*!< Pointer to an array of length dimension   
                                               of initial position in user's memory      
                                               space. */
  DCT_Integer              *VarNpts;      /*!< Pointer to an array of length dimension   
                                               of number of point in each direction      
                                               indicating the number of points in the    
                                               user's memory space. */
  DCT_Domain_Type          *VarDomType;   /*!< Pointer to an array of the type of        
                                               discretization. */
  DCT_VoidPointer           VarTempVal;   /*!< Pointer to an array where the filtered    
                                               values are stored before to be copied     
                                               to user's memory space. */
  DCT_VoidPointer          *VarLabels;    /*!< Pointers of the an array of the           
                                               transformation weights. */
  DCT_Integer             **VarInt;       /*!< Pointer to an array of DCT_Integer        
                                               pointers, used to index the weights. */
  DCT_Integer              *VarMask;      /*!< Array of DCT_Integer defining the mask    
                                               for the operation between the             
                                               transformation and unit conversion. */
  DCT_Data_Types            VarRcvdDType; /*!< User data type from the producer Model. */
  DCT_Integer              *VarRcvdNpts;  /*!< Pointer to an array of length dimension   
                                               of number of point in each direction to   
                                               be received. */
  DCT_VoidPointer           VarRcvdVal;   /*!< Pointer to an array where the received    
                                               values are stored before to be filtered. */
} DCT_Consume_Plan;

/**********************************************************/
/*                     DCT_Hollow_Var                     */
/*!  Structure to represent the remote variable, and its     
     data is declared in another process */
typedef struct {
  DCT_Name          VarName;       /*!< Remote name, must match exactly with     
                                        actual name intended to be coupled. */
  DCT_Scalar        VarTimeIni;    /*!< Initial time that the variable should    
                                        be updated. */
  DCT_Scalar        VarLastUpdate; /*!< Simulation time that the variable was    
                                        last updated. */
  DCT_Scalar        VarFrequency;  /*!< Interval of simulation time for the      
                                        variable should be updated. */
} DCT_Hollow_Var;

/**********************************************************/
/*                   DCT_Hollow_CPLVar                    */
/*!  Structure to represent the remote coupled variable      
     inside the DCT_Couple structure, and is used            
     by the broker to receive the \DCT_Vars information */
typedef struct {
  DCT_Name          VarName;         /*!< Remote name, must match exactly with     
                                          actual name intended to be coupled. */
  DCT_Model        *VarModel;        /*!< Pointer to the DCT_Model that the        
                                          variable belongs to. */
  DCT_Tag           VarTag;          /*!< Value tag indetifying the variable. */
  DCT_Object_Types  CoupleVarTypes;  /*!< Type of the actual \DCT_Vars that is
                                          defined in the remote DCT_Model. */
  DCT_Units         VarUnits;        /*!< Units of the field or variable. */
  DCT_Time_Types    VarTimeUnits;    /*!< Time units of the remote model time
                                          in which \DCT_Vars is defined. */
  DCT_Scalar        VarTimeIni;      /*!< Indicates the time when the \DCT_Vars    
                                          becomes to be operational . */
  DCT_Scalar        VarFrequency;    /*!< Frequency of production or Consumption. */
  DCT_ProdCons      VarProduced;     /*!< Flags whether the variable is produced   
                                          or Consumed by the remote model. */
  DCT_Data_Types    VarUserDataType; /*!< Type that indicates the user data       
                                          plugged to the remote DCT_Vars data
                                          structure. */
} DCT_Hollow_CPLVar;


/**********************************************************/
/*                     DCT_CPL_Node                       */
/*!  Structure to store the var couple, which is used when   
     a coupled variable is added to a DCT_Couple structure   
     or by the broker when the variable info is received */
typedef struct {
  DCT_VoidPointer          CoupleVars1; /*!< Pointer to the 1st variable. It could     
                                             be a local \DCT_Vars structure or a      
                                             DCT_Hollow_CPLVar. */
  DCT_VoidPointer          CoupleVars2; /*!< Pointer to the 2nd variable. It could     
                                             be a DCT_Hollow_Var structure or a        
                                             DCT_Hollow_CPLVar. */
  DCT_Object_Types         CVarType;    /*!< Determine the type of structure that      
                                             fist pointer is pointing to. */
  DCT_Data_Transformation  CVarTrans;   /*!< Determine the type of transformation is   
                                             used from one mesh to the another. */
} DCT_CPL_Node;


/**********************************************************/
/*                     DCT_Var_Node                       */
/*!  Structure to store the var tag and the var memory       
     address. This is used to sort the list of all DCT       
     variables after the broker finishes its role. Then,     
     it is defined an order to communicate independelly      
     processes working on a pair of coupled vars. The        
     order is absolute among all variables and processes     
     to avoid death lock. */
typedef struct {
  DCT_Tag           VarMsgTag;  /*!< Variable Tag assigned by broker      
                                     (key field). */
  DCT_Object_Types  VarType;    /*!< Type of the \DCT_Vars that is
                                     pointed by the node. */
  DCT_VoidPointer   VarAddr;    /*!< Pointer to the actual DCT_Var data
                                     structure. */
  DCT_ProdCons      VarProd;    /*!< Indicates if the \DCT_Vars podruces or   
                                     consumes. */
  DCT_Integer       CPLMTIndex; /*!< The corresponding index of this DCT_Vars in the
                                     global data structure array CPLMasterTable. */
  DCT_VoidPointer   SDComm;     /*!< Pointer to a DCT_SDProduce_Plan or a       
                                     DCT_SDConsume_Plan variable, depending
                                     on VarProd value. */
} DCT_Var_Node;

/**************************************************/
/*                  DCT_Dist_Procs                */
/*!  Structure which represents the registration     
     communication environment. */
typedef struct{
  DCT_Integer    ProcCommSize;  /*!< Range or size of the coupling     
                                     communicator. */
  DCT_Rank       ProcDCTRank;   /*!< Rank or id of the processor in    
                                     the DCT_Comm_World communicator. */
  DCT_Rank       ProcDCTBroker; /*!< Rank or id of the broker in the   
                                     DCT_Comm_World communicator. */
} DCT_Dist_Procs;


/**************************************************/
/*                DCT_CPL_Vars                    */
/*! Structure that represents the coupling process   
    between two variables */
typedef struct{
  DCT_VoidPointer    *CPLVarLocal;  /*!< Pointer to an array of pointers   
                                         to \DCT_Vars instances. */
  DCT_Hollow_Var    **CPLVarRemote; /*!< Pointer to an array of pointers   
                                         to DCT_Hollow_Var instance representing
                                         the remote \DCT_Vars. */
  DCT_Object_Types   *CPLVarTypes;  /*!< Pointer to an array to DCT_Object_Types            
                                         values given the respective type of
                                         \DCT_Vars. */
  DCT_ProdCons       *CPLVarProd;   /*!< Indicates if the \DCT_Vars produces or   
                                         consumes. */
  DCT_VoidPointer    *CPLDataComm;  /*!< Pointer to array of pointers to   
                                         communication structures DCT_Produce_Plan          
                                         or DCT_Consume_Plan, depending on the
                                         corresponding CPLVarProd value. */
} DCT_CPL_Vars;

/**************************************************/
/*            DCT_CPL_Master_Table                */
/*!  Root node to the list structure that stores     
     the coupling information for all \DCT_Vars. */
typedef struct{
  DCT_Integer    TotNumCPLVars;  /*!< Length of the array. */
  DCT_CPL_Vars   CPLMasterTable; /*!< Base node to the each array of the table         
                                      of coupled variables. */
} DCT_CPL_Master_Table;

/****** Defining the registration states constants ********/
/*!  \brief Type of different states during DCT Registration Phase.

    Stats the different states to control the consistency 
    of the DCT system during DCT Registration Phase. The behavior or
    the ability to call certain functions depend on the state where
    the DCT Registration Phase is. */
typedef enum{
   DCT_NULL_STATE=        0, /*!< DCT_BeginRegistration function has
                                  not been called. */
   DCT_BEGIN_STATE=       1, /*!< DCT_BeginRegistration function has
                                  been called. */
   DCT_BROKER_STATE=      2, /*!< DCT_EndRegistration function has     
                                  been called, but the broker is       
                                  using the information (only broker   
                                  has this state). */
   DCT_ALLREGISTER_STATE= 3, /*!< After broker's role, all processes
                                  work on the calculations. */
   DCT_END_STATE=         4  /*!< DCT_EndRegistration function has
                                  been completed, and the DCT Registration
                                  Phase is concluded. */
}  DCT_Registration_State;

/******  Defining the number of messages constant  ********/
#define DCT_NO_MSG     0 /*!< Value that means no messages defined */

#ifndef DCT_GLB_VARS   /* DCT_GLB_VARS to declare the actual extern variables
                          DCT_GLB_VARS is defined inside of the dct_broker.c file */

/* --------------------------------------------  Global Variables  */

/** The actual variable definitions are defined below and
    included in dct_broker.c **/

/*! Check (count) the three steps during registration. */
  extern DCT_Registration_State   DCT_Step_Check;

/*!  DCT_List structure where each process registers
     the DCT_Couple structures created. The maximum amount
     of DCT_Couple that can be registered is DCT_MAX_COUPLE. */
  extern DCT_List             *DCT_Reg_Couple;

/*!  DCT_List structure where each process registers
     the DCT_Model structures created. The maximum amount
     of DCT_Couple that can be registered is DCT_MAX_MODEL. */
  extern DCT_List             *DCT_Reg_Model;

/*!  DCT_List structure where each process registers
     the DCT_Vars structures created in its system. The maximum amount
     of \DCT_Vars that can be registered is DCT_MAX_VARS. */
  extern DCT_List             *DCT_Reg_Vars;
  
/*!  Counter of local DCT_Couple structures created. The maximum amount
     of DCT_Couple that can be registered is DCT_MAX_COUPLE. */
  extern DCT_Integer           DCT_LocalCouple;

/*!  Counter of local DCT_Model structures created. The maximum amount
     of DCT_Couple that can be registered is DCT_MAX_MODEL. */
  extern DCT_Integer           DCT_LocalModel;

/*!  Counter of local \DCT_Vars structures created. The maximum amount
     of \DCT_Vars that can be registered is DCT_MAX_VARS. */
  extern DCT_Integer           DCT_LocalVars;

/*!  Counter of local Couple Variables declared. The maximum amount
     of \DCT_Vars that can be registered is DCT_MAX_VARS. */
  extern DCT_Integer           DCT_CPLVars;

/*! Master table to store the communication pattern and 
    trasformation between each coupled \DCT_Vars. */
  extern DCT_CPL_Master_Table  DCT_CPL_Table;

/*!  The DCT_Comm is the communicator of the hole DCT system,
     and it is set during the DCT_BeginRegistration. */
  extern DCT_Comm              DCT_Comm_World;

/*!  The DCT_Comm passed by the user when call
     DCT_BeginRegistration or DCT_Assign_Broker  */
  extern DCT_Comm              DCT_User_Comm;

/*!  The DCT_Group is the communication of the hole DCT system,
     and it is set during the DCT_BeginRegistration  */
  extern DCT_Group             DCT_Group_World;

/*! Global variable to indetify the DCT_Comm_World size,
    local process id, and Broker id. \see DCT_Dist_Procs  */
  extern DCT_Dist_Procs        DCT_procinfo;

#else  /* DCT_GLB_VARS to declare the actual global variables */

/* --------------------------------------------  Global Variables  */
/*! Check (count) the three steps during registration */
DCT_Registration_State  DCT_Step_Check = DCT_NULL_STATE;

/*!  DCT_List structure where each processor registers
    the DCT_Couple structures created                */
DCT_List    *DCT_Reg_Couple = (DCT_List *)NULL;

/*!  DCT_List structure where each processor registers
    the DCT_Model structures created                */
DCT_List    *DCT_Reg_Model = (DCT_List *)NULL;

/*!  DCT_List structure where each processor registers
    the DCT_{Vars} structures created                */
DCT_List    *DCT_Reg_Vars = (DCT_List *)NULL;

/*!  Counter of local DCT_Couple structures created   */
DCT_Integer  DCT_LocalCouple = 0;

/*!  Counter of local DCT_Model structures created   */
DCT_Integer  DCT_LocalModel = 0;

/*!  Counter of local DCT Variables structures created */
DCT_Integer  DCT_LocalVars = 0;

/*!  Counter of local Couple Variables declarared */
DCT_Integer  DCT_CPLVars = 0;

/*! Master table to store the communication pattern and 
    trasformation between each variable                 */
DCT_CPL_Master_Table  DCT_CPL_Table = { 0,
                                        { NULL, NULL, NULL, NULL, NULL } };

/*!  The DCT_Comm_World is set during the DCT_BeginRegistration  */
DCT_Comm    DCT_Comm_World = DCT_COMM_NULL;
/*!  The DCT_Comm passed by the user when call DCT_BeginRegistration or DCT_Assign_Broker  */
DCT_Comm    DCT_User_Comm = DCT_COMM_NULL;
/*!  The DCT_Group_World is set during the DCT_BeginRegistration  */
DCT_Group   DCT_Group_World = DCT_GROUP_NULL;

/*! Global variable to indetify the DCT_Comm_World size,
   processor id, and Broker id (see \c DCT_Dist_Procs)   */
DCT_Dist_Procs   DCT_procinfo = { 0,
                             DCT_UNDEFINED_RANK,
                             DCT_UNDEFINED_RANK };

#endif /* DCT_GLB_VARS */

/* -----------------------------------------  Function Prototypes  */

/************************************************/
/*          defined in dct_data_utils.c         */
/************************************************/
DCT_Boolean Set_Distribution( const DCT_Distribution, const DCT_Integer *,
                              DCT_Integer **, DCT_Integer *);

DCT_Boolean DCT_Check_Intersec( const DCT_Integer , const DCT_Integer ,
                                const DCT_Data_SubDom * );

DCT_Boolean Set_Distribution_Type( const DCT_Distribution, const DCT_Integer *,
                                  DCT_Distribution *, DCT_Integer **);

DCT_Error DCT_Mask_Copy( DCT_Boolean *, const DCT_Boolean *,
                         const DCT_Object_Types,
                         const DCT_Integer *);

int DCT_Model_And_Var_DomCheck( DCT_Model_Dom *, DCT_Domain_Type *,
                                DCT_Scalar *[], DCT_Integer *, DCT_Integer );

int DCT_Extract_SubDom_Ranks( DCT_Model_Dom *, DCT_Rank **, int );

int DCT_SubDom_ListOrder( DCT_Data_SubDom *, int, int );

int DCT_Couple_ListOrder( DCT_Couple **, int );

int DCT_Model_ListOrder( DCT_Model **, int );

int DCT_CheckandSwap( DCT_Scalar *, int *, int *, int );

int DCT_index_bsearch(const DCT_Scalar *, const int, const int,
                      const DCT_Scalar, const int);

int DCT_GC_index_bsearch(const DCT_Scalar *, const int, const int,
                      const DCT_Scalar, const int, const int *,
                      const int, const int );

int DCT_SearchIndex( const DCT_Scalar *, const DCT_Integer, const DCT_Domain_Type,
                     const int, const int, const DCT_Scalar, const int, DCT_Scalar * );

int DCT_Return_SubDom_Interc( DCT_Model_Dom *, DCT_Scalar *[], DCT_Integer *,
                              DCT_Scalar *, DCT_Scalar *, DCT_Integer *, DCT_Integer *);

int DCT_Return_DomBoundVals( DCT_Model_Dom *, DCT_Scalar *[], DCT_Integer * );

int DCT_Return_SDBoundaryVals ( int, DCT_Data_SubDom *, DCT_Domain_Type *,
                                DCT_Integer *, DCT_Scalar **, DCT_Scalar *,
                                DCT_Scalar * );

int DCT_IncludedIndex_Cartes( DCT_Scalar *, int, int, DCT_Scalar, DCT_Scalar,
                              DCT_Integer *, DCT_Integer *, DCT_Integer *, DCT_Integer );

int DCT_IncludedIndex_Rectil( DCT_Scalar *, int, int, DCT_Scalar, DCT_Scalar,
                              DCT_Integer *, DCT_Integer *, DCT_Integer *, DCT_Integer );

int DCT_IncludedValues_Cartes( DCT_Scalar *, int, int, DCT_Scalar, DCT_Scalar,
                              DCT_Scalar *, DCT_Scalar *, DCT_Integer *,
                              DCT_Integer *, DCT_Scalar **, DCT_Integer );

int DCT_IncludedValues_Rectil( DCT_Scalar *, int, int, DCT_Scalar, DCT_Scalar,
                                DCT_Scalar *, DCT_Scalar *, DCT_Integer *,
                                DCT_Integer *, DCT_Scalar **, DCT_Integer );

int DCT_IncludedValues( int, int *, DCT_Domain_Type *, DCT_Scalar **, int *,
                        DCT_Scalar *, DCT_Scalar *, DCT_Scalar *, DCT_Scalar *,
                        DCT_Integer *, DCT_Integer *, DCT_Scalar **, int * );

int DCT_IncludedValues_GC( int, int *, DCT_Scalar **, int *, DCT_Scalar *, DCT_Scalar *,
                        DCT_Scalar *, DCT_Scalar *, DCT_Integer *, DCT_Integer *,
                        DCT_Scalar ** );

int DCT_CheckIfNeedMore( int, DCT_Domain_Type *, DCT_Scalar **, /* int *,*/ DCT_Scalar **,
                         int *, DCT_Consume_Plan *, DCT_SDConsume_Plan  *, int *,
                         DCT_Scalar *, int *, int * );

int DCT_BuildMsgTag( const DCT_Couple *, DCT_Tag *, int *, int*, DCT_ProdCons * );

DCT_Produce_Plan * DCT_ProdPlan_Init( DCT_Tag, DCT_Integer );

DCT_Consume_Plan * DCT_ConsPlan_Init( int, DCT_Tag, DCT_Data_Transformation,
                                      DCT_Units, DCT_Data_Types, DCT_Integer );
                                     
inline int DCT_CheckInclude ( int, DCT_Integer *, int * );

int DCT_IncludeNeighbor ( DCT_Rank, DCT_Integer *, DCT_Rank * );

int DCT_Calculate_Neighbors( DCT_Integer, /*DCT_Data_SubDom *,*/ DCT_Distribution,
                             DCT_Integer *, DCT_SDProduce_Plan *,
                             DCT_Rank **, int *, DCT_SDConsume_Plan * );

int DCT_Calculate_PerProc_Neighbors ( DCT_ProdCons, DCT_Integer, DCT_Integer, DCT_Rank,
                                      DCT_Data_SubDom *, DCT_Distribution, DCT_Integer *,
                                      DCT_SDProduce_Plan *, DCT_Rank *, int *,
                                      DCT_SDConsume_Plan * );

void * DCT_Data_Allocate( DCT_Data_Types, int *, int, int );

void * DCT_UserData_GetPtr( DCT_VoidPointer, DCT_Data_Types,
                            DCT_Integer *, int *, int );

int DCT_OnNewBoundary( DCT_Integer *, DCT_Integer *, DCT_Integer *, DCT_Integer *,
                       DCT_Integer *, int );

// int DCT_CHeck_Dims( DCT_Integer *, DCT_Integer *, int *, int );

int DCT_Var_Node_Comp( const void *, const void * );

int DCT_Calculate_Weight( DCT_Consume_Plan *, DCT_Scalar **, DCT_Scalar **,
                          DCT_Domain_Type *, DCT_Integer *, DCT_Data_Types,
                          DCT_Integer );

int DCT_Perfom_Filter( DCT_Consume_Plan *, DCT_Data_Types, DCT_Integer );

int DCT_Perfom_UnitConvertion( DCT_Consume_Plan *, DCT_VoidPointer, DCT_Data_Types,
                               DCT_Units, DCT_Integer *, DCT_Integer );

DCT_List *DCT_List_Add( DCT_List **, DCT_VoidPointer , DCT_Object_Types );

DCT_List * DCT_List_CHKAdd( DCT_List **, DCT_VoidPointer, DCT_Object_Types );

DCT_List * DCT_List_CHKAdd_VarsOnModel( DCT_List **, DCT_VoidPointer,
                                        DCT_Object_Types, DCT_Model * );

void DCT_List_ExtVar( DCT_List **, DCT_VoidPointer *, DCT_Object_Types * );

int DCT_List_Nnode (DCT_List **, int );

int DCT_List_Break_Nnode (DCT_List **, int );

DCT_List *DCT_List_Del( DCT_List **);

DCT_Integer DCT_List_Resume (DCT_List *);

void DCT_List_Ext (DCT_List **, DCT_VoidPointer *);

void DCT_List_Clear (DCT_List **);



# endif /* DCTSYS_H */
