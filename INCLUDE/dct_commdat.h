/**************************************************************/
/*              Distributed Coupling Toolkit (DCT)            */
/*!
   \file dct_commdat.h
  
   \brief Contains constants and data type definitions related
          with the communication library.
  
     This file contains all the constant and data type related
     with the communication library that is used. In addition, contains
     another constants related with the DCT communication system
     and global TAG identification.
  
    \attention The implementation is based on MPI.
    
    \todo Documentation for the use of steps 4 and 5 is needed
          for the data type DCT_Msg_Tags_Type. In addition to
          check the tag system because is inconsistent.

    \date Date of Creation: Jul 23, 2010: MPI version

    \author Dany De Cecchis: dcecchis@gmail.com
    \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.
 */
/**************************************************************/

# ifndef DCT_COMMDAT_H
#   define DCT_COMMDAT_H

/* -----------------------------------------------  Include Files  */
#include <mpi.h>

typedef MPI_Comm                DCT_Comm;   /*!< Define a communicator in the DCT
                                                 system related with the communication 
                                                 library. */
typedef MPI_Group               DCT_Group;  /*!< Define a group in the DCT system
                                                 related with the communication 
                                                 library. */
typedef MPI_Status              DCT_Status; /*!< It is a structure that gives the
                                                 status related with the
                                                 communication library. */
typedef MPI_Datatype            DCT_Comm_Datatype; /*!< Type of data related with a
                                                        communication operation. */
typedef MPI_Request             DCT_Request;  /*!< Structure related with a
                                                   communication request related
                                                   with the communication library. */
typedef int                     DCT_Rank;     /*!< Process Id a communicator in the DCT
                                                   system related with the communication
                                                   library. */
typedef int                     DCT_Tag;      /*!< Tag used in the DCT system related
                                                   with the communication library. */


#define DCT_TAG_UNDEF    -1   /*!< Constant for undefined tag */
       
/* ----------------------------------------  Constants Definition  */

/*******************************************************************/
#define DCT_DEFINED_MSGS  12 /*!< Number of defined type messages in the DCT.*/
/*******************************************************************/
/*                          DCT_Msg_Tags_Type                      */
/*                                                                 */
/*!   This type of variable defines the different kind of messages
      are used inside of the DCT system, during all the phases of
      the coupling process.

      The message tag is composed by a number of 7 digits:
     - The fist (leftmost) determines the phase:
          - 1 Registration;
          - 2 Communication;
          - 3 Finalizing.
     - The second digit determines the step for DCT registration is:
          - 0 Broker identification (not used).
          - 1 Non-broker processe registering Couple.
          - 2 Broker registration send tag for waiting for Model leader.
          - 3 Broker registration send tag asking for models and
              vars and designating as model leader.
          - 4 Mutual Model registration between leaders of models.
          - 5 Registration of variables from model leaders to the broker.
          - 6 Broker send back paired variable info to leader models.
          - 7 Used in test purpose.
          - 8 Definitely Var communication direction producer to consumer.
          - 9 Definitely Var communication direction consumer to producer.
     - The last 6 digits are decomposed as: 1 (leftmost) is the couple
       tag; next two, it is the producer var tag; the last two the consumer
       var tag.

*/
/*******************************************************************/
typedef enum {
  DCT_Msg_Tags_UNDEFINED=             0000000, /*!< Default unset message tag.*/
  /*  Tags during registration time      */
  DCT_Msg_Tags_COUPLE_REG=            1000000, /*!< Tag sent by no broker processes to
                                                    register a DCT_Couple to the broker.*/
  DCT_Msg_Tags_COUPLE_TAG=            1100000, /*!< Tag sent by the broker notifying the
                                                    DTC_Couple tag, and indicating the
                                                    process has to just wait for
                                                    variables schedule.*/
  DCT_Msg_Tags_CPLTAG_MOD=            1200000, /*!< Tag sent by the broker notifying the
                                                    DCT_Couple tag, and indicating that
                                                    the process must sent the rest of
                                                    the local couple information. i.e
                                                    DCT_Model, DCT_Field, ....*/
  DCT_Msg_Tags_CPLCNTR_MOD=           1300000, /*!< Tag used during the process in which
                                                    the broker sends the counter model
                                                    header to their respective model
                                                    leader.*/
  DCT_Msg_Tags_CNTR_MOD_DTLS=         1400000, /*!< Tag used by leaders of models to mu-
                                                    tually exchange their corresponding
                                                    model details with their counterpart
                                                    leader.*/
  DCT_Msg_Tags_MODVAR_REG=            1500000, /*!< Tag used when the broker send the
                                                    paired variables information to the
                                                    model leaders, and these to their
                                                    respective model co-processes.*/
  DCT_Msg_Tags_VARINFO_LEAD=          1600000, /*!< Tag used when the broker send the
                                                    paired variables information to the
                                                    model leaders, and these to their
                                                    respective model co-processes.*/
  DCT_Msg_Tags_VARINFO_PROCS=         1700000, /*!< Tag used for testing purpose.*/
  DCT_Msg_Tags_VAR_PROD_PLAN=         1800000, /*!< Tag used when the producers send the
                                                    information to the consumers.*/
  DCT_Msg_Tags_VAR_CONS_PLAN=         1900000, /*!< Tag used when the consumers send the
                                                    information to the producer asking
                                                    for more info or not.*/
  DCT_Msg_Tags_VAR_ACK=               2000000, /*!< Tag used as acknolegment (no more
                                                    requests) for more info or not.*/
  /*  Tags during the exchange time      */
  DCT_Msg_Tags_COMM_EXCH=             3000000  /*!< Tag used to define the tag message
                                                    between variables during the
                                                    exachange time.*/
} DCT_Msg_Tags_Type;

/*******************************************************************/
/* Depending of the architecture the upper bound of a message tag  */
/* could vary. Then a maximum defined of DCT_Couple, DCT_Model and */
/* DCT_Vars must be defined.                                       */
/*******************************************************************/
#define      DCT_MAX_COUPLE           10   /*!< Constant for maximum number of model
                                                couples.*/
#define      DCT_MAX_MODEL           100   /*!< Constant for maximum number of models.*/
#define      DCT_MAX_VARS            100   /*!< Constant for maximum number of model
                                                variables.*/
#define      DCT_MAX_NAME            256   /*!< Constant for maximum length for a name.*/
#define      DCT_MIN_BUFF        8388608   /*!< Constant for minimum capacity of a
                                                buffer.*/

/**********    Defining special tag constant   ************/
/*  DCT_TAG_BROKER_MOD:  Tag value that means the         */
/*                       DCT_Model structure is created   */
/*                       temporaryly by the broker (and   */
/*                       some of the internal structures  */
/*                       must be deallocated too)         */
/**********************************************************/
// #define DCT_TAG_BROKER_MOD      -1


#define DCT_COMM_NULL           MPI_COMM_NULL  /*!< Constant a NULL Communicator.*/
#define DCT_GROUP_NULL          MPI_GROUP_NULL /*!< Constant a NULL Group.*/
#define DCT_UNDEFINED_RANK      MPI_UNDEFINED  /*!< Constant a undefined Id Rank.*/

/* This definitions depend on the DCT implementation
   of each data type (see dct.h and above) */
#define DCT_BOOL                MPI_INT /*!< Boolean data type related with a
                                             communication library and is used
                                             in DCT_Comm_Datatype.*/
#define DCT_INT                 MPI_INT /*!< Integer data type related with a
                                             communication library and is used
                                             in DCT_Comm_Datatype.*/
#define DCT_TAG                 MPI_INT  /*!< Tag data type related with a
                                             communication library and is used
                                             in DCT_Comm_Datatype.*/
#define DCT_RANK                MPI_INT  /*!< Rank ID data type related with a
                                              communication library and is used
                                              in DCT_Comm_Datatype.*/   
#define DCT_CHAR                MPI_CHAR /*!< Character data type related with a
                                              communication library and is used
                                              in DCT_Comm_Datatype.*/
#define DCT_SCALAR              MPI_DOUBLE /*!< Scalar data type related with a
                                                communication library and is used
                                                in DCT_Comm_Datatype.*/

/* This definitions depend on the DCT definition
   of language data type (see DCT_Data_Types, at dct.h) */
#define DCT_COMM_FLOAT          MPI_FLOAT /*!< Float data type related with a
                                               communication library and is used
                                               in DCT_Comm_Datatype.*/
#define DCT_COMM_DOUBLE         MPI_DOUBLE /*!< Double data type related with a
                                                communication library and is used
                                                in DCT_Comm_Datatype.*/
#define DCT_COMM_LONG_DOUBLE    MPI_LONG_DOUBLE /*!< Long float data type related
                                                     with a communication library
                                                     and is used in
                                                     DCT_Comm_Datatype. */

# endif /* DCT_COMMDAT_H */


