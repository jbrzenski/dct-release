/*******************************************************************/
/*                 Distributed Coupling Toolkit (DCT)              */
/*!
   \file dct_fields.c

   \brief Contains the implementation of the functions to handle
          the data structure DCT_Field.

    This file contains the implementation of all the functions to handle
    the DCT data type structure DCT_Field; which represents
    2-dimensional model variables
    
    \todo Change the use of two function DCT_Set_Field_Freq_Consumption
     and DCT_Set_Field_Freq_Production in only one function


    \date Date of Creation: Sep 12, 2005.

    \author Dany De Cecchis: dcecchis@gmail.com
    \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.

*/
/*******************************************************************/

/* -----------------------------------------------  Include Files  */
#include <stdio.h>
#include <string.h>
#include "dct.h"
/* --------------------------------------------  Global Variables  */
#include "dctsys.h"
#include "dct_commdat.h"

/******************************************************************/
/*                     DCT_Create_Field                           */
/*                                                                */
/*!  This routine when is called by the user it initializes a
     DCT_Field variable with name should be unique globally
     the DCT system, setting the most basic definition for a
     DCT_Field structure; name, description, physical units that
     represents, and if the variable is produced or consumed. This
     DCT_Field instance represents the variable for the entire
     DCT_Model domain that belongs to.
     On the other hand, when the DCT broker process unit internally
     calls this routine, the DCT_Field instance is set to be valid
     for representing a subdomain only.

     \sa DCT_Destroy_Field

     \param[out] var  Is a DCT_Field variable to be setup.
     \param[in] name  Internal name of the variable used during
                      coupling.
     \param[in] desc  Description of the variable 120 char max.
     \param[in]    u  Units DCT_Field.
     \param[in] Prdx  Whether the variable is for Production or
                      Consumption.


     \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Create_Field( DCT_Field *var, const DCT_Name name, const DCT_String desc,
                            const DCT_Units u, const DCT_ProdCons Prdx)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  int                  len;

/* ------------------------------------  BEGIN( DCT_Create_Field ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)150);
    sprintf(ierr.Error_msg,
     "[DCT_Create_Field] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }
  /** get the name length **/
  len = strlen(name);
  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /* Check if the dct_field was not previously created */
     if (var->VarTag==DCT_TAG_UNDEF) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)100);
         sprintf(ierr.Error_msg,
          "[DCT_Create_Field] Invalid DCT_Field passed as 1st argument. The DCT_Field was already created\n");
         return(ierr);
     }
     /* Name is a mandatory field to be filled */
     /*len = strlen(name); it was moved to avoid segmentation fault when a null argument is passed*/
     if (name == (DCT_Name)NULL || (len = strlen(name) == 0)){
       ierr.Error_code  = DCT_INVALID_ARGUMENT;
       ierr.Error_msg   = (DCT_String) malloc(sizeof(char)*(size_t)150);
       sprintf(ierr.Error_msg,
          "[DCT_Create_Field] Invalid Name for Field. A name should be given\n");
       return(ierr);

     }

     if ( (u < 1) || (u > DCT_DEFINED_UNITS) ) {
         ierr.Error_code  = DCT_INVALID_UNITS;
         ierr.Error_msg   = (DCT_String) malloc((size_t)150);
         sprintf(ierr.Error_msg,
         "[DCT_Create_Field] Invalid Units for Field <%s>\n", name);
         return(ierr);
     }

       if ( (Prdx != DCT_PRODUCE) && (Prdx != DCT_CONSUME) ) {
           ierr.Error_code  = DCT_INVALID_ARGUMENT;
           ierr.Error_msg   = (DCT_String) malloc((size_t)150);
           sprintf(ierr.Error_msg,
             "[DCT_Create_Field] Invalid Parameter #6 creating Field <%s>. Valid options are DCT_PRODUCE or DCT_CONSUME.\n",
             name);
           return(ierr);
     }

  }

  var->VarName = (DCT_Name) malloc(sizeof(char)*(size_t)(len+1));
  DCTERROR( var->VarName == (DCT_Name) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  strcpy(var->VarName, name);


  if (desc != (DCT_String)NULL){
    len = strlen(desc);
    var->VarDescription = (DCT_String) malloc(sizeof(char)*(size_t)(len+1));
    DCTERROR( var->VarDescription == (DCT_String) NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    strcpy(var->VarDescription, desc);
  } else var->VarDescription = (DCT_String)NULL;

  var->VarModel         = (DCT_Model *)NULL;
  /* The variable is created unlocked */
  var->VarCommit        = DCT_FALSE;
  var->VarTag           = DCT_TAG_UNDEF;

  var->VarUnits         = u;
  /* Set Default Values for other Meta-Data fields */

  var->VarDim[0]        = 0;
  var->VarDim[1]        = 0;
  var->VarDomType[0]    = DCT_UNKNOWN_TYPE;
  var->VarDomType[1]    = DCT_UNKNOWN_TYPE;
  var->VarLabels[0]      = (DCT_Scalar *)NULL;
  var->VarLabels[1]      = (DCT_Scalar *)NULL;

  var->VarDataLoc       = DCT_LOC_UNSETS;
  var->VarStride[0]     = 0;
  var->VarStride[1]     = 0;

  var->VarDistType      = DCT_DIST_NULL;
  var->VarDistProcCounts = (DCT_Integer *)NULL;
  var->VarDistRank       = (DCT_Rank *)NULL;

  if (Prdx == DCT_PRODUCE)
       var->VarProduced       = DCT_PRODUCE;
  else
       var->VarProduced       = DCT_CONSUME;

  var->VarTimeUnits      = DCT_TIME_UNKNOWN;
  var->VarFrequency      = DCT_TIME_UNSET;
  var->VarTimeIni        = DCT_TIME_UNSET;
  var->VarLastUpdate     = DCT_TIME_UNSET;

  var->VarMask           = DCT_FALSE;
  var->VarMaskMap        = (DCT_Boolean *)NULL;

  var->VarFileName       = (DCT_String)NULL;

  var->VarNumCpl        = 0;
  var->VarCplIndx       = (DCT_Integer *)NULL;

  /*
  var->VarCommSched = (DCT_Trans_Data *)NULL;

  var->VarComm        = DCT_GROUP_NULL;
  */
  var->VarUserDataType   = DCT_DATA_TYPE_UNKNOWN;
  var->VarValues         = (DCT_VoidPointer) NULL;

  if ( DCT_Step_Check == DCT_BEGIN_STATE ) {  /* The DCT_Couple structure is registered
                                                  in the global list and counted        */
     /* The variable is added to the local variable list */
     if ( DCT_List_CHKAdd( &DCT_Reg_Vars, var, DCT_FIELD_TYPE ) != (DCT_List *)NULL ) {
       ierr.Error_code   = DCT_OBJECT_DUPLICATED;
       ierr.Error_msg    = (DCT_String) malloc(115);
       sprintf(ierr.Error_msg,
         "[DCT_Create_Field] DCT_Field name <%s> is already used.\n", var->VarName );
       return(ierr);
     }
     DCT_LocalVars++;
  }

  ierr.Error_code = DCT_SUCCESS;
  ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* --------------------------------------  END( DCT_Create_Field ) */

}

/*******************************************************************/
/*                       DCT_Set_Field_Dims                        */
/*                                                                 */
/*!  This routine can set its respective dimension, based on the
     domain discretization corresponding to the moment in which
     is called.

     \param[in,out] var  Is DCT_Field variable to be setup.
     \param[in]      d1  dim1 of the array of values for this
                         variable.
     \param[in]      d2  dim2 of the array of values for this
                         variable.

     \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Set_Field_Dims( DCT_Field *var, const DCT_Integer d1,
                              const DCT_Integer d2)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;

/* ----------------------------------  BEGIN( DCT_Set_Field_Dims ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)150);
    sprintf(ierr.Error_msg,
     "[DCT_Set_Field_Dims] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /*  Check the variable was created */
     if (var->VarTag!=DCT_TAG_UNDEF) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)150);
         sprintf(ierr.Error_msg,
          "[DCT_Set_Field_Dims] Invalid Field passed as 1st argument. Create DCT_Field first by calling DCT_Create_Field\n");
         return(ierr);
     }
     /* Check if the Variable can be modified, i.e. it is not added to a model */
     if (var->VarCommit == DCT_TRUE){
       ierr.Error_code  = DCT_INVALID_OPERATION;
       ierr.Error_msg   = (DCT_String) malloc((size_t)150);
       sprintf(ierr.Error_msg,
         "[DCT_Set_Field_Dims] Invalid Operation. The DCT_Field %s is already commited\n",
           var->VarName);
       return(ierr);
     }

     if ((d1 <= 0) || (d2 <= 0)) {
         ierr.Error_code   = DCT_INVALID_DIMENSIONS;
         ierr.Error_msg    = (DCT_String) malloc((size_t)150);
         sprintf(ierr.Error_msg,
           "[DCT_Set_Field_Dims] dimesions for Field <%s> should be positive integers [%d, %d]\n",
           var->VarName, d1, d2);
         return(ierr);
     }
  }

  var->VarDim[0]        = d1;
  var->VarDim[1]        = d2;

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* ------------------------------------  END( DCT_Set_Field_Dims ) */

}

/*******************************************************************/
/*                  DCT_Set_Field_Val_Location                     */
/*                                                                 */
/*!   This routine set the VarDataLoc, indicating how the user
      data variable is placed in the mesh, and set the proper
      strides values when the location is DCT_LOC_STRIDE1 or
      DCT_LOC_STRIDE2.

      \param[in,out] var  Is DCT_Field variable to be setup.
      \param[in]    tloc  Value that represents where the model
                          user data is located in  the model mesh.

      \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Set_Field_Val_Location( DCT_Field *var, const DCT_Val_Loc tloc)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;

/* --------------------------  BEGIN( DCT_Set_Field_Val_Location ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)150);
    sprintf(ierr.Error_msg,
     "[DCT_Set_Field_Val_Location] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /*  Check the variable was created */
     if (var->VarTag!=DCT_TAG_UNDEF) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)150);
         sprintf(ierr.Error_msg,
          "[DCT_Set_Field_Val_Location] Invalid Field passed as 1st argument. Create DCT_Field first by calling DCT_Create_Field\n");
         return(ierr);
     }
  /* Check if the Variable can be modified, i.e. it is not added to a model */
     if (var->VarCommit == DCT_TRUE){
       ierr.Error_code  = DCT_INVALID_OPERATION;
       ierr.Error_msg   = (DCT_String) malloc((size_t)150);
       sprintf(ierr.Error_msg,
         "[DCT_Set_Field_Val_Location] Invalid Operation. The DCT_Field %s is already commited\n",
               var->VarName);
       return(ierr);
     }

     if ( ( tloc < DCT_LOC_UNSETS ) || (tloc > DCT_DEFINED_VAL_LOC ) ) {
        ierr.Error_code   = DCT_INVALID_ARGUMENT;
        ierr.Error_msg    = (DCT_String) malloc((size_t)150);
        sprintf(ierr.Error_msg,
           "[DCT_Set_Field_Val_Location] Invalid parameter for Field <%s>, Unknown DCT_Val_Loc type value\n", var->VarName);
        return(ierr);

     }

  }

  switch(tloc){
  case DCT_LOC_CENTERED:
  case DCT_LOC_CORNERS:
  case DCT_LOC_STRIDEN:
    var->VarDataLoc = tloc;
    break;
  case DCT_LOC_STRIDE1:
    var->VarDataLoc = tloc;
    var->VarStride[0]          = 1;
    var->VarStride[1]          = 1;
   break;
  case DCT_LOC_STRIDE2:
    var->VarDataLoc = tloc;
    var->VarStride[0]          = 2;
    var->VarStride[1]          = 2;

  }

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* ----------------------------  END( DCT_Set_Field_Val_Location ) */

}

/*******************************************************************/
/*                     DCT_Set_Field_Strides                       */
/*                                                                 */
/*!   This routine set the VarStride[:] fields, indicating how
      many space the variable is repeated in each direction

  \param[in,out] var  Is DCT_Field variable to be setup.
  \param[in]     st1  Value that represents where the model user
                      data is located in the model mesh for the
                      x-direction.
  \param[in]     st2  Value that represents where the model user
                      data is located in the model mesh for the
                      y-direction.

  \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Set_Field_Strides( DCT_Field *var, const DCT_Integer st1, const DCT_Integer st2)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;

/* -------------------------------  BEGIN( DCT_Set_Field_Strides ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)150);
    sprintf(ierr.Error_msg,
     "[DCT_Set_Field_Strides] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /*  Check the variable was created */
     if (var->VarTag!=DCT_TAG_UNDEF) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)150);
         sprintf(ierr.Error_msg,
          "[DCT_Set_Field_Strides] Invalid Field passed as 1st argument. Create DCT_Field first by calling DCT_Create_Field\n");
         return(ierr);
     }

     /* Check if the Variable can be modified, i.e. it is not added to a model */
     if (var->VarCommit == DCT_TRUE){
       ierr.Error_code  = DCT_INVALID_OPERATION;
       ierr.Error_msg   = (DCT_String) malloc((size_t)150);
       sprintf(ierr.Error_msg,
           "[DCT_Set_Field_Strides] Invalid Operation. The DCT_Field %s is already commited\n",
           var->VarName);
       return(ierr);
     }

     if (var->VarDataLoc != DCT_LOC_STRIDEN) {
       ierr.Error_code   = DCT_INVALID_OPERATION;
       ierr.Error_msg    = (DCT_String) malloc((size_t)150);
       sprintf(ierr.Error_msg,
           "[DCT_Set_Field_Strides] Invalid operation for Field <%s>, To set stride the VarDataLoc must be DCT_LOC_STRIDEN\n",
           var->VarName);
       return(ierr);
     }
  }

  var->VarStride[0]          = st1;
  var->VarStride[1]          = st2;

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* ---------------------------------  END( DCT_Set_Field_Strides ) */

}

/*******************************************************************/
/*                      DCT_Set_Field_Time                         */
/*                                                                 */
/*!   This routine when called by the user contains the time
      units and the initial time when the variable becomes to
      be operational that means the first time to be produced
      or consumed.

    \param[in,out]    var  Is DCT_Field variable to be setup.
    \param[in] time_units  Units of time must be belongs to
                           DCT_Time_Types.
    \param[in]   time_ini  Initial time of production/consumption,
                           related with time_units (can be a
                           positive scalar).

    \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Set_Field_Time( DCT_Field *var, const DCT_Time_Types time_units,
                              const DCT_Scalar time_ini)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;

/* ----------------------------------  BEGIN( DCT_Set_Field_Time ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)150);
    sprintf(ierr.Error_msg,
     "[DCT_Set_Field_Time] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /*  Check the variable was created */
     if (var->VarTag!=DCT_TAG_UNDEF) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)150);
         sprintf(ierr.Error_msg,
          "[DCT_Set_Field_Time] Invalid Field passed as 1st argument. Create DCT_Field first by calling DCT_Create_Field\n");
         return(ierr);
     }
     /* Check if the Variable can be modified, i.e. it is not added to a model */
     if (var->VarCommit == DCT_TRUE){
       ierr.Error_code  = DCT_INVALID_OPERATION;
       ierr.Error_msg   = (DCT_String) malloc((size_t)150);
       sprintf(ierr.Error_msg,
           "[DCT_Set_Field_Time] Invalid Operation. The DCT_Field %s is already commited\n",
           var->VarName);
       return(ierr);
     }

     if ((time_units < 1) || (time_units > DCT_DEFINED_TIME_UNITS)) {
         ierr.Error_code   = DCT_INVALID_UNITS;
         ierr.Error_msg    = (DCT_String) malloc((size_t)150);
         sprintf(ierr.Error_msg,
           "[DCT_Set_Field_Time] time for Field <%s> should be a defined unit of time\n", var->VarName);
         return(ierr);
     }

     if ( (time_ini < 0.0) )
     {
       ierr.Error_code  = DCT_INVALID_ARGUMENT;
       ierr.Error_msg   = (DCT_String) malloc(80);
       sprintf(ierr.Error_msg,
       "[DCT_Set_Field_Time] Initial time for Field <%s> must be >= 0.0\n", var->VarName);
       return(ierr);
     }
  }

  var->VarTimeUnits     = (DCT_Time_Types) time_units;
  var->VarTimeIni       = (DCT_Scalar) time_ini;

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* ------------------------------------  END( DCT_Set_Field_Time ) */

}


/*******************************************************************/
/*                       DCT_Set_Field_Dist                        */
/*                                                                 */
/*!    Sets the Processor Layout associated with a DCT_Field.

    \param[in,out] var  Is DCT_Field variable to be setup.
    \param[in]    dist  Valid distribution of processors related to
                        DCT_distribution defined in the file dct.h.
    \param[in]    punt  Pointer which points out to the distribution
                        values.

    \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Set_Field_Dist( DCT_Field *var, const DCT_Distribution dist, const DCT_Integer *punt)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;

/* ----------------------------------  BEGIN( DCT_Set_Field_Dist ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  if (DCT_Step_Check != DCT_BEGIN_STATE){
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)150);
    sprintf(ierr.Error_msg,
     "[DCT_Set_Field_Dist] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  /*  Check the variable was created */
  if (var->VarTag!=DCT_TAG_UNDEF) {
      ierr.Error_code   = DCT_INVALID_ARGUMENT;
      ierr.Error_msg    = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
       "[DCT_Set_Field_Dist] Invalid Field passed as 1st argument. Create DCT_Field first by calling DCT_Create_Field\n");
      return(ierr);
  }
  /* Check if the Variable can be modified, i.e. it is not added to a model */
  if (var->VarCommit == DCT_TRUE){
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)150);
    sprintf(ierr.Error_msg,
        "[DCT_Set_Field_Dist] Invalid Operation. The DCT_Field %s is already commited\n",
        var->VarName);
    return(ierr);
  }

  if ((dist < 0) || (dist >  DCT_MAX_DISTRIBUTION_TYPES)) {
      ierr.Error_code    = DCT_INVALID_DISTRIBUTION;
      ierr.Error_msg     = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
     "[DCT_Set_Field_Dist] The distribution type entered for Field <%s> is not valid\n", var->VarName);
      return(ierr);
  }
  if (!Set_Distribution_Type( dist, punt, &var->VarDistType, &var->VarDistProcCounts )) {
     ierr.Error_code    = DCT_INVALID_DISTRIBUTION;
     ierr.Error_msg     = (DCT_String) malloc((size_t)150);
     sprintf(ierr.Error_msg,
   "[DCT_Set_Field_Dist] The distribution does not agree with the process distribution entered\n");
     return(ierr);
  }

  ierr.Error_code        = DCT_SUCCESS;
  ierr.Error_msg         = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* ------------------------------------  END( DCT_Set_Field_Dist ) */

}

/*******************************************************************/
/*                      DCT_Set_Field_Mask                         */
/*                                                                 */
/*!   Set the mask to activate or deactivate entries in the
      user data entries.

      \param[in,out] var  Is DCT_Field variable to be setup.
      \param[in]    mask  Array where the matriz is indicating if
                          the values must be used or not.

      \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Set_Field_Mask( DCT_Field *var, const DCT_Boolean *mask)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;
  DCT_Integer          *dims;

/* ----------------------------------  BEGIN( DCT_Set_Field_Mask ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)150);
    sprintf(ierr.Error_msg,
     "[DCT_Set_Field_Mask] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /*  Check the variable was created */
     if (var->VarTag!=DCT_TAG_UNDEF) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)150);
         sprintf(ierr.Error_msg,
          "[DCT_Set_Field_Mask] Invalid Field passed as 1st argument. Create DCT_Field first by calling DCT_Create_Field\n");
         return(ierr);
     }
     /* Check if the Variable can be modified, i.e. it is not added to a model */
     if (var->VarCommit == DCT_TRUE){
       ierr.Error_code  = DCT_INVALID_OPERATION;
       ierr.Error_msg   = (DCT_String) malloc((size_t)150);
       sprintf(ierr.Error_msg,
           "[DCT_Set_Field_Mask] Invalid Operation. The DCT_Field %s is already commited\n",
           var->VarName);
       return(ierr);
     }

   /* This is asumed to be cheked when the dimensions are set */
     if ( (var->VarDim[0] <=0) || ( var->VarDim[1] <=0 ) ) {
         ierr.Error_code   = DCT_INVALID_DIMENSIONS;
         ierr.Error_msg    = (DCT_String) malloc((size_t)150);
         sprintf(ierr.Error_msg,
   "[DCT_Set_Field_Mask] Field <%s> Needs to have dimensions. Call DCT_Set_Field_Dims before setting a mask\n", var->VarName);
         return(ierr);
     }
  }

  dims = var->VarDim;

  if (var->VarMask) free(var->VarMaskMap);
  var->VarMaskMap = (DCT_Boolean *)malloc(sizeof(DCT_Boolean)*(size_t)(dims[0]*dims[1]));
  DCTERROR( var->VarMaskMap == (DCT_Boolean *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  ierr = DCT_Mask_Copy( var->VarMaskMap, mask, DCT_FIELD_TYPE, dims );
  if (ierr.Error_code != DCT_SUCCESS) return(ierr);
  var->VarMask          = DCT_TRUE;

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* ------------------------------------  END( DCT_Set_Field_Mask ) */

}

/*******************************************************************/
/*                 DCT_Set_Field_Freq_Consumption                  */
/*                                                                 */
/*!     Set the frequency at which a variable consumes (receives)
        values from a producer model variable.

      \param[in,out] var  Is DCT_Field variable to be setup.
      \param[in]    freq  Frequency (real scalar) of consumption,
                          and the time units used are the same as
                          in the model (ModTimeUnits).

      \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Set_Field_Freq_Consumption( DCT_Field *var, const DCT_Scalar freq)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;

/* ----------------------  BEGIN( DCT_Set_Field_Freq_Consumption ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)150);
    sprintf(ierr.Error_msg,
     "[DCT_Set_Field_Freq_Consumption] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /*  Check the variable was created */
     if (var->VarTag!=DCT_TAG_UNDEF) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)150);
         sprintf(ierr.Error_msg,
          "[DCT_Set_Field_Freq_Consumption] Invalid Field passed as 1st argument. Create DCT_Field first by calling DCT_Create_Field\n");
         return(ierr);
     }
     /* Check if the Variable can be modified, i.e. it is not added to a model */
     if (var->VarCommit == DCT_TRUE){
       ierr.Error_code  = DCT_INVALID_OPERATION;
       ierr.Error_msg   = (DCT_String) malloc((size_t)150);
       sprintf(ierr.Error_msg,
           "[DCT_Set_Field_Freq_Consumption] Invalid Operation. The DCT_Field %s is already commited\n",
           var->VarName);
       return(ierr);
     }

     if (freq<=0) {
       ierr.Error_code     = DCT_INVALID_FREQ;
       ierr.Error_msg      = (DCT_String) malloc((size_t)150);
       sprintf(ierr.Error_msg,
       "[DCT_Set_Field_Freq_Consumption] Invalid frequency for Field <%s> it must be > 0.0\n",
        var->VarName);
       return(ierr);
     }
     if (var->VarProduced == DCT_PRODUCE) {
       ierr.Error_code     = DCT_INVALID_ARGUMENT;
       ierr.Error_msg      = (DCT_String) malloc((size_t)150);
       sprintf(ierr.Error_msg,
       "[DCT_Set_Field_Freq_Consumption] Field <%s> is declared for production\n",
            var->VarName);
       return(ierr);
     }
  }

  var->VarFrequency  = (DCT_Scalar) freq;

  ierr.Error_code    = DCT_SUCCESS;
  ierr.Error_msg     = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* ------------------------  END( DCT_Set_Field_Freq_Consumption ) */

}

/*******************************************************************/
/*                 DCT_Set_Field_Freq_Production                   */
/*                                                                 */
/*!     Set the frequency at which a variable produces (sends)
        values to a consumer model variable.

      \param[in,out] var  Is DCT_Field variable to be setup.
      \param[in]    freq  Frequency (real scalar) of production,
                          and the time units used are the same as
                          in the model (ModTimeUnits).

      \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Set_Field_Freq_Production( DCT_Field *var, DCT_Scalar freq)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;

/* -----------------------  BEGIN( DCT_Set_Field_Freq_Production ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)150);
    sprintf(ierr.Error_msg,
     "[DCT_Set_Field_Freq_Production] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /*  Check the variable was created */
     if (var->VarTag!=DCT_TAG_UNDEF) {
       ierr.Error_code   = DCT_INVALID_ARGUMENT;
       ierr.Error_msg    = (DCT_String) malloc((size_t)150);
       sprintf(ierr.Error_msg,
         "[DCT_Set_Field_Freq_Production] Invalid Field passed as 1st argument. Create DCT_Field first by calling DCT_Create_Field\n");
       return(ierr);
     }
     /* Check if the Variable can be modified, i.e. it is not added to a model */
     if (var->VarCommit == DCT_TRUE){
       ierr.Error_code  = DCT_INVALID_OPERATION;
       ierr.Error_msg   = (DCT_String) malloc((size_t)150);
       sprintf(ierr.Error_msg,
           "[DCT_Set_Field_Freq_Production] Invalid Operation. The DCT_Field %s is already commited\n",
           var->VarName);
       return(ierr);
     }

     if (freq<=0) {
       ierr.Error_code     = DCT_INVALID_FREQ;
       ierr.Error_msg      = (DCT_String) malloc((size_t)150);
       sprintf(ierr.Error_msg,
       "[DCT_Set_Field_Freq_Production] Invalid frequency for Field: %s, it must be > 0.0\n",
        var->VarName);
       return(ierr);
     }
     if (var->VarProduced == DCT_CONSUME) {
       ierr.Error_code     = DCT_INVALID_ARGUMENT;
       ierr.Error_msg      = (DCT_String) malloc((size_t)150);
       sprintf(ierr.Error_msg,
       "[DCT_Set_Field_Freq_Production] Field <%s> is declared for consumption\n",
           var->VarName);
       return(ierr);
     }
  }

  var->VarFrequency  = (DCT_Scalar) freq;

  ierr.Error_code    = DCT_SUCCESS;
  ierr.Error_msg     = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* -------------------------  END( DCT_Set_Field_Freq_Production ) */

}

/*******************************************************************/
/*                    DCT_Set_Field_Labels                         */
/*                                                                 */
/*!     Set the tick mark labels to the DCT_Fields.  It is
        assumed that the number of labels in each direction
        (numlab1 and numlab2) are correct and there must be
        exactly this number of labels for each dimension
        respectively.

       \param[in,out] var  Is DCT_Field variable to be setup.
       \param[in]   Type1  DCT_Domain_Type value to establish if
                           it is equally spaced or not equally
                           spaced in direction 1, or general
                           curvilinear.
       \param[in]   Type2  DCT_Domain_Type value to establish if
                           it is equally spaced or not equally
                           spaced in direction 2, or general
                           curvilinear.
       \param[in] numlab1  Number of labels along the x-direction,
                           which should match with dim1.
       \param[in] numlab2  Number of labels along the y-direction,
                           which should match with dim2.
       \param[in]  label1  Array of tick mark labels along the
                           x-direction, or initial and final values
                           depending of the type of domain
                           discretization.
       \param[in]  label2  Array of tick mark labels along the
                           y-direction, or initial and final values
                           depending of the type of domain
                           discretization.

       \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Set_Field_Labels( DCT_Field *var, const DCT_Domain_Type type1,
                            const DCT_Scalar *label1, const DCT_Integer numlab1,
                            const DCT_Domain_Type type2, const DCT_Scalar *label2,
                            const DCT_Integer numlab2)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;

  DCT_Scalar            *l1, *l2;
  int                   i;

/* --------------------------------  BEGIN( DCT_Set_Field_Labels ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)150);
    sprintf(ierr.Error_msg,
     "[DCT_Set_Field_Labels] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /*  Check the variable was created */
     if (var->VarTag!=DCT_TAG_UNDEF) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)150);
         sprintf(ierr.Error_msg,
          "[DCT_Set_Field_Labels] Invalid Field passed as 1st argument. Create DCT_Field first by calling DCT_Create_Field\n");
         return(ierr);
     }
     /* Check if the Variable can be modified, i.e. it is not added to a model */
     if (var->VarCommit == DCT_TRUE){
       ierr.Error_code  = DCT_INVALID_OPERATION;
       ierr.Error_msg   = (DCT_String) malloc((size_t)150);
       sprintf(ierr.Error_msg,
           "[DCT_Set_Field_Labels] Invalid Operation. The DCT_Field %s is already commited\n",
           var->VarName);
       return(ierr);
     }

     /* Check the value of the number of label,
        which should be equal to the dimensions */
     if ((numlab1 <= 0) || (numlab2 <= 0)) {
         ierr.Error_code   = DCT_INVALID_LABELING;
         ierr.Error_msg    = (DCT_String) malloc((size_t)150);
         sprintf(ierr.Error_msg,
       "[DCT_Set_Field_Labels] number of labels for Field <%s> should be positive integers [%d,%d]\n",
                 var->VarName, numlab1, numlab2);
         return(ierr);
     } else if ( ( (var->VarDim[0] != 0) && (var->VarDim[1] != 0) ) &&
                 ( (var->VarDim[0] != numlab1) || (var->VarDim[1] != numlab2) ) ) {
         ierr.Error_code   = DCT_INVALID_LABELING;
         ierr.Error_msg    = (DCT_String) malloc((size_t)150);
         sprintf(ierr.Error_msg,
       "[DCT_Set_Field_Labels] number of labels for Field <%s> should be equal to the dimensions [%d,%d]\n",
                 var->VarName, numlab1, numlab2);
         return(ierr);
     }

     if ( ( (type1 == DCT_GENERAL_CURV) || (type2 == DCT_GENERAL_CURV) ) &&
          ( (type1 != DCT_GENERAL_CURV) || (type2 != DCT_GENERAL_CURV) )    ) {
         ierr.Error_code   = DCT_INVALID_LABELING;
         ierr.Error_msg    = (DCT_String) malloc((size_t)150);
         sprintf(ierr.Error_msg,
       "[DCT_Set_Field_Labels] For General Curvilinear domain, all direction must be of type DCT_GENERAL_CURV for Field <%s>\n",
                 var->VarName );
         return(ierr);
     }
  }

  /* If the dimensions are not set, are set */
  if ( (var->VarDim[0] == 0) && (var->VarDim[1] == 0) ) {
      var->VarDim[0] = numlab1;
      var->VarDim[1] = numlab2;

  }

  var->VarDomType[0] = type1;
  var->VarDomType[1] = type2;

  if ( (type1 == DCT_GENERAL_CURV) || (type2 == DCT_GENERAL_CURV) ) {
    int prod;
    prod = numlab1*numlab2;
    var->VarLabels[0] = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)prod);
      DCTERROR( var->VarLabels[0] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    var->VarLabels[1] = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)prod);
      DCTERROR( var->VarLabels[1] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

    l1 = var->VarLabels[0];
    l2 = var->VarLabels[1];
    for ( i=0; i < prod; i++ ) {
       l1[i] = (DCT_Scalar)label1[i];
       l2[i] = (DCT_Scalar)label2[i];
    }
  } else {
    if (type1 == DCT_RECTILINEAR) {

      var->VarLabels[0] =(DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)numlab1);
      DCTERROR( var->VarLabels[0] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
      l1 = var->VarLabels[0];
      for (i=0; i < numlab1; i++ )
        l1[i] = (DCT_Scalar) label1[i];
    } else if (type1 == DCT_CARTESIAN) {
      if ( label1[1] <= label1[0] ) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)110);
         sprintf(ierr.Error_msg,
        "[DCT_Set_Field_Labels] initial value, %lf; greater than final one, %lf; Field <%s> in dimension 1\n",
             label1[0], label1[1], var->VarName);
         return(ierr);
      }
      l1 = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)3);
      DCTERROR( l1 == (DCT_Scalar *) NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
      l1[0] = (DCT_Scalar) label1[0];
      l1[1] = (DCT_Scalar) label1[1];
      l1[2] = (l1[1] - l1[0])/(DCT_Scalar)(numlab1-1);

      var->VarLabels[0] = l1;
    } else {
        ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)110);
         sprintf(ierr.Error_msg,
            "[DCT_Set_Field_Labels] Invalid value %d for DCT_Domain_Type on Field <%s> in dimension 1\n",
            type1, var->VarName);
         return(ierr);
    }

    if (type2 == DCT_RECTILINEAR) {

      var->VarLabels[1] =(DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)numlab2);
      DCTERROR( var->VarLabels[1] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
      l2 = var->VarLabels[1];
      for (i=0; i < numlab2; i++ )
        l2[i] = (DCT_Scalar) label2[i];
    } else if (type2 == DCT_CARTESIAN) {
      if ( label2[1] <= label2[0] ) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)110);
         sprintf(ierr.Error_msg,
    "[DCT_Set_Field_Labels] initial value, %lf; greater than final one, %lf; Field <%s> in dimension 2\n",
             label2[0], label2[1], var->VarName);
         return(ierr);
      }

      l2 = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)3);
      DCTERROR( l2 == (DCT_Scalar *) NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
      l2[0] = (DCT_Scalar) label2[0];
      l2[1] = (DCT_Scalar) label2[1];
      l2[2] = (l2[1] - l2[0])/(DCT_Scalar)(numlab2-1);

      var->VarLabels[1] = l2;
    } else {
        ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)110);
         sprintf(ierr.Error_msg,
            "[DCT_Set_Field_Labels] Invalid value %d for DCT_Domain_Type on Field <%s> in dimension 2\n",
            type2, var->VarName);
         return(ierr);
    }

  }

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* ----------------------------------  END( DCT_Set_Field_Labels ) */

}

/*******************************************************************/
/*                        DCT_Set_Field_Units                      */
/*                                                                 */
/*!    Set the DCT_Fields Units in accordance to DCT_Units.

       \param[in,out] var  Is DCT_Field variable to be setup.
       \param[in]       u  Units represented for the DCT_Field
                           values (i.e., CELSIUS, Km/h, etc.).
 
 \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Set_Field_Units( DCT_Field *var, const DCT_Units u)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error ierr;

/* ---------------------------------  BEGIN( DCT_Set_Field_Units ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  if (DCT_Step_Check != DCT_BEGIN_STATE){
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)150);
    sprintf(ierr.Error_msg,
     "[DCT_Set_Field_Units] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  /*  Check the variable was created */
  if (var->VarTag!=DCT_TAG_UNDEF) {
      ierr.Error_code   = DCT_INVALID_ARGUMENT;
      ierr.Error_msg    = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
       "[DCT_Set_Field_Units] Invalid Field passed as 1st argument. Create DCT_Field first by calling DCT_Create_Field\n");
      return(ierr);
  }
  /* Check if the Variable can be modified, i.e. it is not added to a model */
  if (var->VarCommit == DCT_TRUE){
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)150);
    sprintf(ierr.Error_msg,
        "[DCT_Set_Field_Units] Invalid Operation. The DCT_Field %s is already commited\n",
        var->VarName);
    return(ierr);
  }

  if ( (u < 1) || (u > DCT_DEFINED_UNITS)) {
    ierr.Error_code  = DCT_INVALID_UNITS;
    ierr.Error_msg   = (DCT_String) malloc((size_t)150);
    sprintf(ierr.Error_msg,
    "[DCT_Set_Units] Invalid unit type for var <%s>\n", var->VarName);
    return(ierr);
  }

  var->VarUnits     = u;

  ierr.Error_code   = DCT_SUCCESS;
  ierr.Error_msg    = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* -----------------------------------  END( DCT_Set_Field_Units ) */

}

/*******************************************************************/
/*                     DCT_Set_Field_Values                        */
/*                                                                 */
/*!   This routine sets the pointer to the user data

       \param[in,out] var  Is DCT_Field variable to be setup.
       \param[in]    type  Data type of the user data is defined,
                           i.e. float, double, etc.
       \param[in]    data  Pointer to the actual user data array
                           representing a 3D variable.
 
 \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Set_Field_Values( DCT_Field *var, const DCT_Data_Types type,
                                const DCT_VoidPointer data)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;

/* --------------------------------  BEGIN( DCT_Set_Field_Values ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  if (DCT_Step_Check != DCT_BEGIN_STATE){
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)150);
    sprintf(ierr.Error_msg,
     "[DCT_Set_Field_Values] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  /*  Check the variable was created */
  if (var->VarTag!=DCT_TAG_UNDEF) {
      ierr.Error_code   = DCT_INVALID_ARGUMENT;
      ierr.Error_msg    = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
       "[DCT_Set_Field_Values] Invalid Field passed as 1st argument. Create DCT_Field first by calling DCT_Create_Field\n");
      return(ierr);
  }

  /* Check if the Variable can be modified, i.e. it is not added to a model */
  if (var->VarCommit == DCT_TRUE){
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)150);
    sprintf(ierr.Error_msg,
        "[DCT_Set_Field_Values] Invalid Operation. The DCT_Field %s is already commited\n",
        var->VarName);
    return(ierr);
  }

  if ((type <= 0) || (type >  DCT_DEFINED_DATA_TYPES)) {
      ierr.Error_code    = DCT_INVALID_DATA_TYPE;
      ierr.Error_msg     = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
     "[DCT_Set_Field_Values] The user data type entered for Field <%s> is not valid\n", var->VarName);
      return(ierr);
  }

  var->VarUserDataType   = type;
  var->VarValues         = data;

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* ----------------------------------  END( DCT_Set_Field_Values ) */

}

/*******************************************************************/
/*                           DCT_Destroy_Field                     */
/*                                                                 */
/*!   Destroy (free) all the data used to represent the 3D model
      variable.

       \param[in,out] var  Is DCT_Field variable to be setup.
 
       \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Destroy_Field( DCT_Field *var)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error   ierr;

/* -----------------------------------  BEGIN( DCT_Destroy_Field ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  if (DCT_Step_Check < DCT_BROKER_STATE) {  /* All states before the broker starts to work */
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)150);
    sprintf(ierr.Error_msg,
     "[DCT_Destroy_Field] Invalid Operation. The function DCT_EndRegistration() must be called before to destroy a field.\n");
    return(ierr);
  }
  /* Check if the DCT_Field was created before to destroy it */
  if ( ( DCT_Step_Check == DCT_END_STATE ) && 
       ( (var->VarTag<DCT_TAG_UNDEF) || (var->VarTag>=DCT_MAX_VARS) ) ) {
      ierr.Error_code   = DCT_INVALID_ARGUMENT;
      ierr.Error_msg    = (DCT_String) malloc((size_t)120);
      sprintf(ierr.Error_msg,
       "[DCT_Destroy_Field] Invalid Field passed as 1st argument. DCT_Field was not created by calling DCT_Create_Field\n");
      return(ierr);
  }
  free(var->VarName);
  var->VarName = (DCT_Name)NULL;

  if (var->VarDescription != (DCT_String)NULL)
     free(var->VarDescription);
  var->VarDescription = (DCT_String)NULL;

  var->VarDim[0]       = 0;
  var->VarDim[1]       = 0;

  if (var->VarLabels[0] != (DCT_Scalar *)NULL)
     free(var->VarLabels[0]);
  var->VarLabels[0] = (DCT_Scalar *)NULL;
  if (var->VarLabels[1] != (DCT_Scalar *)NULL)
     free(var->VarLabels[1]);
  var->VarLabels[1] = (DCT_Scalar *)NULL;

  var->VarDomType[0] = DCT_UNKNOWN_TYPE;
  var->VarDomType[1] = DCT_UNKNOWN_TYPE;

  var->VarDistType      = DCT_DIST_NULL;

  if (var->VarDistProcCounts != (DCT_Integer *)NULL)
     free(var->VarDistProcCounts);
  var->VarDistProcCounts = (DCT_Integer *)NULL;

  if (var->VarDistRank != (DCT_Rank *)NULL)
     free(var->VarDistRank);
  var->VarDistRank = (DCT_Rank *)NULL;

  var->VarProduced       = DCT_PRODUCE;
  var->VarTimeUnits      = DCT_TIME_UNKNOWN;
  var->VarFrequency      = DCT_TIME_UNSET;
  var->VarTimeIni        = DCT_TIME_UNSET;
  var->VarLastUpdate     = DCT_TIME_UNSET;

  var->VarMask           = DCT_FALSE;
  if (var->VarMaskMap != (DCT_Boolean *)NULL)
     free(var->VarMaskMap);
  var->VarMaskMap = (DCT_Boolean *)NULL;

  var->VarUnits          = DCT_UNIT_UNKNOWN;

  if ( var->VarFileName != (DCT_String)NULL )
     free ( var->VarFileName );
  var->VarFileName = (DCT_String)NULL;

  var->VarNumCpl        = 0;
  if ( var->VarCplIndx != (DCT_Integer *)NULL )
     free ( var->VarCplIndx );
  var->VarCplIndx = (DCT_Integer *)NULL;

  var->VarTag            = DCT_TAG_UNDEF;
  var->VarModel          = (DCT_Model *)NULL;
  var->VarValues         = (DCT_VoidPointer) NULL;

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* -------------------------------------  END( DCT_Destroy_Field ) */

}
