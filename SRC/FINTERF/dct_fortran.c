/**************************************************************/
/*              Distributed Coupling Toolkit (DCT)            */
/*!
   \file dct_fortran.c

   \brief Contains the interface function use in fortran to
   interact with DCT

    This file contains the functions that interface the DCT
    function calls from FORTRAN

   \date Date of Creation: March 05, 2010.  

   \author Dany De Cecchis: dcecchis@uc.edu.ve
   \author Tony Drummond: LADrummond@lbl.gov

   \todo This file might need a deeper check to see if there
         something changed, to make consisten with the C
         implementation

    \copyright GNU Public License.
 */
/**************************************************************/

/* -----------------------------------------------  Include Files  */
#include <mpi.h>
#include <ctype.h>
#include <dctfortran.h>

/*************************************************************/
/*   Functions associated with DCT_Field data structures     */
/*************************************************************/

/******************************************************************/
/*                     dct_create_field_                          */
/*                                                                */
/*!   This routine initialize the DCT_Field variable in the memory   
      given in var.
      \see DCT_Create_Field
                                                                
    \param [out]     var DCT_Field pool memory to be setup in this
                         routine.
    \param [in]     name  Internal name of the variable (use during
                          coupling).
    \param [in]     desc  Description of the variable 120 char max.
    \param [in]    units  Units DCT_Field. See dct.h for predefined
                          DCT_Unit types.
    \param [in] prodcons  Whether the variable is for Production or
                           Consumption.
    \param [out]   err  Returns the error code.
    \param [out] imesg  Returns the error message.
    
    \return An integer used as bifurcation, with value 0 if something
            went wrong error, or 1 if everything was fine.

*/
/******************************************************************/
int dct_create_field_( DCT_Field *var, char *name, char *desc, int *units,
                       int *prodcons, int *err, char *imesg, int lname,
                       int ldesc, int limesg )
{

/* ---------------------------------------------  Local Variables  */
  register int         ii;
  
  DCT_Error            ierr;
  DCT_Name             fname;
  DCT_String           fdesc;

/* -----------------------------------  BEGIN( dct_create_field_ ) */

  /*   transform name from FORTRAN string to C string */
  fname = (DCT_Name) malloc(sizeof(char)*(size_t)(lname+1));
  
  for(ii=0; (ii<lname) && ( !isspace(*(name+ii)) ) ; ii++)
    *(fname+ii) = *(name+ii);
  *(fname + ii) = '\0';

  /*   transform name from FORTRAN string to C string */
  fdesc = (DCT_String) malloc(sizeof(char)*(size_t)(ldesc+1));
  for(ii=0; ii<ldesc ; ii++)
    *(fdesc + ii) = *(desc + ii);
  *(fdesc + ldesc) = '\0';
  
  /*** create the field ***/
  ierr = DCT_Create_Field( var, fname, fdesc , (DCT_Units) *units,
                           (DCT_ProdCons) *prodcons );

  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return(0);
  }
                               
  return(1);
/* -------------------------------------  END( dct_create_field_ ) */
}

/******************************************************************/
/*                    dct_set_field_dims_                         */
/*                                                                */
/* This routine sets the dimensions of the DCT_Field variable     */
/* in the memory pool given in var                                */
/*                                                                */
/*  var: DCT_Field pool memory to be setup in this routine        */
/*  dim1: is the first dimension of the variable to be decribed   */
/*  dim2: is the second dimension of the variable to be decribed  */
/*  type: indicates if the order of dimension is in FORTRAN order */
/*        DCT_F_ORDER or not DCT_C_ORDER                          */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_field_dims_( DCT_Field *var, int *dim1, int *dim2, int *type,
                         int *err, char *imesg, int limesg )
{
/* ---------------------------------------------  Local Variables  */
  register int         ii;
  DCT_Error            ierr;

/* ---------------------------------  BEGIN( dct_set_field_dims_ ) */
  
  /*** Set the DCT_Field dimensions  ***/
  if (*type==DCT_C_ORDER)
    ierr = DCT_Set_Field_Dims( var, (DCT_Integer) *dim1,
                             (DCT_Integer) *dim2 );
  else if (*type==DCT_F_ORDER)
    ierr = DCT_Set_Field_Dims( var, (DCT_Integer) *dim2,
                               (DCT_Integer) *dim1 );
  else {
     ierr.Error_code   = DCT_INVALID_ARGUMENT;
     ierr.Error_msg    = (DCT_String) malloc((size_t)150);
     sprintf(ierr.Error_msg,
         "[dct_set_field_dims] Type of dimension for Field <%s> should be DCT_F_ORDER or DCT_C_ORDER\n",
         var->VarName );
  }
  
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* -----------------------------------  END( dct_set_field_dims_ ) */
}

/******************************************************************/
/*                dct_set_field_val_location_                     */
/*                                                                */
/* This routine set the VarDataLoc field, indicating how the user */
/* data is placed in the mesh, and set the proper strides values  */
/* when the location is DCT_LOC_STRIDE1 or DCT_LOC_STRIDE2        */
/*                                                                */
/* var: DCT_Field pool memory to be setup in this routine         */
/* val_loc: sets how the data es located in the mesh, see         */
/*           DCT_Val_Loc                                          */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_field_val_location_( DCT_Field *var, int *val_loc, int *err,
                         char *imesg, int limesg )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
/* -------------------------  BEGIN( dct_set_field_val_location_ ) */
  
  /*** Set the DCT_Field variable location  ***/
  ierr = DCT_Set_Field_Val_Location( var, (DCT_Val_Loc) *val_loc);
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* ---------------------------  END( dct_set_field_val_location_ ) */
}

/******************************************************************/
/*                    dct_set_field_strides_                      */
/*                                                                */
/* This routine set the VarStride[:] fields, indicating how many  */
/* space the variable is repeated in each direction               */
/*                                                                */
/* var: DCT_Field pool memory to be setup in this routine         */
/* st1: sets the spaces along the first dimension, see DCT_Val_Loc*/
/* st2: sets the spaces along the second dimension                */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_field_strides_( DCT_Field *var, int *st1, int *st2, int *err,
                         char *imesg, int limesg )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
/* ------------------------------  BEGIN( dct_set_field_strides_ ) */

  /*** Set the DCT_Field strides  ***/
  ierr = DCT_Set_Field_Strides( var, (DCT_Integer) *st1,
                                (DCT_Integer) *st2 );
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* --------------------------------  END( dct_set_field_strides_ ) */
}

/******************************************************************/
/*                     dct_set_field_time_                        */
/*                                                                */
/* This routine when called by the user contains the time units   */
/* and the initial time when the field becomes to be operational  */
/*                                                                */
/* var: DCT_Field pool memory to be setup in this routine         */
/* time_units : units of time must be belongs to DCT_Time_Types   */
/* time_ini : Initial time of production/consumption, related with*/
/* time_units (can be a positive scalar)                          */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_field_time_( DCT_Field *var, int *time_units, double  *time_ini,
                            int *err, char *imesg, int limesg )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
/* ---------------------------------  BEGIN( dct_set_field_time_ ) */
  
  /*** Set the DCT_Field time  ***/
  ierr = DCT_Set_Field_Time( var, (DCT_Time_Types) *time_units,
                             (DCT_Scalar) *time_ini);
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* -----------------------------------  END( dct_set_field_time_ ) */
}

/******************************************************************/
/*                     dct_set_field_dist_                        */
/*                                                                */
/* Sets the Processor Layout associated with a DCT_Field          */
/*                                                                */
/* var: DCT_Field pool memory to be setup in this routine         */
/* dist: valid distribution of processors related to              */
/*       DCT_Distribution defined in dct.h                        */
/* punt: pointer which points out to the distribution values, i.e.*/
/*       how many processors are in each layout direction         */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_field_dist_( DCT_Field *var, int *dist, int  *punt,
                            int *err, char *imesg, int limesg )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
/* ---------------------------------  BEGIN( dct_set_field_dist_ ) */
  
  /*** Set the DCT_Field Distribution  ***/
  ierr = DCT_Set_Field_Dist( var, (DCT_Distribution) *dist, (DCT_Integer *) punt);
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* -----------------------------------  END( dct_set_field_dist_ ) */
}

/******************************************************************/
/*                     dct_set_field_mask_                        */
/*                                                                */
/* var: DCT_Field pool memory to be setup in this routine         */
/* mask: array with the mask values                               */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_field_mask_( DCT_Field *var, int *mask, int *err,
                         char *imesg, int limesg )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
/* ---------------------------------  BEGIN( dct_set_field_mask_ ) */
  
  /*** Set the DCT_Field Mask  ***/
  ierr = DCT_Set_Field_Mask( var, (DCT_Boolean *) mask);
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* -----------------------------------  END( dct_set_field_mask_ ) */
}

/******************************************************************/
/*                dct_set_field_freq_consumption_                 */
/*                                                                */
/*   Sets the frequency at which a variable is to be consumed     */
/*   (received) by a model                                        */
/*                                                                */
/* var: DCT_Field pool memory to be setup in this routine         */
/* freq:  frequency (double) of consumption, and the time units   */
/*         used are the same as in the model (see ModTimeUnits)   */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_field_freq_consumption_( DCT_Field *var, double *freq,
                                     int *err, char *imesg, int limesg )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
/* ---------------------  BEGIN( dct_set_field_freq_consumption_ ) */
  
  /*** Set the DCT_Field Frequency of Consumption  ***/
  ierr = DCT_Set_Field_Freq_Consumption( var, (DCT_Scalar) *freq);
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* -----------------------  END( dct_set_field_freq_consumption_ ) */
}

/******************************************************************/
/*                dct_set_field_freq_production_                  */
/*                                                                */
/*   Sets the frequency at which a variable is to be consumed     */
/*   (received) by a model                                        */
/*                                                                */
/* var: DCT_Field pool memory to be setup in this routine         */
/* freq:  frequency (double) of production, and the time units    */
/*         used are the same as in the model (see ModTimeUnits)   */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_field_freq_production_( DCT_Field *var, double *freq,
                                     int *err, char *imesg, int limesg )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
/* ----------------------  BEGIN( dct_set_field_freq_production_ ) */
  
  /*** Set the DCT_Field Frequency of Production  ***/
  ierr = DCT_Set_Field_Freq_Production( var, (DCT_Scalar) *freq);
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* ------------------------  END( dct_set_field_freq_production_ ) */
}

/******************************************************************/
/*                    DCT_Set_Field_Labels                        */
/*  Inserts labels to the DCT_Fields.  We assume that the number  */
/*  of labels in each direction (numlab1 and numlab2) is correct  */
/*  and there must be exactly numlab1 and numlab2 labels in label1*/
/*  and label2, respectecly                                       */
/*  var: DCT_Field pool memory to be setup in this routine        */
/*  Type1:      DCT_Domain_Type value to establish if it is       */
/*              equally spaced or not equally spaced in direction */
/*              1, or general curvilinear                         */
/*  Type2:      DCT_Domain_Type value to establish if it is       */
/*              equally spaced or not equally spaced in direction */
/*              2, or general curvilinear                         */
/*  numlab1:    Number of labels of DCT_Field in the first        */
/*              direction which should match with dim1            */
/*  numlab2:    Number of labels of DCT_Field in the second       */
/*              direction which should match with dim2            */
/*  label1:     Array of labels of DCT_Field in the first         */
/*              direction or initial and final values             */
/*  label2:     Array of labels of DCT_Field in the second        */
/*              direction or initial and final values             */
/*  otype:      Indicates if the order of dimension is in FORTRAN */
/*              order DCT_F_ORDER or not DCT_C_ORDER              */
/*  err:        Returns the error code                            */
/*  imesg:      Returns the error message                         */
/******************************************************************/
int dct_set_field_labels_( DCT_Field *var, int *type1, double *label1,
                           int *numlab1, int *type2, double *label2,
                           int *numlab2, int *otype, int *err, char *imesg,
                           int limesg)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
/* -------------------------------  BEGIN( dct_set_field_labels_ ) */
  
  /*** Set the DCT_Field labels  ***/
  if (*otype==DCT_C_ORDER)
     ierr = DCT_Set_Field_Labels( var, (DCT_Domain_Type) *type1,
                                  (DCT_Scalar *) label1, (DCT_Integer) *numlab1,
                                  (DCT_Domain_Type) *type2, (DCT_Scalar *) label2,
                                  (DCT_Integer) *numlab2);
  else if (*otype==DCT_F_ORDER)
     ierr = DCT_Set_Field_Labels( var, (DCT_Domain_Type) *type2,
                                  (DCT_Scalar *) label2, (DCT_Integer) *numlab2,
                                  (DCT_Domain_Type) *type1, (DCT_Scalar *) label1,
                                  (DCT_Integer) *numlab1);
  else {
     ierr.Error_code   = DCT_INVALID_ARGUMENT;
     ierr.Error_msg    = (DCT_String) malloc((size_t)150);
     sprintf(ierr.Error_msg,
         "[dct_set_field_labels] Type of order for Field <%s> should be DCT_F_ORDER or DCT_C_ORDER\n",
         var->VarName );
  }
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* ---------------------------------  END( dct_set_field_labels_ ) */
}

/******************************************************************/
/*                          DCT_Set_Units                         */
/*                                                                */
/*  Sets the DCT_Fields Units in accordance to DCT_Units          */
/* var: DCT_Field pool memory to be setup in this routine         */
/*   u:  Units of the DCT_Field (i.e., CELSIUS, Km/h, etc)        */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_field_units_( DCT_Field *var, int *u, int *err, char *imesg,
                          int limesg)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
/* --------------------------------  BEGIN( dct_set_field_units_ ) */
  
  /*** Set the DCT_Field Unit  ***/
  ierr = DCT_Set_Field_Units( var, (DCT_Units) *u);  
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* ----------------------------------  END( dct_set_field_units_ ) */
}

/******************************************************************/
/*                    dct_set_field_values_                       */
/*                                                                */
/*  This routine sets the pointer to the user data                */
/*                                                                */
/* var: DCT_Field pool memory to be setup in this routine         */
/* type: Type the data type in the array (see DCT_Data_Types)     */
/* data: Array the data given by the user                         */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_field_values_( DCT_Field *var, int *type, void *data,
                           int *err, char *imesg, int limesg)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
/* -------------------------------  BEGIN( dct_set_field_values_ ) */
  
  /*** Set the DCT_Field Unit  ***/
  ierr = DCT_Set_Field_Values( var, (DCT_Data_Types) *type,
                               (DCT_VoidPointer) data );
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);
  
/* ---------------------------------  END( dct_set_field_values_ ) */
}


/******************************************************************/
/*                        dct_destroy_field_                      */
/*                                                                */
/*   This routine calls the DCT_Destroy_Field function, to free   */
/*   all the data structures created and attached to the          */
/*   DCT_Field data structure                                    */
/*                                                                */
/* var: DCT_Field pool memory to be destroyed in this routine     */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_destroy_field_( DCT_Field *var, int *err, char *imesg, int limesg )
{
  
  /* ---------------------------------------------  Local Variables  */
  
  DCT_Error            ierr;
  register int         ii;
  
/* ----------------------------------  BEGIN( dct_destroy_field_ ) */
  
  /*** destroy the field ***/
  ierr = DCT_Destroy_Field( var );
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return(1);
/* ------------------------------------  END( dct_destroy_field_ ) */
}

/**************************************************************************/
/**************************************************************************/
/*                                                                        */
/*            D C T   3 D   V A R   F U N C T I O N S                     */
/*                                                                        */
/**************************************************************************/
/**************************************************************************/

/******************************************************************/
/*                     dct_create_3d_var_                         */
/*                                                                */
/* This routine initialize the DCT_3d_Var variable and return the */
/* descriptor to the FORTRAN environment (see DCT_Create_3d_Var)  */
/*                                                                */
/* var: DCT_3d_Var pool memory variable to be setup               */
/* name: Internal name of the variable (use during coupling)      */
/* desc: Description of the variable 120 char max                 */
/*    u: Units DCT_3d_Var. See dct.h for predefined DCT_Unit      */
/*       types                                                    */
/* prdx: Whether the variable is for Production or Consumption    */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_create_3d_var_( DCT_3d_Var *var, char *name, char *desc, int *units,
                       int *prodcons, int *err, char *imesg, int lname,
                       int ldesc, int limesg )
{

/* ---------------------------------------------  Local Variables  */
  register int         ii;
  
  DCT_Error            ierr;
  DCT_Name             v3dname;
  DCT_String           v3ddesc;

/* ----------------------------------  BEGIN( dct_create_3d_var_ ) */
  
  /*   transform name from FORTRAN string to C string */
  v3dname = (DCT_Name) malloc(sizeof(char)*(size_t)(lname+1));
  
  for(ii=0; (ii<lname) && ( !isspace(*(name+ii)) ) ; ii++)
    *(v3dname+ii) = *(name+ii);
  *(v3dname + ii) = '\0';

  /*   transform name from FORTRAN string to C string */
  v3ddesc = (DCT_String) malloc(sizeof(char)*(size_t)(ldesc+1));
  for(ii=0; ii<ldesc ; ii++)
    *(v3ddesc + ii) = *(desc + ii);
  *(v3ddesc + ldesc) = '\0';
  
  /*** create the 3d var ***/
  ierr = DCT_Create_3d_Var( var, v3dname, v3ddesc, 
                            (DCT_Units) *units, (DCT_ProdCons) *prodcons );

  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return(0);
  }
                               
  return(1);
/* ------------------------------------  END( dct_create_3d_var_ ) */
}

/******************************************************************/
/*                     DCT_Set_3d_Var_Dims                        */
/*                                                                */
/* This routine sets the dimensions of the DCT_3d_Var variables   */
/* under the given descriptor                                     */
/*                                                                */
/* var: DCT_3d_Var pool memory variable to be setup               */
/*  d1: dim1 of the array of values for this variable             */
/*  d2: dim2 of the array of values for this variable             */
/*  d3: dim3 of the array of values for this variable             */
/*  type: indicates if the order of dimension is in FORTRAN order */
/*        DCT_F_ORDER or not DCT_C_ORDER                          */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_3d_var_dims_( DCT_3d_Var *var, int *dim1, int *dim2, int *dim3,
                          int *type, int *err, char *imesg, int limesg )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
/* --------------------------------  BEGIN( dct_set_3d_var_dims_ ) */
  
  /*** Set the DCT_3d_Var dimensions  ***/
  if (*type==DCT_C_ORDER)
    ierr = DCT_Set_3d_Var_Dims( var, (DCT_Integer) *dim1,
                              (DCT_Integer) *dim2, (DCT_Integer) *dim3 );
  else if (*type==DCT_F_ORDER)
    ierr = DCT_Set_3d_Var_Dims( var, (DCT_Integer) *dim3,
                                  (DCT_Integer) *dim2, (DCT_Integer) *dim1 );
  else {
     ierr.Error_code   = DCT_INVALID_ARGUMENT;
     ierr.Error_msg    = (DCT_String) malloc((size_t)150);
     sprintf(ierr.Error_msg,
         "[dct_set_3d_var_dims] Type of dimension for 3D_Var <%s> should be DCT_F_ORDER or DCT_C_ORDER\n",
         var->VarName );
  }
  
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* ----------------------------------  END( dct_set_3d_var_dims_ ) */
}

/******************************************************************/
/*                 dct_set_3d_var_val_location_                   */
/*                                                                */
/* This routine set the VarDataLoc field, indicating how the user */
/* is placed in the meash, and set the proper strides values when */
/* the location is DCT_LOC_STRIDE1 or DCT_LOC_STRIDE2             */
/*                                                                */
/* var: DCT_3d_Var pool memory variable to be setup               */
/* val_loc: sets how the data es located in the mesh, see         */
/*           DCT_Val_Loc                                          */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/*******************************************************************/
int dct_set_3d_var_val_location_( DCT_3d_Var *var, int *val_loc, int *err,
                          char *imesg, int limesg )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
/* ------------------------  BEGIN( dct_set_3d_var_val_location_ ) */
  
  /*** Set the DCT_3d_Var values location ***/
  ierr = DCT_Set_3d_Var_Val_Location( var, (DCT_Val_Loc) *val_loc);
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* --------------------------  END( dct_set_3d_var_val_location_ ) */
}

/******************************************************************/
/*                   dct_set_3d_var_strides_                      */
/*                                                                */
/* This routine set the VarStride[:] fields, indicating how many  */
/* space the variable is repeated in each direction               */
/*                                                                */
/* var: DCT_3d_Var pool memory variable to be setup               */
/* st1: sets the spaces along the first dimension, see DCT_Val_Loc*/
/* st2: sets the spaces along the second dimension                */
/* st3: sets the spaces along the third dimension                */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_3d_var_strides_( DCT_3d_Var *var, int *st1, int *st2, int *st3,
                             int *err, char *imesg, int limesg )
{
/* ---------------------------------------------  Local Variables  */
  register int         ii;
  DCT_Error            ierr;

/* -----------------------------  BEGIN( dct_set_3d_var_strides_ ) */
  
  /*** Set the DCT_3d_Var strides ***/
  ierr = DCT_Set_3d_Var_Strides(  var, (DCT_Integer) *st1,
                                  (DCT_Integer) *st2, (DCT_Integer) *st3);
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* -------------------------------  END( dct_set_3d_var_strides_ ) */
}

/******************************************************************/
/*                    dct_set_3d_var_time_                        */
/*                                                                */
/* This routine when called by the user contains the time units   */
/*and the initial time when the variable becomes to be operational*/
/*that means the first time to be produced or consumed            */
/*                                                                */
/* var: DCT_3d_Var pool memory variable to be setup               */
/* time_units : units of time must be belongs to DCT_Time_Types   */
/* time_ini : Initial time of production/consumption, related with*/
/*            time_units (can be a positive scalar)               */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_3d_var_time_( DCT_3d_Var *var, int *time_units, double *time_ini,
                             int *err, char *imesg, int limesg )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
/* --------------------------------  BEGIN( dct_set_3d_var_time_ ) */
  
  /*** Set the DCT_3d_Var time ***/
  ierr = DCT_Set_3d_Var_Time( var, (DCT_Time_Types) *time_units,
                              (DCT_Scalar) *time_ini);
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* ----------------------------------  END( dct_set_3d_var_time_ ) */
}

/******************************************************************/
/*                      dct_set_3d_var_dist_                      */
/*                                                                */
/* Sets the Processor Layout associated with a DCT_3d_Var         */
/*                                                                */
/* var: DCT_3d_Var pool memory variable to be setup               */
/* dist: valid distribution of processors related to              */
/*       DCT_distribution defined in the file dct.h               */
/* punt: pointer which points out to the distribution values      */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_3d_var_dist_( DCT_3d_Var *var, int *dist, int *punt,
                             int *err, char *imesg, int limesg )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
/* --------------------------------  BEGIN( dct_set_3d_var_dist_ ) */
  
  /*** Set the DCT_3d_Var Processor distribution ***/
  ierr = DCT_Set_3d_Var_Dist( var, (DCT_Distribution) *dist,
                              (DCT_Integer *) punt);
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* ----------------------------------  END( dct_set_3d_var_dist_ ) */
}

/******************************************************************/
/*                     dct_set_3d_var_mask_                       */
/*                                                                */
/* var: DCT_3d_Var pool memory variable to be setup               */
/* mask: array with the mask values                               */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_3d_var_mask_( DCT_3d_Var *var, int *mask, int *err,
                          char *imesg, int limesg )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
/* --------------------------------  BEGIN( dct_set_3d_var_mask_ ) */
  
  /*** Set the DCT_3d_Var Mask ***/
  ierr = DCT_Set_3d_Var_Mask( var, (DCT_Boolean *) mask);
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* ----------------------------------  END( dct_set_3d_var_mask_ ) */
}

/******************************************************************/
/*             dct_set_3d_var_freq_consumption_                   */
/*                                                                */
/*   Sets the frequency at which a variable is to be consumed     */
/*   (received) by a model                                        */
/*                                                                */
/* var: DCT_3d_Var pool memory variable to be setup               */
/*  freq:  frequency (real scalar) of consumption, and the time   */
/*         units used are the same as in the model (ModTimeUnits) */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_3d_var_freq_consumption_( DCT_3d_Var *var, double *freq,
                                      int *err, char *imesg, int limesg )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
/* --------------------  BEGIN( dct_set_3d_var_freq_consumption_ ) */
  
  /*** Set the DCT_3d_Var Frequency consumption ***/
  ierr = DCT_Set_3d_Var_Freq_Consumption( var, (DCT_Scalar) *freq );
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* ----------------------  END( dct_set_3d_var_freq_consumption_ ) */
}

/******************************************************************/
/*               dct_set_3d_var_freq_production_                  */
/*                                                                */
/*   Sets the frequency at which a variable is to be produced     */
/*   (sent) by a model                                            */
/*                                                                */
/* var: DCT_3d_Var pool memory variable to be setup               */
/*  freq:  frequency (real scalar) of production and the time     */
/*         units used are the same as in the model (ModTimeUnits) */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_3d_var_freq_production_( DCT_3d_Var *var, double *freq,
                                      int *err, char *imesg, int limesg )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
/* --------------------  BEGIN( dct_set_3d_var_freq_consumption_ ) */
  
  /*** Set the DCT_3d_Var Frequency production ***/
  ierr = DCT_Set_3d_Var_Freq_Production( var, (DCT_Scalar) *freq );
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* ----------------------  END( dct_set_3d_var_freq_consumption_ ) */
}

/******************************************************************/
/*                    DCT_Set_3d_Var_Labels                      */
/*                                                                */
/*  Inserts labels to the DCT_3d_Vars.  We assume that the number */
/*  of labels in each direction (numlab1, numlab2 and numlab3) is */
/*  correct and there must be exactly this number of labels for   */
/*  each dimension respectecly                                    */
/*                                                                */
/*  var: DCT_3d_Var pool memory variable to be setup              */
/*  Type1:      DCT_Domain_Type value to establish if it is       */
/*              equally spaced or not equally spaced in direction */
/*              1, or general curvilinear                         */
/*  Type2:      DCT_Domain_Type value to establish if it is       */
/*              equally spaced or not equally spaced in direction */
/*              2, or general curvilinear                         */
/*  Type3:      DCT_Domain_Type value to establish if it is       */
/*              equally spaced or not equally spaced in direction */
/*              3, or general curvilinear                         */
/*  numlab1:    Number of labels of DCT_Field in the first        */
/*              direction which should match with dim1            */
/*  numlab2:    Number of labels of DCT_Field in the second       */
/*              direction which should match with dim2            */
/*  numlab3:    Number of labels of DCT_Field in the third        */
/*              direction which should match with dim3            */
/*  label1:  Labels of DCT_Field in the first direction or        */
/*           initial and final values                             */
/*  label2:  Labels of DCT_Field in the second direction or       */
/*           initial and final values                             */
/*  label3:  Labels of DCT_Field in the third direction or        */
/*           initial and final values                             */
/*  otype:   Indicates if the order of dimension is in FORTRAN    */
/*           order DCT_F_ORDER or not DCT_C_ORDER                 */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_3d_var_labels_( DCT_3d_Var *var, int *type1, double *label1,
                            int *numlab1, int *type2, double *label2,
                            int *numlab2, int *type3, double *label3,
                            int *numlab3, int *otype, int *err, char *imesg,
                            int limesg )
{
/* ---------------------------------------------  Local Variables  */
   DCT_Error            ierr;
   
   register int         ii;
  
/* ------------------------------  BEGIN( dct_set_3d_var_labels_ ) */
  
   /*** Set the DCT_3d_Var Labels ***/
   if (*otype==DCT_C_ORDER)
     ierr = DCT_Set_3d_Var_Labels( var, (DCT_Domain_Type) *type1,
                             (DCT_Scalar *) label1, (DCT_Integer) *numlab1,
                             (DCT_Domain_Type) *type2, (DCT_Scalar *) label2,
                             (DCT_Integer) *numlab2, (DCT_Domain_Type) *type3,
                             (DCT_Scalar *) label3, (DCT_Integer) *numlab3);
   else if (*otype==DCT_F_ORDER)
     ierr = DCT_Set_3d_Var_Labels( var, (DCT_Domain_Type) *type3,
                             (DCT_Scalar *) label3, (DCT_Integer) *numlab3,
                             (DCT_Domain_Type) *type2, (DCT_Scalar *) label2,
                             (DCT_Integer) *numlab2, (DCT_Domain_Type) *type1,
                             (DCT_Scalar *) label1, (DCT_Integer) *numlab1 );
   else {
     ierr.Error_code   = DCT_INVALID_ARGUMENT;
     ierr.Error_msg    = (DCT_String) malloc((size_t)150);
     sprintf(ierr.Error_msg,
         "[dct_set_3d_var_labels] Type of order for Field <%s> should be DCT_F_ORDER or DCT_C_ORDER\n",
         var->VarName );
   }
   
   *err = (int) ierr.Error_code;
   strcpy(imesg, ierr.Error_msg);
   
   /*** If something does not go well  ***/
   if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
   }
   
   return (1);

/* --------------------------------  END( dct_set_3d_var_labels_ ) */
}

/******************************************************************/
/*                  dct_set_3d_var_units_                         */
/*                                                                */
/*  Sets the DCT_3d_Vars Units in accordance to DCT_Units         */
/*                                                                */
/* var: DCT_3d_Var pool memory variable to be setup               */
/*      u: Units of the DCT_3d_Var (i.e., CELSIUS, Km/h, etc)     */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_3d_var_units_( DCT_3d_Var *var, int *u, int *err, char *imesg,
                           int limesg )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
/* -------------------------------  BEGIN( dct_set_3d_var_units_ ) */
  
  /*** Set the DCT_3d_Var Unit ***/
  ierr = DCT_Set_3d_Var_Units( var, (DCT_Units) *u);
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* ---------------------------------  END( dct_set_3d_var_units_ ) */
}

/******************************************************************/
/*                  dct_set_3d_var_values_                        */
/*                                                                */
/* This routine sets the pointer to the user data                 */
/*                                                                */
/* var: DCT_3d_Var pool memory variable to be setup               */
/* type: Type the data type in the array (see DCT_Data_Types)     */
/* data: Array the data given by the user                         */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_3d_var_values_( DCT_3d_Var *var, int *type, void *data,
                            int *err, char *imesg, int limesg )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
/* ------------------------------  BEGIN( dct_set_3d_var_values_ ) */
  
  /*** Set the DCT_3d_Var Values ***/
  ierr = DCT_Set_3d_Var_Values( var, (DCT_Data_Types) *type,
                                (DCT_VoidPointer) data);
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);

  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* --------------------------------  END( dct_set_3d_var_values_ ) */
}

/******************************************************************/
/*                       dct_destroy_3d_var_                      */
/*                                                                */
/*   This routine calls the DCT_Destroy_3d_Var function, to free  */
/*   all the data structures created and attached to the          */
/*   DCT_3d_Var data structure                                   */
/*                                                                */
/* var: DCT_3d_Var pool memory variable to be destroyed           */
/*         destoyed                                               */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_destroy_3d_var_( DCT_3d_Var *var, int *err, char *imesg, int limesg )
{
  
/* ---------------------------------------------  Local Variables  */
  
  DCT_Error            ierr;
  register int         ii;
  
/* ---------------------------------  BEGIN( dct_destroy_3d_var_ ) */
  
  /*** destroy the 3D Var ***/
  ierr = DCT_Destroy_3d_Var( var );
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return(1);
/* -----------------------------------  END( dct_destroy_3d_var_ ) */
}


/**************************************************************************/
/**************************************************************************/
/*                                                                        */
/*             D C T   M O D E L    F U N C T I O N S                     */
/*                                                                        */
/**************************************************************************/
/**************************************************************************/

/******************************************************************/
/*                     dct_create_model_                          */
/*                                                                */
/* This routine when called by the user it initializes a global   */
/* model.                                                         */
/*                                                                */
/* mod:  DCT_Model pool memory to be setup in this routine        */
/* name: Internal name of the model (use during coupling)         */
/* desc: Description of the model 120 char max                    */
/* nprocs: Number of procesors used by the model                  */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_create_model_( DCT_Model *mod, char *name, char *desc, int *nprocs,
                         int *err, char *imesg, int lname, int ldesc,
                         int limesg )
{

/* ---------------------------------------------  Local Variables  */
  register int         ii;
  
  DCT_Error            ierr;
  DCT_Name             mname;
  DCT_String           mdesc;

/* -----------------------------------  BEGIN( dct_create_model_ ) */
  
  /*   transform name from FORTRAN string to C string */
  mname = (DCT_Name) malloc(sizeof(char)*(size_t)(lname+1));
  for(ii=0; (ii<lname) && ( !isspace(*(name+ii)) ) ; ii++)
    *(mname+ii) = *(name+ii);
  *(mname + ii) = '\0';
//  printf("[dct_create_model]: Model Name %s with %d chars\n" ,mname, ii);

  /*   transform name from FORTRAN string to C string */
  mdesc = (DCT_String) malloc(sizeof(char)*(size_t)(ldesc+1));
  for(ii=0; ii<ldesc ; ii++)
    *(mdesc + ii) = *(desc + ii);
  *(mdesc + ldesc) = '\0';
  
//   printf("[dct_create_model]: Model Name %s with %d procs\n" ,mname, *nprocs);
  /*** create the Model ***/
  ierr = DCT_Create_Model( mod, mname, mdesc, (DCT_Integer) *nprocs );
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return(0);
  }
                               
  return(1);
/* -------------------------------------  END( dct_create_model_ ) */
}

/******************************************************************/
/*                         dct_set_model_time_                    */
/*                                                                */
/* Sets the DCT_Models Time Units in accordance to DCT_Time_Types */
/*                                                                */
/*        mod:  DCT_Model pool memory to be setup in this routine */
/*   time_ini:  initial time of the model                         */
/*   time_int:  integration time of the model                     */
/* time_units:  Time Units of the DCT_Model (i.e., weeks, hours,  */
/*              etc.)                                             */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_model_time_( DCT_Model *mod, double *time_ini, double *time_int,
                        int *time_units, int *err, char *imesg,
                        int limesg )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
/* ---------------------------------  BEGIN( dct_set_model_time_ ) */

  
  /*** Set the DCT_Model Time ***/
  ierr = DCT_Set_Model_Time( mod, (DCT_Scalar) *time_ini,
                           (DCT_Scalar) *time_int, (DCT_Time_Types) *time_units );
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* -----------------------------------  END( dct_set_model_time_ ) */
}

/******************************************************************/
/*                  dct_set_model_rspaced_dom_                    */
/*                                                                */
/* This routine fixes the scope of the model domain discretized   */
/* equally spaced in all of its dimensions                        */
/*                                                                */
/*  mod:  DCT_Model pool memory to be setup in this routine       */
/*  dim: set the dimension of the domain, must be 2,3 or 4.       */
/*  xo: Array with the initial values in each dimension           */
/*  xf: Array with the final values in each dimension             */
/*  npts: Array with the number of points in each dimension       */
/*  otype:   Indicates if the order of dimension is in FORTRAN    */
/*           order DCT_F_ORDER or not DCT_C_ORDER                 */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_model_rspaced_dom_( DCT_Model *mod, int *dim, double *xo,
                                double *xf, int *npts, int *otype, int *err,
                                char *imesg, int limesg )
{
/* ---------------------------------------------  Local Variables  */
   DCT_Error            ierr;
   register int         ii;
  
/* --------------------------  BEGIN( dct_set_model_rspaced_dom_ ) */
  
   /*** Set the DCT_Model Equally Spaced ***/
   if (*otype==DCT_C_ORDER)
      ierr = DCT_Set_Model_RSpaced_Dom( mod, (DCT_Integer) *dim,
                                        (DCT_Scalar *) xo, (DCT_Scalar *) xf,
                                        (DCT_Integer *) npts );
   else if (*otype==DCT_F_ORDER) {
      int lpts[MAX_DOMAIN_DIM];
      DCT_Scalar  lxo[MAX_DOMAIN_DIM], lxf[MAX_DOMAIN_DIM];
      for ( ii=0; ii < *dim; ii++ ) {
         lpts[*dim - ii - 1] = npts[ii];
         lxo[*dim - ii - 1] = xo[ii];
         lxf[*dim - ii - 1] = xf[ii];
      }
      ierr = DCT_Set_Model_RSpaced_Dom( mod, (DCT_Integer) *dim,
                                        (DCT_Scalar *) lxo, (DCT_Scalar *) lxf,
                                        (DCT_Integer *) lpts );
   }
   else {
     ierr.Error_code   = DCT_INVALID_ARGUMENT;
     ierr.Error_msg    = (DCT_String) malloc((size_t)150);
     sprintf(ierr.Error_msg,
         "[dct_set_model_dom] Type of order for model <%s> should be DCT_F_ORDER or DCT_C_ORDER\n",
         mod->ModName );
   }

   *err = (int) ierr.Error_code;
   strcpy(imesg, ierr.Error_msg);
   
   /*** If something does not go well  ***/
   if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
   }
   
   return (1);
   
/* ----------------------------  END( dct_set_model_rspaced_dom_ ) */
}

/******************************************************************/
/*                       dct_set_model_dom_                       */
/*                                                                */
/* This routine when called by the user fix the scope of the      */
/* model equally spaced                                           */
/*                                                                */
/*  mod:  DCT_Model pool memory to be setup in this routine       */
/*  dim:  Set the dimension of the domain, must be 2,3 or 4.      */
/*  dir:  Direction in one of the dimension to set the domain     */
/*  type: DCT_Domain_Type value to indicate if it is structured,  */
/*        unstructured or general curvilinear.                    */
/*  val:  Values of the labels if type is DCT_TRUE (not equally   */
/*        spaced), the initial and  final value if type is        */
/*        DCT_FALSE (equally spaced).                             */
/*  npts: Array with the number of points along dir               */
/*  otype:   Indicates if the order of dimension is in FORTRAN    */
/*           order DCT_F_ORDER or not DCT_C_ORDER                 */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_model_dom_( DCT_Model *mod, int *dim, int *dir, int *type,
                        double *val, int *npts, int *otype, int *err,
                        char *imesg, int limesg )
{
/* ---------------------------------------------  Local Variables  */
   DCT_Error            ierr;
   register int         ii;
  
/* ----------------------------------  BEGIN( dct_set_model_dom_ ) */
  
   /*** Set the DCT_Model Domain ***/
   if (*otype==DCT_C_ORDER)
     ierr = DCT_Set_Model_Dom( mod, (DCT_Integer) *dim,
                              (DCT_Integer) *dir, (DCT_Domain_Type) *type,
                              (DCT_Scalar *) val, (DCT_Integer) *npts );
   else if (*otype==DCT_F_ORDER) {
     DCT_Integer     ldir = *dim - *dir + 1;
     ierr = DCT_Set_Model_Dom( mod, (DCT_Integer) *dim,
                              (DCT_Integer) ldir, (DCT_Domain_Type) *type,
                              (DCT_Scalar *) val, (DCT_Integer) *npts );
   }
   else {
     ierr.Error_code   = DCT_INVALID_ARGUMENT;
     ierr.Error_msg    = (DCT_String) malloc((size_t)150);
     sprintf(ierr.Error_msg,
         "[dct_set_model_dom] Type of order for model <%s> should be DCT_F_ORDER or DCT_C_ORDER\n",
         mod->ModName );
   }
   
   *err = (int) ierr.Error_code;
   strcpy(imesg, ierr.Error_msg);
   
   /*** If something does not go well  ***/
   if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
   }
   
   return (1);

/* ------------------------------------  END( dct_set_model_dom_ ) */
}

/******************************************************************/
/*                    dct_set_model_gencur_dom_                   */
/*                                                                */
/* This routine when called by the user to set the model domain   */
/* when is a general curvilinear and the actual coordinate of all */
/* the points are needed. Attention: To safe memory the structure */
/* point to the used array of points. So, changes made in the user*/
/* arrays affect the Model representtation.                       */
/*                                                                */
/*  mod:  DCT_Model pool memory to be setup in this routine       */
/*  dir:  Direction in one of the dimension to set the domain     */
/*  dim:  Set the dimension of the domain, must be 2 or 3.        */
/*  x, y, z: arrays of values of the corresponding coordinates    */
/*           for point. The length is the product of npts values  */
/*           in the dim positions                                 */
/*  npts: Array length dim with the number of points along each   */
/*        direction                                               */
/*  otype:   Indicates if the order of dimension is in FORTRAN    */
/*           order DCT_F_ORDER or not DCT_C_ORDER                 */
/*  err: Returns the error code                                   */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_model_gencur_dom_( DCT_Model *mod, int *dim, double *x,
                        double *y, double *z, int *npts, int *otype,
                        int *err, char *imesg, int limesg )
{
/* ---------------------------------------------  Local Variables  */
   DCT_Error            ierr;
   register int         ii;
  
/* ---------------------------  BEGIN( dct_set_model_gencur_dom_ ) */
  
   /*** Set the DCT_Model Domain ***/
   if (*otype==DCT_C_ORDER)
      ierr = DCT_Set_Model_GenCur_Dom( mod, (DCT_Integer) *dim, (DCT_Scalar *) x,
                                      (DCT_Scalar *) y, (DCT_Scalar *) z,
                                      (DCT_Integer *) npts );
   else if (*otype==DCT_F_ORDER) {
       int      lpts[3];
       lpts[2] = npts[0];
       lpts[1] = npts[1];
       lpts[0] = npts[2];
       ierr = DCT_Set_Model_GenCur_Dom( mod, (DCT_Integer) *dim, (DCT_Scalar *) z,
                                      (DCT_Scalar *) y, (DCT_Scalar *) x,
                                      (DCT_Integer *) lpts );
   }   
   else {
      ierr.Error_code   = DCT_INVALID_ARGUMENT;
      ierr.Error_msg    = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
         "[dct_set_model_gencur_dom] Type of order for model <%s> should be DCT_F_ORDER or DCT_C_ORDER\n",
         mod->ModName );
   }
   
   *err = (int) ierr.Error_code;
   strcpy(imesg, ierr.Error_msg);
   
   /*** If something does not go well  ***/
   if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
   }
   
   return (1);

/* -----------------------------  END( dct_set_model_gencur_dom_ ) */
}

/******************************************************************/
/*                     dct_set_model_parlayout_                   */
/*                                                                */
/* Sets the Processor Layout associated with a DCT_Model and      */
/* assign the processors ranks                                    */
/*                                                                */
/*  mod:  DCT_Model pool memory to be setup in this routine       */
/* dist: valid distribution of processors related to              */
/*       DCT_Distribution defined in the file dct.h               */
/* laydims: Arrays with the dimesions in each direction of the    */
/*          layout                                                */
/* ranks: pointer to array with the processors ranks.    Get rid of this parameter */
/* otype:  Indicates if the order of dimension is in FORTRAN      */
/*         order DCT_F_ORDER or not DCT_C_ORDER                   */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_model_parlayout_( DCT_Model *mod, int *dist, int *laydims,
                              int *otype, int *err, char *imesg,
                              int limesg )
/* int dct_set_model_parlayout_( DCT_Model *mod, int *dist, int *laydims,
                              int *ranks, int *otype, int *err, char *imesg,
                              int limesg ) */
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  int                    ii;
  
/* ----------------------------  BEGIN( dct_set_model_parlayout_ ) */
  
   /*** Set the DCT_Model Parallel Layout ***/
   if (*otype==DCT_C_ORDER)
      ierr = DCT_Set_Model_ParLayout( mod, (DCT_Distribution) *dist,
                                     (DCT_Integer *) laydims); /*, (DCT_Rank *) ranks );*/
   else if (*otype==DCT_F_ORDER) {
      DCT_Integer  dimtotal;
      DCT_Integer *fldims = (DCT_Integer *)NULL;

      switch(*dist)
      {
         case DCT_DIST_SEQUENTIAL:
            dimtotal = 1;
            fldims = (DCT_Integer *) laydims;
            break;
         case DCT_DIST_RECTANGULAR:
            dimtotal = (DCT_Integer) *(laydims) * *(laydims+1);
            fldims = (DCT_Integer *) malloc( sizeof(DCT_Integer) * (size_t)2 );
            DCTERROR( fldims == (DCT_Integer *)NULL, "Memory Allocation Failed",
                                              *err  =  DCT_FAILED_MALLOC; return(1) );
            fldims[0] = laydims[1];
            fldims[1] = laydims[0];
            break;
         case DCT_DIST_3D_RECTANGULAR:
            dimtotal = (DCT_Integer) *(laydims) * *(laydims+1) * *(laydims+2);
            fldims = (DCT_Integer *) malloc( sizeof(DCT_Integer) * (size_t)3 );
            DCTERROR( fldims == (DCT_Integer *)NULL, "Memory Allocation Failed",
                                              *err  =  DCT_FAILED_MALLOC; return(1) );
            fldims[0] = laydims[2];
            fldims[1] = laydims[1];
            fldims[2] = laydims[0];
            break;
         default:
            ierr.Error_code   = DCT_INVALID_ARGUMENT;
            ierr.Error_msg    = (DCT_String) malloc((size_t)150);
            sprintf(ierr.Error_msg,
               "[dct_set_model_parlayout] Invalid Type of DCT_Distribution for model <%s>.\n",
               mod->ModName );
      }

      ierr = DCT_Set_Model_ParLayout( mod, (DCT_Distribution) *dist,
                                      fldims); /*, (DCT_Rank *) ranks );*/
   }   
   else {
      ierr.Error_code   = DCT_INVALID_ARGUMENT;
      ierr.Error_msg    = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
         "[dct_set_model_parlayout] Type of order for model <%s> should be DCT_F_ORDER or DCT_C_ORDER\n",
         mod->ModName );
   }
   *err = (int) ierr.Error_code;
   strcpy(imesg, ierr.Error_msg);
   
   /*** If something does not go well  ***/
   if (ierr.Error_code != DCT_SUCCESS) {
      /*** Pad with spaces the string until the end ***/
      for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
      return (0);
   }
   
   return (1);

/* ------------------------------  END( dct_set_model_parlayout_ ) */
}

/******************************************************************/
/*                      dct_set_model_subdom_                     */
/*                                                                */
/* Sets the Subdomains and the processors assigned to DCT_Model   */
/*                                                                */
/*   mod:  DCT_Model pool memory to be setup in this routine      */
/* procs: is an array with dimension agreed with DCT_distribution */
/*        and and the lenght in each one agrees with the entries  */
/*        in the variable ModDistProcCounts. The distribution     */
/*        agrees row wise.                                        */
/* inipos: is an array indicating the start point in each         */
/*         dimension for each subdomain (same quantity as procs   */
/*         times dims). The array dimension are                   */
/*         dim X ModDistProcCounts, where dim is the domain       */
/*         dimension, ModDistProcCounts is the number of          */
/*         subdomains defined at Parallel Layout                  */
/* endpos: is an array indicating the ending point in each        */
/*         dimension for each subdomain (same quantity as dims    */
/*         times processes, as the previous).                     */
/* otype:  Indicates if the order of dimension is in FORTRAN      */
/*         order DCT_F_ORDER or not DCT_C_ORDER                   */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_model_subdom_( DCT_Model *mod, int *procs, int *inipos,
                           int *endpos, int *otype, int *err, char *imesg,
                           int limesg )
{
/* ---------------------------------------------  Local Variables  */
   DCT_Error            ierr;
  
   DCT_Model_Dom        *tmod;
   
   int                  *Cinipos;
   int                  *Cendpos;
   int                  offset;

   int                   dimtotal, totalelem;
   DCT_Integer           dim;

   register int         ii, jj, flag;
  
/* -------------------------------  BEGIN( dct_set_model_subdom_ ) */

   /******** The index used in fortran starts in one,
             but in C starts in 0. The indices are changed *******/
   flag = 0;
   tmod = mod->ModDomain;
   
   if ( (tmod != (DCT_Model_Dom *)NULL) ||         /* Check if the domain was set */
        ( tmod->ModSubDomParLayout != DCT_DIST_NULL ) ||
        ( tmod->ModSubDomLayoutDim != (DCT_Integer *)NULL ) ) {
      flag = 1;
      dim = tmod->ModDomDim;
   
      switch(tmod->ModSubDomParLayout)
      {
         case DCT_DIST_SEQUENTIAL:
            dimtotal = 1;
            break;
         case DCT_DIST_RECTANGULAR:
            dimtotal = *(tmod->ModSubDomLayoutDim) * *(tmod->ModSubDomLayoutDim+1);
            break;
         case DCT_DIST_3D_RECTANGULAR:
            dimtotal = *(tmod->ModSubDomLayoutDim) * *(tmod->ModSubDomLayoutDim+1) *
                       *(tmod->ModSubDomLayoutDim+2);
      }
      
      totalelem = dimtotal * dim;
      Cinipos = (DCT_Integer *) malloc( sizeof(DCT_Integer) * (size_t)totalelem );
      DCTERROR( Cinipos == (int *)NULL, "Memory Allocation Failed",
                *err  =  DCT_FAILED_MALLOC; return(1) );
      Cendpos = (DCT_Integer *) malloc( sizeof(DCT_Integer) * (size_t)totalelem );
      DCTERROR( Cendpos == (int *)NULL, "Memory Allocation Failed",
                *err  =  DCT_FAILED_MALLOC; return(1) );

      if (*otype==DCT_C_ORDER) {
         /* Transforming FORTRAN indices (starting at 1) to C/C++ indices (starting at 0) */
         for ( ii=0; ii < totalelem; ii++ ) *(Cinipos + ii) = *(inipos + ii) - 1;
   
         for ( ii=0; ii < totalelem; ii++ ) *(Cendpos + ii) = *(endpos + ii) - 1;
      }
      else if (*otype==DCT_F_ORDER) {
         /* Transforming FORTRAN indices (starting at 1) to C/C++ indices (starting at 0) */
         for ( ii=0; ii < dimtotal; ii++ ) {
            offset = ii*dim;
            for ( jj=0; jj < dim; jj++ )
               *(Cinipos + offset + jj) = *(inipos + offset + dim - jj - 1) - 1;
         }
   
         for ( ii=0; ii < dimtotal; ii++ ) {
            offset = ii*dim;
            for ( jj=0; jj < dim; jj++ )
               *(Cendpos + offset + jj) = *(endpos + offset + dim - jj - 1) - 1;
         }
      }
      else {
         *err = (int) DCT_INVALID_ARGUMENT;
         sprintf(imesg,
            "[dct_set_model_subdom] Type of order for model <%s> should be DCT_F_ORDER or DCT_C_ORDER\n",
            mod->ModName );
         for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
         return (0);
      }


   }
  
   /*** Set the DCT_Model Sub Domain ***/
   ierr = DCT_Set_Model_SubDom( mod, (DCT_Rank *) procs,
                                (DCT_Integer *) Cinipos, (DCT_Integer *) Cendpos );
   
   /* Arrays are freed */
   if ( flag) {
      free ( Cinipos );
      free ( Cendpos );
   }
   
   *err = (int) ierr.Error_code;
   strcpy(imesg, ierr.Error_msg);
  
   /*** If something does not go well  ***/
   if (ierr.Error_code != DCT_SUCCESS) {
      /*** Pad with spaces the string until the end ***/
      for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
      return (0);
   }
  
   return (1);

/* ---------------------------------  END( dct_set_model_subdom_ ) */
}

/******************************************************************/
/*                   dct_set_model_production_                    */
/*                                                                */
/*  Sets the variables to be Produced as well as their frequencies*/
/*     and the initial time of Production                         */
/*                                                                */
/*    mod:  DCT_Model pool memory to be setup in this routine     */
/*   pvar:     Pointer to the variable to be added to the model   */
/*   vname:    Name of the variable to be added to the model      */
/*   var_type: Type of the variable to be added. It must be one   */
/*             among DCT_Field, DCT_3d_Var or DCT_4d_Var.         */
/*   u:        Units of the DCT_Field (i.e., CELSIUS, Km/h, etc)  */
/* time_units: units of time must be belongs to DCT_Time_Types    */
/*   freq:     frequency (real scalar) of Production              */
/*   time_ini: initial time when the variable must be Produced    */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_model_production_( DCT_Model *mod, DCT_VoidPointer pvar,
                               char *vname, int *var_type, int *u,
                               int *time_units, double *freq, double *time_ini,
                               int *err, char *imesg, int lname, int limesg )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
  DCT_Name             var;
  
/* ---------------------------  BEGIN( dct_set_model_production_ ) */
  
  
  /*   transform name from FORTRAN string to C string */
  var = (DCT_Name) malloc(sizeof(char)*(size_t)(lname+1));
  
  for(ii=0; (ii<lname) && ( !isspace(*(vname+ii)) ) ; ii++)
    *(var + ii) = *(vname+ii);
  *(var + ii) = '\0';
  
  /*** Set the DCT_Model Production variables ***/
  ierr = DCT_Set_Model_Production( mod, pvar, (DCT_Name) var,
                               (DCT_Object_Types) *var_type, (DCT_Units) *u,
                               (DCT_Time_Types) *time_units, (DCT_Scalar) *freq,
                               (DCT_Scalar) *time_ini );
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* -----------------------------  END( dct_set_model_production_ ) */
}

/******************************************************************/
/*                   dct_set_model_production_                    */
/*                                                                */
/*  Sets the variables to be Produced as well as their frequencies*/
/*     and the initial time of Production                         */
/*                                                                */
/*   mod:  DCT_Model pool memory to be setup in this routine      */
/*   var:  Pointer to the variable to be added to the model       */
/* vname:      Name of the variable to be added to the model      */
/* var_type:   Type of the variable to be added. It must be one   */
/*             among DCT_Field, DCT_3d_Var or DCT_4d_Var.         */
/* u:          Units of the DCT_Field (i.e., CELSIUS, Km/h, etc)  */
/* time_units: units of time must be belongs to DCT_Time_Types    */
/*   freq:     frequency (real scalar) of Consumption             */
/*   time_ini: initial time when the variable must be Consumed    */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_model_consumption_( DCT_Model *mod, DCT_VoidPointer pvar,
                               char *vname, int *var_type, int *u,
                               int *time_units, double *freq, double *time_ini,
                               int *err, char *imesg, int lname, int limesg )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
  DCT_Name             var;
  
/* --------------------------  BEGIN( dct_set_model_consumption_ ) */
  
  /*   transform name from FORTRAN string to C string */
  var = (DCT_Name) malloc(sizeof(char)*(size_t)(lname+1));
  
  for(ii=0; (ii<lname) && ( !isspace(*(vname+ii)) ) ; ii++)
    *(var + ii) = *(vname+ii);
  *(var + ii) = '\0';
  
  /*** Set the DCT_Model Consumer Variables ***/
  ierr = DCT_Set_Model_Consumption( mod, pvar, (DCT_Name) var, *var_type,
                                   (DCT_Units) *u, (DCT_Time_Types) *time_units,
                                   (DCT_Scalar) *freq, (DCT_Scalar) *time_ini );
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* ----------------------------  END( dct_set_model_consumption_ ) */
}

/******************************************************************/
/*                      dct_set_model_var_                        */
/*                                                                */
/* Sets the variables to be consumed as well as their frequencies */
/* and the initial time of production                             */
/*                                                                */
/*    mod:  DCT_Model pool memory to be setup in this routine     */
/*    var:  Pointer to the variable to be added to the model      */
/*  var_type: Type of the variable to be added. It must be one    */
/*      among DCT_Field, DCT_3d_Var or DCT_4d_Var.                */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_model_var_( DCT_Model *mod, DCT_VoidPointer pvar, int *var_type,
                        int *err, char *imesg, int limesg )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  
/* ----------------------------------  BEGIN( dct_set_model_var_ ) */
  
  
  /*** Set the DCT_Model Variables ***/
  ierr = DCT_Set_Model_Var( mod, pvar, (DCT_Object_Types) *var_type );
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* ------------------------------------  END( dct_set_model_var_ ) */
}

/******************************************************************/
/*                    dct_update_model_time_                      */
/*                                                                */
/* Updates the current time in the DCT_Model structure, using the */
/* initial time and the time step set before                      */
/*                                                                */
/* mod: DCT_Model pool memory to be destroyed in this routine     */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_update_model_time_( DCT_Model *mod, int *err, char *imesg, int limesg )
{
  
/* ---------------------------------------------  Local Variables  */
  
  DCT_Error            ierr;
  register int         ii;
  
/* ------------------------------  BEGIN( dct_update_model_time_ ) */
  
  /*** update the model time ***/
  ierr = DCT_Update_Model_Time( mod );
  *err = (int) ierr.Error_code;
  strcpy( imesg, ierr.Error_msg );
  
  /*** If something does not go well  ***/
  if ( ierr.Error_code != DCT_SUCCESS ) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return(1);
/* --------------------------------  END( dct_update_model_time_ ) */
}

/******************************************************************/
/*                        dct_destroy_model_                      */
/*                                                                */
/*   This routine calls the DCT_Destroy_Model function, to free   */
/*   all the data structures created and attached to the          */
/*   DCT_Model data structure                                    */
/*                                                                */
/* mod: DCT_Model pool memory to be destroyed in this routine     */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_destroy_model_( DCT_Model *mod, int *err, char *imesg, int limesg )
{
  
/* ---------------------------------------------  Local Variables  */
  
  DCT_Error            ierr;
  register int         ii;
  
/* ----------------------------------  BEGIN( dct_destroy_model_ ) */
  
  /*** destroy the field ***/
  ierr = DCT_Destroy_Model( mod );
  *err = (int) ierr.Error_code;
  strcpy( imesg, ierr.Error_msg );
  
  /*** If something does not go well  ***/
  if ( ierr.Error_code != DCT_SUCCESS ) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return(1);
/* ------------------------------------  END( dct_destroy_model_ ) */
}


/**************************************************************************/
/**************************************************************************/
/*                                                                        */
/*              D C T   C O U P L E    F U N C T I O N S                  */
/*                                                                        */
/**************************************************************************/
/**************************************************************************/

/******************************************************************/
/*                      dct_create_couple_                        */
/*                                                                */
/* This routine when called by the user it initializes a global   */
/* couple.                                                        */
/*                                                                */
/* cop: DCT_Couple memory pool to be setup in this routine        */
/* name: Internal name of the coupler (use during coupling)       */
/* desc: Description of the coupler 120 char max                  */
/* moda: First of the DCT_Model to couple                         */
/* modb: Second DCT_Model name to couple                          */
/* numvar: Number of variables to be coupled between the two      */
/*         models                                                 */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_create_couple_( DCT_Couple *cop, char *name, char *desc,
                        DCT_Model *moda, char *modb, int *err,
                        char *imesg, int lname, int ldesc, int lmodb, int limesg )
{

/* ---------------------------------------------  Local Variables  */
  register int         ii;
  
  DCT_Error            ierr;
  DCT_Name             cname, cmodb;
  DCT_String           cdesc;

/* ----------------------------------  BEGIN( dct_create_couple_ ) */
  
  
  /*   transform name from FORTRAN string to C string */
  cname = (DCT_Name) malloc(sizeof(char)*(size_t)(lname+1));
  
  for(ii=0; (ii<lname) && ( !isspace(*(name+ii)) ) ; ii++)
    *(cname+ii) = *(name+ii);
  *(cname + ii) = '\0';

  /*   transform name from FORTRAN string to C string */
  cdesc = (DCT_String) malloc(sizeof(char)*(size_t)(ldesc+1));
  for(ii=0; ii<ldesc ; ii++)
    *(cdesc + ii) = *(desc + ii);
  *(cdesc + ldesc) = '\0';
  
  /*   transform name from FORTRAN string to C string */
  cmodb = (DCT_Name) malloc(sizeof(char)*(size_t)(lmodb+1));
  
  for(ii=0; (ii<lmodb) && ( !isspace(*(modb+ii)) ) ; ii++)
    *(cmodb+ii) = *(modb+ii);
  *(cmodb + ii) = '\0';
    
  /*** create the Couple ***/
  ierr = DCT_Create_Couple( cop, (DCT_Name) cname, (DCT_String) cdesc,
                            moda, cmodb );
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return(0);
  }
                               
  return(1);
/* ------------------------------------  END( dct_create_couple_ ) */
}

/******************************************************************/
/*                dct_set_coupling_vars_                          */
/*                                                                */
/*  Sets the variables to be coupled as well as their frequencies */
/*                                                                */
/*    cop:  DCT_Couple memory pool to be setup in this routine    */
/*   var1:  Pointer to the variable to be added to the couple     */
/*   var2:  Name to the corresponding variable to be coupled      */
/* var_type: Type of the variable to be added. It must be one     */
/*      among DCT_Field, DCT_3d_Var or DCT_4d_Var.                */
/*  var_trans:Type of transformation to use. It must be defined   */
/*      in DCT_Data_Transformations                               */
/*  freq:  frequency (real scalar) of coupling                    */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_set_coupling_vars_( DCT_Couple *cop, DCT_VoidPointer var1, char *var2,
                            int *var_type, int *var_trans, int *err,
                            char *imesg, int lvar2, int limesg )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  register int         ii;
  DCT_Name             cvar2;
  
/* ------------------------------  BEGIN( dct_set_coupling_vars_ ) */

  /*   transform name from FORTRAN string to C string */
  cvar2 = (DCT_Name) malloc(sizeof(char)*(size_t)(lvar2+1));
  
  for(ii=0; (ii<lvar2) && ( !isspace(*(var2+ii)) ) ; ii++)
    *(cvar2+ii) = *(var2+ii);
  *(cvar2 + ii) = '\0';
  
  /*** Set the DCT_Model Consumer Variables ***/
  ierr = DCT_Set_Coupling_Vars( cop, var1, cvar2, (DCT_Object_Types) *var_type,
                                (DCT_Data_Transformation) *var_trans );
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return (1);

/* --------------------------------  END( dct_set_coupling_vars_ ) */
}

/******************************************************************/
/*                        dct_destroy_couple_                     */
/*                                                                */
/*   This routine calls the DCT_Destroy_Couple function, to free  */
/*   all the data structures created and attached to the          */
/*   DCT_Couple data structure                                   */
/*                                                                */
/* cop: DCT_Couple memory pool to be destroyed in this routine    */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_destroy_couple_( DCT_Couple *cop, int *err, char *imesg, int limesg )
{
  
/* ---------------------------------------------  Local Variables  */
  
  DCT_Error            ierr;
  register int         ii;
  
/* ---------------------------------  BEGIN( dct_destroy_couple_ ) */
  
  /*** destroy the couple ***/
  ierr = DCT_Destroy_Couple( cop );
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS) {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return(1);
/* -----------------------------------  END( dct_destroy_couple_ ) */
}

/**************************************************************************/
/**************************************************************************/
/*                                                                        */
/*           D C T   D A T A   U T I L    F U N C T I O N S               */
/*                                                                        */
/**************************************************************************/
/**************************************************************************/

/***************************************************************/
/*                     dct_set_filetitle_                      */
/*                                                             */
/*   Set the string used as title of the simulation and it is  */
/*   placed at the beginning of file name used to exchange     */
/*   variables. No space in the string, if it is so, they will */
/*   be changed by underscore                                  */
/*                                                             */
/*   Arguments:                                                */
/*     title: the title to be set for the simulation           */
/* err: Returns the error code                                 */
/* imesg: Returns the error message                            */
/***************************************************************/
// int dct_set_filetitle_( char *title, int *err, char *imesg,
//                             int ltitle, int limesg )
// {
//   
// /* ---------------------------------------------  Local Variables  */
//   
//   DCT_Error            ierr;
//   DCT_String           ctitle;
//   register int         ii, ind;
//   
// /* ----------------------------------  BEGIN( dct_set_filetitle_ ) */
//   
//   /*   transform name from FORTRAN string to C string */
//   ctitle = (DCT_String) malloc(sizeof(char)*(size_t)(ltitle+1));
//   for(ii=0; ii < ltitle ; ii++) {
//     if (!isspace(*(ctitle + ii))) ind=ii;
//     *(ctitle + ii) = *(title + ii);
//   }
//   *(ctitle + (++ind)) = '\0';
//   
//   /*** Call the registration step ***/
//   ierr = DCT_Set_FileTitle( ctitle );
//   *err = (int) ierr.Error_code;
//   strcpy(imesg, ierr.Error_msg);
//   
//   /*** If something does not go well  ***/
//   if (ierr.Error_code != DCT_SUCCESS)  {
//     /*** Pad with spaces the string until the end ***/
//     for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
//     return (0);
//   }
//   
//   return(1);
// /* ------------------------------------  END( dct_set_filetitle_ ) */
// }

/**************************************************************************/
/**************************************************************************/
/*                                                                        */
/*              D C T   F I L E I O    F U N C T I O N S                  */
/*                                                                        */
/**************************************************************************/
/**************************************************************************/

/******************************************************************/
/*                     dct_write_field_                           */
/*                                                                */
/*   This function writes in a file the values of the a variable  */
/*                                                                */
/* var:  Is a pointer to a DCT_Field structure to be written      */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
// int dct_write_field_( DCT_Field *var, int *err, char *imesg, int limesg )
// {
//   
// /* ---------------------------------------------  Local Variables  */
//   
//   DCT_Error            ierr;
//   register int         ii;
//   
// /* ------------------------------------  BEGIN( dct_write_field_ ) */
// 
//   /*** Call the registration step ***/
//   ierr = DCT_Write_Field( var );
//   *err = (int) ierr.Error_code;
//   strcpy(imesg, ierr.Error_msg);
//   
//   /*** If something does not go well  ***/
//   if (ierr.Error_code != DCT_SUCCESS)  {
//     /*** Pad with spaces the string until the end ***/
//     for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
//     return (0);
//   }
//   
//   return(1);
// /* --------------------------------------  END( dct_write_field_ ) */
// }


/******************************************************************/
/*                     dct_write_3d_var_                          */
/*                                                                */
/*   This function writes in a file the values of the a variable  */
/*                                                                */
/* var:  Is a pointer to a DCT_3d_Var structure to be written     */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
// int dct_write_3d_var_( DCT_3d_Var *var, int *err, char *imesg, int limesg )
// {
//   
// /* ---------------------------------------------  Local Variables  */
//   
//   DCT_Error            ierr;
//   register int         ii;
//   
// /* -----------------------------------  BEGIN( dct_write_3d_var_ ) */
// 
//   /*** Call the registration step ***/
//   ierr = DCT_Write_3d_Var( var );
//   *err = (int) ierr.Error_code;
//   strcpy(imesg, ierr.Error_msg);
//   
//   /*** If something does not go well  ***/
//   if (ierr.Error_code != DCT_SUCCESS)  {
//     /*** Pad with spaces the string until the end ***/
//     for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
//     return (0);
//   }
//   
//   return(1);
// /* -------------------------------------  END( dct_write_3d_var_ ) */
// }

/******************************************************************/
/*                      dct_read_field_                           */
/*                                                                */
/*   This function reads from a file the values of the a variable */
/*                                                                */
/* var:  Is a pointer to a DCT_Field structure to be read         */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
// int dct_read_field_( DCT_Field *var, int *err, char *imesg, int limesg )
// {
//   
// /* ---------------------------------------------  Local Variables  */
//   
//   DCT_Error            ierr;
//   register int         ii;
//   
// /* -------------------------------------  BEGIN( dct_read_field_ ) */
// 
//   /*** Call the registration step ***/
//   ierr = DCT_Read_Field( var );
//   *err = (int) ierr.Error_code;
//   strcpy(imesg, ierr.Error_msg);
//   
//   /*** If something does not go well  ***/
//   if (ierr.Error_code != DCT_SUCCESS)  {
//     /*** Pad with spaces the string until the end ***/
//     for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
//     return (0);
//   }
//   
//   return(1);
// /* ---------------------------------------  END( dct_read_field_ ) */
// }


/******************************************************************/
/*                      dct_read_3d_var_                          */
/*                                                                */
/*   This function reads from a file the values of the a variable */
/*                                                                */
/* var:  Is a pointer to a DCT_3d_Var structure to be read        */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
// int dct_read_3d_var_( DCT_3d_Var *var, int *err, char *imesg, int limesg )
// {
//   
// /* ---------------------------------------------  Local Variables  */
//   
//   DCT_Error            ierr;
//   register int         ii;
//   
// /* ------------------------------------  BEGIN( dct_read_3d_var_ ) */
// 
//   /*** Call the registration step ***/
//   ierr = DCT_Read_3d_Var( var );
//   *err = (int) ierr.Error_code;
//   strcpy(imesg, ierr.Error_msg);
//   
//   /*** If something does not go well  ***/
//   if (ierr.Error_code != DCT_SUCCESS)  {
//     /*** Pad with spaces the string until the end ***/
//     for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
//     return (0);
//   }
//   
//   return(1);
// /* --------------------------------------  END( dct_read_3d_var_ ) */
// }

/**************************************************************************/
/**************************************************************************/
/*                                                                        */
/*            D C T   D A T A   C O M M    F U N C T I O N S              */
/*                                                                        */
/**************************************************************************/
/**************************************************************************/

/******************************************************************/
/*                       dct_send_field_                          */
/*                                                                */
/*   This function sends the values of a DCT_Field variable       */
/*                                                                */
/* var:  Is a pointer to a DCT_Field structure to be sent         */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_send_field_( DCT_Field *var, int *err, char *imesg, int limesg )
{
  
/* ---------------------------------------------  Local Variables  */
  
  DCT_Error            ierr;
  register int         ii;
  
/* -------------------------------------  BEGIN( dct_send_field_ ) */

  /*** Call the registration step ***/
  ierr = DCT_Send_Field( var );
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS)  {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return(1);
/* ---------------------------------------  END( dct_send_field_ ) */
}

/******************************************************************/
/*                       dct_send_3d_var_                         */
/*                                                                */
/*   This function sends the values of a DCT_3d_Var variable      */
/*                                                                */
/* var:  Is a pointer to a DCT_3d_Var structure to be sent        */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_send_3d_var_( DCT_3d_Var *var, int *err, char *imesg, int limesg )
{
  
/* ---------------------------------------------  Local Variables  */
  
  DCT_Error            ierr;
  register int         ii;
  
/* ------------------------------------  BEGIN( dct_send_3d_var_ ) */

  /*** Call the registration step ***/
  ierr = DCT_Send_3d_Var( var );
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS)  {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return(1);
/* --------------------------------------  END( dct_send_3d_var_ ) */
}

/******************************************************************/
/*                      dct_recv_field_                           */
/*                                                                */
/*   This function receives the values of a DCT_Field variable    */
/*                                                                */
/* var:  Is a pointer to a DCT_Field structure to be sent         */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_recv_field_( DCT_Field *var, int *err, char *imesg, int limesg )
{
  
/* ---------------------------------------------  Local Variables  */
  
  DCT_Error            ierr;
  register int         ii;
  
/* -------------------------------------  BEGIN( dct_recv_field_ ) */

  /*** Call the registration step ***/
  ierr = DCT_Recv_Field( var );
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS)  {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return(1);
/* ---------------------------------------  END( dct_recv_field_ ) */
}


/******************************************************************/
/*                      dct_recv_3d_var_                          */
/*                                                                */
/*   This function receives the values of a DCT_3d_Var variable   */
/*                                                                */
/* var:  Is a pointer to a DCT_3d_Var structure to be sent        */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_recv_3d_var_( DCT_3d_Var *var, int *err, char *imesg, int limesg )
{
  
/* ---------------------------------------------  Local Variables  */
  
  DCT_Error            ierr;
  register int         ii;
  
/* ------------------------------------  BEGIN( dct_recv_3d_var_ ) */

  /*** Call the registration step ***/
  ierr = DCT_Recv_3d_Var( var );
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS)  {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return(1);
/* --------------------------------------  END( dct_recv_3d_var_ ) */
}


/**************************************************************************/
/**************************************************************************/
/*                                                                        */
/*              D C T   B R O K E R    F U N C T I O N S                  */
/*                                                                        */
/**************************************************************************/
/**************************************************************************/

/******************************************************************/
/*                        dct_assign_broker_                      */
/*                                                                */
/*   This routine could be call at the before the Registration    */
/*   step. The broker id is set in order to assign an specific    */
/*   broker process                                               */
/*                                                                */
/* broker: Is the rank of the broker in the specified comm        */
/* comm:   Is the communicator or context where all the coupled   */
/*         models processes are defined                           */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_assign_broker_( int *broker, MPI_Fint *comm, int *err, char *imesg,
                            int limesg )
{
  
/* ---------------------------------------------  Local Variables  */
  
  DCT_Error            ierr;
  register int         ii;
  
/* ------------------------------  BEGIN( dct_beginregistration_ ) */

  /*** Call the registration step ***/
  ierr = DCT_Assign_Broker( (DCT_Rank) *broker, (DCT_Comm) MPI_Comm_f2c( *comm) );
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS)  {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return(1);
/* --------------------------------  END( dct_beginregistration_ ) */
}

/******************************************************************/
/*                     dct_beginregistration_                     */
/*                                                                */
/*   This routine should be call at the beginning of the          */
/*   Registration step. The system check the initialization       */
/*   variables, and the communication library status, if so is    */
/*   required                                                     */
/*                                                                */
/* comm:   Is the communicator or context where all the coupled   */
/*         models processes are defined                           */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_beginregistration_( int *comm, int *err, char *imesg, int limesg )
{
  
/* ---------------------------------------------  Local Variables  */
  
  DCT_Error            ierr;
  register int         ii;
  
/* ------------------------------  BEGIN( dct_beginregistration_ ) */

  /*** Call the registration step ***/
  ierr = DCT_BeginRegistration( (DCT_Comm) MPI_Comm_f2c( *comm) );
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS)  {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return(1);
/* --------------------------------  END( dct_beginregistration_ ) */
}


/******************************************************************/
/*                      dct_endregistration_                      */
/*                                                                */
/*   This routine should be call at the end of the Registration   */
/*   step.                                                        */
/*                                                                */
/* err: Returns the error code                                    */
/* imesg: Returns the error message                               */
/******************************************************************/
int dct_endregistration_( int *err, char *imesg, int limesg )
{
  
/* ---------------------------------------------  Local Variables  */
  
  DCT_Error            ierr;
  register int         ii;
  
/* --------------------------------  BEGIN( dct_endregistration_ ) */
  
  /*** Call the registration step ***/
  ierr = DCT_EndRegistration(  );
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS)  {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return(1);
/* ----------------------------------  END( dct_endregistration_ ) */
}

/******************************************************************/
/*                          dct_finalized_                        */
/*                                                                */
/*   This routine should be call at the end of the program.       */
/*   It cleans and frees the structures created in the library    */
/*                                                                */
/*  err: Returns the error code                                   */
/*  imesg: Returns the error message                              */
/******************************************************************/
int dct_finalized_( int *err, char *imesg, int limesg )
{
  
/* ---------------------------------------------  Local Variables  */
  
  DCT_Error            ierr;
  register int         ii;
  
/* --------------------------------------  BEGIN( dct_finalized_ ) */
  
  /*** Call the registration step ***/
  ierr = DCT_Finalized(  );
  *err = (int) ierr.Error_code;
  strcpy(imesg, ierr.Error_msg);
  
  /*** If something does not go well  ***/
  if (ierr.Error_code != DCT_SUCCESS)  {
    /*** Pad with spaces the string until the end ***/
    for (ii = (strlen(imesg)-1); ii < limesg; ii++) *(imesg+ii)=' ';
    return (0);
  }
  
  return(1);
/* ----------------------------------------  END( dct_finalized_ ) */
}

