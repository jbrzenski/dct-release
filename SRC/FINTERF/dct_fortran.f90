!===================================================================
!              Distributed Coupling Toolkit (DCT)
!>
!! \file dct_fortran.f90
!!
!! \brief FORTRAN Module of DCT library to be used from FORTRAN 90
!!
!!  This file contains the constant parameter values used in DCT
!!  to make its use compatible from FORTRAN 90. In addition, contains
!!  the definitions of the DCT data structure \DCT_Couple, DCT_Model,
!!  DCT_3d_Var, and DCT_Field
!!
!!
!! \warning  The DCT structures are defined here as a pool of memory
!!     space where the C library can use the actual estructure.
!!
!! \date Date of Creation: July 7, 2010.  
!!
!! \author Dany De Cecchis: dcecchis@uc.edu.ve
!! \author Tony Drummond: LADrummond@lbl.gov
!!
!!  \copyright GNU Public License.
!!
!!
!!    \todo This file might need a deeper check to see if there
!!          something changed, to make consisten with the C
!!          implementation
!!
!
!===================================================================
!
!   $Id: dct_fortran.f90,v 1.8 2012/11/06 20:54:21 dcecchis Exp $
!

MODULE DCT_FORTRAN

implicit none


!  Constants for Time Manipulation:
!  different defined time units for
!  data fields/variables (see DCT_Time_Types)
INTEGER, PARAMETER    ::  DCT_TIME_UNKNOWN=               0
INTEGER, PARAMETER    ::  DCT_TIME_NO_UNIT=               1               
INTEGER, PARAMETER    ::  DCT_TIME_SECONDS=               2
INTEGER, PARAMETER    ::  DCT_TIME_MINUTES=               3
INTEGER, PARAMETER    ::  DCT_TIME_HOURS=                 4
INTEGER, PARAMETER    ::  DCT_TIME_DAYS=                  5
INTEGER, PARAMETER    ::  DCT_TIME_WEEKS=                 6
INTEGER, PARAMETER    ::  DCT_TIME_YEARS=                 7

!  Constants for Parallel Processor Layouts
!  set in models (see DCT_Distribution)
INTEGER, PARAMETER    ::  DCT_DIST_NULL=                  0
INTEGER, PARAMETER    ::  DCT_DIST_SEQUENTIAL=            1
INTEGER, PARAMETER    ::  DCT_DIST_RECTANGULAR=           2
INTEGER, PARAMETER    ::  DCT_DIST_3D_RECTANGULAR=        3

!  Constants for Data Transformations
!  when remapping operations are required
!  (see DCT_Data_Transformation)
INTEGER, PARAMETER    ::  DCT_NO_INTERPOLATION=           0
INTEGER, PARAMETER    ::  DCT_LINEAR_INTERPOLATION=       1
INTEGER, PARAMETER    ::  DCT_QUAD_INTERPOLATION=         2
INTEGER, PARAMETER    ::  DCT_CUBIC_INTERPOLATION=        3

!  In order to be consisten in ow the arrays are stored in memory
!  two option are given. DCT_F_ORDER, where in memory the last index
!  goes first (column wise) or DCT_NO_FORDER where the first index 
!  goes first
INTEGER, PARAMETER    ::  DCT_F_ORDER=                    1
INTEGER, PARAMETER    ::  DCT_C_ORDER=                    0


!  Constants to define DCT Object Types
!  (see DCT_Object_Types)
INTEGER, PARAMETER    ::  DCT_TYPE_UNKNOWN=               0
INTEGER, PARAMETER    ::  DCT_FIELD_TYPE=                 1
INTEGER, PARAMETER    ::  DCT_3D_VAR_TYPE=                2
INTEGER, PARAMETER    ::  DCT_4D_VAR_TYPE=                3
INTEGER, PARAMETER    ::  DCT_MODEL_TYPE=                 4
!INTEGER, PARAMETER    ::  DCT_COUPLE_TYPE=                5
!INTEGER, PARAMETER    ::  DCT_DATA_SUBDOM_TYPE=           6


!  Basic types of data which coud be
!  wrapped by the meta data (see DCT_Data_Types)
INTEGER, PARAMETER    ::  DCT_DATA_TYPE_UNKNOWN=          0
INTEGER, PARAMETER    ::  DCT_INTEGER=                    1
INTEGER, PARAMETER    ::  DCT_FLOAT=                      2
INTEGER, PARAMETER    ::  DCT_DOUBLE=                     3
! INTEGER, PARAMETER    ::  DCT_LONG_DOUBLE=              
INTEGER, PARAMETER    ::  DCT_REAL=                       DCT_FLOAT
INTEGER, PARAMETER    ::  DCT_DOUBLE_PRECISION=           DCT_DOUBLE


!  Constants for Error Manipulation (Error Codes)
!  (see DCT_Error_Handler)
INTEGER, PARAMETER    ::  DCT_SUCCESS=                    0
INTEGER, PARAMETER    ::  DCT_INVALID_ARGUMENT=           1
INTEGER, PARAMETER    ::  DCT_INVALID_MODEL_DEFINITION=   2
INTEGER, PARAMETER    ::  DCT_INVALID_MODEL_TAG=          3
INTEGER, PARAMETER    ::  DCT_INVALID_COUPLING_VARIABLE=  4
INTEGER, PARAMETER    ::  DCT_INVALID_COUPLING_DOMAIN=    5
INTEGER, PARAMETER    ::  DCT_INVALID_UNITS=              6
INTEGER, PARAMETER    ::  DCT_FAILED_MALLOC=              7
INTEGER, PARAMETER    ::  DCT_FAILED_TRANSFOR=            8
INTEGER, PARAMETER    ::  DCT_FAILED_LIB_COMM=            9
INTEGER, PARAMETER    ::  DCT_INVALID_COMM=              10
INTEGER, PARAMETER    ::  DCT_INVALID_DISTRIBUTION=      11
INTEGER, PARAMETER    ::  DCT_INVALID_LABELING=          12
INTEGER, PARAMETER    ::  DCT_INVALID_DIMENSIONS=        13
INTEGER, PARAMETER    ::  DCT_INVALID_FREQ=              14
INTEGER, PARAMETER    ::  DCT_INVALID_OPERATION=         15
INTEGER, PARAMETER    ::  DCT_INVALID_DATA_TYPE=         16
INTEGER, PARAMETER    ::  DCT_INVALID_DATA_TRANS=        17
INTEGER, PARAMETER    ::  DCT_INVALID_NUMB_CPLS=         18
INTEGER, PARAMETER    ::  DCT_FILE_OPEN_ERROR=           19
INTEGER, PARAMETER    ::  DCT_FILE_IO_ERROR=             20
INTEGER, PARAMETER    ::  DCT_NAME_MATCH_ERROR=          21
INTEGER, PARAMETER    ::  DCT_VALUE_MATCH_ERROR=         22
INTEGER, PARAMETER    ::  DCT_OBJECT_DUPLICATED=         23
INTEGER, PARAMETER    ::  DCT_COUPLE_LIST_ERROR=         24
INTEGER, PARAMETER    ::  DCT_MODEL_LIST_ERROR=          25
INTEGER, PARAMETER    ::  DCT_VARS_LIST_ERROR=           26
INTEGER, PARAMETER    ::  DCT_COUPLE_TAG_EXHAUSTED=      27
INTEGER, PARAMETER    ::  DCT_MODEL_TAG_EXHAUSTED=       28
INTEGER, PARAMETER    ::  DCT_VARS_TAG_EXHAUSTED=        29
INTEGER, PARAMETER    ::  DCT_UNKNOWN_ERROR=             30


!  Constant for immidiate data exchange
INTEGER, PARAMETER    ::  DCT_NOW=                      -1


!  Constants to set a variable as Production or
!  Consumption Arguments (see DCT_ProdCons)
INTEGER, PARAMETER    ::  DCT_PRODUCE=                    0
INTEGER, PARAMETER    ::  DCT_CONSUME=                    1

!  Supported Units by the DCT interface if transformation
!  operation is required (see DCT_Units)
INTEGER, PARAMETER    ::  DCT_UNIT_UNKNOWN=               0
INTEGER, PARAMETER    ::  DCT_NO_UNITS=                   1
INTEGER, PARAMETER    ::  CENTIMETER=                     2
INTEGER, PARAMETER    ::  METER=                          3
INTEGER, PARAMETER    ::  KILOMETER=                      4
INTEGER, PARAMETER    ::  MILES=                          5
INTEGER, PARAMETER    ::  INCH=                           6
INTEGER, PARAMETER    ::  FEET=                           7
INTEGER, PARAMETER    ::  SECONDS=                        8
INTEGER, PARAMETER    ::  MINUTES=                        9
INTEGER, PARAMETER    ::  HOURS=                         10
INTEGER, PARAMETER    ::  DAYS=                          11
INTEGER, PARAMETER    ::  KILOMETERS_HOUR=               12
INTEGER, PARAMETER    ::  KILOMETERS_SECOND=             13
INTEGER, PARAMETER    ::  KILOMETERS_MINUTE=             14
INTEGER, PARAMETER    ::  MILES_HOUR=                    15
INTEGER, PARAMETER    ::  MILES_SECOND=                  16
INTEGER, PARAMETER    ::  MILES_MINUTE=                  17
INTEGER, PARAMETER    ::  CELSIUS=                       18
INTEGER, PARAMETER    ::  KELVIN=                        19
INTEGER, PARAMETER    ::  FAHRENHEIT=                    20
INTEGER, PARAMETER    ::  USER_DEFINED1=                 21
INTEGER, PARAMETER    ::  USER_DEFINED2=                 22
INTEGER, PARAMETER    ::  USER_DEFINED3=                 23
INTEGER, PARAMETER    ::  USER_DEFINED4=                 24
INTEGER, PARAMETER    ::  USER_DEFINED5=                 25
INTEGER, PARAMETER    ::  USER_DEFINED6=                 26
INTEGER, PARAMETER    ::  USER_DEFINED7=                 27
INTEGER, PARAMETER    ::  USER_DEFINED8=                 28
INTEGER, PARAMETER    ::  USER_DEFINED9=                 29
INTEGER, PARAMETER    ::  USER_DEFINED10=                30


! DCT Boolean manipulation
INTEGER, PARAMETER    ::  DCT_FALSE=                      0
INTEGER, PARAMETER    ::  DCT_TRUE=                       1

!  DCT_Val_Loc descibes how the actual
!  user data are located with respect
!  the mesh (see DCT_Val_Loc)
INTEGER, PARAMETER    ::  DCT_LOC_UNSETS=                 0
INTEGER, PARAMETER    ::  DCT_LOC_CENTERED=               1
INTEGER, PARAMETER    ::  DCT_LOC_CORNERS=                2
INTEGER, PARAMETER    ::  DCT_LOC_STRIDE1=                3
INTEGER, PARAMETER    ::  DCT_LOC_STRIDE2=                4
INTEGER, PARAMETER    ::  DCT_LOC_STRIDEN=                5


!  DCT_Domain_Type descibes how is the
!  domain, such that structured, non
!  structured or general curvilinear
!  (see DCT_Domain_Type)
INTEGER, PARAMETER    ::  DCT_UNKNOWN_TYPE=               0
INTEGER, PARAMETER    ::  DCT_CARTESIAN  =               1
INTEGER, PARAMETER    ::  DCT_RECTILINEAR=               2
INTEGER, PARAMETER    ::  DCT_GENERAL_CURV=               3

TYPE DCT_FIELD
         CHARACTER (KIND=1, LEN=200) :: data
END TYPE DCT_FIELD

TYPE DCT_3D_VAR
         CHARACTER (KIND=1, LEN=224) :: data
END TYPE DCT_3D_VAR

TYPE DCT_MODEL
         CHARACTER (KIND=1, LEN=104) :: data ! That is give by size test
END TYPE DCT_MODEL

TYPE DCT_COUPLE
         CHARACTER (KIND=1, LEN=176) :: data ! That is give by size test
END TYPE DCT_COUPLE



END MODULE DCT_FORTRAN


