/**************************************************************/
/*              Distributed Coupling Toolkit (DCT)            */
/*!
   \file dct_4d_var.c

   \brief Contains the implementation of the functions to handle
          the data structure DCT_4d_Var.

    This file contains the implementation of all the functions to handle
    the DCT data type structure DCT_4d_Var; which represents
    4-dimensional model variables

    \date Date of Creation: Oct 6, 2005.

    \todo Change the use of two function DCT_Set_3d_Var_Freq_Consumption
     and DCT_Set_3d_Var_Freq_Production in only one function

    \warning This type is not fully working. Not all the
             functionalities in the DCT system have been neither
             implemented nor tested.

   \author Dany De Cecchis: dcecchis@gmail.com
   \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.
 */
/**************************************************************/


/* -----------------------------------------------  Include Files  */
#include <stdio.h>
#include "dct.h"
/* --------------------------------------------  Global Variables  */
#include "dctsys.h"
#include "dct_commdat.h"

/******************************************************************/
/*                     DCT_Create_4d_Var                          */
/*                                                                */
/*!   This routine when called by the user it initializes a global
   variable.  Internally the broker also calls this routine to
   create subdomain variables

   var: Is DCT_4d_Var variable to be setup as such in this routine
   name: Internal name of the variable (use during coupling)
   desc: Description of the variable 120 char max
      u: Units DCT_4d_Var. See dct.h for predefined DCT_Unit types
   comm: Group (model or application) that the var belongs to
   Prdx: Whether the variable is for Production or Consumption
*/
/******************************************************************/
DCT_Error DCT_Create_4d_Var( DCT_4d_Var *var, const DCT_Name name, const DCT_String desc,
                             const  DCT_Units u, const DCT_ProdCons Prdx)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  int                  len;

/* -----------------------------------  BEGIN( DCT_Create_4d_Var ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
     "[DCT_Create_4d_Var] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }
  /** get the name length **/
  len = strlen(name);
  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /* Check if the dct_4d_var was not previously created */
     if (var->VarTag==DCT_TAG_UNDEF) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)100);
         sprintf(ierr.Error_msg,
          "[DCT_Create_4d_Var] Invalid 4d_Var passed as 1st argument. The DCT_4d_Var was already created\n");
         return(ierr);
     }
     /* Name is a mandatory field to be filled */
     /*len = strlen(name); it was moved to avoid segmentation fault when a null argument is passed*/
     if (name == (DCT_Name)NULL || (len = strlen(name) == 0)) {
       ierr.Error_code  = DCT_INVALID_ARGUMENT;
       ierr.Error_msg   = (DCT_String) malloc((size_t)60);
       sprintf(ierr.Error_msg,
          "[DCT_Create_4d_Var] Invalid Name for Field. A name should be given\n");
       return(ierr);

     }

     if ( (u < 1) || (u > DCT_DEFINED_UNITS)) {
         ierr.Error_code  = DCT_INVALID_UNITS;
         ierr.Error_msg   = (DCT_String) malloc((size_t)125);
         sprintf(ierr.Error_msg,
         "[DCT_Create_4d_Var] Invalid Units for 4d_Var <%s>\n", name);
         return(ierr);
     }

     if ( (Prdx != DCT_PRODUCE) && (Prdx != DCT_CONSUME) ) {
         ierr.Error_code  = DCT_INVALID_ARGUMENT;
         ierr.Error_msg   = (DCT_String) malloc((size_t)125);
         sprintf(ierr.Error_msg,
             "[DCT_Create_4d_Var] Invalid Parameter #6 creating 4d_Var <%s>. Valid options are DCT_PRODUCE or DCT_CONSUME.\n", name);
         return(ierr);
     }
  }

  var->VarName = (DCT_Name) malloc(sizeof(char)*(size_t)(len+1));
  DCTERROR( var->VarName == (DCT_Name) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  strcpy(var->VarName, name);

  if (desc != (DCT_String)NULL) {
    len = strlen(desc);
    var->VarDescription = (DCT_String)malloc(sizeof(char)*(size_t)(len+1));
    DCTERROR( var->VarDescription == (DCT_String) NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    strcpy(var->VarDescription, desc);
  } else var->VarDescription = (DCT_String)NULL;


  var->VarModel         = (DCT_Model *)NULL;
  /* The variable is created unlocked */
  var->VarCommit        = DCT_FALSE;
  var->VarTag           = DCT_TAG_UNDEF;

  var->VarUnits         = u;
  /* Set Default Values for other Meta-Data 4d_var */

  var->VarDim[0]        = 0;
  var->VarDim[1]        = 0;
  var->VarDim[2]        = 0;
  var->VarDim[3]        = 0;

  var->VarDomType[0]    = DCT_UNKNOWN_TYPE;
  var->VarDomType[1]    = DCT_UNKNOWN_TYPE;
  var->VarDomType[2]    = DCT_UNKNOWN_TYPE;
  var->VarDomType[3]    = DCT_UNKNOWN_TYPE;
  var->VarLabels[0]      = (DCT_Scalar *)NULL;
  var->VarLabels[1]      = (DCT_Scalar *)NULL;
  var->VarLabels[2]      = (DCT_Scalar *)NULL;
  var->VarLabels[3]      = (DCT_Scalar *)NULL;

  var->VarDataLoc       = DCT_LOC_UNSETS;
  var->VarStride[0]     = 0;
  var->VarStride[1]     = 0;
  var->VarStride[2]     = 0;
  var->VarStride[3]     = 0;

  var->VarDistType      = DCT_DIST_NULL;
  var->VarDistProcCounts = (DCT_Integer *)NULL;
  var->VarDistRank       = (DCT_Rank *)NULL;
  if (Prdx == DCT_PRODUCE)
       var->VarProduced       = DCT_PRODUCE;
  else if (Prdx == DCT_CONSUME)
       var->VarProduced       = DCT_CONSUME;

  var->VarTimeUnits      = DCT_TIME_UNKNOWN;
  var->VarFrequency      = DCT_TIME_UNSET;
  var->VarTimeIni        = DCT_TIME_UNSET;
  var->VarLastUpdate     = DCT_TIME_UNSET;

  var->VarMask           = DCT_FALSE;
  var->VarMaskMap        = (DCT_Boolean *)NULL;

  var->VarFileName       = (DCT_String)NULL;

  var->VarNumCpl        = 0;
  var->VarCplIndx       = (DCT_Integer *)NULL;

  /*
  var->VarCommSched      = (DCT_Trans_Data   *)NULL;

  var->VarComm        = DCT_GROUP_NULL;
  */
  var->VarUserDataType   = DCT_DATA_TYPE_UNKNOWN;
  var->VarValues         = (DCT_Scalar *)NULL;

  if ( DCT_Step_Check == DCT_BEGIN_STATE ) {  /* The DCT_Couple structure is registered
                                                  in the global list and counted        */
     /* The variable is added to the local variable list */
     if ( DCT_List_CHKAdd( &DCT_Reg_Vars, var, DCT_4D_VAR_TYPE ) != (DCT_List *)NULL ) {
       ierr.Error_code   = DCT_OBJECT_DUPLICATED;
       ierr.Error_msg    = (DCT_String) malloc(115);
       sprintf(ierr.Error_msg,
         "[DCT_Create_Field] DCT_Field name <%s> is already used.\n", var->VarName );
       return(ierr);
     }
     DCT_LocalVars++;
  }

  ierr.Error_code = DCT_SUCCESS;
  ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* -------------------------------------  END( DCT_Create_4d_Var ) */

}

/*********************************************************************/
/*                     DCT_Set_4d_Var_Dims                           */
/*                                                                   */
/*!   This routine when called by the user contains global d1, d2, d3,
    and d4 values.  When called internally by the Data Broker it
    contains subdomain values set at the end of registration.

   var: Is DCT_4d_Var variable to be setup as such in this routine
    d1: dim1 of the array of values for this variable
    d2: dim2 of the array of values for this variable
    d3: dim3 of the array of values for this variable
    d4: dim4 of the array of values for this variable
*/
/*********************************************************************/
DCT_Error DCT_Set_4d_Var_Dims( DCT_4d_Var *var, const DCT_Integer d1,
                               const DCT_Integer d2, const DCT_Integer d3,
                               const DCT_Integer d4)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;

/* ---------------------------------  BEGIN( DCT_Set_4d_Var_Dims ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
     "[DCT_Set_4d_Var_Dims] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /*  Check the variable was created */
     if (var->VarTag!=DCT_TAG_UNDEF) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)115);
         sprintf(ierr.Error_msg,
           "[DCT_Set_4d_Var_Dims] Invalid 4d_Var passed as 1st argument. Create DCT_4d_Var first by calling DCT_Create_4d_Var\n");
         return(ierr);
     }
     /* Check if the Variable can be modified, i.e. it is not added to a model */
     if (var->VarCommit == DCT_TRUE){
       ierr.Error_code  = DCT_INVALID_OPERATION;
       ierr.Error_msg   = (DCT_String) malloc((size_t)150);
       sprintf(ierr.Error_msg,
           "[DCT_Set_4d_Var_Dims] Invalid Operation. The DCT_4d_var %s is already commited\n",
           var->VarName);
       return(ierr);
     }

     if ((d1 <= 0) || (d2 <= 0) ||  (d3 <= 0)  ||  (d4 <= 0)) {
         ierr.Error_code   = DCT_INVALID_UNITS;
         ierr.Error_msg    = (DCT_String) malloc((size_t)110);
         sprintf(ierr.Error_msg,
           "[DCT_Set_4d_Var_Dims] dimesions for 4d_Var <%s> should be positive integers [%d, %d, %d, %d]\n",
           var->VarName, d1, d2, d3, d4);
         return(ierr);
     }
  }

  var->VarDim[0]        = d1;
  var->VarDim[1]        = d2;
  var->VarDim[2]        = d3;
  var->VarDim[3]        = d4;

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* -----------------------------------  END( DCT_Set_4d_Var_Dims ) */

}

/******************************************************************/
/*                 DCT_Set_4d_Var_Val_Location                    */
/*                                                                */
/*!   This routine set the VarDataLoc field, indicating how the user
      is placed in the meash, and set the proper strides values when
      the location is DCT_LOC_STRIDE1 or DCT_LOC_STRIDE2
*/
/******************************************************************/
DCT_Error DCT_Set_4d_Var_Val_Location( DCT_4d_Var *var, const DCT_Val_Loc tloc)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;

/* -------------------------  BEGIN( DCT_Set_4d_Var_Val_Location ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
     "[DCT_Set_4d_Var_Val_Location] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /*  Check the variable was created */
     if (var->VarTag!=DCT_TAG_UNDEF) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)115);
         sprintf(ierr.Error_msg,
          "[DCT_Set_4d_Var_Val_Location] Invalid 4d_Var passed as 1st argument. Create DCT_4d_Var first by calling DCT_Create_4d_Var\n");
         return(ierr);
     }

     /* Check if the Variable can be modified, i.e. it is not added to a model */
     if (var->VarCommit == DCT_TRUE){
       ierr.Error_code  = DCT_INVALID_OPERATION;
       ierr.Error_msg   = (DCT_String) malloc((size_t)150);
       sprintf(ierr.Error_msg,
           "[DCT_Set_4d_Var_Val_Location] Invalid Operation. The DCT_4d_var %s is already commited\n",
           var->VarName);
       return(ierr);
     }

     if ( ( tloc < DCT_LOC_UNSETS ) || (tloc > DCT_DEFINED_VAL_LOC ) ) {
        ierr.Error_code   = DCT_INVALID_ARGUMENT;
        ierr.Error_msg    = (DCT_String) malloc((size_t)115);
        sprintf(ierr.Error_msg,
            "[DCT_Set_4d_Var_Val_Location] Invalid parameter for 4D_Var <%s>, Unknown DCT_Val_Loc type value\n", var->VarName);
        return(ierr);
     }
  }

  switch(tloc){
  case DCT_LOC_CENTERED:
  case DCT_LOC_CORNERS:
  case DCT_LOC_STRIDEN:
    var->VarDataLoc = tloc;
    break;
  case DCT_LOC_STRIDE1:
    var->VarDataLoc = tloc;
    var->VarStride[0]          = 1;
    var->VarStride[1]          = 1;
    var->VarStride[2]          = 1;
    var->VarStride[3]          = 1;
   break;
  case DCT_LOC_STRIDE2:
    var->VarDataLoc = tloc;
    var->VarStride[0]          = 2;
    var->VarStride[1]          = 2;
    var->VarStride[2]          = 2;
    var->VarStride[3]          = 2;

  }

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* ---------------------------  END( DCT_Set_4d_Var_Val_Location ) */

}

/******************************************************************/
/*                    DCT_Set_4d_Var_Strides                      */
/*                                                                */
/*!   This routine set the VarStride[:] fields, indicating how many
      space the variable is repeated in each direction
*/
/******************************************************************/
DCT_Error DCT_Set_4d_Var_Strides( DCT_4d_Var *var, const DCT_Integer st1, const DCT_Integer st2,
                                  const DCT_Integer st3, const DCT_Integer st4)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;

/* ------------------------------  BEGIN( DCT_Set_4d_Var_Strides ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
     "[DCT_Set_4d_Var_Strides] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /*  Check the variable was created */
     if (var->VarTag!=DCT_TAG_UNDEF) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)115);
         sprintf(ierr.Error_msg,
          "[DCT_Set_4d_Var_Strides] Invalid 4d_Var passed as 1st argument. Create DCT_4d_Var first by calling DCT_Create_4d_Var\n");
         return(ierr);
     }

     /* Check if the Variable can be modified, i.e. it is not added to a model */
     if (var->VarCommit == DCT_TRUE){
       ierr.Error_code  = DCT_INVALID_OPERATION;
       ierr.Error_msg   = (DCT_String) malloc((size_t)150);
       sprintf(ierr.Error_msg,
           "[DCT_Set_4d_Var_Strides] Invalid Operation. The DCT_4d_var %s is already commited\n",
           var->VarName);
       return(ierr);
     }

     if (var->VarDataLoc != DCT_LOC_STRIDEN) {
       ierr.Error_code   = DCT_INVALID_OPERATION;
       ierr.Error_msg    = (DCT_String) malloc((size_t)115);
       sprintf(ierr.Error_msg,
           "[DCT_Set_4d_Var_Strides] Invalid operation for 4D_Var <%s>, To set stride the VarDataLoc must be DCT_LOC_STRIDEN\n",
           var->VarName);
       return(ierr);
     }
  }

  var->VarStride[0]          = st1;
  var->VarStride[1]          = st2;
  var->VarStride[2]          = st3;
  var->VarStride[3]          = st4;

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* --------------------------------  END( DCT_Set_4d_Var_Strides ) */

}

/******************************************************************/
/*                     DCT_Set_4d_Var_Time                        */
/*                                                                */
/*!   This routine when called by the user contains the time units
      and the initial time when the variable becomes to be operational
      meaning the first time to be produced or consumed

   var: Is DCT_4d_Var variable to be setup as such in this routine
   time_units : units of time must be belongs to DCT_Time_Types
   time_ini : Initial time of production/consumption, related with
      time_units (can be a positive scalar)
*/
/******************************************************************/
DCT_Error DCT_Set_4d_Var_Time( DCT_4d_Var *var, const DCT_Time_Types time_units,
                               const DCT_Scalar time_ini)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;

/* ---------------------------------  BEGIN( DCT_Set_4d_Var_Time ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
     "[DCT_Set_4d_Var_Time] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /*  Check the variable was created */
     if (var->VarTag!=DCT_TAG_UNDEF) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)115);
         sprintf(ierr.Error_msg,
           "[DCT_Set_4d_Var_Time] Invalid 4d_Var passed as 1st argument. Create DCT_4d_Var first by calling DCT_Create_4d_Var\n");
         return(ierr);
     }
     /* Check if the Variable can be modified, i.e. it is not added to a model */
     if (var->VarCommit == DCT_TRUE){
       ierr.Error_code  = DCT_INVALID_OPERATION;
       ierr.Error_msg   = (DCT_String) malloc((size_t)150);
       sprintf(ierr.Error_msg,
           "[DCT_Set_4d_Var_Time] Invalid Operation. The DCT_4d_var %s is already commited\n",
           var->VarName);
       return(ierr);
     }

     if ((time_units < 1) || (time_units > DCT_DEFINED_TIME_UNITS)) {
         ierr.Error_code   = DCT_INVALID_UNITS;
         ierr.Error_msg    = (DCT_String) malloc((size_t)110);
         sprintf(ierr.Error_msg,
             "[DCT_Set_4d_Var_Time] time for 4d_Var <%s> should be a defined unit of time\n", var->VarName);
         return(ierr);
     }

     if ( (time_ini < 0.0) )
     {
       ierr.Error_code  = DCT_INVALID_ARGUMENT;
       ierr.Error_msg   = (DCT_String) malloc((size_t)80);
       sprintf(ierr.Error_msg,
       "[DCT_Set_4d_Var_Time], initial time for 4d_Var <%s> must be >= 0.0\n", var->VarName);
       return(ierr);
     }
  }

  var->VarTimeUnits     = (DCT_Time_Types) time_units;
  var->VarTimeIni       = (DCT_Scalar) time_ini;

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* -----------------------------------  END( DCT_Set_4d_Var_Time ) */

}


/*********************************************************************/
/*                      DCT_Set_4d_Var_Dist                          */
/*                                                                   */
/*!   Sets the Processor Layout associated with a DCT_4d_Var

   var: Is DCT_4d_Var variable to be setup as such in this routine
   dist: valid distribution of processors related to DCT_distribution
     defined in the file dct.h
   punt: pointer which points out to the distribution values
*/
/*********************************************************************/
DCT_Error DCT_Set_4d_Var_Dist( DCT_4d_Var *var, const DCT_Distribution dist,
                               const DCT_Integer *punt)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;

/* ---------------------------------  BEGIN( DCT_Set_4d_Var_Dist ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  if (DCT_Step_Check != DCT_BEGIN_STATE){
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
     "[DCT_Set_4d_Var_Dist] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  /*  Check the variable was created */
  if (var->VarTag!=DCT_TAG_UNDEF) {
      ierr.Error_code   = DCT_INVALID_ARGUMENT;
      ierr.Error_msg    = (DCT_String) malloc((size_t)115);
      sprintf(ierr.Error_msg,
       "[DCT_Set_4d_Var_Dist] Invalid 4d_Var passed as 1st argument. Create DCT_4d_Var first by calling DCT_Create_4d_Var\n");
      return(ierr);
  }

  /* Check if the Variable can be modified, i.e. it is not added to a model */
  if (var->VarCommit == DCT_TRUE){
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)150);
    sprintf(ierr.Error_msg,
        "[DCT_Set_4d_Var_Dist] Invalid Operation. The DCT_4d_var %s is already commited\n",
        var->VarName);
    return(ierr);
  }
  if ((dist < 0) || (dist >  DCT_MAX_DISTRIBUTION_TYPES)) {
      ierr.Error_code    = DCT_INVALID_DISTRIBUTION;
      ierr.Error_msg     = (DCT_String) malloc((size_t)100);
      sprintf(ierr.Error_msg,
     "[DCT_Set_4d_Var_Dist], The distribution type entered for 4d_Var <%s> is not valid\n", var->VarName);
      return(ierr);
  }
  if (!Set_Distribution_Type( dist, punt, &var->VarDistType, &var->VarDistProcCounts )) {
     ierr.Error_code    = DCT_INVALID_DISTRIBUTION;
     ierr.Error_msg     = (DCT_String) malloc((size_t)100);
     sprintf(ierr.Error_msg,
   "[DCT_Set_4d_Var_Dist], the distribution does not agree with the process distribution entered\n");
     return(ierr);
  }

  ierr.Error_code        = DCT_SUCCESS;
  ierr.Error_msg         = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* -----------------------------------  END( DCT_Set_4d_Var_Dist ) */

}

/******************************************************************/
/*                     DCT_Set_4d_Var_Mask                         */
/*                                                                */
/******************************************************************/
DCT_Error DCT_Set_4d_Var_Mask( DCT_4d_Var *var, const DCT_Boolean *mask)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;
  DCT_Integer          *dims;

/* ---------------------------------  BEGIN( DCT_Set_4d_Var_Mask ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
     "[DCT_Set_4d_Var_Mask] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /*  Check the variable was created */
     if (var->VarTag!=DCT_TAG_UNDEF) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)115);
         sprintf(ierr.Error_msg,
          "[DCT_Set_4d_Var_Mask] Invalid 4d_Var passed as 1st argument. Create DCT_4d_Var first by calling DCT_Create_4d_Var\n");
         return(ierr);
     }

     /* Check if the Variable can be modified, i.e. it is not added to a model */
     if (var->VarCommit == DCT_TRUE){
       ierr.Error_code  = DCT_INVALID_OPERATION;
       ierr.Error_msg   = (DCT_String) malloc((size_t)150);
       sprintf(ierr.Error_msg,
           "[DCT_Set_4d_Var_Mask] Invalid Operation. The DCT_4d_var %s is already commited\n",
           var->VarName);
       return(ierr);
     }

     if ((var->VarDim[0] <=0) || ( var->VarDim[1] <=0) || ( var->VarDim[2] <=0) ||
                                                      ( var->VarDim[3] <=0)) {
         ierr.Error_code   = DCT_INVALID_DIMENSIONS;
         ierr.Error_msg    = (DCT_String) malloc((size_t)130);
         sprintf(ierr.Error_msg,
   "[DCT_Set_4d_Var_Mask] 4d_Var <%s> Needs to have dimensions. Call DCT_Set_4d_Var_Dims before setting a mask\n", var->VarName);
         return(ierr);
     }
  }

  dims = var->VarDim;

  if (var->VarMask) free (var->VarMaskMap);
  var->VarMaskMap = (DCT_Boolean *)
          malloc(sizeof(DCT_Boolean)*(size_t)(dims[0]*dims[1]*dims[2]*dims[3]));
  DCTERROR( var->VarMaskMap == (DCT_Boolean *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

  ierr = DCT_Mask_Copy( var->VarMaskMap, mask, DCT_4D_VAR_TYPE, dims );
  if (ierr.Error_code != DCT_SUCCESS) return (ierr);
  var->VarMask          = DCT_TRUE;

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* -----------------------------------  END( DCT_Set_4d_Var_Mask ) */

}

/******************************************************************/
/*                DCT_Set_4d_Var_Freq_Consumption                 */
/*                                                                */
/*!     Sets the frequency at which a variable is to be consumed
        (received) by a model

     var:  DCT_4d_Var variable to be affected
    freq:  frequency (real scalar) of consumption, and the time
           units used are the same as in the model (ModTimeUnits)
*/
/******************************************************************/
DCT_Error DCT_Set_4d_Var_Freq_Consumption( DCT_4d_Var *var, const DCT_Scalar freq)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;

/* ---------------------  BEGIN( DCT_Set_4d_Var_Freq_Consumption ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
     "[DCT_Set_4d_Var_Freq_Consumption] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /*  Check the variable was created */
     if (var->VarTag!=DCT_TAG_UNDEF) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)115);
         sprintf(ierr.Error_msg,
          "[DCT_Set_4d_Var_Freq_Consumption] Invalid 4d_Var passed as 1st argument. Create DCT_4d_Var first by calling DCT_Create_4d_Var\n");
         return(ierr);
     }
     /* Check if the Variable can be modified, i.e. it is not added to a model */
     if (var->VarCommit == DCT_TRUE){
       ierr.Error_code  = DCT_INVALID_OPERATION;
       ierr.Error_msg   = (DCT_String) malloc((size_t)150);
       sprintf(ierr.Error_msg,
           "[DCT_Set_4d_Var_Freq_Consumption] Invalid Operation. The DCT_4d_var %s is already commited\n",
           var->VarName);
       return(ierr);
     }

     if (freq<=0) {
       ierr.Error_code     = DCT_INVALID_FREQ;
       ierr.Error_msg      = (DCT_String) malloc((size_t)90);
       sprintf(ierr.Error_msg,
       "[DCT_Set_4d_Var_Freq_Consumption], Invalid frequency for 4d_Var <%s> it must be > 0.0\n",
         var->VarName);
       return(ierr);
     }
     if (var->VarProduced==DCT_PRODUCE) {
       ierr.Error_code     = DCT_INVALID_ARGUMENT;
       ierr.Error_msg      = (DCT_String) malloc((size_t)85);
       sprintf(ierr.Error_msg,
       "[DCT_Set_4d_Var_Freq_Consumption], 4d_Var <%s> is declared for production\n",
         var->VarName);
       return(ierr);
     }
  }

  var->VarFrequency  = (DCT_Scalar) freq;

  ierr.Error_code    = DCT_SUCCESS;
  ierr.Error_msg     = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* -----------------------  END( DCT_Set_4d_Var_Freq_Consumption ) */

}

/******************************************************************/
/*                DCT_Set_4d_Var_Freq_Production                  */
/*                                                                */
/*!     Sets the frequency at which a variable is to be produced
        (sent) by a model

     var:  DCT_4d_Var variable to be affected
    freq:  frequency (real scalar) of production and the time
           units used are the same as in the model (ModTimeUnits)
*/
/******************************************************************/
DCT_Error DCT_Set_4d_Var_Freq_Production( DCT_4d_Var *var, const DCT_Scalar freq)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;

/* ----------------------  BEGIN( DCT_Set_4d_Var_Freq_Production ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
     "[DCT_Set_4d_Var_Freq_Production] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /*  Check the variable was created */
     if (var->VarTag!=DCT_TAG_UNDEF) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)115);
         sprintf(ierr.Error_msg,
          "[DCT_Set_4d_Var_Freq_Production] Invalid 4d_Var passed as 1st argument. Create DCT_4d_Var first by calling DCT_Create_4d_Var\n");
         return(ierr);
     }
     /* Check if the Variable can be modified, i.e. it is not added to a model */
     if (var->VarCommit == DCT_TRUE){
       ierr.Error_code  = DCT_INVALID_OPERATION;
       ierr.Error_msg   = (DCT_String) malloc((size_t)150);
       sprintf(ierr.Error_msg,
           "[DCT_Set_4d_Var_Freq_Production] Invalid Operation. The DCT_4d_var %s is already commited\n",
           var->VarName);
       return(ierr);
     }

     if (freq<=0) {
       ierr.Error_code     = DCT_INVALID_FREQ;
       ierr.Error_msg      = (DCT_String) malloc((size_t)90);
       sprintf(ierr.Error_msg,
       "[DCT_Set_4d_Var_Freq_Production], Invalid frequency for 4d_Var: %s, it must be > 0.0\n",
         var->VarName);
       return(ierr);
     }
     if (var->VarProduced==DCT_CONSUME) {
       ierr.Error_code     = DCT_INVALID_ARGUMENT;
       ierr.Error_msg      = (DCT_String) malloc((size_t)85);
       sprintf(ierr.Error_msg,
       "[DCT_Set_4d_Var_Freq_Production], 4d_Var <%s> is declared for consumption \n", var->VarName);
       return(ierr);
     }
  }

  var->VarFrequency  = (DCT_Scalar) freq;

  ierr.Error_code    = DCT_SUCCESS;
  ierr.Error_msg     = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* ------------------------  END( DCT_Set_4d_Var_Freq_Production ) */

}


/*********************************************************************/
/*                    DCT_Set_4d_Var_Labels                          */
/*!    Inserts labels to the DCT_4d_Vars.  We assume that the number of
       of labels in each direction (numlab1, numlab2,etc) is correct and
       there must be exactly this number of labels in each direction

       var:  Is the DCT_4d_Var variable to be affected.
    Type1:      DCT_Domain_Type value to establish if it is equally
                spaced or not equally spaced in direction 1,
                or general curvilinear
    Type2:      DCT_Domain_Type value to establish if it is equally
                spaced or not equally spaced in direction 2,
                or general curvilinear
    Type3:      DCT_Domain_Type value to establish if it is equally
                spaced or not equally spaced in direction 3,
                or general curvilinear
    Type4:      DCT_Domain_Type value to establish if it is equally
                spaced or not equally spaced in direction 3,
                or general curvilinear
    numlab1:    Number of labels of DCT_Field in the first direction
                which should match with dim1
    numlab2:    Number of labels of DCT_Field in the second direction
                which should match with dim2
    numlab3:    Number of labels of DCT_Field in the third direction
                which should match with dim3
    numlab4:    Number of labels of DCT_Field in the fourth direction
                which should match with dim4
    label1:     Labels of DCT_Field in the first direction or
                initial and final values
    label2:     Labels of DCT_Field in the second direction or
                initial and final values
    label3:     Labels of DCT_Field in the third direction or
                initial and final values
    label4:     Labels of DCT_Field in the fourth direction or
                initial and final values
*/
/*********************************************************************/
DCT_Error DCT_Set_4d_Var_Labels( DCT_4d_Var *var, const DCT_Domain_Type type1,
                             const DCT_Scalar *label1, const DCT_Integer numlab1,
                             const DCT_Domain_Type type2, const DCT_Scalar *label2,
                             const DCT_Integer numlab2, const DCT_Domain_Type type3,
                             const DCT_Scalar *label3, const DCT_Integer numlab3,
                             const DCT_Domain_Type type4, const DCT_Scalar *label4,
                             const DCT_Integer numlab4)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;

  DCT_Scalar            *l1, *l2, *l3, *l4;

  int                   i;

/* -------------------------------  BEGIN( DCT_Set_4d_Var_Labels ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
     "[DCT_Set_4d_Var_Labels] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /*  Check the variable was created */
     if (var->VarTag!=DCT_TAG_UNDEF) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)115);
         sprintf(ierr.Error_msg,
          "[DCT_Set_4d_Var_Labels] Invalid 4d_Var passed as 1st argument. Create DCT_4d_Var first by calling DCT_Create_4d_Var\n");
         return(ierr);
     }

     /* Check if the Variable can be modified, i.e. it is not added to a model */
     if (var->VarCommit == DCT_TRUE){
       ierr.Error_code  = DCT_INVALID_OPERATION;
       ierr.Error_msg   = (DCT_String) malloc((size_t)150);
       sprintf(ierr.Error_msg,
           "[DCT_Set_4d_Var_Labels] Invalid Operation. The DCT_4d_var %s is already commited\n",
           var->VarName);
       return(ierr);
     }

     /* Check the value of the number of label,
        which should be equal to the dimensions */
     if ((numlab1 <= 0) || (numlab2 <= 0) || (numlab3 <= 0) || (numlab4 <= 0)) {
         ierr.Error_code   = DCT_INVALID_LABELING;
         ierr.Error_msg    = (DCT_String) malloc((size_t)110);
         sprintf(ierr.Error_msg,
       "[DCT_Set_4d_Var_Labels] number of labels for 4d_Var <%s> should be positive integers [ %d, %d, %d, %d]\n",
                 var->VarName, numlab1, numlab2, numlab3, numlab4);
         return(ierr);
     }  else if ( ( (var->VarDim[0] != 0) && (var->VarDim[1] != 0) && (var->VarDim[2] != 0)
                                                               && (var->VarDim[3] != 0)  ) &&
                 ( (var->VarDim[0] != numlab1) || (var->VarDim[1] != numlab2) ||
                                (var->VarDim[2] != numlab3) || (var->VarDim[3] != numlab4) ) ) {
         ierr.Error_code   = DCT_INVALID_LABELING;
         ierr.Error_msg    = (DCT_String) malloc((size_t)110);
         sprintf(ierr.Error_msg,
       "[DCT_Set_4d_Var_Labels] number of labels for 4d_Var <%s> should be equal to the dimensions [ %d, %d, %d, %d]\n",
                 var->VarName, numlab1, numlab2, numlab3, numlab4);
         return(ierr);
     }

     if ( ( (type1 == DCT_GENERAL_CURV) || (type2 == DCT_GENERAL_CURV) ||
            (type3 == DCT_GENERAL_CURV) || (type4 == DCT_GENERAL_CURV) ) &&
          ( (type1 != DCT_GENERAL_CURV) || (type2 != DCT_GENERAL_CURV) ||
            (type3 != DCT_GENERAL_CURV) || (type4 != DCT_GENERAL_CURV) )    ) {
         ierr.Error_code   = DCT_INVALID_LABELING;
         ierr.Error_msg    = (DCT_String) malloc((size_t)150);
         sprintf(ierr.Error_msg,
       "[DCT_Set_4d_Var_Labels] For General Curvilinear domain, all direction must be of type DCT_GENERAL_CURV for 4d_Var <%s>\n",
                 var->VarName );
         return(ierr);
     }
  }

    /* If the dimensions are not set, are set */
  if ((var->VarDim[0] == 0) && (var->VarDim[1] == 0) &&
      (var->VarDim[2] == 0) && (var->VarDim[3] == 0)) {
      var->VarDim[0] = numlab1;
      var->VarDim[1] = numlab2;
      var->VarDim[2] = numlab3;
      var->VarDim[3] = numlab4;

  }

  var->VarDomType[0] = type1;
  var->VarDomType[1] = type2;
  var->VarDomType[2] = type3;
  var->VarDomType[3] = type4;


  if ( (type1 == DCT_GENERAL_CURV) || (type2 == DCT_GENERAL_CURV)
       || (type3 == DCT_GENERAL_CURV) || (type4 == DCT_GENERAL_CURV)) {
    int prod;
    prod = numlab1*numlab2*numlab3*numlab4;
    var->VarLabels[0] = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)prod);
      DCTERROR( var->VarLabels[0] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    var->VarLabels[1] = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)prod);
      DCTERROR( var->VarLabels[1] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    var->VarLabels[2] = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)prod);
      DCTERROR( var->VarLabels[2] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    var->VarLabels[3] = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)prod);
      DCTERROR( var->VarLabels[3] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

    l1 = var->VarLabels[0];
    l2 = var->VarLabels[1];
    l3 = var->VarLabels[2];
    l4 = var->VarLabels[4];
    for ( i=0; i < prod; i++ ) {
       l1[i] = (DCT_Scalar)label1[i];
       l2[i] = (DCT_Scalar)label2[i];
       l3[i] = (DCT_Scalar)label3[i];
       l4[i] = (DCT_Scalar)label4[i];
    }

  } else {
    if (type1 == DCT_RECTILINEAR) {

      var->VarLabels[0] =(DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)numlab1);
      DCTERROR( var->VarLabels[0] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
      for (i=0; i < numlab1; i++ )
        var->VarLabels[0][i] = (DCT_Scalar) label1[i];
    } else if (type1 == DCT_CARTESIAN) {
      if ( label1[1] <= label1[0] ) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)110);
         sprintf(ierr.Error_msg,
            "[DCT_Set_Field_Labels] initial value greater than final one Field <%s> in dimension 2\n",
             var->VarName);
         return(ierr);
      }

      l1 = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)3);
      DCTERROR( l1 == (DCT_Scalar *) NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
      l1[0] = (DCT_Scalar) label1[0];
      l1[1] = (DCT_Scalar) label1[1];
      l1[2] = (l1[1] - l1[0])/(DCT_Scalar)(numlab1-1);

      var->VarLabels[0] = l1;
    }

    if (type2 == DCT_RECTILINEAR) {

      var->VarLabels[1] =(DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)numlab2);
      DCTERROR( var->VarLabels[1] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
      for (i=0; i < numlab2; i++ )
        var->VarLabels[1][i] = (DCT_Scalar) label2[i];
    } else if (type2 == DCT_CARTESIAN) {
      if ( label2[1] <= label2[0] ) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)110);
         sprintf(ierr.Error_msg,
            "[DCT_Set_Field_Labels] initial value greater than final one Field <%s> in dimension 2\n",
             var->VarName);
         return(ierr);
      }

      l2 = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)3);
      DCTERROR( l2 == (DCT_Scalar *) NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
      l2[0] = (DCT_Scalar) label2[0];
      l2[1] = (DCT_Scalar) label2[1];
      l2[2] = (l2[1] - l2[0])/(DCT_Scalar)(numlab1-1);

      var->VarLabels[1] = l2;
    }

    if (type3 == DCT_RECTILINEAR) {

      var->VarLabels[2] =(DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)numlab3);
      DCTERROR( var->VarLabels[2] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
      for (i=0; i < numlab3; i++ )
        var->VarLabels[2][i] = (DCT_Scalar) label3[i];
    } else if (type3 == DCT_CARTESIAN) {
      if ( label3[1] <= label3[0] ) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)110);
         sprintf(ierr.Error_msg,
            "[DCT_Set_Field_Labels] initial value greater than final one Field <%s> in dimension 2\n",
             var->VarName);
         return(ierr);
      }

      l3 = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)3);
      DCTERROR( l3 == (DCT_Scalar *) NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
      l3[0] = (DCT_Scalar) label3[0];
      l3[1] = (DCT_Scalar) label3[1];
      l3[2] = (l3[1] - l3[0])/(DCT_Scalar)(numlab1-1);

      var->VarLabels[2] = l3;
    }

    if (type4 == DCT_RECTILINEAR) {

      var->VarLabels[3] =(DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)numlab4);
      DCTERROR( var->VarLabels[3] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
      for (i=0; i < numlab4; i++ )
        var->VarLabels[3][i] = (DCT_Scalar) label4[i];
    } else if (type4 == DCT_CARTESIAN) {
      if ( label4[1] <= label4[0] ) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)110);
         sprintf(ierr.Error_msg,
            "[DCT_Set_Field_Labels] initial value greater than final one Field <%s> in dimension 2\n",
             var->VarName);
         return(ierr);
      }

      l4 = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)3);
      DCTERROR( l4 == (DCT_Scalar *) NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
      l4[0] = (DCT_Scalar) label4[0];
      l4[1] = (DCT_Scalar) label4[1];
      l4[2] = (l4[1] - l4[0])/(DCT_Scalar)(numlab1-1);

      var->VarLabels[3] = l4;
    }
  }

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* ---------------------------------  END( DCT_Set_4d_Var_Labels ) */

}

/******************************************************************/
/*                          DCT_Set_Units                         */
/*                                                                */
/*!    Sets the DCT_4d_Vars Units in accordance to DCT_Units
       var:  Is the DCT_4d_Var variable to be affected.
         u:  Units of the DCT_4d_Var (i.e., CELSIUS, Km/h, etc)
*/
/******************************************************************/
DCT_Error DCT_Set_4d_Var_Units( DCT_4d_Var *var, const DCT_Units u)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error ierr;

/* --------------------------------  BEGIN( DCT_Set_4d_Var_Units ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  if (DCT_Step_Check != DCT_BEGIN_STATE){
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
     "[DCT_Set_4d_Var_Units] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  /*  Check the variable was created */
  if (var->VarTag!=DCT_TAG_UNDEF) {
      ierr.Error_code   = DCT_INVALID_ARGUMENT;
      ierr.Error_msg    = (DCT_String) malloc((size_t)115);
      sprintf(ierr.Error_msg,
       "[DCT_Set_4d_Var_Units] Invalid 4d_Var passed as 1st argument. Create DCT_4d_Var first by calling DCT_Create_4d_Var\n");
      return(ierr);
  }

  /* Check if the Variable can be modified, i.e. it is not added to a model */
  if (var->VarCommit == DCT_TRUE){
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)150);
    sprintf(ierr.Error_msg,
        "[DCT_Set_4d_Var_Units] Invalid Operation. The DCT_4d_var %s is already commited\n",
        var->VarName);
    return(ierr);
  }
  if ( (u < 1) || (u > DCT_DEFINED_UNITS) ) {
    ierr.Error_code  = DCT_INVALID_UNITS;
    ierr.Error_msg   = (DCT_String) malloc((size_t)80);
    sprintf(ierr.Error_msg,
    "[DCT_Set_Units], invalid unit type for var <%s>\n", var->VarName);
    return(ierr);
  }

  var->VarUnits       = u;

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* ----------------------------------  END( DCT_Set_4d_Var_Units ) */

}

/******************************************************************/
/*                     DCT_Set_4d_Var_Values                      */
/*                                                                */
/*!   This routine sets the pointer to the user data

*/
/******************************************************************/
DCT_Error DCT_Set_4d_Var_Values( DCT_4d_Var *var, const DCT_Data_Types type, const DCT_VoidPointer data)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;

/* -------------------------------  BEGIN( DCT_Set_4d_Var_Values ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  if (DCT_Step_Check != DCT_BEGIN_STATE){
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
     "[DCT_Set_4d_Var_Values] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  /*  Check the variable was created */
  if (var->VarTag!=DCT_TAG_UNDEF) {
      ierr.Error_code   = DCT_INVALID_ARGUMENT;
      ierr.Error_msg    = (DCT_String) malloc((size_t)115);
      sprintf(ierr.Error_msg,
       "[DCT_Set_4d_Var_Values] Invalid 4d_Var passed as 1st argument. Create DCT_4d_Var first by calling DCT_Create_4d_Var\n");
      return(ierr);
  }

  /* Check if the Variable can be modified, i.e. it is not added to a model */
  if (var->VarCommit == DCT_TRUE){
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)150);
    sprintf(ierr.Error_msg,
        "[DCT_Set_4d_Var_Values] Invalid Operation. The DCT_4d_var %s is already commited\n",
        var->VarName);
    return(ierr);
  }

  if ((type < 0) || (type >  DCT_DEFINED_DATA_TYPES)) {
      ierr.Error_code    = DCT_INVALID_DATA_TYPE;
      ierr.Error_msg     = (DCT_String) malloc((size_t)100);
      sprintf(ierr.Error_msg,
     "[DCT_Set_4d_Var_Values] The user data type entered for 4D_Val <%s> is not valid\n", var->VarName);
      return(ierr);
  }

  var->VarUserDataType   = type;
  var->VarValues         = data;

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* ---------------------------------  END( DCT_Set_4d_Var_Values ) */
}

/******************************************************************/
/*                          DCT_Destroy_4d_Var                    */
/*                                                                */
/******************************************************************/
DCT_Error DCT_Destroy_4d_Var( DCT_4d_Var *var)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error   ierr;

/* ----------------------------------  BEGIN( DCT_Destroy_4d_Var ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  if (DCT_Step_Check < DCT_BROKER_STATE){  /* All states before the broker starts to work */
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
        "[DCT_Destroy_4d_Var] Invalid Operation. The function DCT_EndRegistration() must be called  before to destroy a 4D var.\n");
    return(ierr);
  }

  /* Check if the DCT_3d_Var was created before to destroy it */
  if ( ( DCT_Step_Check == DCT_END_STATE ) && 
       ( (var->VarTag<DCT_TAG_UNDEF) || (var->VarTag>=DCT_MAX_VARS) ) ) {
      ierr.Error_code   = DCT_INVALID_ARGUMENT;
      ierr.Error_msg    = (DCT_String) malloc((size_t)120);
      sprintf(ierr.Error_msg,
       "[DCT_Destroy_4d_Var] Invalid 4d_Var passed as 1st argument. DCT_4d_Var was not created by calling DCT_Create_4d_Var\n");
      return(ierr);
  }
  free(var->VarName);
  var->VarName = (DCT_Name)NULL;

  if (var->VarDescription != (DCT_String)NULL)
     free(var->VarDescription);
  var->VarDescription = (DCT_String)NULL;

  var->VarDim[0]       = 0;
  var->VarDim[1]       = 0;
  var->VarDim[2]       = 0;
  var->VarDim[3]       = 0;

  if (var->VarLabels[0] != (DCT_Scalar *)NULL)
     free(var->VarLabels[0]);
  var->VarLabels[0] = (DCT_Scalar *)NULL;
  if (var->VarLabels[1] != (DCT_Scalar *)NULL)
     free(var->VarLabels[1]);
  var->VarLabels[1] = (DCT_Scalar *)NULL;
  if (var->VarLabels[2] != (DCT_Scalar *)NULL)
     free(var->VarLabels[2]);
  var->VarLabels[2] = (DCT_Scalar *)NULL;
  if (var->VarLabels[3] != (DCT_Scalar *)NULL)
     free(var->VarLabels[3]);
  var->VarLabels[3] = (DCT_Scalar *)NULL;

  var->VarDomType[0] = DCT_UNKNOWN_TYPE;
  var->VarDomType[1] = DCT_UNKNOWN_TYPE;
  var->VarDomType[2] = DCT_UNKNOWN_TYPE;
  var->VarDomType[3] = DCT_UNKNOWN_TYPE;

  var->VarDistType      = DCT_DIST_NULL;

  if (var->VarDistProcCounts != (DCT_Integer *)NULL)
     free(var->VarDistProcCounts);
  var->VarDistProcCounts = (DCT_Integer *)NULL;

  if (var->VarDistRank != (DCT_Rank *)NULL)
     free(var->VarDistRank);
  var->VarDistRank = (DCT_Rank *)NULL;

  var->VarProduced       = DCT_PRODUCE;
  var->VarTimeUnits      = DCT_TIME_UNKNOWN;
  var->VarFrequency      = DCT_TIME_UNSET;
  var->VarTimeIni        = DCT_TIME_UNSET;
  var->VarLastUpdate     = DCT_TIME_UNSET;

  var->VarMask           = DCT_FALSE;
  /*var->VarMaskMap        = (DCT_Boolean *)NULL;*/
  if (var->VarMaskMap != (DCT_Boolean *)NULL)
     free(var->VarMaskMap);
  var->VarMaskMap = (DCT_Boolean *)NULL;
  var->VarUnits          = DCT_UNIT_UNKNOWN;

  if ( var->VarFileName != (DCT_String)NULL )
     free ( var->VarFileName );
  var->VarFileName = (DCT_String)NULL;

  var->VarNumCpl        = 0;
  if ( var->VarCplIndx != (DCT_Integer *)NULL )
     free ( var->VarCplIndx );
  var->VarCplIndx = (DCT_Integer *)NULL;

  var->VarTag            = DCT_TAG_UNDEF;
  var->VarModel          = (DCT_Model *)NULL;
  var->VarValues         = (DCT_Scalar *)NULL; /* In this case, the assignment is
                                                  correct to keep the user data */

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* ------------------------------------  END( DCT_Destroy_4d_Var ) */

}
