/*******************************************************************/
/*                 Distributed Coupling Toolkit (DCT)              */
/*                                                                 */
/*!
   \file dct_coupler.c

   \brief Contains the functions implementation of the data structure
          DCT_Couple.

    This file contains the functions to handle the DCT data type
    structure DCT_Couple; which represents the coupling conception
    between a local DCT_Model together a number of \DCT_Vars with their
    remote DCT_Model and \DCT_Vars counterparts.

    \date Date of Creation: Oct 13, 2005.  

    \author Dany De Cecchis: dcecchis@gmail.com
    \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.

*/
/*******************************************************************/

/* -----------------------------------------------  Include Files  */
#include <stdio.h>
#include <string.h>
#include "dct.h"
/* --------------------------------------------  Global Variables  */
#include "dctsys.h"


/*******************************************************************/
/*                      DCT_Create_Couple                          */
/*                                                                 */
/*!    This routine when called by the user it initializes a
       global couple.  Internally the broker also calls this
       routine to create subdomain couple.

       \sa DCT_Destroy_Coupler

      \param[out]  cop Is a DCT_Couple coupler to be setup.
      \param[in]  name Internal name of the coupler (used during
                       coupling).
      \param[in]  desc Description of the coupler 120 char max.
      \param[in]  moda First of the DCT_Model to couple.
      \param[in]  modb Name of the second DCT_Model to couple.
      
      \return An error handler variable 

*/
/*******************************************************************/
DCT_Error DCT_Create_Couple( DCT_Couple *cop, const DCT_Name name,
                             const DCT_String desc, DCT_Model *moda,
                             const DCT_Name modb )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  int                  len, lenb;
  
//   DCT_Integer          numvar;
  
/* -----------------------------------  BEGIN( DCT_Create_Couple ) */

  /* Check if the the function DCT_BeginRegistration() was called (Value 1) */
  /* Or when the broker is calling the function during the registration (Value 2) */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
     "[DCT_Create_Couple] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }
  /** get names length **/
  len  = strlen(name);
  lenb = strlen(modb);
  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /* Name is a mandatory field to be filled */
     /*len = strlen(name); it was moved to avoid segmentation fault when a null argument is passed*/
     if ( (name == (DCT_Name)NULL) || (len = strlen(name) == 0) ) {
       ierr.Error_code  = DCT_INVALID_ARGUMENT;
       ierr.Error_msg   = (DCT_String) malloc((size_t)60);
       sprintf(ierr.Error_msg,
          "[DCT_Create_Couple] Invalid Name for Couple. A name should be given\n");
       return(ierr);
     }
   
     if ( (moda->ModName==(DCT_Name)NULL) || (modb == (DCT_Name)NULL) ||
            (strlen(moda->ModName) == 0) || (lenb == 0) ) {
       ierr.Error_code   = DCT_INVALID_MODEL_DEFINITION;
       ierr.Error_msg    = (DCT_String) malloc(115);
       sprintf(ierr.Error_msg,
         "[DCT_Create_Couple] Invalid Model passed as argument.\n");
       return(ierr);
     }
  }
  
  cop->CoupleName = (DCT_Name) malloc(sizeof(char)*(size_t)(len+1));
  DCTERROR( cop->CoupleName == (DCT_Name) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  strcpy(cop->CoupleName, name);
  
  /* The tag is set as negative (no tag) */
  cop->CoupleTag = DCT_TAG_UNDEF;
  
  
  if (desc != (DCT_String)NULL){
    len = strlen(desc);
    cop->CoupleDescription = (DCT_String)malloc(sizeof(char)*(size_t)(len+1));
    DCTERROR( cop->CoupleDescription == (DCT_String) NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    strcpy(cop->CoupleDescription, desc);
  } else cop->CoupleDescription = (DCT_String)NULL;

  cop->CoupleModelA = (DCT_Model *) moda;
  cop->CoupleModelB = (DCT_Model *)NULL;
  
  /* ModelB Hollow_Model structure is set */
  cop->CoupleHModelB = (DCT_Hollow_Model *) malloc( sizeof(DCT_Hollow_Model) );
  cop->CoupleHModelB->ModelName = (DCT_Name) malloc(sizeof(char)*(size_t)(lenb+1));
  DCTERROR( cop->CoupleHModelB->ModelName == (DCT_Name) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  strcpy(cop->CoupleHModelB->ModelName, modb);
  cop->CoupleHModelB->ModelLastUpdate = DCT_TIME_UNSET;
  cop->CoupleHModelB->ModelUpdateIntrv = DCT_TIME_UNSET;
  cop->CoupleHModelB->ModLeadRank = DCT_UNDEFINED_RANK;
  
  moda->ModCommit = DCT_TRUE;
  
  /* Pointer to DCT_Model to link the other model */
  cop->CoupleModelB = (DCT_Model *)NULL;
  
  cop->CoupleFirstVarCPL  =  -1;
  cop->CouplingTable      = (DCT_VoidPointer)NULL;

  /* As consumer SubDomain structure    */
  /* See DCT_SDConsume_Plan at dct.h */
  cop->CoupleRcv.Recvr = DCT_UNDEFINED_RANK;
  cop->CoupleRcv.IniVal = (DCT_Scalar *)NULL;
  cop->CoupleRcv.EndVal = (DCT_Scalar *)NULL;
  cop->CoupleRcv.GlbInd = (DCT_Integer *)NULL;
  cop->CoupleRcv.Npts   = (DCT_Integer *)NULL;
  
  cop->CoupleRcv.Nmsg = DCT_NO_MSG;
  cop->CoupleRcv.SDSlab = (DCT_SD_Consume *)NULL;

  cop->CoupleRcv.SndrNpts = (DCT_Integer *)NULL;
  cop->CoupleRcv.SndrNSD  = (DCT_Integer) 0;
  cop->CoupleRcv.SndrSDom = (DCT_Data_SubDom *)NULL;


  /* As producer SubDomain structure */
  /* See DCT_SDProduce_Plan at dctsys.h */
  cop->CoupleSnd.Nmsg = DCT_NO_MSG;
  cop->CoupleSnd.Sendr = DCT_UNDEFINED_RANK;
  cop->CoupleSnd.Msend = (DCT_SD_Produce *)NULL;

  /* Set Default Values for other Meta-Data in DCT_Couple  */
  cop->CoupleTotNumVars = 0;
  cop->CoupleNumVars = 0;
  cop->CoupleNumVars2 = 0;
   
  if ( DCT_Step_Check == DCT_BEGIN_STATE ) {  /* The DCT_Couple structure is registered
                                                  in the global list and counted        */
     if ( DCT_List_CHKAdd( &DCT_Reg_Couple, cop, DCT_COUPLE_TYPE ) != (DCT_List *)NULL ) {
       ierr.Error_code   = DCT_OBJECT_DUPLICATED;
       ierr.Error_msg    = (DCT_String) malloc(115);
       sprintf(ierr.Error_msg,
         "[DCT_Create_Couple] DCT_Couple name <%s> is already used.\n", cop->CoupleName );
       return(ierr);
     }
     DCT_LocalCouple++;
  }

  ierr.Error_code = DCT_SUCCESS;
  ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* -------------------------------------  END( DCT_Create_Couple ) */
}

/*******************************************************************/
/*                    DCT_Set_Coupling_Vars                        */
/*                                                                 */
/*!    Set or include the variables to be coupled as well as
       their frequencies of production or consumption.

      \param[in,out]  cop  Is a DCT_Couple coupler to be setup.
      \param[in]     var1  Pointer to the variable to be added
                           to the coupled.
      \param[in]     var2  Name to the corresponding variable to
                           be coupled.
      \param[in] var_type  Type of the variable to be added. It
                           must be one among DCT_Field, DCT_3d_Var
                           or DCT_4d_Var.
      \param[in] var_trans Type of transformation to use. It must
                           be defined in DCT_Data_Transformations.
      
      \return An error handler variable 

*/
/*******************************************************************/
DCT_Error DCT_Set_Coupling_Vars( DCT_Couple *cop, const DCT_VoidPointer var1,
                        const DCT_Name var2, const DCT_Object_Types var_type,
                        const DCT_Data_Transformation var_trans )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error          ierr;
  int                len, commit;
  int               *cpln;  
  DCT_ProdCons       prod;
  
  DCT_Model         *model;
  
  DCT_Field         *field;
  DCT_3d_Var        *var3d;
  DCT_4d_Var        *var4d;

  DCT_Hollow_Var    *HCVar2 = (DCT_Hollow_Var *)NULL;
  DCT_CPL_Node      *cplnode = (DCT_CPL_Node *)NULL;

/* -------------------------------  BEGIN( DCT_Set_Coupling_Vars ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  if (DCT_Step_Check != DCT_BEGIN_STATE){
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
     "[DCT_Set_Coupling_Vars] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }
  /* Name is a mandatory field to be filled */
  if (cop->CoupleName==(DCT_Name)NULL) {
    ierr.Error_code   = DCT_INVALID_ARGUMENT;
    ierr.Error_msg    = (DCT_String) malloc(115);
    sprintf(ierr.Error_msg,
      "[DCT_Set_Coupling_Vars] Invalid Couple passed as 1st argument. Create DCT_Couple first by calling DCT_Create_Couple\n");
    return(ierr);
  }

  if ((var_type<=DCT_TYPE_UNKNOWN) || (var_type> DCT_DEFINED_VAR_OBJECT)) {
    ierr.Error_code     = DCT_INVALID_ARGUMENT;
    ierr.Error_msg      = (DCT_String) malloc(90);
    sprintf(ierr.Error_msg,
    "[DCT_Set_Coupling_Vars], Invalid variable type for Couple <%s>\n",
                        cop->CoupleName);
    return(ierr);
  }

  switch (var_type)
  {
    case DCT_FIELD_TYPE:
      field  = (DCT_Field *) var1;
      prod   = field->VarProduced;
      model  = field->VarModel;
      commit = (field->VarCommit == DCT_TRUE);
      cpln   = &(field->VarNumCpl);
      break;
    case DCT_3D_VAR_TYPE:
      var3d  = (DCT_3d_Var *) var1;
      prod   = var3d->VarProduced;
      model  = var3d->VarModel;
      commit = (var3d->VarCommit == DCT_TRUE);
      cpln   = &(var3d->VarNumCpl);
      break;
    case DCT_4D_VAR_TYPE:
      var4d  = (DCT_4d_Var *) var1;
      prod   = var4d->VarProduced;
      model  = var4d->VarModel;
      commit = (var4d->VarCommit == DCT_TRUE);
      cpln   = &(var4d->VarNumCpl);
      break;
    default:
      ierr.Error_code     = DCT_INVALID_ARGUMENT;
      ierr.Error_msg      = (DCT_String) malloc(90);
      sprintf(ierr.Error_msg,
          "[DCT_Set_Coupling_Vars], Invalid variable type to coupling a var in Couple <%s>\n",
          cop->CoupleName);
      return(ierr);
  }
  if (!commit){
    ierr.Error_code     = DCT_FAILED_MALLOC;
    ierr.Error_msg      = (DCT_String) malloc(150);
    sprintf(ierr.Error_msg,
      "[DCT_Set_Coupling_Vars], In Couple <%s>, Variable must be commited (add to a model) first\n",
      cop->CoupleName);
    return(ierr);
  }
  if (model != cop->CoupleModelA){
    ierr.Error_code     = DCT_INVALID_ARGUMENT;
    ierr.Error_msg      = (DCT_String) malloc(150);
    sprintf(ierr.Error_msg,
      "[DCT_Set_Coupling_Vars], In Couple <%s>, Variable does not belong to model <%s>\n",
      cop->CoupleName, cop->CoupleModelA->ModName);
    return(ierr);
  }
  /* The data transformation is only checked when is consumer */
  if ( prod == DCT_CONSUME ) {
    if ( ( var_trans < DCT_NO_INTERPOLATION ) ||
         ( var_trans > DCT_DATA_TRANSFORMATION ) ) {
       ierr.Error_code     = DCT_INVALID_DATA_TRANS;
       ierr.Error_msg      = (DCT_String) malloc(150);
       sprintf(ierr.Error_msg,
         "[DCT_Set_Coupling_Vars], In Couple <%s>, Invalid data transformation type\n",
         cop->CoupleName);
       return(ierr);
    }
    /* Check that it was involved in only one coupling */
    if ( *cpln > 0) {
       ierr.Error_code     = DCT_INVALID_NUMB_CPLS;
       ierr.Error_msg      = (DCT_String) malloc(150);
       sprintf(ierr.Error_msg,
       "[DCT_Set_Coupling_Vars], In Couple <%s>, A consumer var can only be coupled once\n",
          cop->CoupleName);
       return(ierr);    
    }
  }

  /* The Hollow variable 2 is set */
  HCVar2 = (DCT_Hollow_Var *) malloc(sizeof(DCT_Hollow_Var));
  DCTERROR( HCVar2 == (DCT_Hollow_Var *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  len=strlen(var2);
  HCVar2->VarName = (DCT_Name) malloc(sizeof(char)*(size_t)(len+1));
  DCTERROR( HCVar2->VarName == (DCT_Name)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  /* Plug the variable 2 (remote) to the couple */
  strcpy( HCVar2->VarName, var2 );
  HCVar2->VarLastUpdate = DCT_TIME_UNSET;

  /* The temporaryly couple var node is created */
  cplnode = (DCT_CPL_Node *) malloc(sizeof(DCT_CPL_Node));
  DCTERROR( cplnode == (DCT_CPL_Node *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  
  cplnode->CoupleVars1 = var1;
  cplnode->CoupleVars2 = HCVar2;
  cplnode->CVarType = (DCT_Object_Types) var_type;
  cplnode->CVarTrans = (DCT_Data_Transformation)
                       ( prod == DCT_CONSUME ? var_trans : DCT_NO_INTERPOLATION);
  
  cop->CouplingTable = (DCT_VoidPointer)
                 DCT_List_Add ( (DCT_List **)&(cop->CouplingTable), cplnode, DCT_CPL_NODE_TYPE);
  
  (cop->CoupleTotNumVars)++; /* The number of variables in this couple is counted */
  cop->CoupleNumVars = cop->CoupleTotNumVars;
  (*cpln)++;     /* Number of times the variable is involved in a couple is counted */
  
  DCT_CPLVars++; /* The total number of variables coupling in the process is counted */
  
  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* ---------------------------------  END( DCT_Set_Coupling_Vars ) */
}

/*******************************************************************/
/*                          DCT_Destroy_3d_Var                     */
/*                                                                 */
/*!   Destroy (free) all the data used to represent the couple
      variable.

       \param[in,out]  cop  Is a DCT_Couple coupler to be setup.
 
       \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Destroy_Couple( DCT_Couple *cop)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error      ierr;
  
  DCT_CPL_Node  *cplnode;
  DCT_List      *current;
  
  DCT_SD_Produce      *ModSDMsend;
  DCT_SD_Consume      *ModSDMrecv;
  DCT_Data_SubDom     *SndrSDom;
  DCT_SDProduce_Plan  *CSnd;
  DCT_SDConsume_Plan  *CRcv;

  int            nmsg;
  register int   ii;

/* ----------------------------------  BEGIN( DCT_Destroy_Couple ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  if (DCT_Step_Check < DCT_BEGIN_STATE) {  /* All states before the broker starts to work */
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
      "[DCT_Destroy_Couple] Invalid Operation. The function DCT_EndRegistration() must be called before to destroy a Couple.\n");
    return(ierr);
  }
  /* Name is a mandatory field to be filled */
  if ( ( DCT_Step_Check == DCT_END_STATE ) && (cop->CoupleName==(DCT_Name)NULL) ) {
    ierr.Error_code   = DCT_INVALID_ARGUMENT;
    ierr.Error_msg    = (DCT_String) malloc(115);
    sprintf(ierr.Error_msg,
      "[DCT_Destroy_Couple] Invalid Couple passed as 1st argument. Create DCT_Couple first by calling DCT_Create_Couple\n");
    return(ierr);
  }
  free(cop->CoupleName);
  //cop->CoupleName = (DCT_Name)NULL;
  
  //cop->CoupleTag = DCT_TAG_UNDEF;

  if (cop->CoupleDescription  != (DCT_String)NULL) {
    free(cop->CoupleDescription);
    cop->CoupleDescription = (DCT_String)NULL;
  }

  /* How those two variables are not allocated in a Couple function
     the operation is only unpluging the variables to the pointers
     but they are uncommit in order to can destroy them */
  if (DCT_Step_Check != DCT_BROKER_STATE) cop->CoupleModelA->ModCommit = DCT_FALSE;
  
  cop->CoupleModelA         = (DCT_Model *)NULL;
  cop->CoupleModelB         = (DCT_Model *)NULL;

  /* ModelB Hollow_Model structure is unset */
  if ( cop->CoupleHModelB != (DCT_Hollow_Model *)NULL ) {
     if ( cop->CoupleHModelB->ModelName != (DCT_Name)NULL)
        free ( cop->CoupleHModelB->ModelName );
     free ( cop->CoupleHModelB );
     cop->CoupleHModelB = (DCT_Hollow_Model *)NULL;
  }


   /* The list of the couple var list is cleared */
   if ( cop->CouplingTable != NULL ) {
      /* The nodes are freed first */
      current = (DCT_List *) cop->CouplingTable;
      while (current != (DCT_List *)NULL) {
         DCT_List_Ext ( &current, (DCT_VoidPointer *) &cplnode );
         free ( cplnode );
      }
      /* The List is deleted */
      DCT_List_Clear ( (DCT_List **)&(cop->CouplingTable) );
   }

  cop->CoupleFirstVarCPL  =  -1;
  cop->CouplingTable      = (DCT_VoidPointer)NULL;

  /* How these structures; cop->CoupleRcv and cop->CoupleSnd; are not used
     after the registration process, they are freed in function DCT_Local_Data_Delete,
     defined in dct_broker.c. They are also declared here for completeness in 
     deallocating all parts of a DCT_Couple structure */
  CRcv = &(cop->CoupleRcv);
  CRcv->Recvr = DCT_UNDEFINED_RANK;
  if ( CRcv->IniVal != (DCT_Scalar *)NULL ) free( CRcv->IniVal );
  if ( CRcv->EndVal != (DCT_Scalar *)NULL ) free( CRcv->EndVal );
  if ( CRcv->GlbInd != (DCT_Integer *)NULL ) free( CRcv->GlbInd );
  if ( CRcv->Npts   != (DCT_Integer *)NULL ) free( CRcv->Npts );
  if ( CRcv->SDSlab != (DCT_SD_Consume *)NULL ) {
     nmsg = CRcv->Nmsg;
     ModSDMrecv = CRcv->SDSlab;
     for ( ii=0; ii < nmsg; ii++ ) {
        free( ModSDMrecv->IniPos );
        free( ModSDMrecv->Length );
        ModSDMrecv++;
     }
     free( CRcv->SDSlab );
     CRcv->SDSlab = (DCT_SD_Consume *)NULL;
  }
  CRcv->Nmsg = DCT_NO_MSG;
  if ( CRcv->SndrNpts != (DCT_Integer *)NULL ) free( CRcv->SndrNpts );
  if ( CRcv->SndrSDom != (DCT_Data_SubDom *)NULL ) {
     nmsg = CRcv->SndrNSD;
     SndrSDom = CRcv->SndrSDom;
     for ( ii=0; ii < nmsg; ii++ ) {
        free ( SndrSDom->IniPos );
        free ( SndrSDom->EndPos );
        SndrSDom++;
     }
     free ( CRcv->SndrSDom );
     CRcv->SndrSDom = (DCT_Data_SubDom *)NULL;
  }

  CSnd = &(cop->CoupleSnd);
  CSnd->Sendr = DCT_UNDEFINED_RANK;
  if ( CSnd->Msend != (DCT_SD_Produce *)NULL ) {
      nmsg = CSnd->Nmsg;
      ModSDMsend = CSnd->Msend;
      for ( ii=0; ii < nmsg; ii++ ) {
         free ( ModSDMsend->IniVal );
         free ( ModSDMsend->EndVal );
         free ( ModSDMsend->Length );
         ModSDMsend++;
      }
      free( CSnd->Msend );
      CSnd->Msend = (DCT_SD_Produce *)NULL;
  }
  CSnd->Nmsg = DCT_NO_MSG;

  cop->CoupleTotNumVars = 0;
  cop->CoupleNumVars = 0;
  cop->CoupleNumVars2 = 0;
  
  cop->CoupleFirstVarCPL  =  -1;
  /* After the Registration Phase the list constructed here it was removed */
  cop->CouplingTable      = (DCT_VoidPointer)NULL;


  
  if ( DCT_Step_Check == DCT_END_STATE )  /* The DCT_Couple structure is decounted   */
     DCT_LocalCouple--;
  
  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* ------------------------------------  END( DCT_Destroy_Couple ) */

}

