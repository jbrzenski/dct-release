/**************************************************************/
/*              Distributed Coupling Toolkit (DCT)            */
/*!
   \file dct_broker.c

   \brief Contains the implementation of the functions involved
          during the DCT Registration Phase (begin and end).

    This file contains the functions to handle the required actions
    for the begin and end stages in the DCT Registration Phase.
    Here are implemented the actions of the functions
    DCT_BeginRegistration and DCT_EndRegistration.

    \attention Communication details are not included in here,
    because they are isolated in comm's files.

    \warning In this file is declared the actual Global Variables
             to control the DCT System, that are defined in the
             fiel \c dctsys.h.
    
    \date Date of Creation: Aug 04, 2008.

    \author Dany De Cecchis: dcecchis@gmail.com
    \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.
 */
/**************************************************************/

/* -----------------------------------------------  Include Files  */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

#include "dct.h"
/* --------------------------------------------  Global Variables  */
/*! Directive to define where the Global DCT system wide variables are declared */
#define  DCT_GLB_VARS
#include "dctsys.h"
#undef   DCT_GLB_VARS
#include "dct_comm.h"

/*******************************************************************/
/*                        DCT_BrokerArrange                        */
/*                                                                 */
/*!      This routine calculates the preliminar subdomain
         communication between modA and modB DCT_Model, and
         is performed by the broker. The DCT_Data_SubDom
         structure is pointed by the counter part model in
         Mod[AB]Rcv.SndrSDom fields as well as the total number
         of points in Mod[AB]Rcv.SndrNpts.

         \param[in]     modA  Pointer to DCT_Model model A.
         \param[in]     modB  Pointer to DCT_Model model B.
         \param[out] ModASen  An array of the structure
                              describing the SD preliminary
                              communication with modA considered
                              as producer.
         \param[out] nModASD  Total Number of subdomains in
                              Model A involved in the coupling
                              interface.
         \param[out] ModARcv  An array of the structure
                              describing the SD preliminary
                              communication with modA considered
                              as consumer.
         \param[out] ModBSen  An array of the structure
                              describing the SD preliminary
                              communication with modA considered
                              as producer.
         \param[out] nModBSD  Total Number of subdomains in
                              Model B involved in the coupling
                              interface.
         \param[out] ModBRcv  An array of the structure
                              describing the SD preliminary
                              communication with modB considered
                              as consumer.

         \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_BrokerArrange(const DCT_Model *modA, const DCT_Model *modB,
                          DCT_SDProduce_Plan **ModASen, int *nModASD,
                          DCT_SDConsume_Plan **ModARcv,
                          DCT_SDProduce_Plan **ModBSen, int *nModBSD,
                          DCT_SDConsume_Plan **ModBRcv )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;

  DCT_Model_Dom        *PtrDomA, *PtrDomB;
  DCT_Integer           dim;
  DCT_Integer           inipos, endpos, iniind ;
  DCT_Scalar           *DomA[2];
  DCT_Scalar           *DomB[2];
  DCT_Scalar            IniVal[4], EndVal[4];
  DCT_Scalar            ModAIniVal[4], ModAEndVal[4];
  DCT_Scalar            inival, coniniv, endval, conendv; //, hA[4], hB[4];
  DCT_SDProduce_Plan   *SDModASen;
  DCT_SDProduce_Plan   *SDModBSen;
  DCT_SDProduce_Plan   *SDauxSen;
  DCT_SDConsume_Plan   *ModelARcv;
  DCT_SDConsume_Plan   *ModelBRcv;
  DCT_Rank            **ProdNeigh; 

  DCT_SD_Consume       *AuxSDSlab;
  DCT_Scalar          **ModAValues;
  DCT_Scalar          **ModBValues;
  DCT_Data_SubDom      *ModASubDom;
  DCT_Data_SubDom      *ModBSubDom;
  DCT_Domain_Type      *ModADomType;
  DCT_Domain_Type      *ModBDomType;
  DCT_Integer          *ModANumPts;
  DCT_Integer          *ModBNumPts;
  DCT_Integer          *ModAEndPos;
  DCT_Integer          *ModBEndPos;
  DCT_Distribution      ModAParLayout;
  DCT_Distribution      ModBParLayout;
  DCT_Integer          *ModALayoutDim;
  DCT_Integer          *ModBLayoutDim;

  DCT_Integer          *ModIniPos, *ModEndPos;
  DCT_Integer          *ModSDLength, *ModSDGlbInd, *ModSDNpts, *ModSDIniPos;
  DCT_Scalar           *ModSDIniVal, *ModSDEndVal;
  DCT_Scalar           *ModSD2IniVal, *ModSD2EndVal;

  DCT_SD_Produce       *ModSDMsend, *AuxSDsend;
  DCT_SD_Consume       *ModSDMrecv, *AuxSDrecv;

  DCT_SDConsume_Plan   *ModSDMrecvPlan;
  DCT_SDProduce_Plan   *ModSDMsendPlan, *AuxSDsendPlan;

  int                  *indices;
  int                   NSDomA, NSDomB;
  int                   ind, dimflag, maxsd;
  register int          i, j, k, l;


/* -----------------------------------  BEGIN( DCT_BrokerArrange ) */

  /* Getting the model a domain info */
  PtrDomA = modA->ModDomain;
  ModAValues = PtrDomA->ModValues;
  ModASubDom = PtrDomA->ModSubDom;
  ModADomType = PtrDomA->ModDomType;
  ModANumPts = PtrDomA->ModNumPts;
  ModAParLayout = PtrDomA->ModSubDomParLayout;
  ModALayoutDim = PtrDomA->ModSubDomLayoutDim;
  dim = PtrDomA->ModDomDim;
  
  /* Getting the model b domain info */
  PtrDomB = modB->ModDomain;
  ModBValues = PtrDomB->ModValues;
  ModBSubDom = PtrDomB->ModSubDom;
  ModBDomType = PtrDomB->ModDomType;
  ModBNumPts = PtrDomB->ModNumPts;
  ModBParLayout = PtrDomB->ModSubDomParLayout;
  ModBLayoutDim = PtrDomB->ModSubDomLayoutDim;

  /* Structures to store the model domain limits */
  DomA[0] = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
  DCTERROR( DomA[0] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  DomA[1] = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
  DCTERROR( DomA[1] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  ModAEndPos = (DCT_Integer *) malloc(sizeof(DCT_Integer)*(size_t)dim);
  DCTERROR( ModAEndPos == (DCT_Integer *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

  DomB[0] = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
  DCTERROR( DomB[0] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  DomB[1] = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
  DCTERROR( DomB[1] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  ModBEndPos = (DCT_Integer *) malloc(sizeof(DCT_Integer)*(size_t)dim);
  DCTERROR( ModBEndPos == (DCT_Integer *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );


  /* Checking layout in model A and getting the number of subdomains */
  switch( ModAParLayout )
  {
    case DCT_DIST_SEQUENTIAL:
      NSDomA = 1;
      break;
    case DCT_DIST_RECTANGULAR:
      NSDomA = *(ModALayoutDim) * *(ModALayoutDim+1);
      break;
    case DCT_DIST_3D_RECTANGULAR:
      NSDomA = *(ModALayoutDim) * *(ModALayoutDim+1) *
               *(ModALayoutDim+2);
    case DCT_DIST_NULL:
      ierr.Error_code    = DCT_INVALID_ARGUMENT;
      ierr.Error_msg     = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
        "[DCT_BrokerArrange] The distribution type entered for Model <%s> is not valid\n",
              modA->ModName);
      return(ierr);
  }

  /* Checking layout in model B and getting the number of subdomains */
  switch( ModBParLayout )
  {
    case DCT_DIST_SEQUENTIAL:
      NSDomB = 1;
      break;
    case DCT_DIST_RECTANGULAR:
      NSDomB = *(ModBLayoutDim) * *(ModBLayoutDim+1);
      break;
    case DCT_DIST_3D_RECTANGULAR:
      NSDomB = *(ModBLayoutDim) * *(ModBLayoutDim+1) *
               *(ModBLayoutDim+2);
    case DCT_DIST_NULL:
      ierr.Error_code    = DCT_INVALID_ARGUMENT;
      ierr.Error_msg     = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
        "[DCT_BrokerArrange] The distribution type entered for Model <%s> is not valid\n",
              modB->ModName);
      return(ierr);
  }

  /* Get domain limits in model Model A */
  switch(dim) {
    case 4:
      DomA[0][3] = ModAValues[3][0];
      ModAEndPos[3] = ModANumPts[3]-1;
      if (ModADomType[3]== DCT_RECTILINEAR) {
        DomA[1][3] = ModAValues[3][ ModAEndPos[3] ];
      }
      else {
        DomA[1][3] = ModAValues[3][1];
        // hA[3] = (DomA[1][3] - DomA[0][3])/ (DCT_Scalar)( ModAEndPos[3] );
      }
    case 3:
      DomA[0][2] = ModAValues[2][0];
      ModAEndPos[2] = ModANumPts[2]-1;
      if (ModADomType[2]== DCT_RECTILINEAR) {
        DomA[1][2] = ModAValues[2][ ModAEndPos[2] ];
      }
      else {
        DomA[1][2] = ModAValues[2][1];
        // hA[2] = (DomA[1][2] - DomA[0][2])/ (DCT_Scalar)( ModAEndPos[2] );
      }
    case 2:
      DomA[0][1] = ModAValues[1][0];
      ModAEndPos[1] = ModANumPts[1]-1;
      if (ModADomType[1]== DCT_RECTILINEAR) {
        DomA[1][1] = ModAValues[1][ ModAEndPos[1] ];
      }
      else {
        DomA[1][1] = ModAValues[1][1];
        // hA[1] = (DomA[1][1] - DomA[0][1])/ (DCT_Scalar)( ModAEndPos[1] );
      }
      /* ---------------------- DIM 1 -------------------- */
      DomA[0][0] = ModAValues[0][0];
      ModAEndPos[0] = ModANumPts[0]-1;
      if (ModADomType[0]== DCT_RECTILINEAR) {
        DomA[1][0] = ModAValues[0][ ModAEndPos[0] ];
      }
      else {
        DomA[1][0] = ModAValues[0][1];
        // hA[0] = (DomA[1][0] - DomA[0][0])/ (DCT_Scalar)( ModAEndPos[0] );
      }
  }


  /* Get domain limits in model Model B */
  switch(dim) {
    case 4:
      DomB[0][3] = ModBValues[3][0];
      ModBEndPos[3] = ModBNumPts[3]-1;
      if (ModBDomType[3]== DCT_RECTILINEAR) {
        DomB[1][3] = ModBValues[3][ ModBEndPos[3] ];
      }
      else {
        DomB[1][3] = ModBValues[3][1];
        // hB[3] = (DomB[1][3] - DomB[0][3])/ (DCT_Scalar)( ModBEndPos[3] );
      }
    case 3:
      DomB[0][2] = ModBValues[2][0];
      ModBEndPos[2] = ModBNumPts[2]-1;
      if (ModBDomType[2]== DCT_RECTILINEAR) {
        DomB[1][2] = ModBValues[2][ ModBEndPos[2] ];
      }
      else {
        DomB[1][2] = ModBValues[2][1];
        // hB[2] = (DomB[1][2] - DomB[0][2])/ (DCT_Scalar)( ModBEndPos[2] );
      }
    case 2:
      DomB[0][1] = ModBValues[1][0];
      ModBEndPos[1] = ModBNumPts[1]-1;
      if (ModBDomType[1]== DCT_RECTILINEAR) {
        DomB[1][1] = ModBValues[1][ ModBEndPos[1] ];
      }
      else {
        DomB[1][1] = ModBValues[1][1];
        // hB[1] = (DomB[1][1] - DomB[0][1])/ (DCT_Scalar)( ModBEndPos[1] );
      }
      /* ---------------------- DIM 1 -------------------- */
      DomB[0][0] = ModBValues[0][0];
      ModBEndPos[0] = ModBNumPts[0]-1;
      if (ModBDomType[0]== DCT_RECTILINEAR) {
        DomB[1][0] = ModBValues[0][ ModBEndPos[0] ];
      }
      else {
        DomB[1][0] = ModBValues[0][1];
        // hB[0] = (DomB[1][0] - DomB[0][0])/ (DCT_Scalar)( ModBEndPos[0] );
      }
  }

  /*****************************************************************************/
  /********** Communication  Model A (producer) to Model B (consumer) **********/

  /* Allocating the DCT_SDConsume_Plan ModelBRcv structure */
  ModelBRcv = (DCT_SDConsume_Plan *) malloc( sizeof(DCT_SDConsume_Plan)*(size_t)NSDomB );
  DCTERROR( ModelBRcv == (DCT_SDConsume_Plan *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  for(i=0; i < NSDomB; i++ ){
    ModSDMrecvPlan = ModelBRcv+i;

    ModSDIniVal = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
    DCTERROR( ModSDIniVal == (DCT_Scalar *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    ModSDMrecvPlan->IniVal = ModSDIniVal;

    ModSDEndVal = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
    DCTERROR( ModSDEndVal == (DCT_Scalar *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    ModSDMrecvPlan->EndVal = ModSDEndVal;

    ModSDGlbInd = (DCT_Integer *) malloc(sizeof(DCT_Integer)*(size_t)dim);
    DCTERROR( ModSDGlbInd == (DCT_Integer *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    ModSDMrecvPlan->GlbInd = ModSDGlbInd;

    ModSDNpts = (DCT_Integer *) malloc(sizeof(DCT_Integer)*(size_t)dim);
    DCTERROR( ModSDNpts == (DCT_Integer *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    ModSDMrecvPlan->Npts = ModSDNpts;

  }

  /* The maximun to avoid multiple allocation */
  maxsd = ( NSDomA >= NSDomB ? NSDomA: NSDomB ); 
  AuxSDSlab = (DCT_SD_Consume *) malloc(sizeof(DCT_SD_Consume)*(size_t)maxsd);
  DCTERROR( AuxSDSlab == (DCT_SD_Consume *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  for ( i=0; i < maxsd; i++ ) {
     ModSDMrecv = AuxSDSlab+i;

     ModSDIniPos = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t)dim);
     DCTERROR( ModSDIniPos == (DCT_Integer *)NULL,
               "Memory Allocation Failed", ierr.Error_code  =  DCT_FAILED_MALLOC;
               return(ierr) );
     ModSDMrecv->IniPos = ModSDIniPos;

     ModSDLength = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t)dim);
     DCTERROR( ModSDLength == (DCT_Integer *)NULL,
               "Memory Allocation Failed", ierr.Error_code  =  DCT_FAILED_MALLOC;
               return(ierr) );
     ModSDMrecv->Length = ModSDLength;

  }

  /** Allocating the neighbor to avoid multiple allocations **/
  ProdNeigh = (DCT_Rank **) malloc ( sizeof(DCT_Rank *) * (size_t)maxsd );
  DCTERROR( ProdNeigh == (DCT_Rank **)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  for ( i=0; i < maxsd; i++ ) {
     ProdNeigh[i] = (DCT_Rank *) malloc(sizeof(DCT_Rank)*(size_t)maxsd);
     DCTERROR( ProdNeigh[i] == (DCT_Rank *)NULL, "Memory Allocation Failed",
                  ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  }

  /** Array of indices in the producer subdmain **/
  indices = (int *) malloc(sizeof(int)*(size_t)maxsd);
  DCTERROR( indices == (int *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

  /* Allocating the DCT_SDProduce_Plan SDauxSen structure to avoid multiple allocation */
  SDauxSen = (DCT_SDProduce_Plan *) malloc( sizeof(DCT_SDProduce_Plan)*(size_t)maxsd );
  DCTERROR( SDauxSen == (DCT_SDProduce_Plan *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  for ( i=0; i < maxsd; i++ ) {
     ModSDMsendPlan = SDauxSen+i;
     ModSDMsendPlan->Nmsg = 0;
     ModSDMsendPlan->RcvrNSD = 0;
     ModSDMsend = (DCT_SD_Produce *) malloc( sizeof(DCT_SD_Produce)*(size_t)maxsd );
     DCTERROR( ModSDMsend == (DCT_SD_Produce *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
     ModSDMsendPlan->Msend = ModSDMsend;

     for ( j=0; j < maxsd; j++ ) {
        ModSDMsend = ModSDMsendPlan->Msend+j;

        ModSDIniVal = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)dim);
        DCTERROR( ModSDIniVal == (DCT_Scalar *)NULL,
                  "Memory Allocation Failed", ierr.Error_code  =  DCT_FAILED_MALLOC;
                  return(ierr) );
        ModSDMsend->IniVal = ModSDIniVal;

        ModSDEndVal = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)dim);
        DCTERROR( ModSDEndVal == (DCT_Scalar *)NULL,
                  "Memory Allocation Failed", ierr.Error_code  =  DCT_FAILED_MALLOC;
                  return(ierr) );
        ModSDMsend->EndVal = ModSDEndVal;

        ModSDLength = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t)dim);
        DCTERROR( ModSDLength == (DCT_Integer *)NULL,
                  "Memory Allocation Failed", ierr.Error_code  =  DCT_FAILED_MALLOC;
                  return(ierr) );
        ModSDMsend->Length = ModSDLength;

     }
     
  }
  
  /********** Taking the producer domain region (Producer resolution) ***********/
  for (i=0; i < NSDomB; i++ ) {
/*************************************************************************************/
/*************************************************************************************/
    /** The boundary values of the subdomain is extracted **/
    DCT_Return_SDBoundaryVals ( dim, (ModBSubDom+i), ModBDomType, ModBEndPos, ModBValues,
                                IniVal, EndVal );
    ModSDGlbInd = ModelBRcv[ i ].GlbInd;
    ModSDIniVal = ModelBRcv[ i ].IniVal;
    ModSDEndVal = ModelBRcv[ i ].EndVal;
    ModSDNpts = ModelBRcv[ i ].Npts;
    dimflag = 0;
    switch(dim) {
      case 4:
         inival = IniVal[3];
         endval = EndVal[3];

         /* Lower bound is the maximum of lower bounds */
         if (inival <= DomA[0][3]) {
            if ( endval < DomA[0][3] ) break; /* the subdomain is out of area */
            ModSDIniVal[3] = DomA[0][3];
            iniind = 0; /* Register the initial index in model a */
         }
         else{
            if (inival > DomA[1][3]) break;   /* the subdomain is out of area */
            iniind = DCT_SearchIndex( ModAValues[3], ModANumPts[3], ModADomType[3], // hA[3],
                                      0, ModAEndPos[3], inival, -1, (ModSDIniVal+3) );
         }
         ModSDGlbInd[3] = iniind; 

         /* Upper bound is the minimum of upper bounds */
         if (DomA[1][3] <= endval) {
            if ( DomA[1][3] < inival ) break; /* the subdomain is out of area */
            ModSDEndVal[3] = DomA[1][3];
            ind = ModAEndPos[3]; /* Register the ending index in model a */
         }
         else {
            if (endval < DomA[0][3]) break;   /* the subdomain is outbound */
            ind = DCT_SearchIndex( ModAValues[3], ModANumPts[3], ModADomType[3], // hA[3],
                                   0, ModAEndPos[3], endval, 1, (ModSDEndVal+3) );
         }
         ModSDNpts[3] = ind - iniind + 1;
         dimflag++;

      case 3:
         inival = IniVal[2];
         endval = EndVal[2];

         /* Lower bound is the maximum of lower bounds */
         if (inival <= DomA[0][2]) {
            if ( endval < DomA[0][2] ) break; /* the subdomain is out of area */
            ModSDIniVal[2] = DomA[0][2];
            iniind = 0; /* Register the initial index in model a */
         }
         else{
            if (inival > DomA[1][2]) break;   /* the subdomain is out of area */
            iniind = DCT_SearchIndex( ModAValues[2], ModANumPts[2], ModADomType[2], // hA[2],
                                      0, ModAEndPos[2], inival, -1, (ModSDIniVal+2) );
         }
         ModSDGlbInd[2] = iniind; 

         /* Upper bound is the minimum of upper bounds */
         if (DomA[1][2] <= endval) {
            if ( DomA[1][2] < inival ) break; /* the subdomain is out of area */
            ModSDEndVal[2] = DomA[1][2];
            ind = ModAEndPos[2]; /* Register the ending index in model a */
         }
         else {
            if (endval < DomA[0][2]) break;   /* the subdomain is outbound */
            ind = DCT_SearchIndex( ModAValues[2], ModANumPts[2], ModADomType[2], // hA[2],
                                   0, ModAEndPos[2], endval, 1, (ModSDEndVal+2) );
         }
         ModSDNpts[2] = ind - iniind + 1;
         dimflag++;

      case 2:
         inival = IniVal[1];
         endval = EndVal[1];

         /* Lower bound is the maximum of lower bounds */
         if (inival <= DomA[0][1]) {
            if ( endval < DomA[0][1] ) break; /* the subdomain is out of area */
            ModSDIniVal[1] = DomA[0][1];
            iniind = 0; /* Register the initial index in model a */
         }
         else{
            if (inival > DomA[1][1]) break;   /* the subdomain is out of area */
            iniind = DCT_SearchIndex( ModAValues[1], ModANumPts[1], ModADomType[1], // hA[1],
                                      0, ModAEndPos[1], inival, -1, (ModSDIniVal+1) );
         }
         ModSDGlbInd[1] = iniind; 

         /* Upper bound is the minimum of upper bounds */
         if (DomA[1][1] <= endval) {
            if ( DomA[1][1] < inival ) break; /* the subdomain is out of area */
            ModSDEndVal[1] = DomA[1][1];
            ind = ModAEndPos[1]; /* Register the ending index in model a */
         }
         else {
            if (endval < DomA[0][1]) break;   /* the subdomain is outbound */
            ind = DCT_SearchIndex( ModAValues[1], ModANumPts[1], ModADomType[1], // hA[1],
                                   0, ModAEndPos[1], endval, 1, (ModSDEndVal+1) );
         }
         ModSDNpts[1] = ind - iniind + 1;
         dimflag++;

        /* ---------------------- DIM 1 -------------------- */
         inival = IniVal[0];
         endval = EndVal[0];

         /* Lower bound is the maximum of lower bounds */
         if (inival <= DomA[0][0]) {
            if ( endval < DomA[0][0] ) break; /* the subdomain is out of area */
            ModSDIniVal[0] = DomA[0][0];
            iniind = 0; /* Register the initial index in model a */
         }
         else{
            if (inival > DomA[1][0]) break;   /* the subdomain is out of area */
            iniind = DCT_SearchIndex( ModAValues[0], ModANumPts[0], ModADomType[0], // hA[0],
                                      0, ModAEndPos[0], inival, -1, (ModSDIniVal) );
         }
         ModSDGlbInd[0] = iniind; 

         /* Upper bound is the minimum of upper bounds */
         if (DomA[1][0] <= endval) {
            if ( DomA[1][0] < inival ) break; /* the subdomain is out of area */
            ModSDEndVal[0] = DomA[1][0];
            ind = ModAEndPos[0]; /* Register the ending index in model a */
         }
         else {
            if (endval < DomA[0][0]) break;   /* the subdomain is outbound */
            ind = DCT_SearchIndex( ModAValues[0], ModANumPts[0], ModADomType[0], // hA[0],
                                   0, ModAEndPos[0], endval, 1, (ModSDEndVal) );
         }
         ModSDNpts[0] = ind - iniind + 1;
         dimflag++;

    } /* End of switch(dim) */
/*************************************************************************************/
/*************************************************************************************/
    ModelBRcv[ i ].Recvr = (ModBSubDom+i)->RankProc;
    if (dimflag == dim) {            /*** Intersecting Model A (producer) subdomains ***/
       /**** Storing the global information (equal for every subdomin) ****/
       /* Model A (Producer) data stored into the Model B (Consumer)      */
       ModelBRcv[ i ].SndrNpts = ModANumPts;
//        ModelBRcv[ i ].SndrNSD = NSDomA;
//        ModelBRcv[ i ].SndrSDom = ModASubDom;
       k = 0;
       /** Here the intersection with each model A (producer) subdomain **/
       for (j=0; j < NSDomA; j++ ) {
         DCT_Return_SDBoundaryVals ( dim, (ModASubDom+j), ModADomType, ModAEndPos,
                                     ModAValues, ModAIniVal, ModAEndVal );
         ModIniPos = (ModASubDom + j)->IniPos;
         ModEndPos = (ModASubDom + j)->EndPos;
         l = SDauxSen[ j ].Nmsg;  /* Getting the last spot for the j-th Model A's SD */
         ModSD2IniVal = SDauxSen[ j ].Msend[l].IniVal;
         ModSD2EndVal = SDauxSen[ j ].Msend[l].EndVal;
         ModSDLength = SDauxSen[ j ].Msend[l].Length;
         ModSDIniPos = AuxSDSlab[ k ].IniPos;
         ModSDNpts = AuxSDSlab[ k ].Length; // Reusing variables
         dimflag = 0;
         switch (dim) {
            case 4:

               inipos = ModIniPos[3];
               endpos = ModEndPos[3];               
               inival = ModAIniVal[3];
               endval = ModAEndVal[3];
   
               coniniv = ModSDIniVal[3];
               conendv = ModSDEndVal[3];
               
               /* Lower bound is the maximum of lower bounds */
               if (inival < coniniv) {
                  if ( endval < coniniv ) break; /* the subdomain is out of area */
                  iniind = DCT_SearchIndex( ModAValues[3], ModANumPts[3], ModADomType[3], // hA[3],
                                            inipos, endpos, coniniv, -1, (ModSD2IniVal+3) );
                  ModSDIniPos[3] = 0;
               }
               else{
                  if (inival > conendv) break;   /* the subdomain is out of area */
                  ModSD2IniVal[3] =inival;
                  iniind = inipos;
                  ModSDIniPos[3] = inipos - ModSDGlbInd[3];
               }
               
               /* Upper bound is the minimum of upper bounds */
               if (conendv < endval) {
                  if ( conendv < inival ) break; /* the subdomain is out of area */
                  ind = DCT_SearchIndex( ModAValues[3], ModANumPts[3], ModADomType[3], // hA[3],
                                            inipos, endpos, conendv, 1, (ModSD2EndVal+3) );
               }
               else {
                  if (endval < coniniv) break;   /* the subdomain is outbound */
                  ModSD2EndVal[3] = endval;
                  ind = endpos;
               }
               ModSDLength[3] = ind - iniind + 1;
               ModSDNpts[3] = ModSDLength[3]; // AuxSDSlab[ k ].Length[3] = ModSDLength[3];
               dimflag++;

            case 3:

               inipos = ModIniPos[2];
               endpos = ModEndPos[2];               
               inival = ModAIniVal[2];
               endval = ModAEndVal[2];
   
               coniniv = ModSDIniVal[2];
               conendv = ModSDEndVal[2];
               
               /* Lower bound is the maximum of lower bounds */
               if (inival < coniniv) {
                  if ( endval < coniniv ) break; /* the subdomain is out of area */
                  iniind = DCT_SearchIndex( ModAValues[2], ModANumPts[2], ModADomType[2], // hA[2],
                                            inipos, endpos, coniniv, -1, (ModSD2IniVal+2) );
                  ModSDIniPos[2] = 0;
               }
               else{
                  if (inival > conendv) break;   /* the subdomain is out of area */
                  ModSD2IniVal[2] =inival;
                  iniind = inipos;
                  ModSDIniPos[2] = inipos - ModSDGlbInd[2];
               }
               
               /* Upper bound is the minimum of upper bounds */
               if (conendv < endval) {
                  if ( conendv < inival ) break; /* the subdomain is out of area */
                  ind = DCT_SearchIndex( ModAValues[2], ModANumPts[2], ModADomType[2], // hA[2],
                                            inipos, endpos, conendv, 1, (ModSD2EndVal+2) );
               }
               else {
                  if (endval < coniniv) break;   /* the subdomain is outbound */
                  ModSD2EndVal[2] = endval;
                  ind = endpos;
               }
               ModSDLength[2] = ind - iniind + 1;
               ModSDNpts[2] = ModSDLength[2]; // AuxSDSlab[ k ].Length[2] = ModSDLength[2];
               dimflag++;

            case 2:

               inipos = ModIniPos[1];
               endpos = ModEndPos[1];               
               inival = ModAIniVal[1];
               endval = ModAEndVal[1];
   
               coniniv = ModSDIniVal[1];
               conendv = ModSDEndVal[1];
               
               /* Lower bound is the maximum of lower bounds */
               if (inival < coniniv) {
                  if ( endval < coniniv ) break; /* the subdomain is out of area */
                  iniind = DCT_SearchIndex( ModAValues[1], ModANumPts[1], ModADomType[1], // hA[1],
                                            inipos, endpos, coniniv, -1, (ModSD2IniVal+1) );
                  ModSDIniPos[1] = 0;
               }
               else{
                  if (inival > conendv) break;   /* the subdomain is out of area */
                  ModSD2IniVal[1] =inival;
                  iniind = inipos;
                  ModSDIniPos[1] = inipos - ModSDGlbInd[1];
               }
               
               /* Upper bound is the minimum of upper bounds */
               if (conendv < endval) {
                  if ( conendv < inival ) break; /* the subdomain is out of area */
                  ind = DCT_SearchIndex( ModAValues[1], ModANumPts[1], ModADomType[1], // hA[1],
                                            inipos, endpos, conendv, 1, (ModSD2EndVal+1) );
               }
               else {
                  if (endval < coniniv) break;   /* the subdomain is outbound */
                  ModSD2EndVal[1] = endval;
                  ind = endpos;
               }
               ModSDLength[1] = ind - iniind + 1;
               ModSDNpts[1] = ModSDLength[1]; // AuxSDSlab[ k ].Length[1] = ModSDLength[1];
               dimflag++;

           /* ---------------------- DIM 1 -------------------- */

               inipos = ModIniPos[0];
               endpos = ModEndPos[0];               
               inival = ModAIniVal[0];
               endval = ModAEndVal[0];
   
               coniniv = ModSDIniVal[0];
               conendv = ModSDEndVal[0];
               
               /* Lower bound is the maximum of lower bounds */
               if (inival < coniniv) {
                  if ( endval < coniniv ) break; /* the subdomain is out of area */
                  iniind = DCT_SearchIndex( ModAValues[0], ModANumPts[0], ModADomType[0], // hA[0],
                                            inipos, endpos, coniniv, -1, (ModSD2IniVal) );
                  ModSDIniPos[0] = 0;
               }
               else{
                  if (inival > conendv) break;   /* the subdomain is out of area */
                  ModSD2IniVal[0] =inival;
                  iniind = inipos;
                  ModSDIniPos[0] = inipos - ModSDGlbInd[0];
               }
               
               /* Upper bound is the minimum of upper bounds */
               if (conendv < endval) {
                  if ( conendv < inival ) break; /* the subdomain is out of area */
                  ind = DCT_SearchIndex( ModAValues[0], ModANumPts[0], ModADomType[0], // hA[0],
                                            inipos, endpos, conendv, 1, (ModSD2EndVal) );
               }
               else {
                  if (endval < coniniv) break;   /* the subdomain is outbound */
                  ModSD2EndVal[0] = endval;
                  ind = endpos;
               }
               ModSDLength[0] = ind - iniind + 1;
               ModSDNpts[0] = ModSDLength[0]; // AuxSDSlab[ k ].Length[0] = ModSDLength[0];
               dimflag++;

         }  /* switch (dim) */
   
         if (dimflag == dim ) {
            /* The consumer register the sender */
            AuxSDSlab[ k ].Sendr = (ModASubDom + j)->RankProc;
            indices[k] = j;
            k++;   /* The message is counted **/
           
            /* The producer register the recceiver */
            SDauxSen[ j ].Msend[ l ].Recvr = ModelBRcv[ i ].Recvr; /* The rcvr is registered */
            SDauxSen[ j ].Nmsg++;   /** The producer message is counted */
            DCT_IncludeNeighbor ( (int)ModelBRcv[ i ].Recvr,
                                  (DCT_Integer *)&(SDauxSen[ j ].RcvrNSD),
                                  (int *)ProdNeigh[ j ] );
         }          
       } /* End of for (j=0; j < NSDomA; j++ ) */
       ModSDMrecvPlan = ModelBRcv + i;
       ModSDMrecvPlan->Nmsg = k; /* The number of messages is registered */

       /** Allocating the  definite i-th Model B messages **/
       ModSDMrecv = (DCT_SD_Consume *) malloc(sizeof(DCT_SD_Consume)*(size_t)k);
       DCTERROR( ModSDMrecv == (DCT_SD_Consume *)NULL, "Memory Allocation Failed",
                 ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

       ModSDMrecvPlan->SDSlab = ModSDMrecv;
       AuxSDrecv = AuxSDSlab;
       for (j=0; j < k; j++, ModSDMrecv++, AuxSDrecv++ ) {

          ModSDMrecv->Sendr = AuxSDrecv->Sendr;

          ModSDIniPos = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t) dim);
          DCTERROR( ModSDIniPos == (DCT_Integer *)NULL,
                    "Memory Allocation Failed",ierr.Error_code  =  DCT_FAILED_MALLOC;
                    return(ierr) );
          ModSDMrecv->IniPos = ModSDIniPos;

          ModSDLength = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t) dim);
          DCTERROR( ModSDLength == (DCT_Integer *)NULL,
                    "Memory Allocation Failed",ierr.Error_code  =  DCT_FAILED_MALLOC;
                    return(ierr) );
          ModSDMrecv->Length = ModSDLength;

          ModIniPos = AuxSDrecv->IniPos;
          ModEndPos = AuxSDrecv->Length; // Reusing variables
          for ( l=0; l < dim; l++ ) {
             ModSDIniPos[l] = ModIniPos[l];
             ModSDLength[l] = ModEndPos[l];
          }

       } /* End of for (j=0; j < k; j++ ) */
       DCT_Calculate_Neighbors( NSDomA, /*ModASubDom,*/ ModAParLayout, ModALayoutDim,
                                SDauxSen, ProdNeigh, indices, (ModelBRcv + i) );

    } /* End of if (dimflag == dim) */
    else ModelBRcv[ i ].Nmsg = 0; /* No subdomain overlapping */
  } /* End of for (i=0; i < NSDomB; i++ ) */

  /* Allocating the final DCT_SDProduce_Plan SDModASen structure */
  SDModASen = (DCT_SDProduce_Plan *) malloc( sizeof(DCT_SDProduce_Plan)*(size_t)NSDomA );
  DCTERROR( SDModASen == (DCT_SDProduce_Plan *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  /* This from the previous number of message calculation */
  AuxSDsendPlan = SDauxSen;
  ModSDMsendPlan = SDModASen;
  for(i=0; i < NSDomA; i++, AuxSDsendPlan++, ModSDMsendPlan++ ) {
     
     k = AuxSDsendPlan->Nmsg;
     AuxSDsendPlan->Nmsg = 0; /*** The auxiliar structure is reset ***/

     ModSDMsendPlan->Nmsg = k;
     ModSDMsendPlan->Sendr = (ModASubDom + i)->RankProc;
     ModSDMsendPlan->RcvrNSD = AuxSDsendPlan->RcvrNSD; /** This was calculated 
                                                      in DCT_Calculate_Neighbors **/
     AuxSDsendPlan->RcvrNSD = 0; /*** The auxiliar structure is reset ***/
     ModSDMsend = (DCT_SD_Produce *) malloc( sizeof(DCT_SD_Produce)*(size_t)k );
     DCTERROR( ModSDMsend == (DCT_SD_Produce *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
     ModSDMsendPlan->Msend = ModSDMsend;
     AuxSDsend = AuxSDsendPlan->Msend;

     for ( j=0; j < k; j++, AuxSDsend++, ModSDMsend++ ) {
        ModSDMsend->Recvr = AuxSDsend->Recvr;

        ModSDIniVal = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)dim);
        DCTERROR( ModSDIniVal == (DCT_Scalar *)NULL,
                  "Memory Allocation Failed", ierr.Error_code  =  DCT_FAILED_MALLOC;
                  return(ierr) );
        ModSDMsend->IniVal = ModSDIniVal;

        ModSDEndVal = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)dim);
        DCTERROR( ModSDEndVal == (DCT_Scalar *)NULL,
                  "Memory Allocation Failed", ierr.Error_code  =  DCT_FAILED_MALLOC;
                  return(ierr) );
        ModSDMsend->EndVal = ModSDEndVal;

        ModSDLength = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t)dim);
        DCTERROR( ModSDLength == (DCT_Integer *)NULL,
                  "Memory Allocation Failed", ierr.Error_code  =  DCT_FAILED_MALLOC;
                  return(ierr) );
        ModSDMsend->Length = ModSDLength;

        ModSD2IniVal = AuxSDsend->IniVal;
        ModSD2EndVal = AuxSDsend->EndVal;
        ModSDNpts = AuxSDsend->Length; // Reusing the variable
        for ( l=0; l < dim; l++ ) {
           ModSDIniVal[l] = ModSD2IniVal[l];
           ModSDEndVal[l] = ModSD2EndVal[l];
           ModSDLength[l] = ModSDNpts[l];
        }
     } /* End of for ( j=0; j < k; j++ ) */

  } /* End of for(i=0; i < NSDomA; i++ ) */


  /*****************************************************************************/
  /********** Communication  Model B (producer) to Model A (consumer) **********/

  /* Allocating the DCT_SDConsume_Plan ModelARcv structure */
  ModelARcv = (DCT_SDConsume_Plan *) malloc( sizeof(DCT_SDConsume_Plan)*(size_t)NSDomA );
  DCTERROR( ModelARcv == (DCT_SDConsume_Plan *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  for(i=0; i < NSDomA; i++ ){
    ModSDMrecvPlan = ModelARcv+i;

    ModSDIniVal = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
    DCTERROR( ModSDIniVal == (DCT_Scalar *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    ModSDMrecvPlan->IniVal = ModSDIniVal;

    ModSDEndVal = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
    DCTERROR( ModSDEndVal == (DCT_Scalar *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    ModSDMrecvPlan->EndVal = ModSDEndVal;

    ModSDGlbInd = (DCT_Integer *) malloc(sizeof(DCT_Integer)*(size_t)dim);
    DCTERROR( ModSDGlbInd == (DCT_Integer *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    ModSDMrecvPlan->GlbInd = ModSDGlbInd;

    ModSDNpts = (DCT_Integer *) malloc(sizeof(DCT_Integer)*(size_t)dim);
    DCTERROR( ModSDNpts == (DCT_Integer *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    ModSDMrecvPlan->Npts = ModSDNpts;

  }

  /**** Taking the producer domain region (Producer resolution) ****/
  for (i=0; i < NSDomA; i++ ) {
/*************************************************************************************/
/*************************************************************************************/
    /** The boundary values of the subdomain is extracted **/
    DCT_Return_SDBoundaryVals ( dim, (ModASubDom+i), ModADomType, ModAEndPos, ModAValues,
                                IniVal, EndVal );
    ModSDGlbInd = ModelARcv[ i ].GlbInd;
    ModSDIniVal = ModelARcv[ i ].IniVal;
    ModSDEndVal = ModelARcv[ i ].EndVal;
    ModSDNpts = ModelARcv[ i ].Npts;
    dimflag = 0;
    switch(dim) {
      case 4:

         inival = IniVal[3];
         endval = EndVal[3];

         /* Lower bound is the maximum of lower bounds */
         if (inival <= DomB[0][3]) {
            if ( endval < DomB[0][3] ) break; /* the subdomain is out of area */
            ModSDIniVal[3] = DomB[0][3];
            iniind = 0; /* Register the initial index in model a */
         }
         else{
            if (inival > DomB[1][3]) break;   /* the subdomain is out of area */
            iniind = DCT_SearchIndex( ModBValues[3], ModBNumPts[3], ModBDomType[3], // hB[3],
                                      0, ModBEndPos[3], inival, -1, (ModSDIniVal+3) );
         }
         ModSDGlbInd[3] = iniind; 

         /* Upper bound is the minimum of upper bounds */
         if (DomB[1][3] <= endval) {
            if ( DomB[1][3] < inival ) break; /* the subdomain is out of area */
            ModSDEndVal[3] = DomB[1][3];
            ind = ModBEndPos[3]; /* Register the ending index in model a */
         }
         else {
            if (endval < DomB[0][3]) break;   /* the subdomain is outbound */
            ind = DCT_SearchIndex( ModBValues[3], ModBNumPts[3], ModBDomType[3], // hB[3],
                                   0, ModBEndPos[3], endval, 1, (ModSDEndVal+3) );
         }
         ModSDNpts[3] = ind - iniind + 1;
         dimflag++;

      case 3:

         inival = IniVal[2];
         endval = EndVal[2];

         /* Lower bound is the maximum of lower bounds */
         if (inival <= DomB[0][2]) {
            if ( endval < DomB[0][2] ) break; /* the subdomain is out of area */
            ModSDIniVal[2] = DomB[0][2];
            iniind = 0; /* Register the initial index in model a */
         }
         else{
            if (inival > DomB[1][2]) break;   /* the subdomain is out of area */
            iniind = DCT_SearchIndex( ModBValues[2], ModBNumPts[2], ModBDomType[2], // hB[2],
                                      0, ModBEndPos[2], inival, -1, (ModSDIniVal+2) );
         }
         ModSDGlbInd[2] = iniind; 

         /* Upper bound is the minimum of upper bounds */
         if (DomB[1][2] <= endval) {
            if ( DomB[1][2] < inival ) break; /* the subdomain is out of area */
            ModSDEndVal[2] = DomB[1][2];
            ind = ModBEndPos[2]; /* Register the ending index in model a */
         }
         else {
            if (endval < DomB[0][2]) break;   /* the subdomain is outbound */
            ind = DCT_SearchIndex( ModBValues[2], ModBNumPts[2], ModBDomType[2], // hB[2],
                                   0, ModBEndPos[2], endval, 1, (ModSDEndVal+2) );
         }
         ModSDNpts[2] = ind - iniind + 1;
         dimflag++;

      case 2:

         inival = IniVal[1];
         endval = EndVal[1];

         /* Lower bound is the maximum of lower bounds */
         if (inival <= DomB[0][1]) {
            if ( endval < DomB[0][1] ) break; /* the subdomain is out of area */
            ModSDIniVal[1] = DomB[0][1];
            iniind = 0; /* Register the initial index in model a */
         }
         else{
            if (inival > DomB[1][1]) break;   /* the subdomain is out of area */
            iniind = DCT_SearchIndex( ModBValues[1], ModBNumPts[1], ModBDomType[1], // hB[1],
                                      0, ModBEndPos[1], inival, -1, (ModSDIniVal+1) );
         }
         ModSDGlbInd[1] = iniind; 

         /* Upper bound is the minimum of upper bounds */
         if (DomB[1][1] <= endval) {
            if ( DomB[1][1] < inival ) break; /* the subdomain is out of area */
            ModSDEndVal[1] = DomB[1][1];
            ind = ModBEndPos[1]; /* Register the ending index in model a */
         }
         else {
            if (endval < DomB[0][1]) break;   /* the subdomain is outbound */
            ind = DCT_SearchIndex( ModBValues[1], ModBNumPts[1], ModBDomType[1], // hB[1],
                                   0, ModBEndPos[1], endval, 1, (ModSDEndVal+1) );
         }
         ModSDNpts[1] = ind - iniind + 1;
         dimflag++;

        /* ---------------------- DIM 1 -------------------- */

         inival = IniVal[0];
         endval = EndVal[0];

         /* Lower bound is the maximum of lower bounds */
         if (inival <= DomB[0][0]) {
            if ( endval < DomB[0][0] ) break; /* the subdomain is out of area */
            ModSDIniVal[0] = DomB[0][0];
            iniind = 0; /* Register the initial index in model a */
         }
         else{
            if (inival > DomB[1][0]) break;   /* the subdomain is out of area */
            iniind = DCT_SearchIndex( ModBValues[0], ModBNumPts[0], ModBDomType[0], // hB[0],
                                      0, ModBEndPos[0], inival, -1, (ModSDIniVal) );
         }
         ModSDGlbInd[0] = iniind; 

         /* Upper bound is the minimum of upper bounds */
         if (DomB[1][0] <= endval) {
            if ( DomB[1][0] < inival ) break; /* the subdomain is out of area */
            ModSDEndVal[0] = DomB[1][0];
            ind = ModBEndPos[0]; /* Register the ending index in model a */
         }
         else {
            if (endval < DomB[0][0]) break;   /* the subdomain is outbound */
            ind = DCT_SearchIndex( ModBValues[0], ModBNumPts[0], ModBDomType[0], // hB[0],
                                   0, ModBEndPos[0], endval, 1, (ModSDEndVal) );
         }
         ModSDNpts[0] = ind - iniind + 1;
         dimflag++;

    } /* End of switch(dim) */
/*************************************************************************************/
/*************************************************************************************/
    ModelARcv[ i ].Recvr = (ModASubDom+i)->RankProc;
    if (dimflag == dim) {            /*** Intersecting Model B (producer) subdomains ***/
       /**** Storing the global information (equal for every subdomin) ****/
       /* Model B (Producer) data stored into the Model A (Consumer)      */
       ModelARcv[ i ].SndrNpts = ModBNumPts;
//        ModelARcv[ i ].SndrNSD = NSDomB;
//        ModelARcv[ i ].SndrSDom = ModBSubDom;
       k = 0;
       /** Here the intersection with each model B (producer) subdomain **/
       for ( j=0; j < NSDomB; j++ ) {
         DCT_Return_SDBoundaryVals ( dim, (ModBSubDom+j), ModBDomType, ModBEndPos,
                                     ModBValues, ModAIniVal, ModAEndVal );
         ModIniPos = (ModBSubDom + j)->IniPos;
         ModEndPos = (ModBSubDom + j)->EndPos;
         l = SDauxSen[ j ].Nmsg;  /* Getting the last spot for the j-th Model A's SD */
         ModSD2IniVal = SDauxSen[ j ].Msend[l].IniVal;
         ModSD2EndVal = SDauxSen[ j ].Msend[l].EndVal;
         ModSDLength = SDauxSen[ j ].Msend[l].Length;
         ModSDIniPos = AuxSDSlab[ k ].IniPos;
         ModSDNpts = AuxSDSlab[ k ].Length; // Reusing variables
         dimflag = 0;
         switch (dim) {
            case 4:

               inipos = ModIniPos[3];
               endpos = ModEndPos[3];               
               inival = ModAIniVal[3];
               endval = ModAEndVal[3];
   
               coniniv = ModSDIniVal[3];
               conendv = ModSDEndVal[3];
               
               /* Lower bound is the maximum of lower bounds */
               if (inival < coniniv) {
                  if ( endval < coniniv ) break; /* the subdomain is out of area */
                  iniind = DCT_SearchIndex( ModBValues[3], ModBNumPts[3], ModBDomType[3], // hB[3],
                                            inipos, endpos, coniniv, -1, (ModSD2IniVal+3) );
                  ModSDIniPos[3] = 0;
               }
               else{
                  if (inival > conendv) break;   /* the subdomain is out of area */
                  ModSD2IniVal[3] =inival;
                  iniind = inipos;
                  ModSDIniPos[3] = inipos - ModSDGlbInd[3];
               }
               
               /* Upper bound is the minimum of upper bounds */
               if (conendv < endval) {
                  if ( conendv < inival ) break; /* the subdomain is out of area */
                  ind = DCT_SearchIndex( ModBValues[3], ModBNumPts[3], ModBDomType[3], // hB[3],
                                            inipos, endpos, conendv, 1, (ModSD2EndVal+3) );
               }
               else {
                  if (endval < coniniv) break;   /* the subdomain is outbound */
                  ModSD2EndVal[3] = endval;
                  ind = endpos;
               }
               ModSDLength[3] = ind - iniind + 1;
               ModSDNpts[3] = ModSDLength[3]; // AuxSDSlab[ k ].Length[3] = ModSDLength[3];
               dimflag++;

            case 3:

               inipos = ModIniPos[2];
               endpos = ModEndPos[2];               
               inival = ModAIniVal[2];
               endval = ModAEndVal[2];
   
               coniniv = ModSDIniVal[2];
               conendv = ModSDEndVal[2];
               
               /* Lower bound is the maximum of lower bounds */
               if (inival < coniniv) {
                  if ( endval < coniniv ) break; /* the subdomain is out of area */
                  iniind = DCT_SearchIndex( ModBValues[2], ModBNumPts[2], ModBDomType[2], // hB[2],
                                            inipos, endpos, coniniv, -1, (ModSD2IniVal+2) );
                  ModSDIniPos[2] = 0;
               }
               else{
                  if (inival > conendv) break;   /* the subdomain is out of area */
                  ModSD2IniVal[2] =inival;
                  iniind = inipos;
                  ModSDIniPos[2] = inipos - ModSDGlbInd[2];
               }
               
               /* Upper bound is the minimum of upper bounds */
               if (conendv < endval) {
                  if ( conendv < inival ) break; /* the subdomain is out of area */
                  ind = DCT_SearchIndex( ModBValues[2], ModBNumPts[2], ModBDomType[2], // hB[2],
                                            inipos, endpos, conendv, 1, (ModSD2EndVal+2) );
               }
               else {
                  if (endval < coniniv) break;   /* the subdomain is outbound */
                  ModSD2EndVal[2] = endval;
                  ind = endpos;
               }
               ModSDLength[2] = ind - iniind + 1;
               ModSDNpts[2] = ModSDLength[2]; // AuxSDSlab[ k ].Length[2] = ModSDLength[2];
               dimflag++;

            case 2:

               inipos = ModIniPos[1];
               endpos = ModEndPos[1];               
               inival = ModAIniVal[1];
               endval = ModAEndVal[1];
   
               coniniv = ModSDIniVal[1];
               conendv = ModSDEndVal[1];
               
               /* Lower bound is the maximum of lower bounds */
               if (inival < coniniv) {
                  if ( endval < coniniv ) break; /* the subdomain is out of area */
                  iniind = DCT_SearchIndex( ModBValues[1], ModBNumPts[1], ModBDomType[1], // hB[1],
                                            inipos, endpos, coniniv, -1, (ModSD2IniVal+1) );
                  ModSDIniPos[1] = 0;
               }
               else{
                  if (inival > conendv) break;   /* the subdomain is out of area */
                  ModSD2IniVal[1] =inival;
                  iniind = inipos;
                  ModSDIniPos[1] = inipos - ModSDGlbInd[1];
               }
               
               /* Upper bound is the minimum of upper bounds */
               if (conendv < endval) {
                  if ( conendv < inival ) break; /* the subdomain is out of area */
                  ind = DCT_SearchIndex( ModBValues[1], ModBNumPts[1], ModBDomType[1], // hB[1],
                                            inipos, endpos, conendv, 1, (ModSD2EndVal+1) );
               }
               else {
                  if (endval < coniniv) break;   /* the subdomain is outbound */
                  ModSD2EndVal[1] = endval;
                  ind = endpos;
               }
               ModSDLength[1] = ind - iniind + 1;
               ModSDNpts[1] = ModSDLength[1]; // AuxSDSlab[ k ].Length[1] = ModSDLength[1];
               dimflag++;

           /* ---------------------- DIM 1 -------------------- */

               inipos = ModIniPos[0];
               endpos = ModEndPos[0];               
               inival = ModAIniVal[0];
               endval = ModAEndVal[0];
   
               coniniv = ModSDIniVal[0];
               conendv = ModSDEndVal[0];
               
               /* Lower bound is the maximum of lower bounds */
               if (inival < coniniv) {
                  if ( endval < coniniv ) break; /* the subdomain is out of area */
                  iniind = DCT_SearchIndex( ModBValues[0], ModBNumPts[0], ModBDomType[0], // hB[0],
                                            inipos, endpos, coniniv, -1, (ModSD2IniVal) );
                  ModSDIniPos[0] = 0;
               }
               else{
                  if (inival > conendv) break;   /* the subdomain is out of area */
                  ModSD2IniVal[0] =inival;
                  iniind = inipos;
                  ModSDIniPos[0] = inipos - ModSDGlbInd[0];
               }
               
               /* Upper bound is the minimum of upper bounds */
               if (conendv < endval) {
                  if ( conendv < inival ) break; /* the subdomain is out of area */
                  ind = DCT_SearchIndex( ModBValues[0], ModBNumPts[0], ModBDomType[0], // hB[0],
                                            inipos, endpos, conendv, 1, (ModSD2EndVal) );
               }
               else {
                  if (endval < coniniv) break;   /* the subdomain is outbound */
                  ModSD2EndVal[0] = endval;
                  ind = endpos;
               }
               ModSDLength[0] = ind - iniind + 1;
               ModSDNpts[0] = ModSDLength[0]; // AuxSDSlab[ k ].Length[0] = ModSDLength[0];
               dimflag++;

         }  /* switch (dim) */
   
         if (dimflag == dim ) {
            /* The consumer register the sender */
            AuxSDSlab[ k ].Sendr = (ModBSubDom + j)->RankProc;
            indices[k] = j;
            k++;   /* The message is counted **/
           
            /* The producer register the recceiver */
            SDauxSen[ j ].Msend[l].Recvr = ModelARcv[ i ].Recvr; /* The rcvr is registered */
            SDauxSen[ j ].Nmsg++;   /** The producer message is counted */
            DCT_IncludeNeighbor ( (int)ModelARcv[ i ].Recvr,
                                  (DCT_Integer *)&(SDauxSen[ j ].RcvrNSD),
                                  (int *)ProdNeigh[ j ] );
         }          
       } /* End of for (j=0; j < NSDomB; j++ ) */
       ModSDMrecvPlan = ModelARcv + i;
       ModSDMrecvPlan->Nmsg = k; /* The number of messages is registered */
       
       /** Allocating the  definite i-th Model B messages **/
       ModSDMrecv = (DCT_SD_Consume *) malloc(sizeof(DCT_SD_Consume)*(size_t)k);
       DCTERROR( ModSDMrecv == (DCT_SD_Consume *)NULL, "Memory Allocation Failed",
                 ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

       ModSDMrecvPlan->SDSlab = ModSDMrecv;
       AuxSDrecv = AuxSDSlab;
       for (j=0; j < k; j++, ModSDMrecv++, AuxSDrecv++ ) {


          ModSDMrecv->Sendr = AuxSDrecv->Sendr;

          ModSDIniPos = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t) dim);
          DCTERROR( ModSDIniPos == (DCT_Integer *)NULL,
                    "Memory Allocation Failed",ierr.Error_code  =  DCT_FAILED_MALLOC;
                    return(ierr) );
          ModSDMrecv->IniPos = ModSDIniPos;

          ModSDLength = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t) dim);
          DCTERROR( ModSDLength == (DCT_Integer *)NULL,
                    "Memory Allocation Failed",ierr.Error_code  =  DCT_FAILED_MALLOC;
                    return(ierr) );
          ModSDMrecv->Length = ModSDLength;

          ModIniPos = AuxSDrecv->IniPos;
          ModEndPos = AuxSDrecv->Length; // Reusing variables
          for ( l=0; l < dim; l++ ) {
             ModSDIniPos[l] = ModIniPos[l];
             ModSDLength[l] = ModEndPos[l];
          }
       }
       DCT_Calculate_Neighbors( NSDomB, /*ModBSubDom,*/ ModBParLayout, ModBLayoutDim,
                                SDauxSen, ProdNeigh, indices, (ModelARcv + i) );

    } /* End of if (dimflag == dim) */
    else ModelARcv[ i ].Nmsg = 0;
  }  /* End of for (i=0; i < NSDomA; i++ ) */


  /* Allocating the final DCT_SDProduce_Plan SDModASen structure */
  SDModBSen = (DCT_SDProduce_Plan *) malloc( sizeof(DCT_SDProduce_Plan)*(size_t)NSDomB );
  DCTERROR( SDModBSen == (DCT_SDProduce_Plan *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  /* This from the previous number of message calculation */
  AuxSDsendPlan = SDauxSen;
  ModSDMsendPlan = SDModBSen;
  for(i=0; i < NSDomB; i++, AuxSDsendPlan++, ModSDMsendPlan++ ) {

     k = AuxSDsendPlan->Nmsg;
     ModSDMsendPlan->Nmsg = k;

     ModSDMsendPlan->Sendr = (ModBSubDom + i)->RankProc;
     ModSDMsendPlan->RcvrNSD = AuxSDsendPlan->RcvrNSD; /** This was calculated 
                                                     in DCT_Calculate_Neighbors **/
     ModSDMsend = (DCT_SD_Produce *) malloc( sizeof(DCT_SD_Produce)*(size_t)k );
     DCTERROR( ModSDMsend == (DCT_SD_Produce *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
     ModSDMsendPlan->Msend = ModSDMsend;
     AuxSDsend = AuxSDsendPlan->Msend;

     for ( j=0; j < k; j++, AuxSDsend++, ModSDMsend++ ) {
        ModSDMsend->Recvr = AuxSDsend->Recvr;

        ModSDIniVal = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)dim);
        DCTERROR( ModSDIniVal == (DCT_Scalar *)NULL,
                  "Memory Allocation Failed", ierr.Error_code  =  DCT_FAILED_MALLOC;
                  return(ierr) );
        ModSDMsend->IniVal = ModSDIniVal;

        ModSDEndVal = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)dim);
        DCTERROR( ModSDEndVal == (DCT_Scalar *)NULL,
                  "Memory Allocation Failed", ierr.Error_code  =  DCT_FAILED_MALLOC;
                  return(ierr) );
        ModSDMsend->EndVal = ModSDEndVal;

        ModSDLength = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t)dim);
        DCTERROR( ModSDLength == (DCT_Integer *)NULL,
                  "Memory Allocation Failed", ierr.Error_code  =  DCT_FAILED_MALLOC;
                  return(ierr) );
        ModSDMsend->Length = ModSDLength;

        ModSD2IniVal = AuxSDsend->IniVal;
        ModSD2EndVal = AuxSDsend->EndVal;
        ModSDNpts = AuxSDsend->Length; // Reusing the variable
        for ( l=0; l < dim; l++ ) {
           ModSDIniVal[l] = ModSD2IniVal[l];
           ModSDEndVal[l] = ModSD2EndVal[l];
           ModSDLength[l] = ModSDNpts[l];
        }

     } /* End of for ( j=0; j < k; j++ ) */

  } /* End of for(i=0; i < NSDomB; i++ ) */


  /* Freeing the auxiliar structures */
  free( DomA[0] );
  free( DomA[1] );
  free( ModAEndPos );

  free( DomB[0] );
  free( DomB[1] );
  free( ModBEndPos );

  for ( i=0; i < maxsd; i++ ) {
     free ( AuxSDSlab[i].IniPos );
     free ( AuxSDSlab[i].Length );
  }  
  free ( AuxSDSlab );

  for ( i=0; i < maxsd; i++ ) free ( ProdNeigh[i] );
  free ( ProdNeigh );

  free ( indices );

  for ( i=0; i < maxsd; i++ ) {
     for ( j=0; j < maxsd; j++ ) {
        free ( SDauxSen[i].Msend[j].IniVal );
        free ( SDauxSen[i].Msend[j].EndVal );
        free ( SDauxSen[i].Msend[j].Length );
     }
     free ( SDauxSen[i].Msend );
  }
  free ( SDauxSen );

/*                                                   BEGIN DEBUG */
//   printf("Consumer Model Name: %s\n", modA->ModName);
//   printf("Total Number of subdomains: %d\n", NSDomA);
//   for(i=0; i<NSDomA; i++){
//     printf("Rank in subdomain %d: %d\n", i, ModelARcv[ i ].Recvr);
//     if ( ModelARcv[ i ].Nmsg == 0 ) {
//        printf("\tSUBDOMAIN  %d DOES NOT LAY ON COUPLING DOMAIN.\n\n", i );
//     } else {
//        
//        for ( j=0; j < dim; j++ ) {
//          printf("\tIniVal Direction %d: %f\n", j, ModelARcv[ i ].IniVal[j]);
//          printf("\tEndVal Direction %d: %f\n", j, ModelARcv[ i ].EndVal[j]);
//          printf("\tGlbInd Direction %d: %d\n", j, ModelARcv[ i ].GlbInd[j]);
//          printf("\tNpts  Direction %d: %d\n", j, ModelARcv[ i ].Npts[j]);
//        }
//        k = ModelARcv[ i ].Nmsg;
//        printf("\t**** SubDomain %d receives %d messages.\n",i ,k );
//        for(j=0; j < k; j++){
//           printf("\t\t Message %d from process %d.\n", j, ModelARcv[i].SDSlab[j].Sendr );
//           for ( l=0; l < dim; l++ ) {
//             printf("\t\tIniPos Direction %d: %d\n", l, 
//                                       ModelARcv[i].SDSlab[j].IniPos[l]);
//             printf("\t\tLength  Direction %d: %d\n", l,
//                                       ModelARcv[i].SDSlab[j].Length[l]);
//           }          
//        }
//     }
//   }
//   printf("Producer Model Name: %s\n", modB->ModName);
//   printf("Total Number of subdomains: %d\n", NSDomB);
//   for(i=0; i < NSDomB; i++ ) {
//      printf("Rank in subdomain %d: %d\n", i, SDModBSen[i].Sendr );
//      if ( SDModBSen[i].Nmsg == 0 ) {
//         printf("\tSUBDOMAIN  %d DOES NOT LAY ON COUPLING DOMAIN.\n\n", i );
//      } else {
//         k = SDModBSen[i].Nmsg;
//         for ( j=0; j < k; j++ ) {
//            printf("\t\t Message %d to process %d.\n", j, SDModBSen[i].Msend[j].Recvr );
//            for ( l=0; l < dim; l++ ) {
//               printf("\t\tIniVal Direction %d: %f\n", l,
//                                                SDModBSen[i].Msend[j].IniVal[l] );
//               printf("\t\tEndVal Direction %d: %f\n", l,
//                                                SDModBSen[i].Msend[j].EndVal[l] );
//               printf("\t\tLength Direction %d: %d\n", l,
//                                                SDModBSen[i].Msend[j].Length[l] );
//            }
//         } /* End of for ( j=0; j < k; j++ ) */
//      }
// 
//   } /* End of for(i=0; i < NSDomB; i++ ) */  
// /*                                                     END DEBUG */
// /*                                                   BEGIN DEBUG */
//   printf("Consumer Model Name: %s\n", modB->ModName);
//   printf("Total Number of subdomains: %d\n", NSDomB);
//   for(i=0; i<NSDomB; i++){
//     printf("Rank in subdomain %d: %d\n", i, ModelBRcv[ i ].Recvr);
//     if ( ModelBRcv[ i ].Nmsg == 0 ) {
//        printf("\tSUBDOMAIN  %d DOES NOT LAY ON COUPLING DOMAIN.\n\n", i );
//     } else {
//        for ( j=0; j < dim; j++ ) {
//          printf("\tIniVal Direction %d: %f\n", j, ModelBRcv[ i ].IniVal[j]);
//          printf("\tEndVal Direction %d: %f\n", j, ModelBRcv[ i ].EndVal[j]);
//          printf("\tGlbInd Direction %d: %d\n", j, ModelBRcv[ i ].GlbInd[j]);
//          printf("\tNpts  Direction %d: %d\n", j, ModelBRcv[ i ].Npts[j]);
//        }
//        k = ModelBRcv[ i ].Nmsg;
//        printf("\t**** SubDomain %d receives %d messages.\n",i ,k );
//        for(j=0; j < k; j++){
//           printf("\t\t Message %d from process %d.\n", j, ModelBRcv[i].SDSlab[j].Sendr );
//           for ( l=0; l < dim; l++ ) {
//             printf("\t\tIniPos Direction %d: %d\n", l, 
//                                       ModelBRcv[i].SDSlab[j].IniPos[l]);
//             printf("\t\tLength  Direction %d: %d\n", l,
//                                       ModelBRcv[i].SDSlab[j].Length[l]);
//           }          
//        }
//     }
//   }
//   printf("Producer Model Name: %s\n", modA->ModName);
//   printf("Total Number of subdomains: %d\n", NSDomA);
//   for(i=0; i < NSDomA; i++ ) {
//      printf("Rank in subdomain %d: %d\n", i, SDModASen[i].Sendr );
//      if ( SDModASen[i].Nmsg == 0 ) {
//         printf("\tSUBDOMAIN  %d DOES NOT LAY ON COUPLING DOMAIN.\n\n", i );
//      } else {
//         k = SDModASen[i].Nmsg;
//         for ( j=0; j < k; j++ ) {
//            printf("\t\t Message %d to process %d.\n", j, SDModASen[i].Msend[j].Recvr );
//            for ( l=0; l < dim; l++ ) {
//               printf("\t\tIniVal Direction %d: %f\n", l,
//                                                SDModASen[i].Msend[j].IniVal[l] );
//               printf("\t\tEndVal Direction %d: %f\n", l,
//                                                SDModASen[i].Msend[j].EndVal[l] );
//               printf("\t\tLength Direction %d: %d\n", l,
//                                                SDModASen[i].Msend[j].Length[l] );
//            }
//         } /* End of for ( j=0; j < k; j++ ) */
//      }
// 
//  } /* End of for(i=0; i < NSDomA; i++ ) */
/*                                                     END DEBUG */

  /* The output variables are set */
  *ModASen = SDModASen;
  *nModASD = NSDomA;
  *ModARcv = ModelARcv;
  
  *ModBSen = SDModBSen;
  *nModBSD = NSDomB;
  *ModBRcv = ModelBRcv;

  ierr.Error_code = DCT_SUCCESS;
  ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
  return(ierr);


/* -------------------------------------  END( DCT_BrokerArrange ) */

}


/* This function detects the intersection between domains and subdomains of two models */
/* The domains are specified as General Curvilinear                                    */
DCT_Error DCT_BrokerArrangeGC(const DCT_Model *modA, const DCT_Model *modB,
                          DCT_SDProduce_Plan **ModASen, int *nModASD, DCT_SDConsume_Plan **ModARcv,
                          DCT_SDProduce_Plan **ModBSen, int *nModBSD, DCT_SDConsume_Plan **ModBRcv )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;

  DCT_Model_Dom       *PtrDomA, *PtrDomB;
  DCT_Integer          dim, NumSubDomA, NumSubDomB;
  DCT_Integer          offsetA, offsetB;
  DCT_Integer          dim1A, dim2A, dim3A, dim4A;
  DCT_Integer          dim1B, dim2B, dim3B, dim4B;
  DCT_Integer          indI, indJ, indK, indL;
  DCT_Integer          inipos, endpos;
  DCT_Scalar          *DomA[2] = {(DCT_Scalar *)NULL, (DCT_Scalar *)NULL};
  DCT_Scalar          *DomB[2] = {(DCT_Scalar *)NULL, (DCT_Scalar *)NULL};
  DCT_Scalar           inival, endval;
  DCT_SDProduce_Plan      *SDModASen = (DCT_SDProduce_Plan *)NULL;
  DCT_SDProduce_Plan      *SDModBSen = (DCT_SDProduce_Plan *)NULL;
  DCT_SDProduce_Plan      *SDauxSen = (DCT_SDProduce_Plan *)NULL;
  DCT_SDConsume_Plan       *ModelARcv = (DCT_SDConsume_Plan *)NULL;
  DCT_SDConsume_Plan       *ModelBRcv = (DCT_SDConsume_Plan *)NULL;
  DCT_Intersect_SubDom *auxlst = (DCT_Intersect_SubDom *)NULL;
  DCT_Intersect_SubDom *SDomAlst = (DCT_Intersect_SubDom *)NULL;
  DCT_Intersect_SubDom *SDomBlst = (DCT_Intersect_SubDom *)NULL;
  DCT_String           Msg;
  int                  NSDomA, NSDomB;
  int                  ind, dimflag, maxsd;
  register int         i, j, k;


/* ---------------------------------  BEGIN( DCT_BrokerArrangeGC ) */


  PtrDomA = modA->ModDomain;
  dim = PtrDomA->ModDomDim;

  DomA[0] = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
  DCTERROR( DomA[0] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  DomA[1] = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
  DCTERROR( DomA[1] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

  /* Checking layout in model A and getting the number of subdomains */
  switch(PtrDomA->ModSubDomParLayout)
  {
    case DCT_DIST_SEQUENTIAL:
      NumSubDomA = 1;
      break;
    case DCT_DIST_RECTANGULAR:
      NumSubDomA = *(PtrDomA->ModSubDomLayoutDim) * *(PtrDomA->ModSubDomLayoutDim+1);
      break;
    case DCT_DIST_3D_RECTANGULAR:
      NumSubDomA = *(PtrDomA->ModSubDomLayoutDim) * *(PtrDomA->ModSubDomLayoutDim+1) *
                   *(PtrDomA->ModSubDomLayoutDim+2);
    case DCT_DIST_NULL:
      ierr.Error_code    = DCT_INVALID_ARGUMENT;
      ierr.Error_msg     = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
        "[DCT_BrokerArrange] The distribution type entered for Model <%s> is not valid\n",
              modA->ModName);
      return(ierr);
  }

  offsetA = 1;

  /* Get the strides in each dimension for Model A */
  switch(dim) {
    case 4:
      dim4A = PtrDomA->ModNumPts[3];
      offsetA *= dim4A;
      
    case 3:
      dim3A = PtrDomA->ModNumPts[2];
      offsetA *= dim3A;

    case 2:
      dim2A = PtrDomA->ModNumPts[1];
      offsetA *= dim2A;

      /* ---------------------- DIM 1 -------------------- */
      dim1A = PtrDomA->ModNumPts[0];
      offsetA *= dim1A;
  }
  offsetA--;

  /* Get domain limits in model Model A */
  switch(dim) {
    case 4:
      DomA[0][3] = PtrDomA->ModValues[3][0];
      DomA[1][3] = PtrDomA->ModValues[3][offsetA];
      
    case 3:
      DomA[0][2] = PtrDomA->ModValues[2][0];
      DomA[1][2] = PtrDomA->ModValues[2][offsetA];

    case 2:
      DomA[0][1] = PtrDomA->ModValues[1][0];
      DomA[1][1] = PtrDomA->ModValues[1][offsetA];

      /* ---------------------- DIM 1 -------------------- */
      DomA[0][0] = PtrDomA->ModValues[0][0];
      DomA[1][0] = PtrDomA->ModValues[0][offsetA];
  }


  PtrDomB = modB->ModDomain;

  DomB[0] = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
  DCTERROR( DomB[0] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  DomB[1] = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
  DCTERROR( DomB[1] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );


  /* Checking layout in model B and getting the number of subdomains */
  switch(PtrDomB->ModSubDomParLayout)
  {
    case DCT_DIST_SEQUENTIAL:
      NumSubDomB = 1;
      break;
    case DCT_DIST_RECTANGULAR:
      NumSubDomB = *(PtrDomB->ModSubDomLayoutDim) * *(PtrDomB->ModSubDomLayoutDim+1);
      break;
    case DCT_DIST_3D_RECTANGULAR:
      NumSubDomB = *(PtrDomB->ModSubDomLayoutDim) * *(PtrDomB->ModSubDomLayoutDim+1) *
                   *(PtrDomB->ModSubDomLayoutDim+2);
    case DCT_DIST_NULL:
      ierr.Error_code    = DCT_INVALID_ARGUMENT;
      ierr.Error_msg     = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
        "[DCT_BrokerArrange] The distribution type entered for Model <%s> is not valid\n",
              modB->ModName);
      return(ierr);
  }

  offsetB = 1;

  /* Get the strides in each dimension for Model B */
  switch(dim) {
    case 4:
      dim4B = PtrDomB->ModNumPts[3];
      offsetB *= dim4B;
      
    case 3:
      dim3B = PtrDomB->ModNumPts[2];
      offsetB *= dim3B;

    case 2:
      dim2B = PtrDomB->ModNumPts[1];
      offsetB *= dim2B;

      /* ---------------------- DIM 1 -------------------- */
      dim1B = PtrDomB->ModNumPts[0];
      offsetB *= dim1B;
  }
  offsetB--;

  /* Get domain limits in model Model B */
  switch(dim) {
    case 4:
      DomB[0][3] = PtrDomB->ModValues[3][0];
      DomB[1][3] = PtrDomB->ModValues[3][offsetB];

    case 3:
      DomB[0][2] = PtrDomB->ModValues[2][0];
      DomB[1][2] = PtrDomB->ModValues[2][offsetB];

    case 2:
      DomB[0][1] = PtrDomB->ModValues[1][0];
      DomB[1][1] = PtrDomB->ModValues[1][offsetB];

      /* ---------------------- DIM 1 -------------------- */
      DomB[0][0] = PtrDomB->ModValues[0][0];
      DomB[1][0] = PtrDomB->ModValues[0][offsetB];

  }

  /* Determining the DomA and DomB intersection, it's kept in DomA */
  dimflag = 0;
  switch(dim) {
    case 4:
      /* Lower bound is the maximum of lower bounds */
      if (DomA[0][3] < DomB[0][3]) DomA[0][3] = DomB[0][3];
      /* Upper bound is the minimum of upper bounds */
      if (DomB[1][3] < DomA[1][3]) DomA[1][3] = DomB[1][3];
      /* Lower bound greater than upper one, then no intresection */
      if (DomA[1][3] <= DomA[0][3]) break;
      dimflag++;
    case 3:
      /* Lower bound is the maximum of lower bounds */
      if (DomA[0][2] < DomB[0][2]) DomA[0][2] = DomB[0][2];
      /* Upper bound is the minimum of upper bounds */
      if (DomB[1][2] < DomA[1][2]) DomA[1][2] = DomB[1][2];
      /* Lower bound greater than upper one, then no intresection */
      if (DomA[1][2] <= DomA[0][2]) break;
      dimflag++;
    case 2:
      if (DomA[0][1] < DomB[0][1]) DomA[0][1] = DomB[0][1];
      /* Upper bound is the minimum of upper bounds */
      if (DomB[1][1] < DomA[1][1]) DomA[1][1] = DomB[1][1];
      /* Lower bound greater than upper one, then no intresection */
      if (DomA[1][1] <= DomA[0][1]) break;
      dimflag++;
      /* ---------------------- DIM 1 -------------------- */
      if (DomA[0][0] < DomB[0][0]) DomA[0][0] = DomB[0][0];
      /* Upper bound is the minimum of upper bounds */
      if (DomB[1][0] < DomA[1][0]) DomA[1][0] = DomB[1][0];
      /* Lower bound greater than upper one, then no intresection */
      if (DomA[1][0] <= DomA[0][0]) break;
      dimflag++;
  }

  if (dimflag != dim ) {
    ierr.Error_code  = DCT_INVALID_COUPLING_DOMAIN;
    ierr.Error_msg   = (DCT_String) malloc((size_t)150);
    sprintf(ierr.Error_msg,
      "[DCT_BrokerArrange] No intersection area between models <%s> and <%s>\n",
      modA->ModName, modB->ModName );
    return(ierr);

  }

  /* Temporary array */
  maxsd = (NumSubDomB > NumSubDomA? NumSubDomB : NumSubDomA );
  auxlst = (DCT_Intersect_SubDom *) malloc( sizeof(DCT_Intersect_SubDom)*(size_t)maxsd );
  DCTERROR( auxlst == (DCT_Intersect_SubDom *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  for(i=0; i < maxsd; i++ ){
    auxlst[i].IniVal = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
    DCTERROR( auxlst[i].IniVal == (DCT_Scalar *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    auxlst[i].EndVal = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
    DCTERROR( auxlst[i].EndVal == (DCT_Scalar *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  }

  /* Determining which Subdomains in ModA lie in the intersection area */
  NSDomA = 0;
  for (i=0; i < NumSubDomA; i++ ) {
    dimflag = 0;
    switch(dim) {
      case 4:
        /* ---------------------- DIM 1 -------------------- */
        indI = (PtrDomA->ModSubDom+i)->IniPos[0];
        indJ = (PtrDomA->ModSubDom+i)->IniPos[1];
        indK = (PtrDomA->ModSubDom+i)->IniPos[2];
        indL = (PtrDomA->ModSubDom+i)->IniPos[3];
        inipos =  indL  + dim4A*(indK + dim3A*(indJ + dim2A*indI) );

        indI = (PtrDomA->ModSubDom+i)->EndPos[0];
        endpos = indL  + dim4A*(indK + dim3A*(indJ + dim2A*indI) );

        inival = PtrDomA->ModValues[0][inipos];
        endval = PtrDomA->ModValues[0][endpos];

        /* Lower bound is the maximum of lower bounds */
        if (inival < DomA[0][0]) {
           if ( endval < DomA[0][0] ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomA->ModValues[0], inipos, endpos, DomA[0][0],
                                       1, PtrDomA->ModNumPts, 4, 1 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomA].IniVal[0] = PtrDomA->ModValues[0][ind];
        }
        else{
           if (inival > DomA[1][0]) break;   /* the subdomain is out of area */
           auxlst[NSDomA].IniVal[0] = inival;
        }

        /* Upper bound is the minimum of upper bounds */
        if (DomA[1][0] < endval) {
           if ( DomA[1][0] < inival ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomA->ModValues[0], inipos, endpos, DomA[1][0],
                                      -1, PtrDomA->ModNumPts, 4, 1 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomA].EndVal[0] = PtrDomA->ModValues[0][ind];
        }
        else {
           if (endval < DomA[0][0]) break;   /* the subdomain is outbound */
           auxlst[NSDomA].EndVal[0] = endval;
        }

/********** Attention: This is only if the data is figured out from here
                       but if the values are sent to the processor to
                       calculate whether it is data to send or not        **********/
//         if ( (auxlst[NSDomA].EndVal[0] == inival) &&  /* the left limit is open */
//              (auxlst[NSDomA].IniVal[0] == inival) &&
//              (inipos != 0) )                  /*  excepting the domain boundary */
//            break;
        dimflag++;

        /* ---------------------- DIM 2 -------------------- */
        inipos = endpos;
        inival = PtrDomA->ModValues[1][inipos];
        
        indJ = (PtrDomA->ModSubDom+i)->EndPos[1];
        endpos = indL  + dim4A*(indK + dim3A*(indJ + dim2A*indI) );

        endval = PtrDomA->ModValues[1][endpos];

        /* Lower bound is the maximum of lower bounds */
        if (inival < DomA[0][1]) {
           if ( endval < DomA[0][1] ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomA->ModValues[1], inipos, endpos, DomA[0][1],
                                       1, PtrDomA->ModNumPts, 4, 2 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomA].IniVal[1] = PtrDomA->ModValues[1][ind];
        }
        else{
           if (inival > DomA[1][1]) break;   /* the subdomain is out of area */
           auxlst[NSDomA].IniVal[1] = inival;
        }

        /* Upper bound is the minimum of upper bounds */
        if (DomA[1][1] < endval) {
           if ( DomA[1][1] < inival ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomA->ModValues[1], inipos, endpos, DomA[1][1],
                                      -1, PtrDomA->ModNumPts, 4, 2 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomA].EndVal[1] = PtrDomA->ModValues[1][ind];
        }
        else {
           if (endval < DomA[0][1]) break;   /* the subdomain is outbound */
           auxlst[NSDomA].EndVal[1] = endval;
        }

/********** Attention: This is only if the data is figured out from here
                       but if the values are sent to the processor to
                       calculate whether it is data to send or not        **********/
//         if ( (auxlst[NSDomA].EndVal[1] == inival) &&  /* the left limit is open */
//              (auxlst[NSDomA].IniVal[1] == inival) &&
//              (inipos != 0) )                  /*  excepting the domain boundary */
//            break;

        dimflag++;

        /* ---------------------- DIM 3 -------------------- */
        inipos = endpos;
        inival = PtrDomA->ModValues[2][inipos];
        
        indK = (PtrDomA->ModSubDom+i)->EndPos[2];
        endpos = indL  + dim4A*(indK + dim3A*(indJ + dim2A*indI) );

        endval = PtrDomA->ModValues[2][endpos];

        /* Lower bound is the maximum of lower bounds */
        if (inival < DomA[0][2]) {
           if ( endval < DomA[0][2] ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomA->ModValues[2], inipos, endpos, DomA[0][2],
                                       1, PtrDomA->ModNumPts, 4, 3 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomA].IniVal[2] = PtrDomA->ModValues[2][ind];
        }
        else{
           if (inival > DomA[1][2]) break;   /* the subdomain is out of area */
           auxlst[NSDomA].IniVal[2] = inival;
        }

        /* Upper bound is the minimum of upper bounds */
        if (DomA[1][2] < endval) {
           if ( DomA[1][2] < inival ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomA->ModValues[2], inipos, endpos, DomA[1][2],
                                      -1, PtrDomA->ModNumPts, 4, 3 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomA].EndVal[2] = PtrDomA->ModValues[2][ind];
        }
        else {
           if (endval < DomA[0][2]) break;   /* the subdomain is outbound */
           auxlst[NSDomA].EndVal[2] = endval;
        }

/********** Attention: This is only if the data is figured out from here
                       but if the values are sent to the processor to
                       calculate whether it is data to send or not        **********/
//         if ( (auxlst[NSDomA].EndVal[2] == inival) &&  /* the left limit is open */
//              (auxlst[NSDomA].IniVal[2] == inival) &&
//              (inipos != 0) )                  /*  excepting the domain boundary */
//            break;
        dimflag++;

        /* ---------------------- DIM 4 -------------------- */
        inipos = endpos;
        inival = PtrDomA->ModValues[3][inipos];
        
        indL = (PtrDomA->ModSubDom+i)->EndPos[3];
        endpos = indL  + dim4A*(indK + dim3A*(indJ + dim2A*indI) );

        endval = PtrDomA->ModValues[3][endpos];

        /* Lower bound is the maximum of lower bounds */
        if (inival < DomA[0][3]) {
           if ( endval < DomA[0][3] ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomA->ModValues[3], inipos, endpos, DomA[0][3],
                                       1, PtrDomA->ModNumPts, 4, 4 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomA].IniVal[3] = PtrDomA->ModValues[3][ind];
        }
        else{
           if (inival > DomA[1][3]) break;   /* the subdomain is out of area */
           auxlst[NSDomA].IniVal[3] = inival;
        }

        /* Upper bound is the minimum of upper bounds */
        if (DomA[1][3] < endval) {
           if ( DomA[1][3] < inival ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomA->ModValues[3], inipos, endpos, DomA[1][3],
                                      -1, PtrDomA->ModNumPts, 4, 4 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomA].EndVal[3] = PtrDomA->ModValues[3][ind];
        }
        else {
           if (endval < DomA[0][3]) break;   /* the subdomain is outbound */
           auxlst[NSDomA].EndVal[3] = endval;
        }

/********** Attention: This is only if the data is figured out from here
                       but if the values are sent to the processor to
                       calculate whether it is data to send or not        **********/
//         if ( (auxlst[NSDomA].EndVal[2] == inival) &&  /* the left limit is open */
//              (auxlst[NSDomA].IniVal[2] == inival) &&
//              (inipos != 0) )                  /*  excepting the domain boundary */
//            break;
        dimflag++;

      case 3:
        /* ---------------------- DIM 1 -------------------- */
        indI = (PtrDomA->ModSubDom+i)->IniPos[0];
        indJ = (PtrDomA->ModSubDom+i)->IniPos[1];
        indK = (PtrDomA->ModSubDom+i)->IniPos[2];
        inipos = indK + dim3A*(indJ + dim2A*indI);

        indI = (PtrDomA->ModSubDom+i)->EndPos[0];
        endpos = indK + dim3A*(indJ + dim2A*indI);

        inival = PtrDomA->ModValues[0][inipos];
        endval = PtrDomA->ModValues[0][endpos];

        /* Lower bound is the maximum of lower bounds */
        if (inival < DomA[0][0]) {
           if ( endval < DomA[0][0] ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomA->ModValues[0], inipos, endpos, DomA[0][0],
                                       1, PtrDomA->ModNumPts, 3, 1 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomA].IniVal[0] = PtrDomA->ModValues[0][ind];
        }
        else{
           if (inival > DomA[1][0]) break;   /* the subdomain is out of area */
           auxlst[NSDomA].IniVal[0] = inival;
        }

        /* Upper bound is the minimum of upper bounds */
        if (DomA[1][0] < endval) {
           if ( DomA[1][0] < inival ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomA->ModValues[0], inipos, endpos, DomA[1][0],
                                      -1, PtrDomA->ModNumPts, 3, 1 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomA].EndVal[0] = PtrDomA->ModValues[0][ind];
        }
        else {
           if (endval < DomA[0][0]) break;   /* the subdomain is outbound */
           auxlst[NSDomA].EndVal[0] = endval;
        }

/********** Attention: This is only if the data is figured out from here
                       but if the values are sent to the processor to
                       calculate whether it is data to send or not        **********/
//         if ( (auxlst[NSDomA].EndVal[0] == inival) &&  /* the left limit is open */
//              (auxlst[NSDomA].IniVal[0] == inival) &&
//              (inipos != 0) )                  /*  excepting the domain boundary */
//            break;
        dimflag++;

        /* ---------------------- DIM 2 -------------------- */
        inipos = endpos;
        inival = PtrDomA->ModValues[1][inipos];;
        
        indJ = (PtrDomA->ModSubDom+i)->EndPos[1];
        endpos = indK + dim3A*(indJ + dim2A*indI);

        endval = PtrDomA->ModValues[1][endpos];

        /* Lower bound is the maximum of lower bounds */
        if (inival < DomA[0][1]) {
           if ( endval < DomA[0][1] ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomA->ModValues[1], inipos, endpos, DomA[0][1],
                                       1, PtrDomA->ModNumPts, 3, 2 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomA].IniVal[1] = PtrDomA->ModValues[1][ind];
        }
        else{
           if (inival > DomA[1][1]) break;   /* the subdomain is out of area */
           auxlst[NSDomA].IniVal[1] = inival;
        }

        /* Upper bound is the minimum of upper bounds */
        if (DomA[1][1] < endval) {
           if ( DomA[1][1] < inival ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomA->ModValues[1], inipos, endpos, DomA[1][1],
                                      -1, PtrDomA->ModNumPts, 3, 2 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomA].EndVal[1] = PtrDomA->ModValues[1][ind];
        }
        else {
           if (endval < DomA[0][1]) break;   /* the subdomain is outbound */
           auxlst[NSDomA].EndVal[1] = endval;
        }

/********** Attention: This is only if the data is figured out from here
                       but if the values are sent to the processor to
                       calculate whether it is data to send or not        **********/
//         if ( (auxlst[NSDomA].EndVal[1] == inival) &&  /* the left limit is open */
//              (auxlst[NSDomA].IniVal[1] == inival) &&
//              (inipos != 0) )                  /*  excepting the domain boundary */
//            break;

        dimflag++;

        /* ---------------------- DIM 3 -------------------- */
        inipos = endpos;
        inival = PtrDomA->ModValues[2][inipos];
        
        indK = (PtrDomA->ModSubDom+i)->EndPos[2];
        endpos = indK + dim3A*(indJ + dim2A*indI);

        endval = PtrDomA->ModValues[2][endpos];

        /* Lower bound is the maximum of lower bounds */
        if (inival < DomA[0][2]) {
           if ( endval < DomA[0][2] ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomA->ModValues[2], inipos, endpos, DomA[0][2],
                                       1, PtrDomA->ModNumPts, 3, 3 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomA].IniVal[2] = PtrDomA->ModValues[2][ind];
        }
        else{
           if (inival > DomA[1][2]) break;   /* the subdomain is out of area */
           auxlst[NSDomA].IniVal[2] = inival;
        }

        /* Upper bound is the minimum of upper bounds */
        if (DomA[1][2] < endval) {
           if ( DomA[1][2] < inival ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomA->ModValues[2], inipos, endpos, DomA[1][2],
                                      -1, PtrDomA->ModNumPts, 3, 3 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomA].EndVal[2] = PtrDomA->ModValues[2][ind];
        }
        else {
           if (endval < DomA[0][2]) break;   /* the subdomain is outbound */
           auxlst[NSDomA].EndVal[2] = endval;
        }

/********** Attention: This is only if the data is figured out from here
                       but if the values are sent to the processor to
                       calculate whether it is data to send or not        **********/
//         if ( (auxlst[NSDomA].EndVal[2] == inival) &&  /* the left limit is open */
//              (auxlst[NSDomA].IniVal[2] == inival) &&
//              (inipos != 0) )                  /*  excepting the domain boundary */
//            break;
        dimflag++;
        break;
      case 2:
        /* ---------------------- DIM 1 -------------------- */
        indI = (PtrDomA->ModSubDom+i)->IniPos[0];
        indJ = (PtrDomA->ModSubDom+i)->IniPos[1];
        inipos = indJ + dim2A*indI;

        indI = (PtrDomA->ModSubDom+i)->EndPos[0];
        endpos = indJ + dim2A*indI;

        inival = PtrDomA->ModValues[0][inipos];
        endval = PtrDomA->ModValues[0][endpos];

        /* Lower bound is the maximum of lower bounds */
        if (inival < DomA[0][0]) {
           if ( endval < DomA[0][0] ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomA->ModValues[0], inipos, endpos, DomA[0][0],
                                       1, PtrDomA->ModNumPts, 2, 1 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomA].IniVal[0] = PtrDomA->ModValues[0][ind];
        }
        else{
           if (inival > DomA[1][0]) break;   /* the subdomain is out of area */
           auxlst[NSDomA].IniVal[0] = inival;
        }

        /* Upper bound is the minimum of upper bounds */
        if (DomA[1][0] < endval) {
           if ( DomA[1][0] < inival ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomA->ModValues[0], inipos, endpos, DomA[1][0],
                                      -1, PtrDomA->ModNumPts, 2, 1 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomA].EndVal[0] = PtrDomA->ModValues[0][ind];
        }
        else {
           if (endval < DomA[0][0]) break;   /* the subdomain is outbound */
           auxlst[NSDomA].EndVal[0] = endval;
        }

/********** Attention: This is only if the data is figured out from here
                       but if the values are sent to the processor to
                       calculate whether it is data to send or not        **********/
//         if ( (auxlst[NSDomA].EndVal[0] == inival) &&  /* the left limit is open */
//              (auxlst[NSDomA].IniVal[0] == inival) &&
//              (inipos != 0) )                  /*  excepting the domain boundary */
//            break;
        dimflag++;

        /* ---------------------- DIM 2 -------------------- */
        inipos = endpos;
        inival = PtrDomA->ModValues[1][inipos];
        
        indJ = (PtrDomA->ModSubDom+i)->EndPos[1];
        endpos = indJ + dim2A*indI;

        endval = PtrDomA->ModValues[1][endpos];

        /* Lower bound is the maximum of lower bounds */
        if (inival < DomA[0][1]) {
           if ( endval < DomA[0][1] ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomA->ModValues[1], inipos, endpos, DomA[0][1],
                                       1, PtrDomA->ModNumPts, 2, 2 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomA].IniVal[1] = PtrDomA->ModValues[1][ind];
        }
        else{
           if (inival > DomA[1][1]) break;   /* the subdomain is out of area */
           auxlst[NSDomA].IniVal[1] = inival;
        }

        /* Upper bound is the minimum of upper bounds */
        if (DomA[1][1] < endval) {
           if ( DomA[1][1] < inival ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomA->ModValues[1], inipos, endpos, DomA[1][1],
                                      -1, PtrDomA->ModNumPts, 2, 2 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomA].EndVal[1] = PtrDomA->ModValues[1][ind];
        }
        else {
           if (endval < DomA[0][1]) break;   /* the subdomain is outbound */
           auxlst[NSDomA].EndVal[1] = endval;
        }

/********** Attention: This is only if the data is figured out from here
                       but if the values are sent to the processor to
                       calculate whether it is data to send or not        **********/
//         if ( (auxlst[NSDomA].EndVal[1] == inival) &&  /* the left limit is open */
//              (auxlst[NSDomA].IniVal[1] == inival) &&
//              (inipos != 0) )                  /*  excepting the domain boundary */
//            break;

        dimflag++;
    } /* switch(dim) */

    if (dimflag == dim) {
      auxlst[NSDomA].RankProc = (PtrDomA->ModSubDom+i)->RankProc;
      NSDomA++;  /* The subdomain (intersection) is registered */
    }
  } /* for (i=0; i < NumSubDomA; i++ ) */

/* The structure es temporary so, I think this is not nessesary. Just assign  
   the value is enough                                                        */
  SDomAlst = (DCT_Intersect_SubDom *) malloc( sizeof(DCT_Intersect_SubDom)*(size_t)NSDomA );
  DCTERROR( SDomAlst == (DCT_Intersect_SubDom *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  for(i=0; i< NSDomA; i++ ){
    SDomAlst[i].IniVal = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
    DCTERROR( SDomAlst[i].IniVal == (DCT_Scalar *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    SDomAlst[i].EndVal = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
    DCTERROR( SDomAlst[i].EndVal == (DCT_Scalar *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    SDomAlst[i].RankProc = auxlst[i].RankProc;
    for ( j=0; j < dim; j++ ) {
      SDomAlst[i].IniVal[j] = auxlst[i].IniVal[j];
      SDomAlst[i].EndVal[j] = auxlst[i].EndVal[j];
    }
  }

  /* Determining which Subdomains in ModB lie in the intersection area */
  /* Determining which Subdomains in ModA lie in the intersection area */
  NSDomB = 0;
  for (i=0; i < NumSubDomB; i++ ) {
    dimflag = 0;
    switch(dim) {
      case 4:
        /* ---------------------- DIM 1 -------------------- */
        indI = (PtrDomB->ModSubDom+i)->IniPos[0];
        indJ = (PtrDomB->ModSubDom+i)->IniPos[1];
        indK = (PtrDomB->ModSubDom+i)->IniPos[2];
        indL = (PtrDomB->ModSubDom+i)->IniPos[3];
        inipos = indL  + dim4B*(indK + dim3B*(indJ + dim2B*indI) );

        indI = (PtrDomB->ModSubDom+i)->EndPos[0];
        endpos = indL  + dim4B*(indK + dim3B*(indJ + dim2B*indI) );

        inival = PtrDomB->ModValues[0][inipos];
        endval = PtrDomB->ModValues[0][endpos];

        /* Lower bound is the maximum of lower bounds */
        if (inival < DomA[0][0]) {
           if ( endval < DomA[0][0] ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomB->ModValues[0], inipos, endpos, DomA[0][0],
                                       1, PtrDomB->ModNumPts, 4, 1 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomB].IniVal[0] = PtrDomB->ModValues[0][ind];
        }
        else{
           if (inival > DomA[1][0]) break;   /* the subdomain is out of area */
           auxlst[NSDomB].IniVal[0] = inival;
        }

        /* Upper bound is the minimum of upper bounds */
        if (DomA[1][0] < endval) {
           if ( DomA[1][0] < inival ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomB->ModValues[0], inipos, endpos, DomA[1][0],
                                      -1, PtrDomB->ModNumPts, 4, 1 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomB].EndVal[0] = PtrDomB->ModValues[0][ind];
        }
        else {
           if (endval < DomA[0][0]) break;   /* the subdomain is outbound */
           auxlst[NSDomB].EndVal[0] = endval;
        }

/********** Attention: This is only if the data is figured out from here
                       but if the values are sent to the processor to
                       calculate whether it is data to send or not        **********/
//         if ( (auxlst[NSDomB].EndVal[0] == inival) &&  /* the left limit is open */
//              (auxlst[NSDomB].IniVal[0] == inival) &&
//              (inipos != 0) )                  /*  excepting the domain boundary */
//            break;
        dimflag++;

        /* ---------------------- DIM 2 -------------------- */
        inipos = endpos;
        inival = PtrDomB->ModValues[1][inipos];
        
        indJ = (PtrDomB->ModSubDom+i)->EndPos[1];
        endpos = indL  + dim4B*(indK + dim3B*(indJ + dim2B*indI) );

        endval = PtrDomB->ModValues[1][endpos];

        /* Lower bound is the maximum of lower bounds */
        if (inival < DomA[0][1]) {
           if ( endval < DomA[0][1] ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomB->ModValues[1], inipos, endpos, DomA[0][1],
                                       1, PtrDomB->ModNumPts, 4, 2 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomB].IniVal[1] = PtrDomB->ModValues[1][ind];
        }
        else{
           if (inival > DomA[1][1]) break;   /* the subdomain is out of area */
           auxlst[NSDomB].IniVal[1] = inival;
        }

        /* Upper bound is the minimum of upper bounds */
        if (DomA[1][1] < endval) {
           if ( DomA[1][1] < inival ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomB->ModValues[1], inipos, endpos, DomA[1][1],
                                      -1, PtrDomB->ModNumPts, 4, 2 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomB].EndVal[1] = PtrDomB->ModValues[1][ind];
        }
        else {
           if (endval < DomA[0][1]) break;   /* the subdomain is outbound */
           auxlst[NSDomB].EndVal[1] = endval;
        }

/********** Attention: This is only if the data is figured out from here
                       but if the values are sent to the processor to
                       calculate whether it is data to send or not        **********/
//         if ( (auxlst[NSDomB].EndVal[1] == inival) &&  /* the left limit is open */
//              (auxlst[NSDomB].IniVal[1] == inival) &&
//              (inipos != 0) )                  /*  excepting the domain boundary */
//            break;

        dimflag++;

        /* ---------------------- DIM 3 -------------------- */
        inipos = endpos;
        inival = PtrDomB->ModValues[2][inipos];
        
        indK = (PtrDomB->ModSubDom+i)->EndPos[2];
        endpos = indL  + dim4B*(indK + dim3B*(indJ + dim2B*indI) );

        endval = PtrDomB->ModValues[2][endpos];

        /* Lower bound is the maximum of lower bounds */
        if (inival < DomA[0][2]) {
           if ( endval < DomA[0][2] ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomB->ModValues[2], inipos, endpos, DomA[0][2],
                                       1, PtrDomB->ModNumPts, 4, 3 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomB].IniVal[2] = PtrDomB->ModValues[2][ind];
        }
        else{
           if (inival > DomA[1][2]) break;   /* the subdomain is out of area */
           auxlst[NSDomB].IniVal[2] = inival;
        }

        /* Upper bound is the minimum of upper bounds */
        if (DomA[1][2] < endval) {
           if ( DomA[1][2] < inival ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomB->ModValues[2], inipos, endpos, DomA[1][2],
                                      -1, PtrDomB->ModNumPts, 4, 3 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomB].EndVal[2] = PtrDomB->ModValues[2][ind];
        }
        else {
           if (endval < DomA[0][2]) break;   /* the subdomain is outbound */
           auxlst[NSDomB].EndVal[2] = endval;
        }

/********** Attention: This is only if the data is figured out from here
                       but if the values are sent to the processor to
                       calculate whether it is data to send or not        **********/
//         if ( (auxlst[NSDomB].EndVal[2] == inival) &&  /* the left limit is open */
//              (auxlst[NSDomB].IniVal[2] == inival) &&
//              (inipos != 0) )                  /*  excepting the domain boundary */
//            break;
        dimflag++;

        /* ---------------------- DIM 4 -------------------- */
        inipos = endpos;
        inival = PtrDomB->ModValues[3][inipos];
       
        indL = (PtrDomB->ModSubDom+i)->EndPos[3];
        endpos = indL  + dim4B*(indK + dim3B*(indJ + dim2B*indI) );

        endval = PtrDomB->ModValues[3][endpos];

        /* Lower bound is the maximum of lower bounds */
        if (inival < DomA[0][3]) {
           if ( endval < DomA[0][3] ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomB->ModValues[3], inipos, endpos, DomA[0][3],
                                       1, PtrDomB->ModNumPts, 4, 4 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomB].IniVal[3] = PtrDomB->ModValues[3][ind];
        }
        else{
           if (inival > DomA[1][3]) break;   /* the subdomain is out of area */
           auxlst[NSDomB].IniVal[3] = inival;
        }

        /* Upper bound is the minimum of upper bounds */
        if (DomA[1][3] < endval) {
           if ( DomA[1][3] < inival ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomB->ModValues[3], inipos, endpos, DomA[1][3],
                                      -1, PtrDomB->ModNumPts, 4, 4 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomB].EndVal[3] = PtrDomB->ModValues[3][ind];
        }
        else {
           if (endval < DomA[0][3]) break;   /* the subdomain is outbound */
           auxlst[NSDomB].EndVal[3] = endval;
        }

/********** Attention: This is only if the data is figured out from here
                       but if the values are sent to the processor to
                       calculate whether it is data to send or not        **********/
//         if ( (auxlst[NSDomB].EndVal[2] == inival) &&  /* the left limit is open */
//              (auxlst[NSDomB].IniVal[2] == inival) &&
//              (inipos != 0) )                  /*  excepting the domain boundary */
//            break;
        dimflag++;

      case 3:
        /* ---------------------- DIM 1 -------------------- */
        indI = (PtrDomB->ModSubDom+i)->IniPos[0];
        indJ = (PtrDomB->ModSubDom+i)->IniPos[1];
        indK = (PtrDomB->ModSubDom+i)->IniPos[2];
        inipos = indK + dim3B*(indJ + dim2B*indI);

        indI = (PtrDomB->ModSubDom+i)->EndPos[0];
        endpos = indK + dim3B*(indJ + dim2B*indI);

        inival = PtrDomB->ModValues[0][inipos];
        endval = PtrDomB->ModValues[0][endpos];

        /* Lower bound is the maximum of lower bounds */
        if (inival < DomA[0][0]) {
           if ( endval < DomA[0][0] ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomB->ModValues[0], inipos, endpos, DomA[0][0],
                                       1, PtrDomB->ModNumPts, 3, 1 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomB].IniVal[0] = PtrDomB->ModValues[0][ind];
        }
        else{
           if (inival > DomA[1][0]) break;   /* the subdomain is out of area */
           auxlst[NSDomB].IniVal[0] = inival;
        }

        /* Upper bound is the minimum of upper bounds */
        if (DomA[1][0] < endval) {
           if ( DomA[1][0] < inival ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomB->ModValues[0], inipos, endpos, DomA[1][0],
                                      -1, PtrDomB->ModNumPts, 3, 1 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomB].EndVal[0] = PtrDomB->ModValues[0][ind];
        }
        else {
           if (endval < DomA[0][0]) break;   /* the subdomain is outbound */
           auxlst[NSDomB].EndVal[0] = endval;
        }

/********** Attention: This is only if the data is figured out from here
                       but if the values are sent to the processor to
                       calculate whether it is data to send or not        **********/
//         if ( (auxlst[NSDomB].EndVal[0] == inival) &&  /* the left limit is open */
//              (auxlst[NSDomB].IniVal[0] == inival) &&
//              (inipos != 0) )                  /*  excepting the domain boundary */
//            break;
        dimflag++;

        /* ---------------------- DIM 2 -------------------- */
        inipos = endpos;
        inival = PtrDomB->ModValues[1][inipos];
       
        indJ = (PtrDomB->ModSubDom+i)->EndPos[1];
        endpos = indK + dim3B*(indJ + dim2B*indI);

        endval = PtrDomB->ModValues[1][endpos];

        /* Lower bound is the maximum of lower bounds */
        if (inival < DomA[0][1]) {
           if ( endval < DomA[0][1] ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomB->ModValues[1], inipos, endpos, DomA[0][1],
                                       1, PtrDomB->ModNumPts, 3, 2 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomB].IniVal[1] = PtrDomB->ModValues[1][ind];
        }
        else{
           if (inival > DomA[1][1]) break;   /* the subdomain is out of area */
           auxlst[NSDomB].IniVal[1] = inival;
        }

        /* Upper bound is the minimum of upper bounds */
        if (DomA[1][1] < endval) {
           if ( DomA[1][1] < inival ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomB->ModValues[1], inipos, endpos, DomA[1][1],
                                      -1, PtrDomB->ModNumPts, 3, 2 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomB].EndVal[1] = PtrDomB->ModValues[1][ind];
        }
        else {
           if (endval < DomA[0][1]) break;   /* the subdomain is outbound */
           auxlst[NSDomB].EndVal[1] = endval;
        }

/********** Attention: This is only if the data is figured out from here
                       but if the values are sent to the processor to
                       calculate whether it is data to send or not        **********/
//         if ( (auxlst[NSDomB].EndVal[1] == inival) &&  /* the left limit is open */
//              (auxlst[NSDomB].IniVal[1] == inival) &&
//              (inipos != 0) )                  /*  excepting the domain boundary */
//            break;

        dimflag++;

        /* ---------------------- DIM 3 -------------------- */
        inipos = endpos;
        inival = PtrDomB->ModValues[2][inipos];
        
        indK = (PtrDomB->ModSubDom+i)->EndPos[2];
        endpos = indK + dim3B*(indJ + dim2B*indI);

        endval = PtrDomB->ModValues[2][endpos];

        /* Lower bound is the maximum of lower bounds */
        if (inival < DomA[0][2]) {
           if ( endval < DomA[0][2] ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomB->ModValues[2], inipos, endpos, DomA[0][2],
                                       1, PtrDomB->ModNumPts, 3, 3 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomB].IniVal[2] = PtrDomB->ModValues[2][ind];
        }
        else{
           if (inival > DomA[1][2]) break;   /* the subdomain is out of area */
           auxlst[NSDomB].IniVal[2] = inival;
        }

        /* Upper bound is the minimum of upper bounds */
        if (DomA[1][2] < endval) {
           if ( DomA[1][2] < inival ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomB->ModValues[2], inipos, endpos, DomA[1][2],
                                      -1, PtrDomB->ModNumPts, 3, 3 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomB].EndVal[2] = PtrDomB->ModValues[2][ind];
        }
        else {
           if (endval < DomA[0][2]) break;   /* the subdomain is outbound */
           auxlst[NSDomB].EndVal[2] = endval;
        }

/********** Attention: This is only if the data is figured out from here
                       but if the values are sent to the processor to
                       calculate whether it is data to send or not        **********/
//         if ( (auxlst[NSDomB].EndVal[2] == inival) &&  /* the left limit is open */
//              (auxlst[NSDomB].IniVal[2] == inival) &&
//              (inipos != 0) )                  /*  excepting the domain boundary */
//            break;
        dimflag++;
        break;
      case 2:
        /* ---------------------- DIM 1 -------------------- */
        indI = (PtrDomB->ModSubDom+i)->IniPos[0];
        indJ = (PtrDomB->ModSubDom+i)->IniPos[1];
        inipos = indJ + dim2B*indI;

        indI = (PtrDomB->ModSubDom+i)->EndPos[0];
        endpos = indJ + dim2B*indI;

        inival = PtrDomB->ModValues[0][inipos];
        endval = PtrDomB->ModValues[0][endpos];

        /* Lower bound is the maximum of lower bounds */
        if (inival < DomA[0][0]) {
           if ( endval < DomA[0][0] ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomB->ModValues[0], inipos, endpos, DomA[0][0],
                                       1, PtrDomB->ModNumPts, 2, 1 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomB].IniVal[0] = PtrDomB->ModValues[0][ind];
        }
        else{
           if (inival > DomA[1][0]) break;   /* the subdomain is out of area */
           auxlst[NSDomB].IniVal[0] = inival;
        }

        /* Upper bound is the minimum of upper bounds */
        if (DomA[1][0] < endval) {
           if ( DomA[1][0] < inival ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomB->ModValues[0], inipos, endpos, DomA[1][0],
                                      -1, PtrDomB->ModNumPts, 2, 1 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomB].EndVal[0] = PtrDomB->ModValues[0][ind];
        }
        else {
           if (endval < DomA[0][0]) break;   /* the subdomain is outbound */
           auxlst[NSDomB].EndVal[0] = endval;
        }

/********** Attention: This is only if the data is figured out from here
                       but if the values are sent to the processor to
                       calculate whether it is data to send or not        **********/
//         if ( (auxlst[NSDomB].EndVal[0] == inival) &&  /* the left limit is open */
//              (auxlst[NSDomB].IniVal[0] == inival) &&
//              (inipos != 0) )                  /*  excepting the domain boundary */
//            break;
        dimflag++;

        /* ---------------------- DIM 2 -------------------- */
        inipos = endpos;
        inival = PtrDomB->ModValues[1][inipos];
       
        indJ = (PtrDomB->ModSubDom+i)->EndPos[1];
        endpos = indJ + dim2B*indI;

        endval = PtrDomB->ModValues[1][endpos];

        /* Lower bound is the maximum of lower bounds */
        if (inival < DomA[0][1]) {
           if ( endval < DomA[0][1] ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomB->ModValues[1], inipos, endpos, DomA[0][1],
                                       1, PtrDomB->ModNumPts, 2, 2 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomB].IniVal[1] = PtrDomB->ModValues[1][ind];
        }
        else{
           if (inival > DomA[1][1]) break;   /* the subdomain is out of area */
           auxlst[NSDomB].IniVal[1] = inival;
        }

        /* Upper bound is the minimum of upper bounds */
        if (DomA[1][1] < endval) {
           if ( DomA[1][1] < inival ) break; /* the subdomain is out of area */
           ind = DCT_GC_index_bsearch(PtrDomB->ModValues[1], inipos, endpos, DomA[1][1],
                                      -1, PtrDomB->ModNumPts, 2, 2 );
           DCTERROR( ind == -1, "Error serching subdomain limits",
                     ierr.Error_code  =  DCT_UNKNOWN_ERROR; return(ierr) );
           auxlst[NSDomB].EndVal[1] = PtrDomB->ModValues[1][ind];
        }
        else {
           if (endval < DomA[0][1]) break;   /* the subdomain is outbound */
           auxlst[NSDomB].EndVal[1] = endval;
        }

/********** Attention: This is only if the data is figured out from here
                       but if the values are sent to the processor to
                       calculate whether it is data to send or not        **********/
//         if ( (auxlst[NSDomB].EndVal[1] == inival) &&  /* the left limit is open */
//              (auxlst[NSDomB].IniVal[1] == inival) &&
//              (inipos != 0) )                  /*  excepting the domain boundary */
//            break;

        dimflag++;
    } /* switch(dim) */

    if (dimflag == dim) {
      auxlst[NSDomB].RankProc = (PtrDomB->ModSubDom+i)->RankProc;
      NSDomB++;  /* The subdomain (intersection) is registered */
    }
  } /* for (i=0; i < NumSubDomB; i++ ) */

/* The structure es temporary so, I think this is not nessesary. Just assign  
   the value is enough                                                        */
  SDomBlst = (DCT_Intersect_SubDom *) malloc( sizeof(DCT_Intersect_SubDom)*(size_t)NSDomB );
  DCTERROR( SDomBlst == (DCT_Intersect_SubDom *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  for(i=0; i< NSDomB; i++ ){
    SDomBlst[i].IniVal = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
    DCTERROR( SDomBlst[i].IniVal == (DCT_Scalar *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    SDomBlst[i].EndVal = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
    DCTERROR( SDomBlst[i].EndVal == (DCT_Scalar *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    SDomBlst[i].RankProc = auxlst[i].RankProc;
    for ( j=0; j < dim; j++ ) {
      SDomBlst[i].IniVal[j] = auxlst[i].IniVal[j];
      SDomBlst[i].EndVal[j] = auxlst[i].EndVal[j];
    }
  }

  /*                                                   BEGIN DEBUG */
  /*printf("Name: %s\n", modA->ModName);
  printf("Total Number of subdomains in COMMON AREA: %d\n", NSDomA);
  for(i=0; i<NSDomA; i++){
    printf("Rank in subdomain %d: %d\n", i, SDomAlst[i].RankProc);
    for ( j=0; j < dim; j++ ) {
      printf("IniVal Direction %d in subdomain %d: %f\n", j, i, SDomAlst[i].IniVal[j]);
      printf("EndVal Direction %d in subdomain %d: %f\n", j, i, SDomAlst[i].EndVal[j]);
    }
  }*/
  /*                                                     END DEBUG */
  /*                                                   BEGIN DEBUG */
  /*printf("Name: %s\n", modB->ModName);
  printf("Total Number of subdomains in COMMON AREA: %d\n", NSDomB);
  for(i=0; i<NSDomB; i++){
    printf("Rank in subdomain %d: %d\n", i, SDomBlst[i].RankProc);
    for ( j=0; j < dim; j++ ) {
      printf("IniVal Direction %d in subdomain %d: %f\n", j, i, SDomBlst[i].IniVal[j]);
      printf("EndVal Direction %d in subdomain %d: %f\n", j, i, SDomBlst[i].EndVal[j]);
    }
  }*/
  /*                                                     END DEBUG */

  /* Deleting Temporary array */
  for(i=0; i < maxsd; i++ ){
    free( auxlst[i].IniVal );
    free( auxlst[i].EndVal );
  }
  free( auxlst );


  /* Temporary array */
  maxsd = (NSDomB > NSDomA? NSDomB : NSDomA );
  SDauxSen = (DCT_SDProduce_Plan *) malloc( sizeof(DCT_SDProduce_Plan)*(size_t)maxsd );
  DCTERROR( SDauxSen == (DCT_SDProduce_Plan *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  for(i=0; i < maxsd; i++ ){
    SDauxSen[i].Msend = (DCT_SD_Produce *) malloc(sizeof(DCT_SD_Produce)*(size_t)maxsd);
    DCTERROR( SDauxSen[i].Msend == (DCT_SD_Produce *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    for(j=0; j < maxsd; j++ ){
      SDauxSen[i].Msend[j].IniVal = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
      DCTERROR( SDauxSen[i].Msend[j].IniVal == (DCT_Scalar *)NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
      SDauxSen[i].Msend[j].EndVal = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
      DCTERROR( SDauxSen[i].Msend[j].EndVal == (DCT_Scalar *)NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    }
  }

  
  /**** Communication  Model A (producer) to Model B (consumer) *****/
  /* Establishing how the SubDomains in Model A fit in those in Model B
     to figure out what area the model A is going to be sent to Model B */


  /* Store for each Model B receiver, the rank of Model A senders */
  ModelARcv = (DCT_SDConsume_Plan *) malloc( sizeof(DCT_SDConsume_Plan)*(size_t)NSDomB );
  DCTERROR( ModelARcv == (DCT_SDConsume_Plan *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  for (i=0; i < NSDomB; i++ ) {
     ModelARcv[i].Nmsg = 0;
     ModelARcv[i].Recvr = SDomBlst[i].RankProc;
     ModelARcv[i].SDSlab = (DCT_SD_Consume *) malloc( sizeof(DCT_Rank)*(size_t)NSDomA );
     DCTERROR( ModelARcv[i].SDSlab == (DCT_SD_Consume *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  }
            
  /* For each Model A subdomain, detect the the intersection of
     Model B subdomains */
  for( i=0; i < NSDomA; i++ ) {
    k = 0;
    SDauxSen[i].Sendr = SDomAlst[i].RankProc; /* The sender rank is registered */
    for( j=0; j < NSDomB; j++ ) {
      dimflag = 0;
      switch (dim) {
      case 4:
        /* Upper bound is the minimum of upper bounds */
        SDauxSen[i].Msend[k].EndVal[3] =
             ( (SDomBlst[j].EndVal[3] <= SDomAlst[i].EndVal[3] ) ?
               SDomBlst[j].EndVal[3] : SDomAlst[i].EndVal[3] );

        /* Lower bound is the maximum of lower bounds */
        SDauxSen[i].Msend[k].IniVal[3] =
          ( (SDomBlst[j].IniVal[3] >= SDomAlst[i].IniVal[3] ) ?
            SDomBlst[j].IniVal[3] : SDomAlst[i].IniVal[3] );
        if (SDauxSen[i].Msend[k].EndVal[3] < SDauxSen[i].Msend[k].IniVal[3])
          break; /* No common area */
        dimflag++;
      case 3:
        /* Upper bound is the minimum of upper bounds */
        SDauxSen[i].Msend[k].EndVal[2] =
             ( (SDomBlst[j].EndVal[2] <= SDomAlst[i].EndVal[2] ) ?
               SDomBlst[j].EndVal[2] : SDomAlst[i].EndVal[2] );

        /* Lower bound is the maximum of lower bounds */
        SDauxSen[i].Msend[k].IniVal[2] =
          ( (SDomBlst[j].IniVal[2] >= SDomAlst[i].IniVal[2] ) ?
            SDomBlst[j].IniVal[2] : SDomAlst[i].IniVal[2] );
        if (SDauxSen[i].Msend[k].EndVal[2] < SDauxSen[i].Msend[k].IniVal[2])
          break; /* No common area */
        dimflag++;
      case 2:
        /* Upper bound is the minimum of upper bounds */
        SDauxSen[i].Msend[k].EndVal[1] =
             ( (SDomBlst[j].EndVal[1] <= SDomAlst[i].EndVal[1] ) ?
               SDomBlst[j].EndVal[1] : SDomAlst[i].EndVal[1] );

        /* Lower bound is the maximum of lower bounds */
        SDauxSen[i].Msend[k].IniVal[1] =
          ( (SDomBlst[j].IniVal[1] >= SDomAlst[i].IniVal[1] ) ?
            SDomBlst[j].IniVal[1] : SDomAlst[i].IniVal[1] );
        if (SDauxSen[i].Msend[k].EndVal[1] < SDauxSen[i].Msend[k].IniVal[1])
          break; /* No common area */
        dimflag++;
        /* ---------------------- DIM 1 -------------------- */
        /* Upper bound is the minimum of upper bounds */
        SDauxSen[i].Msend[k].EndVal[0] =
             ( (SDomBlst[j].EndVal[0] <= SDomAlst[i].EndVal[0] ) ?
               SDomBlst[j].EndVal[0] : SDomAlst[i].EndVal[0] );

        /* Lower bound is the maximum of lower bounds */
        SDauxSen[i].Msend[k].IniVal[0] =
          ( (SDomBlst[j].IniVal[0] >= SDomAlst[i].IniVal[0] ) ?
            SDomBlst[j].IniVal[0] : SDomAlst[i].IniVal[0] );
        if (SDauxSen[i].Msend[k].EndVal[0] < SDauxSen[i].Msend[k].IniVal[0])
          break; /* No common area */
        dimflag++;
      }  /* switch (dim) */

      if (dimflag == dim ) {
        SDauxSen[i].Msend[k].Recvr = SDomBlst[j].RankProc; /* The rcvr is registered */
                                        /* The sndr is registered */
        ModelARcv[j].SDSlab[ModelARcv[j].Nmsg].Sendr = SDomAlst[i].RankProc;
        ModelARcv[j].Nmsg++; /* the j-th receiver count a message */
        
        k++;
      }
    } /*  for( j=0; j < NSDomB; j++ ) */
    SDauxSen[i].Nmsg = k;
    if (k == 0) {
      Msg = (DCT_String) malloc((size_t)150);
      sprintf(Msg,
        "[DCT_BrokerArrange] The subdomain in proc %d, of the model <%s> has no messages to receive\n",
        SDomAlst[i].RankProc, modA->ModName );
      DCTWARN( Msg );
    }

  }  /*  for( i=0; i < NSDomA; i++ ) */

  SDModASen = (DCT_SDProduce_Plan *) malloc( sizeof(DCT_SDProduce_Plan)*(size_t)NSDomA );
  DCTERROR( SDModASen == (DCT_SDProduce_Plan *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  for(i=0; i < NSDomA; i++ ) {
    SDModASen[i].Sendr = SDauxSen[i].Sendr; /* The sender rank is registered */
    ind = SDauxSen[i].Nmsg;
    SDModASen[i].Nmsg = ind;
    SDModASen[i].Msend = (DCT_SD_Produce *) malloc(sizeof(DCT_SD_Produce)* (size_t)ind );
    DCTERROR( SDModASen[i].Msend == (DCT_SD_Produce *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

    for(j=0; j < ind; j++ ){
      SDModASen[i].Msend[j].Recvr = SDauxSen[i].Msend[j].Recvr;

      SDModASen[i].Msend[j].IniVal = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
      DCTERROR( SDModASen[i].Msend[j].IniVal == (DCT_Scalar *)NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
      SDModASen[i].Msend[j].EndVal = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
      DCTERROR( SDModASen[i].Msend[j].EndVal == (DCT_Scalar *)NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
      for (k=0 ; k < dim ; k++ ) {
        SDModASen[i].Msend[j].IniVal[k] = SDauxSen[i].Msend[j].IniVal[k];
        SDModASen[i].Msend[j].EndVal[k] = SDauxSen[i].Msend[j].EndVal[k];
      }
    }
  }

  /*                                                   BEGIN DEBUG */
/*  printf("Model %s sends to %s\n", modA->ModName, modB->ModName);
  printf("Total Number of subdomains in COMMON AREA: %d\n", NSDomA);
  for(i=0; i<NSDomA; i++){
    printf("\nSubdomain %d, with processor %d\n", i, SDModASen[i].Sendr);
    printf("is going to send %d SubDomains package\n", SDModASen[i].Nmsg);
    for ( j=0; j < SDModASen[i].Nmsg; j++ ) {
      printf("To the process: %d\n",
              SDModASen[i].Msend[j].Recvr );
      for (k=0 ; k<dim ; k++ ){
        printf("Direction %d, IniVal = %f\n", k, SDModASen[i].Msend[j].IniVal[k]);
        printf("Direction %d, EndVal = %f\n", k, SDModASen[i].Msend[j].EndVal[k]);
      }
    }
  }*/
  /*                                                     END DEBUG */


  /**** Communication  Model B (producer) to Model A (consumer) *****/
  /* Establishing how the SubDomains in Model A fit in those in Model B
     to figure out what area the model B is going to be sent to Model A */
  
  /* Store for each Model B receiver, the rank of Model A senders */
  ModelBRcv = (DCT_SDConsume_Plan *) malloc( sizeof(DCT_SDConsume_Plan)*(size_t)NSDomA );
  DCTERROR( ModelBRcv == (DCT_SDConsume_Plan *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  for (i=0; i < NSDomA; i++ ) {
     ModelBRcv[i].Nmsg = 0;
     ModelBRcv[i].Recvr = SDomAlst[i].RankProc;
     ModelBRcv[i].SDSlab = (DCT_SD_Consume *) malloc( sizeof(DCT_Rank)*(size_t)NSDomB );
     DCTERROR( ModelBRcv[i].SDSlab == (DCT_SD_Consume *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  }
            
  /* For each Model B subdomain, detect the the intersection of
     Model A subdomains */
  for( i=0; i < NSDomB; i++ ) {
    k = 0;
    SDauxSen[i].Sendr = SDomBlst[i].RankProc; /* The sender rank is registered */
    for( j=0; j < NSDomA; j++ ) {
      dimflag = 0;
      switch (dim) {
      case 4:
        /* Upper bound is the minimum of upper bounds */
        SDauxSen[i].Msend[k].EndVal[3] =
             ( (SDomAlst[j].EndVal[3] <= SDomBlst[i].EndVal[3] ) ?
               SDomAlst[j].EndVal[3] : SDomBlst[i].EndVal[3] );

        /* Lower bound is the maximum of lower bounds */
        SDauxSen[i].Msend[k].IniVal[3] =
          ( (SDomAlst[j].IniVal[3] >= SDomBlst[i].IniVal[3] ) ?
            SDomAlst[j].IniVal[3] : SDomBlst[i].IniVal[3] );
        if (SDauxSen[i].Msend[k].EndVal[3] < SDauxSen[i].Msend[k].IniVal[3])
          break; /* No common area */
        dimflag++;
      case 3:
        /* Upper bound is the minimum of upper bounds */
        SDauxSen[i].Msend[k].EndVal[2] =
             ( (SDomAlst[j].EndVal[2] <= SDomBlst[i].EndVal[2] ) ?
               SDomAlst[j].EndVal[2] : SDomBlst[i].EndVal[2] );

        /* Lower bound is the maximum of lower bounds */
        SDauxSen[i].Msend[k].IniVal[2] =
          ( (SDomAlst[j].IniVal[2] >= SDomBlst[i].IniVal[2] ) ?
            SDomAlst[j].IniVal[2] : SDomBlst[i].IniVal[2] );
        if (SDauxSen[i].Msend[k].EndVal[2] < SDauxSen[i].Msend[k].IniVal[2])
          break; /* No common area */
        dimflag++;
      case 2:
        /* Upper bound is the minimum of upper bounds */
        SDauxSen[i].Msend[k].EndVal[1] =
             ( (SDomAlst[j].EndVal[1] <= SDomBlst[i].EndVal[1] ) ?
               SDomAlst[j].EndVal[1] : SDomBlst[i].EndVal[1] );

        /* Lower bound is the maximum of lower bounds */
        SDauxSen[i].Msend[k].IniVal[1] =
          ( (SDomAlst[j].IniVal[1] >= SDomBlst[i].IniVal[1] ) ?
            SDomAlst[j].IniVal[1] : SDomBlst[i].IniVal[1] );
        if (SDauxSen[i].Msend[k].EndVal[1] < SDauxSen[i].Msend[k].IniVal[1])
          break; /* No common area */
        dimflag++;
        /* ---------------------- DIM 1 -------------------- */
        /* Upper bound is the minimum of upper bounds */
        SDauxSen[i].Msend[k].EndVal[0] =
             ( (SDomAlst[j].EndVal[0] <= SDomBlst[i].EndVal[0] ) ?
               SDomAlst[j].EndVal[0] : SDomBlst[i].EndVal[0] );

        /* Lower bound is the maximum of lower bounds */
        SDauxSen[i].Msend[k].IniVal[0] =
          ( (SDomAlst[j].IniVal[0] >= SDomBlst[i].IniVal[0] ) ?
            SDomAlst[j].IniVal[0] : SDomBlst[i].IniVal[0] );
        if (SDauxSen[i].Msend[k].EndVal[0] < SDauxSen[i].Msend[k].IniVal[0])
          break; /* No common area */
        dimflag++;
      }  /* switch (dim) */

      if (dimflag == dim ) {
        SDauxSen[i].Msend[k].Recvr = SDomAlst[j].RankProc; /* The rcvr is registered */
                                        /* The sndr is registered */
        ModelBRcv[j].SDSlab[ModelBRcv[j].Nmsg].Sendr = SDomBlst[i].RankProc;
        ModelBRcv[j].Nmsg++; /* the j-th receiver count a message */

        k++;
      }
    } /*  for( j=0; j < NSDomA; j++ ) */
    SDauxSen[i].Nmsg = k;
    if (k == 0) {
      Msg = (DCT_String) malloc((size_t)150);
      sprintf(Msg,
        "[DCT_BrokerArrange] The subdomain in proc %d, of the model <%s> has no messages to receive\n",
        SDomBlst[i].RankProc, modB->ModName );
      DCTWARN( Msg );
    }
  }  /*  for( i=0; i < NSDomB; i++ ) */

  SDModBSen = (DCT_SDProduce_Plan *) malloc( sizeof(DCT_SDProduce_Plan)*(size_t)NSDomB );
  DCTERROR( SDModBSen == (DCT_SDProduce_Plan *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  for(i=0; i < NSDomB; i++ ){
    SDModBSen[i].Sendr = SDauxSen[i].Sendr; /* The sender rank is registered */
    ind = SDauxSen[i].Nmsg;
    SDModBSen[i].Nmsg = ind;
    SDModBSen[i].Msend = (DCT_SD_Produce *) malloc(sizeof(DCT_SD_Produce)* (size_t)ind );
    DCTERROR( SDModBSen[i].Msend == (DCT_SD_Produce *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

    for(j=0; j < ind; j++ ){
      SDModBSen[i].Msend[j].Recvr = SDauxSen[i].Msend[j].Recvr;
      SDModBSen[i].Msend[j].IniVal = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
      DCTERROR( SDModBSen[i].Msend[j].IniVal == (DCT_Scalar *)NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

      SDModBSen[i].Msend[j].EndVal = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
      DCTERROR( SDModBSen[i].Msend[j].EndVal == (DCT_Scalar *)NULL, "Memory Allocation Failed",
                ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

      for (k=0 ; k < dim ; k++ ) {
        SDModBSen[i].Msend[j].IniVal[k] = SDauxSen[i].Msend[j].IniVal[k];
        SDModBSen[i].Msend[j].EndVal[k] = SDauxSen[i].Msend[j].EndVal[k];
      }
    }
  }

/*                                                   BEGIN DEBUG */
/*  printf("Model %s sends to %s\n", modB->ModName, modA->ModName);
  printf("Total Number of subdomains in COMMON AREA: %d\n", NSDomB);
  for(i=0; i<NSDomB; i++){
    printf("\nSubdomain %d, with processor %d\n", i, SDModASen[i].Sendr);
    printf("is going to send %d SubDomains package\n", SDModBSen[i].Nmsg);
    for ( j=0; j < SDModBSen[i].Nmsg; j++ ) {
      printf("To the process: %d\n",
              SDModBSen[i].Msend[j].Recvr );
      for (k=0 ; k<dim ; k++ ){
        printf("Direction %d, IniVal = %f\n", k, SDModBSen[i].Msend[j].IniVal[k]);
        printf("Direction %d, EndVal = %f\n", k, SDModBSen[i].Msend[j].EndVal[k]);
      }
    }
  }*/
/*                                                     END DEBUG */


  /* The output variables are set */
  *ModASen = SDModASen;
  *nModASD = NSDomA;
  *ModARcv = ModelARcv;
  
  *ModBSen = SDModBSen;
  *nModBSD = NSDomB;
  *ModBRcv = ModelBRcv;



  /* Deleting Temporary array */
  for(i=0; i < maxsd; i++ ){
    for(j=0; j < maxsd; j++ ){
      free( SDauxSen[i].Msend[j].IniVal );
      free( SDauxSen[i].Msend[j].EndVal );
    }
    free( SDauxSen[i].Msend );
  }
  free( SDauxSen );

  /* Deleting dynamically allocated structures */
  free( DomA[0] );
  free( DomA[1] );

  free( DomB[0] );
  free( DomB[1] );

  for(i=0; i< NSDomA; i++ ){
    free( SDomAlst[i].IniVal );
    free( SDomAlst[i].EndVal );
  }
  free( SDomAlst );

  for(i=0; i< NSDomB; i++ ){
    free( SDomBlst[i].IniVal );
    free( SDomBlst[i].EndVal );
  }
  free( SDomBlst );

  ierr.Error_code = DCT_SUCCESS;
  ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* -------------------------------------  END( DCT_BrokerArrangeGC ) */

}

/*******************************************************************/
/*                          DCT_Arrange_SD                         */
/*                                                                 */
/*!    This routine calculates the subdomain overlapping between
       between modA and modB DCT_Model. The calculation is made
       from Model A's point of view; i.e. The process who run the
       routine owns the model A.

         \param[in]     modA  Pointer to DCT_Model model A.
         \param[out] ModASen  An array of the structure
                              describing the SD preliminary
                              communication with modA considered
                              as producer.
         \param[out] ModARcv  An array of the structure
                              describing the SD preliminary
                              communication with modA considered
                              as consumer.

         \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Arrange_SD( const DCT_Model *modA,
                          const DCT_Model *modB, DCT_SDProduce_Plan *ModASen,
                          DCT_SDConsume_Plan *ModARcv)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;

  DCT_Model_Dom        *PtrDomA, *PtrDomB;
  DCT_Integer           dim;
  DCT_Integer           inipos, endpos, iniind ;
  DCT_Scalar           *DomA[2];
  DCT_Scalar           *DomB[2];
  DCT_Scalar            inival, coniniv, endval, conendv;
  DCT_Scalar            IniVal[MAX_DOMAIN_DIM], EndVal[MAX_DOMAIN_DIM];
  DCT_Scalar            ModAIniVal[MAX_DOMAIN_DIM], ModAEndVal[MAX_DOMAIN_DIM];
  DCT_SDProduce_Plan   *SDauxSen;
  DCT_SDConsume_Plan    ModelBRcv; /* Only one is needed */
  DCT_Rank             *ProdNeigh; 

  DCT_SD_Consume       *AuxSDSlab;
  DCT_Scalar          **ModAValues;
  DCT_Scalar          **ModBValues;
  DCT_Data_SubDom      *ModASubDom;
  DCT_Data_SubDom      *ModBSubDom;
  DCT_Domain_Type      *ModADomType;
  DCT_Domain_Type      *ModBDomType;
  DCT_Integer          *ModANumPts;
  DCT_Integer          *ModBNumPts;
  DCT_Integer          *ModAEndPos;
  DCT_Integer          *ModBEndPos;
  DCT_Distribution      ModAParLayout;
  DCT_Distribution      ModBParLayout;
  DCT_Integer          *ModALayoutDim;
  DCT_Integer          *ModBLayoutDim;
  
  DCT_Integer          *ModIniPos, *ModEndPos;
  DCT_Integer          *ModSDLength, *ModSDGlbInd, *ModSDNpts, *ModSDIniPos;
  DCT_Integer           ModSDGlbIndTest[2], ModSDNptsTest[2];
  DCT_Scalar           *ModSDIniVal, *ModSDEndVal;
  DCT_Scalar           *ModSD2IniVal, *ModSD2EndVal;
  DCT_SD_Produce       *ModSDMsend, *ModASendDef;
  DCT_SD_Consume       *ModSDMrecv, *AuxSDrecv;

  DCT_SDProduce_Plan   *ModSDMsendPlan;
  //DCT_SDConsume_Plan   *ModSDMrecvPlan;
  
//   DCT_Scalar           *Values;
  DCT_Rank              rank;
  int                  *indices;
  int                   NSDomA, NSDomB;
  int                   ind, rind, dimflag, maxsd;
  register int          i, j, k, l;


/* --------------------------------------  BEGIN( DCT_Arrange_SD ) */
  /* Getting the model a domain info */
  PtrDomA = modA->ModDomain;
  ModAValues = PtrDomA->ModValues;
  ModASubDom = PtrDomA->ModSubDom;
  ModADomType = PtrDomA->ModDomType;
  ModANumPts = PtrDomA->ModNumPts;
  ModAParLayout = PtrDomA->ModSubDomParLayout;
  ModALayoutDim = PtrDomA->ModSubDomLayoutDim;
  dim = PtrDomA->ModDomDim;
  NSDomA = modA->ModNumProcs;
  
  /* Getting the model b domain info */
  PtrDomB = modB->ModDomain;
  ModBValues = PtrDomB->ModValues;
  ModBSubDom = PtrDomB->ModSubDom;
  ModBDomType = PtrDomB->ModDomType;
  ModBNumPts = PtrDomB->ModNumPts;
  ModBParLayout = PtrDomB->ModSubDomParLayout;
  ModBLayoutDim = PtrDomB->ModSubDomLayoutDim;
  NSDomB = modB->ModNumProcs;

  /* Structures to store the model domain limits */
  DomA[0] = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
  DCTERROR( DomA[0] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  DomA[1] = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
  DCTERROR( DomA[1] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  ModAEndPos = (DCT_Integer *) malloc(sizeof(DCT_Integer)*(size_t)dim);
  DCTERROR( ModAEndPos == (DCT_Integer *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

  DomB[0] = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
  DCTERROR( DomB[0] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  DomB[1] = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
  DCTERROR( DomB[1] == (DCT_Scalar *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  ModBEndPos = (DCT_Integer *) malloc(sizeof(DCT_Integer)*(size_t)dim);
  DCTERROR( ModBEndPos == (DCT_Integer *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

  /************************************************/
  /*   Get domain limit values in model Model A   */
  DCT_Return_DomBoundVals( PtrDomA, DomA, ModAEndPos);
  
  /************************************************/
  /*   Get domain limit values in model Model B   */
  DCT_Return_DomBoundVals( PtrDomB, DomB, ModBEndPos);

  /*****************************************************************************/
  /************** Allocating different auxiliary data structures ***************/

  AuxSDSlab = (DCT_SD_Consume *) malloc(sizeof(DCT_SD_Consume)*(size_t)NSDomB);
  DCTERROR( AuxSDSlab == (DCT_SD_Consume *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  ModSDMrecv = AuxSDSlab;

  for ( i=0; i < NSDomB; i++, ModSDMrecv++ ) {

     ModSDIniPos = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t)dim);
     DCTERROR( ModSDIniPos == (DCT_Integer *)NULL,
               "Memory Allocation Failed", ierr.Error_code  =  DCT_FAILED_MALLOC;
               return(ierr) );
     ModSDMrecv->IniPos = ModSDIniPos;

     ModSDLength = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t)dim);
     DCTERROR( ModSDLength == (DCT_Integer *)NULL,
               "Memory Allocation Failed", ierr.Error_code  =  DCT_FAILED_MALLOC;
               return(ierr) );
     ModSDMrecv->Length = ModSDLength;
  }

  /** Allocating the neighbor to avoid multiple allocations **/
  ProdNeigh = (DCT_Rank *) malloc(sizeof(DCT_Rank)*(size_t)NSDomB);
  DCTERROR( ProdNeigh == (DCT_Rank *)NULL, "Memory Allocation Failed",
               ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

  /* Allocating the DCT_SDProduce_Plan SDauxSen structure to avoid multiple allocation */
  /* In this case is used 2 because is one for don't care, and information for this process */
  SDauxSen = (DCT_SDProduce_Plan *) malloc( sizeof(DCT_SDProduce_Plan)*(size_t)2 );
  DCTERROR( SDauxSen == (DCT_SDProduce_Plan *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  ModSDMsendPlan = SDauxSen;

  for ( i=0; i < 2; i++, ModSDMsendPlan++ ) {
     
     ModSDMsendPlan->Nmsg = 0;
     ModSDMsendPlan->RcvrNSD = 0;
                           /*! \todo   Probably can change NSDomB by ( i == 0 ? NSDomB: 1) */
     ModSDMsend = (DCT_SD_Produce *) malloc( sizeof(DCT_SD_Produce)*(size_t)NSDomB );
     DCTERROR( ModSDMsend == (DCT_SD_Produce *)NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
     ModSDMsendPlan->Msend = ModSDMsend;
     
     for ( j=0; j < NSDomB; j++,  ModSDMsend++ ) {

        ModSDIniVal = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)dim);
        DCTERROR( ModSDIniVal == (DCT_Scalar *)NULL,
                  "Memory Allocation Failed", ierr.Error_code  =  DCT_FAILED_MALLOC;
                  return(ierr) );
        ModSDMsend->IniVal = ModSDIniVal;

        ModSDEndVal = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)dim);
        DCTERROR( ModSDEndVal == (DCT_Scalar *)NULL,
                  "Memory Allocation Failed", ierr.Error_code  =  DCT_FAILED_MALLOC;
                  return(ierr) );
        ModSDMsend->EndVal = ModSDEndVal;

        ModSDLength = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t)dim);
        DCTERROR( ModSDLength == (DCT_Integer *)NULL,
                  "Memory Allocation Failed", ierr.Error_code  =  DCT_FAILED_MALLOC;
                  return(ierr) );
        ModSDMsend->Length = ModSDLength;
     }
     
  }

  /** Array of indices in the producer subdmain **/
  maxsd = ( NSDomA >= NSDomB ? NSDomA: NSDomB );
  indices = (int *) malloc(sizeof(int)*(size_t)maxsd);
  DCTERROR( indices == (int *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

  /* The actual process is identified */
  rank = DCT_procinfo.ProcDCTRank;
  for ( j=0; (j < NSDomA) && ( rank != (ModASubDom + j)->RankProc ); j++ );
  rind = j; /* The actual process index in the subdomain array */

  /*****************************************************************************/
  /********** Communication  Model A (producer) to Model B (consumer) **********/

  /********** Taking the producer domain region (Producer resolution) ***********/
  for (i=0; i < NSDomB; i++ ) {
/*************************************************************************************/
/*! \todo       This pice of code below work only to          
                calculate the producer plan fron THIS proc                           */
/*************************************************************************************/
    /** The boundary values of the subdomain is extracted **/
    DCT_Return_SDBoundaryVals ( dim, (ModBSubDom+i), ModBDomType, ModBEndPos, ModBValues,
                                IniVal, EndVal );
/*************************************************************************************/
    /** Calculates if consumer's subdomain intersects the producer domain **/
    dimflag = DCT_Return_SubDom_Interc( PtrDomA, DomA, ModAEndPos, IniVal, EndVal,
                                        NULL, NULL);
/*************************************************************************************/
    /** Here the intersection with each model A (producer) subdomain **/
    ModelBRcv.Recvr = (ModBSubDom+i)->RankProc;
    if (dimflag == dim) {            /*** Intersecting Model A (producer) subdomains ***/
       k = 0;
       for (j=0; j < NSDomA; j++ ) {
         maxsd = ( j == rind ? 0: 1); /* Make 0 just when the actual pocs SD is processed */
         DCT_Return_SDBoundaryVals ( dim, (ModASubDom+j), ModADomType, ModAEndPos,
                                     ModAValues, ModAIniVal, ModAEndVal );
         ModIniPos = (ModASubDom + j)->IniPos;
         ModEndPos = (ModASubDom + j)->EndPos;
         /* Getting the last spot for the j-th Model A's SD */
         ModSDMsendPlan = SDauxSen + maxsd;
         l = ( j == rind ? ModSDMsendPlan->Nmsg: 0); /* l becomes previous Nmsg for this procs */
         ModSDMsend = ModSDMsendPlan->Msend + l;  /* next available entry of Msend for this procs */
         ModSDIniVal = ModSDMsend->IniVal; /** Extract the addresses of   **/
         ModSDEndVal = ModSDMsend->EndVal; /** the structures entries of  **/
         ModSDLength = ModSDMsend->Length; /** the next empty Msend entry **/
         dimflag = 0;
         switch (dim) {
            case 4:
               inipos = ModIniPos[3];    /* Model A SD Initial Pos   */
               endpos = ModEndPos[3];    /* Model A SD Final Pos     */
               inival = ModAIniVal[3];   /* Model A SD Initial Value */
               endval = ModAEndVal[3];   /* Model A SD Final Value   */
   
               coniniv = IniVal[3];      /* Values where Mod B SD overlaps on  */
               conendv = EndVal[3];      /* Model A Domain resolution */
               
               /* Lower bound is the maximum of lower bounds */
               if (inival < coniniv) {
                  if ( endval < coniniv ) break; /* the subdomain is out of area */
                  iniind = DCT_SearchIndex( ModAValues[3], ModANumPts[3], ModADomType[3], //hA[3],
                                            inipos, endpos, coniniv, -1, (ModSDIniVal+3) );
               }
               else{
                  if (inival > conendv) break;   /* the subdomain is out of area */
                  ModSDIniVal[3] =inival;
                  iniind = inipos;
               }
               
               /* Upper bound is the minimum of upper bounds */
               if (conendv < endval) {
                  if ( conendv < inival ) break; /* the subdomain is out of area */
                  ind = DCT_SearchIndex( ModAValues[3], ModANumPts[3], ModADomType[3], // hA[3],
                                            inipos, endpos, conendv, 1, (ModSDEndVal+3) );
               }
               else {
                  if (endval < coniniv) break;   /* the subdomain is outbound */
                  ModSDEndVal[3] = endval;
                  ind = endpos;
               }
               ModSDLength[3] = ind - iniind + 1;/* The Msed description es completed for dim 4 */
               dimflag++;
            case 3:
               inipos = ModIniPos[2];    /* Model A SD Initial Pos   */
               endpos = ModEndPos[2];    /* Model A SD Final Pos     */               
               inival = ModAIniVal[2];   /* Model A SD Initial Value */
               endval = ModAEndVal[2];   /* Model A SD Final Value   */
   
               coniniv = IniVal[2];      /* Values where Mod B SD overlaps on  */
               conendv = EndVal[2];      /* Model A Domain resolution */
               
               /* Lower bound is the maximum of lower bounds */
               if (inival < coniniv) {
                  if ( endval < coniniv ) break; /* the subdomain is out of area */
                  iniind = DCT_SearchIndex( ModAValues[2], ModANumPts[2], ModADomType[2], // hA[2],
                                            inipos, endpos, coniniv, -1, (ModSDIniVal+2) );
               }
               else{
                  if (inival > conendv) break;   /* the subdomain is out of area */
                  ModSDIniVal[2] =inival;
                  iniind = inipos;
               }
               
               /* Upper bound is the minimum of upper bounds */
               if (conendv < endval) {
                  if ( conendv < inival ) break; /* the subdomain is out of area */
                  ind = DCT_SearchIndex( ModAValues[2], ModANumPts[2], ModADomType[2], // hA[2],
                                            inipos, endpos, conendv, 1, (ModSDEndVal+2) );
               }
               else {
                  if (endval < coniniv) break;   /* the subdomain is outbound */
                  ModSDEndVal[2] = endval;
                  ind = endpos;
               }
               ModSDLength[2] = ind - iniind + 1; /* The Msed description es completed for dim 3 */
               dimflag++;
            case 2:
               inipos = ModIniPos[1];    /* Model A SD Initial Pos   */
               endpos = ModEndPos[1];    /* Model A SD Final Pos     */               
               inival = ModAIniVal[1];   /* Model A SD Initial Value */
               endval = ModAEndVal[1];   /* Model A SD Final Value   */
   
               coniniv = IniVal[1];      /* Values where Mod B SD overlaps on  */
               conendv = EndVal[1];      /* Model A Domain resolution */
               
               /* Lower bound is the maximum of lower bounds */
               if (inival < coniniv) {
                  if ( endval < coniniv ) break; /* the subdomain is out of area */
                  iniind = DCT_SearchIndex( ModAValues[1], ModANumPts[1], ModADomType[1], // hA[1],
                                            inipos, endpos, coniniv, -1, (ModSDIniVal+1) );
               }
               else{
                  if (inival > conendv) break;   /* the subdomain is out of area */
                  ModSDIniVal[1] =inival;
                  iniind = inipos;
               }
               
               /* Upper bound is the minimum of upper bounds */
               if (conendv < endval) {
                  if ( conendv < inival ) break; /* the subdomain is out of area */
                  ind = DCT_SearchIndex( ModAValues[1], ModANumPts[1], ModADomType[1], // hA[1],
                                            inipos, endpos, conendv, 1, (ModSDEndVal+1) );
               }
               else {
                  if (endval < coniniv) break;   /* the subdomain is outbound */
                  ModSDEndVal[1] = endval;
                  ind = endpos;
               }
               ModSDLength[1] = ind - iniind + 1;/* The Msed description es completed for dim 2 */
               dimflag++;
           /* ---------------------- DIM 1 -------------------- */
               inipos = ModIniPos[0];    /* Model A SD Initial Pos   */
               endpos = ModEndPos[0];    /* Model A SD Final Pos     */
               inival = ModAIniVal[0];   /* Model A SD Initial Value */
               endval = ModAEndVal[0];   /* Model A SD Final Value   */
   
               coniniv = IniVal[0];      /* Values where Mod B SD overlaps on  */
               conendv = EndVal[0];      /* Model A Domain resolution */
               
               /* Lower bound is the maximum of lower bounds */
               if (inival < coniniv) {
                  if ( endval < coniniv ) break; /* the subdomain is out of area */
                  iniind = DCT_SearchIndex( ModAValues[0], ModANumPts[0], ModADomType[0], // hA[0],
                                            inipos, endpos, coniniv, -1, (ModSDIniVal+0) );
               }
               else{
                  if (inival > conendv) break;   /* the subdomain is out of area */
                  ModSDIniVal[0] =inival;
                  iniind = inipos;
               }
               
               /* Upper bound is the minimum of upper bounds */
               if (conendv < endval) {
                  if ( conendv < inival ) break; /* the subdomain is out of area */
                  ind = DCT_SearchIndex( ModAValues[0], ModANumPts[0], ModADomType[0], // hA[0],
                                            inipos, endpos, conendv, 1, (ModSDEndVal+0) );
               }
               else {
                  if (endval < coniniv) break;   /* the subdomain is outbound */
                  ModSDEndVal[0] = endval;
                  ind = endpos;
               }
               ModSDLength[0] = ind - iniind + 1;/* The Msed description es completed for dim 1 */
               dimflag++;
         }  /* switch (dim) */
   
         if (dimflag == dim ) {
            /* The consumer register the sender */ 
            indices[k] = j;
            k++;   /* The message is counted **/
           
            if ( !maxsd ) { /* Equivalent to ( rank == (ModASubDom + j)->RankProc ) */
               /* The producer register the recceiver */
               ModSDMsend->Recvr = ModelBRcv.Recvr; /* The rcvr is registered */
               ModSDMsendPlan->Nmsg++;   /** The producer message is counted */
               /* Make a list of ModB procs lying on the corresponding Mod A proc */
               DCT_IncludeNeighbor ( (int)ModelBRcv.Recvr,
                                     (DCT_Integer *)&(ModSDMsendPlan->RcvrNSD),
                                     (int *)ProdNeigh );
            }
         }          
       } /* End of for (j=0; j < NSDomA; j++ ) */
       ModelBRcv.Nmsg = k; /* The number of messages is registered */

       DCT_Calculate_PerProc_Neighbors( DCT_PRODUCE, NSDomA, dim, rank, ModASubDom, ModAParLayout,
                                        ModALayoutDim, SDauxSen, ProdNeigh, indices, &ModelBRcv );
    } /* End of if (dimflag == dim) */
  } /* End of for (i=0; i < NSDomB; i++ ) */

  /* This from the previous number of message calculation */
  k = SDauxSen->Nmsg;
  ModASen->Nmsg = k;
  ModASen->Sendr = (ModASubDom + rind)->RankProc;
  ModASen->RcvrNSD = SDauxSen->RcvrNSD; /** This was calculated in
                                            DCT_Calculate_PerProc_Neighbors **/
  ModASendDef = (DCT_SD_Produce *) malloc( sizeof(DCT_SD_Produce)*(size_t)k );
  DCTERROR( ModASendDef == (DCT_SD_Produce *)NULL, "Memory Allocation Failed",
           ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  ModASen->Msend = ModASendDef;
  ModSDMsend = SDauxSen->Msend;

  for ( j=0; j < k; j++, ModSDMsend++, ModASendDef++ ) {
     ModASendDef->Recvr = ModSDMsend->Recvr;

     ModSDIniVal = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)dim);
     DCTERROR( ModSDIniVal == (DCT_Scalar *)NULL,
               "Memory Allocation Failed", ierr.Error_code  =  DCT_FAILED_MALLOC;
               return(ierr) );
     ModASendDef->IniVal = ModSDIniVal;

     ModSDEndVal = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)dim);
     DCTERROR( ModSDEndVal == (DCT_Scalar *)NULL,
               "Memory Allocation Failed", ierr.Error_code  =  DCT_FAILED_MALLOC;
               return(ierr) );
     ModASendDef->EndVal = ModSDEndVal;

     ModSDLength = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t)dim);
     DCTERROR( ModSDLength == (DCT_Integer *)NULL,
               "Memory Allocation Failed", ierr.Error_code  =  DCT_FAILED_MALLOC;
               return(ierr) );
     ModASendDef->Length = ModSDLength;

     ModSD2IniVal = ModSDMsend->IniVal;
     ModSD2EndVal = ModSDMsend->EndVal;
     ModSDIniPos = ModSDMsend->Length; /* The assigning variable name is meaningless */
     for ( l=0; l < dim; l++ ) {
        ModSDIniVal[l] = ModSD2IniVal[l];
        ModSDEndVal[l] = ModSD2EndVal[l];
        ModSDLength[l] = ModSDIniPos[l];/* The var name is meaningless */
     }
  } /* End of for ( j=0; j < k; j++ ) */


  /*****************************************************************************/
  /********** Communication  Model B (producer) to Model A (consumer) **********/

  /* Allocating the DCT_SDConsume_Plan ModelARcv structure */
  ModSDIniVal = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
  DCTERROR( ModSDIniVal == (DCT_Scalar *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  ModSDEndVal = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)dim);
  DCTERROR( ModSDEndVal == (DCT_Scalar *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  ModSDGlbInd = (DCT_Integer *) malloc(sizeof(DCT_Integer)*(size_t)dim);
  DCTERROR( ModSDGlbInd == (DCT_Integer *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  ModSDNpts = (DCT_Integer *) malloc(sizeof(DCT_Integer)*(size_t)dim);
  DCTERROR( ModSDNpts == (DCT_Integer *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  ModSDIniPos = (DCT_Integer *) malloc(sizeof(DCT_Integer)*(size_t)dim);
  DCTERROR( ModSDIniPos == (DCT_Integer *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

  ModARcv->IniVal = ModSDIniVal;
  ModARcv->EndVal = ModSDEndVal;
  ModARcv->GlbInd = ModSDGlbInd;
  ModARcv->Npts   = ModSDNpts;
  ModARcv->SndrNpts = ModSDIniPos; /** Reusing Variables **/
  /**** Taking the producer domain region (Producer resolution) ****/
  /** The boundary values of the subdomain is extracted **/
  DCT_Return_SDBoundaryVals ( dim, (ModASubDom+rind), ModADomType, ModAEndPos, ModAValues,
                              ModSDIniVal, ModSDEndVal );

/*************************************************************************************/
    /** Calculates if consumer's subdomain intersects the producer domain **/
  dimflag = DCT_Return_SubDom_Interc( PtrDomB, DomB, ModBEndPos, ModSDIniVal,
                                      ModSDEndVal, ModSDGlbInd, ModSDNpts );
/*************************************************************************************/
  ModARcv->Recvr = (ModASubDom + rind)->RankProc;
  if (dimflag == dim) {            /*** Intersecting Model B (producer) subdomains ***/
     /**** Storing the global information (equal for every subdomin) ****/
     /* Model B (Producer) data stored into the Model A (Consumer)      */
     for (i=0; i < dim; i++ )
         ModSDIniPos[i] = ModBNumPts[i]; /* ModARcv->SndrNpts = ModBNumPts[ii] */
//    ModARcv->SndrNSD = NSDomB;
//    ModARcv->SndrSDom = ModBSubDom;
     k = 0;
     /** Here the intersection with each model B (producer) subdomain **/
     for ( j=0; j < NSDomB; j++ ) {
         AuxSDrecv = AuxSDSlab + k;

         ModSDIniPos = AuxSDrecv->IniPos;
         ModSDLength = AuxSDrecv->Length;
         DCT_Return_SDBoundaryVals ( dim, (ModBSubDom+j), ModBDomType, ModBEndPos,
                                     ModBValues, ModAIniVal, ModAEndVal );
         ModIniPos = (ModBSubDom + j)->IniPos;
         ModEndPos = (ModBSubDom + j)->EndPos;
         dimflag = 0;
         switch (dim) {
            case 4:
               inipos = ModIniPos[3];    /* Model B SD Initial Pos   */
               endpos = ModEndPos[3];    /* Model B SD Final Pos     */
               inival = ModAIniVal[3];   /* Model B SD Initial Value */
               endval = ModAEndVal[3];   /* Model B SD Final Value   */
   
               coniniv = ModSDIniVal[3];     /* Mod A SD overlapped on Model B   */
               conendv = ModSDEndVal[3];     /* Domain using Model B resolution  */
   
               /* Lower bound is the maximum of lower bounds */
               if (inival < coniniv) {
                  if ( endval < coniniv ) break; /* the subdomain is out of area */
                  iniind = ModSDGlbInd[3];
                  ModSDIniPos[3] = 0;
               }
               else{
                  if (inival > conendv) break;   /* the subdomain is out of area */
                  iniind = inipos;
                  ModSDIniPos[3] = inipos - ModSDGlbInd[3];
               }
               
               /* Upper bound is the minimum of upper bounds */
               if (conendv < endval) {
                  if ( conendv < inival ) break; /* the subdomain is out of area */
                  ModSDLength[3] = ModSDGlbInd[3] + ModSDNpts[3] - iniind;
               }
               else {
                  if (endval < coniniv) break;   /* the subdomain is outbound */
                  ModSDLength[3] = endpos - iniind + 1;
               }
               dimflag++;   /* The Msed description es completed for dim 4 */
            case 3:
               inipos = ModIniPos[2];    /* Model B SD Initial Pos   */
               endpos = ModEndPos[2];    /* Model B SD Final Pos     */
               inival = ModAIniVal[2];   /* Model B SD Initial Value */
               endval = ModAEndVal[2];   /* Model B SD Final Value   */
   
               coniniv = ModSDIniVal[2];     /* Mod A SD overlapped on Model B   */
               conendv = ModSDEndVal[2];     /* Domain using Model B resolution  */
   
               /* Lower bound is the maximum of lower bounds */
               if (inival < coniniv) {
                  if ( endval < coniniv ) break; /* the subdomain is out of area */
                  iniind = ModSDGlbInd[2];
                  ModSDIniPos[2] = 0;
               }
               else{
                  if (inival > conendv) break;   /* the subdomain is out of area */
                  iniind = inipos;
                  ModSDIniPos[2] = inipos - ModSDGlbInd[2];
               }
               
               /* Upper bound is the minimum of upper bounds */
               if (conendv < endval) {
                  if ( conendv < inival ) break; /* the subdomain is out of area */
                  ModSDLength[2] = ModSDGlbInd[2] + ModSDNpts[2] - iniind;
               }
               else {
                  if (endval < coniniv) break;   /* the subdomain is outbound */
                  ModSDLength[2] = endpos - iniind + 1;
               }
               dimflag++; /* The Msed description es completed for dim 3 */
            case 2:
               inipos = ModIniPos[1];    /* Model B SD Initial Pos   */
               endpos = ModEndPos[1];    /* Model B SD Final Pos     */
               inival = ModAIniVal[1];   /* Model B SD Initial Value */
               endval = ModAEndVal[1];   /* Model B SD Final Value   */
   
               coniniv = ModSDIniVal[1];     /* Mod A SD overlapped on Model B   */
               conendv = ModSDEndVal[1];     /* Domain using Model B resolution  */
   
               /* Lower bound is the maximum of lower bounds */
               if (inival < coniniv) {
                  if ( endval < coniniv ) break; /* the subdomain is out of area */
                  iniind = ModSDGlbInd[1];
                  ModSDIniPos[1] = 0;
               }
               else{
                  if (inival > conendv) break;   /* the subdomain is out of area */
                  iniind = inipos;
                  ModSDIniPos[1] = inipos - ModSDGlbInd[1];
               }
               
               /* Upper bound is the minimum of upper bounds */
               if (conendv < endval) {
                  if ( conendv < inival ) break; /* the subdomain is out of area */
                  ModSDLength[1] = ModSDGlbInd[1] + ModSDNpts[1] - iniind;
               }
               else {
                  if (endval < coniniv) break;   /* the subdomain is outbound */
                  ModSDLength[1] = endpos - iniind + 1;
               }
               dimflag++;  /* The Msed description es completed for dim 2 */
           /* ---------------------- DIM 1 -------------------- */
               inipos = ModIniPos[0];    /* Model B SD Initial Pos   */
               endpos = ModEndPos[0];    /* Model B SD Final Pos     */
               inival = ModAIniVal[0];   /* Model B SD Initial Value */
               endval = ModAEndVal[0];   /* Model B SD Final Value   */
   
               coniniv = ModSDIniVal[0];     /* Mod A SD overlapped on Model B   */
               conendv = ModSDEndVal[0];     /* Domain using Model B resolution  */
   
               /* Lower bound is the maximum of lower bounds */
               if (inival < coniniv) {
                  if ( endval < coniniv ) break; /* the subdomain is out of area */
                  iniind = ModSDGlbInd[0];
                  ModSDIniPos[0] = 0;
               }
               else{
                  if (inival > conendv) break;   /* the subdomain is out of area */
                  iniind = inipos;
                  ModSDIniPos[0] = inipos - ModSDGlbInd[0];
               }
               
               /* Upper bound is the minimum of upper bounds */
               if (conendv < endval) {
                  if ( conendv < inival ) break; /* the subdomain is out of area */
                  ModSDLength[0] = ModSDGlbInd[0] + ModSDNpts[0] - iniind;
               }
               else {
                  if (endval < coniniv) break;   /* the subdomain is outbound */
                  ModSDLength[0] = endpos - iniind + 1;
               }
               dimflag++;/* The Msed description es completed for dim 1 */
         }  /* switch (dim) */
   
         if (dimflag == dim ) {
            /* The consumer register the sender */
            AuxSDrecv->Sendr = (ModBSubDom + j)->RankProc;
            indices[k] = j;
            k++;   /* The message is counted **/
         }          
     } /* End of for (j=0; j < NSDomB; j++ ) */
     ModARcv->Nmsg = k; /* The number of messages is registered */
       
     /** Allocating the  definite i-th Model B messages **/
     ModSDMrecv = (DCT_SD_Consume *) malloc(sizeof(DCT_SD_Consume)*(size_t)k);
     DCTERROR( ModSDMrecv == (DCT_SD_Consume *)NULL, "Memory Allocation Failed",
               ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
     ModARcv->SDSlab = ModSDMrecv;
     AuxSDrecv = AuxSDSlab;

     for (j=0; j < k; j++, ModSDMrecv++, AuxSDrecv++ ) {

          ModSDMrecv->Sendr = AuxSDrecv->Sendr;
          ModSDIniPos = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t) dim);
          DCTERROR( ModSDIniPos == (DCT_Integer *)NULL,
                    "Memory Allocation Failed",ierr.Error_code  =  DCT_FAILED_MALLOC;
                    return(ierr) );
          ModSDMrecv->IniPos = ModSDIniPos;

          ModSDLength = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t) dim);
          DCTERROR( ModSDLength == (DCT_Integer *)NULL,
                    "Memory Allocation Failed",ierr.Error_code  =  DCT_FAILED_MALLOC;
                    return(ierr) );
          ModSDMrecv->Length = ModSDLength;

          ModIniPos = AuxSDrecv->IniPos;
          ModSDGlbInd = AuxSDrecv->Length;/* The assigning variable name is meaningless */
          for ( l=0; l < dim; l++ ) {
             ModSDIniPos[l] = ModIniPos[l];
             ModSDLength[l] = ModSDGlbInd[l];/* The var name is meaningless */
          }
     }
     DCT_Calculate_PerProc_Neighbors( DCT_CONSUME, NSDomB, dim, rank, ModBSubDom, ModBParLayout,
                                      ModBLayoutDim, NULL, NULL, indices, ModARcv );
   
  } /* End of if (dimflag == dim) */
  else ModARcv->Nmsg = 0;


  /* Freeing the auxiliar structures */
  free( DomA[0] );
  free( DomA[1] );
  free( ModAEndPos );

  free( DomB[0] );
  free( DomB[1] );
  free( ModBEndPos );

  ModSDMrecv = AuxSDSlab;
  for ( i=0; i < NSDomB; i++, ModSDMrecv++ ) {
     free ( ModSDMrecv->IniPos );
     free ( ModSDMrecv->Length );
  }  
  free ( AuxSDSlab );

  free ( ProdNeigh );
  free ( indices );

  ModSDMsendPlan = SDauxSen;
  for ( i=0; i < 2; i++, ModSDMsendPlan++ ) {
     ModSDMsend = ModSDMsendPlan->Msend;
     for ( j=0; j < NSDomB; j++, ModSDMsend++ ) {
        
        free ( ModSDMsend->IniVal );
        free ( ModSDMsend->EndVal );
        free ( ModSDMsend->Length );
     }
     free ( ModSDMsendPlan->Msend );
  }
  free ( SDauxSen );

/*                                                   BEGIN DEBUG */
//    printf("Consumer Model Name: %s\n", modA->ModName );
//    printf("Total Number of subdomains: %d\n", ModARcv->SndrNSD );
//    printf("Rank %d\n", ModARcv->Recvr );
//    if ( ModARcv->Nmsg == 0 ) {
//       printf("\tSUBDOMAIN  %d DOES NOT LAY ON COUPLING DOMAIN.\n\n", i );
//    } else {
//    
//       printf("\tIniVal [" );
//       for ( j=0; j < dim; j++ ) {
//          printf("  %f ", ModARcv->IniVal[j]);
//       }
//       printf("]\n" );
//       printf("\tEndVal [" );
//       for ( j=0; j < dim; j++ ) {
//          printf("  %f ", ModARcv->EndVal[j]);
//       }
//       printf("]\n" );
//       printf("\tGlbInd [" );
//       for ( j=0; j < dim; j++ ) {
//          printf("  %d ", ModARcv->GlbInd[j]);
//       }
//       printf("]\n" );
//       printf("\tNpts [" );
//       for ( j=0; j < dim; j++ ) {
//          printf("  %d ", ModARcv->Npts[j]);
//       }
//       printf("]\n" );
//       k = ModARcv->Nmsg;
//       printf("\t**** SubDomain receives %d messages.\n", k );
//       for(j=0; j < k; j++){
//          printf("\t\t Message %d from process %d.\n", j, ModARcv->SDSlab[j].Sendr );
//          printf("\t\tIniPos [" );
//          for ( l=0; l < dim; l++ ) {
//             printf("   %d  ", ModARcv->SDSlab[j].IniPos[l]);
//          }
//          printf("]\n" );
//          printf("\t\tLength [" );
//          for ( l=0; l < dim; l++ ) {
//             printf("   %d  ", ModARcv->SDSlab[j].Length[l]);
//          }          
//          printf("]\n" );
//       }
//    }
/*                                                     END DEBUG */
/*                                                   BEGIN DEBUG */
//   printf("Producer Model Name: %s\n", modA->ModName);
//   printf("Total Number of subdomains: %d\n", ModASen->RcvrNSD);
//   printf("Rank %d\n", ModASen->Sendr );
//   if ( ModASen->Nmsg == 0 ) {
//      printf("\tSUBDOMAIN  %d DOES NOT LAY ON COUPLING DOMAIN.\n\n", i );
//   } else {
//      k = ModASen->Nmsg;
//      for ( j=0; j < k; j++ ) {
//         printf("\t\t Message %d to process %d.\n", j, ModASen->Msend[j].Recvr );
//         printf("\t\tIniVal [" );
//         for ( l=0; l < dim; l++ ) {
//            printf("    %f  ", ModASen->Msend[j].IniVal[l] );
//         }
//         printf("]\n" );
//         printf("\t\tEndVal [" );
//         for ( l=0; l < dim; l++ ) {
//            printf("    %f  ", ModASen->Msend[j].EndVal[l] );
//         }
//         printf("]\n" );
//         printf("\t\tLength [" );
//         for ( l=0; l < dim; l++ ) {
//            printf("    %d  ", ModASen->Msend[j].Length[l] );
//         }
//         printf("]\n" );
//      } /* End of for ( j=0; j < k; j++ ) */
//   }
/*                                                     END DEBUG */

  ierr.Error_code = DCT_SUCCESS;
  ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
  return(ierr);


/* ----------------------------------------  END( DCT_Arrange_SD ) */

}


/*******************************************************************/
/*                         DCT_Assign_Broker                       */
/*                                                                 */
/*!     This routine could be call before the Registration step
        and sets the given processor rank as identifying the 
        DCT Broker.

        \param[in] broker  Is the rank of the broker in the
                           user specified comm.
        \param[in]   comm  Is the communicator or context where
                           all the coupled models processes are
                           defined from the model users.

        \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Assign_Broker( DCT_Rank broker, DCT_Comm comm )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  
/* -----------------------------------  BEGIN( DCT_Assign_Broker ) */
  
   /* Check if the the function DCT_BeginRegistration() was called */
   if (DCT_Step_Check != DCT_NULL_STATE){
      ierr.Error_code  = DCT_INVALID_OPERATION;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
         "[DCT_Assign_Broker] Invalid Operation. The function DCT_Assign_Broker() should be called before DCT_BeginRegistration.\n");
      return(ierr);
   }
   
   /* Check if the library MPI is initialized */
   if ( DCT_Check_CommIni( ) ){
      ierr.Error_code  = DCT_FAILED_LIB_COMM;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
         "[DCT_BeginRegistration] The communication library fails\n");
      return(ierr);
   }
   
   /* Create the DCT_Comm_World */
   if ( DCT_Create_Comm( comm ) ){
      ierr.Error_code  = DCT_FAILED_LIB_COMM;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
        "[DCT_BeginRegistration] It is not possible initialize the DCT_Comm_World Communicator\n");
     return(ierr);
   }
   
   /* Set the broker rank inside of the communicator */
   if ( DCT_Comm_SetBrokerRank( broker ) ) {
      ierr.Error_code  = DCT_FAILED_LIB_COMM;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
          "[DCT_BeginRegistration] The Broker ID is out of the communicator in use\n");
      return(ierr);
   }
    
   ierr.Error_code = DCT_SUCCESS;
   ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
   return(ierr);
  
/* -------------------------------------  END( DCT_Assign_Broker ) */
  
}

/*******************************************************************/
/*                       DCT_BeginRegistration                     */
/*                                                                 */
/*!     This routine should be called at the beginning of the
        Registration Phase. The system checks the initialization
        variables, and the communication library status.

        \param[in]   comm  Is the communicator or context where
                           all the coupled models processes are
                           defined from the model users.

        \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_BeginRegistration( DCT_Comm comm )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  
/* -------------------------------  BEGIN( DCT_BeginRegistration ) */
  
   /* Check if the the function DCT_BeginRegistration() is the firstly called */
   if (DCT_Step_Check != DCT_NULL_STATE){
      ierr.Error_code  = DCT_INVALID_OPERATION;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
         "[DCT_BeginRegistration] Invalid Operation. The function DCT_BeginRegistration() has been called.\n");
      return(ierr);
   }
   
   /* Check if the library MPI is initialized */
   if ( DCT_Check_CommIni( ) ){
      ierr.Error_code  = DCT_FAILED_LIB_COMM;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
        "[DCT_BeginRegistration] The communication library fails\n");
     return(ierr);
   }
   

   /* Create the DCT_Comm_World */
   if ( DCT_Create_Comm( comm ) ){
      ierr.Error_code  = DCT_FAILED_LIB_COMM;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
        "[DCT_BeginRegistration] It is not possible initialize the DCT_Comm_World Communicator\n");
     return(ierr);
   }
   
   /* Set the broker rank inside of the communicator */
   if ( DCT_Comm_SetBrokerRank( (DCT_Rank) 0 ) ) {
      ierr.Error_code  = DCT_FAILED_LIB_COMM;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
        "[DCT_BeginRegistration] The assigment of Broker's Id fails\n");
     return(ierr);
   }
    
   /* Set the processor rank inside of the communicator */
   if ( DCT_Comm_GetRank() ){
      ierr.Error_code  = DCT_FAILED_LIB_COMM;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
        "[DCT_BeginRegistration] Error calling the Communication Library\n");
     return(ierr);
   }
   
   /** All processes synchronize from this point **/
   DCT_Sync( DCT_Comm_World );
   
   /*** The Registration Phase was made completely and is registered ***/
   DCT_Step_Check = DCT_BEGIN_STATE;
   
   ierr.Error_code = DCT_SUCCESS;
   ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
   return(ierr);
  
/* ---------------------------------  END( DCT_BeginRegistration ) */
  
}

/*******************************************************************/
/*                       DCT_Broker_RegCouple                      */
/*                                                                 */
/*!   This routine is called by the broker to obtain the
      DCT_Couple structures from other processes.

      \param[in,out] brklcpl  Array of DCT_Coupler that stores
                              the DCT_Coupler declared in the
                              broker.
      \param[in,out] ncplmsg  Counter which register the number
                              of messages to be received later
                              by broker with DCT_Model and var
                              info.
      \param[out]    cpltag   Pointer the variable that registers
                              the last DCT_Couple tag used.
      \param[out]    modtag   Pointer the variable register the
                              last DCT_Model tag used.

      \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Broker_RegCouple( DCT_Couple **brklcpl, DCT_Integer *ncplmsg,
                                DCT_Tag *cpltag, DCT_Tag *modtag )
{
/* ---------------------------------------------  Local Variables  */
  
   DCT_List            *current;
   DCT_List            *modlst;
   DCT_Couple         **lcpl;
   
   DCT_Couple          *couple;
   DCT_Couple          *cplaux;
   
   DCT_Model           *ModelA;
   
   DCT_Integer          CNumVar;

   DCT_Tag            **sbuff;
   DCT_Tag             *init;
   int                 *buffsize;
   int                 *totlead;
   int                 *cntlead;
   DCT_Rank            *srank;
   DCT_Rank             rcvr, cntrank, brkrank;
   
   DCT_Error            ierr;
   DCT_Integer          nproc;
   DCT_Msg_Tags_Type    msgtag;
   int                  chk4msg;
   DCT_Tag             *rtrnmsg;
   DCT_Tag              modt, cplt;
   int                  ii, kk, jj, ind, ind2, len, nclp, nlead, tlead, nosend;
  
/* --------------------------------  BEGIN( DCT_Broker_RegCouple ) */
   
   /** The Registration of Couples is posting for receiving **/
   if ( DCT_Post_RecvCouple( ) ) {
      ierr.Error_code  = DCT_FAILED_LIB_COMM;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
           "[DCT_Broker_RegCouple] Error posting DCT_Couple registration.\n");
      return(ierr);
   }

   /* Sweep its own DCT_Couple list */
   current = DCT_Reg_Couple;
   ii=0;
   while (current != (DCT_List *) NULL) {
      DCT_List_Ext (&current, (DCT_VoidPointer *) &couple);
      (*cpltag)++;
      DCTERROR( *cpltag == DCT_MAX_COUPLE, "Number of couple tag exhausted",
                ierr.Error_code = DCT_COUPLE_TAG_EXHAUSTED; return(ierr) );
      couple->CoupleTag = *cpltag;
      brklcpl[ii++] = couple;
   }
   /*DCT_Couple_ListOrder( brklcpl, DCT_LocalCouple );*/ /* this does not make any sense
                                                         because in the way tag are 
                                                         generated are already ordered */
   
   /* Sweep its own DCT_Model list */
   rcvr = DCT_procinfo.ProcDCTRank;
   current = DCT_Reg_Model;
   while (current != (DCT_List *) NULL) {
      DCT_List_Ext (&current, (DCT_VoidPointer *) &ModelA);
      (*modtag)++;
      DCTERROR( *modtag == DCT_MAX_MODEL, "Number of model tag exhausted",
                ierr.Error_code = DCT_MODEL_TAG_EXHAUSTED; return(ierr) );
      ModelA->ModTag = *modtag;
      ModelA->ModLeadRank = rcvr;
   }
   
   nproc = DCT_procinfo.ProcCommSize; /* Number of processors (how many messages) */
   brkrank = DCT_procinfo.ProcDCTRank;
   kk  = 1;              /* Broker is already done!!! */
   chk4msg = 1;          /* Waiting for messages */
   *ncplmsg = 0;          /* Number of messages to be received later for
                            more info about the couple (like model, variables) */
   
   /** Maximum allowed DCT_Coupler instances **/
   lcpl = (DCT_Couple **) malloc( sizeof(DCT_Couple *) *(size_t)DCT_MAX_COUPLE );
   DCTERROR( lcpl == (DCT_Couple **)NULL, "Memory Allocation Failed",
             ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
   /** As the most frequent event is to repeat DCT_Couple and DCT_Model
       headers, in order to avoid unnecessary allocations, the array is kept
       always full allocated, and a replacement is reallocated when is used   **/
   for ( ii=0; ii < DCT_MAX_COUPLE; ii++ ) {
      lcpl[ii] = (DCT_Couple *) malloc( sizeof(DCT_Couple ) );
      DCTERROR( lcpl[ii] == (DCT_Couple *)NULL, "Memory Allocation Failed",
             ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
      ModelA = (DCT_Model *)malloc( sizeof(DCT_Model) );
      DCTERROR( ModelA == (DCT_Model *)NULL, "Memory Allocation Failed",
             ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
      lcpl[ii]->CoupleModelA = ModelA;
   }
   /** Maximum allowed DCT_Coupler possible tags **/
   /*** rtrnmsg is an array to the message back to the rest processes
        after to register a couple. It consists in 4 values (Couple Tag,
        Model tag, designated model leader, designated counter model leader ***/        
   rtrnmsg = (DCT_Tag *) malloc( sizeof(DCT_Tag) *(size_t)(4*DCT_MAX_COUPLE) );
   DCTERROR( rtrnmsg == (DCT_Tag *)NULL, "Memory Allocation Failed",
             ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
   /** Maximum of possible model leaders buffers **/
   sbuff = (DCT_Tag **) malloc( sizeof(DCT_Tag *)*(size_t)2*DCT_MAX_COUPLE );
   DCTERROR( sbuff == (DCT_Tag **) NULL, "Memory Allocation Failed",
             ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
   buffsize = (int *) malloc( sizeof(int)*(size_t)2*DCT_MAX_COUPLE );
   DCTERROR( buffsize == (int *) NULL, "Memory Allocation Failed",
             ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
   /** Total number of model that proc is leader */
   totlead = (int *) malloc( sizeof(int)*(size_t)2*DCT_MAX_COUPLE );
   DCTERROR( totlead == (int *) NULL, "Memory Allocation Failed",
             ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
   /** How many are already paired **/
   cntlead = (int *) malloc( sizeof(int)*(size_t)2*DCT_MAX_COUPLE );
   DCTERROR( cntlead == (int *) NULL, "Memory Allocation Failed",
             ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  
   srank = (DCT_Rank *) malloc( sizeof(DCT_Rank)*(size_t)2*DCT_MAX_COUPLE );
   DCTERROR( srank == (DCT_Rank *) NULL, "Memory Allocation Failed",
             ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
   /* Receiving the couple from others */
   while( chk4msg ) {
      msgtag = DCT_Msg_Tags_UNDEFINED;
      nosend = 0;
      nlead = 0;
      tlead = 0;
      if ( DCT_Get_Couple( lcpl, &nclp, &rcvr ) ) {
         ierr.Error_code  = DCT_FAILED_LIB_COMM;
         ierr.Error_msg   = (DCT_String) malloc((size_t)150);
         sprintf(ierr.Error_msg,
              "[DCT_Broker_RegCouple] Error obtaining a DCT_Couple structure.\n");
         return(ierr);
      }
      /** For each couple received **/
      for ( ii=0; ii < nclp; ii++ ) {
         couple = lcpl[ii];
         ModelA = couple->CoupleModelA;
         jj = 4*ii; /** return message index **/
         /* Serching if the DCT_Couple is new */
         /* Check in the list, if exist return the node in current, otherwise is added */
         current = DCT_List_CHKAdd( &DCT_Reg_Couple, couple, DCT_COUPLE_TYPE );
         /* if it is new, it is registered */
         if ( current == (DCT_List *)NULL ) {
            (*cpltag)++;
            DCTERROR( *cpltag == DCT_MAX_COUPLE, "Number of couple tag exhausted",
                      ierr.Error_code = DCT_COUPLE_TAG_EXHAUSTED; return(ierr) );
            couple->CoupleTag = *cpltag;
            
            /* The used couple is replaced */
            lcpl[ii] = (DCT_Couple *) malloc( sizeof(DCT_Couple ) );
            DCTERROR( lcpl[ii] == (DCT_Couple *)NULL, "Memory Allocation Failed",
                   ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
            
            /** Save the Couple tag **/
            rtrnmsg[jj] = *cpltag;

            /* Check if the Model exists in the list */
            modlst = DCT_List_CHKAdd( &DCT_Reg_Model, ModelA, DCT_MODEL_TYPE );
            if ( modlst == (DCT_List *)NULL ) { /* it wasn't exist, so was added */
               (*modtag)++;
               DCTERROR( *modtag == DCT_MAX_MODEL, "Number of model tag exhausted",
                      ierr.Error_code = DCT_COUPLE_TAG_EXHAUSTED; return(ierr) );
               ModelA->ModTag = *modtag;
               ModelA->ModLeadRank = rcvr;
               /** Save the model tag **/
               rtrnmsg[jj+1] = ModelA->ModTag;
               /** Save the model leader rank (itself) **/
               rtrnmsg[jj+2] = rcvr;
               
               /*** Here the the system should know that has to wait
                    until the another DCT_Model is registered       ***/
               nosend = 1;
               /** The coupled model that is a leader is counted **/
               tlead++;
               /* The used model is replaced */
               ModelA = (DCT_Model *)malloc( sizeof(DCT_Model) );
               DCTERROR( ModelA == (DCT_Model *)NULL, "Memory Allocation Failed",
                      ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
            
            }
            else {
               /* The model exists and the new couple is plugged */
               couple->CoupleModelA = (DCT_Model *) modlst->AddressData;
               /** Save the model tag **/
               rtrnmsg[jj+1] = couple->CoupleModelA->ModTag;
               /** Save the model leader rank **/
               rtrnmsg[jj+2] = couple->CoupleModelA->ModLeadRank;
               if (rcvr == rtrnmsg[jj+2]) {
                  nosend = 1;
                  tlead++;
               }

               DCT_Destroy_Model( ModelA );
            }
            /* The model is plugged to the new couple */
            lcpl[ii]->CoupleModelA = ModelA;

            /* The counter model rank is undefined */
            rtrnmsg[jj+3] = DCT_UNDEFINED_RANK;
            if ( ( msgtag != DCT_Msg_Tags_CPLTAG_MOD ) && ( rcvr == rtrnmsg[jj+2] ) ) {
               msgtag = DCT_Msg_Tags_CPLTAG_MOD;
               //(*ncplmsg)++;  /* A messager is sent later with model and variable info */               
            }
            
         }
         else {    /* The DCT_Couple exists */
             /* The couple estructure is extracted */
             cplaux = (DCT_Couple *) current->AddressData;
   
             /***** No more info required, the structure full ******/
             /***** Or the info sent is from prosses the same model (no new info) ***/
            if ( ( cplaux->CoupleModelB != (DCT_Model *)NULL ) ||
                 ( strcmp( cplaux->CoupleModelA->ModName, 
                                         ModelA->ModName  )==0 )  ) {
   
               /* The number of variables to coupler is checked */
               if ( couple->CoupleTotNumVars != cplaux->CoupleTotNumVars ) {
                  ierr.Error_code  = DCT_FAILED_LIB_COMM;
                  ierr.Error_msg   = (DCT_String) malloc((size_t)250);
                  sprintf(ierr.Error_msg,
                    "[DCT_Broker_RegCouple] In couple <%s> does not agree the number of couple veriables\n between process %d with %d and registered with %d variables number.\n",
                    cplaux->CoupleName, rcvr, couple->CoupleTotNumVars, cplaux->CoupleTotNumVars);
                  return(ierr);
               }
   
               /** Save the tags **/
               rtrnmsg[jj] = cplaux->CoupleTag;
               /* Sending Model A's tag */
               if ( strcmp( cplaux->CoupleModelA->ModName, 
                           couple->CoupleModelA->ModName  )==0 ) {
                  rtrnmsg[jj+1] = cplaux->CoupleModelA->ModTag;
                  rtrnmsg[jj+2] = cplaux->CoupleModelA->ModLeadRank;

               } /* Sending Model B's tag */
               else if ( strcmp( cplaux->CoupleModelB->ModName, 
                                               ModelA->ModName  )==0 ) {
                  rtrnmsg[jj+1] = cplaux->CoupleModelB->ModTag;
                  rtrnmsg[jj+2] = cplaux->CoupleModelB->ModLeadRank;
               }
               else {
                  ierr.Error_code  = DCT_FAILED_LIB_COMM;
                  ierr.Error_msg   = (DCT_String) malloc((size_t)150);
                  sprintf(ierr.Error_msg,
                       "[DCT_Broker_RegCouple] Error in DCT_Models declared in DCT_Couple <%s>.\n", couple->CoupleName );
                  return(ierr);
               }
               /* The counter model rank is undefined */
               rtrnmsg[jj+3] = DCT_UNDEFINED_RANK;
               /** the message tag is stored **/
               if ( msgtag != DCT_Msg_Tags_CPLTAG_MOD ) msgtag = DCT_Msg_Tags_COUPLE_TAG;
               
               /* Cleaning the unused structures */
               DCT_Destroy_Couple( couple );
               DCT_Destroy_Model( ModelA );
               couple->CoupleModelA = ModelA;
               
            } else {  /* The DCT_Couple exists but needs the ModelB information */
            
               /************ Compares both names to double check **********/
               if ( strcmp( cplaux->CoupleHModelB->ModelName, ModelA->ModName ) ) {
                  ierr.Error_code  = DCT_NAME_MATCH_ERROR;
                  ierr.Error_msg   = (DCT_String) malloc((size_t)350);
                  sprintf(ierr.Error_msg,
                       "[DCT_Broker_RegCouple] Error in couple <%s>.\n   ModelB name does not match: %s, %d chars || %s , %d chars\n",
                       cplaux->CoupleName, cplaux->CoupleHModelB->ModelName,
                       (int)strlen( cplaux->CoupleHModelB->ModelName ),
                       couple->CoupleModelA->ModName,
                       (int)strlen( couple->CoupleModelA->ModName ) );
                  return(ierr);
               }
               if (strcmp( cplaux->CoupleModelA->ModName, couple->CoupleHModelB->ModelName ) ) {
                  ierr.Error_code  = DCT_NAME_MATCH_ERROR;
                  ierr.Error_msg   = (DCT_String) malloc((size_t)350);
                  sprintf(ierr.Error_msg,
                     "[DCT_Broker_RegCouple] Error in couple <%s>.\n   ModelA name does not match: %s, %d chars || %s, %d chars\n", cplaux->CoupleName, cplaux->CoupleModelA->ModName, (int)strlen( cplaux->CoupleModelA->ModName ),
                     couple->CoupleHModelB->ModelName, 
                     (int)strlen( couple->CoupleHModelB->ModelName ) );
                  return(ierr);
               }

               /* The number of variables to coupler is checked */
               CNumVar = cplaux->CoupleTotNumVars;
               if ( couple->CoupleTotNumVars != CNumVar ) {
                  ierr.Error_code  = DCT_FAILED_LIB_COMM;
                  ierr.Error_msg   = (DCT_String) malloc((size_t)250);
                  sprintf(ierr.Error_msg,
                    "[DCT_Broker_RegCouple] In couple <%s> does not agree the number of couple veriables\n between process %d with %d and registered with %d variables number.\n",
                    cplaux->CoupleName, rcvr, couple->CoupleTotNumVars, CNumVar);
                  return(ierr);
               }

               /** Save the couple tag **/
               rtrnmsg[jj] = cplaux->CoupleTag;
               cntrank = cplaux->CoupleModelA->ModLeadRank; /* Counter model rank */
               /* Model B info and structure is taken */
               /* Check if the Model exists in the list */
               modlst = DCT_List_CHKAdd( &DCT_Reg_Model, ModelA, DCT_MODEL_TYPE );
               if ( modlst == (DCT_List *)NULL ) { /* it wasn't exist, and it is added */
                  (*modtag)++;
                  DCTERROR( *modtag > DCT_MAX_MODEL, "Number of model tag exhausted",
                         ierr.Error_code = DCT_COUPLE_TAG_EXHAUSTED; return(ierr) );
                  ModelA->ModTag = *modtag;
                  ModelA->ModLeadRank = rcvr;
                  cplaux->CoupleModelB = ModelA;

                  /** Save the model tag **/
                  rtrnmsg[jj+1] = ModelA->ModTag;
                  /** Save the model leader rank (itself) **/
                  rtrnmsg[jj+2] = rcvr;
                  rtrnmsg[jj+3] = cntrank;
                  
                  /** The model that is a leader is counted **/
                  tlead++;
                  nlead++; /* The current model pairing is counted */
                  /*** Here the the system should know that has to wait
                       until the another DCT_Model is registered       ***/
                  nosend = 1;
                  /* The memory allocated for ModelA was used and need to be restored */
                  ModelA = (DCT_Model *)malloc( sizeof(DCT_Model) );
                  DCTERROR( ModelA == (DCT_Model *)NULL, "Memory Allocation Failed",
                         ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
               }
               else {
                  /* The model exists and the new couple is plugged */
                  cplaux->CoupleModelB = (DCT_Model *) modlst->AddressData;
                  /** Save the model tag **/
                  rtrnmsg[jj+1] = cplaux->CoupleModelB->ModTag;
                  /** Save the model leader rank (itself) **/
                  rtrnmsg[jj+2] = cplaux->CoupleModelB->ModLeadRank;
                  rtrnmsg[jj+3] = DCT_UNDEFINED_RANK;

                  if (rcvr == rtrnmsg[jj+2]) {
                     rtrnmsg[jj+3] = cntrank;
                     nosend = 1;
                     tlead++;
                     nlead++; /* The current model pairing is counted */
                  }
                  DCT_Destroy_Model( ModelA );

               }
               /* Prepare the message for the counter model (Model A) leader when the
                  model B is finally registered   */
               if ( brkrank == cntrank ) /* The cntrank is the broker no message */
                  cplaux->CoupleHModelB->ModLeadRank = cplaux->CoupleModelB->ModLeadRank;
               else {
                  /* The no-current counter model leader data is updated */
                  /* The model should be paired with the counter one */
                  len = *ncplmsg;
                  for (ind = 0; (ind<len) && (srank[ind]!=cntrank) ; ind++)
                  	; /* close the loop */
                   DCTERROR( ind == len, "DCT_Model leader rank not found",
                         ierr.Error_code  =  DCT_VALUE_MATCH_ERROR; return(ierr) );
                 
                  cplt = rtrnmsg[jj];
                  modt = cplaux->CoupleModelA->ModTag;
                  len = buffsize[ind];
                  init = sbuff[ind];
                  for (ind2 = 0; (ind2<len) && ( (init[ind2] != cplt) 
                                              || (init[ind2+1] != modt) ); ind2+=4 );
                  DCTERROR( ind2 == len, "DCT_Couple and DCT_Model not found",
                         ierr.Error_code  =  DCT_VALUE_MATCH_ERROR; return(ierr) );
                  init[ind2+3] = rcvr;
                  cntlead[ind]++; /* The paired model is counted */
                  /* Check if the counter model is ready to get the tags */
                  if ( cntlead[ind] == totlead[ind]) {
                     if ( DCT_Send_Tag( cntrank, len, init, DCT_Msg_Tags_CPLTAG_MOD ) ) {
                        ierr.Error_code  = DCT_FAILED_LIB_COMM;
                        ierr.Error_msg   = (DCT_String) malloc((size_t)60);
                        sprintf(ierr.Error_msg,
                          "[DCT_Broker_RegCouple] Error sending tag.\n");
                        return(ierr);
                     }
                  }
               } /* End of else from if ( brkrank == cntrank ) */

               /** The Coupler is cleared **/
               DCT_Destroy_Couple( couple );
               couple->CoupleModelA = ModelA;

               /* Check if it is marked before to send information and 
                  the its rank is the same as the model leader */

               if ( ( msgtag != DCT_Msg_Tags_CPLTAG_MOD ) && ( rcvr == rtrnmsg[jj+2] ) ) {
                  msgtag = DCT_Msg_Tags_CPLTAG_MOD;
                  //(*ncplmsg)++;  /* A messager is sent later with model and variable info */               
               }
   
            }  /* End else couple needs info of ModelB */
         } /* End of if ( current == (DCT_List *)NULL ) */
      } /* End of for ( ii=0; ii < nclp; ii++ ) */
      /* Send tags and the Tag message saying
         to provide the rest of information */
      if ( msgtag != DCT_Msg_Tags_CPLTAG_MOD ) { /* No a DCT_Model leader, send tag immediately */
         if ( DCT_Send_Tag( rcvr, 4*nclp, rtrnmsg, msgtag ) ) {
            ierr.Error_code  = DCT_FAILED_LIB_COMM;
            ierr.Error_msg   = (DCT_String) malloc((size_t)60);
            sprintf(ierr.Error_msg,
              "[DCT_Broker_RegCouple] Error sending tag.\n");
            return(ierr);
         }
      }
      else if ( nosend ){ /* The tags include at least one model leader */
         ii = (*ncplmsg)++;
         srank[ii] = rcvr;
         len = 4*nclp;
         buffsize[ii] = len;
         totlead[ii] = tlead; /* How many model the proc is leading */
         cntlead[ii] = nlead; /* How many of them are already paired */
         sbuff[ii] = (DCT_Tag *) malloc( sizeof(DCT_Tag)*(size_t)len );
         DCTERROR( sbuff[ii] == (DCT_Tag *) NULL, "Memory Allocation Failed",
                         ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
         init = sbuff[ii];
         for ( jj = 0; jj < len; jj++) init[jj]= rtrnmsg[jj];
         if ( tlead == nlead ) { /* Everything was paired */
            if ( DCT_Send_Tag( rcvr, 4*nclp, rtrnmsg, msgtag ) ) {
               ierr.Error_code  = DCT_FAILED_LIB_COMM;
               ierr.Error_msg   = (DCT_String) malloc((size_t)60);
               sprintf(ierr.Error_msg,
                 "[DCT_Broker_RegCouple] Error sending tag.\n");
               return(ierr);
            }
         }

      }
      kk++;  /* Counting another process */
      if (kk == nproc) chk4msg = 0; /* all processes were received */
      
   }  /* end while( chk4msg ) */

   /** The DCT_Couple registration is release **/
   DCT_Rels_RecvCouple( );   

   free ( rtrnmsg );
   free( buffsize );
   free( totlead );
   free( cntlead );
   free( srank );
   len = *ncplmsg;
   for ( ii=0; ii < len; ii++ ) {
      free ( sbuff[ii] );
   }
   free( sbuff );
   for ( ii=0; ii < DCT_MAX_COUPLE; ii++ ) {
      free ( lcpl[ii]->CoupleModelA);
      free ( lcpl[ii] );
   }
   free ( lcpl );
  
   ierr.Error_code = DCT_SUCCESS;
   ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
   return(ierr);
  
/* ----------------------------------  END( DCT_Broker_RegCouple ) */
  
}

/*******************************************************************/
/*                        DCT_Broker_RegVar                        */
/*                                                                 */
/*!    This routine is called by the broker to receive the
       DCT_Model variables and fields structures by other
       processes in order to process them.

       \param[in] ncplmsg  Number of messages expected to be
                           received.
       \param[in]  vartag  Variable registering the last tag
                           generates.

       \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Broker_RegVar( DCT_Integer ncplmsg, DCT_Tag *vartag )
{
/* ---------------------------------------------  Local Variables  */
  
   DCT_List            *current;
   
   DCT_VoidPointer      dctvar;
   
   DCT_Object_Types     vartype;
  
   DCT_Field           *field;
   DCT_3d_Var          *var3d;
   DCT_4d_Var          *var4d;

   DCT_Error            ierr;

   int                  chk4msg;
   int                  ii;
  
/* -----------------------------------  BEGIN( DCT_Broker_RegVar ) */


   /* Sweep its own DCT_Vars list */
   current = DCT_Reg_Vars;
   while (current != (DCT_List *) NULL) {
      (*vartag)++;
      DCTERROR( *vartag > DCT_MAX_VARS, "Number of variable tag exhausted",
                ierr.Error_code = DCT_VARS_TAG_EXHAUSTED; return(ierr) );
      DCT_List_ExtVar ( &current, &dctvar, &vartype );
      switch (vartype) {
         case DCT_FIELD_TYPE:
           field = (DCT_Field *) dctvar;
           field->VarTag = *vartag;
         break;
         case DCT_3D_VAR_TYPE:
           var3d = (DCT_3d_Var *) dctvar;
           var3d->VarTag = *vartag;
/*                                                   BEGIN DEBUG */
/*            printf("[DCT_Broker_RegVar]: var3d->VarName %s, VAR TAG: %d\n\n",
                                                           var3d->VarName, *vartag ); */
/*                                                     END DEBUG */
         break;
         case DCT_4D_VAR_TYPE:
           var4d = (DCT_4d_Var *) dctvar;
           var4d->VarTag = *vartag;
         break;
         default:
           ierr.Error_code  = DCT_INVALID_DATA_TYPE;
           ierr.Error_msg   = (DCT_String) malloc((size_t)150);
           sprintf(ierr.Error_msg,
              "[DCT_Broker_RegVar] Error DCT Vars type.\n");
           return(ierr);
      }
   }

   chk4msg = 1;          /* Waiting for messages */
   ii=0;
   
   /* Receiving the couple from others */
   while( chk4msg ){

      if ( DCT_Get_Var( vartag ) ) {
         ierr.Error_code  = DCT_FAILED_LIB_COMM;
         ierr.Error_msg   = (DCT_String) malloc((size_t)150);
         sprintf(ierr.Error_msg,
           "[DCT_Broker_RegVar] Error receiving Model and Vars structures.\n");
         return(ierr);
      }
      
      ii++;
      if (ii == ncplmsg) chk4msg = 0; /* all information were received */
   }
  
  ierr.Error_code = DCT_SUCCESS;
  ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
  return(ierr);
  
/* -------------------------------------  END( DCT_Broker_RegVar ) */
}

/*******************************************************************/
/*                       DCT_Local_SubDom_Comm                     */
/*                                                                 */
/*!  This routine calculates the preliminar SubDomain comunication
     between a pair of model in a couple.

     \param[in,out] lcpl  Array of pointers to the local
                          DCT_Couple's.

     \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Local_SubDom_Comm ( DCT_Couple **lcpl )
{
/* ---------------------------------------------  Local Variables  */
  
   DCT_Couple          *couple;
   
   DCT_Model           *ModelA;
   DCT_Model           *ModelB;
   
   DCT_Error            ierr;
   
   int                  icpl;
  
/* -------------------------------  BEGIN( DCT_Local_SubDom_Comm ) */

   for ( icpl = 0; icpl < DCT_LocalCouple; icpl++ ) {
      couple = lcpl[icpl];
      
      ModelA = couple->CoupleModelA;
      ModelB = couple->CoupleModelB;
   
      /* The broker obtain the different models subdomains intersections */
      if ( ModelA->ModDomain->ModDomType[0] == DCT_GENERAL_CURV );
         // ierr = DCT_BrokerArrangeGC(ModelA, ModelB, &ModASen, &nModASD, &ModARcv,
//                                                     &ModBSen, &nModBSD, &ModBRcv );
      else
         ierr = DCT_Arrange_SD( ModelA, ModelB, &(couple->CoupleSnd),
                                &(couple->CoupleRcv) );

      if ( ierr.Error_code != DCT_SUCCESS ) return(ierr);
   
/*                                                   BEGIN DEBUG */
//       {
//         int k,l;
//         DCT_Integer dim = ModelA->ModDomain->ModDomDim;
//         printf("Consumer Model Name: %s\n", ModelA->ModName);
//         printf("Total Number of subdomains: %d\n", nModASD);
//         for(ii=0; ii<nModASD; ii++){
//           printf("Rank in subdomain %d: %d\n", ii, ModARcv[ ii ].Recvr);
//           if ( ModARcv[ ii ].Nmsg == 0 ) {
//              printf("\tSUBDOMAIN  %d DOES NOT LAY ON COUPLING DOMAIN.\n\n", ii );
//           } else {
//              
//              for ( jj=0; jj < dim; jj++ ) {
//                printf("\tIniVal Direction %d: %f\n", jj, ModARcv[ ii ].IniVal[jj]);
//                printf("\tEndVal Direction %d: %f\n", jj, ModARcv[ ii ].EndVal[jj]);
//                printf("\tGlbInd Direction %d: %d\n", jj, ModARcv[ ii ].GlbInd[jj]);
//                printf("\tNpts  Direction %d: %d\n", jj, ModARcv[ ii ].Npts[jj]);
//              }
//              k = ModARcv[ ii ].Nmsg;
//              printf("\t**** SubDomain %d receives %d messages.\n",ii ,k );
//              for(jj=0; jj < k; jj++){
//                 printf("\t\t Message %d from process %d.\n", jj, ModARcv[ii].SDSlab[jj].Sendr );
//                 for ( l=0; l < dim; l++ ) {
//                   printf("\t\tIniPos Direction %d: %d\n", l, 
//                                             ModARcv[ii].SDSlab[jj].IniPos[l]);
//                   printf("\t\tLength  Direction %d: %d\n", l,
//                                             ModARcv[ii].SDSlab[jj].Length[l]);
//                 }          
//              }
//           }
//         }
//         printf("Producer Model Name: %s\n", ModelB->ModName);
//         printf("Total Number of subdomains: %d\n", nModBSD);
//         for(ii=0; ii < nModBSD; ii++ ) {
//            printf("Rank in subdomain %d: %d\n", ii, ModBSen[ii].Sendr );
//            if ( ModBSen[ii].Nmsg == 0 ) {
//               printf("\tSUBDOMAIN  %d DOES NOT LAY ON COUPLING DOMAIN.\n\n", ii );
//            } else {
//               k = ModBSen[ii].Nmsg;
//               for ( jj=0; jj < k; jj++ ) {
//                  printf("\t\t Message %d to process %d.\n", jj, ModBSen[ii].Msend[jj].Recvr );
//                  for ( l=0; l < dim; l++ ) {
//                     printf("\t\tIniVal Direction %d: %f\n", l,
//                                                      ModBSen[ii].Msend[jj].IniVal[l] );
//                     printf("\t\tEndVal Direction %d: %f\n", l,
//                                                      ModBSen[ii].Msend[jj].EndVal[l] );
//                     printf("\t\tLength Direction %d: %d\n", l,
//                                                      ModBSen[ii].Msend[jj].Length[l] );
//                  }
//               } /* End of for ( jj=0; jj < k; jj++ ) */
//            }
//       
//         } /* End of for(ii=0; ii < nModBSD; ii++ ) */  
/*                                                     END DEBUG */
/*                                                   BEGIN DEBUG */
//         printf("Consumer Model Name: %s\n", ModelB->ModName);
//         printf("Total Number of subdomains: %d\n", nModBSD);
//         for(ii=0; ii<nModBSD; ii++){
//           printf("Rank in subdomain %d: %d\n", ii, ModBRcv[ ii ].Recvr);
//           if ( ModBRcv[ ii ].Nmsg == 0 ) {
//              printf("\tSUBDOMAIN  %d DOES NOT LAY ON COUPLING DOMAIN.\n\n", ii );
//           } else {
//              for ( jj=0; jj < dim; jj++ ) {
//                printf("\tIniVal Direction %d: %f\n", jj, ModBRcv[ ii ].IniVal[jj]);
//                printf("\tEndVal Direction %d: %f\n", jj, ModBRcv[ ii ].EndVal[jj]);
//                printf("\tGlbInd Direction %d: %d\n", jj, ModBRcv[ ii ].GlbInd[jj]);
//                printf("\tNpts  Direction %d: %d\n", jj, ModBRcv[ ii ].Npts[jj]);
//              }
//              k = ModBRcv[ ii ].Nmsg;
//              printf("\t**** SubDomain %d receives %d messages.\n",ii ,k );
//              for(jj=0; jj < k; jj++){
//                 printf("\t\t Message %d from process %d.\n", jj, ModBRcv[ii].SDSlab[jj].Sendr );
//                 for ( l=0; l < dim; l++ ) {
//                   printf("\t\tIniPos Direction %d: %d\n", l, 
//                                             ModBRcv[ii].SDSlab[jj].IniPos[l]);
//                   printf("\t\tLength  Direction %d: %d\n", l,
//                                             ModBRcv[ii].SDSlab[jj].Length[l]);
//                 }          
//              }
//           }
//         }
//         printf("Producer Model Name: %s\n", ModelA->ModName);
//         printf("Total Number of subdomains: %d\n", nModASD);
//         for(ii=0; ii < nModASD; ii++ ) {
//            printf("Rank in subdomain %d: %d\n", ii, ModASen[ii].Sendr );
//            if ( ModASen[ii].Nmsg == 0 ) {
//               printf("\tSUBDOMAIN  %d DOES NOT LAY ON COUPLING DOMAIN.\n\n", ii );
//            } else {
//               k = ModASen[ii].Nmsg;
//               for ( jj=0; jj < k; jj++ ) {
//                  printf("\t\t Message %d to process %d.\n", jj, ModASen[ii].Msend[jj].Recvr );
//                  for ( l=0; l < dim; l++ ) {
//                     printf("\t\tIniVal Direction %d: %f\n", l,
//                                                      ModASen[ii].Msend[jj].IniVal[l] );
//                     printf("\t\tEndVal Direction %d: %f\n", l,
//                                                      ModASen[ii].Msend[jj].EndVal[l] );
//                     printf("\t\tLength Direction %d: %d\n", l,
//                                                      ModASen[ii].Msend[jj].Length[l] );
//                  }
//               } /* End of for ( jj=0; jj < k; jj++ ) */
//            }
//       
//         } /* End of for(ii=0; ii < nModASD; ii++ ) */
//       }
/*                                                     END DEBUG */

   } /* End of for ( icpl = 0; icpl < DCT_LocalCouple; icpl++ ) */
   
   ierr.Error_code = DCT_SUCCESS;
   ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
   return(ierr);
  
/* ---------------------------------  END( DCT_Local_SubDom_Comm ) */
}

/*******************************************************************/
/*                  DCT_Broker_Chg_CPLHollowVars                  */
/*                                                                 */
/*  This routine change from DCT_Hollow_CPLVar to DCT_Hollow_Var  */
/*  structures in DCT_Couple structures                           */
/*                                                                 */
/*******************************************************************/
DCT_Error DCT_Broker_Chg_CPLHollowVars()
{
/* ---------------------------------------------  Local Variables  */

   DCT_Error            ierr;
  
   DCT_Hollow_CPLVar      *CPLVar2;
   DCT_Hollow_Var         *Var2;

   DCT_List               *cplcurr;
   DCT_List               *current;
   DCT_CPL_Node           *cplnode;
   DCT_CPL_Vars           *CPLMasterT;

   DCT_Couple             *couple;
   
   DCT_Integer             vind;
   
   register int             ii, jj, nvar;

/* ------------------------  BEGIN( DCT_Broker_Chg_CPLHollowVars ) */

   /* Master Couple List is plugged */
   CPLMasterT = &(DCT_CPL_Table.CPLMasterTable);

   cplcurr = DCT_Reg_Couple;
   for ( jj = 0; ( jj < DCT_LocalCouple) && (cplcurr != (DCT_List *) NULL); jj++ ) {
      DCT_List_Ext (&cplcurr, (DCT_VoidPointer *) &couple);
   
      /* Last master table index */
      vind = couple->CoupleFirstVarCPL;
      nvar = (int) couple->CoupleTotNumVars;
      
      current = (DCT_List *) couple->CouplingTable;
      /* The coupled var list is sweep */
      for ( ii=0; (ii < nvar) && (current != (DCT_List *) NULL ); ii++ ) {
         /* Node in the DCT_Couple  vars list is got */
         DCT_List_Ext ( &current, (DCT_VoidPointer *) &cplnode );
         
         /* Check if the node is the coorect node to update */
         DCTERROR( cplnode->CoupleVars1 != CPLMasterT->CPLVarLocal[ vind + ii ],
                  "Internal Error. Updating wrong CPLMasterTable entry",
                  ierr.Error_code = DCT_UNKNOWN_ERROR; return(ierr) );

         CPLVar2 = cplnode->CoupleVars2;
         Var2 = (DCT_Hollow_Var *) malloc( sizeof(DCT_Hollow_Var) );
         DCTERROR( Var2 == (DCT_Hollow_Var *) NULL, "Memory Allocation Failed",
                               ierr.Error_code = DCT_FAILED_MALLOC; return(ierr) );
         /* The information is passed to Var2 */
         Var2->VarName = (DCT_Name)
                         malloc( sizeof(char)*(size_t)(strlen( CPLVar2->VarName )+1) );
         DCTERROR( Var2 == (DCT_Hollow_Var *) NULL, "Memory Allocation Failed",
                               ierr.Error_code = DCT_FAILED_MALLOC; return(ierr) );
         strcpy( Var2->VarName, CPLVar2->VarName );
         Var2->VarTimeIni = CPLVar2->VarTimeIni;
         Var2->VarFrequency = CPLVar2->VarFrequency;
         Var2->VarLastUpdate = DCT_TIME_UNSET;

         
         cplnode->CoupleVars2 = Var2; /* That is probably no necessary */
         CPLMasterT->CPLVarRemote[ vind + ii ] = Var2;

      } /* End of while ( current != (DCT_List *) NULL ) */

   } /* End of while (cplcurr != (DCT_List *) NULL) */

  
   ierr.Error_code = DCT_SUCCESS;
   ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
   return(ierr);

/* --------------------------  END( DCT_Broker_Chg_CPLHollowVars ) */
}

/*******************************************************************/
/*                        DCT_Broker_Delete                       */
/*                                                                 */
/*  This routine cleans the information gathered from other       */
/*  processes put into the list                                   */
/*                                                                 */
/*******************************************************************/
DCT_Error DCT_Broker_Delete(  )
{
/* ---------------------------------------------  Local Variables  */
  
   DCT_Error            ierr;
   DCT_List            *current;
   DCT_List            *previous;
   DCT_Couple          *couple;
   DCT_Model           *Model;
   
   DCT_Hollow_CPLVar   *DCTVar;
   
//    DCT_Field           *field;
//    DCT_3d_Var          *var3d;
//    DCT_4d_Var          *var4d;
   
   DCT_Object_Types     VarTypes;
//    DCT_VoidPointer      CVars;

//    register int            ii;
  
/* -----------------------------------  BEGIN( DCT_Broker_Delete ) */


   /* Obtaining the first non local couple node */
   current = DCT_Reg_Couple;
   if ( DCT_List_Break_Nnode ( &current, DCT_LocalCouple ) != DCT_LocalCouple ) {
      ierr.Error_code  = DCT_COUPLE_LIST_ERROR;
      ierr.Error_msg   = (DCT_String) malloc((size_t)60);
      sprintf(ierr.Error_msg,
        "[DCT_Broker_Delete] Error sweeping the local couple list.\n");
      return(ierr);
   }
   /* Free the non local couple structures */
   previous = current;
   while (current != (DCT_List *) NULL) {
      DCT_List_Ext ( &current, (DCT_VoidPointer *) &couple ); /* pop up the couple */
      /* Clean the couple   */
      DCT_Destroy_Couple( couple );
      free ( couple );
   }
   DCT_List_Clear ( &previous );

   /* Obtaining the first non local var node */
   current = DCT_Reg_Vars;
   if ( DCT_List_Break_Nnode ( &current, DCT_LocalVars ) != DCT_LocalVars ) {
      ierr.Error_code  = DCT_VARS_LIST_ERROR;
      ierr.Error_msg   = (DCT_String) malloc((size_t)60);
      sprintf(ierr.Error_msg,
        "[DCT_Broker_Delete] Error sweeping the local variable list.\n");
      return(ierr);
   }
   /* Free the non local variable structures */
   previous = current;
   while (current != (DCT_List *) NULL) {
      DCT_List_ExtVar( &current, (DCT_VoidPointer *)&DCTVar, &VarTypes );
      if ( VarTypes != DCT_HCPLVAR_TYPE) {
         ierr.Error_code  = DCT_VARS_LIST_ERROR;
         ierr.Error_msg   = (DCT_String) malloc((size_t)60);
         sprintf(ierr.Error_msg,
           "[DCT_Broker_Delete] Error type of non local variable list type.\n");
         return(ierr);
      }
      else {
         free ( DCTVar->VarName );
         free ( DCTVar );
      }
      
   }
   DCT_List_Clear ( &previous );
   
   /* Obtaining the first non local model node */
   current = DCT_Reg_Model;
   if ( DCT_List_Break_Nnode ( &current, DCT_LocalModel ) != DCT_LocalModel ) {
      ierr.Error_code  = DCT_MODEL_LIST_ERROR;
      ierr.Error_msg   = (DCT_String) malloc((size_t)60);
      sprintf(ierr.Error_msg,
        "[DCT_Broker_Delete] Error sweeping the local model list.\n");
      return(ierr);
   }
   /* Free the non local model structures */
   previous = current;
   while (current != (DCT_List *) NULL) {
      DCT_List_Ext ( &current, (DCT_VoidPointer *) &Model ); /* pop up the model */
      /* Clean Models     */
      DCT_Destroy_Model( Model );
      free ( Model );
   }
   DCT_List_Clear ( &previous );


  
   ierr.Error_code = DCT_SUCCESS;
   ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
   return(ierr);
  
/* -------------------------------------  END( DCT_Broker_Delete ) */
  
}

/*******************************************************************/
/*                          DCT_Reg_Delete                        */
/*                                                                 */
/*  This routine cleans the information gathered from other       */
/*  processes that are not the Registration Broker                */
/*                                                                 */
/*******************************************************************/
DCT_Error DCT_Reg_Delete(  )
{
/* ---------------------------------------------  Local Variables  */
  
   DCT_Error            ierr;
   DCT_List            *current;
   DCT_Couple          *couple;
   DCT_Model           *Model;
  
/* --------------------------------------  BEGIN( DCT_Reg_Delete ) */

   /* The list of the couples */
   current = DCT_Reg_Couple;
   while (current != (DCT_List *) NULL) {
      DCT_List_Ext ( &current, (DCT_VoidPointer *) &couple ); /* pop up the couple */
      Model = couple->CoupleModelB;
      couple->CoupleModelB = (DCT_Model *)NULL;
      /* Clean the couple   */
      DCT_Destroy_Model( Model );
      free ( Model );
   }


  
   ierr.Error_code = DCT_SUCCESS;
   ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
   return(ierr);
  
/* ----------------------------------------  END( DCT_Reg_Delete ) */
  
}


/*******************************************************************/
/*                          DCT_RegCouple                         */
/*                                                                 */
/*  This routine is called by the rest processes to send the      */
/*  DCT_Couple structures to the broker.                          */
/*                                                                 */
/*  tagmsg:  Array to register the message tags tp know in which  */
/*           couple the model and vars structure must be sent    */
/*  ncplmsg: Counter which register the number of messages to be  */
/*           received later by broker with DCT_Model and var info */
/*******************************************************************/
DCT_Error DCT_RegCouple( DCT_Couple **lcpl, DCT_Integer *ncplmsg  )
{
/* ---------------------------------------------  Local Variables  */
  
   DCT_Error            ierr;
  
/* ---------------------------------------  BEGIN( DCT_RegCouple ) */

   *ncplmsg = 0;
   /** The couple structures and model header are sent to the broker **/
   if ( DCT_Send_Couple( lcpl ) ) {
      ierr.Error_code  = DCT_FAILED_LIB_COMM;
      ierr.Error_msg   = (DCT_String) malloc((size_t)60);
      sprintf(ierr.Error_msg,
        "[DCT_RegCouple] Error sending DCT_Couple structures.\n");
      return(ierr);
   }
   /** The process received back the tags **/
   if ( DCT_Get_Tag( lcpl, ncplmsg ) ) {
      ierr.Error_code  = DCT_FAILED_LIB_COMM;
      ierr.Error_msg   = (DCT_String) malloc((size_t)60);
      sprintf(ierr.Error_msg,
        "[DCT_RegCouple] Error obtaining tags.\n");
      return(ierr);
   }
   DCT_Couple_ListOrder( lcpl, DCT_LocalCouple );
  
   ierr.Error_code = DCT_SUCCESS;
   ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
   return(ierr);
  
/* -----------------------------------------  END( DCT_RegCouple ) */
  
}

/*******************************************************************/
/*                    DCT_Grouping_Models                         */
/*                                                                 */
/*  This routine create the mechanism to group the processes for  */
/*  every model                                                   */
/*******************************************************************/
DCT_Error DCT_Grouping_Models(  )
{
/* ---------------------------------------------  Local Variables  */
  
   DCT_Error          ierr;
   
   DCT_List          *current;
   DCT_Model         *Model;
   
   DCT_Model        **lmod;
   
   int                ii;
  
/* ---------------------------------  BEGIN( DCT_Grouping_Models ) */

   lmod = (DCT_Model **) malloc( sizeof(DCT_Model *)*(size_t)DCT_LocalModel );
   DCTERROR( lmod == (DCT_Model **) NULL, "Memory Allocation Failed",
                   ierr.Error_code = DCT_FAILED_MALLOC; return(ierr) );

   current = DCT_Reg_Model;
   for ( ii =0; ii < DCT_LocalModel; ii++ ) {
      DCT_List_Ext (&current, (DCT_VoidPointer *) &Model);
      lmod[ii] = Model;
   }


   DCT_Model_ListOrder( lmod, DCT_LocalModel );
   
   for ( ii =0; ii < DCT_LocalModel; ii++ ) {
   
      if ( DCT_Create_Model_Comm( lmod[ii] ) ) {
         ierr.Error_code  = DCT_FAILED_LIB_COMM;
         ierr.Error_msg   = (DCT_String) malloc((size_t)150);
         sprintf(ierr.Error_msg,
           "[DCT_Grouping_Models] Error creating the group of a model.\n");
         return(ierr);
      }
   
   }
   
   free ( lmod );
  
   ierr.Error_code = DCT_SUCCESS;
   ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
   return(ierr);
  
/* -----------------------------------  END( DCT_Grouping_Models ) */
  
}

/*******************************************************************/
/*                   DCT_DeGrouping_Models                        */
/*                                                                 */
/*  This routine releases the mechanism to group the processes    */
/*  for every model                                               */
/*******************************************************************/
DCT_Error DCT_DeGrouping_Models(  )
{
/* ---------------------------------------------  Local Variables  */
  
   DCT_Error          ierr;
   
   DCT_List          *current;
   DCT_Model         *Model;
   
   int                ii;
  
/* -------------------------------  BEGIN( DCT_DeGrouping_Models ) */

   current = DCT_Reg_Model;
   for ( ii =0; ii < DCT_LocalModel; ii++ ) {
      DCT_List_Ext (&current, (DCT_VoidPointer *) &Model);
      if ( DCT_Destroy_Model_Comm( Model ) ) {
         ierr.Error_code  = DCT_FAILED_LIB_COMM;
         ierr.Error_msg   = (DCT_String) malloc((size_t)150);
         sprintf(ierr.Error_msg,
           "[DCT_DeGrouping_Models] Error creating the group of a model.\n");
         return(ierr);
      }
   }
  
   ierr.Error_code = DCT_SUCCESS;
   ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
   return(ierr);
  
/* ---------------------------------  END( DCT_DeGrouping_Models ) */
  
}

/*******************************************************************/
/*                     DCT_VarNode_List_Set                       */
/*                                                                 */
/*!    This routine fill the array with local variables and
       it is ordered using the VarMsgTag as key field.

       \param[in,out] VarList  Pointer to an array already
                               allocated. The length is the global
                               variable DCT_CPLVars.

    \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_VarNode_List_Set( DCT_Var_Node *VarList )
{
/* ---------------------------------------------  Local Variables  */
  
   DCT_CPL_Vars        *CPLMasterT;

   DCT_List            *current;
   DCT_Couple          *couple;
   
   DCT_Field           *field;
   DCT_3d_Var          *var3d;
   DCT_4d_Var          *var4d;
   
   DCT_Object_Types     CVarTypes;
   DCT_VoidPointer      CVars1;
   DCT_ProdCons         CVarProd;
   
   DCT_Produce_Plan    *VarProd;
   DCT_Consume_Plan    *VarCons;

   DCT_Error            ierr;
   
   int                  ii, jj, nvars, cplind;
   DCT_Integer          varscount, cplvartot, mtind;
  
/* --------------------------------  BEGIN( DCT_VarNode_List_Set ) */

   
   /* Access to the master table */
   CPLMasterT = &(DCT_CPL_Table.CPLMasterTable);
   cplvartot = DCT_CPL_Table.TotNumCPLVars;
   

   current = DCT_Reg_Couple;
   varscount = 0;
   for ( jj = 0; (jj < DCT_LocalCouple) && (current != (DCT_List *) NULL); jj++ ) {
      
      DCT_List_Ext (&current, (DCT_VoidPointer *) &couple);
      
      /* Get the number of couple vars in this DCT_Couple */
      nvars = (int) couple->CoupleTotNumVars;
      /* Get the first element in the master table */
      cplind = (int) couple->CoupleFirstVarCPL;
   
      /* For each pair of variables */
      for ( ii=0; ii < nvars; ii++ ) {
         DCTERROR( varscount == cplvartot, 
            "Internal Error. Failure in Number of Coupled Variables exceeds DCT_CPLVars",
            ierr.Error_msg = NULL; ierr.Error_code = DCT_UNKNOWN_ERROR; return(ierr) );

         mtind = cplind + ii;
         (VarList + varscount)->CPLMTIndex = mtind;
         CVars1 = CPLMasterT->CPLVarLocal[mtind];
         CVarTypes = CPLMasterT->CPLVarTypes[mtind];

         (VarList + varscount)->VarAddr = CVars1;
         (VarList + varscount)->VarType = CVarTypes;
         switch (CVarTypes)
         {
            case DCT_FIELD_TYPE:
               field = (DCT_Field *)CVars1;
               CVarProd = field->VarProduced;
               break;
            case DCT_3D_VAR_TYPE:
               var3d = (DCT_3d_Var *)CVars1;
               CVarProd = var3d->VarProduced;
               break;
            case DCT_4D_VAR_TYPE:
               var4d = (DCT_4d_Var *)CVars1;
               CVarProd = var4d->VarProduced;
         }
         (VarList + varscount)->VarProd = CVarProd;
         if ( CVarProd == DCT_PRODUCE ) {
            (VarList + varscount)->SDComm = &(couple->CoupleSnd);
            VarProd = (DCT_Produce_Plan *) CPLMasterT->CPLDataComm[mtind];
            (VarList + varscount)->VarMsgTag = VarProd->VarMsgTag;
         }
         else {
            (VarList + varscount)->SDComm = &(couple->CoupleRcv);
            VarCons = (DCT_Consume_Plan *) CPLMasterT->CPLDataComm[mtind];
            (VarList + varscount)->VarMsgTag = VarCons->VarMsgTag;
         }
         varscount++;
         
      } /* End of for ( ii=0; ii < nvars; ii++ ) */
   } /* End of while (current != (DCT_List *) NULL) */

   /* The array is sorted using qsort function */ 
   qsort( VarList, (size_t)varscount, sizeof(DCT_Var_Node), DCT_Var_Node_Comp );

   ierr.Error_code = DCT_SUCCESS;
   ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
   return(ierr);
  
/* ----------------------------------  END( DCT_VarNode_List_Set ) */
  
}


/*******************************************************************/
/*                        DCT_VarComm_Set                         */
/*                                                                 */
/*!    This routine fill the array with local variables and it is
    ordered using the VarMsgTag as key field

    \param[in,out] VarList  Pointer to an array already allocated.
                            The length is the global variable
                            DCT_CPLVars.

    \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_VarComm_Set( DCT_Var_Node *VarList )
{
/* ---------------------------------------------  Local Variables  */
  
   DCT_Error            ierr;
   
   int                  ii;
  
/* -------------------------------------  BEGIN( DCT_VarComm_Set ) */

   
   for ( ii=0; ii < DCT_CPLVars; ii++ ) {
      if ( (VarList + ii)->VarProd == DCT_PRODUCE ) {
           if ( DCT_ProduceVar_Comm( (VarList + ii) ) ) {
              ierr.Error_code  = DCT_UNKNOWN_ERROR;
              ierr.Error_msg   = (DCT_String) malloc((size_t)150);
              sprintf(ierr.Error_msg,
                 "[DCT_VarComm_Set] Error processing communication of the producer variable tagged %d.\n",
                 (VarList + ii)->VarMsgTag );
              return(ierr);
           }
      }
      else {
           if ( DCT_ConsumeVar_Comm( (VarList + ii) ) ) {
              ierr.Error_code  = DCT_UNKNOWN_ERROR;
              ierr.Error_msg   = (DCT_String) malloc((size_t)150);
              sprintf(ierr.Error_msg,
                 "[DCT_VarComm_Set] Error processing communication of the consumer variable tagged %d.\n",
                 (VarList + ii)->VarMsgTag );
              return(ierr);
           }
      }
   }

   ierr.Error_code = DCT_SUCCESS;
   ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
   return(ierr);
  
/* ---------------------------------------  END( DCT_VarComm_Set ) */
  
}


/*******************************************************************/
/*                   DCT_Var_TableIndex_Create                    */
/*                                                                 */
/*  This routine allocate the array of index in each DCT_{Vars}   */
/*                                                                 */
/*******************************************************************/
DCT_Error DCT_Var_TableIndex_Create(  )
{
/* ---------------------------------------------  Local Variables  */
  
   DCT_Error            ierr;

   DCT_List            *current;
   DCT_Object_Types     vartype;
   
   DCT_VoidPointer      dctvar;

   DCT_Field           *field;
   DCT_3d_Var          *var3d;
   DCT_4d_Var          *var4d;

   DCT_Integer        **cplind;
   DCT_Integer         *cpln;
   
//    int                  ii;
  
/* ---------------------------  BEGIN( DCT_Var_TableIndex_Create ) */

   
   /* Sweep its own DCT_Vars list */
   current = DCT_Reg_Vars;
   while (current != (DCT_List *) NULL) {
      DCT_List_ExtVar ( &current, &dctvar, &vartype );
      switch (vartype) {
         case DCT_FIELD_TYPE:
           field = (DCT_Field *) dctvar;
           cpln   = &(field->VarNumCpl);
           cplind = &(field->VarCplIndx);
         break;
         case DCT_3D_VAR_TYPE:
           var3d = (DCT_3d_Var *) dctvar;
           cpln   = &(var3d->VarNumCpl);
           cplind = &(var3d->VarCplIndx);
         break;
         case DCT_4D_VAR_TYPE:
           var4d = (DCT_4d_Var *) dctvar;
           cpln   = &(var4d->VarNumCpl);
           cplind = &(var4d->VarCplIndx);
         break;
         default:
           ierr.Error_code  = DCT_INVALID_DATA_TYPE;
           ierr.Error_msg   = (DCT_String) malloc((size_t)150);
           sprintf(ierr.Error_msg,
              "[DCT_Var_TableIndex_Create] Error DCT Vars type.\n");
           return(ierr);
      }
      /* The index array is allocated */
      *cplind = (DCT_Integer *) malloc( sizeof(DCT_Integer)* (size_t)*cpln );
      DCTERROR( *cplind == (DCT_Integer *) NULL, "Memory Allocation Failed",
                                       ierr.Error_code = DCT_FAILED_MALLOC; return(ierr) );
      *cpln = 0;  /* The counter is reinitialized when the indices will be stored */
   }

   ierr.Error_code = DCT_SUCCESS;
   ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
   return(ierr);
  
/* -----------------------------  END( DCT_Var_TableIndex_Create ) */
  
}

/*******************************************************************/
/*                      DCT_Local_Data_Delete                     */
/*                                                                 */
/*   This routine should be called from DCT_EndRegistration to    */
/*   clear all the local intermediate DCT structures.            */
/*                                                                 */
/*******************************************************************/
DCT_Error DCT_Local_Data_Delete(  )
{
/* ---------------------------------------------  Local Variables  */
   DCT_List            *current;
   DCT_Couple          *cop;
   DCT_Error            ierr;
   DCT_Integer          nmsg;
   DCT_SD_Produce      *ModSDMsend;
   DCT_Data_SubDom     *SndrSDom;
   DCT_SD_Consume      *ModSDMrecv;
   DCT_SDProduce_Plan  *CSnd;
   DCT_SDConsume_Plan  *CRcv;

   register int         ii;
  
/* -------------------------------  BEGIN( DCT_Local_Data_Delete ) */
  
   /* Obtaining the local couple structures */
   current = DCT_Reg_Couple;
   while (current != (DCT_List *) NULL) {
      DCT_List_Ext ( &current, (DCT_VoidPointer *) &cop ); /* pop up the couple */
      /* Clean the intermediate couple structure  */
      CRcv = &(cop->CoupleRcv);
      CRcv->Recvr = DCT_UNDEFINED_RANK;
      if ( CRcv->IniVal != (DCT_Scalar *)NULL ) {
         free( CRcv->IniVal );
         CRcv->IniVal = (DCT_Scalar *)NULL;
      }
      if ( CRcv->EndVal != (DCT_Scalar *)NULL ) {
         free( CRcv->EndVal );
         CRcv->EndVal = (DCT_Scalar *)NULL;
      }
      if ( CRcv->GlbInd != (DCT_Integer *)NULL ) {
         free( CRcv->GlbInd );
         CRcv->GlbInd = (DCT_Integer *)NULL;
      }
      if ( CRcv->Npts   != (DCT_Integer *)NULL ) {
         free( CRcv->Npts );
         CRcv->Npts = (DCT_Integer *)NULL;
      }
      if ( CRcv->SDSlab != (DCT_SD_Consume *)NULL ) {
         nmsg = CRcv->Nmsg;
         ModSDMrecv = CRcv->SDSlab;
         for ( ii=0; ii < nmsg; ii++ ) {
            free( ModSDMrecv->IniPos );
            free( ModSDMrecv->Length );
            ModSDMrecv++;
         }
         free( CRcv->SDSlab );
         CRcv->SDSlab = (DCT_SD_Consume *)NULL;
      }
      CRcv->Nmsg = DCT_NO_MSG;
      if ( CRcv->SndrNpts != (DCT_Integer *)NULL ) {
         free( CRcv->SndrNpts );
         CRcv->SndrNpts = (DCT_Integer *)NULL;
      }
      if ( CRcv->SndrSDom != (DCT_Data_SubDom *)NULL ) {
         nmsg = CRcv->SndrNSD;
         SndrSDom = CRcv->SndrSDom;
         for ( ii=0; ii < nmsg; ii++ ) {
            free ( SndrSDom->IniPos );
            free ( SndrSDom->EndPos );
            SndrSDom++;
         }
         free ( CRcv->SndrSDom );
         CRcv->SndrSDom = (DCT_Data_SubDom *)NULL;
      }
      
      CSnd = &(cop->CoupleSnd);
      CSnd->Sendr = DCT_UNDEFINED_RANK;
      if ( CSnd->Msend != (DCT_SD_Produce *)NULL ) {
         nmsg = CSnd->Nmsg;
         ModSDMsend = CSnd->Msend;
         for ( ii=0; ii < nmsg; ii++ ) {
            free ( ModSDMsend->IniVal );
            free ( ModSDMsend->EndVal );
            free ( ModSDMsend->Length );
            ModSDMsend++;
         }
         free( CSnd->Msend );
         CSnd->Msend = (DCT_SD_Produce *)NULL;
      }
      CSnd->Nmsg = DCT_NO_MSG;

   }

  
   ierr.Error_code = DCT_SUCCESS;
   ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
   return(ierr);
  
/* ---------------------------------  END( DCT_Local_Data_Delete ) */
  
}


/*******************************************************************/
/*                       DCT_EndRegistration                      */
/*                                                                 */
/*!     This routine should be call at the end of the Registration
     step.

 \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_EndRegistration(  )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;

  DCT_Integer          ncplmsg;
  DCT_Couple         **lcpl; /* Array of DCT_Couple to be sent */

  
  DCT_Var_Node        *VarList;
  
//   register int         ii;
  
/* ---------------------------------  BEGIN( DCT_EndRegistration ) */
  
   /* Check if the the function DCT_BeginRegistration() is the firstly called */
   if (DCT_Step_Check != DCT_BEGIN_STATE) {
      ierr.Error_code  = DCT_INVALID_OPERATION;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
         "[DCT_EndRegistration] Invalid Operation. The function DCT_BeginRegistration() must be called first.\n");
      return(ierr);
   }

   if ( (DCT_CPLVars == 0) || (DCT_LocalCouple == 0) ){
      ierr.Error_code  = DCT_INVALID_OPERATION;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
        "[DCT_EndRegistration] Invalid Operation. No DCT_Couple structure declared or no variables involved in coupling.\n");
      return(ierr);
   }
   
   
   /***** The global coupling structures and their pointer in each var are created */ 
   /* The Master Table is created */
   DCT_CPL_Table.CPLMasterTable.CPLVarLocal = (DCT_VoidPointer *)
               malloc( sizeof(DCT_VoidPointer)*(size_t)DCT_CPLVars );
   DCTERROR( DCT_CPL_Table.CPLMasterTable.CPLVarLocal == (DCT_VoidPointer *)NULL,
           "Memory Allocation Failed", ierr.Error_code = DCT_FAILED_MALLOC; return(ierr) );
   DCT_CPL_Table.CPLMasterTable.CPLVarRemote = (DCT_Hollow_Var **)
               malloc( sizeof(DCT_Hollow_Var *)*(size_t)DCT_CPLVars );
   DCTERROR( DCT_CPL_Table.CPLMasterTable.CPLVarRemote == (DCT_Hollow_Var **)NULL,
           "Memory Allocation Failed", ierr.Error_code = DCT_FAILED_MALLOC; return(ierr) );
   DCT_CPL_Table.CPLMasterTable.CPLVarTypes = (DCT_Object_Types *)
               malloc( sizeof(DCT_Object_Types)*(size_t)DCT_CPLVars );
   DCTERROR( DCT_CPL_Table.CPLMasterTable.CPLVarTypes == (DCT_Object_Types *)NULL,
           "Memory Allocation Failed", ierr.Error_code = DCT_FAILED_MALLOC; return(ierr) );
   DCT_CPL_Table.CPLMasterTable.CPLVarProd = (DCT_ProdCons *)
               malloc( sizeof(DCT_ProdCons)*(size_t)DCT_CPLVars );
   DCTERROR( DCT_CPL_Table.CPLMasterTable.CPLVarProd == (DCT_ProdCons *)NULL,
           "Memory Allocation Failed", ierr.Error_code = DCT_FAILED_MALLOC; return(ierr) );
   DCT_CPL_Table.CPLMasterTable.CPLDataComm = (DCT_VoidPointer *)
               malloc( sizeof(DCT_VoidPointer)*(size_t)DCT_CPLVars );
   DCTERROR( DCT_CPL_Table.CPLMasterTable.CPLDataComm == (DCT_VoidPointer *)NULL,
           "Memory Allocation Failed", ierr.Error_code = DCT_FAILED_MALLOC; return(ierr) );
   
   /** The arrays to index the master table are allocated **/
   ierr = DCT_Var_TableIndex_Create(  );
   /* Propagate the error message */
   if ( ierr.Error_code != DCT_SUCCESS ) return(ierr);
   
   lcpl = (DCT_Couple **) malloc(sizeof(DCT_Couple *)*(size_t)DCT_LocalCouple);
   DCTERROR( lcpl == (DCT_Couple **)NULL, "Memory Allocation Failed",
             ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

   /******** If it is the broker *********/
   DCT_Step_Check = DCT_BROKER_STATE;  /* Pass the variable identifying the broker step */
   if (DCT_procinfo.ProcDCTBroker == DCT_procinfo.ProcDCTRank) { /** BROKER **/
      /* Variable store the sequence of generating couple tags */
      DCT_Tag  CplTag = DCT_TAG_UNDEF;
      DCT_Tag  ModTag = DCT_TAG_UNDEF;
      DCT_Tag  VarTag = DCT_TAG_UNDEF;
      
      ierr = DCT_Broker_RegCouple( lcpl, &ncplmsg, &CplTag, &ModTag );
      /* Propagate the error message */
      if ( ierr.Error_code != DCT_SUCCESS ) return(ierr);
      
      /* Now all models are declared and identified,
        it's posible to group the processes of models */
      ierr = DCT_Grouping_Models(  );
      /* Propagate the error message */
      if ( ierr.Error_code != DCT_SUCCESS ) return(ierr);
      
      if ( DCT_Mutual_ModReg( lcpl ) ) {
         ierr.Error_code  = DCT_FAILED_LIB_COMM;
         ierr.Error_msg   = (DCT_String) malloc((size_t)150);
         sprintf(ierr.Error_msg,
           "[DCT_Mutual_ModReg] Error sending the mutual model registration data.\n");
         return(ierr);
      }
      
      ierr = DCT_Local_SubDom_Comm( lcpl );
      /* Propagate the error message */
      if ( ierr.Error_code != DCT_SUCCESS ) return(ierr);
      
      /********* RECEIVE THE DCT_MODEL HEADER AND THE VARIABLES ********/
      ierr = DCT_Broker_RegVar( ncplmsg, &VarTag );
      /* Propagate the error message */
      if ( ierr.Error_code != DCT_SUCCESS ) return(ierr);
      
      
      /***** CALCULATE AND SEND THE VARIABLES COUPLE INFORMATION ****/
      if ( DCT_Send_Var_Plan( lcpl ) ) {
         ierr.Error_code  = DCT_FAILED_LIB_COMM;
         ierr.Error_msg   = (DCT_String) malloc((size_t)150);
         sprintf(ierr.Error_msg,
           "[DCT_Send_Var_Plan] Error sending the variable coupling information.\n");
         return(ierr);
      }
     
   }
   else {                                 /********** NOT THE BROKER **********/
   
      ierr = DCT_RegCouple( lcpl, &ncplmsg );
      /* Propagate the error message */
      if ( ierr.Error_code != DCT_SUCCESS ) return(ierr);
      
      /* Now all models are declared and identified,
        it's posible to group the processes of models */
      ierr = DCT_Grouping_Models(  );
      /* Propagate the error message */
      if ( ierr.Error_code != DCT_SUCCESS ) return(ierr);
      
      /* If it is designated, receives the info of the counter model designated process  */
      if (ncplmsg) {
         if ( DCT_Mutual_ModReg( lcpl ) ) {
            ierr.Error_code  = DCT_FAILED_LIB_COMM;
            ierr.Error_msg   = (DCT_String) malloc((size_t)150);
            sprintf(ierr.Error_msg,
              "[DCT_Mutual_ModReg] Error exchanging the model subdomain data.\n");
            return(ierr);
         }

      }
      else { /* Wait to receive from the Model Leader */
         /* Who is no desiganted, just received from the model leader */
         if ( DCT_Recv_ModReg( lcpl ) ) {
            ierr.Error_code  = DCT_FAILED_LIB_COMM;
            ierr.Error_msg   = (DCT_String) malloc((size_t)150);
            sprintf(ierr.Error_msg,
              "[DCT_Recv_ModReg] Error receiving the model subdomain data from designated process.\n");
            return(ierr);
         }      
      }
      
      ierr = DCT_Local_SubDom_Comm( lcpl );
      /* Propagate the error message */
      if ( ierr.Error_code != DCT_SUCCESS ) return(ierr);      

      /* If it is designated, send the info of the model header and variables */
      if (ncplmsg) {
         if ( DCT_Send_Var( lcpl ) ) {
            ierr.Error_code  = DCT_FAILED_LIB_COMM;
            ierr.Error_msg   = (DCT_String) malloc((size_t)150);
            sprintf(ierr.Error_msg,
              "[DCT_Send_Var] Error sending data of variables.\n");
            return(ierr);
         }

         if ( DCT_GetSend_Var_Plan( lcpl ) ) {
            ierr.Error_code  = DCT_FAILED_LIB_COMM;
            ierr.Error_msg   = (DCT_String) malloc((size_t)150);
            sprintf(ierr.Error_msg,
              "[DCT_GetSend_Var_Plan] Error in designated processes receiving and resending coupled variable info.\n");
            return(ierr);
         }
      }
      else {
         /***** RECEIVE THE VARIABLES COUPLE INFORMATION ****/
         if ( DCT_Get_Var_Plan( lcpl ) ) {
            ierr.Error_code  = DCT_FAILED_LIB_COMM;
            ierr.Error_msg   = (DCT_String) malloc((size_t)150);
            sprintf(ierr.Error_msg,
              "[DCT_Get_Var_Plan] Error receiving from designated process coupled variables info.\n");
            return(ierr);
         }
      }
      
   } /* if (DCT_procinfo.ProcDCTBroker == DCT_procinfo.ProcDCTRank) */
   /****************            A L L   P R O C E S S E S         ****************/
   /*****     Process must be performed by all processes (No more broker)   ******/
   DCT_Step_Check = DCT_ALLREGISTER_STATE;  /* Pass the variable identifying the broker step */
   free( lcpl );
   
   /* Check for each couple (starting by the smallest tag value and following
     the order) and for each variable (starting ordering by tag value as well),
     the piece of subdomain that can be sent (Produce var), or check the chunks
     to be received (Comsume var).                                              */
   VarList = (DCT_Var_Node *) malloc( sizeof(DCT_Var_Node)*(size_t)DCT_CPLVars );
   DCTERROR( VarList == (DCT_Var_Node *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
            
   ierr = DCT_VarNode_List_Set( VarList );
   if ( ierr.Error_code != DCT_SUCCESS ) return(ierr);
            
   ierr = DCT_VarComm_Set( VarList );
   if ( ierr.Error_code != DCT_SUCCESS ) return(ierr);
   
   /** The VarList is freed **/
   free ( VarList );
   
   /***********     CLEAN ALL THE TEMPORARY STRUCTURES     ***********/
   /******** If it was the broker *********/
   if (DCT_procinfo.ProcDCTBroker == DCT_procinfo.ProcDCTRank) { /** BROKER **/
     /** The DCT_Hollow_CPLVar are changed to DCT_Hollow_Vars */
     DCT_Broker_Chg_CPLHollowVars( );
     /*********** CLEAN ALL THE TEMPORARY STRUCTURES ***********/
     ierr = DCT_Broker_Delete(  );
     if ( ierr.Error_code != DCT_SUCCESS ) return(ierr);
     
   }
   else {
      /* Clean the structures used during the registration */
      ierr = DCT_Reg_Delete(  );
      if ( ierr.Error_code != DCT_SUCCESS ) return(ierr);
   }
   /* The model groups are released */
   ierr = DCT_DeGrouping_Models(  );
   if ( ierr.Error_code != DCT_SUCCESS ) return(ierr);

   ierr = DCT_Local_Data_Delete(  );
   if ( ierr.Error_code != DCT_SUCCESS ) return(ierr);
   
  /*** The Registration Phase was made completely and is registered ***/
  DCT_Step_Check = DCT_END_STATE;

  /* All processes synchronize here berofe start the model */
  DCT_Sync( DCT_Comm_World );
  
  ierr.Error_code = DCT_SUCCESS;
  ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
  return(ierr);
  
/* -----------------------------------  END( DCT_EndRegistration ) */
  
}

/*******************************************************************/
/*                      DCT_Global_Data_Delete                    */
/*                                                                 */
/*   This routine should be called from DCT_Finalized to clear    */
/*   all the global DCT structures.                              */
/*                                                                 */
/*******************************************************************/
DCT_Error DCT_Global_Data_Delete(  )
{
/* ---------------------------------------------  Local Variables  */
   DCT_Error            ierr;

   DCT_Hollow_Var     **CPLVarRemote;
   DCT_Object_Types    *CPLVarTypes;
   DCT_ProdCons        *CPLVarProd;
   DCT_VoidPointer     *CPLDataComm;
   
   DCT_Produce_Plan    *ProdPlan;
   DCT_Consume_Plan    *ConsPlan;
   
   DCT_Request         *reqs;
   DCT_Comm_Datatype   *datatype;
   
   int                  ii, jj, nmsg, dim, nmax;
  
/* ------------------------------  BEGIN( DCT_Global_Data_Delete ) */
  
   /* Master table is freed */
   nmax = DCT_CPL_Table.TotNumCPLVars;
   CPLVarRemote = DCT_CPL_Table.CPLMasterTable.CPLVarRemote;
   CPLVarTypes = DCT_CPL_Table.CPLMasterTable.CPLVarTypes;
   CPLVarProd = DCT_CPL_Table.CPLMasterTable.CPLVarProd;
   CPLDataComm = DCT_CPL_Table.CPLMasterTable.CPLDataComm;
   
   
   for ( ii = 0; ii < nmax; ii++ ) {
      free ( CPLVarRemote[ii]->VarName );
      free ( CPLVarRemote[ii] );

      if ( CPLVarProd[ii] == DCT_PRODUCE ) {
         ProdPlan = (DCT_Produce_Plan *)CPLDataComm[ii];
         nmsg = ProdPlan->Nmsg;
         reqs = ProdPlan->reqsts;
         DCT_Free_Requests ( reqs, nmsg );
         free ( reqs );
         datatype = ProdPlan->DType;
         DCT_Free_DType( datatype, nmsg );
         free ( datatype );
      }
      else {
         ConsPlan = (DCT_Consume_Plan *)CPLDataComm[ii];
         nmsg = ConsPlan->Nmsg;
         reqs = ConsPlan->reqsts;
         DCT_Free_Requests ( reqs, nmsg );
         free ( reqs );
         datatype = ConsPlan->DType;
         DCT_Free_DType( datatype, nmsg );
         free ( datatype );
         
         if ( CPLVarTypes[ii] == DCT_FIELD_TYPE ) dim = 2;
         else if ( CPLVarTypes[ii] == DCT_3D_VAR_TYPE ) dim = 3;
         else if ( CPLVarTypes[ii] == DCT_4D_VAR_TYPE ) dim = 4;
   
         if (ConsPlan->VarLabels != (DCT_VoidPointer *)NULL ) {
            for ( jj=0; jj < dim; jj++ ) free ( ConsPlan->VarLabels[jj] );
            free ( ConsPlan->VarLabels );
         }
         if (ConsPlan->VarInt != (DCT_Integer **)NULL ) {
            for ( jj=0; jj < dim; jj++ ) free ( ConsPlan->VarInt[jj] );
            free ( ConsPlan->VarInt );
         }
         if ( ConsPlan->VarMask != (DCT_Integer *)NULL ) free(ConsPlan->VarMask);
         free ( ConsPlan->VarIniPos );
         free ( ConsPlan->VarNpts );
         free ( ConsPlan->VarDomType );
         free ( ConsPlan->VarRcvdNpts );
         /* Just only if the temporal variable was necessary */
         if (ConsPlan->VarRcvdVal != ConsPlan->VarTempVal ) free ( ConsPlan->VarTempVal );
         free ( ConsPlan->VarRcvdVal );
      }
      free ( CPLDataComm[ii] );
   }
   free ( DCT_CPL_Table.CPLMasterTable.CPLVarLocal );
   free ( CPLVarRemote );
   free ( CPLVarTypes );
   free ( CPLVarProd );
   free ( CPLDataComm );
   DCT_CPL_Table.TotNumCPLVars = 0;

   /* Free the local couple structures */
   DCT_List_Clear ( &DCT_Reg_Couple );

   /* Free the local model structures */
   DCT_List_Clear ( &DCT_Reg_Model );

   /* Free the local Variables structures */
   DCT_List_Clear ( &DCT_Reg_Vars );
   
   /*  Counter of local DCT_Couple structures created   */
   DCT_LocalCouple = 0;
   
   /*  Counter of local DCT_Model structures created   */
   DCT_LocalModel = 0;
   
   /*  Counter of local DCT Variables structures created */
   DCT_LocalVars = 0;
   
   /*  Counter of local Couple Variables declarared */
   DCT_CPLVars = 0;
  
  
   ierr.Error_code = DCT_SUCCESS;
   ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
   return(ierr);
  
/* --------------------------------  END( DCT_Global_Data_Delete ) */
  
}

/*******************************************************************/
/*                          DCT_Finalized                         */
/*                                                                 */
/*   This routine should be call at the end of the program, it    */
/*   frees and clear all the global structures.                  */
/*                                                                 */
/*******************************************************************/
DCT_Error DCT_Finalized(  )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  
/* ---------------------------------------  BEGIN( DCT_Finalized ) */
  
  /* Check if the the function DCT_EndRegistration() was called */
  if (DCT_Step_Check != DCT_END_STATE){
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)150);
    sprintf(ierr.Error_msg,
     "[DCT_Finalized] Invalid Operation. The function DCT_Finalized() must be called at the end.\n");
    return(ierr);
  }
  
  /* Synchronization point at the end */
  DCT_Sync( DCT_Comm_World );

  /* Frees the Communicator used in the library */
  if ( DCT_Free_Comm(  ) ){
      ierr.Error_code  = DCT_FAILED_LIB_COMM;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
              "[DCT_Finalized] It is not possible free the DCT_Comm_World Communicator\n");
              return(ierr);
  }
  
  /* Clear all the global structures  */
//   if (DCT_File_Path != NULL) {
//      free( DCT_File_Path );
//   }
//   if (DCT_File_Prefix != NULL) {
//      free( DCT_File_Prefix );
//   }
  
  /***** Clean all the global structures ***/
  ierr = DCT_Global_Data_Delete( );
  if ( ierr.Error_code != DCT_SUCCESS ) return(ierr);
  DCT_Step_Check = DCT_NULL_STATE;

  ierr.Error_code = DCT_SUCCESS;
  ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
  return(ierr);
  
/* -----------------------------------------  END( DCT_Finalized ) */
  
}


