/**************************************************************/
/*              Distributed Coupling Toolkit (DCT)            */
/*!
   \file dct_data_utils.c

   \brief Contains the implementation of the internal DCT and
          user support data functions.

    This file contains the different functions to handle several
    internal data structures and support some user data handeling.

    \date Date of Creation: Sep 16, 2005

    \author Dany De Cecchis: dcecchis@gmail.com
    \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.

*/
/**************************************************************/

/* -----------------------------------------------  Include Files  */
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "dct.h"
/* --------------------------------------------  Global Variables  */
#include "dctsys.h"

/*******************************************************************/
/*                  DCT_Generate_Spatial_Labels                    */
/*                                                                 */
/*!   Automatically generates a set of labels (tick marks) to be
      used in a variable or field.

      \param[in]  start  first spatial label in a given direction.
      \param[in]    end  last spatial label in a given direction.
      \param[in]    inc  increment between label marks.
      \param[out] point  Array of n generated labels.
      \param[out]     n  number of generated labels.

      \return An error handler variable.

      \attention To more practical sense, would be better that n is given
                 and is calculated the tiks and inc?

*/        
/*******************************************************************/
DCT_Error DCT_Gen_Spatial_Labels( const DCT_Scalar start, const DCT_Scalar end,
                                  const DCT_Scalar inc, DCT_Scalar **point,
                                  DCT_Integer *n )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Integer i, div;
  DCT_Error   ierr;

/* ------------------------------  BEGIN( DCT_Gen_Spatial_Labels ) */

  div= (DCT_Integer) (end - start)/inc;
  if ( (div*inc)<(end-start) ) {
    ierr.Error_code = DCT_INVALID_LABELING;
    ierr.Error_msg   = (DCT_String) malloc((size_t)120);
    sprintf(ierr.Error_msg,
    "[DCT_Gen_Spatial_Labels]: %6.2f does not envenly divide the space between %6.2f and %6.2f \n",inc, start, end);
  }
  else
  {
    *point = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)(div+1));

    if (point == NULL) {
      i = sizeof(DCT_Scalar)*(div+1);
      ierr.Error_code = DCT_FAILED_MALLOC;
      ierr.Error_msg   = (DCT_String) malloc((size_t)120);
      sprintf(ierr.Error_msg,
        "[DCT_Gen_Spatial_Labels]: Cannot Allocate %d bytes\n",i);
    }
    else {
      for (i=0 ; i <= div ; i++) {
        *(*point+i)= start + (inc*(DCT_Scalar)i);
        /*printf("point[%d]= %lf == %lf\n", i, (*point)[i], start + (inc*(DCT_Scalar)i));*/
      }
      *n = div+1;
      ierr.Error_code = DCT_SUCCESS;
      ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
    }
  }
  return(ierr);

/* --------------------------------  END( DCT_Gen_Spatial_Labels ) */

}

/*******************************************************************/
/*                    DCT_Gen_Number_Labels                        */
/*                                                                 */
/*   Automatically generates a set of labels (tick marks) to
     be used in a variable or field given the quantity of
     intervals, i.e the interval, from star to end, is divided
     into n subintervals.

     \param[in]  start  first spatial label in a given direction.
     \param[in]    end  last spatial label in a given direction.
     \param[in]      n  number of subintervals.
     \param[out] point  Array of n generated labels.
     \param[out]   dim  number of generated labels.

     \return An error handler variable.

     \attention This data depends on the Model (if the model structure
                is not deleted).
*/
/*******************************************************************/
DCT_Error DCT_Gen_Number_Labels( const DCT_Scalar start, const DCT_Scalar end,
                                 const DCT_Integer n, DCT_Scalar **point,
                                 DCT_Integer *dim)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Integer i;
  DCT_Scalar inc;
  DCT_Error   ierr;

/* -------------------------------  BEGIN( DCT_Gen_Number_Labels ) */

  inc= (end - start)/(DCT_Scalar)n;
  *dim = n+1;
  *point = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)(*dim));

  if (point == NULL) {
    i = sizeof(DCT_Scalar)*(*dim);
    ierr.Error_code = DCT_FAILED_MALLOC;
    ierr.Error_msg   = (DCT_String) malloc((size_t)120);
    sprintf(ierr.Error_msg,
      "[DCT_Gen_Number_Labels]: Cannot Allocate %d bytes\n",i);
  }
  else {
    for (i=0 ; i < *dim ; i++) {
      *(*point+i)= start + ((inc)*(DCT_Scalar)i);
      /*printf("point[%d]= %lf == %lf\n", i, (*point)[i], start + (inc*(DCT_Scalar)i));*/
    }

    ierr.Error_code = DCT_SUCCESS;
    ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
  }
  return(ierr);

/* ---------------------------------  END( DCT_Gen_Number_Labels ) */

}

/*******************************************************************/
/*                    Set_Distribution_Type                        */
/*                                                                 */
/*!    Tests for valid Parallel Layouts and copy the information
       to a variable.

      \param[in]      dist  Type of Distribution to be set
      \param[in]     procs  Array of 1 or more entries describing
                            dimension of the the number of
                            processors in every processor layout
                            (or distribution) if DCT_DIST_SEQUENTIAL
                            procs should be 1 for others it should
                            be an array of lenght >=2.
      \param[out]  vartype  Is the part of the variable or field
                            that has the DCT_DISTRIBUTION_TYPE
                            (check dct.h).
      \param[out] varprocs  Is the part of the variable of field
                            that holds the actual data distribution
                            pattern.

   \return A DCT_Boolean error variable with DCT_TRUE is complete
           successfully or DCT_FALSE if is not.

*/
/*******************************************************************/
DCT_Boolean Set_Distribution_Type( const DCT_Distribution dist, const DCT_Integer *procs,
                                  DCT_Distribution *vartype, DCT_Integer **varprocs)
{

/* -------------------------------  BEGIN( Set_Distribution_Type ) */

  switch(dist)
    {
    case DCT_DIST_NULL:
      return(DCT_FALSE);
      break;
    case DCT_DIST_SEQUENTIAL:
      if (*procs==1)
      {
        *vartype = DCT_DIST_SEQUENTIAL;
        *varprocs = (DCT_Integer *)malloc(sizeof(DCT_Integer));
        **varprocs = 1;
        return (DCT_TRUE);
      }
      else
        return (DCT_FALSE);
    break;
    case DCT_DIST_RECTANGULAR:
      if ((*procs>0)&&(*(procs+1)>0))
      {
        *vartype = DCT_DIST_RECTANGULAR;
        *varprocs = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t)2);
        **varprocs = *procs;
              *(*varprocs+1) = *(procs+1);
        return(DCT_TRUE);
      }
      else
        return (DCT_FALSE);
    break;
    case DCT_DIST_3D_RECTANGULAR:
      if ((*procs>0)&&(*(procs+1)>0)&&(*(procs+2)>0))
      {
        *vartype = DCT_DIST_3D_RECTANGULAR;
        *varprocs = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t)3);
        *(*varprocs) = *procs;
        *(*varprocs+1) = *(procs+1);
        *(*varprocs+2) = *(procs+2);
        return(DCT_TRUE);
      }
      else
        return (DCT_FALSE);
    break;
    default:
      return(DCT_FALSE);
    }

/* ---------------------------------  END( Set_Distribution_Type ) */

}

/*******************************************************************/
/*                         Set_Distribution                        */
/*                                                                 */
/*!  Tests for valid Processor Data Layouts and Assigns to    
     dist variable accordingly.      

    \param[in]      dist  Type of Distribution to be set                    
    \param[in]     procs  An array of 1 or more entries describing      
                          the number of processors in every
                          dimension of the processor layout (or                
                          distribution) if DCT_DIST_SEQUENTIAL          
                          procs should be 1 for others it should
                          be an array of lenght >= 2.    
    \param[out] varprocs  Is an allocated array where procs    
                          is copied.                                      
    \param[out] dimtotal  Return the product of procs entries.

   \return  A DCT_Boolean error variable with DCT_TRUE is complete
            successfully or DCT_FALSE if is not.

   \attention COULD REPLACE Set_Distribution_Type ABOVE.

*/
/******************************************************************/
DCT_Boolean Set_Distribution( const DCT_Distribution dist, const DCT_Integer *procs,
                              DCT_Integer **varprocs, DCT_Integer *dimtotal)
{
/* ---------------------------------------------  Local Variables  */
  *dimtotal=1;

/* ------------------------------------  BEGIN( Set_Distribution ) */

  switch(dist)
  {
    case DCT_DIST_NULL:
      return(DCT_FALSE);
    break;
    case DCT_DIST_SEQUENTIAL:
      if (*procs==1)
      {
        *varprocs = (DCT_Integer *)malloc(sizeof(DCT_Integer));
        DCTERROR( *varprocs == (DCT_Integer *) NULL, "Memory Allocation Failed", 
                  return(DCT_FALSE) );
        **varprocs = 1;
        /* *dimtotal=1, which is already */
      }
      else
        return (DCT_FALSE);
    break;
    case DCT_DIST_RECTANGULAR:
      if ((*procs>0)&&(*(procs+1)>0))
      {
        *varprocs = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t)2);
        DCTERROR( *varprocs == (DCT_Integer *) NULL, "Memory Allocation Failed",
                  return(DCT_FALSE) );
        **varprocs     = *procs;
        *(*varprocs+1) = *(procs+1);
        *dimtotal = *procs * *(procs+1);
      }
      else
        return (DCT_FALSE);
    break;
    case DCT_DIST_3D_RECTANGULAR:
      if ((*procs>0)&&(*(procs+1)>0)&&(*(procs+2)>0))
      {
        *varprocs = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t)3);
        DCTERROR( *varprocs == (DCT_Integer *) NULL, "Memory Allocation Failed",
                  return(DCT_FALSE) );
        *(*varprocs)   = *procs;
        *(*varprocs+1) = *(procs+1);
        *(*varprocs+2) = *(procs+2);
        *dimtotal = *procs * *(procs+1) * *(procs+2);
      }
      else
        return (DCT_FALSE);
    break;
    default:
      return(DCT_FALSE);
  }

  return(DCT_TRUE);

/* --------------------------------------  END( Set_Distribution ) */

}

/*******************************************************************/
/*                         DCT_Check_Intersec                      */
/*                                                                 */
/*!    Test if a list of subdomains intersect each other (i.e.
       a subdomain overlaps another). This function is based
       on the index order of the tick marks or cells, as a 
       structured mesh should be. The subdomains are defined
       as a correspondence of rectilinear tiles of the indexes
       of the mesh.

      \param[in] dimtotal  Number of subdomains in the array.
      \param[in]      dim  Domain dimension, i.e. 2, 3 or 4 for 2D,
                           3D or 4D domain, respectively.
      \param[in]   SubDom  Pointer to a sudomain array as DCT_SubDom
                           structure, see dct.h.

   \return  A DCT_Boolean indicating DCT_TRUE when there is not
             overlap of subdomains, or DCT_FALSE if there is.

*/
/*******************************************************************/

DCT_Boolean DCT_Check_Intersec( const DCT_Integer dimtotal, const DCT_Integer dim,
                                const DCT_Data_SubDom *SubDom )
{
/* ---------------------------------------------  Local Variables  */
  int           i,j;
  int           dimflag;
  DCT_Integer   inia, enda, inib, endb;

/* ----------------------------------  BEGIN( DCT_Check_Intersec ) */

  for ( i=0; i < dimtotal; i++ ) {
    for ( j=(i+1); j < dimtotal; j++ ) {
      dimflag =0;
      switch (dim) {
        case 4:
          inia = (SubDom+i)->IniPos[3];
          enda = (SubDom+i)->EndPos[3];
          inib = (SubDom+j)->IniPos[3];
          endb = (SubDom+j)->EndPos[3];

          /* Intersection in direction 3 */
          if ( ( ( (inia < inib) && (inib < enda) ) ||
                 ( (inia < endb) && (endb < enda) ) ) && (dim > 3) ) dimflag++;
        case 3:
          inia = (SubDom+i)->IniPos[2];
          enda = (SubDom+i)->EndPos[2];
          inib = (SubDom+j)->IniPos[2];
          endb = (SubDom+j)->EndPos[2];

          /* Intersection in direction 3 */
          if ( ( ( (inia < inib) && (inib < enda) ) ||
                 ( (inia < endb) && (endb < enda) ) ) && (dim > 2) ) dimflag++;

        case 2:
          inia = (SubDom+i)->IniPos[1];
          enda = (SubDom+i)->EndPos[1];
          inib = (SubDom+j)->IniPos[1];
          endb = (SubDom+j)->EndPos[1];

          /* Intersection in direction 2 */
          if ( ( ( (inia < inib) && (inib < enda) ) ||
                 ( (inia < endb) && (endb < enda) ) ) && (dim > 1) ) dimflag++;
          inia = (SubDom+i)->IniPos[0];
          enda = (SubDom+i)->EndPos[0];
          inib = (SubDom+j)->IniPos[0];
          endb = (SubDom+j)->EndPos[0];

          /* Intersection in direction 1 */
          if ( ( ( (inia < inib) && (inib < enda) ) ||
                 ( (inia < endb) && (endb < enda) ) ) && (dim > 0) ) dimflag++;
      }
      if (dimflag == dim ) return (DCT_TRUE);
    }
  }
  return (DCT_FALSE);

/* ------------------------------------  END( DCT_Check_Intersec ) */
}

/*******************************************************************/
/*                         DCT_Mask_Copy                           */
/*                                                                 */
/*!  Copies a user defined mask into a field variable area.

     \param[out] mask1  Output mask.
     \param[in]  mask2  Input  mask.
     \param[in]   i, j  Dimension of the mask.

     \return An error handler variable.

*/
/***************************************************************/
DCT_Error DCT_Mask_Copy( DCT_Boolean mask1[], const DCT_Boolean mask2[],
                         const DCT_Object_Types vartype, const DCT_Integer dims[])
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error  ierr;

  int i, n;

/* ---------------------------------------  BEGIN( DCT_Mask_Copy ) */

  if ( vartype == DCT_FIELD_TYPE )
    n = dims[0]*dims[1];
  else if ( vartype == DCT_3D_VAR_TYPE )
    n = dims[0]*dims[1]*dims[2];
  else if ( vartype == DCT_4D_VAR_TYPE )
    n = dims[0]*dims[1]*dims[2]*dims[3];
  else {
    ierr.Error_code = DCT_INVALID_ARGUMENT;
    ierr.Error_msg  = (DCT_String)malloc((size_t)100);
    sprintf(ierr.Error_msg, "[DCT_Mask_Copy] Unknown DCT Variable type: %d\n", vartype);
    return(ierr);
  }
  for( i = 0; i < n; i++)
    mask1[i] = mask2[i];
  ierr.Error_code = DCT_SUCCESS;
  ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* -----------------------------------------  END( DCT_Mask_Copy ) */
}

/*******************************************************************/
/*                   DCT_Model_And_Var_DomCheck                    */
/*                                                                 */
/*!     Check the model domain discretization and the variable
        discretization agree each other.

     \param[in]    tmod  Pointer to model domain.
     \param[in] DomType  Pointer to an array with the DCT_Domain_Type in
                         the DCT variable.
     \param[in]  Labels  Array of the DCT variable tickmar representation.
     \param[in]     dim  Value of the dimension of the DCT variable.

     \return An integer value as error control with 0 indicating success.

*/
/***************************************************************/
int DCT_Model_And_Var_DomCheck( DCT_Model_Dom *tmod, DCT_Domain_Type *DomType,
                                DCT_Scalar *Labels[], DCT_Integer *npts, DCT_Integer dim )
{
/* ---------------------------------------------  Local Variables  */
   int               ierr=0;

   DCT_Domain_Type  *ModDomType;
   DCT_Scalar       *DomValues;
   DCT_Scalar       *VarValues;
   
   DCT_Integer       varnpts, modnpts;
   
   int               ii;
/* --------------------------  BEGIN( DCT_Model_And_Var_DomCheck ) */

   ModDomType = tmod->ModDomType;

   for ( ii=0; (!ierr)&&(ii < dim); ii++ ) {
      DomValues = tmod->ModValues[ii];
      modnpts = tmod->ModNumPts[ii];
      VarValues = Labels[ii];
      varnpts = npts[ii];
      if ( (ModDomType[ii] == DCT_CARTESIAN ) &&
           (DomType[ii] == DCT_CARTESIAN ) ) {
         ierr |= ( VarValues[0] < DomValues[0] ) ||
                 ( DomValues[1] < VarValues[1] ) || ( modnpts < varnpts );
      }
      else if ( (ModDomType[ii] == DCT_RECTILINEAR ) &&
                (DomType[ii] == DCT_CARTESIAN ) ) {
         ierr |= ( VarValues[0] < DomValues[0] ) ||
                 ( DomValues[modnpts-1] < VarValues[1] ) || ( modnpts < varnpts );
      }
      else if ( (ModDomType[ii] == DCT_CARTESIAN ) &&
                (DomType[ii] == DCT_RECTILINEAR ) ) {
         ierr |= ( VarValues[0] < DomValues[0] ) ||
                 ( DomValues[1] < VarValues[varnpts-1] ) || ( modnpts < varnpts );
      }
      else if ( (ModDomType[ii] == DCT_RECTILINEAR ) &&
                (DomType[ii] == DCT_RECTILINEAR ) ) {
         ierr |= ( VarValues[0] < DomValues[0] ) ||
                 ( DomValues[modnpts-1] < VarValues[varnpts-1] ) || ( modnpts < varnpts );
      } else return (1); // Error in the input
   }

   return(ierr);

/* ----------------------------  END( DCT_Model_And_Var_DomCheck ) */

}

/*******************************************************************/
/*                       DCT_Change_TimeUnit                       */
/*                                                                 */
/*!   Translates the time1 in unit1 to its equivalent in unit2.

     \param[in] time1  The time value to be transformed.
     \param[in] unit1  The units that represents time1.
     \param[in] unit2  The units in wich time1 must be translated.

     \return The value of time1 translated from unit1 to unit2.

*/
/*******************************************************************/
DCT_Scalar DCT_Change_TimeUnit( DCT_Scalar time1, DCT_Time_Types unit1,
                                DCT_Time_Types unit2 )
{
/* ---------------------------------------------  Local Variables  */

/* ---------------------------------  BEGIN( DCT_Change_TimeUnit ) */

   if ( unit1 == DCT_TIME_NO_UNIT) {
      switch ( unit2 )
      {
         case DCT_TIME_NO_UNIT:
            return ( time1 );
            break;
         case DCT_TIME_SECONDS:
            return ( time1 );
            break;
         case DCT_TIME_MINUTES:
            return ( time1 );
            break;
         case DCT_TIME_HOURS:
            return ( time1 );
            break;
         case DCT_TIME_DAYS:
            return ( time1 );
            break;
         case DCT_TIME_WEEKS:
            return ( time1 );
            break;
         case DCT_TIME_YEARS:
            return ( time1 );
      }  /* End of  switch ( unit2 ) */
   
   } else if ( unit1 == DCT_TIME_SECONDS) {
      switch ( unit2 )
      {
         case DCT_TIME_NO_UNIT:
            return ( time1 );
            break;
         case DCT_TIME_SECONDS:
            return ( time1 );
            break;
         case DCT_TIME_MINUTES:
            return ( time1/60.0 );
            break;
         case DCT_TIME_HOURS:
            return ( time1/3600.0 );
            break;
         case DCT_TIME_DAYS:
            return ( time1/86400.0 );
            break;
         case DCT_TIME_WEEKS:
            return ( time1/604800.0 );
            break;
         case DCT_TIME_YEARS:
            return ( time1/31557600.0 );
      }  /* End of  switch ( unit2 ) */
   }
    else if ( unit1 == DCT_TIME_MINUTES) {
      switch ( unit2 )
      {
         case DCT_TIME_NO_UNIT:
            return ( time1 );
            break;
         case DCT_TIME_SECONDS:
            return ( time1*60.0 );
            break;
         case DCT_TIME_MINUTES:
            return ( time1 );
            break;
         case DCT_TIME_HOURS:
            return ( time1/60.0 );
            break;
         case DCT_TIME_DAYS:
            return ( time1/1440.0 );
            break;
         case DCT_TIME_WEEKS:
            return ( time1/10080.0 );
            break;
         case DCT_TIME_YEARS:
            return ( time1/525960.0 );
      }  /* End of  switch ( unit2 ) */
   }
    else if ( unit1 == DCT_TIME_HOURS) {
      switch ( unit2 )
      {
         case DCT_TIME_NO_UNIT:
            return ( time1 );
            break;
         case DCT_TIME_SECONDS:
            return ( time1*3600.0 );
            break;
         case DCT_TIME_MINUTES:
            return ( time1*60.0 );
            break;
         case DCT_TIME_HOURS:
            return ( time1 );
            break;
         case DCT_TIME_DAYS:
            return ( time1/24.0 );
            break;
         case DCT_TIME_WEEKS:
            return ( time1/168.0 );
            break;
         case DCT_TIME_YEARS:
            return ( time1/8766.0 );
      }  /* End of  switch ( unit2 ) */
   }
    else if ( unit1 == DCT_TIME_DAYS) {
      switch ( unit2 )
      {
         case DCT_TIME_NO_UNIT:
            return ( time1 );
            break;
         case DCT_TIME_SECONDS:
            return ( time1*86400.0 );
            break;
         case DCT_TIME_MINUTES:
            return ( time1*1440.0 );
            break;
         case DCT_TIME_HOURS:
            return ( time1*24.0 );
            break;
         case DCT_TIME_DAYS:
            return ( time1 );
            break;
         case DCT_TIME_WEEKS:
            return ( time1/7.0 );
            break;
         case DCT_TIME_YEARS:
            return ( time1/365.25 );
      }  /* End of  switch ( unit2 ) */
   }
    else if ( unit1 == DCT_TIME_WEEKS) {
      switch ( unit2 )
      {
         case DCT_TIME_NO_UNIT:
            return ( time1 );
            break;
         case DCT_TIME_SECONDS:
            return ( time1*604800.0 );
            break;
         case DCT_TIME_MINUTES:
            return ( time1*10080.0 );
            break;
         case DCT_TIME_HOURS:
            return ( time1*168.0 );
            break;
         case DCT_TIME_DAYS:
            return ( time1*7.0 );
            break;
         case DCT_TIME_WEEKS:
            return ( time1 );
            break;
         case DCT_TIME_YEARS:
            return ( time1/52.1785714285714 );
      }  /* End of  switch ( unit2 ) */
   }
    else if ( unit1 == DCT_TIME_YEARS) {
      switch ( unit2 )
      {
         case DCT_TIME_NO_UNIT:
            return ( time1 );
            break;
         case DCT_TIME_SECONDS:
            return ( time1*31557600.0 );
            break;
         case DCT_TIME_MINUTES:
            return ( time1*525960.0 );
            break;
         case DCT_TIME_HOURS:
            return ( time1*8766.0 );
            break;
         case DCT_TIME_DAYS:
            return ( time1*365.25 );
            break;
         case DCT_TIME_WEEKS:
            return ( time1*52.1785714285714 );
            break;
         case DCT_TIME_YEARS:
            return ( time1 );
      }  /* End of  switch ( unit2 ) */
   }

   /* If time units are wrong no change */
   return ( time1 );

/* -----------------------------------  END( DCT_Change_TimeUnit ) */
}

/*******************************************************************/
/*                       DCT_CheckandSwap                          */
/*                                                                 */
/*!    Defines an order relasionship on n-dimensional vector
       considering the first component be growing faster until
       the last one; i.e, the rightmost significant. The
       function takes a values vector IniVal of dimension
       Nelem*dim, where dim is the dimension of each vector.
       Given two indices for two elements of dimension dim,
       swap them if they are in reversed order.

       \param[in]     Inival  Array of initial values of elements
                              dim-dimension.
       \param[in,out] index1  Index of first element.
       \param[in,out] index2  Index of second element.
       \param[in]        dim  Integer indicating the array dimesion,
                              i.e number of direction in which can
                              be made the search. The values allowed
                              are: 1, 2, 3 and 4.

       \return An integer value with 0 if the element in position
               Index1 is smaller than that one in position Index2,
               1 otherwise (indices are swapped).

*/
/*******************************************************************/
int DCT_CheckandSwap( DCT_Scalar *Inival, int *index1, int *index2, int dim )
{
/* ---------------------------------------------  Local Variables  */
   int swp = 0;
   int aux;
   int ii1, jj1;
   
/* ------------------------------------  BEGIN( DCT_CheckandSwap ) */
   ii1 = dim* *index1;
   jj1 = dim* *index2;
   
   switch( dim ) {
      case 4:
         if ( *(Inival + ii1 + 3) > *(Inival + jj1 + 3) ) {
            aux = *index1;
            *index1 = *index2;
            *index2 = aux;
            swp = 1;
            break;
         } else if ( *(Inival + ii1 + 3) < *(Inival + jj1 + 3) ) {
            break;
         } 
      case 3:
         if ( *(Inival + ii1 + 2) > *(Inival + jj1 + 2) ) {
            aux = *index1;
            *index1 = *index2;
            *index2 = aux;
            swp = 1;
            break;
         } else if ( *(Inival + ii1 + 2) < *(Inival + jj1 + 2) ) {
            break;
         } 
      case 2:
         if ( *(Inival + ii1 + 1) > *(Inival + jj1 + 1) ) {
            aux = *index1;
            *index1 = *index2;
            *index2 = aux;
            swp = 1;
            break;
         } else if ( *(Inival + ii1 + 1) < *(Inival + jj1 + 1) ) {
            break;
         } 
         if ( *(Inival + ii1) > *(Inival + jj1) ) {
            aux = *index1;
            *index1 = *index2;
            *index2 = aux;
            swp = 1;
            break;
         } else if ( *(Inival + ii1) < *(Inival + jj1) ) {
            break;
         } 
      default:
       return (-1);
   }

   return swp;

/* --------------------------------------  END( DCT_CheckandSwap ) */
}

/*******************************************************************/
/*                    DCT_Extract_SubDom_Ranks                     */
/*                                                                 */
/*!      Extracts the ranks in a subdomain array.

    \param [in] ModDom  Pointer to a DCT_Model_Dom structure.
    \param [out] ranks  Pointer to pointer to an array of the
                        ranks.
    \param [in]  nproc  Number of processes no repeated
                        (Length of *ranks).

    \return Integer values with 0 if finish successfully,
            otherwise if it fails.

*/
/*******************************************************************/
int DCT_Extract_SubDom_Ranks( DCT_Model_Dom *ModDom, DCT_Rank **ranks,
                                                           int nproc )
{
/* ---------------------------------------------  Local Variables  */

   DCT_Rank         *Localranks;
   DCT_Data_SubDom  *SubDom;
   
   int               ii;

/* ----------------------------  BEGIN( DCT_Extract_SubDom_Ranks ) */
   
   SubDom       = ModDom->ModSubDom;

   Localranks = (DCT_Rank *)malloc( sizeof(DCT_Rank)*(size_t)nproc );
   DCTERROR( Localranks == (DCT_Rank *)NULL, "Memory Allocation Failed",
                                                         return(-1) );

   for ( ii=0; ii < nproc; ii++ )
      Localranks[ ii ] =  (SubDom + ii)->RankProc;

   *ranks = Localranks;

   return 0;
/* ------------------------------  END( DCT_Extract_SubDom_Ranks ) */
}


int DCT_GLB_Dim; /*!< This is a global var used in the functions
                    DCT_Comp_SubDomain and DCT_SubDom_ListOrder
                    to use it with qsort */

/*******************************************************************/
/*                       DCT_Comp_SubDomain                        */
/*                                                                 */
/*!   Compares to subdomain indices using a order relationship
      on n-dimensional vector, considering the first component
      be growing faster until the last one; i.e, the rightmost
      significant.

      \param[in] Subdom1  Pointer to first element.
      \param[in] Subdom2  Pointer to second element.

      \return Integer value with -1, 0 or 1 if Subdom1 is less,
              equal or greater that Subdom2, respectively.

*/
/*******************************************************************/
int DCT_Comp_SubDomain( const void *Subdom1, const void *Subdom2 )
{
/* ---------------------------------------------  Local Variables  */
   DCT_Data_SubDom *elem1 = (DCT_Data_SubDom*)Subdom1;
   DCT_Data_SubDom *elem2 = (DCT_Data_SubDom*)Subdom2;
/* ----------------------------------  BEGIN( DCT_Comp_SubDomain ) */
   switch( DCT_GLB_Dim ) {
      case 4:
         if ( (elem1)->IniPos[3] > elem2->IniPos[3] ) {
            return 1;
            break;
         } else if ( elem1->IniPos[3] < elem2->IniPos[3] ) {
            return -1;
            break;
         } 
      case 3:
         if ( elem1->IniPos[2] > elem2->IniPos[2] ) {
            return 1;
            break;
         } else if ( elem1->IniPos[2] < elem2->IniPos[2] ) {
            return -1;
            break;
         } 
      case 2:
         if ( elem1->IniPos[1] > (elem2)->IniPos[1] ) {
            return 1;
            break;
         } else if ( elem1->IniPos[1] < elem2->IniPos[1] ) {
            return -1;
            break;
         } 
         if ( elem1->IniPos[0] > elem2->IniPos[0] ) {
            return 1;
            break;
         } else if ( elem1->IniPos[0] < elem2->IniPos[0] ) {
            return -1;
            break;
         } 
   }

   return 0;

/* ------------------------------------  END( DCT_Comp_SubDomain ) */
}

/*******************************************************************/
/*                      DCT_SubDom_ListOrder                       */
/*                                                                 */
/*!    Order an array of a model subdomain, considering the
       the order relation where the first index o dimension
       runs faster that the rest; e.g. (1,2) < (2,1).

       \param[in,out] Subdom  Pointer to an array of DCT_Data_SubDom
                              elements.
       \param[in]      NSDom  Number of elements in the array.
       \param[in]        dim  Dimension of the domain, i.e. 2, 3
                              or 4.

       \return Integer values with 0 if finish successfully,
               otherwise if it fails.

*/
/***************************************************************/
int DCT_SubDom_ListOrder( DCT_Data_SubDom *SubDom, int NSDom, int dim )
{
/* ---------------------------------------------  Local Variables  */

/* --------------------------------  BEGIN( DCT_SubDom_ListOrder ) */

   DCT_GLB_Dim = dim; /* DCT_GLB_Dim is a global variable declare above
                         out side of the function DCT_Comp_SubDomain   */
   qsort( SubDom, (size_t)NSDom, sizeof(DCT_Data_SubDom), DCT_Comp_SubDomain );

   return 0;
/* ----------------------------------  END( DCT_SubDom_ListOrder ) */
}

/*******************************************************************/
/*                        DCT_Couple_Comp                          */
/*                                                                 */
/*!    Function to compare two DCT_Couple pointers, using the
       CoupleTag as a key field.

       \parem[in] node1  Pointer to a DCT_Couple structure.
       \parem[in] node2  Pointer to a DCT_Couple structure.
       
       \return Integer with values less than, equal to, or
       greater than 0; if node1's CoupleTag is less than, equal
       to, or greater than node2's CoupleTag.

*/
/*******************************************************************/
int DCT_Couple_Comp( const void *node1, const void *node2 )
{
/* ---------------------------------------------  Local Variables  */

   const DCT_Couple *n1 = *(DCT_Couple **)node1;
   const DCT_Couple *n2 = *(DCT_Couple **)node2;
   
/* -------------------------------------  BEGIN( DCT_Couple_Comp ) */

   return ((int)( n1->CoupleTag - n2->CoupleTag ));

/* ---------------------------------------  END( DCT_Couple_Comp ) */

}

/*******************************************************************/
/*                      DCT_Couple_ListOrder                       */
/*                                                                 */
/*!   Order an array of pointers to DCT_Couple, using the qsort
      function, provided by stdlib.c. The orther is given by the
      Coupletag.

      \param[in,out] lcpl  Pointer to an array of pointers to
                           DCT_Couple.
      \param[in]    nelem  Number of elements in the array.

      \return Integer values with 0 if finish successfully,
              otherwise if it fails.

*/
/*******************************************************************/
int DCT_Couple_ListOrder( DCT_Couple **lcpl, int nelem )
{
/* ---------------------------------------------  Local Variables  */

/* --------------------------------  BEGIN( DCT_Couple_ListOrder ) */

   qsort( lcpl, (size_t)nelem, sizeof(DCT_Couple *), DCT_Couple_Comp );

   return 0;
/* ----------------------------------  END( DCT_Couple_ListOrder ) */
}

/*******************************************************************/
/*                        DCT_Model_Comp                           */
/*                                                                 */
/*!    Function to compare two DCT_Model pointers, using the
       ModTag as a key field.

       \parem[in] node1   Pointer to a DCT_Model structure.
       \parem[in] node2   Pointer to a DCT_Model structure.

       \return Integer with values less than, equal to, or
       greater than 0; if node1's ModTag is less than, equal
       to, or greater than node2's ModTag.

*/
/*******************************************************************/
int DCT_Model_Comp( const void *node1, const void *node2 )
{
/* ---------------------------------------------  Local Variables  */

   const DCT_Model *n1 = *(DCT_Model **)node1;
   const DCT_Model *n2 = *(DCT_Model **)node2;
   
/* --------------------------------------  BEGIN( DCT_Model_Comp ) */

   return ((int)( n1->ModTag - n2->ModTag ));

/* ----------------------------------------  END( DCT_Model_Comp ) */

}

/*******************************************************************/
/*                       DCT_Model_ListOrder                       */
/*                                                                 */
/*!   Order an array of pointers to DCT_Model, using the qsort
      function, provided by stdlib.c. The orther is given by the
      ModTag.

      \param[in,out] lmod  Pointer to an array of pointers to
                           DCT_Model.
      \param[in]    nelem  Number of elements in the array.

      \return Integer values with 0 if finish successfully,
              otherwise if it fails.

*/
/*******************************************************************/
int DCT_Model_ListOrder( DCT_Model **lmod, int nelem )
{
/* ---------------------------------------------  Local Variables  */

/* ---------------------------------  BEGIN( DCT_Model_ListOrder ) */

   qsort( lmod, (size_t)nelem, sizeof(DCT_Model *), DCT_Model_Comp );

   return 0;
/* -----------------------------------  END( DCT_Model_ListOrder ) */
}

/*******************************************************************/
/*                    DCT_GC_index_bsearch                         */
/*                                                                 */
/*!   Approximately (no exact) binary search in an array.
      Return the array index of the element founded, if opt
      is eual to 0,  or the smalest bigger, if opt is equal
      to 1, or the biggest smaller, if opt is equal to -1.
      In this case, a linear array representing a multi-
      dimensional matrix is consider.

      \param[in]  array  Array of element.
      \param[in] inipos  Initial position in the array to be searched.
      \param[in] endpos  Final position in the array to be searched
      \param[in]   elem  Element to be searched in the array
      \param[in]    opt  greater than, less than or equal to 0; search
                         the exact to or immidiate greater than, the
                         exact to or immidiate smaler, or equal to elem.
                         If opt is greater than 1 or smaller than -1,
                         get rid the condition that elem must be inside
                         of the initial range:
                         array[inipos] <= elem <= array[endpos].
      \param[in]   npts  Array of the number of points in each direction.
      \param[in]    dim  Integer indicating the array dimesion, i.e
                         number of direction in which can be made the
                         search. The values allowed are: 1, 2, 3 and 4.
      \param[in]    dir  Direction where the search takes place.

     \return The index resulting for the bsearch, or -1 if it is not
             found the element or do not comply the requirements.

*/
/*******************************************************************/
int DCT_GC_index_bsearch(const DCT_Scalar *array, const int inipos, const int endpos,
                      const DCT_Scalar elem, const int opt, const int *npts,
                      const int dim, const int dir )
{
/* ---------------------------------------------  Local Variables  */
  int ini, end, new, offset, offset2;

/* --------------------------------  BEGIN( DCT_GC_index_bsearch ) */

  if (dim < dir ) return(-1); /* Error. Serching out of dimension */

  switch( dim ) {
    case 4:
       if ( dir == 1 ) offset = npts[1]*npts[2]*npts[3];
       else if ( dir == 2 ) offset = npts[2]*npts[3];
       else if ( dir == 3 ) offset = npts[3];
       else if ( dir == 4 ) offset = 1;
       else return (-1);
        break;
    case 3:
       if ( dir == 1 ) offset = npts[1]*npts[2];
       else if ( dir == 2 ) offset = npts[2];
       else if ( dir == 3 ) offset = 1;
       else return (-1);
       break;
    case 2:
       if ( dir == 1 ) offset = npts[1];
       else if ( dir == 1 ) offset = 1;
       else return (-1);
    default:
       return (-1);
  }
  offset2 = 2*offset;

  ini = inipos;
  end = endpos;

  if ( array[ini]== elem ) return(ini);
  else if ( array[end]== elem ) return(end);
  else if ( ( elem < array[ini] ) || ( array[end] < elem ) ) {/* out of range */
     /* get rid of the extreme limits (usefull when it needs values
        between to values no necessary inside the range             */
     if ( opt > 1 ) return ( (elem < array[ini] ? ini : -1) ); /* frist element */
     else if ( opt < -1) return ( (array[end] < elem ? end : -1) ); /* last element */
     else return(-1);
  }

  if ( ( elem < array[ini + offset] ) && (opt > 0) ) return(ini + offset);
  if ( ( array[end - offset] < elem ) && (opt < 0) ) return(end - offset);

  while (ini < end) {
    new = ini + ((end - ini)/offset2)*offset;
    if ( elem == array[new] ) return(new);
    else if ( array[new] < elem ) {
      if ( ( elem < array[new + offset] ) && (opt > 0) ) return(new + offset);
      ini = new;
    }
    else if ( elem < array[new] ) {
      if ( ( array[new - offset] < elem ) && (opt < 0) ) return(new - offset);
      end = new;
    }

  } /*  while (ini < end) */

  return(-1); /* No found it */

/* ----------------------------------  END( DCT_GC_index_bsearch ) */
}

/*******************************************************************/
/*                       DCT_index_bsearch                         */
/*                                                                 */
/*!  Approximately (no exact) binary search in an array. Return
     the array index of the element founded, if opt is equal to 0;
     or the smalest bigger, if opt is greater than 0; or the
     biggest smaller, if opt is smaller than 0.

     \param[in]  array: Array of elements.
     \param[in] inipos: Initial position in the array to be searched.
     \param[in] endpos: Final position in the array to be searched.
     \param[in]   elem: Element to be searched in the array.
     \param[in]    opt: Option that controls the bsearch behavior.
                If it is greater than, less than or equal to 0;
                search the exact to or immidiate greater than, the
                exact to or immidiate smaler, or equal to elem. If
                opt is greater than 1 or smaller than -1, get rid the
                condition that elem must be inside of the initial
                range, i.e. array[inipos] <= elem <= array[endpos].

     \return The index resulting for the bsearch, or -1 if it is not
             found the element or do not comply the requirements.

*/
/*******************************************************************/
int DCT_index_bsearch(const DCT_Scalar *array, const int inipos, const int endpos,
                      const DCT_Scalar elem, const int opt)
{
/* ---------------------------------------------  Local Variables  */
  int ini, end, new;

/* -----------------------------------  BEGIN( DCT_index_bsearch ) */

  ini = inipos;
  end = endpos;

  if ( array[ini]== elem ) return(ini);
  else if ( array[end]== elem ) return(end);
  else if ( ( elem < array[ini] ) || ( array[end] < elem ) ) { /* out of range */
     /* get rid of the extreme limits (usefull when it needs values
        between to values no necessary inside the range             */
     if ( opt > 1 ) return ( (elem < array[ini] ? ini : -1) ); /* frist element */
     else if ( opt < -1) return ( (array[end] < elem ? end : -1) ); /* last element */
     else return(-1);
  }

  if ( ( elem < array[ini+1] ) && (opt > 0) ) return(ini+1);
  if ( ( array[end-1] < elem ) && (opt < 0) ) return(end-1);

  while (ini < end) {
    new = (ini + end)/2;
    if ( elem == array[new] ) return(new);
    else if ( array[new] < elem ) {
      if ( ( elem <= array[new+1] ) && (opt > 0) ) return(new+1);
      ini = new;
    }
    else if ( elem < array[new] ) {
      if ( ( array[new-1] <= elem ) && (opt < 0) ) return(new-1);
      end = new;
    }

  } /*  while (ini < end) */

  return(-1); /* No found it */

/* -------------------------------------  END( DCT_index_bsearch ) */

}

/*******************************************************************/
/*                        DCT_SearchIndex                          */
/*                                                                 */
/*!   Return the index and its respective entry value of an
      array, corresponding to the exact or closest value of an
      elem above or below, depending if opt is greater or less
      than 0. The way to generate the index depends on the value
      of DomType. If DomType is DCT_RECTILINEAR, the function
      DCT_index_bsearch is called using the same parameters, but
      if it is DCT_CARTESIAN the index is generated using integer
      arithmetic rounding with floor or ceil depending the value
      of opt.

      \param[in]  array  Array of element.
      \param[in] inipos  Initial position in the array to be searched.
      \param[in] endpos  Final position in the array to be searched.
      \param[in]   elem  Element to be searched in the array.
      \param[in]    opt  Greater than, less than or equal to 0;
                         search the exact to or immidiate greater than,
                         the exact to or immediate smaler, or equal to
                         elem.
      \param[out] value  The value corresponding to the index in array.

      \return The index resulting for the bsearch, or -1 if it is not
              found the element or do not comply the requirements.

*/
/*******************************************************************/
int DCT_SearchIndex( const DCT_Scalar *array, const DCT_Integer npts,
                     const DCT_Domain_Type DomType, const int inipos,
                     const int endpos, const DCT_Scalar elem, const int opt,
                     DCT_Scalar *value )
{
/* ---------------------------------------------  Local Variables  */
  int         ind;
  
  DCT_Scalar  hh, inival, tempval, scInd;
/* -------------------------------------  BEGIN( DCT_SearchIndex ) */

   if ( DomType == DCT_RECTILINEAR ) {
      ind = DCT_index_bsearch( array, inipos, endpos, elem, opt );
      *value = array[ind];
   }
   else if ( DomType == DCT_CARTESIAN ) {
      inival = array[0];
      hh = array[2];
      scInd = (elem - inival)/hh;
      tempval = inival + hh*round(scInd);

      /** This to avoid roudoff error to identify the exact value **/
      if ( ( fabs(tempval - elem) < (0.0001*hh) ) ) {
         ind = (int)lround( scInd );
         *value = tempval;
      }
      else {
         if ( opt < 0 )
            ind = (int) floor( scInd );
         else
            ind = (int) ceil( scInd );
         if ( (ind < 0) && (ind >= npts) ) ind = -1;
         *value = inival + hh*(DCT_Scalar)ind;
      }
   }
   else return -1;
   
   return ind;

/* ---------------------------------------  END( DCT_SearchIndex ) */
}

/*******************************************************************/
/*                   DCT_Return_SubDom_Interc                      */
/*                                                                 */
/*!   Calculates the intersection of the consumer.

      \param[in]       PtrDom  Pointer to a Model Domain structure.
      \param[in]     DomBound  Array of dimension 2 times Domain
                               Dimension where is stored the initial
                               and end values of each dimension.
      \param[in]    ModEndPos  Array of length dimension where the
                               last corresponding index is stored.
      \param[in,out]   IniVal  Contains the consumer Initial values
                               of the subdomain, and returns the
                               initial values of the intersection
                               in each dimension in the producer
                               resolution.
      \param[in,out]   EndVal  Contains the consumer End values
                               of the subdomain, and returns the
                               end values of the intersection in
                               each dimension in the producer
                               resolution.
      \param[out] ModSDGlbInd  Optional array that stores the
                               global initial index of the
                               intersected subdomain.
      \param[out]   ModSDNpts  Optional array that stores the
                               number of point of the intersected
                               subdomain on the producer resolution.

      \return An integer as flag equal to dimension given in the
              doman, if the subdomain intersects in the domain,
              less than dim, otherwise.

*/
/*******************************************************************/
int DCT_Return_SubDom_Interc( DCT_Model_Dom *PtrDom, DCT_Scalar *DomBound[],
                              DCT_Integer *ModEndPos, DCT_Scalar *IniVal,
                              DCT_Scalar *EndVal, DCT_Integer *ModSDGlbInd,
                              DCT_Integer *ModSDNpts)
{
/* ---------------------------------------------  Local Variables  */
   DCT_Scalar          **MValues;
   DCT_Integer          *MNumPts;
   DCT_Domain_Type      *MDomType;
   DCT_Scalar            inival, endval;
   DCT_Integer           ind, iniind;
   
   int ii, dim, dimflag;
/* ----------------------------  BEGIN( DCT_Return_SubDom_Interc ) */
   dim      = (int)PtrDom->ModDomDim;
   MValues  =      PtrDom->ModValues;
   MNumPts  =      PtrDom->ModNumPts;
   MDomType =      PtrDom->ModDomType;

   dimflag = 0;
   for (ii = 0; ii < dim; ii++ ){
      inival = IniVal[ii];
      endval = EndVal[ii];

      /* Lower bound is the maximum of lower bounds */
      if (inival <= DomBound[0][ii]) {
         if ( endval < DomBound[0][ii] ) break; /* the subdomain is out of area */
         IniVal[ii] = DomBound[0][ii];
         iniind = 0; /* Register the initial index in model Dom */
      }
      else{
         if (inival > DomBound[1][ii]) break;   /* the subdomain is out of area */
         iniind = DCT_SearchIndex( MValues[ii], MNumPts[ii], MDomType[ii], 0,
                                   ModEndPos[ii], inival, -1, (IniVal+ii) );
      }
      /* Register the initial index in Producer model */
      if ( ModSDGlbInd != NULL )  ModSDGlbInd[ii] = iniind;

      /* Upper bound is the minimum of upper bounds */
      if (DomBound[1][ii] <= endval) {
         if ( DomBound[1][ii] < inival ) break; /* the subdomain is out of area */
         EndVal[ii] = DomBound[1][ii];
         ind = ModEndPos[ii]; /* Register the ending index in Producer model */
      }
      else {
         if (endval < DomBound[0][ii]) break;   /* the subdomain is outbound */
         ind = DCT_SearchIndex( MValues[ii], MNumPts[ii], MDomType[ii],
                                0, ModEndPos[ii], endval, 1, (EndVal+ii) );
      }
      if ( ModSDNpts != NULL )  ModSDNpts[ii] = ind - iniind + 1;

      dimflag++;
      
   } /* End for (ii = 0; ii < dim; ii++ ) */

   return ( dimflag );

/* ----------------------------  BEGIN( DCT_Return_SubDom_Interc ) */
}

/*******************************************************************/
/*                    DCT_Return_DomBoundVals                      */
/*                                                                 */
/*!   Return the initial and ending values for a given model
      domain to extract the values of the bounds and the last
      index associated to each direction.

      \param[in]      PtrDom  Pointer to a Model Domain structure.
      \param[out]   DomBound  Array of dimension 2 times Domain
                              Dimension where is stored the initial
                              and end values of each dimension.
      \param[out] ModEndPos  Array of length dimension where the
                              last corresponding index is stored.

      \return An integer error control value with 1 if success, 
              and 0 otherwise.

*/
/*******************************************************************/
int DCT_Return_DomBoundVals( DCT_Model_Dom *PtrDom, DCT_Scalar *DomBound[],
                             DCT_Integer *ModEndPos)
{
/* ---------------------------------------------  Local Variables  */
   DCT_Scalar           *Values;
   DCT_Scalar          **MValues;
   DCT_Integer          *MNumPts;
   DCT_Domain_Type      *MDomType;
   
   int ii, ind, dim;
/* -----------------------------  BEGIN( DCT_Return_DomBoundVals ) */

   dim      = (int)PtrDom->ModDomDim;
   MValues  =      PtrDom->ModValues;
   MNumPts  =      PtrDom->ModNumPts;
   MDomType =      PtrDom->ModDomType;
   for (ii = 0; ii < dim; ii++ ){
      Values = MValues[ii];
      
      DomBound[0][ii] = Values[0];
      ModEndPos[ii] = MNumPts[ii]-1;
      ind = ( MDomType[ii] == DCT_RECTILINEAR ? ModEndPos[ii] : 1 );
      DomBound[1][ii] = Values[ ind ];
   } /* End for (ii = 0; ii < dim; ii++ ) */

   return ( 0 );

/* -----------------------------  BEGIN( DCT_Return_DomBoundVals ) */
}

/*******************************************************************/
/*                   DCT_Return_SDBoundaryVals                     */
/*                                                                 */
/*!   Return the initial and ending values for a given subdomain
      ModSubDom, as a subdomain decomposition of a model domain.

      \param[in]        dim  Dimension of the variable  2D, 3D or 4D.
      \param[in]  ModSubDom  Pointer to corresponding DCT_Data_SubDom
                             subdomain structure.
      \param[in] ModDomType  Type of Domain discretization.
      \param[in]  ModEndPos  The final index in each dimension for
                             the label array.
      \param[in]  ModLabels  The domain labels (tick marks) for each
                             dimension.
      \param[out]    IniVal  The Initial values of the subdomian in
                             each dimension.
      \param[out]    EndVal  The Ending values of the subdomian in
                             each dimension.

      \return An integer error control value with 1 if success, 
              and 0 otherwise.

*/
/*******************************************************************/
int DCT_Return_SDBoundaryVals ( int dim, DCT_Data_SubDom *ModSubDom,
                                DCT_Domain_Type *ModDomType, DCT_Integer *ModEndPos,
                                DCT_Scalar **ModLabels, DCT_Scalar *IniVal,
                                DCT_Scalar *EndVal )
{
/* ---------------------------------------------  Local Variables  */
   DCT_Scalar    *Labels;
   DCT_Scalar     h, lowbound;
   DCT_Integer   *MIniPos, *MEndPos;
   DCT_Integer    inipos, endpos;
   
/* ---------------------------  BEGIN( DCT_Return_SDBoundaryVals ) */

   MIniPos = ModSubDom->IniPos;
   MEndPos = ModSubDom->EndPos;
   switch(dim) {
      case 4:
         inipos = MIniPos[3];
         endpos = MEndPos[3];
         Labels = ModLabels[3];
         if ( ModDomType[3]== DCT_RECTILINEAR ) { 
            IniVal[3] = Labels[inipos];
            EndVal[3] = Labels[endpos];
         }
         else {
            h      = Labels[2];
            lowbound  = Labels[0];
            if ( inipos == 0) IniVal[3] = lowbound;
            else IniVal[3] = lowbound + h*(DCT_Scalar)inipos;
            if ( endpos == ModEndPos[3] ) EndVal[3] = Labels[1];
            else EndVal[3] = lowbound + h*(DCT_Scalar)endpos;
         }
      case 3:
         inipos = MIniPos[2];
         endpos = MEndPos[2];
         Labels = ModLabels[2];
         if ( ModDomType[2]== DCT_RECTILINEAR ) { 
            IniVal[2] = Labels[inipos];
            EndVal[2] = Labels[endpos];
         }
         else {
            h      = Labels[2];
            lowbound  = Labels[0];
            if ( inipos == 0) IniVal[2] = lowbound;
            else IniVal[2] = lowbound + h*(DCT_Scalar)inipos;
            if ( endpos == ModEndPos[2] ) EndVal[2] = Labels[1];
            else EndVal[2] = lowbound + h*(DCT_Scalar)endpos;
         }
      case 2:
         inipos = MIniPos[1];
         endpos = MEndPos[1];
         Labels = ModLabels[1];
         if ( ModDomType[1]== DCT_RECTILINEAR ) { 
            IniVal[1] = Labels[inipos];
            EndVal[1] = Labels[endpos];
         }
         else {
            h      = Labels[2];
            lowbound  = Labels[0];
            if ( inipos == 0) IniVal[1] = lowbound;
            else IniVal[1] = lowbound + h*(DCT_Scalar)inipos;
            if ( endpos == ModEndPos[1] ) EndVal[1] = Labels[1];
            else EndVal[1] = lowbound + h*(DCT_Scalar)endpos;
         }
         /* ---------------------- DIM 1 -------------------- */
         inipos = MIniPos[0];
         endpos = MEndPos[0];
         Labels = ModLabels[0];
         if ( ModDomType[0]== DCT_RECTILINEAR ) { 
            IniVal[0] = Labels[inipos];
            EndVal[0] = Labels[endpos];
         }
         else {
            h      = Labels[2];
            lowbound  = Labels[0];
            if ( inipos == 0) IniVal[0] = lowbound;
            else IniVal[0] = lowbound + h*(DCT_Scalar)inipos;
            if ( endpos == ModEndPos[0] ) EndVal[0] = Labels[1];
            else EndVal[0] = lowbound + h*(DCT_Scalar)endpos;
         }
   }
   
   return ( 0 );

/* ---------------------------  BEGIN( DCT_Return_SDBoundaryVals ) */
}

/*******************************************************************/
/*                    DCT_IncludedIndex_Cartes                     */
/*                                                                 */
/*!   Returns the initial index, the lenth, and optionally
      the ending index, of the Labels (tick marks) between
      SDIniVal and SDEndVal, when the labels are defined as
      DCT_CARTESIAN.

      \param[in]   Labels: Initial and ending tick marks of a
                           DCT variable.
      \param[in]   inipos: Initial position to be consider in
                           the Labels.
      \param[in]     npts: Number of points considered after the
                           inipos.
      \param[in] SDIniVal: Initial value to be considered.
      \param[in] SDEndVal: End value to be considered.
      \param[out]  IniInd: Corresponding index to SDIniVal.
      \param[out]  Length: Number of elements between SDIniVal
                           and SDEndVal.
      \param[out]  EndInd: Corresponding index to EndVal (optional).
      \param[in]      Inc: It is a parameter that indicates if it is
                           necessary to increment or to extend on one
                           more the upper and the lower bounds. If
                           Inc = 0, no bound is extended; Inc = 1,
                           the lower bound is extended; and Inc = 2,
                           the upper bound is extended.

      \return An integer error control value with 1 if success, 
              and 0 otherwise.

*/
/*******************************************************************/
int DCT_IncludedIndex_Cartes( DCT_Scalar *Labels, int inipos, int npts,
                             DCT_Scalar SDIniVal, DCT_Scalar SDEndVal,
                             DCT_Integer *IniInd, DCT_Integer *Length,
                             DCT_Integer *EndInd, DCT_Integer  Inc )
{
/* ---------------------------------------------  Local Variables  */
   DCT_Scalar           inival, endval;
   DCT_Scalar           tempval, scInd;
   DCT_Scalar           hh, factor;
   
   DCT_Integer          IniL, EndL;
   DCT_Integer          endpos, len;
   
/* ----------------------------  BEGIN( DCT_IncludedIndex_Cartes ) */

   endpos = inipos + npts-1;
   
   inival = Labels[0];
   endval = Labels[1];
   hh     = Labels[2];
   
   factor = 0.0001*hh;
   
   if ( inival < SDIniVal ) {
      /* if ( endval < SDIniVal ) This is a trick to avoid roundoff errors 
         if SDInival is greater than endval with a distance 
         greater than the 0.01% of the hh to consider it out of range */
      if ( (SDIniVal - endval) > factor )  return(0); /* out of range */
   
      scInd = (SDIniVal - inival)/hh;
      tempval = inival + hh*round(scInd);
      
      /** This to avoid roudoff error to identify the exact value **/
      if ( ( fabs(tempval-SDIniVal) < factor ) ) 
         IniL = (DCT_Integer)lround( scInd );
      else {
         /* It's calculated the immediate upper index  */
         /** When (Inc & 1) == 1 The lower bound should be extended **/
         IniL = (DCT_Integer) ( (Inc & 1) == 1? floor( scInd ): ceil( scInd ) );
      }
   }
   else{
      /* if (inival > SDEndVal) This is a trick to avoid roundoff errors 
         if inival is greater than SDEndVal with a distance 
         greater than the 0.01% of the hh to consider it out of range */
      if ((inival - SDEndVal) > factor) return(0);   /* out of range */
      IniL = inipos;
   }

   if (SDEndVal < endval) {
      /* if ( SDEndVal < inival ) This is a trick to avoid roundoff errors 
         if inival is greater than SDEndVal with a distance 
         greater than the 0.01% of the hh to consider it out of range */
      if ( (inival - SDEndVal) > factor ) return(0); /* out of range */

      scInd = (SDEndVal - inival)/hh;
      tempval = inival + hh*round(scInd);
      
      /** This to avoid roudoff error to identify the exact value **/
      if ( ( fabs(tempval-SDEndVal) < factor ) ) 
         EndL = (DCT_Integer)lround( scInd );
      else {
         /* It's calculated the immediate lower index  */
         /** When (Inc & 2) == 2 The upper bound should be extended **/
         EndL = (DCT_Integer) ( (Inc & 2) == 2? ceil( scInd ): floor( scInd ) );
      }
   }
   else {
      /* if (endval < SDIniVal) This is a trick to avoid roundoff errors 
         if SDIniVal is greater than endval with a distance 
         greater than the 0.01% of the hh to consider it out of range */
      if ((SDIniVal - endval) > factor) return(0);   /* the subdomain is outbound */
      EndL = endpos;
   }

   *IniInd = IniL;
   len = EndL - IniL + 1;
   *Length = len;
   if ( EndInd != NULL ) *EndInd = EndL;
   if ( len <= 0) return(0); /* Length empty */
   
   return ( 1 );

/* ----------------------------  BEGIN( DCT_IncludedIndex_Cartes ) */
}

/*******************************************************************/
/*                    DCT_IncludedIndex_Rectil                     */
/*                                                                 */
/*!   Returns the initial index, the length, and optionally the
      ending index, of the Labels between SDIniVal and SDEndVal,
      when the labels are defined as DCT_RECTILINEAR.

      \param[in]   Labels: Initial and ending tick marks of a
                           DCT variable.
      \param[in]   inipos: Initial position to be consider in
                           the Labels.
      \param[in]     npts: Number of points considered after the
                           inipos.
      \param[in] SDIniVal: Initial value to be considered
      \param[in] SDEndVal: End value to be considered
      \param[out]  IniInd: Corresponding index to SDIniVal
      \param[out]  Length: Number of elements between SDIniVal
                           and SDEndVal
      \param[out]  EndInd: Corresponding index to EndVal (optional)
      \param[in]      Inc: It is a parameter that indicates if it is
                           necessary to increment or to extend on one
                           more the upper and the lower bounds. If
                           Inc = 0, no bound is extended; Inc = 1,
                           the lower bound is extended; and Inc = 2,
                           the upper bound is extended.

      \return An integer error control value with 1 if success, 
              and 0 otherwise.

*/
/*******************************************************************/
int DCT_IncludedIndex_Rectil( DCT_Scalar *Labels, int inipos, int npts, 
                               DCT_Scalar SDIniVal, DCT_Scalar SDEndVal,
                               DCT_Integer *IniInd,  DCT_Integer *Length,
                               DCT_Integer *EndInd, DCT_Integer Inc )
{
/* ---------------------------------------------  Local Variables  */
   DCT_Scalar           inival, endval;
   
   DCT_Integer          IniL, EndL;
   DCT_Integer          endpos, len, opt;
   
/* ----------------------------  BEGIN( DCT_IncludedIndex_Rectil ) */

   endpos = inipos + npts-1;
   
   inival = Labels[inipos];
   endval = Labels[endpos];

   /* Lower bound is the maximum of lower bounds */
   if ( inival < SDIniVal ) {
      if ( endval < SDIniVal ) return(0); /* out of range */
      /* It's calculated the immediate lower index  */
      /** When (Inc & 1) == 1 The lower bound should be extended **/
      opt = ( (Inc & 1) == 1? -1: 1 );
      IniL = DCT_index_bsearch(Labels, inipos, endpos, SDIniVal, opt );
      *IniInd = IniL;
   }
   else{
      if (inival > SDEndVal) return(0);   /* out of range */
      IniL = inipos;
      *IniInd = IniL;
   }

   /* Upper bound is the minimum of upper bounds */
   if (SDEndVal < endval) {
      if ( SDEndVal < inival ) return(0); /* out of range */
      /* It's calculated the immediate upper index  */
      /** When (Inc & 2) == 2 The upper bound should be extended **/
      opt = ( (Inc & 2) == 2? 1: -1 );
      EndL = DCT_index_bsearch(Labels, inipos, endpos, SDEndVal,  opt );
   }
   else {
      if (endval < SDIniVal) return(0);   /* the subdomain is outbound */
      EndL = endpos;
   }

   len = EndL - IniL + 1;
   *Length = len;
   if ( EndInd != NULL ) *EndInd = EndL;
   if ( len <= 0) return(0); /* Length empty */
   
   return ( 1 );

/* ----------------------------  BEGIN( DCT_IncludedIndex_Rectil ) */
}

/*******************************************************************/
/*                    DCT_IncludedValues_Cartes                    */
/*                                                                 */
/*!  Returns the values in Labels between SDIniVal and SDEndVal,
     when the labels are defined as DCT_CARTESIAN.

    \param[in]   Labels  Initial and Ending Tick marks of a DCT
                         variable.
    \param[in]   inipos  Initial position to be consider in the
                         Labels.
    \param[in]     npts  Number of points considered after the
                         inipos.
    \param[in] SDIniVal  Initial value to be considered.
    \param[in] SDEndVal  End value to be considered.
    \param[out]  IniVal  The upper closest value to SDIniVal.
    \param[out]  EndVal  The lower closest value to SDEndVal.
    \param[out]  IniInd  Corresponding index to IniVal.
    \param[out]  Length  Number of elements between IniVal and
                         EndVal.
    \param[out] MsgSlab  Array of pointers to the initial elements
                         in Labels array.
    \param[in]      Inc  It is a parameter that indicates if it is
                         necessary to increment or to extend on one
                         more the upper and the lower bounds. If
                         Inc = 0, no bound is extended; Inc = 1,
                         the lower bound is extended; and Inc = 2,
                         the upper bound is extended.

    \return An integer error control value with 1 if success, 
            and 0 otherwise.

*/
/*******************************************************************/
int DCT_IncludedValues_Cartes( DCT_Scalar *Labels, int inipos, int npts,
                              DCT_Scalar SDIniVal, DCT_Scalar SDEndVal, DCT_Scalar *IniVal,
                              DCT_Scalar *EndVal, DCT_Integer *IniInd, DCT_Integer *Length,
                              DCT_Scalar **MsgSlab, DCT_Integer Inc )
{
/* ---------------------------------------------  Local Variables  */
   DCT_Scalar           inival, endval;
   DCT_Scalar           locinival, locendval;

   DCT_Scalar          *MSlab;
   DCT_Scalar           hh;
   
   DCT_Integer          IniL, EndL;
   DCT_Integer          endpos, len;
   
   int    jj, ii;

/* ---------------------------  BEGIN( DCT_IncludedValues_Cartes ) */

   ii = DCT_IncludedIndex_Cartes( Labels, inipos, npts, SDIniVal, SDEndVal, &IniL,
                                                                     &len, &EndL, Inc );
   
   if ( ii == 1 ) {
      endpos = inipos + npts-1;
      inival = Labels[0];
      endval = Labels[1];
      hh     = Labels[2];
      // EndL = len + IniL - 1;

      if ( IniL == 0 ) locinival = inival;
      else locinival = inival + hh*(DCT_Scalar)IniL;
      if ( EndL == endpos ) locendval = endval;
      else locendval = inival + hh*(DCT_Scalar)EndL;
      
      MSlab = (DCT_Scalar *) malloc( sizeof(DCT_Scalar)*(size_t)len );
      DCTERROR( MSlab == (DCT_Scalar *)NULL, "Memory Allocation Failed", return(-1) );

      *IniInd = IniL;
      *Length = len;
      *IniVal = locinival;
      *EndVal = locendval;
      *MsgSlab = MSlab;
   
      MSlab[0] = locinival;
      len--;
      for (jj = 1; jj < len; jj++ )
         MSlab[jj] = inival + hh*(DCT_Scalar)(IniL + jj);
      MSlab[len] = locendval;
   }

   return ( ii );

/* ---------------------------  BEGIN( DCT_IncludedValues_Cartes ) */
}

/*******************************************************************/
/*                     DCT_IncludedValues_Rectil                   */
/*                                                                 */
/*!  Returns the values in Labels between SDIniVal and SDEndVal,
     when the labels are defined as DCT_RECTILINEAR.

    \param[in]   Labels  Initial and Ending Tick marks of a DCT
                         variable.
    \param[in]   inipos  Initial position to be consider in the
                         Labels.
    \param[in]     npts  Number of points considered after the
                         inipos.
    \param[in] SDIniVal  Initial value to be considered.
    \param[in] SDEndVal  End value to be considered.
    \param[out]  IniVal  The upper closest value to SDIniVal.
    \param[out]  EndVal  The lower closest value to SDEndVal.
    \param[out]  IniInd  Corresponding index to IniVal.
    \param[out]  Length  Number of elements between IniVal and
                         EndVal.
    \param[out] MsgSlab  Array of pointers to the initial elements
                         in Labels array.
    \param[in]      Inc  It is a parameter that indicates if it is
                         necessary to increment or to extend on one
                         more the upper and the lower bounds. If
                         Inc = 0, no bound is extended; Inc = 1,
                         the lower bound is extended; and Inc = 2,
                         the upper bound is extended.

    \return An integer error control value with 1 if success, 
            and 0 otherwise.

*/
/*******************************************************************/
int DCT_IncludedValues_Rectil( DCT_Scalar *Labels, int inipos, int npts, DCT_Scalar SDIniVal,
                                DCT_Scalar SDEndVal, DCT_Scalar *IniVal, DCT_Scalar *EndVal,
                                DCT_Integer *IniInd, DCT_Integer *Length, DCT_Scalar **MsgSlab,
                                DCT_Integer Inc )
{
/* ---------------------------------------------  Local Variables  */
   DCT_Integer          IniL, EndL;
   
   int                  jj;
   
/* ---------------------------  BEGIN( DCT_IncludedValues_Rectil ) */

   jj = DCT_IncludedIndex_Rectil( Labels, inipos, npts, SDIniVal, SDEndVal, IniInd,
                                                                      Length, &EndL, Inc );
   
   if ( jj == 1 ) {
      IniL = *IniInd;
      *IniVal = Labels[ IniL ];
      // EndL = *Length + IniL - 1;
      *EndVal = Labels[ EndL ];
      *MsgSlab = Labels + IniL;
   }
   
   return ( jj );

/* ---------------------------  BEGIN( DCT_IncludedValues_Rectil ) */
}

/*******************************************************************/
/*                         DCT_IncludedValues                      */
/*                                                                 */
/*!     Returns the values in Labels between SDIniVal and
        SDEndVal, in all dimensions. Return a integer value
        equal to dim, if values in all directions are between
        the SDIniVal and SDEndVal.

    \param[in]      dim  Dimension of the variable: 2D, 3D or
                         4D.
    \param[out]  totlen  Return the total length of the all tick
                         marks.
    \param[in]  DomType  Domain type for each domain direction.
    \param[in]   Labels  Initial and Ending Tick marks of a DCT variable.
    \param[in]   inipos  Initial position to be consider in the Labels.
    \param[in]     npts  Number of points considered after the inipos.
    \param[in] SDIniVal  Initial value to be considered.
    \param[in] SDEndVal  End value to be considered.
    \param[out]  IniVal  The upper closest value to SDIniVal.
    \param[out]  EndVal  The lower closest value to SDEndVal.
    \param[out]  IniInd  Corresponding index to IniVal.
    \param[out]  Length  Number of elements between IniVal and EndVal.
    \param[out] MsgSlab  Tick Marks array corresponding the values between
                         IniVal and EndVal, when the DomType is
                            DCT_RECTILINEAR; or an array of pointers to
                            initial elements in Labels array when DomType is
                              DCT_CARTESIAN.
    \param[in]      Inc  It is a parameter that indicates if it is
                         necessary to increment or to extend on one
                         more the upper and the lower bounds. If
                         Inc = 0, no bound is extended; Inc = 1,
                         the lower bound is extended; and Inc = 2,
                         the upper bound is extended.

   \return  An int value that indicates how many dimensions have
            intersection. This indicates if a subdomain intersects
            another.

*/
/*******************************************************************/
int DCT_IncludedValues( int dim, int *totlen, DCT_Domain_Type *DomType, DCT_Scalar **Labels,
                        int *Dim, DCT_Scalar *SDIniVal, DCT_Scalar *SDEndVal,
                        DCT_Scalar *IniVal, DCT_Scalar *EndVal, DCT_Integer *IniInd,
                        DCT_Integer *Length, DCT_Scalar **MsgSlab, int *Inc )
{
/* ---------------------------------------------  Local Variables  */
   
   int    dimflag, jj;
   int    LocInc[MAX_DOMAIN_DIM] = { 0, 0, 0, 0 };

/* ----------------------------------  BEGIN( DCT_IncludedValues ) */

   dimflag =0;
   *totlen = 0;
   if ( Inc == NULL ) Inc = LocInc;
   switch(dim) {
      case 4:
         if ( DomType[3] == DCT_CARTESIAN ) {
            jj = DCT_IncludedValues_Cartes( Labels[3], 0, Dim[3], SDIniVal[3],
                                           SDEndVal[3], (IniVal + 3), (EndVal + 3),
                                           (IniInd + 3), (Length + 3), (MsgSlab + 3), Inc[3] );
            DCTERROR( jj == -1, "Error calculating the values to be produced", return(1) );
            dimflag += jj;
         } else {  /*  DomType[3] == DCT_RECTILINEAR   */
            jj = DCT_IncludedValues_Rectil( Labels[3], 0, Dim[3], SDIniVal[3], SDEndVal[3],
                                             (IniVal + 3), (EndVal + 3), (IniInd + 3),
                                             (Length + 3), (MsgSlab + 3), Inc[3] );
            DCTERROR( jj == -1, "Error calculating the values to be produced", return(1) );
            dimflag += jj;
         }
         *totlen += Length[3];
         if (!jj) break;
      case 3:
         if ( DomType[2] == DCT_CARTESIAN ) {
            jj = DCT_IncludedValues_Cartes( Labels[2], 0, Dim[2], SDIniVal[2], SDEndVal[2],
                                           (IniVal + 2), (EndVal + 2), (IniInd + 2),
                                           (Length + 2), (MsgSlab + 2), Inc[2] );
            DCTERROR( jj == -1, "Error calculating the values to be produced", return(1) );
            dimflag += jj;
         } else {  /*  DomType[2] == DCT_RECTILINEAR   */
            jj = DCT_IncludedValues_Rectil( Labels[2], 0, Dim[2], SDIniVal[2], SDEndVal[2],
                                             (IniVal + 2), (EndVal + 2), (IniInd + 2),
                                             (Length + 2), (MsgSlab + 2), Inc[2] );
            DCTERROR( jj == -1, "Error calculating the values to be produced", return(1) );
            dimflag += jj;
         }
         *totlen += Length[2];
         if (!jj) break;
      case 2:
         if ( DomType[1] == DCT_CARTESIAN ) {
            jj = DCT_IncludedValues_Cartes( Labels[1], 0, Dim[1], SDIniVal[1], SDEndVal[1],
                                           (IniVal + 1), (EndVal + 1), (IniInd + 1),
                                           (Length + 1), (MsgSlab + 1), Inc[1] );
            DCTERROR( jj == -1, "Error calculating the values to be produced", return(1) );
            dimflag += jj;
         } else {  /*  DomType[1] == DCT_RECTILINEAR   */
            jj = DCT_IncludedValues_Rectil( Labels[1], 0, Dim[1], SDIniVal[1], SDEndVal[1],
                                             (IniVal + 1), (EndVal + 1), (IniInd + 1),
                                             (Length + 1), (MsgSlab + 1), Inc[1] );
            DCTERROR( jj == -1, "Error calculating the values to be produced", return(1) );
            dimflag += jj;
         }
         *totlen += Length[1];
         if (!jj) break;
      /********************** Dim1 **************************/
         if ( DomType[0] == DCT_CARTESIAN ) {
            jj = DCT_IncludedValues_Cartes( Labels[0], 0, Dim[0], SDIniVal[0], SDEndVal[0],
                                           (IniVal), (EndVal), (IniInd),
                                           (Length), (MsgSlab), Inc[0] );
            DCTERROR( jj == -1, "Error calculating the values to be produced", return(1) );
            dimflag += jj;
         } else {  /*  DomType[0] == DCT_RECTILINEAR   */
            jj = DCT_IncludedValues_Rectil( Labels[0], 0, Dim[0], SDIniVal[0], SDEndVal[0],
                                             (IniVal), (EndVal), (IniInd),
                                             (Length), (MsgSlab), Inc[0] );
            DCTERROR( jj == -1, "Error calculating the values to be produced", return(1) );
            dimflag += jj;
         }
         *totlen += Length[0];
   }  /* End of switch(dim) */

   return ( dimflag );
/* ----------------------------------  BEGIN( DCT_IncludedValues ) */
}

/*******************************************************************/
/*                      DCT_IncludedValues_GC                      */
/*                                                                 */
/*!     Returns the values in Labels between SDIniVal and
        SDEndVal, in all dimensions when the discretization
        used is   DCT_GENERAL_CURV. Return an integer value
        equal to dim, if values in all directions are between
        the SDIniVal and SDEndVal.

    \param[in]      dim  Dimension of the variable: 2D, 3D or
                         4D.
    \param[out]  totlen  Return the total length of the all tick
                         marks.
    \param[in]  DomType  Domain type for each domain direction.
    \param[in]   Labels  Initial and Ending Tick marks of a DCT variable.
    \param[in]   inipos  Initial position to be consider in the Labels.
    \param[in]     npts  Number of points considered after the inipos.
    \param[in] SDIniVal  Initial value to be considered.
    \param[in] SDEndVal  End value to be considered.
    \param[out]  IniVal  The upper closest value to SDIniVal.
    \param[out]  EndVal  The lower closest value to SDEndVal.
    \param[out]  IniInd  Corresponding index to IniVal.
    \param[out]  Length  Number of elements between IniVal and EndVal.
    \param[out] MsgSlab  Tick Marks array corresponding the values between
                         IniVal and EndVal.

    (this is not implemented here, we don't know yet if we will use it)
                    Inc  It is a parameter that indicates if it is
                         necessary to increment or to extend on one
                         more the upper and the lower bounds. If
                         Inc = 0, no bound is extended; Inc = 1,
                         the lower bound is extended; and Inc = 2,
                         the upper bound is extended.

   \return  An int value that indicates how many dimensions have
            intersection. This indicates if a subdomain intersects
            another.

*/
/*******************************************************************/
int DCT_IncludedValues_GC( int dim, int *totlen, DCT_Scalar **Labels, int *Dim,
                        DCT_Scalar *SDIniVal, DCT_Scalar *SDEndVal, DCT_Scalar *IniVal,
                        DCT_Scalar *EndVal, DCT_Integer *IniInd, DCT_Integer *Length,
                        DCT_Scalar **MsgSlab )
{
/* ---------------------------------------------  Local Variables  */
   
   int    dimflag, jj, ii;
   int    inipos, endpos, IniI, EndI, IniJ, EndJ, IniL, EndL, IniK, EndK;
   
   DCT_Integer  aux;

/* -------------------------------  BEGIN( DCT_IncludedValues_GC ) */

   dimflag =0;
   switch(dim) {
      case 2:
         inipos = 0;
         endpos = (Dim[0]-1)*Dim[1];
         IniI = DCT_GC_index_bsearch( Labels[0], inipos, endpos, SDIniVal[0],
                                      2, Dim, 2, 1 );
         
         EndI = DCT_GC_index_bsearch( Labels[0], inipos, endpos, SDEndVal[0],
                                     -2, Dim, 2, 1 );
         if ( ( IniI == -1) || ( EndI == -1 ) ) break; /* Out of range */
         dimflag++;
         inipos = IniI;
         endpos = IniI + Dim[1] - 1;
         IniJ = DCT_GC_index_bsearch( Labels[1], inipos, endpos, SDIniVal[1],
                                      2, Dim, 2, 2 );
         
         EndJ = DCT_GC_index_bsearch( Labels[1], inipos, endpos, SDEndVal[1],
                                     -2, Dim, 2, 2 );
         if ( ( IniJ == -1) || ( EndJ == -1 ) ) break; /* Out of range */
         dimflag++;
         
         /* Store the i index from IniJ= j + i*Dim[1] */
         jj = dim*IniI;
         IniInd[jj] = IniJ / Dim[1];
         IniVal[jj] = Labels[0][IniJ];
         Length[jj] = (DCT_Integer) (EndI / Dim[1]) - (DCT_Integer) IniInd[jj] + 1;
         if ( Length[jj] <= 0 ) {
            dimflag = 0;
            break;
         }

         /* Store the j index from IniJ= j + i*Dim[1] */
         jj++;
         IniInd[jj] = IniJ % Dim[1];
         IniVal[jj] = Labels[1][IniJ];
         Length[jj] = (DCT_Integer) (EndJ % Dim[1]) - (DCT_Integer) IniInd[jj] + 1;
         if ( Length[jj] <= 0 ) {
            dimflag = 0;
            break;
         }

         break;
      case 3:
         inipos = 0;
         endpos = (Dim[0]-1)*Dim[1]*Dim[2];
         IniI = DCT_GC_index_bsearch( Labels[0], inipos, endpos, SDIniVal[0],
                                      2, Dim, 3, 1 );
         
         EndI = DCT_GC_index_bsearch( Labels[0], inipos, endpos, SDEndVal[0],
                                     -2, Dim, 3, 1 );
         if ( ( IniI == -1) || ( EndI == -1 ) ) break; /* Out of range */
         dimflag++;

         inipos = IniI;
         endpos = IniI + (Dim[1]-1)*Dim[2];
         IniJ = DCT_GC_index_bsearch( Labels[1], inipos, endpos, SDIniVal[1],
                                      2, Dim, 3, 2 );
         
         EndJ = DCT_GC_index_bsearch( Labels[1], inipos, endpos, SDEndVal[1],
                                     -2, Dim, 3, 2 );
         if ( ( IniJ == -1) || ( EndJ == -1 ) ) break; /* Out of range */
         dimflag++;
         
         inipos = IniJ;
         endpos = IniJ + Dim[2]-1;
         IniK = DCT_GC_index_bsearch( Labels[2], inipos, endpos, SDIniVal[2],
                                      2, Dim, 3, 3 );
         
         EndK = DCT_GC_index_bsearch( Labels[2], inipos, endpos, SDEndVal[2],
                                     -2, Dim, 3, 3 );
         if ( ( IniK == -1) || ( EndK == -1 ) ) break; /* Out of range */
         dimflag++;
         
         /* Store the k index from IniK= k + Dim[2]*(j + i*Dim[1]) */
         jj = dim*IniI + 2;
         aux  = IniK / Dim[2];
         IniInd[jj] = IniK % Dim[2];
         IniVal[jj] = Labels[2][IniK];
         Length[jj] = (DCT_Integer) (EndK % Dim[2]) - (DCT_Integer) IniInd[jj] + 1;
         if ( Length[jj] <= 0 ) {
            dimflag = 0;
            break;
         }

         /* Store the j index from IniK= k + Dim[2]*(j + i*Dim[1]) */
         jj--;
         IniInd[jj] = aux % Dim[1];
         IniVal[jj] = Labels[1][IniK];
         Length[jj] = (DCT_Integer) ((EndJ / Dim[2]) % Dim[1]) -
                      (DCT_Integer) IniInd[jj] + 1;
         if ( Length[jj] <= 0 ) {
            dimflag = 0;
            break;
         }

         /* Store the i index from IniK= k + Dim[2]*(j + i*Dim[1]) */
         jj--;
         IniInd[jj] = aux / Dim[1];
         IniVal[jj] = Labels[0][IniK];
         Length[jj] = (DCT_Integer) ((EndI / Dim[0]) / Dim[1]) -
                      (DCT_Integer) IniInd[jj] + 1;
         if ( Length[jj] <= 0 ) {
            dimflag = 0;
            break;
         }


         break;
      case 4:
         inipos = 0;
         endpos = (Dim[0]-1)*Dim[1]*Dim[2]*Dim[3];
         IniI = DCT_GC_index_bsearch( Labels[0], inipos, endpos, SDIniVal[0],
                                      2, Dim, 4, 1 );
         
         EndI = DCT_GC_index_bsearch( Labels[0], inipos, endpos, SDEndVal[0],
                                     -2, Dim, 4, 1 );
         if ( ( IniI == -1) || ( EndI == -1 ) ) break; /* Out of range */
         dimflag++;

         inipos = IniI;
         endpos = IniI + (Dim[1]-1)*Dim[2]*Dim[3];
         IniJ = DCT_GC_index_bsearch( Labels[1], inipos, endpos, SDIniVal[1],
                                      2, Dim, 4, 2 );
         
         EndJ = DCT_GC_index_bsearch( Labels[1], inipos, endpos, SDEndVal[1],
                                     -2, Dim, 4, 2 );
         if ( ( IniJ == -1) || ( EndJ == -1 ) ) break; /* Out of range */
         dimflag++;
         
         inipos = IniJ;
         endpos = IniJ + (Dim[2]-1)*Dim[3];
         IniK = DCT_GC_index_bsearch( Labels[2], inipos, endpos, SDIniVal[2],
                                      2, Dim, 4, 3 );
         
         EndK = DCT_GC_index_bsearch( Labels[2], inipos, endpos, SDEndVal[2],
                                     -2, Dim, 4, 3 );
         if ( ( IniK == -1) || ( EndK == -1 ) ) break; /* Out of range */
         dimflag++;
         
         inipos = IniK;
         endpos = IniK + Dim[3] - 1;
         IniL = DCT_GC_index_bsearch( Labels[3], inipos, endpos, SDIniVal[3],
                                      2, Dim, 4, 4 );
         
         EndL = DCT_GC_index_bsearch( Labels[3], inipos, endpos, SDEndVal[3],
                                     -2, Dim, 4, 4 );
         if ( ( IniL == -1) || ( EndL == -1 ) ) break; /* Out of range */
         dimflag++;
         

         /* Store the l index from IniL= l + Dim[3]*( k + Dim[2]*(j + i*Dim[1]) ) */
         jj = dim*IniI + 3;
         IniInd[jj] = IniL % Dim[3];
         IniVal[jj] = Labels[3][IniL];
         Length[jj] = (DCT_Integer) (EndL % Dim[3]) - (DCT_Integer) IniInd[jj] + 1;
         if ( Length[jj] <= 0 ) {
            dimflag = 0;
            break;
         }


         /* Store the k index from IniL= l + Dim[3]*( k + Dim[2]*(j + i*Dim[1]) ) */
         jj--;
         aux = IniL / Dim[3];
         IniInd[jj] = aux % Dim[2];
         IniVal[jj] = Labels[2][IniL];
         Length[jj] = (DCT_Integer) ((EndK / Dim[3]) % Dim[2]) -
                      (DCT_Integer) IniInd[jj] + 1;
         if ( Length[jj] <= 0 ) {
            dimflag = 0;
            break;
         }

         /* Store the j index from IniL= l + Dim[3]*( k + Dim[2]*(j + i*Dim[1]) ) */
         jj--;
         aux = aux / Dim[2];
         IniInd[jj] = aux % Dim[1];
         IniVal[jj] = Labels[1][IniL];
         Length[jj] = (DCT_Integer) (((EndJ / Dim[3]) / Dim[2]) % Dim[1]) - 
                      (DCT_Integer) IniInd[jj] + 1;
         if ( Length[jj] <= 0 ) {
            dimflag = 0;
            break;
         }

         /* Store the i index from IniL= l + Dim[3]*( k + Dim[2]*(j + i*Dim[1]) ) */
         jj--;
         IniInd[jj] = aux / Dim[1];
         IniVal[jj] = Labels[0][IniL];
         Length[jj] = (DCT_Integer) (((EndI / Dim[3]) / Dim[2]) / Dim[1]) -
                      (DCT_Integer) IniInd[jj] + 1;
         if ( Length[jj] <= 0 ) {
            dimflag = 0;
            break;
         }

         break;
   }  /* End of switch(dim) */

   return ( dimflag );
/* -------------------------------  BEGIN( DCT_IncludedValues_GC ) */
}

/*******************************************************************/
/*                      DCT_CheckIfNeedMore                        */
/*                                                                 */
/*!  Determine if the values provided by producers are enough
     to perform the interpolation operation; i.e. consumer values
     are inside of provided producers values.

    \param[in]       dim  Dimension of the variable  2D, 3D or 4D.
    \param[in]   DomType  Domain type for each domain direction.
    \param[in]    Labels  Consumer (Local) DCT_Vars label tick marks.
    \param[in]     endpos  Ending position for the Labels arrays.
    \param[in]  CommSched  Consumer DCT_Consume_Plan structure for
                           the DCT_Var.
    \param[in]       CRcv  The DCT_SDConsume structure corresponding
                           for the DCT_Couple structure of the
                           considering DCT_Var.
    \param[out]       Inc  Indicates in each direction if the needed
                           increment is required in the up or down
                           boundaries. 1, means expand the down
                           boundary, 2, up boundary, 3 both.
    \param[out]    Needit  Values requested to producer.
    \param[in,out] GlbIni  Global initial indices of the overlapped
                           domain on the producer resolution.
    \param[in,out] GlbEnd  Global ending indices of the overlapped
                           domain on the producer resolution.

   \return  An int with the value equal to dim, when no more data
            is needed or no more data is available.

   \todo  Check in this function if the parameter Dim is required,
          because it seems is not been in use.

*/
/*******************************************************************/
int DCT_CheckIfNeedMore( int dim, DCT_Domain_Type *DomType, DCT_Scalar **Labels, /* int *Dim,*/
                         DCT_Scalar **VarLabels, int *endpos, DCT_Consume_Plan *CommSched,
                         DCT_SDConsume_Plan  *CRcv, int *Inc, DCT_Scalar *Needit,
                         int *GlbIni, int *GlbEnd )
//     \param[in]        Dim  Number of points of Labels array.
{
/* ---------------------------------------------  Local Variables  */
   
   int    dimflag, flag2, ii;
   int    IniI, EndI, pos;
   
   DCT_Scalar  *Vlab, *Lab;
   
   DCT_Scalar   inival, endval, hl;
   
/* ---------------------------------  BEGIN( DCT_CheckIfNeedMore ) */
   dimflag = 0;
   for ( ii=0; ii < dim; ii++ ) Inc[ii] = 0;
   
   for ( ii=0; ii < dim; ii++ ) {
      flag2 = 0;
      pos = endpos[ii];
      IniI = CommSched->VarIniPos[ii]; /* Where the overlapping starts */
      EndI = IniI + CommSched->VarNpts[ii] - 1; /* Where it ends */
      Vlab = VarLabels[ii];
      Lab = Labels[ii];
      if ( DomType[ii] == DCT_CARTESIAN ) {
         /* The data is enough to interpolate */
         inival = Lab[0];
         endval = Lab[1];
         hl     = Lab[2];
         if ( IniI != 0) inival = inival + hl*(DCT_Scalar)IniI;
         if ( EndI != pos ) endval = inival + hl*(DCT_Scalar)EndI;
         /** Checking if all values are inside  **/
         /*** Domain Lower Bound ***/
         if ( ( Vlab[0] <= inival ) && ( endval <= Vlab[pos] ) ) {
            /* Check if the amount of data is exceeded */
            dimflag++;
         }
         else { /** The data is not enough o more data or reduce the subdomain **/
            if ( inival < Vlab[0] ) {
               if ( CRcv->GlbInd[ii] == 0 ) { /** The receiver subdomain should be reduced **/
                  /* Get the smallest upper index */
                  flag2++;
               }
               else {  /* From where the information should be asked */
                  Inc[ii] += 1; // The direction of increment is registered
               } /* End of else case when more info is needed
                    if ( CRcv->GlbInd[ii] == 0 ) */
            }
            else flag2++;
            /*** Domain Upper Bound ***/
            if ( Vlab[pos] < endval ) {
               /** The receiver subdomain should be reduced **/
               if ( (CRcv->GlbInd[ii]+CRcv->Npts[ii]) == CRcv->SndrNpts[ii] ) {
                  /* Get the largest lower index */
                  flag2++;
               }
               else {  /* From where the information should be asked */
                  Inc[ii] += 2; // The direction of increment is registered
               } /* End of else case when more info is needed
                    if ( (CRcv->GlbInd[ii]+CRcv->Npts[ii]) == CRcv->SndrNpts[ii] ) */
            }
            else flag2++;
            if ( flag2 == 2 ) dimflag++;
         } /* End of if ( Vlab[0] <= inival ) && ( endval <= Vlab[pos] ) */
   
      }
      else { /*  DomType[ii] == DCT_RECTILINEAR   */
          /* The data is enough to interpolate */
         if ( ( Vlab[0] <= Lab[IniI] ) && ( Lab[EndI] <= Vlab[pos] ) ) {
            dimflag++;
         }
         else { /** The data is not enough o more data or reduce the subdomain **/
            /*** Domain Lower Bound ***/
            if ( Lab[IniI] < Vlab[0] ) {
               if ( CRcv->GlbInd[ii] == 0 ) { /** The receiver subdomain should be reduced **/
                  /* Get the smallest upper index */
                  flag2++;
               }
               else {  /* From where the information should be asked */
                  Inc[ii] += 1; // The direction of increment is registered
               } /* End of else case when more info is needed
                    if ( CRcv->GlbInd[ii] == 0 ) */
            }
            else flag2++;
            /*** Domain Upper Bound ***/
            if ( Vlab[pos] < Lab[EndI] ) {
               /** The receiver subdomain should be reduced **/
               if ( (CRcv->GlbInd[ii]+CRcv->Npts[ii]) == CRcv->SndrNpts[ii] ) {
                  /* Get the smallest upper index */
                  flag2++;
               }
               else {  /* From where the information should be asked */
                  Inc[ii] += 2; // The direction of increment is registered
                                             
               } /* End of else case when more info is needed
                    if ( (CRcv->GlbInd[ii]+CRcv->Npts[ii]) == CRcv->SndrNpts[ii] ) */
            }
            else flag2++;
            if ( flag2 == 2 ) dimflag++;
         } /* End of if ( Vlab[0] <= Lab[0] ) */
   
      }  /* End of if ( DomType[ii] == DCT_CARTESIAN ) */

   }  /* End of for ( ii=0; ii<dim; ii++ ) */

   /** Something is needed and it is not on schedule **/
   if ( dimflag != dim ) {
      /*** Asking for the data needed below ***/
      for ( ii=0; ii < dim; ii++ ) {
         Vlab = VarLabels[ii];
         Lab = Labels[ii];
         flag2 = 2*ii;
         /* Getting the index limits */
         if ( (Inc[ii]&1) == 1 ) {
            GlbIni[ii]--;
            Needit[flag2] = Lab[ IniI ];
         }
         else {
            Needit[flag2] = Vlab[0];
         }
         
         flag2++;
         if ( (Inc[ii]&2) == 2 ) {
            GlbEnd[ii]++;
            Needit[flag2] = Lab[ EndI ];
         }
         else {
           Needit[flag2] = Vlab[ endpos[ii] ];
         }
      } /* End of for ( ii=0; ii < dim; ii++ ) */
   } /* End of if ( dimflag != dim ) */
   
   return ( dimflag );
/* ---------------------------------  BEGIN( DCT_CheckIfNeedMore ) */
}

/*******************************************************************/
/*                       DCT_BuildMsgTag                           */
/*                                                                 */
/*!     Build the message tags to exchange for each variable
        the is defined as XXYYZZ, where XX is the couple tag,
        YY is producer variable tag and ZZ is the consumer
        variable tag. In addition the function extracts the
        model A's var name length, model B's var name legth
        and the type of production of model A's variables. All
        the variables involved in couple.

    \param[in] couple  Pointer to the DCT_Couple structure.
    \param[out]  vtag  Array with length nvar to store the
                       message tags.
    \param[out]  lvnA  Array with length nvar to store the
                       model A's var name length.
    \param[out]  lvnB  Array with length nvar to store the
                       model B's var name length.
    \param[out] vprod  Array to store the variables production
                       type.

    \return An integer with the number of variables pairs
            processed.
*/
/*******************************************************************/
int DCT_BuildMsgTag( const DCT_Couple *couple, DCT_Tag *vtag, int *lvnA,
                     int *lvnB, DCT_ProdCons *vprod )
{
/* ---------------------------------------------  Local Variables  */

   register int ii;
   
   DCT_List                *current;
   DCT_CPL_Node            *cplnode;
   DCT_Hollow_CPLVar       *DCTVar1;
   DCT_Hollow_CPLVar       *DCTVar2;

   DCT_Field               *field;
   DCT_3d_Var              *var3d;
   DCT_4d_Var              *var4d;
   
   DCT_Tag  ctag;
   int nvar;
/* -------------------------------------  BEGIN( DCT_BuildMsgTag ) */
   nvar = couple->CoupleTotNumVars;
   
   ctag = 10000*couple->CoupleTag;

   /* The varcpl table is pointed */
   current = (DCT_List *) couple->CouplingTable;
   /* The coupled var list is sweep */
   for ( ii=0; (ii < nvar) && (current != (DCT_List *) NULL ); ii++ ) {

      vtag[ii] = ctag;

      DCT_List_Ext ( &current, (DCT_VoidPointer *) &cplnode );
      switch ( cplnode->CVarType )
      {
         case DCT_FIELD_TYPE:
            field = (DCT_Field *) cplnode->CoupleVars1;
            lvnA[ii] = strlen( field->VarName );
            vprod[ii] = field->VarProduced;
            vtag[ii] += field->VarTag * (vprod[ii] == DCT_PRODUCE ? 100 : 1 );
            break;
         case DCT_3D_VAR_TYPE:
            var3d = (DCT_3d_Var *) cplnode->CoupleVars1;
            lvnA[ii] = strlen( var3d->VarName );
            vprod[ii] = var3d->VarProduced;
            vtag[ii] += var3d->VarTag * (vprod[ii] == DCT_PRODUCE ? 100 : 1 );
            break;
         case DCT_4D_VAR_TYPE:
            var4d = (DCT_4d_Var *) cplnode->CoupleVars1;
            lvnA[ii] = strlen( var4d->VarName );
            vprod[ii] = var4d->VarProduced;
            vtag[ii] += var4d->VarTag * (vprod[ii] == DCT_PRODUCE ? 100 : 1 );
         case DCT_HCPLVAR_TYPE:
            DCTVar1 = (DCT_Hollow_CPLVar *) cplnode->CoupleVars1;
            lvnA[ii] = strlen( DCTVar1->VarName );
            vprod[ii] = DCTVar1->VarProduced;
            vtag[ii] += DCTVar1->VarTag * (vprod[ii] == DCT_PRODUCE ? 100 : 1 );
      }  /* End of  switch ( CVarTypes ) */

      DCTVar2 = (DCT_Hollow_CPLVar *) cplnode->CoupleVars2;
      lvnB[ii] = strlen( DCTVar2->VarName );
      vtag[ii] += DCTVar2->VarTag * (DCTVar2->VarProduced == DCT_PRODUCE ? 100 : 1 );      
   }

   return ( nvar );
/* ---------------------------------------  END( DCT_BuildMsgTag ) */
}

/*******************************************************************/
/*                      DCT_ProdPlan_Init                          */
/*                                                                 */
/*!   Allocate and initialize the DCT_Produce_Plan structure

      \param[in]  tag  Tag to be assigned to the variable messages.
      \param[in] nmsg  Number of messages to be allocated in the
                       structure. If the value is 0 (DCT_NO_MSG) no
                       message structure memory is allocated.

      \return A pointer to a DCT_Produce_Plan structure initialized.

*/
/*******************************************************************/
DCT_Produce_Plan * DCT_ProdPlan_Init( DCT_Tag tag, DCT_Integer nmsg )
{
/* ---------------------------------------------  Local Variables  */
   DCT_Produce_Plan    *aux = (DCT_Produce_Plan *)NULL;
   
/* -----------------------------------  BEGIN( DCT_ProdPlan_Init ) */

   aux = (DCT_Produce_Plan *)malloc( sizeof(DCT_Produce_Plan) );
   if ( aux == (DCT_Produce_Plan *)NULL ) return( aux );
   
   aux->VarMsgTag = tag;
   aux->Nmsg = nmsg;

   return( aux );

/* -------------------------------------  END( DCT_ProdPlan_Init ) */
}

/*******************************************************************/
/*                       DCT_ConsPlan_Init                         */
/*                                                                 */
/*!   Allocate and initialize the DCT_Consume_Plan structure.

      \param[in]  dim  Dimesion of the variable.
      \param[in]  tag  Tag to be assigned to the variable messages.
      \param[in] nmsg  Number of messages to be allocated in the
                       structure. If the value is 0 (DCT_NO_MSG) no
                       message structure memory is allocated.

      \return A pointer to a DCT_Consume_Plan structure initialized.

*/
/*******************************************************************/
DCT_Consume_Plan * DCT_ConsPlan_Init( int dim, DCT_Tag tag, DCT_Data_Transformation vtran,
                                      DCT_Units vunit, DCT_Data_Types uvtype,
                                      DCT_Integer nmsg )
{
/* ---------------------------------------------  Local Variables  */
   DCT_Consume_Plan    *aux=(DCT_Consume_Plan *)NULL;
   
/* -----------------------------------  BEGIN( DCT_ConsPlan_Init ) */

   aux = (DCT_Consume_Plan *)malloc( sizeof(DCT_Consume_Plan) );
   if ( aux == (DCT_Consume_Plan *)NULL ) return( aux );
 
   aux->VarMsgTag  = tag;
   aux->VarTransf  = vtran;
   aux->VarChUnits = vunit;
   aux->VarIniPos  = (DCT_Integer *)malloc( sizeof(DCT_Integer)*dim );
   if ( aux->VarIniPos == (DCT_Integer *)NULL ) {
      free ( aux );
      aux = (DCT_Consume_Plan *)NULL;
      return( aux );
   }
   aux->VarNpts    = (DCT_Integer *)malloc( sizeof(DCT_Integer)*dim );
   if ( aux->VarNpts == (DCT_Integer *)NULL ) {
      free ( aux->VarIniPos );
      free ( aux );
      aux = (DCT_Consume_Plan *)NULL;
      return( aux );
   }
   aux->VarDomType = (DCT_Domain_Type *)malloc( sizeof(DCT_Domain_Type)*dim );
   if ( aux->VarDomType == (DCT_Domain_Type *)NULL ) {
      free ( aux->VarIniPos );
      free ( aux->VarNpts );
      free ( aux );
      aux = (DCT_Consume_Plan *)NULL;
      return( aux );
   }
   aux->VarInt     = (DCT_Integer **)NULL;
   aux->VarLabels  = (DCT_VoidPointer *)NULL;
   aux->VarMask     = (DCT_Integer *)NULL;

   aux->VarRcvdDType = uvtype;
   aux->VarTempVal   = (DCT_VoidPointer)NULL;
   aux->VarRcvdNpts  = (DCT_Integer *)malloc( sizeof(DCT_Integer)*(size_t)dim );
   if ( aux->VarRcvdNpts == (DCT_Integer *)NULL ) {
      free ( aux->VarIniPos );
      free ( aux->VarNpts );
      free ( aux->VarDomType );
      free ( aux );
      aux = (DCT_Consume_Plan *)NULL;
      return( aux );
   }
   aux->VarRcvdVal   = (DCT_VoidPointer)NULL;

   aux->Nmsg = nmsg;
   return( aux );

/* -------------------------------------  END( DCT_ConsPlan_Init ) */
}

/*******************************************************************/
/*                       DCT_IncludeNeighbor                       */
/*                                                                 */
/*!   Include the receiver (consumer processor) in a given array
      if it is not in the array, otherwise leave the array unchanged.

      \param[in]         Recvr  Rank or process Id of the receiver
                                to included in the array.
      \param[in,out]   RcvrNSD  Number of receivers, or length of
                                the array.
      \param[in,out] ProdNeigh  Array containing the ranks of the
                                receivers.

      \return Integer value with 1 if the Receiver rank was included
              in the array, 0 otherwise the Receiver already existed.

*/
/*******************************************************************/
int DCT_IncludeNeighbor ( int Recvr, DCT_Integer *RcvrNSD, int *ProdNeigh )
{
/* ---------------------------------------------  Local Variables  */

   DCT_Integer  ii, maxsd;
   int          included=0;
   
/* ---------------------------------  BEGIN( DCT_IncludeNeighbor ) */

   maxsd = *RcvrNSD;
   /** Check if Recvr is already in the list **/
   for ( ii = 0; (ii < maxsd) && ( ProdNeigh[ii] != Recvr); ii++ );
   
   if ( ii == maxsd ) {
      ProdNeigh[ (*RcvrNSD)++ ] = Recvr;
      included = 1;
   }

   return included; 
/* -----------------------------------  END( DCT_IncludeNeighbor ) */
}

/*******************************************************************/
/*                        DCT_CheckInclude                         */
/*                                                                 */
/*!   Include the receiver (consumer processor) in a given array
      if it is not in the array, otherwise leave the array unchanged.

      \param[in]         Recvr  Rank or process Id of the receiver
                                to included in the array.
      \param[in,out]   RcvrNSD  Number of receivers, or length of
                                the array.
      \param[in,out] ProdNeigh  Array containing the ranks of the
                                receivers.

      \return Integer value with value of the index where it is
              inserted, if the Receiver rank was included in the
              array, the value of RcvrNSD otherwise the Receiver
              already existed. I.e. less than RcvrNSD, the receiver
              was included, equal to RcvrNSD if Recvr existed.

*/
/*******************************************************************/
inline int DCT_CheckInclude ( int Recvr, DCT_Integer *RcvrNSD, int *ProdNeigh )
{
/* ---------------------------------------------  Local Variables  */

   DCT_Integer  ii, maxsd;
   
/* ------------------------------------  BEGIN( DCT_CheckInclude ) */

   maxsd = *RcvrNSD;
   /** Check if Recvr is already in the list **/
   for ( ii = 0; (ii < maxsd) && ( ProdNeigh[ii] != Recvr); ii++ );
   
   if ( ii == maxsd ) {
      ProdNeigh[ (*RcvrNSD)++ ] = Recvr;
   }

   return ii; 
/* --------------------------------------  END( DCT_CheckInclude ) */
}

/*******************************************************************/
/*                   DCT_SearchArray_RcvSlRank                     */
/*                                                                 */
/*!   Include the receiver (consumer processor) in a given array
      if it is not in the array, otherwise leave the array unchanged.

      \param[in] ConsRecvNmsg  Number of subdomains, or length of
                               the array.
      \param[in] ConsRecvSlab  Array of entry as DCT_SD_Consume.
      \param[in]     RankProc  Rank of the procesor to be searched
                               in the subdomain.

      \return Integer value with value of the index where the RankProc
              is founded in the consume structure, otherwise the value
              of ConsRecvNmsg,because it was not found.

     \note This function is used several time inside
           DCT_Calculate_Neighbors.

*/
/*******************************************************************/
inline int DCT_SearchArray_RcvSlRank( int ConsRecvNmsg, DCT_SD_Consume *ConsRecvSlab,
                                             DCT_Rank RankProc )
{
/* ---------------------------------------------  Local Variables  */
register int    ii;

/* ---------------------------  BEGIN( DCT_SearchArray_RcvSlRank ) */
   for ( ii=0; ( ii < ConsRecvNmsg ) && ( ConsRecvSlab[ii].Sendr != RankProc  ); ii++ );
   return ii;
/* -----------------------------  END( DCT_SearchArray_RcvSlRank ) */
}

/*******************************************************************/
/*                   DCT_Calculate_Neighbors                       */
/*                                                                 */
/*!   Calculate the neighbor processes of a set of processes
      that intersect a consumer domain. The neighbor set is
      the possible set where a consumer can ask for more data

      \param[in]      ProdNSDom  Number of subdomain in the producer
                                 model.
      \param[in]  ProdParLayout  The parallel layout of the producer.
      \param[in]  ProdLayoutDim  The dimensions of the parallel layout.
      \param[in,out] ProdSDPlan  Array of DCT_SDProduce_Plan structure
                                 that represents the producer subdomain
                                 overlapping.
      \param[in,out]  ProdNeigh  Arrays of DCT_Rank of producer processes
                                 that a consumer could ask for information.
      \param[in,out]    indices  An array of indices of the order in the
                                 producer subdomains.
      \param[in,out] ConsSDPlan  Array of DCT_SDConsume_Plan structure
                                 that represents the consumer subdomain
                                 overlapping.

      \return An integer error control value with 1 if success, 
              and 0 otherwise.

*/
/*******************************************************************/
int DCT_Calculate_Neighbors( DCT_Integer ProdNSDom, /*DCT_Data_SubDom *ProdSubDom,*/
                             DCT_Distribution  ProdParLayout, DCT_Integer *ProdLayoutDim,
                             DCT_SDProduce_Plan *ProdSDPlan, DCT_Rank **ProdNeigh,
                             int *indices, DCT_SDConsume_Plan *ConsSDPlan )
//       \param[in]     ProdSubDom  Array of DCT_Data_SubDom that represent
//                                  the structure of every subdomain.
{
/* ---------------------------------------------  Local Variables  */

   int             ierr=0;
   int             pardim, nneigh;

   // int             neigh[26][3]; /** The maximum number of neighbors **/
   int             neigh[78]; /** The maximum number of neighbors **/
   int             Istride, Jstride; /* Kstride; */
   DCT_Integer     ConsRecvNmsg, ConsNSDom;
   int            *ProSDInd;
   DCT_Rank        Rcvr; /* RankProc;*/
   int             ind, indI, indJ, neighInd;
   int             neI, neJ;
   register int    ii, kk;
   
/* -----------------------------  BEGIN( DCT_Calculate_Neighbors ) */

   
   switch( ProdParLayout )
   {
      case DCT_DIST_SEQUENTIAL:
         return (0);
         break;
      case DCT_DIST_RECTANGULAR:
         pardim = 2;
         Istride = ProdLayoutDim[0];
         Jstride = ProdLayoutDim[1];
         nneigh = 16;
         /** The subdomain array is ordered
             with the i index moving first  **/
         /** The neighbors are assigned in such a way that be consecutive **/
         /** The directions are considered
             i equal to x; j equal to y    **/
         neigh[0] = - 1; /** SW **/
         neigh[1] = - 1; /** SW **/
         neigh[2] =   0; /** South **/
         neigh[3] = - 1; /** South **/
         neigh[4] =   1; /** SE **/
         neigh[5] = - 1; /** SE **/
         neigh[6] = - 1; /** West **/
         neigh[7] =   0; /** West **/
         neigh[8] =   1; /** East **/
         neigh[9] =   0; /** East **/
         neigh[10] = - 1; /** NW **/
         neigh[11] =   1; /** NW **/
         neigh[12] =   0; /** North **/
         neigh[13] =   1; /** North **/
         neigh[14] =   1; /** NE **/
         neigh[15] =   1; /** NE **/
         break;
      case DCT_DIST_3D_RECTANGULAR:
         pardim = 3;
         Istride = ProdLayoutDim[0];
         Jstride = ProdLayoutDim[1];
         /*Kstride = ProdLayoutDim[2];*/
         nneigh = 78;
         /** The subdomain array is ordered
             with the i index moving first  **/
         /** The directions are considered
             i equal to x; j equal to y; k equal to z    **/
         /** Plane below  **/
         neigh[0] = - 1; /** SW **/
         neigh[1] = - 1; /** SW **/
         neigh[2] = - 1; /** SW **/
         neigh[3] =   0; /** South **/
         neigh[4] = - 1; /** South **/
         neigh[5] = - 1; /** South **/
         neigh[6] =   1; /** SE **/
         neigh[7] = - 1; /** SE **/
         neigh[8] = - 1; /** SE **/
         neigh[9] = - 1; /** West **/
         neigh[10] =   0; /** West **/
         neigh[11] = - 1; /** West **/
         neigh[12] =   0; /** Below **/
         neigh[13] =   0; /** Below **/
         neigh[14] = - 1; /** Below **/
         neigh[15] =   1; /** East **/
         neigh[16] =   0; /** East **/
         neigh[17] = - 1; /** East **/
         neigh[18] = - 1; /** NW **/
         neigh[19] =   1; /** NW **/
         neigh[20] = - 1; /** NW **/
         neigh[21] =   0; /** North **/
         neigh[22] =   1; /** North **/
         neigh[23] = - 1; /** North **/
         neigh[24] =   1; /** NE **/
         neigh[25] =   1; /** NE **/
         neigh[26] = - 1; /** NE **/
         /** Same plane  **/
         neigh[27] = - 1; /** SW **/
         neigh[28] = - 1; /** SW **/
         neigh[29] =   0; /** SW **/
         neigh[30] =   0; /** South **/
         neigh[31] = - 1; /** South **/
         neigh[32] =   0; /** South **/
         neigh[33] =   1; /** SE **/
         neigh[34] = - 1; /** SE **/
         neigh[35] =   0; /** SE **/
         neigh[36] = - 1; /** West **/
         neigh[37] =   0; /** West **/
         neigh[38] =   0; /** West **/
         neigh[39] =   1; /** East **/
         neigh[40] =   0; /** East **/
         neigh[41] =   0; /** East **/
         neigh[42] = - 1; /** NW **/
         neigh[43] =   1; /** NW **/
         neigh[44] =   0; /** NW **/
         neigh[45] =   0; /** North **/
         neigh[46] =   1; /** North **/
         neigh[47] =   0; /** North **/
         neigh[48] =   1; /** NE **/
         neigh[49] =   1; /** NE **/
         neigh[50] =   0; /** NE **/
         /** Plane Above **/
         neigh[51] = - 1; /** SW **/
         neigh[52] = - 1; /** SW **/
         neigh[53] =   1; /** SW **/
         neigh[54] =   0; /** South **/
         neigh[55] = - 1; /** South **/
         neigh[56] =   1; /** South **/
         neigh[57] =   1; /** SE **/
         neigh[58] = - 1; /** SE **/
         neigh[59] =   1; /** SE **/
         neigh[60] = - 1; /** West **/
         neigh[61] =   0; /** West **/
         neigh[62] =   1; /** West **/
         neigh[63] =   0; /** Above **/
         neigh[64] =   0; /** Above **/
         neigh[65] =   1; /** Above **/
         neigh[66] =   1; /** East **/
         neigh[67] =   0; /** East **/
         neigh[68] =   1; /** East **/
         neigh[69] = - 1; /** NW **/
         neigh[70] =   1; /** NW **/
         neigh[71] =   1; /** NW **/
         neigh[72] =   0; /** North **/
         neigh[73] =   1; /** North **/
         neigh[74] =   1; /** North **/
         neigh[75] =   1; /** NE **/
         neigh[76] =   1; /** NE **/
         neigh[77] =   1; /** NE **/

         break;
      default:
         return(2);
   } /* End of switch( ProdParLayout ) */

//    TmpProSDInd= (int *) malloc(sizeof(int)*(size_t)ProdNSDom);
//    DCTERROR( TmpProSDInd == (int *)NULL, "Memory Allocation Failed", return(1) );
   
   ConsRecvNmsg = ConsSDPlan->Nmsg;
   Rcvr = ConsSDPlan->Recvr;
   ConsNSDom = ConsRecvNmsg; /* Number of Prod Sub Dom register in consumer */
   /* For each subdomain look for the neighbor */
   if ( pardim == 2 ) {
      for ( ii = 0; (ConsNSDom < ProdNSDom) && (ii < ConsRecvNmsg); ii++ ) {
         ind = indices[ii];
         indI = ind%Istride;
         indJ = ind/Istride;
         /* For every neighbor */
         for ( kk = 0; (ConsNSDom < ProdNSDom) && (kk < nneigh); kk+=2 ) {
            neI = indI + neigh[kk];
            neJ = indJ + neigh[kk+1];
            if ( ( ( neI >= 0) && (neI < Istride) ) &&
                 ( ( neJ >= 0) && (neJ < Jstride) ) ) {
               neighInd = neJ*Istride + neI;
               /* RankProc = ProdSubDom[ neighInd ].RankProc;*/
               /* If it is not inlcuded */
               if ( DCT_IncludeNeighbor ( neighInd, &(ConsNSDom), indices ) ) {
                  DCT_IncludeNeighbor ( (int)Rcvr,
                                        (DCT_Integer *)&(ProdSDPlan[ neighInd ].RcvrNSD),
                                        (int *)ProdNeigh[ neighInd ] );
               }
            }
         }
      } /* End of for ( ii = 0; ( ii < ProdNSDom ) && ( chked < ConsRecvNmsg ); ii++ ) */
   } /* End of if ( pardim == 2 ) */

   ConsSDPlan->SndrNSD = ConsNSDom;
   ProSDInd= (int *) malloc(sizeof(int)*(size_t)ConsNSDom);
   DCTERROR( ProSDInd == (int *)NULL, "Memory Allocation Failed", return(1) );
   for ( ii=0; ii < ConsNSDom; ii++ ) ProSDInd[ii] = indices[ii];
   
   /* To simplify the store and avoid store several time
      pieces of the subdomain, only the indices are used
      in a DCT_Integer array                             **/
   ConsSDPlan->SndrSDom = (DCT_Data_SubDom *)ProSDInd;

   return ierr;
/* -------------------------------  END( DCT_Calculate_Neighbors ) */
}

/*******************************************************************/
/*                 DCT_Calculate_PerProc_Neighbors                 */
/*                                                                 */
/*!   Calculates the neighbor processes of each subdomain
      that intersects with a consumer subdomain. The neighbor set
      is the possible set where a consumer can ask for more data


      \param[in]           prod  Variable to indicate if the neighbors
                                 are calculated from the producer or
                                 consumer point of view.
      \param[in]      ProdNSDom  Number of subdomain of the model.
      \param[in]            dim  Dimension of the model domain.
      \param[in]           rank  Id of the process who is running the
                                 function.
      \param[in]     ProdSubDom  Array of subdomains of the model.
      \param[in]  ProdParLayout  Type of parallel layout.
      \param[in]  ProdLayoutDim  Dimensions of the parallel layout.
      \param[in,out] ProdSDPlan  Producer model plan. If the parameter
                                 prod is equal to DCT_CONSUME, this
                                 parameter is not used.
      \param[in,out]  ProdNeigh  Producer model plan. If the parameter
                                 prod is equal to DCT_CONSUME, this
                                 parameter is not used.
      \param[in,out]    indices  Array of indices included in the
                                 neighbors.
      \param[in,out] ConsSDPlan  Consumer model plan, that is calculate 
                                 the neighbors in the overlap area.

      \return An integer error control value with 1 if success, 
              and 0 otherwise.

*/
/*******************************************************************/
int DCT_Calculate_PerProc_Neighbors ( 
                    DCT_ProdCons prod, DCT_Integer ProdNSDom, DCT_Integer dim,
                    DCT_Rank rank, DCT_Data_SubDom *ProdSubDom,
                    DCT_Distribution ProdParLayout, DCT_Integer *ProdLayoutDim,
                    DCT_SDProduce_Plan *ProdSDPlan, DCT_Rank *ProdNeigh, int *indices,
                    DCT_SDConsume_Plan *ConsSDPlan )
{
/* ---------------------------------------------  Local Variables  */

   int               ierr=0;
   int               pardim, nneigh;
   
   DCT_Integer      *IniPos, *EndPos;
   DCT_Integer      *ProdIniPos, *ProdEndPos;

   int               neigh[78]; /** The maximum number of neighbors **/
   int               Istride, Jstride, Kstride, IJstride;
   DCT_Integer       ConsRecvNmsg, ConsNSDom;
   //int              *TmpProSDInd;
   DCT_Data_SubDom  *ProSDLst, *auxsdptr;
   DCT_Rank          RankProc, Rcvr;
   int               ind, indI, indJ, indK, neighInd;/*, indK, neK, jj*/
   int               neI, neJ, neK, aux;
   register int      ii, kk;
   
/* ---------------------  BEGIN( DCT_Calculate_PerProc_Neighbors ) */

   
   switch( ProdParLayout )
   {
      case DCT_DIST_SEQUENTIAL:
         return (0);
         break;
      case DCT_DIST_RECTANGULAR:
         pardim = 2;
         Istride = ProdLayoutDim[0];
         Jstride = ProdLayoutDim[1];
         nneigh = 16;
         /** The subdomain array is ordered
             with the i index moving first  **/
         /** The neighbors are assigned in such a way that be consecutive **/
         /** The directions are considered
             i equal to x; j equal to y                                        **/
         neigh[0] = - 1; /** SW-i            *        *        *        *      **/
         neigh[1] = - 1; /** SW-j       *************************************  **/
         neigh[2] =   0; /** South-i         *        *        *        *      **/
         neigh[3] = - 1; /** South-j    j=1  *   NW   *   N    *   NE   *      **/
         neigh[4] =   1; /** SE-i            *        *        *        *      **/
         neigh[5] = - 1; /** SE-j       *************************************  **/
         neigh[6] = - 1; /** West-i          *        * Consum *        *      **/
         neigh[7] =   0; /** West-j     j=0  *   W    * SubDom *    E   *      **/
         neigh[8] =   1; /** East-i          *        * i=0,j=0*        *      **/
         neigh[9] =   0; /** East-j     *************************************  **/
         neigh[10] = - 1; /** NW-i           *        *        *        *      **/
         neigh[11] =   1; /** NW-j      j=-1 *  SW    *   S    *   SE   *      **/
         neigh[12] =   0; /** North-i        *        *        *        *      **/
         neigh[13] =   1; /** North-j   *************************************  **/
         neigh[14] =   1; /** NE-i           *  i=-1  *  i=0   *   i=1  *      **/
         neigh[15] =   1; /** NE-j                                             **/
         break;
      case DCT_DIST_3D_RECTANGULAR:
         pardim = 3;
         Istride = ProdLayoutDim[0];
         Jstride = ProdLayoutDim[1];
         Kstride = ProdLayoutDim[2];
         IJstride = Istride*Jstride;
         nneigh = 78;
         /** The subdomain array is ordered
             with the i index moving first  **/
         /** The directions are considered
             i equal to x; j equal to y; k equal to z    **/
         /** Plane below  **/
/**             Plane Below                  **/
/**   k=-1 *        *        *        *      **/
/**   *************************************  **/
/**        *        *        *        *      **/
/**   j=1  *   NW   *   N    *   NE   *      **/
/**        *        *        *        *      **/
/**   *************************************  **/
/**        *        *        *        *      **/
/**   j=0  *   W    * Below  *    E   *      **/
/**        *        *        *        *      **/
/**   *************************************  **/
/**        *        *        *        *      **/
/**   j=-1 *  SW    *   S    *   SE   *      **/
/**        *        *        *        *      **/
/**   *************************************  **/
/**        *  i=-1  *  i=0   *   i=1  *      **/
/**                                          **/
/**               Same Plane                 **/
/**   k=0  *        *        *        *      **/
/**   *************************************  **/
/**        *        *        *        *      **/
/**   j=1  *   NW   *   N    *   NE   *      **/
/**        *        *        *        *      **/
/**   *************************************  **/
/**        *        * Consum *        *      **/
/**   j=0  *   W    * SubDom *    E   *      **/
/**        *        * i=0,j=0*        *      **/
/**   *************************************  **/
/**        *        *        *        *      **/
/**   j=-1 *  SW    *   S    *   SE   *      **/
/**        *        *        *        *      **/
/**   *************************************  **/
/**        *  i=-1  *  i=0   *   i=1  *      **/
/**                                          **/
/**               Plane Above                **/
/**   k=1  *        *        *        *      **/
/**   *************************************  **/
/**        *        *        *        *      **/
/**   j=1  *   NW   *   N    *   NE   *      **/
/**        *        *        *        *      **/
/**   *************************************  **/
/**        *        *        *        *      **/
/**   j=0  *   W    * Above  *    E   *      **/
/**        *        *        *        *      **/
/**   *************************************  **/
/**        *        *        *        *      **/
/**   j=-1 *  SW    *   S    *   SE   *      **/
/**        *        *        *        *      **/
/**   *************************************  **/
/**        *  i=-1  *  i=0   *   i=1  *      **/
/**                                          **/
         neigh[0] = - 1; /** SW **/
         neigh[1] = - 1; /** SW **/
         neigh[2] = - 1; /** SW **/
         neigh[3] =   0; /** South **/
         neigh[4] = - 1; /** South **/
         neigh[5] = - 1; /** South **/
         neigh[6] =   1; /** SE **/
         neigh[7] = - 1; /** SE **/
         neigh[8] = - 1; /** SE **/
         neigh[9] = - 1; /** West **/
         neigh[10] =   0; /** West **/
         neigh[11] = - 1; /** West **/
         neigh[12] =   0; /** Below **/
         neigh[13] =   0; /** Below **/
         neigh[14] = - 1; /** Below **/
         neigh[15] =   1; /** East **/
         neigh[16] =   0; /** East **/
         neigh[17] = - 1; /** East **/
         neigh[18] = - 1; /** NW **/
         neigh[19] =   1; /** NW **/
         neigh[20] = - 1; /** NW **/
         neigh[21] =   0; /** North **/
         neigh[22] =   1; /** North **/
         neigh[23] = - 1; /** North **/
         neigh[24] =   1; /** NE **/
         neigh[25] =   1; /** NE **/
         neigh[26] = - 1; /** NE **/
         /** Same plane  **/
         neigh[27] = - 1; /** SW **/
         neigh[28] = - 1; /** SW **/
         neigh[29] =   0; /** SW **/
         neigh[30] =   0; /** South **/
         neigh[31] = - 1; /** South **/
         neigh[32] =   0; /** South **/
         neigh[33] =   1; /** SE **/
         neigh[34] = - 1; /** SE **/
         neigh[35] =   0; /** SE **/
         neigh[36] = - 1; /** West **/
         neigh[37] =   0; /** West **/
         neigh[38] =   0; /** West **/
         neigh[39] =   1; /** East **/
         neigh[40] =   0; /** East **/
         neigh[41] =   0; /** East **/
         neigh[42] = - 1; /** NW **/
         neigh[43] =   1; /** NW **/
         neigh[44] =   0; /** NW **/
         neigh[45] =   0; /** North **/
         neigh[46] =   1; /** North **/
         neigh[47] =   0; /** North **/
         neigh[48] =   1; /** NE **/
         neigh[49] =   1; /** NE **/
         neigh[50] =   0; /** NE **/
         /** Plane Above **/
         neigh[51] = - 1; /** SW **/
         neigh[52] = - 1; /** SW **/
         neigh[53] =   1; /** SW **/
         neigh[54] =   0; /** South **/
         neigh[55] = - 1; /** South **/
         neigh[56] =   1; /** South **/
         neigh[57] =   1; /** SE **/
         neigh[58] = - 1; /** SE **/
         neigh[59] =   1; /** SE **/
         neigh[60] = - 1; /** West **/
         neigh[61] =   0; /** West **/
         neigh[62] =   1; /** West **/
         neigh[63] =   0; /** Above **/
         neigh[64] =   0; /** Above **/
         neigh[65] =   1; /** Above **/
         neigh[66] =   1; /** East **/
         neigh[67] =   0; /** East **/
         neigh[68] =   1; /** East **/
         neigh[69] = - 1; /** NW **/
         neigh[70] =   1; /** NW **/
         neigh[71] =   1; /** NW **/
         neigh[72] =   0; /** North **/
         neigh[73] =   1; /** North **/
         neigh[74] =   1; /** North **/
         neigh[75] =   1; /** NE **/
         neigh[76] =   1; /** NE **/
         neigh[77] =   1; /** NE **/

         break;
      default:
         return(2);
   } /* End of switch( ProdParLayout ) */

//    TmpProSDInd= (int *) malloc(sizeof(int)*(size_t)ProdNSDom);
//    DCTERROR( TmpProSDInd == (int *)NULL, "Memory Allocation Failed", return(1) );
   
   ConsRecvNmsg = ConsSDPlan->Nmsg;
   Rcvr = ConsSDPlan->Recvr;
   ConsNSDom = ConsRecvNmsg; /* Number of Prod Sub Dom register in consumer */
   /* For each subdomain look for the neighbor */
   if ( pardim == 2 ) {
      if ( prod == DCT_CONSUME ) {
         for ( ii = 0; (ConsNSDom < ProdNSDom) && (ii < ConsRecvNmsg); ii++ ) {
            ind = indices[ii];
            indI = ind%Istride;
            indJ = ind/Istride;
            /* For every neighbor */
            for ( kk = 0; (ConsNSDom < ProdNSDom) && (kk < nneigh); kk+=2 ) {
               neI = indI + neigh[kk];
               neJ = indJ + neigh[kk+1];
               if ( ( ( neI >= 0) && (neI < Istride) ) &&
                    ( ( neJ >= 0) && (neJ < Jstride) ) ) {
                  neighInd = neJ*Istride + neI;
                  RankProc = ProdSubDom[ neighInd ].RankProc;
                  /* If it is not inlcuded */
                  DCT_IncludeNeighbor ( neighInd, &(ConsNSDom), indices );
               }
            }
         } /* End of for ( ii = 0; ( ii < ProdNSDom ) && ( chked < ConsRecvNmsg ); ii++ ) */
      }
      else {
         for ( ii = 0; (ConsNSDom < ProdNSDom) && (ii < ConsRecvNmsg); ii++ ) {
            ind = indices[ii];
            indI = ind%Istride;
            indJ = ind/Istride;
            /* For every neighbor */
            for ( kk = 0; (ConsNSDom < ProdNSDom) && (kk < nneigh); kk+=2 ) {
               neI = indI + neigh[kk];
               neJ = indJ + neigh[kk+1];
               if ( ( ( neI >= 0) && (neI < Istride) ) &&
                    ( ( neJ >= 0) && (neJ < Jstride) ) ) {
                  neighInd = neJ*Istride + neI;
                  RankProc = ProdSubDom[ neighInd ].RankProc;
                  /* If it is not inlcuded */
                  if ( DCT_IncludeNeighbor ( neighInd, &(ConsNSDom), indices ) ) {
                     if ( RankProc == rank )
                        DCT_IncludeNeighbor ( (int)Rcvr,
                                              (DCT_Integer *)&(ProdSDPlan->RcvrNSD),
                                              (int *)ProdNeigh );
                  }
               }
            }
         } /* End of for ( ii = 0; ( ii < ProdNSDom ) && ( chked < ConsRecvNmsg ); ii++ ) */
      }
   } /* End of if ( pardim == 2 ) */
   else if ( pardim == 3 ) {     /*** It has not been tested  **/
      if ( prod == DCT_CONSUME ) {
         for ( ii = 0; (ConsNSDom < ProdNSDom) && (ii < ConsRecvNmsg); ii++ ) {
            ind = indices[ii];
            aux = ind%IJstride;
            indI = aux%Istride;
            indJ = aux/Istride;
            indK = ind/IJstride;
            /* For every neighbor */
            for ( kk = 0; (ConsNSDom < ProdNSDom) && (kk < nneigh); kk+=3 ) {
               neI = indI + neigh[kk];
               neJ = indJ + neigh[kk+1];
               neK = indK + neigh[kk+2];
               if ( ( ( neI >= 0) && (neI < Istride) ) &&
                    ( ( neJ >= 0) && (neJ < Jstride) ) &&
                    ( ( neK >= 0) && (neK < Kstride) ) ) {
                  neighInd = neK*IJstride + neJ*Istride + neI;
                  RankProc = ProdSubDom[ neighInd ].RankProc;
                  /* If it is not inlcuded */
                  DCT_IncludeNeighbor ( neighInd, &(ConsNSDom), indices );
               }
            }
         } /* End of for ( ii = 0; ( ii < ProdNSDom ) && ( chked < ConsRecvNmsg ); ii++ ) */
      }
      else {
         for ( ii = 0; (ConsNSDom < ProdNSDom) && (ii < ConsRecvNmsg); ii++ ) {
            ind = indices[ii];
            aux = ind%IJstride;
            indI = aux%Istride;
            indJ = aux/Istride;
            indK = ind/IJstride;
            /* For every neighbor */
            for ( kk = 0; (ConsNSDom < ProdNSDom) && (kk < nneigh); kk+=3 ) {
               neI = indI + neigh[kk];
               neJ = indJ + neigh[kk+1];
               neK = indK + neigh[kk+2];
               if ( ( ( neI >= 0) && (neI < Istride) ) &&
                    ( ( neJ >= 0) && (neJ < Jstride) )  &&
                    ( ( neK >= 0) && (neK < Kstride) ) ) {
                  neighInd = neK*IJstride + neJ*Istride + neI;
                  RankProc = ProdSubDom[ neighInd ].RankProc;
                  /* If it is not inlcuded */
                  if ( DCT_IncludeNeighbor ( neighInd, &(ConsNSDom), indices ) ) {
                     if ( RankProc == rank )
                        DCT_IncludeNeighbor ( (int)Rcvr,
                                              (DCT_Integer *)&(ProdSDPlan->RcvrNSD),
                                              (int *)ProdNeigh );
                  }
               }
            }
         } /* End of for ( ii = 0; ( ii < ProdNSDom ) && ( chked < ConsRecvNmsg ); ii++ ) */
      }
   } /* End of if ( pardim == 3 ) */

   if ( prod == DCT_CONSUME ) {
      ConsSDPlan->SndrNSD = ConsNSDom;
      ProSDLst= (DCT_Data_SubDom *) malloc(sizeof(DCT_Data_SubDom)*(size_t)ConsNSDom);
      DCTERROR( ProSDLst == (DCT_Data_SubDom *)NULL, "Memory Allocation Failed", return(1) );
      /* The array starting point is stored */
      ConsSDPlan->SndrSDom = ProSDLst;

      /* The space is allocated */
      auxsdptr = ProSDLst;
      for ( ii=0; ii < ConsNSDom; ii++, auxsdptr++ ) {
         IniPos = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t) dim);
         DCTERROR( IniPos == (DCT_Integer *)NULL, "Memory Allocation Failed", return(1) );
         auxsdptr->IniPos = IniPos;
         EndPos = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t) dim);
         DCTERROR( EndPos == (DCT_Integer *)NULL, "Memory Allocation Failed", return(1) );
         auxsdptr->EndPos = EndPos;
      }

      for ( ii=0; ii < ConsNSDom; ii++,  ProSDLst++ ) {
         auxsdptr = ProdSubDom + indices[ii];
         ProdIniPos = auxsdptr->IniPos;
         ProdEndPos = auxsdptr->EndPos;
         IniPos = ProSDLst->IniPos;
         EndPos = ProSDLst->EndPos;

         ProSDLst->RankProc = auxsdptr->RankProc;
         for ( kk =0; kk < dim; kk++ ) {
            IniPos[kk] = ProdIniPos[kk];
            EndPos[kk] = ProdEndPos[kk];
         }

      }

   }

   return ierr;
/* -----------------------  END( DCT_Calculate_PerProc_Neighbors ) */
}

/*******************************************************************/
/*                       DCT_Data_Allocate                         */
/*                                                                 */
/*!      Returns the address of the data array Allocated of type
         dimension specified.

    \param[in] UDType  Type of the user data, i.e. float, double,
                       etc. \sa DCT_Data_Types
    \param[in]    Dim  Array of lenght dim, with the number of
                       elements in each direction.
    \param[in]    dim  Dimension (2, 3 or 4) that was set the user
                       data array.
    \param[in]    opt  Option to initialize the allocated array.
                       If opt is different to zero, the array will
                       be set to 0, otherwise return no-initialed.

      \return A pointer to the memory was allocated.

*/
/*******************************************************************/
void * DCT_Data_Allocate( DCT_Data_Types UDType, int *Dim, int dim, int opt )
{
/* ---------------------------------------------  Local Variables  */

   DCT_Integer  *idata;
   float        *fdata;
   double       *ddata;
   long double  *lddata;

   int           totsize;
   register int  ii;
   
/* -----------------------------------  BEGIN( DCT_Data_Allocate ) */

   totsize = 1;
   for ( ii = 0; ii < dim; ii++ ) totsize *= Dim[ii];
   
   switch (UDType) {
      case DCT_INTEGER:
         idata = malloc( sizeof(DCT_Integer)*(size_t)totsize );
         if (opt) for ( ii = 0; ii < totsize; ii++ ) idata[ii] = 0;
         return ( idata );
      case DCT_FLOAT:
         fdata = malloc( sizeof(float)*(size_t)totsize );
         if (opt) for ( ii = 0; ii < totsize; ii++ ) fdata[ii] = 0.0;
         return ( fdata );
      case DCT_DOUBLE:
         ddata = malloc( sizeof(double)*(size_t)totsize );
         if (opt) for ( ii = 0; ii < totsize; ii++ ) ddata[ii] = 0.0E0;
         return ( ddata );
      case DCT_LONG_DOUBLE:
         lddata = malloc( sizeof(long double)*(size_t)totsize );
         if (opt) for ( ii = 0; ii < totsize; ii++ ) lddata[ii] = 0.0E0;
         return ( lddata );
      default:
         return ( (DCT_VoidPointer)NULL);
   }

/* -------------------------------------  END( DCT_Data_Allocate ) */
}

/*******************************************************************/
/*                     DCT_UserData_GetPtr                         */
/*                                                                 */
/*!   Calculate the address in the data user array corresponding
      with the element indicated by the indices (i,j,k) given in
      the array IniInd.

      \param[in]  UData  Pointer to the data user array.
      \param[in] UDType  Type of the user data, i.e. float, double,
                         etc.
      \param[in] IniInd  Array of lenght dim, with the element indices.
      \param[in]    Dim  Array of lenght dim, with the number of elements
                         in each direction.
      \param[in]    dim  Dimension (2, 3 or 4) that was set the user data
                         array.

      \return A pointer to an entry in the user array, corresponding
              with the indices (i,j,k) given in IniInd.
              
*/
/*******************************************************************/
void * DCT_UserData_GetPtr( DCT_VoidPointer UData, DCT_Data_Types UDType,
                            DCT_Integer *IniInd, int *Dim, int dim )
{
/* ---------------------------------------------  Local Variables  */

   float        *fdata;
   double       *ddata;
   long double  *lddata;

   int           ind=0;
   register int  ii;
   
/* ---------------------------------  BEGIN( DCT_UserData_GetPtr ) */

   ind = IniInd[0];
   for ( ii = 1; ii < dim; ii++ ) ind = ind*Dim[ii] + IniInd[ii];
   
   switch (UDType) {
      case DCT_FLOAT:
         fdata = UData;
         return ( (fdata + ind) );
      case DCT_DOUBLE:
         ddata = UData;
         return ( (ddata + ind) );
      case DCT_LONG_DOUBLE:
         lddata = UData;
         return ( (lddata + ind) );
      default:
         return ( (DCT_VoidPointer)NULL);
   }

/* -----------------------------------  END( DCT_UserData_GetPtr ) */
}

/*******************************************************************/
/*                      DCT_OnNewBoundary                          */
/*                                                                 */
/*!   This function is implemented to know, for a given
      subdomain, the new boundary lie on. That is used when
      more data is needed. 

      \param[in] SDIniPos  Pointer to an array of length dim, with the
                           starting domain index of the subdomain.
      \param[in] SDEndPos  Pointer to an array of length dim, with the
                           ending domain index of the subdomain.
      \param[in]   inipos  Pointer to an array of length dim, with the
                           new initial indices for the expanded subdomain.
      \param[in]   endpos  Pointer to an array of length dim, with the
                           new ending indices for the expanded subdomain
      \param[in]      Inc  Pointer to an array of length dim, with the
                           the value corresponding the direction of the
                           expanding. 1, for expanding below; 2, for
                           expanding above; 3 expanding both.
      \param[in] dim       Dimension (2, 3 or 4) that was set by the user

      \return Returns an integer value with 1, if the corresponding
              subdomain has to be inquired for data; and 0 otherwise.

      \todo Check the description of parameter Inc.
*/
/*******************************************************************/
int DCT_OnNewBoundary( DCT_Integer *SDIniPos, DCT_Integer *SDEndPos ,
                       DCT_Integer *inipos, DCT_Integer *endpos, DCT_Integer *Inc,
                       int dim )
{
/* ---------------------------------------------  Local Variables  */

   register int  ii, jj, flag;
   
/* -----------------------------------  BEGIN( DCT_OnNewBoundary ) */
   flag = 0;
   for ( ii=0; (ii < dim)&&(!flag); ii++ ) {
      if ( ( (Inc[ii]&1) == 1 ) || ((Inc[ii]&4) == 4) ) {
         if ( (SDIniPos[ii] <= inipos[ii]) && (inipos[ii] <= SDEndPos[ii]) ) {
            flag = 1;
            for ( jj=0; jj < dim; jj++ ) {
               if ( (ii != jj) && ( ( (SDIniPos[jj] <= inipos[jj]) && 
                                      (SDEndPos[jj] <  inipos[jj])    ) ||
                                    ( (endpos[jj] <  SDIniPos[jj]) && 
                                      (endpos[jj] <= SDEndPos[jj])    )    ) ) {
                  flag = 0;
                  break;
               }
            }
         }
      }

      if ( ( (Inc[ii]&2) == 2 ) || ( (Inc[ii]&8) == 8) ) {
         if ( (SDIniPos[ii] <= endpos[ii]) && (endpos[ii] <= SDEndPos[ii]) ) {
            flag = 1;
            for ( jj=0; jj < dim; jj++ ) {
               if ( (ii != jj) && ( ( (SDIniPos[jj] <= inipos[jj]) && 
                                      (SDEndPos[jj] <  inipos[jj])    ) ||
                                    ( (endpos[jj] <  SDIniPos[jj]) && 
                                      (endpos[jj] <= SDEndPos[jj])    )    ) ) {
                  flag = 0;
                  break;
               }
            }
         }
      }
   }
   
   return( flag );

/* -------------------------------------  END( DCT_OnNewBoundary ) */
}

// /***************************************************************/
// /*                       DCT_CHeck_Dims                        */
// /*    Function checks that the initial position and the lenght */
// /*    are correct for a multidimensional array of dimension    */
// /*    dim and size of each dimension given in Dim              */
// /*  IniInd: Pointer of array with the initial indices in the   */
// /*          array to be inserted. Its size is dim              */
// /*  Length: Pointer of array with the number of elements in    */
// /*          the array to be inserted. Its size is dim          */
// /*  Dim:    Pointer of array with the number of elements in    */
// /*          every direction. Its lenght is dim                 */
// /*  dim:    Number of array dimensions                         */
// /***************************************************************/
// int DCT_CHeck_Dims( DCT_Integer *IniInd, DCT_Integer *Length, int *Dim, int dim )
// {
// /* ---------------------------------------------  Local Variables  */
// 
//    register int  ii;
//    
// /* --------------------------------------  BEGIN( DCT_CHeck_Dims ) */
// 
//    for ( ii = 0; ii < dim; ii++ ) {
//       if ( (IniInd[ii] + Length[ii]) > Dim[ii] ) return(ii+1);
//    }
//    
//    return( 0 );
// 
// /* ----------------------------------------  END( DCT_CHeck_Dims ) */
// 
// }
// 

/*******************************************************************/
/*                        DCT_Var_Node_Comp                        */
/*                                                                 */
/*! Function to compare two DCT_Var_Node structures, using
    the VarMsgTag as a key field.

    \param[in] node1  Pointer to a DCT_Var_Node structure
    \param[in] node2  Pointer to a DCT_Var_Node structure

    \return An integer value less than, equal to,
    or greater than 0; if node1's VarMsgTag is less than, equal
    to, or greater than node2's VarMsgTag.

*/
/*******************************************************************/
int DCT_Var_Node_Comp( const void *node1, const void *node2 )
{
/* ---------------------------------------------  Local Variables  */

   const DCT_Var_Node *n1 = (DCT_Var_Node *)node1;
   const DCT_Var_Node *n2 = (DCT_Var_Node *)node2;
   
/* -----------------------------------  BEGIN( DCT_Var_Node_Comp ) */

   return ((int)( n1->VarMsgTag - n2->VarMsgTag ));

/* -------------------------------------  END( DCT_Var_Node_Comp ) */
}

/*******************************************************************/
/*                          DCT_List_Add                           */
/*                                                                 */
/*!   Add a node at the end of a DCT_List.

      \param[in,out] plist  Pointer to the root node of the list.
      \param[in]       var  Pointer to the user data to stored in
                            the list.
      \param[in] data_type  Indicate the type of data of the user
                            data given in var.

      \return The address of the last list node.

      \attention Here the bifurcation is not required. And the root
                 node could be passed by value, instead of reference.

*/
/*******************************************************************/
DCT_List * DCT_List_Add( DCT_List **plist, DCT_VoidPointer var,
                         DCT_Object_Types data_type)
{
/* ---------------------------------------------  Local Variables  */
  DCT_List *element = *plist;

/* ----------------------------------------  BEGIN( DCT_List_Add ) */

  /* Why not is it used  element instead of *plist  */
  if ( (*plist) != (DCT_List *)NULL) {
    while ((*plist)->NextData != (DCT_List *)NULL)
      *plist = (*plist)->NextData;
    (*plist)->NextData = (DCT_List *) malloc(sizeof(DCT_List));
    *plist = (*plist)->NextData;
    (*plist)->NextData = (DCT_List *)NULL;
    (*plist)->AddressData = var;
    (*plist)->DataType = data_type;
    return (element);
  }
  else {
    *plist = (DCT_List *) malloc(sizeof(DCT_List));
    (*plist)->NextData = (DCT_List *)NULL;
    (*plist)->AddressData = var;
    (*plist)->DataType = data_type;
    return (*plist);
  }

/* ------------------------------------------  END( DCT_List_Add ) */
}

/*******************************************************************/
/*                        DCT_List_ExtVar                          */
/*                                                                 */
/*!   Fetch the current element of the DCT_List.

      \param[in]     plist  Pointer to the root node of the list.
      \param[out]  reclist  Pointer to the user data to stored the
                            list node information.
      \param[in]   vartype  Indicate the type of data of the user
                            data given in var.

*/
/*******************************************************************/
void DCT_List_ExtVar( DCT_List **plist, DCT_VoidPointer *reclist,
                                       DCT_Object_Types *vartype )
{

/* -------------------------------------  BEGIN( DCT_List_ExtVar ) */

  if ( *plist != (DCT_List *)NULL) {
    *vartype = (DCT_Object_Types) (*plist)->DataType;
    switch (*vartype) {
      case DCT_FIELD_TYPE:
        *reclist = (DCT_Field *) (*plist)->AddressData;
      break;
      case DCT_3D_VAR_TYPE:
        *reclist = (DCT_3d_Var *) (*plist)->AddressData;
      break;
      case DCT_4D_VAR_TYPE:
        *reclist = (DCT_4d_Var *) (*plist)->AddressData;
      break;
      case DCT_MODEL_TYPE:
        *reclist = (DCT_Model *) (*plist)->AddressData;
        break;
      case DCT_COUPLE_TYPE:
        *reclist = (DCT_Couple *) (*plist)->AddressData;
        break;
      case DCT_HCPLVAR_TYPE:
        *reclist = (DCT_Hollow_CPLVar *) (*plist)->AddressData;
        break;
      case DCT_CPL_NODE_TYPE:
        *reclist = (DCT_CPL_Node *) (*plist)->AddressData;
        break;
      default:
      {/*  ERROR MESSAGE MUST BE PLACED HERE */}
    }
    *plist = (*plist)->NextData;
  }

/* ---------------------------------------  END( DCT_List_ExtVar ) */
}

/*******************************************************************/
/*                          DCT_List_CHKAdd                        */
/*                                                                 */
/*!   Function that add a DCT structure (DCT_Couple, DCT_Model
      and DCT_{Vars}) only, if the name does not exist; but if
      the name is found return the node pointer.
      
      and return
      a NULL pointer. Return a pointer to the structure if it is
      found

      \param[in,out] plist  Pointer to the root node of the list.
      \param[in]       var  Pointer to a DCT structure to be added.
      \param[in] data_type  Indicate the type of data of the user
                            data given in var.

      \return The pointer null if the structure in var is not
              in the list; the pointer address the node that match
              with the name of the parameter.

*/
/*******************************************************************/
DCT_List * DCT_List_CHKAdd( DCT_List **plist, DCT_VoidPointer var,
                            DCT_Object_Types data_type)
{
/* ---------------------------------------------  Local Variables  */

   DCT_Field           *field;
   DCT_3d_Var          *var3d;
   DCT_4d_Var          *var4d;
   DCT_Model           *model;
   DCT_Couple          *couple;
   DCT_Hollow_CPLVar   *cplvar;
   DCT_Name             Name;
   DCT_Name             NameLst;

   DCT_Object_Types     vartype;
   DCT_List            *element = *plist;
   
   int nfound, nend;

/* -------------------------------------  BEGIN( DCT_List_CHKAdd ) */

   /* The var must be searched */
   if ( element != (DCT_List *)NULL) {
      /* Getting the name of the structure */
      switch (data_type) {
         case DCT_FIELD_TYPE:
           field = (DCT_Field *) var;
           Name = field->VarName;
         break;
         case DCT_3D_VAR_TYPE:
           var3d = (DCT_3d_Var *) var;
           Name = var3d->VarName;
         break;
         case DCT_4D_VAR_TYPE:
           var4d = (DCT_4d_Var *) var;
           Name = var4d->VarName;
         break;
         case DCT_MODEL_TYPE:
           model = (DCT_Model *) var;
           Name = model->ModName;
           break;
         case DCT_COUPLE_TYPE:
           couple = (DCT_Couple *) var;
           Name = couple->CoupleName;
           break;
         case DCT_HCPLVAR_TYPE:
           cplvar = (DCT_Hollow_CPLVar *) var;
           Name = cplvar->VarName;
           break;
         default:
           return(*plist); /* return the root address */
      }
      /* Sweping the list */
      nfound = 1; nend = 1;
      do {
         vartype = (DCT_Object_Types) element->DataType;
         switch (vartype) {
            case DCT_FIELD_TYPE:
              field = (DCT_Field *) element->AddressData;
              NameLst = field->VarName;
            break;
            case DCT_3D_VAR_TYPE:
              var3d = (DCT_3d_Var *) element->AddressData;
              NameLst = var3d->VarName;
            break;
            case DCT_4D_VAR_TYPE:
              var4d = (DCT_4d_Var *) element->AddressData;
              NameLst = var4d->VarName;
            break;
            case DCT_MODEL_TYPE:
              model = (DCT_Model *) element->AddressData;
              NameLst = model->ModName;
              break;
            case DCT_COUPLE_TYPE:
              couple = (DCT_Couple *) element->AddressData;
              NameLst = couple->CoupleName;
              break;
            case DCT_HCPLVAR_TYPE:
              cplvar = (DCT_Hollow_CPLVar *) element->AddressData;
              NameLst = cplvar->VarName;
         } 
         nfound = strcmp( NameLst, Name );
         if (nfound == 0) return( element );
         else if ( element->NextData == (DCT_List *) NULL ) nend = 0;
         else element = element->NextData;
      } while ( nfound && nend );
      
      element->NextData = (DCT_List *) malloc(sizeof(DCT_List));
      element = element->NextData;
      element->NextData = (DCT_List *)NULL;
      element->AddressData = var;
      element->DataType = data_type;
   }
   else {
      *plist = (DCT_List *) malloc(sizeof(DCT_List));
      (*plist)->NextData = (DCT_List *)NULL;
      (*plist)->AddressData = var;
      (*plist)->DataType = data_type;
   }
   return ( NULL );

/* ---------------------------------------  END( DCT_List_CHKAdd ) */
}

/*******************************************************************/
/*                  DCT_List_CHKAdd_VarsOnModel                    */
/*                                                                 */
/*!   Function that add a DCT_{Vars} only if the name does not
      exist or if it is the only pair DCT_{vars} and Model that
      the DCT_{Vars} belongs to.

      \param[in,out] plist  Pointer to the intial DCT_List pointer.
      \param[in]       var  Pointer to a DCT structure to be added.
      \param[in] data_type  Value of the DCT_Object type corresponding
                            with the type of var.
      \param[in]     model  Pointer to the DCT_Model structure that
                            the DCT_{Vars} belongs to.s

      \return A NULL pointer if the DCT_{Vars} is added or a
              pointer to the structure, if it is found.
*/
/*******************************************************************/
DCT_List * DCT_List_CHKAdd_VarsOnModel( DCT_List **plist, DCT_VoidPointer var,
                            DCT_Object_Types data_type, DCT_Model *model )
{
/* ---------------------------------------------  Local Variables  */

   DCT_Field           *field;
   DCT_3d_Var          *var3d;
   DCT_4d_Var          *var4d;
   DCT_Hollow_CPLVar   *cplvar;
   DCT_Name             modNLst;
   DCT_Name             Name;
   DCT_Name             NameLst;
   DCT_Name             NameMod;
   
   DCT_Object_Types     vartype;
   DCT_List            *element = *plist;
   
   int nfound, nend;

/* -------------------------  BEGIN( DCT_List_CHKAdd_VarsOnModel ) */

   NameMod = model->ModName;

   /* The var must be searched */
   if ( element != (DCT_List *)NULL) {
      /* Getting the name of the structure */
      switch (data_type) {
         case DCT_FIELD_TYPE:
           field = (DCT_Field *) var;
           Name = field->VarName;
         break;
         case DCT_3D_VAR_TYPE:
           var3d = (DCT_3d_Var *) var;
           Name = var3d->VarName;
         break;
         case DCT_4D_VAR_TYPE:
           var4d = (DCT_4d_Var *) var;
           Name = var4d->VarName;
         case DCT_HCPLVAR_TYPE:
           cplvar = (DCT_Hollow_CPLVar *) var;
           Name = cplvar->VarName;
           break;
//          break;
//          default:
//            return(NULL);
      }
      /* Sweping the list */
      nfound = 1; nend = 1;
      do {
         vartype = (DCT_Object_Types) element->DataType;
         switch (vartype) {
            case DCT_FIELD_TYPE:
              field = (DCT_Field *) element->AddressData;
              NameLst = field->VarName;
              modNLst = field->VarModel->ModName;
            break;
            case DCT_3D_VAR_TYPE:
              var3d = (DCT_3d_Var *) element->AddressData;
              NameLst = var3d->VarName;
              modNLst = var3d->VarModel->ModName;
            break;
            case DCT_4D_VAR_TYPE:
              var4d = (DCT_4d_Var *) element->AddressData;
              NameLst = var4d->VarName;
              modNLst = var4d->VarModel->ModName;
            case DCT_HCPLVAR_TYPE:
              cplvar = (DCT_Hollow_CPLVar *) element->AddressData;
              NameLst = cplvar->VarName;
              modNLst = cplvar->VarModel->ModName;
         }
         nfound = strcmp( NameLst, Name );
         if ( (nfound == 0) && (strcmp( modNLst, NameMod )==0) ) return( element );
         else if ( element->NextData == (DCT_List *) NULL ) nend = 0;
         else element = element->NextData;
      } while ( nfound && nend );
      
      element->NextData = (DCT_List *) malloc(sizeof(DCT_List));
      element = element->NextData;
      element->NextData = (DCT_List *)NULL;
      element->AddressData = var;
      element->DataType = data_type;
   }
   else {
      *plist = (DCT_List *) malloc(sizeof(DCT_List));
      (*plist)->NextData = (DCT_List *)NULL;
      (*plist)->AddressData = var;
      (*plist)->DataType = data_type;
   }
   return ( NULL );

/* ---------------------------  END( DCT_List_CHKAdd_VarsOnModel ) */
}

/*******************************************************************/
/*                          DCT_List_Nnode                         */
/*                                                                 */
/*!   Retrieve the address of the num-th node of the DCT_List
      plist.

      \param[in,out] plist  Pointer to the intial DCT_List pointer,
                            and return NULL if the DCT_List is shorter
                            than num elements or the num-th node address.
      \param[in]       num  The number of the element to be retrieved.

      \return An integer value with num if reach the num-th node,
              or less than num if the list is shorter.

*/
/*******************************************************************/
int DCT_List_Nnode (DCT_List **plist, int num )
{
/* ---------------------------------------------  Local Variables  */
  int ii;

/* --------------------------------------  BEGIN( DCT_List_Nnode ) */

  for ( ii=0; (ii < num) && (*plist != (DCT_List *)NULL); ii++ )
    *plist = (*plist)->NextData;

  return ( ii );

/* ----------------------------------------  END( DCT_List_Nnode ) */
}

/*******************************************************************/
/*                         DCT_List_Break_Nnode                    */
/*                                                                 */
/*!    Breaks or split the DCT_List after num nodes, nullifying
       the pointer to next, in position num, and returning the
       next node in plist

      \param[in,out] plist  Pointer to the intial DCT_List pointer,
                            and return the new initial node to the
                            split DCT_List.
      \param[in]       num  The number of the element where the
                            list split.

      \return An integer value with num if reach the num-th node,
              or less than num if the list is shorter.

*/
/*******************************************************************/
int DCT_List_Break_Nnode (DCT_List **plist, int num )
{
/* ---------------------------------------------  Local Variables  */
  int ii;
  DCT_List *aux;

/* --------------------------------  BEGIN( DCT_List_Break_Nnode ) */

  for ( ii=0; (ii < num) && (*plist != (DCT_List *)NULL); ii++ ) {
    aux = *plist;
    *plist = (*plist)->NextData;
  }
  
  if ( ii == num ) aux->NextData = (DCT_List *)NULL;

  return ( ii );

/* ----------------------------------  END( DCT_List_Break_Nnode ) */
}

/*******************************************************************/
/*                           DCT_List_Resume                       */
/*                                                                 */
/*!    Count the number of the nodes in a DCT_List.

      \param[in,out] plist  Pointer to the intial DCT_List pointer.

      \return An integer value with the number of the nodes in
              the DCT_List.

*/
/*******************************************************************/
DCT_Integer DCT_List_Resume (DCT_List *plist)
{
/* ---------------------------------------------  Local Variables  */
  int counter;

/* -------------------------------------  BEGIN( DCT_List_Resume ) */

  /* It's wrong change the root address. */
  DCT_List *element = plist;
  counter = 0;
  while ( element != (DCT_List *)NULL) {
    counter++;
    element = element->NextData;
  }
  return (counter);

/* ---------------------------------------  END( DCT_List_Resume ) */
}

/*******************************************************************/
/*                            DCT_List_Ext                         */
/*                                                                 */
/*!    Count the number of the nodes in a DCT_List.

      \param[in]    plist  Pointer to a node of a DCT_List.
      \param[out] reclist  Pointer to the element stored in the
                           list node, depending of the kind of
                           DCT_Object_Types.

*/
/*******************************************************************/
void DCT_List_Ext (DCT_List **plist, DCT_VoidPointer *reclist)
{

/* ----------------------------------------  BEGIN( DCT_List_Ext ) */

  if ( *plist != (DCT_List *)NULL) {
    DCT_Object_Types vartype = (DCT_Object_Types) (*plist)->DataType;
    switch (vartype) {
      case DCT_FIELD_TYPE:
        *reclist = (DCT_Field *) (*plist)->AddressData;
      break;
      case DCT_3D_VAR_TYPE:
        *reclist = (DCT_3d_Var *) (*plist)->AddressData;
      break;
      case DCT_4D_VAR_TYPE:
        *reclist = (DCT_4d_Var *) (*plist)->AddressData;
      break;
      case DCT_MODEL_TYPE:
        *reclist = (DCT_Model *) (*plist)->AddressData;
        break;
      case DCT_COUPLE_TYPE:
        *reclist = (DCT_Couple *) (*plist)->AddressData;
        break;
      case DCT_HCPLVAR_TYPE:
        *reclist = (DCT_Hollow_CPLVar *) (*plist)->AddressData;
        break;
      case DCT_CPL_NODE_TYPE:
        *reclist = (DCT_CPL_Node *) (*plist)->AddressData;
        break;
      default:
      {
         DCTERROR( (vartype==0) || vartype,
                   "Error in the DCT_Object_Types value vartype", return );
      }
    }
    *plist = (*plist)->NextData;
  }

/* ------------------------------------------  END( DCT_List_Ext ) */
}

/*******************************************************************/
/*                            DCT_List_Ext                         */
/*                                                                 */
/*!    Delete a nodes in a DCT_List, and return the next node.

      \param[in]    plist  Pointer to a node of a DCT_List.

      \return A pointer to a DCT_List node corresponding to
              next one to that passed in plist.

*/
/*******************************************************************/
DCT_List *DCT_List_Del( DCT_List **plist )
{
/* ---------------------------------------------  Local Variables  */
  DCT_List *ptemp;

/* ----------------------------------------  BEGIN( DCT_List_Del ) */

  ptemp = (*plist)->NextData;
  free (*plist);
  return (ptemp);

/* ------------------------------------------  END( DCT_List_Del ) */
}

/*******************************************************************/
/*                          DCT_List_Clear                         */
/*                                                                 */
/*!    Delete completely a DCT_List structure.

      \param[in]    plist  Pointer to a node of a DCT_List.

*/
/*******************************************************************/
void DCT_List_Clear (DCT_List **plist)
{

/* --------------------------------------  BEGIN( DCT_List_Clear ) */

  while ( *plist != (DCT_List *)NULL )
    *plist = (DCT_List *) DCT_List_Del (plist);

/* ----------------------------------------  END( DCT_List_Clear ) */
}

/*******************************************************************/
/*                    DCT_fBiLinIntSparseProd                      */
/*                                                                 */
/*!    Calculates the matrix product used in the bilinear
       interpolation using the sparse structures for the weights
       using float precision. The interpolation goes from Mod1
       into Mod2.
       
       \param[in] Mod1DataType  Data type (float, double, long
                                double) of the producer model
                                (Mod1).
       \param[in]    Mod1npts2  Number of points along y-direction
                                of the producer (Mod1 discretization).
       \param[in]    Mod2npts1  Number of points along x-direction
                                of the consumer (Mod2 discretization).
       \param[in]    Mod2npts2  Number of points along y-direction
                                of the consumer (Mod2 discretization).
       \param[in]          Wx1  Array of the weights calculated along
                                x-direction (t weights).
       \param[in]          Wx2  Array of the weights calculated along
                                y-direction (u weights).
       \param[in]       Windx1  Array of the indices along x-direction
                                where the interpolated data is in
                                between.
       \param[in]       Windx2  Array of the indices along y-direction
                                where the interpolated data is in
                                between.
       \param[in]        Mod1Y  Array of the actual values of the
                                producer (Mod1) variable.
       \param[out]       Mod2Y  Array of the resulting interpolated
                                values to the consumer (Mod2).

      \return Integer values with 0 if finish successfully,
              otherwise if it fails.

*/
/*******************************************************************/
int DCT_fBiLinIntSparseProd ( const DCT_Data_Types Mod1DataType, const DCT_Integer Mod1npts2,
                              const DCT_Integer Mod2npts1, const DCT_Integer Mod2npts2,
                              const float *Wx1, const float *Wx2, const DCT_Integer *Windx1,
                              const DCT_Integer *Windx2, const DCT_VoidPointer Mod1Y,
                              float *Mod2Y)
                        /*  ( const DCT_Integer Mod1npts2, const DCT_VoidPointer Mod1Y,
                              const float *Wx1, const DCT_Integer *Windx1,
                              const DCT_Integer Mod2npts1, const DCT_Integer Mod2npts2,
                              float *Mod2Y, const float *Wx2, const DCT_Integer *Windx2,
                              const DCT_Data_Types Mod1DataType ) */
{
/* ---------------------------------------------  Local Variables  */
  int  ierr = 0;
  float t, oneminust, u;
  DCT_Integer ii, jj, ind, indi;

  float *fMod1Y, *foffset, *foffset2;
  double *dMod1Y, *doffset, *doffset2;
  long double *ldMod1Y, *ldoffset, *ldoffset2;
  
  float *dum, *current;
  
/* -----------------------------  BEGIN( DCT_fBiLinIntSparseProd ) */

   /* auxiliary array to perform the matrices product without a matrix */
   dum = (float *) malloc( sizeof(float) * (size_t) Mod1npts2 );
   DCTERROR( dum == (float *) NULL, "Memory Allocation Failed", return(1));
   
   /* Matrices product F_{int} = W^{(x)} F_o W^{(y)} without use any
     intermediate matrix */
   if (Mod1DataType == DCT_FLOAT ) {
      fMod1Y = (float *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Product of i-th row of W^{(X)} by F_o  */
         t = *(Wx1 + ii);
         oneminust = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Mod1npts2;         /* i-th row */
         //indj = (ind + 1)*Mod1npts2;   /* (i+1)-th row */
         foffset = fMod1Y + indi;
         foffset2 = foffset + Mod1npts2;
         for (jj = 0; jj < Mod1npts2 ; jj++ ) {
            *(dum + jj) = oneminust * *(foffset + jj) + t * *(foffset2 + jj);
         }
         
         /* Prevoius Product by W^{(y)}, giving the i-th row of F_{int} */
         foffset = Mod2Y + ii*Mod2npts2;  /* i-th row */
         for (jj = 0; jj < Mod2npts2; jj++ ){
            u = *(Wx2 + jj);
            ind = *(Windx2 + jj);
            current = dum + ind;
            *(foffset + jj) = (1.0 - u) * *(current) + u * *(current + 1);
         }
      }
   
   } else if (Mod1DataType == DCT_DOUBLE ) {
      dMod1Y = (double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Product of i-th row of W^{(X)} by F_o  */
         t = *(Wx1 + ii);
         oneminust = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Mod1npts2;         /* i-th row */
         //indj = indi + Mod1npts2;   /* (i+1)-th row */
         doffset = dMod1Y + indi;
         doffset2 = doffset + Mod1npts2;
         for (jj = 0; jj < Mod1npts2 ; jj++ ) {
            *(dum + jj) = oneminust * (float)*(doffset + jj) +
                                  t * (float)*(doffset2 + jj);
         }
         
         /* Prevoius Product by W^{(y)}, giving the i-th row of F_{int} */
         foffset = Mod2Y + ii*Mod2npts2; /* i-th row */
         for (jj = 0; jj < Mod2npts2; jj++ ){
            u = *(Wx2 + jj);
            ind = *(Windx2 + jj);
            current = dum + ind;
            *(foffset + jj) = (1.0 - u) * *(current) + u * *(current + 1);
         }
      }
   
   }  else if (Mod1DataType == DCT_LONG_DOUBLE ) {
      ldMod1Y = (long double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Product of i-th row of W^{(X)} by F_o  */
         t = *(Wx1 + ii);
         oneminust = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Mod1npts2;         /* i-th row */
         //indj = indi + Mod1npts2;   /* (i+1)-th row */
         ldoffset = ldMod1Y + indi;
         ldoffset2 = ldoffset + Mod1npts2;
         for (jj = 0; jj < Mod1npts2 ; jj++ ) {
            *(dum + jj) = oneminust * (float)*(ldoffset + jj) +
                                  t * (float)*(ldoffset2 + jj);
         }
         
         /* Prevoius Product by W^{(y)}, giving the i-th row of F_{int} */
         foffset = Mod2Y + ii*Mod2npts2; /* i-th row */
         for (jj = 0; jj < Mod2npts2; jj++ ){
            u = *(Wx2 + jj);
            ind = *(Windx2 + jj);
            current = dum + ind;
            *(foffset + jj) = (1.0 - u) * *(current) + u * *(current + 1);
         }
      }
   
   }
  
#ifdef MEMTRK
   TAU_TRACK_MEMORY_HERE();
#endif
  
   free( dum );
   
   return(ierr);
  
/* -------------------------------  END( DCT_fBiLinIntSparseProd ) */
}

/*******************************************************************/
/*                      DCT_dBiLinIntSparseProd                    */
/*                                                                 */
/*!    Calculates the matrix product used in the bilinear
       interpolation using the sparse structures for the weights
       using double precision. The interpolation goes from Mod1
       into Mod2.
       
       \param[in] Mod1DataType  Data type (float, double, long
                                double) of the producer model
                                (Mod1).
       \param[in]    Mod1npts2  Number of points along y-direction
                                of the producer (Mod1 discretization).
       \param[in]    Mod2npts1  Number of points along x-direction
                                of the consumer (Mod2 discretization).
       \param[in]    Mod2npts2  Number of points along y-direction
                                of the consumer (Mod2 discretization).
       \param[in]          Wx1  Array of the weights calculated along
                                x-direction (t weights).
       \param[in]          Wx2  Array of the weights calculated along
                                y-direction (u weights).
       \param[in]       Windx1  Array of the indices along x-direction
                                where the interpolated data is in
                                between.
       \param[in]       Windx2  Array of the indices along y-direction
                                where the interpolated data is in
                                between.
       \param[in]        Mod1Y  Array of the actual values of the
                                producer (Mod1) variable.
       \param[out]       Mod2Y  Array of the resulting interpolated
                                values to the consumer (Mod2).

      \return Integer values with 0 if finish successfully,
              otherwise if it fails.

*/
/*******************************************************************/
int DCT_dBiLinIntSparseProd ( const DCT_Data_Types Mod1DataType, const DCT_Integer Mod1npts2,
                              const DCT_Integer Mod2npts1, const DCT_Integer Mod2npts2,
                              const double *Wx1, const double *Wx2, const DCT_Integer *Windx1,
                              const DCT_Integer *Windx2, const DCT_VoidPointer Mod1Y,
                              double *Mod2Y)
                         /* ( const DCT_Integer Mod1npts2, const DCT_VoidPointer Mod1Y,
                              const double *Wx1, const DCT_Integer *Windx1,
                              const DCT_Integer Mod2npts1, const DCT_Integer Mod2npts2,
                              double *Mod2Y, const double *Wx2, const DCT_Integer *Windx2,
                              const DCT_Data_Types Mod1DataType ) */
{
/* ---------------------------------------------  Local Variables  */
  int  ierr = 0;
  double t, oneminust, u;
  DCT_Integer ii, jj, ind, indi;
  
  float *fMod1Y, *foffset, *foffset2;
  double *dMod1Y, *doffset, *doffset2;
  long double *ldMod1Y, *ldoffset, *ldoffset2;
  
  double *dum, *current;
  
/* -----------------------------  BEGIN( DCT_dBiLinIntSparseProd ) */

   /* auxiliary array to perform the matrices product without a matrix */
   dum = (double *)malloc(sizeof(double)*(size_t)Mod1npts2);
   DCTERROR( dum == (double *) NULL, "Memory Allocation Failed", return(1) );
   
   /* Matrices product F_{int} = W^{(x)} F_o W^{(y)} without use any
     intermediate matrix */
   if (Mod1DataType == DCT_FLOAT ) {
      fMod1Y = (float *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Product of i-th row of W^{(X)} by F_o  */
         t = *(Wx1 + ii);
         oneminust = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Mod1npts2;         /* i-th row */
         //indj = indi + Mod1npts2;   /* (i+1)-th row */
         foffset = fMod1Y + indi;
         foffset2 = foffset + Mod1npts2;
         for (jj = 0; jj < Mod1npts2 ; jj++ ) {
            *(dum + jj) = oneminust * (double)*(foffset + jj) +
                                  t * (double)*(foffset2 + jj);
         }
         
         /* Prevoius Product by W^{(y)}, giving the i-th row of F_{int} */
         doffset = Mod2Y + ii*Mod2npts2;  /* i-th row */
         for (jj = 0; jj < Mod2npts2; jj++ ){
            u = *(Wx2 + jj);
            ind = *(Windx2 + jj);
            current = dum + ind;
            *(doffset + jj) = (1.0 - u) * *(current) + u * *(current + 1);
         }
      }
      
   } else if (Mod1DataType == DCT_DOUBLE ) {
      dMod1Y = (double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Product of i-th row of W^{(X)} by F_o  */
         t = *(Wx1 + ii);
         oneminust = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Mod1npts2;         /* i-th row */
         //indj = indi + Mod1npts2;   /* (i+1)-th row */
         doffset = dMod1Y + indi;
         doffset2 = doffset + Mod1npts2;
         for (jj = 0; jj < Mod1npts2 ; jj++ ) {
            *(dum + jj) = oneminust * *(doffset + jj) +
                                  t * *(doffset2 + jj);
         }
         
         /* Prevoius Product by W^{(y)}, giving the i-th row of F_{int} */
         doffset = Mod2Y + ii*Mod2npts2; /* i-th row */
         for (jj = 0; jj < Mod2npts2; jj++ ){
            u = *(Wx2 + jj);
            ind = *(Windx2 + jj);
            current = dum + ind;
            *(doffset + jj) = (1.0 - u) * *(current) + u * *(current + 1);
         }
      }
      
   }  else if (Mod1DataType == DCT_LONG_DOUBLE ) {
      ldMod1Y = (long double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Product of i-th row of W^{(X)} by F_o  */
         t = *(Wx1 + ii);
         oneminust = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Mod1npts2;         /* i-th row */
         //indj = indi + Mod1npts2;   /* (i+1)-th row */
         ldoffset = ldMod1Y + indi;
         ldoffset2 = ldoffset + Mod1npts2;
         for (jj = 0; jj < Mod1npts2 ; jj++ ) {
            *(dum + jj) = oneminust * (double)*(ldoffset + jj) +
                                  t * (double)*(ldoffset2 + jj);
         }
         
         /* Prevoius Product by W^{(y)}, giving the i-th row of F_{int} */
         doffset = Mod2Y + ii*Mod2npts2; /* i-th row */
         for (jj = 0; jj < Mod2npts2; jj++ ){
            u = *(Wx2 + jj);
            ind = *(Windx2 + jj);
            current = dum + ind;
            *(doffset + jj) = (1.0 - u) * *(current) + u * *(current + 1);
         }
      }
      
   }
  
#ifdef MEMTRK
   TAU_TRACK_MEMORY_HERE();
#endif
  
   free( dum );
   
   return(ierr);
  
/* -------------------------------  END( DCT_dBiLinIntSparseProd ) */
}

/*******************************************************************/
/*                    DCT_ldBiLinIntSparseProd                     */
/*                                                                 */
/*!    Calculates the matrix product used in the bilinear
       interpolation using the sparse structures for the weights
       using long double precision. The interpolation goes from
       Mod1 into Mod2.
       
       \param[in] Mod1DataType  Data type (float, double, long
                                double) of the producer model
                                (Mod1).
       \param[in]    Mod1npts2  Number of points along y-direction
                                of the producer (Mod1 discretization).
       \param[in]    Mod2npts1  Number of points along x-direction
                                of the consumer (Mod2 discretization).
       \param[in]    Mod2npts2  Number of points along y-direction
                                of the consumer (Mod2 discretization).
       \param[in]          Wx1  Array of the weights calculated along
                                x-direction (t weights).
       \param[in]          Wx2  Array of the weights calculated along
                                y-direction (u weights).
       \param[in]       Windx1  Array of the indices along x-direction
                                where the interpolated data is in
                                between.
       \param[in]       Windx2  Array of the indices along y-direction
                                where the interpolated data is in
                                between.
       \param[in]        Mod1Y  Array of the actual values of the
                                producer (Mod1) variable.
       \param[out]       Mod2Y  Array of the resulting interpolated
                                values to the consumer (Mod2).

      \return Integer values with 0 if finish successfully,
              otherwise if it fails.

*/
/*******************************************************************/
int DCT_ldBiLinIntSparseProd ( const DCT_Data_Types Mod1DataType, const DCT_Integer Mod1npts2,
                               const DCT_Integer Mod2npts1, const DCT_Integer Mod2npts2,
                               const long double *Wx1, const long double *Wx2,
                               const DCT_Integer *Windx1, const DCT_Integer *Windx2,
                               const DCT_VoidPointer Mod1Y, long double *Mod2Y)
                         /*  (const DCT_Integer Mod1npts2, const DCT_VoidPointer Mod1Y,
                              const long double *Wx1, const DCT_Integer *Windx1,
                              const DCT_Integer Mod2npts1, const DCT_Integer Mod2npts2,
                              long double *Mod2Y, const long double *Wx2,
                              const DCT_Integer *Windx2, const DCT_Data_Types Mod1DataType ) */
{
/* ---------------------------------------------  Local Variables  */
  int  ierr = 0;
  long double t, oneminust, u;
  DCT_Integer ii, jj, ind, indi;
  
  float *fMod1Y, *foffset, *foffset2;
  double *dMod1Y, *doffset, *doffset2;
  long double *ldMod1Y, *ldoffset, *ldoffset2;
  
  long double *dum, *current;
  
/* ----------------------------  BEGIN( DCT_ldBiLinIntSparseProd ) */

   /* auxiliary array to perform the matrices product without a matrix */
   dum = (long double *)malloc(sizeof(long double)*(size_t)Mod1npts2);
   DCTERROR( dum == (long double *) NULL, "Memory Allocation Failed", return(ierr) );
   
   /* Matrices product F_{int} = W^{(x)} F_o W^{(y)} without use any
     intermediate matrix */
   if (Mod1DataType == DCT_FLOAT ) {
      fMod1Y = (float *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Product of i-th row of W^{(X)} by F_o  */
         t = *(Wx1 + ii);
         oneminust = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Mod1npts2;         /* i-th row */
         //indj = indi + Mod1npts2;   /* (i+1)-th row */
         foffset = fMod1Y + indi;
         foffset2 = foffset + Mod1npts2;
         for (jj = 0; jj < Mod1npts2 ; jj++ ) {
            *(dum + jj) = oneminust * (long double)*(foffset + jj) +
                                  t * (long double)*(foffset2 + jj);
         }
         
         /* Prevoius Product by W^{(y)}, giving the i-th row of F_{int} */
         ldoffset = Mod2Y + ii*Mod2npts2;  /* i-th row */
         for (jj = 0; jj < Mod2npts2; jj++ ){
            u = *(Wx2 + jj);
            ind = *(Windx2 + jj);
            current = dum + ind;
            *(ldoffset + jj) = (1.0 - u) * *(current) + u * *(current + 1);
         }
      }
      
   } else if (Mod1DataType == DCT_DOUBLE ) {
      dMod1Y = (double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Product of i-th row of W^{(X)} by F_o  */
         t = *(Wx1 + ii);
         oneminust = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Mod1npts2;         /* i-th row */
         //indj = indi + Mod1npts2;   /* (i+1)-th row */
         doffset = dMod1Y + indi;
         doffset2 = doffset + Mod1npts2;
         for (jj = 0; jj < Mod1npts2 ; jj++ ) {
            *(dum + jj) = oneminust * (long double)*(doffset + jj) +
                                  t * (long double)*(doffset2 + jj);
         }
         
         /* Prevoius Product by W^{(y)}, giving the i-th row of F_{int} */
         ldoffset = Mod2Y + ii*Mod2npts2;  /* i-th row */
         for (jj = 0; jj < Mod2npts2; jj++ ){
            u = *(Wx2 + jj);
            ind = *(Windx2 + jj);
            current = dum + ind;
            *(ldoffset + jj) = (1.0 - u) * *(current) + u * *(current + 1);
         }
      }
      
   }  else if (Mod1DataType == DCT_LONG_DOUBLE ) {
      ldMod1Y = (long double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Product of i-th row of W^{(X)} by F_o  */
         t = *(Wx1 + ii);
         oneminust = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Mod1npts2;         /* i-th row */
         //indj = indi + Mod1npts2;   /* (i+1)-th row */
         ldoffset = ldMod1Y + indi;
         ldoffset2 = ldoffset + Mod1npts2;
         for (jj = 0; jj < Mod1npts2 ; jj++ ) {
            *(dum + jj) = oneminust * *(ldoffset + jj) +
                                  t * *(ldoffset2 + jj);
         }
         
         /* Prevoius Product by W^{(y)}, giving the i-th row of F_{int} */
         ldoffset = Mod2Y + ii*Mod2npts2;  /* i-th row */
         for (jj = 0; jj < Mod2npts2; jj++ ){
            u = *(Wx2 + jj);
            ind = *(Windx2 + jj);
            current = dum + ind;
            *(ldoffset + jj) = (1.0 - u) * *(current) + u * *(current + 1);
         }
      }
      
   }
  
#ifdef MEMTRK
   TAU_TRACK_MEMORY_HERE();
#endif
  
   free( dum );
   
   return(ierr);
  
/* ------------------------------  END( DCT_ldBiLinIntSparseProd ) */
}

/*******************************************************************/
/*                   DCT_Msk_fBiLinIntSparseProd                   */
/*                                                                 */
/*!    Calculates the matrix product used in the bilinear
       interpolation using the sparse structures for the weights
       using float precision. The interpolation goes from Mod1
       into Mod2 and this function uses a Mask Array.
       
       \param[in] Mod1DataType  Data type (float, double, long
                                double) of the producer model
                                (Mod1).
       \param[in]    Mod1npts2  Number of points along y-direction
                                of the producer (Mod1 discretization).
       \param[in]    Mod2npts1  Number of points along x-direction
                                of the consumer (Mod2 discretization).
       \param[in]    Mod2npts2  Number of points along y-direction
                                of the consumer (Mod2 discretization).
       \param[in]          Wx1  Array of the weights calculated along
                                x-direction (t weights).
       \param[in]          Wx2  Array of the weights calculated along
                                y-direction (u weights).
       \param[in]       Windx1  Array of the indices along x-direction
                                where the interpolated data is in
                                between.
       \param[in]       Windx2  Array of the indices along y-direction
                                where the interpolated data is in
                                between.
       \param[in]        Mod1Y  Array of the actual values of the
                                producer (Mod1) variable.
       \param[in]      VarMask  Array of integer values with 0's or
                                1's to mask the consumer (Mod2)
                                variable, i.e. 
                                Mod2Y = mask(new value) + (1-mak)Mod2Y.
       \param[out]       Mod2Y  Array of the resulting interpolated
                                values to the consumer (Mod2).

      \return Integer values with 0 if finish successfully,
              otherwise if it fails.

*/
/*******************************************************************/
int DCT_Msk_fBiLinIntSparseProd ( const DCT_Data_Types Mod1DataType, const DCT_Integer Mod1npts2,
                                  const DCT_Integer Mod2npts1, const DCT_Integer Mod2npts2,
                                  const float *Wx1, const float *Wx2, const DCT_Integer *Windx1,
                                  const DCT_Integer *Windx2, const DCT_VoidPointer Mod1Y,
                                  const DCT_Integer *VarMask, float *Mod2Y)
                           /*  ( const DCT_Integer Mod1npts2, const DCT_VoidPointer Mod1Y,
                                 const float *Wx1, const DCT_Integer *Windx1,
                                 const DCT_Integer Mod2npts1, const DCT_Integer Mod2npts2,
                                 float *Mod2Y, const float *Wx2, const DCT_Integer *Windx2,
                                 const DCT_Integer *VarMask, const DCT_Data_Types Mod1DataType) */
{
/* ---------------------------------------------  Local Variables  */
  int  ierr = 0;
  float t, oneminust, u;
  DCT_Integer  ii, jj, ind, indi;
  DCT_Integer *mask;

  float *fMod1Y, *foffset, *foffset2, fmask;
  double *dMod1Y, *doffset, *doffset2;
  long double *ldMod1Y, *ldoffset, *ldoffset2;
  
  float *dum, *current;
  
/* -------------------------  BEGIN( DCT_Msk_fBiLinIntSparseProd ) */

   /* auxiliary array to perform the matrices product without a matrix */
   dum = (float *) malloc( sizeof(float) * (size_t) Mod1npts2 );
   DCTERROR( dum == (float *) NULL, "Memory Allocation Failed", return(1));
   
   /* Matrices product F_{int} = W^{(x)} F_o W^{(y)} without use any
     intermediate matrix */
   if (Mod1DataType == DCT_FLOAT ) {
      fMod1Y = (float *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Product of i-th row of W^{(X)} by F_o  */
         t = *(Wx1 + ii);
         oneminust = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Mod1npts2;         /* i-th row */
         //indj = (ind + 1)*Mod1npts2;   /* (i+1)-th row */
         foffset = fMod1Y + indi;
         foffset2 = foffset + Mod1npts2;
         for (jj = 0; jj < Mod1npts2 ; jj++ ) {
            *(dum + jj) = oneminust * *(foffset + jj) + t * *(foffset2 + jj);
         }
         
         /* Prevoius Product by W^{(y)}, giving the i-th row of F_{int} */
         indi = ii*Mod2npts2;
         foffset = Mod2Y + indi;  /* i-th row */
         mask = (DCT_Integer *)VarMask + indi;
         for (jj = 0; jj < Mod2npts2; jj++ ){
            u = *(Wx2 + jj);
            ind = *(Windx2 + jj);
            fmask = (float)*(mask + jj);
            current = dum + ind;
            *(foffset) = fmask*( (1.0 - u) * *(current) + u * *(current + 1) )  + 
                              (1.0 - fmask) * *(foffset);
            foffset++;
         }
      }
   
   } else if (Mod1DataType == DCT_DOUBLE ) {
      dMod1Y = (double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Product of i-th row of W^{(X)} by F_o  */
         t = *(Wx1 + ii);
         oneminust = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Mod1npts2;         /* i-th row */
         //indj = indi + Mod1npts2;   /* (i+1)-th row */
         doffset = dMod1Y + indi;
         doffset2 = doffset + Mod1npts2;
         for (jj = 0; jj < Mod1npts2 ; jj++ ) {
            *(dum + jj) = oneminust * (float)*(doffset + jj) +
                                  t * (float)*(doffset2 + jj);
         }
         
         /* Prevoius Product by W^{(y)}, giving the i-th row of F_{int} */
         indi = ii*Mod2npts2;
         foffset = Mod2Y + indi; /* i-th row */
         mask = (DCT_Integer *)VarMask + indi;
         for (jj = 0; jj < Mod2npts2; jj++ ){
            u = *(Wx2 + jj);
            ind = *(Windx2 + jj);
            fmask = (float)*(mask + jj);
            current = dum + ind;
            *(foffset) = fmask*( (1.0 - u) * *(current) + u * *(current + 1) )  + 
                              (1.0 - fmask) * *(foffset);
            foffset++;
         }
      }
   
   }  else if (Mod1DataType == DCT_LONG_DOUBLE ) {
      ldMod1Y = (long double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Product of i-th row of W^{(X)} by F_o  */
         t = *(Wx1 + ii);
         oneminust = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Mod1npts2;         /* i-th row */
         //indj = indi + Mod1npts2;   /* (i+1)-th row */
         ldoffset = ldMod1Y + indi;
         ldoffset2 = ldoffset + Mod1npts2;
         for (jj = 0; jj < Mod1npts2 ; jj++ ) {
            *(dum + jj) = oneminust * (float)*(ldoffset + jj) +
                                  t * (float)*(ldoffset2 + jj);
         }
         
         /* Prevoius Product by W^{(y)}, giving the i-th row of F_{int} */
         indi = ii*Mod2npts2;
         foffset = Mod2Y + indi; /* i-th row */
         mask = (DCT_Integer *)VarMask + indi;
         for (jj = 0; jj < Mod2npts2; jj++ ){
            u = *(Wx2 + jj);
            ind = *(Windx2 + jj);
            fmask = (float)*(mask + jj);
            current = dum + ind;
            *(foffset) = fmask*( (1.0 - u) * *(current) + u * *(current + 1) )  + 
                              (1.0 - fmask) * *(foffset);
            foffset++;
         }
      }
   
   }
  
#ifdef MEMTRK
   TAU_TRACK_MEMORY_HERE();
#endif
  
   free( dum );
   
   return(ierr);
  
/* ---------------------------  END( DCT_Msk_fBiLinIntSparseProd ) */
}

/*******************************************************************/
/*                   DCT_Msk_dBiLinIntSparseProd                   */
/*                                                                 */
/*!    Calculates the matrix product used in the bilinear
       interpolation using the sparse structures for the weights
       using double precision. The interpolation goes from Mod1
       into Mod2 and this function uses a Mask Array.
       
       \param[in] Mod1DataType  Data type (float, double, long
                                double) of the producer model
                                (Mod1).
       \param[in]    Mod1npts2  Number of points along y-direction
                                of the producer (Mod1 discretization).
       \param[in]    Mod2npts1  Number of points along x-direction
                                of the consumer (Mod2 discretization).
       \param[in]    Mod2npts2  Number of points along y-direction
                                of the consumer (Mod2 discretization).
       \param[in]          Wx1  Array of the weights calculated along
                                x-direction (t weights).
       \param[in]          Wx2  Array of the weights calculated along
                                y-direction (u weights).
       \param[in]       Windx1  Array of the indices along x-direction
                                where the interpolated data is in
                                between.
       \param[in]       Windx2  Array of the indices along y-direction
                                where the interpolated data is in
                                between.
       \param[in]        Mod1Y  Array of the actual values of the
                                producer (Mod1) variable.
       \param[in]      VarMask  Array of integer values with 0's or
                                1's to mask the consumer (Mod2)
                                variable, i.e. 
                                Mod2Y = mask(new value) + (1-mak)Mod2Y.
       \param[out]       Mod2Y  Array of the resulting interpolated
                                values to the consumer (Mod2).

      \return Integer values with 0 if finish successfully,
              otherwise if it fails.

*/
/*******************************************************************/
int DCT_Msk_dBiLinIntSparseProd ( const DCT_Data_Types Mod1DataType, const DCT_Integer Mod1npts2,
                                  const DCT_Integer Mod2npts1, const DCT_Integer Mod2npts2,
                                  const double *Wx1, const double *Wx2, const DCT_Integer *Windx1,
                                  const DCT_Integer *Windx2, const DCT_VoidPointer Mod1Y,
                                  const DCT_Integer *VarMask, double *Mod2Y)
                           /*  ( const DCT_Integer Mod1npts2, const DCT_VoidPointer Mod1Y,
                                  const double *Wx1, const DCT_Integer *Windx1,
                                  const DCT_Integer Mod2npts1, const DCT_Integer Mod2npts2,
                                  double *Mod2Y, const double *Wx2, const DCT_Integer *Windx2,
                                  const DCT_Integer *VarMask, const DCT_Data_Types Mod1DataType)*/
{
/* ---------------------------------------------  Local Variables  */
  int  ierr = 0;
  double t, oneminust, u;
  DCT_Integer ii, jj, ind, indi;
  DCT_Integer *mask;
  
  float *fMod1Y, *foffset, *foffset2;
  double *dMod1Y, *doffset, *doffset2, dmask;
  long double *ldMod1Y, *ldoffset, *ldoffset2;
  
  double *dum, *current;
  
/* -------------------------  BEGIN( DCT_Msk_dBiLinIntSparseProd ) */

   /* auxiliary array to perform the matrices product without a matrix */
   dum = (double *)malloc(sizeof(double)*(size_t)Mod1npts2);
   DCTERROR( dum == (double *) NULL, "Memory Allocation Failed", return(1) );
   
   /* Matrices product F_{int} = W^{(x)} F_o W^{(y)} without use any
     intermediate matrix */
   if (Mod1DataType == DCT_FLOAT ) {
      fMod1Y = (float *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Product of i-th row of W^{(X)} by F_o  */
         t = *(Wx1 + ii);
         oneminust = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Mod1npts2;         /* i-th row */
         //indj = indi + Mod1npts2;   /* (i+1)-th row */
         foffset = fMod1Y + indi;
         foffset2 = foffset + Mod1npts2;
         for (jj = 0; jj < Mod1npts2 ; jj++ ) {
            *(dum + jj) = oneminust * (double)*(foffset + jj) +
                                  t * (double)*(foffset2 + jj);
         }
         
         /* Prevoius Product by W^{(y)}, giving the i-th row of F_{int} */
         indi = ii*Mod2npts2;
         doffset = Mod2Y + indi;  /* i-th row */
         mask = (DCT_Integer *)VarMask + indi;
         for (jj = 0; jj < Mod2npts2; jj++ ){
            u = *(Wx2 + jj);
            ind = *(Windx2 + jj);
            dmask = (double)*(mask + jj);
            current = dum + ind;
            *(doffset) = dmask*( (1.0 - u) * *(current) + u * *(current + 1) ) + 
                         (1.0 - dmask) * *(doffset);
            doffset++;
         }
      }
      
   } else if (Mod1DataType == DCT_DOUBLE ) {
      dMod1Y = (double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Product of i-th row of W^{(X)} by F_o  */
         t = *(Wx1 + ii);
         oneminust = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Mod1npts2;         /* i-th row */
         //indj = indi + Mod1npts2;   /* (i+1)-th row */
         doffset = dMod1Y + indi;
         doffset2 = doffset + Mod1npts2;
         for (jj = 0; jj < Mod1npts2 ; jj++ ) {
            *(dum + jj) = oneminust * *(doffset + jj) +
                                  t * *(doffset2 + jj);
         }
         
         /* Prevoius Product by W^{(y)}, giving the i-th row of F_{int} */
         indi = ii*Mod2npts2;
         doffset = Mod2Y + indi;  /* i-th row */
         mask = (DCT_Integer *)VarMask + indi;
         for (jj = 0; jj < Mod2npts2; jj++ ){
            u = *(Wx2 + jj);
            ind = *(Windx2 + jj);
            dmask = (double)*(mask + jj);
            current = dum + ind;
            *(doffset) = dmask*( (1.0 - u) * *(current) + u * *(current + 1) ) +
                         (1.0 - dmask) * *(doffset);
            doffset++;
         }
      }
      
   }  else if (Mod1DataType == DCT_LONG_DOUBLE ) {
      ldMod1Y = (long double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Product of i-th row of W^{(X)} by F_o  */
         t = *(Wx1 + ii);
         oneminust = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Mod1npts2;         /* i-th row */
         //indj = indi + Mod1npts2;   /* (i+1)-th row */
         ldoffset = ldMod1Y + indi;
         ldoffset2 = ldoffset + Mod1npts2;
         for (jj = 0; jj < Mod1npts2 ; jj++ ) {
            *(dum + jj) = oneminust * (double)*(ldoffset + jj) +
                                  t * (double)*(ldoffset2 + jj);
         }
         
         /* Prevoius Product by W^{(y)}, giving the i-th row of F_{int} */
         indi = ii*Mod2npts2;
         doffset = Mod2Y + indi;  /* i-th row */
         mask = (DCT_Integer *)VarMask + indi;
         for (jj = 0; jj < Mod2npts2; jj++ ){
            u = *(Wx2 + jj);
            ind = *(Windx2 + jj);
            dmask = (double)*(mask + jj);
            current = dum + ind;
            *(doffset) = dmask*( (1.0 - u) * *(current) + u * *(current + 1) ) +
                         (1.0 - dmask) * *(doffset);
            doffset++;
         }
      }
      
   }
  
#ifdef MEMTRK
   TAU_TRACK_MEMORY_HERE();
#endif
  
   free( dum );
   
   return(ierr);
  
/* ---------------------------  END( DCT_Msk_dBiLinIntSparseProd ) */
}

/*******************************************************************/
/*                    DCT_ldBiLinIntSparseProd                     */
/*                                                                 */
/*!    Calculates the matrix product used in the bilinear
       interpolation using the sparse structures for the weights
       using long double precision. The interpolation goes from
       Mod1 into Mod2 and this function uses a Mask Array.
       
       \param[in] Mod1DataType  Data type (float, double, long
                                double) of the producer model
                                (Mod1).
       \param[in]    Mod1npts2  Number of points along y-direction
                                of the producer (Mod1 discretization).
       \param[in]    Mod2npts1  Number of points along x-direction
                                of the consumer (Mod2 discretization).
       \param[in]    Mod2npts2  Number of points along y-direction
                                of the consumer (Mod2 discretization).
       \param[in]          Wx1  Array of the weights calculated along
                                x-direction (t weights).
       \param[in]          Wx2  Array of the weights calculated along
                                y-direction (u weights).
       \param[in]       Windx1  Array of the indices along x-direction
                                where the interpolated data is in
                                between.
       \param[in]       Windx2  Array of the indices along y-direction
                                where the interpolated data is in
                                between.
       \param[in]        Mod1Y  Array of the actual values of the
                                producer (Mod1) variable.
       \param[in]      VarMask  Array of integer values with 0's or
                                1's to mask the consumer (Mod2)
                                variable, i.e. 
                                Mod2Y = mask(new value) + (1-mak)Mod2Y.
       \param[out]       Mod2Y  Array of the resulting interpolated
                                values to the consumer (Mod2).

      \return Integer values with 0 if finish successfully,
              otherwise if it fails.

*/
/*******************************************************************/
int DCT_Msk_ldBiLinIntSparseProd ( const DCT_Data_Types Mod1DataType, const DCT_Integer Mod1npts2,
                                   const DCT_Integer Mod2npts1, const DCT_Integer Mod2npts2,
                                   const long double *Wx1, const long double *Wx2,
                                   const DCT_Integer *Windx1, const DCT_Integer *Windx2,
                                   const DCT_VoidPointer Mod1Y, const DCT_Integer *VarMask,
                                   long double *Mod2Y)
                             /*  (const DCT_Integer Mod1npts2, const DCT_VoidPointer Mod1Y,
                                  const long double *Wx1, const DCT_Integer *Windx1,
                                  const DCT_Integer Mod2npts1, const DCT_Integer Mod2npts2,
                                  long double *Mod2Y, const long double *Wx2,
                                  const DCT_Integer *Windx2, const DCT_Integer *VarMask, 
                                  const DCT_Data_Types Mod1DataType ) */
{
/* ---------------------------------------------  Local Variables  */
  int  ierr = 0;
  long double t, oneminust, u;
  DCT_Integer ii, jj, ind, indi;
  DCT_Integer *mask;
  
  float *fMod1Y, *foffset, *foffset2;
  double *dMod1Y, *doffset, *doffset2;
  long double *ldMod1Y, *ldoffset, *ldoffset2, ldmask;
  
  long double *dum, *current;
  
/* ------------------------  BEGIN( DCT_Msk_ldBiLinIntSparseProd ) */

   /* auxiliary array to perform the matrices product without a matrix */
   dum = (long double *)malloc(sizeof(long double)*(size_t)Mod1npts2);
   DCTERROR( dum == (long double *) NULL, "Memory Allocation Failed", return(ierr) );
   
   /* Matrices product F_{int} = W^{(x)} F_o W^{(y)} without use any
     intermediate matrix */
   if (Mod1DataType == DCT_FLOAT ) {
      fMod1Y = (float *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Product of i-th row of W^{(X)} by F_o  */
         t = *(Wx1 + ii);
         oneminust = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Mod1npts2;         /* i-th row */
         //indj = indi + Mod1npts2;   /* (i+1)-th row */
         foffset = fMod1Y + indi;
         foffset2 = foffset + Mod1npts2;
         for (jj = 0; jj < Mod1npts2 ; jj++ ) {
            *(dum + jj) = oneminust * (long double)*(foffset + jj) +
                                  t * (long double)*(foffset2 + jj);
         }
         
         /* Prevoius Product by W^{(y)}, giving the i-th row of F_{int} */
         indi = ii*Mod2npts2;
         ldoffset = Mod2Y + indi;  /* i-th row */
         mask = (DCT_Integer *)VarMask + indi;
         for (jj = 0; jj < Mod2npts2; jj++ ){
            u = *(Wx2 + jj);
            ind = *(Windx2 + jj);
            ldmask = (long double)*(mask + jj);
            current = dum + ind;
            *(ldoffset) = ldmask*( (1.0 - u) * *(current) + u * *(current + 1) ) + 
                         (1.0 - ldmask) * *(ldoffset);
            ldoffset++;
         }
      }
      
   } else if (Mod1DataType == DCT_DOUBLE ) {
      dMod1Y = (double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Product of i-th row of W^{(X)} by F_o  */
         t = *(Wx1 + ii);
         oneminust = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Mod1npts2;         /* i-th row */
         //indj = indi + Mod1npts2;   /* (i+1)-th row */
         doffset = dMod1Y + indi;
         doffset2 = doffset + Mod1npts2;
         for (jj = 0; jj < Mod1npts2 ; jj++ ) {
            *(dum + jj) = oneminust * (long double)*(doffset + jj) +
                                  t * (long double)*(doffset2 + jj);
         }
         
         /* Prevoius Product by W^{(y)}, giving the i-th row of F_{int} */
         indi = ii*Mod2npts2;
         ldoffset = Mod2Y + indi;  /* i-th row */
         mask = (DCT_Integer *)VarMask + indi;
         for (jj = 0; jj < Mod2npts2; jj++ ){
            u = *(Wx2 + jj);
            ind = *(Windx2 + jj);
            ldmask = (long double)*(mask + jj);
            current = dum + ind;
            *(ldoffset) = ldmask*( (1.0 - u) * *(current) + u * *(current + 1) ) + 
                         (1.0 - ldmask) * *(ldoffset);
            ldoffset++;
         }
      }
      
   }  else if (Mod1DataType == DCT_LONG_DOUBLE ) {
      ldMod1Y = (long double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Product of i-th row of W^{(X)} by F_o  */
         t = *(Wx1 + ii);
         oneminust = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Mod1npts2;         /* i-th row */
         //indj = indi + Mod1npts2;   /* (i+1)-th row */
         ldoffset = ldMod1Y + indi;
         ldoffset2 = ldoffset + Mod1npts2;
         for (jj = 0; jj < Mod1npts2 ; jj++ ) {
            *(dum + jj) = oneminust * *(ldoffset + jj) +
                                  t * *(ldoffset2 + jj);
         }
         
         /* Prevoius Product by W^{(y)}, giving the i-th row of F_{int} */
         indi = ii*Mod2npts2;
         ldoffset = Mod2Y + indi;  /* i-th row */
         mask = (DCT_Integer *)VarMask + indi;
         for (jj = 0; jj < Mod2npts2; jj++ ){
            u = *(Wx2 + jj);
            ind = *(Windx2 + jj);
            ldmask = (long double)*(mask + jj);
            current = dum + ind;
            *(ldoffset) = ldmask*( (1.0 - u) * *(current) + u * *(current + 1) ) + 
                         (1.0 - ldmask) * *(ldoffset);
            ldoffset++;
         }
      }
      
   }
  
#ifdef MEMTRK
   TAU_TRACK_MEMORY_HERE();
#endif
  
   free( dum );
   
   return(ierr);
  
/* --------------------------  END( DCT_Msk_ldBiLinIntSparseProd ) */
}

/***************************************************************/
/*                     DCT_RectilinearWeight                   */
/*                                                             */
/*!   Calculates the weights of the linear interpolation of a
      rectilinear mesh in both models for a given axis.

      \paran[in]   Mod1label1: Producer model tick marks.
      \paran[in]    Mod1npts1: Number of producer tick marks.
      \paran[in]   Mod2label1: Consumer model tick marks.
      \paran[in]    Mod2npts1: Number of consumer tick marks.
      \paran[out]         Wx1: Array of the corresponding linear
                               interpolation weights with Mod2npts1
                               entries.
      \paran[out]      Windx1: Array of the corresponding linear
                               interpolation indices with Mod2npts1
                               entries.
      \paran[in] Mod2DataType: Consumer model data type precision.

      \return An integer error control value with 0, if success.

*/
/***************************************************************/
int DCT_RectilinearWeight (const DCT_Scalar *Mod1label1,
                 const DCT_Integer Mod1npts1, const DCT_Scalar *Mod2label1,
                 const DCT_Integer Mod2npts1, DCT_VoidPointer Wx1,
                 DCT_Integer *Windx1, const DCT_Data_Types Mod2DataType )
{
/* ---------------------------------------------  Local Variables  */
  int  ierr=0;
  DCT_Scalar valsup, current; 
  float *fWx, fh, invfh, ft, flowv;
  double dh, invdh, dt, dlowv, *dWx;
  long double ldh, invldh, ldt, ldlowv, *ldWx;
  DCT_Integer ii, jj;

/* -------------------------------  BEGIN( DCT_RectilinearWeight ) */

  if (Mod2DataType == DCT_FLOAT ) {
    /** Verify that first data values cover the interpalated values **/
    current = *Mod2label1;
    valsup  = *Mod1label1;
    if ( valsup > current ) return( 1 );
    /** Takes the first values above the values to be interpolated **/
    for ( jj = 0; (jj < Mod1npts1)&&( (valsup = *(Mod1label1 + jj)) <= current ) ; jj++ );

    jj--;
    flowv =  (float) *(Mod1label1 + jj);
    fWx   = (float *) Wx1;
    fh    = (float) valsup - flowv;
    invfh = 1.0/fh;
    for (ii = 0; ii < Mod2npts1; ii++) {
      current = *(Mod2label1 + jj);
      ft = (float) current - flowv;
      if (ft > fh) {
        for ( jj++;
              (jj < Mod1npts1) && ((valsup = *(Mod1label1 + jj)) < current );
              jj++ );
        jj--;
        flowv = (float) *(Mod1label1 + jj);
        ft    = (float) current - flowv;
        fh    = (float) valsup - flowv;
        invfh = 1.0/fh;
      }
      *(fWx + ii) = ft * invfh;
      *(Windx1 + ii) = jj;
    }
  } else if (Mod2DataType == DCT_DOUBLE ) {
    /** Verify that first data values cover the interpalated values **/
    current = *Mod2label1;
    valsup  = *Mod1label1;
    if ( valsup > current ) return( 1 );
    /** Takes the first values above the values to be interpolated **/
    for ( jj = 0; (jj < Mod1npts1)&&( (valsup = *(Mod1label1 + jj)) <= current ) ; jj++ );

    jj--;
    dlowv =  (double) *(Mod1label1 + jj);
    dWx   = (double *) Wx1;
    dh    = (double) valsup - dlowv;
    invdh = 1.0/dh;
    for (ii = 0; ii < Mod2npts1; ii++) {
      current = *(Mod2label1 + ii);
      dt = (double) current - dlowv;
      if (dt > dh) {
        for ( jj++;
              (jj < Mod1npts1) && ((valsup = *(Mod1label1 + jj)) < current );
              jj++ );
        jj--;
        dlowv = (double) *(Mod1label1 + jj);
        dt    = (double) current - dlowv;
        dh    = (double) valsup - dlowv;
        invdh = 1.0/dh;
      }
      *(dWx + ii) = dt * invdh;
      *(Windx1 + ii) = jj;
    }
  } else if (Mod2DataType == DCT_LONG_DOUBLE ) {
    /** Verify that first data values cover the interpalated values **/
    current = *Mod2label1;
    valsup  = *Mod1label1;
    if ( valsup > current ) return( 1 );
    /** Takes the first values above the values to be interpolated **/
    for ( jj = 0; (jj < Mod1npts1)&&( (valsup = *(Mod1label1 + jj)) <= current ) ; jj++ );

    jj--;
    ldlowv =  (long double) *(Mod1label1 + jj);
    ldWx   = (long double *) Wx1;
    ldh    = (long double) valsup - ldlowv;
    invldh = 1.0/ldh;
    for (ii = 0; ii < Mod2npts1; ii++) {
      current = *(Mod2label1 + ii);
      ldt = (long double) current - ldlowv;
      if (ldt > ldh) {
        for ( jj++;
              (jj < Mod1npts1) && ((valsup = *(Mod1label1 + jj)) < current );
              jj++ );
        jj--;
        ldlowv = (long double) *(Mod1label1 + jj);
        ldt    = (long double) current - ldlowv;
        ldh    = (long double) valsup - ldlowv;
        invldh = 1.0/ldh;
      }
      *(ldWx + ii) = ldt * invldh;
      *(Windx1 + ii) = jj;
    }
  }

  return(ierr);
/* ---------------------------------  END( DCT_RectilinearWeight ) */
}

/***************************************************************/
/*                  DCT_Mod1CartesianWeight                    */
/*                                                             */
/*!   Calculates the weights of the linear interpolation of a
      cartesian mesh in the producer model and rectilinear in
      the consumer model for a given axis.

      \paran[in]   Mod1label1: Producer model tick marks.
      \paran[in]    Mod1npts1: Number of producer tick marks.
      \paran[in]   Mod2label1: Consumer model tick marks.
      \paran[in]    Mod2npts1: Number of consumer tick marks.
      \paran[out]         Wx1: Array of the corresponding linear
                               interpolation weights with Mod2npts1
                               entries.
      \paran[out]      Windx1: Array of the corresponding linear
                               interpolation indices with Mod2npts1
                               entries.
      \paran[in] Mod2DataType: Consumer model data type precision.

      \return An integer error control value with 0, if success.

*/
/***************************************************************/
int DCT_Mod1CartesianWeight (const DCT_Scalar *Mod1label1,
                 const DCT_Integer Mod1npts1, const DCT_Scalar *Mod2label1,
                 const DCT_Integer Mod2npts1, DCT_VoidPointer Wx1,
                 DCT_Integer *Windx1, const DCT_Data_Types Mod2DataType )
{
/* ---------------------------------------------  Local Variables  */
  int  ierr=0;
  DCT_Scalar fxi;
  DCT_Scalar fh, invfh, ft;
  float *fWx;
  double dh, invdh, dt, dxi, *dWx;
  long double ldh, invldh, ldt, ldxi, *ldWx;
  DCT_Integer indi, i, beflast;

/* -----------------------------  BEGIN( DCT_Mod1CartesianWeight ) */
  indi=0;
  beflast = Mod1npts1 - 1; /* Before the last index in model 1 */
  if (Mod2DataType == DCT_FLOAT ) {
    fWx = (float *) Wx1;
    fh = (*(Mod1label1 + 1) - *Mod1label1)/(DCT_Scalar)(Mod1npts1 -1);
    invfh = 1.0/fh;
    fxi = *Mod1label1;
    for (i=0; i < Mod2npts1; i++) {
      ft = *(Mod2label1 + i) - fxi;
      while ( (ft > fh) && ( i < beflast ) ) {
        indi++;
        fxi = *Mod1label1 + indi*fh;
        ft = *(Mod2label1 + i) - fxi;
      }
      *(fWx + i) = (float) ft * invfh;
      *(Windx1 + i) = indi;
    }
  } else if (Mod2DataType == DCT_DOUBLE ) {
    dWx = (double *) Wx1;
    dh = (double) (*(Mod1label1 + 1) - *Mod1label1)/(double)(Mod1npts1 -1);
    invdh = 1.0/dh;
    dxi = (double) *Mod1label1;
    for (i=0; i < Mod2npts1; i++) {
      dt = (double)*(Mod2label1 + i) - dxi;
      while ( (dt > dh) && ( i < beflast ) ) {
        indi++;
        dxi = (double)*Mod1label1 + indi*dh;
        dt = (double)*(Mod2label1 + i) - dxi;
      }
      *(dWx + i) = dt * invdh;
      *(Windx1 + i) = indi;
    }
  } else if (Mod2DataType == DCT_LONG_DOUBLE ) {
    ldWx = (long double *) Wx1;
    ldh = (long double) (*(Mod1label1 + 1) - *Mod1label1) / 
                        (long double)(Mod1npts1 -1);
    invldh = 1.0/ldh;
    ldxi = (long double) *Mod1label1;
    for (i=0; i < Mod2npts1; i++) {
      ldt = (long double)*(Mod2label1 + i) - ldxi;
      while ( (ldt > ldh) && ( i < beflast ) ) {
        indi++;
        ldxi = (long double)*Mod1label1 + indi*ldh;
        ldt = (long double)*(Mod2label1 + i) - ldxi;
      }
      *(ldWx + i) = ldt * invldh;
      *(Windx1 + i) = indi;
    }
  }

  return(ierr);

/* -------------------------------  END( DCT_Mod1CartesianWeight ) */
}

/*******************************************************************/
/*                     DCT_Mod2CartesianWeight                     */
/*                                                                 */
/*!   Calculates the weights of the linear interpolation of a
      rectilinear mesh in the producer model and cartesian in
      the consumer model for a given axis.

      \paran[in]   Mod1label1: Producer model tick marks.
      \paran[in]    Mod1npts1: Number of producer tick marks.
      \paran[in]   Mod2label1: Consumer model tick marks.
      \paran[in]    Mod2npts1: Number of consumer tick marks.
      \paran[out]         Wx1: Array of the corresponding linear
                               interpolation weights with Mod2npts1
                               entries.
      \paran[out]      Windx1: Array of the corresponding linear
                               interpolation indices with Mod2npts1
                               entries.
      \paran[in] Mod2DataType: Consumer model data type precision.

      \return An integer error control value with 0, if success.

*/
/*******************************************************************/
int DCT_Mod2CartesianWeight (const DCT_Scalar *Mod1label1,
                 const DCT_Integer Mod1npts1, const DCT_Scalar *Mod2label1,
                 const DCT_Integer Mod2npts1, DCT_VoidPointer Wx1,
                 DCT_Integer *Windx1, const DCT_Data_Types Mod2DataType )
{
/* ---------------------------------------------  Local Variables  */
  int  ierr=0;
  DCT_Scalar h2, valsup;
  DCT_Scalar first, last, current;
  float *fWx, fh, invfh, ft, flowv;
  double dh, invdh, dt, dlowv, *dWx;
  long double ldh, invldh, ldt, ldlowv, *ldWx;
  DCT_Integer ii, jj, itersup;

/* -----------------------------  BEGIN( DCT_Mod2CartesianWeight ) */

  if (Mod2DataType == DCT_FLOAT ) {

    first = *Mod2label1;
    last  = *(Mod2label1+1);
    valsup = *Mod1label1;
    /** Verify that first data values cover the interpalated values **/
    if ( valsup > first ) return( 1 );
    /** Takes the first values above the values to be interpolated **/
    for ( jj = 0; (jj < Mod1npts1) && ((valsup = *(Mod1label1 + jj)) <= first); jj++ );

    jj--;
    flowv =  (float) *(Mod1label1 + jj);
    fWx = (float *) Wx1;
    fh = (float) valsup - flowv;
    invfh = 1.0/fh;
    h2 = *(Mod2label1 + 2);
    /** The first iteration use the first value **/
    /** Like ii=0  **/
      ft = (float) first - flowv;
      if (ft > fh) {
        for ( jj++;
              (jj < Mod1npts1) && ((valsup = *(Mod1label1 + jj)) < first);
              jj++ );
        jj--;
        flowv = (float) *(Mod1label1 + jj);
        ft    = (float) first - flowv;
        fh    = (float) valsup - flowv;
        invfh = 1.0/fh;
      }
      *(fWx) = ft * invfh;
      *(Windx1) = jj;
    itersup = Mod2npts1-1;
    for (ii=1; ii < itersup; ii++) {
      current = first + h2*(DCT_Scalar)ii;
      ft = (float)current - flowv;
      if (ft > fh) {
        for ( jj++;
              (jj < Mod1npts1) && ((valsup = *(Mod1label1 + jj)) < current);
              jj++ );
        jj--;
        flowv = (float) *(Mod1label1 + jj);
        ft    = (float) current - flowv;
        fh    = (float) valsup - flowv;
        invfh = 1.0/fh;
      }
      *(fWx + ii) = ft * invfh;
      *(Windx1 + ii) = jj;
    }
    /** The Last iteration use the last value **/
    /** Like ii=Mod2npts1-1  **/
      ft = (float)last - flowv;
      if (ft > fh) {
        for ( jj++;
              (jj < Mod1npts1) && ((valsup = *(Mod1label1 + jj)) < last);
              jj++ );
        jj--;
        flowv = (float) *(Mod1label1 + jj);
        ft    = (float) last - flowv;
        fh    = (float) valsup - flowv;
        invfh = 1.0/fh;
      }
      *(fWx + itersup) = ft * invfh;
      *(Windx1 + itersup) = jj;

  } else if (Mod2DataType == DCT_DOUBLE ) {

    first = *Mod2label1;
    last  = *(Mod2label1+1);
    valsup = *Mod1label1;
    /** Verify that first data values cover the interpalated values **/
    if ( valsup > first ) return( 1 );
    /** Takes the first values above the values to be interpolated **/
    for ( jj = 0; (jj < Mod1npts1) && ((valsup = *(Mod1label1 + jj)) <= first); jj++ );

    jj--;
    dlowv =  (double) *(Mod1label1 + jj);
    dWx = (double *) Wx1;
    dh = (double) valsup - dlowv;
    invdh = 1.0/dh;
    h2 = *(Mod2label1 + 2);
    /** The first iteration use the first value **/
    /** Like ii=0  **/
      dt = (double) first - dlowv;
      if (dt > dh) {
        for ( jj++;
              (jj < Mod1npts1) && ((valsup = *(Mod1label1 + jj)) < first);
              jj++ );
        jj--;
        dlowv =  (double) *(Mod1label1 + jj);
        dt = (double) first - dlowv;
        dh = (double) valsup - dlowv;
        invdh = 1.0/dh;
      }
      *(dWx) = dt * invdh;
      *(Windx1) = jj;
    itersup = Mod2npts1-1;
    for (ii=1; ii < itersup; ii++) {
      current = first + h2*(DCT_Scalar)ii;
      dt = (double) current - dlowv;
      if (dt > dh) {
        for ( jj++;
              (jj < Mod1npts1) && ((valsup = *(Mod1label1 + jj)) < current);
              jj++ );
        jj--;
        dlowv = (double) *(Mod1label1 + jj);
        dt    = (double) current - dlowv;
        dh    = (double) valsup - dlowv;
        invdh = 1.0/dh;
      }
      *(dWx + ii) = dt * invdh;
      *(Windx1 + ii) = jj;
    }
    /** The Last iteration use the last value **/
    /** Like ii=Mod2npts1-1  **/
      dt = (double) last - dlowv;
      if (dt > dh) {
        for ( jj++;
              (jj < Mod1npts1) && ((valsup = *(Mod1label1 + jj)) < last);
              jj++ );
        jj--;
        dlowv = (double) *(Mod1label1 + jj);
        dt    = (double) last - dlowv;
        dh    = (double) valsup - dlowv;
        invdh = 1.0/dh;
      }
      *(dWx + itersup) = dt * invdh;
      *(Windx1 + itersup) = jj;
  } else  if (Mod2DataType == DCT_LONG_DOUBLE ) {
    first = *Mod2label1;
    last  = *(Mod2label1+1);
    valsup = *Mod1label1;
    /** Verify that first data values cover the interpalated values **/
    if ( valsup > first ) return( 1 );
    /** Takes the first values above the values to be interpolated **/
    for ( jj = 0; (jj < Mod1npts1) && ((valsup = *(Mod1label1 + jj)) <= first); jj++ );

    jj--;
    ldlowv =  (long double) *(Mod1label1 + jj);
    ldWx = (long double *) Wx1;
    ldh = (long double) valsup - ldlowv;
    invldh = 1.0/ldh;
    h2 = *(Mod2label1 + 2);
    /** The first iteration use the first value **/
    /** Like ii=0  **/
      ldt = (long double) first - ldlowv;
      if (ldt > ldh) {
        for ( jj++;
              (jj < Mod1npts1) && ((valsup = *(Mod1label1 + jj)) < first);
              jj++ );
        jj--;
        ldlowv =  (long double) *(Mod1label1 + jj);
        ldt = (long double) first - ldlowv;
        ldh = (long double) valsup - ldlowv;
        invldh = 1.0/ldh;
      }
      *(ldWx) = ldt * invldh;
      *(Windx1) = jj;
    itersup = Mod2npts1-1;
    for (ii=1; ii < itersup; ii++) {
      current = first + h2*(DCT_Scalar)ii;
      ldt = (long double) current - ldlowv;
      if (ldt > ldh) {
        for ( jj++;
              (jj < Mod1npts1) && ((valsup = *(Mod1label1 + jj)) < current);
              jj++ );
        jj--;
        ldlowv = (long double) *(Mod1label1 + jj);
        ldt    = (long double) current - ldlowv;
        ldh    = (long double) valsup - ldlowv;
        invldh = 1.0/ldh;
      }
      *(ldWx + ii) = ldt * invldh;
      *(Windx1 + ii) = jj;
    }
    /** The Last iteration use the last value **/
    /** Like ii=Mod2npts1-1  **/
      ldt = (long double) last - ldlowv;
      if (ldt > ldh) {
        for ( jj++;
              (jj < Mod1npts1) && ((valsup = *(Mod1label1 + jj)) < last);
              jj++ );
        jj--;
        ldlowv = (long double) *(Mod1label1 + jj);
        ldt    = (long double) last - ldlowv;
        ldh    = (long double) valsup - ldlowv;
        invldh = 1.0/ldh;
      }
      *(ldWx + itersup) = ldt * invldh;
      *(Windx1 + itersup) = jj;
  }

  return(ierr);

/* -------------------------------  END( DCT_Mod2CartesianWeight ) */
}

/***************************************************************/
/*                     DCT_CartesianWeight                     */
/*                                                             */
/*!   Calculates the weights of the linear interpolation of a
      cartesian mesh in both models for a given axis.

      \paran[in]   Mod1label1: Producer model tick marks.
      \paran[in]    Mod1npts1: Number of producer tick marks.
      \paran[in]   Mod2label1: Consumer model tick marks.
      \paran[in]    Mod2npts1: Number of consumer tick marks.
      \paran[out]         Wx1: Array of the corresponding linear
                               interpolation weights with Mod2npts1
                               entries.
      \paran[out]      Windx1: Array of the corresponding linear
                               interpolation indices with Mod2npts1
                               entries.
      \paran[in] Mod2DataType: Consumer model data type precision.

      \return An integer error control value with 0, if success.

*/
/***************************************************************/
int DCT_CartesianWeight (const DCT_Scalar *Mod1label1,
                 const DCT_Integer Mod1npts1, const DCT_Scalar *Mod2label1,
                 const DCT_Integer Mod2npts1, DCT_VoidPointer Wx1,
                 DCT_Integer *Windx1, const DCT_Data_Types Mod2DataType )
{
/* ---------------------------------------------  Local Variables  */
  int  ierr=1;
  DCT_Scalar fxi, h2;
  DCT_Scalar fh, invfh, ft;
  float *fWx;
  double dh, invdh, dt, dxi, *dWx;
  long double ldh, invldh, ldt, ldxi, *ldWx;
  DCT_Integer indi, i, beflast;

/* ---------------------------------  BEGIN( DCT_CartesianWeight ) */

  indi=0;
  beflast = Mod1npts1 - 1; /* Before the last index in model 1 */
  if (Mod2DataType == DCT_FLOAT ) {
    fWx = (float *) Wx1;
    fh = (*(Mod1label1 + 1) - *Mod1label1)/(DCT_Scalar)(Mod1npts1 -1);
    invfh = 1.0/fh;
    h2 = (*(Mod2label1 + 1) - *Mod2label1)/(DCT_Scalar)(Mod2npts1 -1);
    fxi = *Mod1label1;
    for (i=0; i < Mod2npts1; i++) {
      ft = *Mod2label1 + i*h2 - fxi;
      while ( (ft > fh) && ( i < beflast ) ) {
        indi++;
        fxi = *Mod1label1 + indi*fh;
        ft = *Mod2label1 + i*h2 - fxi;
      }
      *(fWx + i) = (float) ft * invfh;
      *(Windx1 + i) = indi;
    }
  } else if (Mod2DataType == DCT_DOUBLE ) {
    dWx = (double *) Wx1;
    dh = (double)(*(Mod1label1 + 1) - *Mod1label1)/(double)(Mod1npts1 -1);
    invdh = 1.0/dh;
    h2 = (*(Mod2label1 + 1) - *Mod2label1)/(DCT_Scalar)(Mod2npts1 -1);
    dxi = *Mod1label1;
    for (i=0; i < Mod2npts1; i++) {
      dt = (double) *Mod2label1 + i*h2 - dxi;
      while ( (dt > dh) && ( i < beflast ) ) {
        indi++;
        dxi = (double) *Mod1label1 + indi*dh;
        dt = (double) *Mod2label1 + i*h2 - dxi;
      }
      *(dWx + i) = dt * invdh;
      *(Windx1 + i) = indi;
    }
  } else if (Mod2DataType == DCT_LONG_DOUBLE ) {
    ldWx = (long double *) Wx1;
    ldh = (long double)(*(Mod1label1 + 1) - *Mod1label1) / 
         (long double)(Mod1npts1 -1);
    invldh = 1.0/ldh;
    h2 = (*(Mod2label1 + 1) - *Mod2label1)/(DCT_Scalar)(Mod2npts1 -1);
    ldxi = *Mod1label1;
    for (i=0; i < Mod2npts1; i++) {
      ldt = (long double) *Mod2label1 + i*h2 - ldxi;
      while ( (ldt > ldh) && ( i < beflast ) ) {
        indi++;
        ldxi = (long double) *Mod1label1 + indi*ldh;
        ldt = (long double) *Mod2label1 + i*h2 - ldxi;
      }
      *(ldWx + i) = ldt * invldh;
      *(Windx1 + i) = indi;
    }
  }

  return(ierr=0);

/* -----------------------------------  END( DCT_CartesianWeight ) */
}

/*******************************************************************/
/*                        DCT_Bilinear_Weight                      */
/*                                                                 */
/*!    Calculates the bilinear interpolation weigths in order to
       have them ready when the interpolations will be called

  \param[in,out] CommSched  Pointer to Consume structure.
  \param[in]    ProdLabels  Pointer to an array of length dim, with
                            pointers to tick marks arrays
                            corresponding to the producer receiving
                            subdomain.
  \param[in]    ConsLabels  Pointer to an array of length dim, with
                            pointers to tick marks arrays
                            corresponding to the consumer local
                            subdomain.
  \param[in]   ConsDomType  Array of length dim, indicating the type
                            of domain discretization.
                            \sa DCT_Domain_Type
  \param[in]   ConsTotNpts  Pointer to an array of length dim, with
                            the total number of point for every
                            consumer tick marks array.
  \param[in]         dtype  Data type of elements stored in the
                            consumer subdomain.

      \return An integer error control value with 0 if success.

*/
/*******************************************************************/
int DCT_Bilinear_Weight( DCT_Consume_Plan *CommSched, DCT_Scalar **ProdLabels,
                         DCT_Scalar **ConsLabels, DCT_Domain_Type *ConsDomType,
                         DCT_Integer *ConsTotNpts, DCT_Data_Types dtype )
{
/* ---------------------------------------------  Local Variables  */
   int  ierr = 0;
   
   DCT_Integer Mod1npts1, Mod1npts2;
   DCT_Integer Mod2npts1, Mod2npts2;
   DCT_Integer VarNpts1, VarNpts2;
   
   DCT_VoidPointer  *Wx1, *Wx2;
   DCT_VoidPointer  *Wx1ini, *Wx2ini;
   DCT_Integer *Windx1, *Windx2;
   DCT_Integer  IniPos1, IniPos2, Ind;
   DCT_Integer  RelIniPos1, RelIniPos2;
   DCT_Scalar   h1;
   
   DCT_Integer  *VarMask;
   
   DCT_Scalar  *CLabels[2];
   
   register int      ii, jj, limit;
   
/* ---------------------------------  BEGIN( DCT_Bilinear_Weight ) */

   Mod1npts1 = CommSched->VarRcvdNpts[0];
   Mod1npts2 = CommSched->VarRcvdNpts[1];
   Mod2npts1 = CommSched->VarNpts[0];
   Mod2npts2 = CommSched->VarNpts[1];
   IniPos1 = CommSched->VarIniPos[0];
   IniPos2 = CommSched->VarIniPos[1];
   
   /** In the interpolated consumer domain, the position inside the producer data **/
   /** Determining the limits in x-direction **/
   if ( ConsDomType[0] == DCT_CARTESIAN ) {
      ierr = DCT_IncludedIndex_Cartes( ConsLabels[0], IniPos1, Mod2npts1, ProdLabels[0][0],
                                      ProdLabels[0][CommSched->VarRcvdNpts[0]-1],
                                      &RelIniPos1, &VarNpts1, NULL, 0 );
      DCTERROR( ierr == 0 , "Error: Subdomain obtained by the broker is out of boundary",
                return(1) );
   }
   else {
      ierr = DCT_IncludedIndex_Rectil( ConsLabels[0], IniPos1, Mod2npts1, ProdLabels[0][0],
                                        ProdLabels[0][CommSched->VarRcvdNpts[0]-1],
                                        &RelIniPos1, &VarNpts1, NULL, 0 );
      DCTERROR( ierr == 0 , "Error: Subdomain obtained by the broker is out of boundary",
                return(1) );
   }
   /** Determining the limits in y-direction **/
   if ( ConsDomType[1] == DCT_CARTESIAN ) {
      ierr = DCT_IncludedIndex_Cartes( ConsLabels[1], IniPos2, Mod2npts2, ProdLabels[1][0],
                                      ProdLabels[1][CommSched->VarRcvdNpts[1]-1],
                                      &RelIniPos2, &VarNpts2, NULL, 0 );
      DCTERROR( ierr == 0 , "Error: Subdomain obtained by the broker is out of boundary",
                return(1) );
   }
   else {
      ierr = DCT_IncludedIndex_Rectil( ConsLabels[1], IniPos2, Mod2npts2, ProdLabels[1][0],
                                        ProdLabels[1][CommSched->VarRcvdNpts[1]-1],
                                        &RelIniPos2, &VarNpts2, NULL, 0 );
      DCTERROR( ierr == 0 , "Error: Subdomain obtained by the broker is out of boundary",
                return(1) );
   }
   
   /** The initial position is relative to temporal array **/
   RelIniPos1 = RelIniPos1 - IniPos1;
   RelIniPos2 = RelIniPos2 - IniPos2;
   /* Check if the mask is needed */
   if ( (RelIniPos1==0) && (RelIniPos2==0) && (VarNpts1 == Mod2npts1) &&
                                              (VarNpts2 == Mod2npts2) ) {
      /* No masking needed in this step */
      CommSched->VarMask = (DCT_Integer *)NULL;
   }
   else {
      VarMask = DCT_Data_Allocate( DCT_INTEGER, CommSched->VarNpts, 2, 0 );
      DCTERROR( VarMask == (DCT_Integer *)NULL, "Memory Allocation Failed",
                return(1) );      
   
      /** Setting the mask **/
      CommSched->VarMask = VarMask;
      limit = RelIniPos1*Mod2npts2;
      for ( ii=0; ii < limit; ii++) *(VarMask++) = 0;
      limit = Mod2npts2 - RelIniPos2 - VarNpts2;
      for ( ii=0; ii < VarNpts1; ii++) {
         for( jj = 0; jj < RelIniPos2; jj++) *(VarMask++) = 0;
         for( jj = 0; jj < VarNpts2; jj++) *(VarMask++) = 1;
         for( jj = 0; jj < limit; jj++) *(VarMask++) = 0;
      }
      limit = (Mod2npts1 - RelIniPos1 - VarNpts1)*Mod2npts2;
      for ( ii=0; ii < limit; ii++) *(VarMask++) = 0;
   }

   if (dtype == DCT_FLOAT ) {
      Wx1 = malloc(sizeof(float)*(size_t)Mod2npts1);
      DCTERROR( Wx1 == (DCT_VoidPointer) NULL, "Memory Allocation Failed", return(1) );
      Wx1ini = (void *)((float *)Wx1 + RelIniPos1);
      Wx2 = malloc(sizeof(float)*(size_t)Mod2npts2);
      DCTERROR( Wx2 == (DCT_VoidPointer) NULL, "Memory Allocation Failed", return(1) );
      Wx2ini = (void *)((float *)Wx2 + RelIniPos2);
   } else if (dtype == DCT_DOUBLE ) {
      Wx1 = malloc(sizeof(double)*(size_t)Mod2npts1);
      DCTERROR( Wx1 == (DCT_VoidPointer) NULL, "Memory Allocation Failed", return(1) );
      Wx1ini = (void *)((double *)Wx1 + RelIniPos1);
      Wx2 = malloc(sizeof(double)*(size_t)Mod2npts2);
      DCTERROR( Wx2 == (DCT_VoidPointer) NULL, "Memory Allocation Failed", return(1) );
      Wx2ini = (void *)((double *)Wx2 + RelIniPos2);
   } else if (dtype == DCT_LONG_DOUBLE ) {
      Wx1 = malloc(sizeof(long double)*(size_t)Mod2npts1);
      DCTERROR( Wx1 == (DCT_VoidPointer) NULL, "Memory Allocation Failed", return(1) );
      Wx1ini = (void *)((long double *)Wx1 + RelIniPos1);
      Wx2 = malloc(sizeof(long double)*(size_t)Mod2npts2);
      DCTERROR( Wx2 == (DCT_VoidPointer) NULL, "Memory Allocation Failed", return(1) );
      Wx2ini = (void *)((long double *)Wx2 + RelIniPos2);
   }
   
   Windx1 = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t)Mod2npts1);
   DCTERROR( Windx1 == (DCT_Integer *) NULL, "Memory Allocation Failed", return(1) );
   
   Windx2 = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t)Mod2npts2);
   DCTERROR( Windx2 == (DCT_Integer *) NULL, "Memory Allocation Failed", return(1) );
   
   /***** Calculation of weights in x-directions *****/
   /**** The producer model always is RECTILINEAR ***/
   if ( ConsDomType[0] == DCT_CARTESIAN ) {
      /* Calculating the weight matrix for x-coordinate W^{(x)} */
      CLabels[0] = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)3);
      h1 = ConsLabels[0][2];
      Ind = IniPos1 + RelIniPos1;
      if ( Ind == 0 ) CLabels[0][0] = ConsLabels[0][0];
      else CLabels[0][0] = ConsLabels[0][0] + h1*(DCT_Scalar)Ind;
      Ind += VarNpts1 - 1;
      if ( Ind == (ConsTotNpts[0]-1) ) CLabels[0][1] = ConsLabels[0][1];
      else CLabels[0][1] = ConsLabels[0][0] + h1*(DCT_Scalar)Ind;
      CLabels[0][2] = h1;
      ierr = DCT_Mod2CartesianWeight ( ProdLabels[0], Mod1npts1, CLabels[0], VarNpts1,
                                       Wx1ini, (Windx1+RelIniPos1), dtype );
      free ( CLabels[0] );
   } else if ( ConsDomType[0] == DCT_RECTILINEAR ) {
      /* Calculating the weight matrix for x-coordinate W^{(x)} */
      CLabels[0] = (DCT_Scalar *)( ConsLabels[0] + IniPos1 + RelIniPos1 );
      ierr = DCT_RectilinearWeight ( ProdLabels[0], Mod1npts1, CLabels[0], VarNpts1,
                                     Wx1ini, (Windx1+RelIniPos1), dtype );
   }
   if (ierr) return(1);
   /* the unmasked indices should be a valid index to avoid BAD MEMORY ACCESS error */ 
   limit = Mod1npts1 - 2;
   for ( ii = 0; ii < RelIniPos1; ii++ )
      Windx1[ii] = 0;
   for ( ii = (RelIniPos1+VarNpts1); ii < Mod2npts1; ii++ )
      Windx1[ii] = limit;
   
   /***** Calculation of weights in y-directions *****/
   /**** The producer model always is RECTILINEAR ***/
   if ( ConsDomType[1] == DCT_CARTESIAN ) {
      /* Calculating the weight matrix for y-coordinate W^{(y)} */
      CLabels[1] = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)3);
      h1 = ConsLabels[1][2];
      Ind = IniPos2 + RelIniPos2;
      if ( Ind == 0 ) CLabels[1][0] = ConsLabels[1][0];
      else CLabels[1][0] = ConsLabels[1][0] + h1*(DCT_Scalar)Ind;
      Ind += VarNpts2 - 1;
      if ( Ind == (ConsTotNpts[1]-1) ) CLabels[1][1] = ConsLabels[1][1];
      else CLabels[1][1] = ConsLabels[1][0] + h1*(DCT_Scalar)Ind;
      CLabels[1][2] = h1;
      ierr = DCT_Mod2CartesianWeight ( ProdLabels[1], Mod1npts2, CLabels[1], VarNpts2,
                                    Wx2ini, (Windx2+RelIniPos2), dtype );
      free ( CLabels[1] );
   }  else if ( ConsDomType[1] == DCT_RECTILINEAR ) {
      /* Calculating the weight matrix for y-coordinate W^{(y)} */
      CLabels[1] = (DCT_Scalar *)( ConsLabels[1] + IniPos2 + RelIniPos2  );
      ierr = DCT_RectilinearWeight ( ProdLabels[1], Mod1npts2, CLabels[1], VarNpts2,
                                  Wx2ini, (Windx2+RelIniPos2), dtype );
   }
   if (ierr) return(1);
   /* the unmasked indices should be a valid index to avoid BAD MEMORY ACCESS error */ 
   limit = Mod1npts2 - 2;
   for ( ii = 0; ii < RelIniPos2; ii++ )
      Windx2[ii] = 0;
   for ( ii = (RelIniPos2+VarNpts2); ii < Mod2npts2; ii++ )
      Windx2[ii] = limit;

   /** The index are plugged to the structure **/
   CommSched->VarInt = (DCT_Integer **) malloc(sizeof(DCT_Integer *)*(size_t)2);
   DCTERROR( CommSched->VarInt == (DCT_Integer **)NULL, "Memory Allocation Failed",
             return(1) );
   CommSched->VarInt[0] = (DCT_Integer *) Windx1;
   CommSched->VarInt[1] = (DCT_Integer *) Windx2;

   /** The weights are plugged to the structure **/
   /** The label pointers are already allocated **/
   CommSched->VarLabels = (DCT_VoidPointer *) malloc(sizeof(DCT_VoidPointer)*(size_t)2);
   DCTERROR( CommSched->VarLabels == (DCT_VoidPointer *)NULL, "Memory Allocation Failed",
             return(1) );

   CommSched->VarLabels[0] = Wx1;
   CommSched->VarLabels[1] = Wx2;

   return (ierr);
/* -----------------------------------  END( DCT_Bilinear_Weight ) */
}

/*******************************************************************/
/*                        DCT_Trilinear_Weight                     */
/*                                                                 */
/*!    Calculates the trilinear interpolation weigths in order to
       have them ready when the interpolations will be called

  \param[in,out] CommSched  Pointer to Consume structure.
  \param[in]    ProdLabels  Pointer to an array of length dim, with
                            pointers to tick marks arrays
                            corresponding to the producer receiving
                            subdomain.
  \param[in]    ConsLabels  Pointer to an array of length dim, with
                            pointers to tick marks arrays
                            corresponding to the consumer local
                            subdomain.
  \param[in]   ConsDomType  Array of length dim, indicating the type
                            of domain discretization.
                            \sa DCT_Domain_Type
  \param[in]   ConsTotNpts  Pointer to an array of length dim, with
                            the total number of point for every
                            consumer tick marks array.
  \param[in]         dtype  Data type of elements stored in the
                            consumer subdomain.

      \todo Check why the Wx?Ini points to an inner address of a just
            allocated array. Don't why to start since the beginning at
            the position that it is needed?
      
      \return An integer error control value with 0 if success.

*/
/*******************************************************************/
int DCT_Trilinear_Weight( DCT_Consume_Plan *CommSched, DCT_Scalar **ProdLabels,
                          DCT_Scalar **ConsLabels, DCT_Domain_Type *ConsDomType,
                          DCT_Integer *ConsTotNpts, DCT_Data_Types dtype )
{
/* ---------------------------------------------  Local Variables  */
   int  ierr = 0;
   
   DCT_Integer Mod1npts1, Mod1npts2, Mod1npts3;
   DCT_Integer Mod2npts1, Mod2npts2, Mod2npts3;
   DCT_Integer VarNpts1, VarNpts2, VarNpts3;
   
   DCT_VoidPointer  *Wx1, *Wx2, *Wx3;
   DCT_VoidPointer  *Wx1ini, *Wx2ini, *Wx3ini;
   DCT_Integer *Windx1, *Windx2, *Windx3;
   DCT_Integer  IniPos1, IniPos2, IniPos3, Ind;
   DCT_Integer  RelIniPos1, RelIniPos2, RelIniPos3;
   DCT_Scalar   h1;
   
   DCT_Integer  *VarMask;
   
   DCT_Scalar  *CLabels[2];
   
   register int   ii, jj, kk, limit, limit2, limit3, needmask;
   
/* ---------------------------------  BEGIN( DCT_Trilinear_Weight ) */

   Mod1npts1 = CommSched->VarRcvdNpts[0];
   Mod1npts2 = CommSched->VarRcvdNpts[1];
   Mod1npts3 = CommSched->VarRcvdNpts[2];
   Mod2npts1 = CommSched->VarNpts[0];
   Mod2npts2 = CommSched->VarNpts[1];
   Mod2npts3 = CommSched->VarNpts[2];
   IniPos1 = CommSched->VarIniPos[0];
   IniPos2 = CommSched->VarIniPos[1];
   IniPos3 = CommSched->VarIniPos[2];
   
   /** In the interpolated consumer domain, the position inside the producer data **/
   /** Determining the limits in x-direction **/
   if ( ConsDomType[0] == DCT_CARTESIAN ) {
      ierr = DCT_IncludedIndex_Cartes( ConsLabels[0], IniPos1, Mod2npts1, ProdLabels[0][0],
                                      ProdLabels[0][CommSched->VarRcvdNpts[0]-1],
                                      &RelIniPos1, &VarNpts1, NULL, 0 );
      DCTERROR( ierr == 0 , "Error: Subdomain obtained by the broker is out of boundary",
                return(1) );
   }
   else {
      ierr = DCT_IncludedIndex_Rectil( ConsLabels[0], IniPos1, Mod2npts1, ProdLabels[0][0],
                                        ProdLabels[0][CommSched->VarRcvdNpts[0]-1],
                                        &RelIniPos1, &VarNpts1, NULL, 0 );
      DCTERROR( ierr == 0 , "Error: Subdomain obtained by the broker is out of boundary",
                return(1) );
   }
   /** Determining the limits in y-direction **/
   if ( ConsDomType[1] == DCT_CARTESIAN ) {
      ierr = DCT_IncludedIndex_Cartes( ConsLabels[1], IniPos2, Mod2npts2, ProdLabels[1][0],
                                      ProdLabels[1][CommSched->VarRcvdNpts[1]-1],
                                      &RelIniPos2, &VarNpts2, NULL, 0 );
      DCTERROR( ierr == 0 , "Error: Subdomain obtained by the broker is out of boundary",
                return(1) );
   }
   else {
      ierr = DCT_IncludedIndex_Rectil( ConsLabels[1], IniPos2, Mod2npts2, ProdLabels[1][0],
                                        ProdLabels[1][CommSched->VarRcvdNpts[1]-1],
                                        &RelIniPos2, &VarNpts2, NULL, 0 );
      DCTERROR( ierr == 0 , "Error: Subdomain obtained by the broker is out of boundary",
                return(1) );
   }
   /** Determining the limits in z-direction **/
   if ( ConsDomType[2] == DCT_CARTESIAN ) {
      ierr = DCT_IncludedIndex_Cartes( ConsLabels[2], IniPos3, Mod2npts3, ProdLabels[2][0],
                                      ProdLabels[2][CommSched->VarRcvdNpts[2]-1],
                                      &RelIniPos3, &VarNpts3, NULL, 0 );
      DCTERROR( ierr == 0 , "Error: Subdomain obtained by the broker is out of boundary",
                return(1) );
   }
   else {
      ierr = DCT_IncludedIndex_Rectil( ConsLabels[2], IniPos3, Mod2npts3, ProdLabels[2][0],
                                        ProdLabels[2][CommSched->VarRcvdNpts[2]-1],
                                        &RelIniPos3, &VarNpts3, NULL, 0 );
      DCTERROR( ierr == 0 , "Error: Subdomain obtained by the broker is out of boundary",
                return(1) );
   }
   
   /** The initial position is relative to temporal array **/
   RelIniPos1 = RelIniPos1 - IniPos1;
   RelIniPos2 = RelIniPos2 - IniPos2;
   RelIniPos3 = RelIniPos3 - IniPos3;
   /* Check if the mask is needed */
   needmask =!( (RelIniPos1==0) && (RelIniPos2==0) && (RelIniPos3==0) &&
                (VarNpts1 == Mod2npts1) && (VarNpts2 == Mod2npts2) && (VarNpts3 == Mod2npts3) );

   if ( needmask ) {
      VarMask = DCT_Data_Allocate( DCT_INTEGER, CommSched->VarNpts, 3, 0 );
      DCTERROR( VarMask == (DCT_Integer *)NULL, "Memory Allocation Failed",
                return(1) );      
   
      /** Setting the mask **/
      CommSched->VarMask = VarMask;

      /********************** 3D ***********************************/
      limit = RelIniPos2*Mod2npts2*RelIniPos1;
      /** All the way until the west face **/
      for ( ii=0; ii < limit; ii++) *(VarMask++) = 0;
      
      limit = (Mod2npts2 - RelIniPos2 - VarNpts2)*RelIniPos3;
      limit2 = Mod2npts3 - RelIniPos3 - VarNpts3;
      limit3 = RelIniPos2*RelIniPos3;
      /* starting the consumer region in ii index */
      for ( ii=0; ii < VarNpts1; ii++){
          /* The jj out before the consumer region  */
          for( jj = 0; jj < limit3; jj++) *(VarMask++) = 0;

          /* The jj in of the consumer region  */
          for( jj = 0; jj < VarNpts2; jj++) {
             for( kk = 0; kk < RelIniPos3; kk++) *(VarMask++) = 0;
             for( kk = 0; kk < VarNpts3; kk++) *(VarMask++) = 1;
             for( kk = 0; kk < limit2; kk++) *(VarMask++) = 0;            
          }

          /* The jj out after the consumer region  */
          for( jj = 0; jj < limit; jj++) *(VarMask++) = 0;
      }
   } /** End of if ( needmask ) **/

   if (dtype == DCT_FLOAT ) {
      Wx1 = malloc(sizeof(float)*(size_t)Mod2npts1);
      DCTERROR( Wx1 == (DCT_VoidPointer) NULL, "Memory Allocation Failed", return(1) );
      Wx1ini = (void *)((float *)Wx1 + RelIniPos1);

      Wx2 = malloc(sizeof(float)*(size_t)Mod2npts2);
      DCTERROR( Wx2 == (DCT_VoidPointer) NULL, "Memory Allocation Failed", return(1) );
      Wx2ini = (void *)((float *)Wx2 + RelIniPos2);

      Wx3 = malloc(sizeof(float)*(size_t)Mod2npts3);
      DCTERROR( Wx3 == (DCT_VoidPointer) NULL, "Memory Allocation Failed", return(1) );
      Wx3ini = (void *)((float *)Wx3 + RelIniPos3);
   } else if (dtype == DCT_DOUBLE ) {
      Wx1 = malloc(sizeof(double)*(size_t)Mod2npts1);
      DCTERROR( Wx1 == (DCT_VoidPointer) NULL, "Memory Allocation Failed", return(1) );
      Wx1ini = (void *)((double *)Wx1 + RelIniPos1);

      Wx2 = malloc(sizeof(double)*(size_t)Mod2npts2);
      DCTERROR( Wx2 == (DCT_VoidPointer) NULL, "Memory Allocation Failed", return(1) );
      Wx2ini = (void *)((double *)Wx2 + RelIniPos2);

      Wx3 = malloc(sizeof(double)*(size_t)Mod2npts3);
      DCTERROR( Wx3 == (DCT_VoidPointer) NULL, "Memory Allocation Failed", return(1) );
      Wx3ini = (void *)((double *)Wx3 + RelIniPos3);
   } else if (dtype == DCT_LONG_DOUBLE ) {
      Wx1 = malloc(sizeof(long double)*(size_t)Mod2npts1);
      DCTERROR( Wx1 == (DCT_VoidPointer) NULL, "Memory Allocation Failed", return(1) );
      Wx1ini = (void *)((long double *)Wx1 + RelIniPos1);

      Wx2 = malloc(sizeof(long double)*(size_t)Mod2npts2);
      DCTERROR( Wx2 == (DCT_VoidPointer) NULL, "Memory Allocation Failed", return(1) );
      Wx2ini = (void *)((long double *)Wx2 + RelIniPos2);

      Wx3 = malloc(sizeof(long double)*(size_t)Mod2npts3);
      DCTERROR( Wx3 == (DCT_VoidPointer) NULL, "Memory Allocation Failed", return(1) );
      Wx3ini = (void *)((long double *)Wx3 + RelIniPos3);
   }
   
   Windx1 = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t)Mod2npts1);
   DCTERROR( Windx1 == (DCT_Integer *) NULL, "Memory Allocation Failed", return(1) );
   
   Windx2 = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t)Mod2npts2);
   DCTERROR( Windx2 == (DCT_Integer *) NULL, "Memory Allocation Failed", return(1) );
   
   Windx3 = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t)Mod2npts3);
   DCTERROR( Windx3 == (DCT_Integer *) NULL, "Memory Allocation Failed", return(1) );
   
   
   /* ---------------------- DIM 1 -------------------- */
   /***** Calculation of weights in x-directions *****/
   /**** The producer model always is RECTILINEAR ***/
   if ( ConsDomType[0] == DCT_CARTESIAN ) {
      /* Calculating the weight matrix for x-coordinate W^{(x)} */
      CLabels[0] = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)3);
      h1 = ConsLabels[0][2];
      Ind = IniPos1 + RelIniPos1;
      if ( Ind == 0 ) CLabels[0][0] = ConsLabels[0][0];
      else CLabels[0][0] = ConsLabels[0][0] + h1*(DCT_Scalar)Ind;
      Ind += VarNpts1 - 1;
      if ( Ind == (ConsTotNpts[0]-1) ) CLabels[0][1] = ConsLabels[0][1];
      else CLabels[0][1] = ConsLabels[0][0] + h1*(DCT_Scalar)Ind;
      CLabels[0][2] = h1;
      ierr = DCT_Mod2CartesianWeight ( ProdLabels[0], Mod1npts1, CLabels[0], VarNpts1,
                                    Wx1ini, (Windx1+RelIniPos1), dtype );
      free ( CLabels[0] );
   } else if ( ConsDomType[0] == DCT_RECTILINEAR ) {
      /* Calculating the weight matrix for x-coordinate W^{(x)} */
      CLabels[0] = (DCT_Scalar *)( ConsLabels[0] + IniPos1 + RelIniPos1 );
      ierr = DCT_RectilinearWeight ( ProdLabels[0], Mod1npts1, CLabels[0], VarNpts1,
                                  Wx1ini, (Windx1+RelIniPos1), dtype );
   }
   if (ierr) return(1);
   /* the unmasked indices should be a valid index to avoid BAD MEMORY ACCESS error */ 
   limit = Mod1npts1 - 2;
   for ( ii = 0; ii < RelIniPos1; ii++ )
      Windx1[ii] = 0;
   for ( ii = (RelIniPos1+VarNpts1); ii < Mod2npts1; ii++ )
      Windx1[ii] = limit;
   
   /* ---------------------- DIM 2 -------------------- */
   /***** Calculation of weights in y-directions *****/
   /**** The producer model always is RECTILINEAR ***/
   if ( ConsDomType[1] == DCT_CARTESIAN ) {
      /* Calculating the weight matrix for y-coordinate W^{(y)} */
      CLabels[1] = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)3);
      h1 = ConsLabels[1][2];
      Ind = IniPos2 + RelIniPos2;
      if ( Ind == 0 ) CLabels[1][0] = ConsLabels[1][0];
      else CLabels[1][0] = ConsLabels[1][0] + h1*(DCT_Scalar)Ind;
      Ind += VarNpts2 - 1;
      if ( Ind == (ConsTotNpts[1]-1) ) CLabels[1][1] = ConsLabels[1][1];
      else CLabels[1][1] = ConsLabels[1][0] + h1*(DCT_Scalar)Ind;
      CLabels[1][2] = h1;
      ierr = DCT_Mod2CartesianWeight ( ProdLabels[1], Mod1npts2, CLabels[1], VarNpts2,
                                    Wx2ini, (Windx2+RelIniPos2), dtype );
      free ( CLabels[1] );
   }  else if ( ConsDomType[1] == DCT_RECTILINEAR ) {
      /* Calculating the weight matrix for y-coordinate W^{(y)} */
      CLabels[1] = (DCT_Scalar *)( ConsLabels[1] + IniPos2 + RelIniPos2  );
      ierr = DCT_RectilinearWeight ( ProdLabels[1], Mod1npts2, CLabels[1], VarNpts2,
                                  Wx2ini, (Windx2+RelIniPos2), dtype );
   }
   if (ierr) return(1);
   /* the unmasked indices should be a valid index to avoid BAD MEMORY ACCESS error */ 
   limit = Mod1npts2 - 2;
   for ( ii = 0; ii < RelIniPos2; ii++ )
      Windx2[ii] = 0;
   for ( ii = (RelIniPos2+VarNpts2); ii < Mod2npts2; ii++ )
      Windx2[ii] = limit;

   /* ---------------------- DIM 3 -------------------- */
   /***** Calculation of weights in z-directions *****/
   /**** The producer model always is RECTILINEAR ***/
   if ( ConsDomType[2] == DCT_CARTESIAN ) {
      /* Calculating the weight matrix for z-coordinate W^{(z)} */
      CLabels[2] = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)3);
      h1 = ConsLabels[2][2];
      Ind = IniPos3 + RelIniPos3;
      if ( Ind == 0 ) CLabels[2][0] = ConsLabels[2][0];
      else CLabels[2][0] = ConsLabels[2][0] + h1*(DCT_Scalar)Ind;
      Ind += VarNpts3 - 1;
      if ( Ind == (ConsTotNpts[2]-1) ) CLabels[2][1] = ConsLabels[2][1];
      else CLabels[2][1] = ConsLabels[2][0] + h1*(DCT_Scalar)Ind;
      CLabels[2][2] = h1;
      ierr = DCT_Mod2CartesianWeight ( ProdLabels[2], Mod1npts3, CLabels[2], VarNpts3,
                                       Wx3ini, (Windx3+RelIniPos3), dtype );
      free ( CLabels[2] );
   }  else if ( ConsDomType[2] == DCT_RECTILINEAR ) {
      /* Calculating the weight matrix for y-coordinate W^{(y)} */
      CLabels[2] = (DCT_Scalar *)( ConsLabels[2] + IniPos3 + RelIniPos3  );
      ierr = DCT_RectilinearWeight ( ProdLabels[2], Mod1npts3, CLabels[2], VarNpts3,
                                     Wx3ini, (Windx3+RelIniPos3), dtype );
   }
   if (ierr) return(1);
   /* the unmasked indices should be a valid index to avoid BAD MEMORY ACCESS error */ 
   limit = Mod1npts3 - 2;
   for ( ii = 0; ii < RelIniPos3; ii++ )
      Windx3[ii] = 0;
   for ( ii = (RelIniPos3+VarNpts3); ii < Mod2npts3; ii++ )
      Windx3[ii] = limit;

   /** The index are plugged to the structure **/
   CommSched->VarInt = (DCT_Integer **) malloc(sizeof(DCT_Integer *)*(size_t)3);
   DCTERROR( CommSched->VarInt == (DCT_Integer **)NULL, "Memory Allocation Failed",
             return(1) );
   CommSched->VarInt[0] = (DCT_Integer *) Windx1;
   CommSched->VarInt[1] = (DCT_Integer *) Windx2;
   CommSched->VarInt[2] = (DCT_Integer *) Windx3;

   /** The weights are plugged to the structure **/
   /** The label pointers are already allocated **/
   CommSched->VarLabels = (DCT_VoidPointer *) malloc(sizeof(DCT_VoidPointer)*(size_t)3);
   DCTERROR( CommSched->VarLabels == (DCT_VoidPointer *)NULL, "Memory Allocation Failed",
             return(1) );

   CommSched->VarLabels[0] = Wx1;
   CommSched->VarLabels[1] = Wx2;
   CommSched->VarLabels[2] = Wx3;

   return (ierr);
/* -----------------------------------  END( DCT_Trilinear_Weight ) */
}

/*******************************************************************/
/*                    DCT_CheckNoInterpolation2D                   */
/*                                                                 */
/*    Check the DCT_NO_INTERPOLATION option is properly used
       and set the internal DCT_Consume_Plan structure accordingly
       in 2D domains, and the mask is created if it is needed.

     \param[in,out] CommSched  Pointer to Consume structure
     \param[in]    ProdLabels  Pointer to an array of length dim,
                               with pointers to tick marks arrays
                               corresponding to the producer receiving
                               subdomain.
     \param[in]    ConsLabels  Pointer to an array of length dim,
                               with pointers to tick marks arrays
                               corresponding to the consumer local
                               subdomain.
     \param[in]   ConsDomType  Array of length dim, indicating the
                               type of domain discretization.
     \param[in]   ConsTotNpts  Pointer to an array of length dim,
                               with the total number of point for
                               every consumer tick marks array.
     \param[in]         dtype  Data type of elements stored in the
                               consumer subdomain.

      \return An integer error control value with 0 if success.

      \todo Check if the parameter ConsTotNpts must be removed because
            is not used.

*/
/*******************************************************************/
// int DCT_CheckNoInterpolation2D( DCT_Consume_Plan *CommSched, DCT_Scalar **ProdLabels,
//                                 DCT_Scalar **ConsLabels, DCT_Domain_Type *ConsDomType,
//                                 DCT_Integer *ConsTotNpts, DCT_Data_Types dtype )
// {
// /* ---------------------------------------------  Local Variables  */
//    int  ierr = 0;
//    
//    /* DCT_Integer Mod1npts1, Mod1npts2;*/
//    DCT_Integer Mod2npts1, Mod2npts2;
//    DCT_Integer VarNpts1, VarNpts2;
//    
//    DCT_Integer  IniPos1, IniPos2;
//    DCT_Integer  RelIniPos1, RelIniPos2;
//    
//    DCT_Integer  *VarMask;
//    
//    register int      ii, jj, limit;
//    
// /* --------------------------  BEGIN( DCT_CheckNoInterpolation2D ) */
// 
//  /*Mod1npts1 = CommSched->VarRcvdNpts[0];
//    Mod1npts2 = CommSched->VarRcvdNpts[1];*/
//    Mod2npts1 = CommSched->VarNpts[0];
//    Mod2npts2 = CommSched->VarNpts[1];
//    IniPos1 = CommSched->VarIniPos[0];
//    IniPos2 = CommSched->VarIniPos[1];
//    
//    /** In the interpolated consumer domain, the position inside the producer data **/
//    /** Determining the limits in x-direction **/
//    if ( ConsDomType[0] == DCT_CARTESIAN ) {
//       ierr = DCT_IncludedIndex_Cartes( ConsLabels[0], IniPos1, Mod2npts1, ProdLabels[0][0],
//                                       ProdLabels[0][CommSched->VarRcvdNpts[0]-1],
//                                       &RelIniPos1, &VarNpts1, NULL, 0 );
//       DCTERROR( ierr == 0 , "Error: Subdomain obtained by the broker is out of boundary",
//                 return(1) );
//    }
//    else {
//       ierr = DCT_IncludedIndex_Rectil( ConsLabels[0], IniPos1, Mod2npts1, ProdLabels[0][0],
//                                         ProdLabels[0][CommSched->VarRcvdNpts[0]-1],
//                                         &RelIniPos1, &VarNpts1, NULL, 0 );
//       DCTERROR( ierr == 0 , "Error: Subdomain obtained by the broker is out of boundary",
//                 return(1) );
//    }
//    DCTERROR( VarNpts1 != CommSched->VarRcvdNpts[0] ,
//              "Error: Subdomain resolution inconsistency to use DCT_NO_INTERPOLATION option",
//              return(1) );
//    /** Determining the limits in y-direction **/
//    if ( ConsDomType[1] == DCT_CARTESIAN ) {
//       ierr = DCT_IncludedIndex_Cartes( ConsLabels[1], IniPos2, Mod2npts2, ProdLabels[1][0],
//                                       ProdLabels[1][CommSched->VarRcvdNpts[1]-1],
//                                       &RelIniPos2, &VarNpts2, NULL, 0 );
//       DCTERROR( ierr == 0 , "Error: Subdomain obtained by the broker is out of boundary",
//                 return(1) );
//    }
//    else {
//       ierr = DCT_IncludedIndex_Rectil( ConsLabels[1], IniPos2, Mod2npts2, ProdLabels[1][0],
//                                         ProdLabels[1][CommSched->VarRcvdNpts[1]-1],
//                                         &RelIniPos2, &VarNpts2, NULL, 0 );
//       DCTERROR( ierr == 0 , "Error: Subdomain obtained by the broker is out of boundary",
//                 return(1) );
//    }
//    DCTERROR( VarNpts2 != CommSched->VarRcvdNpts[1] ,
//              "Error: Subdomain resolution inconsistency to use DCT_NO_INTERPOLATION option",
//              return(1) );
//    ierr = 0; /* reset the error variable */
// 
//    /** The initial position is relative to temporal array **/
//    RelIniPos1 = RelIniPos1 - IniPos1;
//    RelIniPos2 = RelIniPos2 - IniPos2;
//    /* Check if the mask is needed */
//    if ( (RelIniPos1==0) && (RelIniPos2==0) && (VarNpts1 == Mod2npts1) &&
//                                               (VarNpts2 == Mod2npts2) ) {
//       /* No masking needed in this step */
//       CommSched->VarMask = (DCT_Integer *)NULL;
//    }
//    else {
//       VarMask = DCT_Data_Allocate( DCT_INTEGER, CommSched->VarNpts, 2, 0 );
//       DCTERROR( VarMask == (DCT_Integer *)NULL, "Memory Allocation Failed",
//                 return(1) );      
//    
//       /** Setting the mask **/
//       CommSched->VarMask = VarMask;
//       limit = RelIniPos1*Mod2npts2;
//       for ( ii=0; ii < limit; ii++) *(VarMask++) = 0;
//       limit = Mod2npts2 - RelIniPos2 - VarNpts2;
//       for ( ii=0; ii < VarNpts1; ii++) {
//          for( jj = 0; jj < RelIniPos2; jj++) *(VarMask++) = 0;
//          for( jj = 0; jj < VarNpts2; jj++) *(VarMask++) = 1;
//          for( jj = 0; jj < limit; jj++) *(VarMask++) = 0;
//       }
//       limit = (Mod2npts1 - RelIniPos1 - VarNpts1)*Mod2npts2;
//       for ( ii=0; ii < limit; ii++) *(VarMask++) = 0;
//    }
// 
//    if (dtype != CommSched->VarRcvdDType ) { /* If the received and local data type are different */
//       /* The memory buffer where values after interpolate/filter is allocated */
//       CommSched->VarTempVal = DCT_Data_Allocate( dtype, CommSched->VarNpts, 2, 1 );
//       DCTERROR( CommSched->VarTempVal == NULL,
//                 "Memory Allocation Failed", return(1) );
//    }
//    else { /* No extra temporal area is needed. Both point to that same */
//       CommSched->VarTempVal = CommSched->VarRcvdVal;
//    
//    }
// 
//    return (ierr);
// /* ----------------------------  END( DCT_CheckNoInterpolation2D ) */
// }

/*******************************************************************/
/*                     DCT_CheckNoInterpolation                    */
/*                                                                 */
/*!    Check the DCT_NO_INTERPOLATION option is properly used
       and set the internal DCT_Consume_Plan structure
       accordingly in any DCT_Vars dimension (2, 3, and 4).

     \param[in,out] CommSched  Pointer to Consume structure
     \param[in]    ProdLabels  Pointer to an array of length dim,
                               with pointers to tick marks arrays
                               corresponding to the producer receiving
                               subdomain.
     \param[in]    ConsLabels  Pointer to an array of length dim,
                               with pointers to tick marks arrays
                               corresponding to the consumer local
                               subdomain.
     \param[in]   ConsDomType  Array of length dim, indicating the
                               type of domain discretization.
     \param[in]         dtype  Data type of elements stored in the
                               consumer subdomain.
     \param[in]           dim  Integer with the space dimension (2, 3
                               or 4).

     \return An integer error control value with 0 if the
              DCT_Consume_Plan is suitable.

     \todo Check if the parameter ConsTotNpts must be removed because
           is not used.

*/
/****************************************************************/
int DCT_CheckNoInterpolation( DCT_Consume_Plan *CommSched, DCT_Scalar **ProdLabels,
                              DCT_Scalar **ConsLabels, DCT_Domain_Type *ConsDomType,
                       /* DCT_Integer *ConsTotNpts,*/ DCT_Data_Types dtype, DCT_Integer dim )
//      \param[in]   ConsTotNpts  Pointer to an array of length dim,
//                                with the total number of point for
//                                every consumer tick marks array.
{
/* ---------------------------------------------  Local Variables  */
   int  ierr = 0;
   
/*   DCT_Integer Mod1npts1, Mod1npts2;
   DCT_Integer Mod2npts1, Mod2npts2;
   DCT_Integer VarNpts1, VarNpts2;
   
   DCT_Integer  IniPos1, IniPos2;
   DCT_Integer  RelIniPos1, RelIniPos2;
   
   DCT_Integer Mod1npts[4];*/
   DCT_Integer Mod2npts[4];
   DCT_Integer VarNpts[4];
   DCT_Integer IniPos[4];
   DCT_Integer  RelIniPos[4];
   
   DCT_Integer  *VarMask;
   
   register int      ii, jj, kk, limit, limit2, limit3, needmask;
   
/* ----------------------------  BEGIN( DCT_CheckNoInterpolation ) */

   
   switch (dim) {
   
    /*******************************   DIM4   *******************************/
    case 4:
     Mod2npts[3] = CommSched->VarNpts[3];
     IniPos[3] = CommSched->VarIniPos[3];
     /** In the interpolated consumer domain, the position inside the producer data **/
     /** Determining the limits in y-direction **/
     if ( ConsDomType[3] == DCT_CARTESIAN ) {
        ierr = DCT_IncludedIndex_Cartes( ConsLabels[3], IniPos[3], Mod2npts[3], ProdLabels[3][0],
                                        ProdLabels[3][CommSched->VarRcvdNpts[3]-1],
                                        (RelIniPos+2), (VarNpts+2), NULL, 0 );
        DCTERROR( ierr == 0 , "Error: Subdomain obtained by the broker is out of boundary",
                  return(1) );
        DCTERROR( VarNpts[3] != CommSched->VarRcvdNpts[3] ,
                  "Error: Subdomain resolution inconsistency to use DCT_NO_INTERPOLATION option",
                  return(1) );
     }
     else {
        ierr = DCT_IncludedIndex_Rectil( ConsLabels[3], IniPos[1], Mod2npts[3], ProdLabels[3][0],
                                          ProdLabels[3][CommSched->VarRcvdNpts[3]-1],
                                          (RelIniPos+2), (VarNpts+2), NULL, 0 );
        DCTERROR( ierr == 0 , "Error: Subdomain obtained by the broker is out of boundary",
                  return(1) );
        /* So far only the same number of points between two points is
           proved an additional test sweeping the values is required.  */
        kk = 1;
        for (ii = 0; ((ii < VarNpts[3]) && (kk)); ii++)
           kk = (ConsLabels[3][ii] == ProdLabels[3][ii] ? 1: 0);

        DCTERROR( (VarNpts[3] != CommSched->VarRcvdNpts[3]) || (kk!=1) ,
                  "Error: Subdomain resolution inconsistency to use DCT_NO_INTERPOLATION option",
                  return(1) );
     }
   
    /*******************************   DIM3   *******************************/
    case 3:
     Mod2npts[2] = CommSched->VarNpts[2];
     IniPos[2] = CommSched->VarIniPos[2];
     /** In the interpolated consumer domain, the position inside the producer data **/
     /** Determining the limits in z-direction **/
     if ( ConsDomType[2] == DCT_CARTESIAN ) {
        ierr = DCT_IncludedIndex_Cartes( ConsLabels[2], IniPos[2], Mod2npts[2], ProdLabels[2][0],
                                        ProdLabels[2][CommSched->VarRcvdNpts[2]-1],
                                        (RelIniPos+2), (VarNpts+2), NULL, 0 );
        DCTERROR( ierr == 0 , "Error: Subdomain obtained by the broker is out of boundary",
                  return(1) );
        DCTERROR( VarNpts[2] != CommSched->VarRcvdNpts[2] ,
                  "Error: Subdomain resolution inconsistency to use DCT_NO_INTERPOLATION option",
                  return(1) );
     }
     else {
        ierr = DCT_IncludedIndex_Rectil( ConsLabels[2], IniPos[1], Mod2npts[2], ProdLabels[2][0],
                                          ProdLabels[2][CommSched->VarRcvdNpts[2]-1],
                                          (RelIniPos+2), (VarNpts+2), NULL, 0 );
        DCTERROR( ierr == 0 , "Error: Subdomain obtained by the broker is out of boundary",
                  return(1) );
        /* So far only the same number of points between two points is
           proved an additional test sweeping the values is required.  */
        kk = 1;
        for (ii = 0; ((ii < VarNpts[2]) && (kk)); ii++)
           kk = (ConsLabels[2][ii] == ProdLabels[2][ii] ? 1: 0);

        DCTERROR( (VarNpts[2] != CommSched->VarRcvdNpts[2]) || (kk!=1) ,
                  "Error: Subdomain resolution inconsistency to use DCT_NO_INTERPOLATION option",
                  return(1) );
     }
   
    /*******************************   DIM2   *******************************/
    case 2:
     /*Mod1npts2 = CommSched->VarRcvdNpts[1];*/
     Mod2npts[1] = CommSched->VarNpts[1];
     IniPos[1] = CommSched->VarIniPos[1];
     /** In the interpolated consumer domain, the position inside the producer data **/
     /** Determining the limits in y-direction **/
     if ( ConsDomType[1] == DCT_CARTESIAN ) {
        ierr = DCT_IncludedIndex_Cartes( ConsLabels[1], IniPos[1], Mod2npts[1], ProdLabels[1][0],
                                        ProdLabels[1][CommSched->VarRcvdNpts[1]-1],
                                        (RelIniPos+1), (VarNpts+1), NULL, 0 );
        DCTERROR( ierr == 0 , "Error: Subdomain obtained by the broker is out of boundary",
                  return(1) );
        DCTERROR( VarNpts[1] != CommSched->VarRcvdNpts[1] ,
                  "Error: Subdomain resolution inconsistency to use DCT_NO_INTERPOLATION option",
                  return(1) );
     }
     else {
        ierr = DCT_IncludedIndex_Rectil( ConsLabels[1], IniPos[1], Mod2npts[1], ProdLabels[1][0],
                                          ProdLabels[1][CommSched->VarRcvdNpts[1]-1],
                                          (RelIniPos+1), (VarNpts+1), NULL, 0 );
        DCTERROR( ierr == 0 , "Error: Subdomain obtained by the broker is out of boundary",
                  return(1) );
        for (ii = 0; ((ii < VarNpts[1]) && (kk)); ii++)
           kk = (ConsLabels[1][ii] == ProdLabels[1][ii] ? 1: 0);

        DCTERROR( (VarNpts[1] != CommSched->VarRcvdNpts[1]) || (kk!=1) ,
                  "Error: Subdomain resolution inconsistency to use DCT_NO_INTERPOLATION option",
                  return(1) );
     }

    /*******************************   DIM1   *******************************/
     /*Mod1npts1 = CommSched->VarRcvdNpts[0]; */
     Mod2npts[0] = CommSched->VarNpts[0];
     IniPos[0] = CommSched->VarIniPos[0];
     /** Determining the limits in x-direction **/
     if ( ConsDomType[0] == DCT_CARTESIAN ) {
        ierr = DCT_IncludedIndex_Cartes( ConsLabels[0], IniPos[0], Mod2npts[0], ProdLabels[0][0],
                                        ProdLabels[0][CommSched->VarRcvdNpts[0]-1],
                                        (RelIniPos+0), (VarNpts+0), NULL, 0 );
        DCTERROR( ierr == 0 , "Error: Subdomain obtained by the broker is out of boundary",
                  return(1) );
        DCTERROR( VarNpts[0] != CommSched->VarRcvdNpts[0] ,
                  "Error: Subdomain resolution inconsistency to use DCT_NO_INTERPOLATION option",
                  return(1) );
     }
     else {
        ierr = DCT_IncludedIndex_Rectil( ConsLabels[0], IniPos[0], Mod2npts[0], ProdLabels[0][0],
                                          ProdLabels[0][CommSched->VarRcvdNpts[0]-1],
                                          (RelIniPos+0), (VarNpts+0), NULL, 0 );
        DCTERROR( ierr == 0 , "Error: Subdomain obtained by the broker is out of boundary",
                  return(1) );
        for (ii = 0; ((ii < VarNpts[0]) && (kk)); ii++)
           kk = (ConsLabels[0][ii] == ProdLabels[0][ii] ? 1: 0);

        DCTERROR( (VarNpts[0] != CommSched->VarRcvdNpts[0]) || (kk!=1) ,
                  "Error: Subdomain resolution inconsistency to use DCT_NO_INTERPOLATION option",
                  return(1) );
     }
   
   }  /**** End of switch (dim)  ****/


   ierr = 0; /* reset the error variable */

   /** The initial position is relative to temporal array **/
   for (ii=0; ii<dim; ii++)
     RelIniPos[ii] = RelIniPos[ii] - IniPos[ii];

   /* Check if the mask is needed */
   needmask = 0;
   for (ii=0; ii<dim; ii++)
      needmask +=  ( ((RelIniPos[ii]==0) && (VarNpts[ii] == Mod2npts[ii]))? 0 : 1);
   
   /* No masking needed in this step */
   /*if ( (RelIniPos[0]==0) && (RelIniPos[1]==0) && (VarNpts[0] == Mod2npts[0]) &&
                                              (VarNpts[1] == Mod2npts[1]) ) {
      CommSched->VarMask = (DCT_Integer *)NULL;
   }
   else The CommSched->VarMask is supposed to be initialized */
   if ( needmask ) {
      VarMask = DCT_Data_Allocate( DCT_INTEGER, CommSched->VarNpts, dim, 0 );
      DCTERROR( VarMask == (DCT_Integer *)NULL, "Memory Allocation Failed",
                return(1) );      
   
      /** Setting the mask **/
      CommSched->VarMask = VarMask;
      
      if (dim == 2) {
        /********************** 2D ***********************************/
        limit = RelIniPos[0]*Mod2npts[1];
        for ( ii=0; ii < limit; ii++) *(VarMask++) = 0;
        limit = Mod2npts[1] - RelIniPos[1] - VarNpts[1];
        for ( ii=0; ii < VarNpts[0]; ii++) {
           for( jj = 0; jj < RelIniPos[1]; jj++) *(VarMask++) = 0;
           for( jj = 0; jj < VarNpts[1]; jj++) *(VarMask++) = 1;
           for( jj = 0; jj < limit; jj++) *(VarMask++) = 0;
        }
        limit = (Mod2npts[0] - RelIniPos[0] - VarNpts[0])*Mod2npts[1];
        for ( ii=0; ii < limit; ii++) *(VarMask++) = 0;
      } else if (dim == 3 ) {
        /********************** 3D ***********************************/
        limit = RelIniPos[1]*Mod2npts[1]*RelIniPos[0];
        /** All the way until the west face **/
        for ( ii=0; ii < limit; ii++) *(VarMask++) = 0;
        
        limit = (Mod2npts[1] - RelIniPos[1] - VarNpts[1])*RelIniPos[2];
        limit2 = Mod2npts[2] - RelIniPos[2] - VarNpts[2];
        limit3 = RelIniPos[1]*RelIniPos[2];
        /* starting the consumer region in ii index */
        for ( ii=0; ii < VarNpts[0]; ii++){
            /* The jj out before the consumer region  */
            for( jj = 0; jj < limit3; jj++) *(VarMask++) = 0;

            /* The jj in of the consumer region  */
            for( jj = 0; jj < VarNpts[1]; jj++) {
               for( kk = 0; kk < RelIniPos[2]; kk++) *(VarMask++) = 0;
               for( kk = 0; kk < VarNpts[2]; kk++) *(VarMask++) = 1;
               for( kk = 0; kk < limit2; kk++) *(VarMask++) = 0;            
            }

            /* The jj out after the consumer region  */
            for( jj = 0; jj < limit; jj++) *(VarMask++) = 0;

        }
        
        limit3 = Mod2npts[0] - RelIniPos[0] - VarNpts[0];
        limit = limit3*Mod2npts[1]*Mod2npts[2];
        /* All the way from the east face until the end */
        for ( ii=0; ii < limit; ii++) *(VarMask++) = 0;

      } else {
        DCTERROR( (CommSched->VarTransf) && (dim ==4), 
              "Internal Error, The function DCT_CheckNoInterpolation is not implemented for given dimension",
              return(1) );
      }
   }

   if (dtype != CommSched->VarRcvdDType ) { /* If the received and local data type are different */
      /* The memory buffer where values after interpolate/filter is allocated */
      CommSched->VarTempVal = DCT_Data_Allocate( dtype, CommSched->VarNpts, dim, 1 );
      DCTERROR( CommSched->VarTempVal == NULL,
                "Memory Allocation Failed", return(1) );
   }
   else { /* No extra temporal area is needed. Both point to that same */
      CommSched->VarTempVal = CommSched->VarRcvdVal;
   
   }

   return (ierr);
/* ------------------------------  END( DCT_CheckNoInterpolation ) */
}

/*******************************************************************/
/*                       DCT_Calculate_Weight                      */
/*                                                                 */
/*!    Calculate the filter or interpolation weigths in order to
       have them ready when the interpolations will be called

  \param[in,out] CommSched  Pointer to Consume structure.
  \param[in]    ConsLabels  Pointer to an array of length dim,
                            with pointers to tick marks arrays
                            corresponding to the producer receiving
                            subdomain.
  \param[in]    ConsLabels  Pointer to an array of length dim,
                            with pointers to tick marks arrays
                            corresponding to the consumer local
                            subdomain.
  \param[in]   ConsDomType  Array of length dim, indicating the
                            type of domain discretization.
                            \sa DCT_Domain_Type
  \param[in]   ConsTotNpts  Pointer to an array of length dim,
                            with the total number of point for
                            every consumer tick marks array.
  \param[in]         dtype  Data type of elements stored in the
                            consumer subdomain.
  \param[in]           dim  Integer with the space dimension (2, 3
                            or 4).

      \return An integer error control value with 0, if success.

*/
/*******************************************************************/
int DCT_Calculate_Weight( DCT_Consume_Plan *CommSched, DCT_Scalar **ProdLabels,
                          DCT_Scalar **ConsLabels, DCT_Domain_Type *ConsDomType,
                          DCT_Integer *ConsTotNpts, DCT_Data_Types dtype,
                          DCT_Integer dim )
{
/* ---------------------------------------------  Local Variables  */
   int  ierr = 0;
   
/* --------------------------------  BEGIN( DCT_Calculate_Weight ) */

   switch ( CommSched->VarTransf ) {
      case DCT_NO_INTERPOLATION:
         if ( ( dim == 2)|| (dim ==3) ){
            /* The internal CommSched structure is set for no interpolation option */
            ierr = DCT_CheckNoInterpolation( CommSched, ProdLabels, ConsLabels,
                                             ConsDomType, /*ConsTotNpts,*/ dtype, dim );
         } else if (dim ==4) {
            DCTERROR( (CommSched->VarTransf) && (dim ==4), 
                      "Internal Error, The no interpolation is not implemented for given dimension",
                       return(1) );
         }
         break;
      case DCT_LINEAR_INTERPOLATION:
             /* The memory buffer where values after interpolate/filter is allocated */
             CommSched->VarTempVal = DCT_Data_Allocate( dtype, CommSched->VarNpts, dim, 1 );
         DCTERROR( CommSched->VarTempVal == NULL, "Memory Allocation Failed", return(1) );
         if ( dim == 2) {
            ierr = DCT_Bilinear_Weight( CommSched, ProdLabels, ConsLabels, ConsDomType,
                                        ConsTotNpts, dtype );
         } else if (dim ==3) {
            ierr = DCT_Trilinear_Weight( CommSched, ProdLabels, ConsLabels, ConsDomType,
                                        ConsTotNpts, dtype );
         } else if (dim ==4) {
            DCTERROR( (CommSched->VarTransf) && (dim ==4), 
                      "Internal Error, No weight defined for given dimension", return(1) );
         }
         break;
      case DCT_QUAD_INTERPOLATION:
         if ( dim == 2) {
            DCTERROR( (CommSched->VarTransf) && (dim ==2), 
                      "Internal Error, No weight defined for given dimension", return(1) );
         } else if (dim ==3) {
            DCTERROR( (CommSched->VarTransf) && (dim ==3), 
                      "Internal Error, No weight defined for given dimension", return(1) );
         } else if (dim ==4) {
            DCTERROR( (CommSched->VarTransf) && (dim ==4), 
                      "Internal Error, No weight defined for given dimension", return(1) );
         }
         break;
      case DCT_CUBIC_INTERPOLATION:
         if ( dim == 2) {
            DCTERROR( (CommSched->VarTransf) && (dim ==2), 
                      "Internal Error, No weight defined for given dimension", return(1) );
         } else if (dim ==3) {
            DCTERROR( (CommSched->VarTransf) && (dim ==3), 
                      "Internal Error, No weight defined for given dimension", return(1) );
         } else if (dim ==4) {
            DCTERROR( (CommSched->VarTransf) && (dim ==4), 
                      "Internal Error, No weight defined for given dimension", return(1) );
         }
         break;
      default:
         DCTERROR( CommSched->VarTransf, "Internal Error, wrong type of transformation",
                   return(1) );
   } /* End of switch ( CommSched->VarTransf ) */

   return (ierr);
/* ----------------------------------  END( DCT_Calculate_Weight ) */
}

/*******************************************************************/
/*                        DCT_Bilinear_Interp                      */
/*                                                                 */
/*!     Call the suitable function that calculates the bilinear
        interpolation depending of the DCT_Data_Types and
        whether or not there is VarMask.

     \param[in]    Proddtype  Data type of elements stored in the
                              producer subdomain.
     \param[in]     ProdNpts  Pointer to an array of length dim,
                              with the number of point for every
                              producer tick marks array.
     \param[in]      ProdVal  Pointer to an array of the producer
                              subdomain values.
     \param[in]       Weight  Pointer to an array of length dim,
                              with pointers to the interpolation
                              weights.
     \param[in]    WeightInd  Pointer to an array of DCT_Integer
                              pointers, used to index the weights.
     \param[in]    Consdtype  Data type of elements stored in the
                              consumer subdomain.
     \param[in]      VarMask  Pointer to an array of mask values
                              to perfom Interpolation and unit
                              convertion. If mask entry is equal
                              to 1 the operation is made, and if
                              it is equal to 0, the output value
                              remains unaltered.
      \param[in]    ConsNpts  Pointer to an array of length dim,
                              with the number of point for every
                              consumer tick marks array.
      \param[in,out] ConsVal  Pointer to an array of the consumer
                              subdomain values.

      \return An integer error control value with 0, if success.

*/
/*******************************************************************/
int DCT_Bilinear_Interp( DCT_Data_Types Proddtype, DCT_Integer *ProdNpts,
                         DCT_VoidPointer ProdVal, DCT_VoidPointer *Weight,
                         DCT_Integer **WeightInd, DCT_Data_Types Consdtype,
                         DCT_Integer *VarMask, DCT_Integer *ConsNpts,
                         DCT_VoidPointer ConsVal )
{
/* ---------------------------------------------  Local Variables  */
   int  ierr = 0;
   
/* ---------------------------------  BEGIN( DCT_Bilinear_Interp ) */

  if ( Consdtype == DCT_FLOAT ) {
    if ( VarMask == (DCT_Integer *)NULL) {
      ierr = DCT_fBiLinIntSparseProd ( Proddtype, ProdNpts[1], ConsNpts[0], ConsNpts[1],
                                       (float *)Weight[0], (float *)Weight[1], WeightInd[0],
                                       WeightInd[1],  ProdVal, (float *)ConsVal );
    }
    else {
      ierr = DCT_Msk_fBiLinIntSparseProd ( Proddtype, ProdNpts[1], ConsNpts[0], ConsNpts[1],
                                       (float *)Weight[0], (float *)Weight[1], WeightInd[0],
                                       WeightInd[1],  ProdVal, VarMask, (float *)ConsVal );
    }
  } else if ( Consdtype == DCT_DOUBLE ) {
    if ( VarMask == (DCT_Integer *)NULL) {
      ierr = DCT_dBiLinIntSparseProd ( Proddtype, ProdNpts[1], ConsNpts[0], ConsNpts[1],
                                       (double *)Weight[0], (double *)Weight[1], WeightInd[0],
                                       WeightInd[1],  ProdVal, (double *)ConsVal );
    }
    else {
      ierr = DCT_Msk_dBiLinIntSparseProd ( Proddtype, ProdNpts[1], ConsNpts[0], ConsNpts[1],
                                       (double *)Weight[0], (double *)Weight[1], WeightInd[0],
                                       WeightInd[1],  ProdVal, VarMask, (double *)ConsVal );
    }
  } else if ( Consdtype == DCT_LONG_DOUBLE ) {
    if ( VarMask == (DCT_Integer *)NULL) {
      ierr = DCT_ldBiLinIntSparseProd ( Proddtype, ProdNpts[1], ConsNpts[0], ConsNpts[1],
                                        (long double *)Weight[0], (long double *)Weight[1],
                                        WeightInd[0], WeightInd[1],  ProdVal,
                                        (long double *)ConsVal );
    }
    else {
      ierr = DCT_Msk_ldBiLinIntSparseProd ( Proddtype, ProdNpts[1], ConsNpts[0], ConsNpts[1],
                                       (long double *)Weight[0], (long double *)Weight[1],
                                       WeightInd[0], WeightInd[1],  ProdVal, VarMask,
                                       (long double *)ConsVal );
    }
  } /* End of if ( Consdtype == DCT_FLOAT ) */

   return (ierr);
/* -----------------------------------  END( DCT_Bilinear_Interp ) */

}

/*******************************************************************/
/*                     DCT_fTriLinIntSparseProd                    */
/*                                                                 */
/*!    Calculates the trilinear interpolation using a separate
       sparse structures for the weights using float precision.
       The interpolation goes from Mod1 into Mod2.
       
       \param[in] Mod1DataType  Data type (float, double, long
                                double) of the producer model
                                (Mod1).
       \param[in]    Mod1npts2  Number of points along y-direction
                                of the producer (Mod1 discretization).
       \param[in]    Mod1npts3  Number of points along z-direction
                                of the producer (Mod1 discretization).
       \param[in]    Mod2npts1  Number of points along x-direction
                                of the consumer (Mod2 discretization).
       \param[in]    Mod2npts2  Number of points along y-direction
                                of the consumer (Mod2 discretization).
       \param[in]    Mod2npts3  Number of points along z-direction
                                of the consumer (Mod2 discretization).
       \param[in]          Wx1  Array of the weights calculated along
                                x-direction (t weights).
       \param[in]          Wx2  Array of the weights calculated along
                                y-direction (u weights).
       \param[in]          Wx3  Array of the weights calculated along
                                z-direction (u weights).
       \param[in]       Windx1  Array of the indices along x-direction
                                where the interpolated data is in
                                between.
       \param[in]       Windx2  Array of the indices along y-direction
                                where the interpolated data is in
                                between.
       \param[in]       Windx3  Array of the indices along z-direction
                                where the interpolated data is in
                                between.
       \param[in]        Mod1Y  Array of the actual values of the
                                producer (Mod1) variable.
       \param[out]       Mod2Y  Array of the resulting interpolated
                                values to the consumer (Mod2).

      \return Integer values with 0 if finish successfully,
              otherwise if it fails.

*/
/*******************************************************************/
int DCT_fTriLinIntSparseProd ( const DCT_Data_Types Mod1DataType, const DCT_Integer Mod1npts2,
                               const DCT_Integer Mod1npts3, const DCT_Integer Mod2npts1,
                               const DCT_Integer Mod2npts2, const DCT_Integer Mod2npts3,
                               const float *Wx1, const float *Wx2, const float *Wx3,
                               const DCT_Integer *Windx1, const DCT_Integer *Windx2,
                               const DCT_Integer *Windx3, const DCT_VoidPointer Mod1Y,
                               float *Mod2Y)
{
/* ---------------------------------------------  Local Variables  */
  int  ierr = 1;
  float t, u, v;
  float oneminusT, oneminusU, oneminusV; 
  DCT_Integer ii, jj, kk, ind, indi;
  DCT_Integer Dim23, Dim23Mod2, Mod2indI;
  
  float *fMod1Y, *foffset, *foffset2;
  double *dMod1Y, *doffset, *doffset2;
  long double *ldMod1Y, *ldoffset, *ldoffset2;
  
  float *dum, *dum2, *current;
  
/* ----------------------------  BEGIN( DCT_fTriLinIntSparseProd ) */

   Dim23 = Mod1npts2*Mod1npts3;
   Dim23Mod2 = Mod2npts2*Mod2npts3;
   /* auxiliary array to perform the intermediate y-z planes */
   dum2 = (float *)malloc(sizeof(float)*(size_t)Dim23);
   DCTERROR( dum2 == (float *) NULL, "Memory Allocation Failed", return(1) );

   /* auxiliary array to perform the matrices product without a matrix */
   dum = (float *)malloc(sizeof(float)*(size_t)Mod1npts3);
   DCTERROR( dum == (float *) NULL, "Memory Allocation Failed", return(1) );
   
   /* Matrices product F_{int} = W^{(x)} F_o W^{(y)} without use any
     intermediate matrix */
   if (Mod1DataType == DCT_FLOAT ) {
      fMod1Y = (float *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Calculate the interpolation between two y-z planes */
         t = *(Wx1 + ii);
         oneminusT = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Dim23; /* i-th y-z plane */
         foffset = fMod1Y + indi;
         foffset2 = foffset + Dim23; /* (i+1)-th y-z plane*/
         /*** Plane y-z (varying j and k) ***/
         for (jj = 0; jj < Dim23; jj++ )
               *(dum2 + jj) = oneminusT * *(foffset + jj) + t * *(foffset2 + jj);


         Mod2indI = ii*Dim23Mod2;
         for (jj = 0; jj < Mod2npts2; jj++ ) {
            /* Calculates the interpolation along j sweeping k */
            u = *(Wx2 + jj);
            oneminusU = 1.0 - u;
            ind = *(Windx2 + jj);
            indi = ind*Mod1npts3;         /* j-th row */
            //indj = indi + Mod1npts3;   /* (j+1)-th row */
            foffset = dum2 + indi;
            foffset2 = foffset + Mod1npts3;
            for (kk = 0; kk < Mod1npts3 ; kk++ ) {
               *(dum + kk) = oneminusU * *(foffset + kk) + u * *(foffset2 + kk);
            }
         
            /* Calculates the interpolation along k sweeping k */
            foffset = Mod2Y + jj*Mod2npts3 + Mod2indI; /* (i,j)-th row */
            for (kk = 0; kk < Mod2npts3 ; kk++ ) {
               v = *(Wx3 + kk);
               oneminusV = 1.0 - v;
               ind = *(Windx3 + kk);
               current = dum + ind;
               *(foffset + kk) = oneminusV * *(current) + v * *(current + 1);
            }
         } /* End of for (jj = 0; jj < Mod2npts2; jj++ ) */

      } /* End of for (ii = 0; ii < Mod2npts1; ii++ ) */
      
   } else if (Mod1DataType == DCT_DOUBLE ) {
      dMod1Y = (double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Calculate the interpolation between two y-z planes */
         t = *(Wx1 + ii);
         oneminusT = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Dim23; /* i-th y-z plane */
         doffset = dMod1Y + indi;
         doffset2 = doffset + Dim23; /* (i+1)-th y-z plane*/
         /*** Plane y-z (varying j and k) ***/
         for (jj = 0; jj < Dim23; jj++ )
               *(dum2 + jj) = oneminusT * *(doffset + jj) + t * *(doffset2 + jj);


         Mod2indI = ii*Dim23Mod2;
         for (jj = 0; jj < Mod2npts2; jj++ ) {
            /* Calculates the interpolation along j sweeping k */
            u = *(Wx2 + jj);
            oneminusU = 1.0 - u;
            ind = *(Windx2 + jj);
            indi = ind*Mod1npts3;         /* j-th row */
            //indj = indi + Mod1npts3;   /* (j+1)-th row */
            foffset = dum2 + indi;
            foffset2 = foffset + Mod1npts3;
            for (kk = 0; kk < Mod1npts3 ; kk++ ) {
               *(dum + kk) = oneminusU * *(foffset + kk) + u * *(foffset2 + kk);
            }
         
            /* Calculates the interpolation along k sweeping k */
            foffset = Mod2Y + jj*Mod2npts3 + Mod2indI; /* (i,j)-th row */
            for (kk = 0; kk < Mod2npts3 ; kk++ ) {
               v = *(Wx3 + kk);
               oneminusV = 1.0 - v;
               ind = *(Windx3 + kk);
               current = dum + ind;
               *(foffset + kk) = oneminusV * *(current) + v * *(current + 1);
            }
         } /* End of for (jj = 0; jj < Mod2npts2; jj++ ) */

      } /* End of for (ii = 0; ii < Mod2npts1; ii++ ) */
      
   }  else if (Mod1DataType == DCT_LONG_DOUBLE ) {
      ldMod1Y = (long double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Calculate the interpolation between two y-z planes */
         t = *(Wx1 + ii);
         oneminusT = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Dim23; /* i-th y-z plane */
         ldoffset = ldMod1Y + indi;
         ldoffset2 = ldoffset + Dim23; /* (i+1)-th y-z plane*/
         /*** Plane y-z (varying j and k) ***/
         for (jj = 0; jj < Dim23; jj++ )
               *(dum2 + jj) = oneminusT * *(ldoffset + jj) + t * *(ldoffset2 + jj);


         Mod2indI = ii*Dim23Mod2;
         for (jj = 0; jj < Mod2npts2; jj++ ) {
            /* Calculates the interpolation along j sweeping k */
            u = *(Wx2 + jj);
            oneminusU = 1.0 - u;
            ind = *(Windx2 + jj);
            indi = ind*Mod1npts3;         /* j-th row */
            //indj = indi + Mod1npts3;   /* (j+1)-th row */
            foffset = dum2 + indi;
            foffset2 = foffset + Mod1npts3;
            for (kk = 0; kk < Mod1npts3 ; kk++ ) {
               *(dum + kk) = oneminusU * *(foffset + kk) + u * *(foffset2 + kk);
            }
         
            /* Calculates the interpolation along k sweeping k */
            foffset = Mod2Y + jj*Mod2npts3 + Mod2indI; /* (i,j)-th row */
            for (kk = 0; kk < Mod2npts3 ; kk++ ) {
               v = *(Wx3 + kk);
               oneminusV = 1.0 - v;
               ind = *(Windx3 + kk);
               current = dum + ind;
               *(foffset + kk) = oneminusV * *(current) + v * *(current + 1);
            }
         } /* End of for (jj = 0; jj < Mod2npts2; jj++ ) */

      } /* End of for (ii = 0; ii < Mod2npts1; ii++ ) */ 
      
   } /* End of if (Mod1DataType == DCT_FLOAT ) */
  
#ifdef MEMTRK
   TAU_TRACK_MEMORY_HERE();
#endif
  
   free( dum );
   free( dum2 );
   return(ierr=0);
  
/* ------------------------------  END( DCT_fTriLinIntSparseProd ) */
}

/*******************************************************************/
/*                     DCT_dTriLinIntSparseProd                    */
/*                                                                 */
/*!    Calculates the trilinear interpolation using a separate
       sparse structures for the weights using double precision.
       The interpolation goes from Mod1 into Mod2.
       
       \param[in] Mod1DataType  Data type (float, double, long
                                double) of the producer model
                                (Mod1).
       \param[in]    Mod1npts2  Number of points along y-direction
                                of the producer (Mod1 discretization).
       \param[in]    Mod1npts3  Number of points along z-direction
                                of the producer (Mod1 discretization).
       \param[in]    Mod2npts1  Number of points along x-direction
                                of the consumer (Mod2 discretization).
       \param[in]    Mod2npts2  Number of points along y-direction
                                of the consumer (Mod2 discretization).
       \param[in]    Mod2npts3  Number of points along z-direction
                                of the consumer (Mod2 discretization).
       \param[in]          Wx1  Array of the weights calculated along
                                x-direction (t weights).
       \param[in]          Wx2  Array of the weights calculated along
                                y-direction (u weights).
       \param[in]          Wx3  Array of the weights calculated along
                                z-direction (u weights).
       \param[in]       Windx1  Array of the indices along x-direction
                                where the interpolated data is in
                                between.
       \param[in]       Windx2  Array of the indices along y-direction
                                where the interpolated data is in
                                between.
       \param[in]       Windx3  Array of the indices along z-direction
                                where the interpolated data is in
                                between.
       \param[in]        Mod1Y  Array of the actual values of the
                                producer (Mod1) variable.
       \param[out]       Mod2Y  Array of the resulting interpolated
                                values to the consumer (Mod2).

      \return Integer values with 0 if finish successfully,
              otherwise if it fails.

*/
/*******************************************************************/
int DCT_dTriLinIntSparseProd ( const DCT_Data_Types Mod1DataType, const DCT_Integer Mod1npts2,
                               const DCT_Integer Mod1npts3, const DCT_Integer Mod2npts1,
                               const DCT_Integer Mod2npts2, const DCT_Integer Mod2npts3,
                               const double *Wx1, const double *Wx2, const double *Wx3,
                               const DCT_Integer *Windx1, const DCT_Integer *Windx2,
                               const DCT_Integer *Windx3, const DCT_VoidPointer Mod1Y,
                               double *Mod2Y)
{
/* ---------------------------------------------  Local Variables  */
  int  ierr = 1;
  long double t, u, v;
  long double oneminusT, oneminusU, oneminusV; 
  DCT_Integer ii, jj, kk, ind, indi;
  DCT_Integer Dim23, Dim23Mod2, Mod2indI;
  
  float *fMod1Y, *foffset, *foffset2;
  double *dMod1Y, *doffset, *doffset2;
  long double *ldMod1Y, *ldoffset, *ldoffset2;
  
  double *dum, *dum2, *current;
  
/* ----------------------------  BEGIN( DCT_dTriLinIntSparseProd ) */

   Dim23 = Mod1npts2*Mod1npts3;
   Dim23Mod2 = Mod2npts2*Mod2npts3;
   /* auxiliary array to perform the intermediate y-z planes */
   dum2 = (double *)malloc(sizeof(double)*(size_t)Dim23);
   DCTERROR( dum2 == (double *) NULL, "Memory Allocation Failed", return(1) );

   /* auxiliary array to perform the matrices product without a matrix */
   dum = (double *)malloc(sizeof(double)*(size_t)Mod1npts3);
   DCTERROR( dum == (double *) NULL, "Memory Allocation Failed", return(1) );
   
   /* Matrices product F_{int} = W^{(x)} F_o W^{(y)} without use any
     intermediate matrix */
   if (Mod1DataType == DCT_FLOAT ) {
      fMod1Y = (float *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Calculate the interpolation between two y-z planes */
         t = *(Wx1 + ii);
         oneminusT = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Dim23; /* i-th y-z plane */
         foffset = fMod1Y + indi;
         foffset2 = foffset + Dim23; /* (i+1)-th y-z plane*/
         /*** Plane y-z (varying j and k) ***/
         for (jj = 0; jj < Dim23; jj++ )
               *(dum2 + jj) = oneminusT * *(foffset + jj) + t * *(foffset2 + jj);


         Mod2indI = ii*Dim23Mod2;
         for (jj = 0; jj < Mod2npts2; jj++ ) {
            /* Calculates the interpolation along j sweeping k */
            u = *(Wx2 + jj);
            oneminusU = 1.0 - u;
            ind = *(Windx2 + jj);
            indi = ind*Mod1npts3;         /* j-th row */
            //indj = indi + Mod1npts3;   /* (j+1)-th row */
            doffset = dum2 + indi;
            doffset2 = doffset + Mod1npts3;
            for (kk = 0; kk < Mod1npts3 ; kk++ ) {
               *(dum + kk) = oneminusU * *(doffset + kk) + u * *(doffset2 + kk);
            }
         
            /* Calculates the interpolation along k sweeping k */
            doffset = Mod2Y + jj*Mod2npts3 + Mod2indI; /* (i,j)-th row */
            for (kk = 0; kk < Mod2npts3 ; kk++ ) {
               v = *(Wx3 + kk);
               oneminusV = 1.0 - v;
               ind = *(Windx3 + kk);
               current = dum + ind;
               *(doffset + kk) = oneminusV * *(current) + v * *(current + 1);
            }
         } /* End of for (jj = 0; jj < Mod2npts2; jj++ ) */

      } /* End of for (ii = 0; ii < Mod2npts1; ii++ ) */
      
   } else if (Mod1DataType == DCT_DOUBLE ) {
      dMod1Y = (double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Calculate the interpolation between two y-z planes */
         t = *(Wx1 + ii);
         oneminusT = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Dim23; /* i-th y-z plane */
         doffset = dMod1Y + indi;
         doffset2 = doffset + Dim23; /* (i+1)-th y-z plane*/
         /*** Plane y-z (varying j and k) ***/
         for (jj = 0; jj < Dim23; jj++ )
               *(dum2 + jj) = oneminusT * *(doffset + jj) + t * *(doffset2 + jj);


         Mod2indI = ii*Dim23Mod2;
         for (jj = 0; jj < Mod2npts2; jj++ ) {
            /* Calculates the interpolation along j sweeping k */
            u = *(Wx2 + jj);
            oneminusU = 1.0 - u;
            ind = *(Windx2 + jj);
            indi = ind*Mod1npts3;         /* j-th row */
            //indj = indi + Mod1npts3;   /* (j+1)-th row */
            doffset = dum2 + indi;
            doffset2 = doffset + Mod1npts3;
            for (kk = 0; kk < Mod1npts3 ; kk++ ) {
               *(dum + kk) = oneminusU * *(doffset + kk) + u * *(doffset2 + kk);
            }
         
            /* Calculates the interpolation along k sweeping k */
            doffset = Mod2Y + jj*Mod2npts3 + Mod2indI; /* (i,j)-th row */
            for (kk = 0; kk < Mod2npts3 ; kk++ ) {
               v = *(Wx3 + kk);
               oneminusV = 1.0 - v;
               ind = *(Windx3 + kk);
               current = dum + ind;
               *(doffset + kk) = oneminusV * *(current) + v * *(current + 1);
            }
         } /* End of for (jj = 0; jj < Mod2npts2; jj++ ) */

      } /* End of for (ii = 0; ii < Mod2npts1; ii++ ) */
      
   }  else if (Mod1DataType == DCT_LONG_DOUBLE ) {
      ldMod1Y = (long double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Calculate the interpolation between two y-z planes */
         t = *(Wx1 + ii);
         oneminusT = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Dim23; /* i-th y-z plane */
         ldoffset = ldMod1Y + indi;
         ldoffset2 = ldoffset + Dim23; /* (i+1)-th y-z plane*/
         /*** Plane y-z (varying j and k) ***/
         for (jj = 0; jj < Dim23; jj++ )
               *(dum2 + jj) = oneminusT * *(ldoffset + jj) + t * *(ldoffset2 + jj);


         Mod2indI = ii*Dim23Mod2;
         for (jj = 0; jj < Mod2npts2; jj++ ) {
            /* Calculates the interpolation along j sweeping k */
            u = *(Wx2 + jj);
            oneminusU = 1.0 - u;
            ind = *(Windx2 + jj);
            indi = ind*Mod1npts3;         /* j-th row */
            //indj = indi + Mod1npts3;   /* (j+1)-th row */
            doffset = dum2 + indi;
            doffset2 = doffset + Mod1npts3;
            for (kk = 0; kk < Mod1npts3 ; kk++ ) {
               *(dum + kk) = oneminusU * *(doffset + kk) + u * *(doffset2 + kk);
            }
         
            /* Calculates the interpolation along k sweeping k */
            doffset = Mod2Y + jj*Mod2npts3 + Mod2indI; /* (i,j)-th row */
            for (kk = 0; kk < Mod2npts3 ; kk++ ) {
               v = *(Wx3 + kk);
               oneminusV = 1.0 - v;
               ind = *(Windx3 + kk);
               current = dum + ind;
               *(doffset + kk) = oneminusV * *(current) + v * *(current + 1);
            }
         } /* End of for (jj = 0; jj < Mod2npts2; jj++ ) */

      } /* End of for (ii = 0; ii < Mod2npts1; ii++ ) */ 
      
   } /* End of if (Mod1DataType == DCT_FLOAT ) */
  
#ifdef MEMTRK
   TAU_TRACK_MEMORY_HERE();
#endif
  
   free( dum );
   free( dum2 );
   return(ierr=0);
  
/* ------------------------------  END( DCT_dTriLinIntSparseProd ) */
}

/*******************************************************************/
/*                     DCT_ldTriLinIntSparseProd                   */
/*                                                                 */
/*!    Calculates the trilinear interpolation using a separate
       sparse structures for the weights using long double
       precision. The interpolation goes from Mod1 into Mod2.
       
       \param[in] Mod1DataType  Data type (float, double, long
                                double) of the producer model
                                (Mod1).
       \param[in]    Mod1npts2  Number of points along y-direction
                                of the producer (Mod1 discretization).
       \param[in]    Mod1npts3  Number of points along z-direction
                                of the producer (Mod1 discretization).
       \param[in]    Mod2npts1  Number of points along x-direction
                                of the consumer (Mod2 discretization).
       \param[in]    Mod2npts2  Number of points along y-direction
                                of the consumer (Mod2 discretization).
       \param[in]    Mod2npts3  Number of points along z-direction
                                of the consumer (Mod2 discretization).
       \param[in]          Wx1  Array of the weights calculated along
                                x-direction (t weights).
       \param[in]          Wx2  Array of the weights calculated along
                                y-direction (u weights).
       \param[in]          Wx3  Array of the weights calculated along
                                z-direction (u weights).
       \param[in]       Windx1  Array of the indices along x-direction
                                where the interpolated data is in
                                between.
       \param[in]       Windx2  Array of the indices along y-direction
                                where the interpolated data is in
                                between.
       \param[in]       Windx3  Array of the indices along z-direction
                                where the interpolated data is in
                                between.
       \param[in]        Mod1Y  Array of the actual values of the
                                producer (Mod1) variable.
       \param[out]       Mod2Y  Array of the resulting interpolated
                                values to the consumer (Mod2).

      \return Integer values with 0 if finish successfully,
              otherwise if it fails.

*/
/*******************************************************************/
int DCT_ldTriLinIntSparseProd ( const DCT_Data_Types Mod1DataType, const DCT_Integer Mod1npts2,
                                const DCT_Integer Mod1npts3, const DCT_Integer Mod2npts1,
                                const DCT_Integer Mod2npts2, const DCT_Integer Mod2npts3,
                                const long double *Wx1, const long double *Wx2,
                                const long double *Wx3, const DCT_Integer *Windx1,
                                const DCT_Integer *Windx2, const DCT_Integer *Windx3,
                                const DCT_VoidPointer Mod1Y, long double *Mod2Y)
{
/* ---------------------------------------------  Local Variables  */
  int  ierr = 1;
  long double t, u, v;
  long double oneminusT, oneminusU, oneminusV; 
  DCT_Integer ii, jj, kk, ind, indi;
  DCT_Integer Dim23, Dim23Mod2, Mod2indI;
  
  float *fMod1Y, *foffset, *foffset2;
  double *dMod1Y, *doffset, *doffset2;
  long double *ldMod1Y, *ldoffset, *ldoffset2;
  
  long double *dum, *dum2, *current;
  
/* ---------------------------  BEGIN( DCT_ldTriLinIntSparseProd ) */

   Dim23 = Mod1npts2*Mod1npts3;
   Dim23Mod2 = Mod2npts2*Mod2npts3;
   /* auxiliary array to perform the intermediate y-z planes */
   dum2 = (long double *)malloc(sizeof(long double)*(size_t)Dim23);
   DCTERROR( dum2 == (long double *) NULL, "Memory Allocation Failed", return(1) );

   /* auxiliary array to perform the matrices product without a matrix */
   dum = (long double *)malloc(sizeof(long double)*(size_t)Mod1npts3);
   DCTERROR( dum == (long double *) NULL, "Memory Allocation Failed", return(1) );
   
   /* Matrices product F_{int} = W^{(x)} F_o W^{(y)} without use any
     intermediate matrix */
   if (Mod1DataType == DCT_FLOAT ) {
      fMod1Y = (float *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Calculate the interpolation between two y-z planes */
         t = *(Wx1 + ii);
         oneminusT = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Dim23; /* i-th y-z plane */
         foffset = fMod1Y + indi;
         foffset2 = foffset + Dim23; /* (i+1)-th y-z plane*/
         /*** Plane y-z (varying j and k) ***/
         for (jj = 0; jj < Dim23; jj++ )
               *(dum2 + jj) = oneminusT * *(foffset + jj) + t * *(foffset2 + jj);


         Mod2indI = ii*Dim23Mod2;
         for (jj = 0; jj < Mod2npts2; jj++ ) {
            /* Calculates the interpolation along j sweeping k */
            u = *(Wx2 + jj);
            oneminusU = 1.0 - u;
            ind = *(Windx2 + jj);
            indi = ind*Mod1npts3;         /* j-th row */
            //indj = indi + Mod1npts3;   /* (j+1)-th row */
            ldoffset = dum2 + indi;
            ldoffset2 = ldoffset + Mod1npts3;
            for (kk = 0; kk < Mod1npts3 ; kk++ ) {
               *(dum + kk) = oneminusU * *(ldoffset + kk) + u * *(ldoffset2 + kk);
            }
         
            /* Calculates the interpolation along k sweeping k */
            ldoffset = Mod2Y + jj*Mod2npts3 + Mod2indI; /* (i,j)-th row */
            for (kk = 0; kk < Mod2npts3 ; kk++ ) {
               v = *(Wx3 + kk);
               oneminusV = 1.0 - v;
               ind = *(Windx3 + kk);
               current = dum + ind;
               *(ldoffset + kk) = oneminusV * *(current) + v * *(current + 1);
            }
         } /* End of for (jj = 0; jj < Mod2npts2; jj++ ) */

      } /* End of for (ii = 0; ii < Mod2npts1; ii++ ) */
      
   } else if (Mod1DataType == DCT_DOUBLE ) {
      dMod1Y = (double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Calculate the interpolation between two y-z planes */
         t = *(Wx1 + ii);
         oneminusT = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Dim23; /* i-th y-z plane */
         doffset = dMod1Y + indi;
         doffset2 = doffset + Dim23; /* (i+1)-th y-z plane*/
         /*** Plane y-z (varying j and k) ***/
         for (jj = 0; jj < Dim23; jj++ )
               *(dum2 + jj) = oneminusT * *(doffset + jj) + t * *(doffset2 + jj);


         Mod2indI = ii*Dim23Mod2;
         for (jj = 0; jj < Mod2npts2; jj++ ) {
            /* Calculates the interpolation along j sweeping k */
            u = *(Wx2 + jj);
            oneminusU = 1.0 - u;
            ind = *(Windx2 + jj);
            indi = ind*Mod1npts3;         /* j-th row */
            //indj = indi + Mod1npts3;   /* (j+1)-th row */
            ldoffset = dum2 + indi;
            ldoffset2 = ldoffset + Mod1npts3;
            for (kk = 0; kk < Mod1npts3 ; kk++ ) {
               *(dum + kk) = oneminusU * *(ldoffset + kk) + u * *(ldoffset2 + kk);
            }
         
            /* Calculates the interpolation along k sweeping k */
            ldoffset = Mod2Y + jj*Mod2npts3 + Mod2indI; /* (i,j)-th row */
            for (kk = 0; kk < Mod2npts3 ; kk++ ) {
               v = *(Wx3 + kk);
               oneminusV = 1.0 - v;
               ind = *(Windx3 + kk);
               current = dum + ind;
               *(ldoffset + kk) = oneminusV * *(current) + v * *(current + 1);
            }
         } /* End of for (jj = 0; jj < Mod2npts2; jj++ ) */

      } /* End of for (ii = 0; ii < Mod2npts1; ii++ ) */
      
   }  else if (Mod1DataType == DCT_LONG_DOUBLE ) {
      ldMod1Y = (long double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Calculate the interpolation between two y-z planes */
         t = *(Wx1 + ii);
         oneminusT = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Dim23; /* i-th y-z plane */
         ldoffset = ldMod1Y + indi;
         ldoffset2 = ldoffset + Dim23; /* (i+1)-th y-z plane*/
         /*** Plane y-z (varying j and k) ***/
         for (jj = 0; jj < Dim23; jj++ )
               *(dum2 + jj) = oneminusT * *(ldoffset + jj) + t * *(ldoffset2 + jj);


         Mod2indI = ii*Dim23Mod2;
         for (jj = 0; jj < Mod2npts2; jj++ ) {
            /* Calculates the interpolation along j sweeping k */
            u = *(Wx2 + jj);
            oneminusU = 1.0 - u;
            ind = *(Windx2 + jj);
            indi = ind*Mod1npts3;         /* j-th row */
            //indj = indi + Mod1npts3;   /* (j+1)-th row */
            ldoffset = dum2 + indi;
            ldoffset2 = ldoffset + Mod1npts3;
            for (kk = 0; kk < Mod1npts3 ; kk++ ) {
               *(dum + kk) = oneminusU * *(ldoffset + kk) + u * *(ldoffset2 + kk);
            }
         
            /* Calculates the interpolation along k sweeping k */
            ldoffset = Mod2Y + jj*Mod2npts3 + Mod2indI; /* (i,j)-th row */
            for (kk = 0; kk < Mod2npts3 ; kk++ ) {
               v = *(Wx3 + kk);
               oneminusV = 1.0 - v;
               ind = *(Windx3 + kk);
               current = dum + ind;
               *(ldoffset + kk) = oneminusV * *(current) + v * *(current + 1);
            }
         } /* End of for (jj = 0; jj < Mod2npts2; jj++ ) */

      } /* End of for (ii = 0; ii < Mod2npts1; ii++ ) */ 
      
   } /* End of if (Mod1DataType == DCT_FLOAT ) */
  
#ifdef MEMTRK
   TAU_TRACK_MEMORY_HERE();
#endif
  
   free( dum );
   free( dum2 );
   return(ierr=0);
  
/* -----------------------------  END( DCT_ldTriLinIntSparseProd ) */
}


/*******************************************************************/
/*                   DCT_Msk_fTriLinIntSparseProd                  */
/*                                                                 */
/*!    Calculates the trilinear interpolation using a separate
       sparse structures for the weights using float precision.
       The interpolation goes from Mod1 into Mod2, and this
       function uses a Mask Array.
       
       \param[in] Mod1DataType  Data type (float, double, long
                                double) of the producer model
                                (Mod1).
       \param[in]    Mod1npts2  Number of points along y-direction
                                of the producer (Mod1 discretization).
       \param[in]    Mod1npts3  Number of points along z-direction
                                of the producer (Mod1 discretization).
       \param[in]    Mod2npts1  Number of points along x-direction
                                of the consumer (Mod2 discretization).
       \param[in]    Mod2npts2  Number of points along y-direction
                                of the consumer (Mod2 discretization).
       \param[in]    Mod2npts3  Number of points along z-direction
                                of the consumer (Mod2 discretization).
       \param[in]          Wx1  Array of the weights calculated along
                                x-direction (t weights).
       \param[in]          Wx2  Array of the weights calculated along
                                y-direction (u weights).
       \param[in]          Wx3  Array of the weights calculated along
                                z-direction (u weights).
       \param[in]       Windx1  Array of the indices along x-direction
                                where the interpolated data is in
                                between.
       \param[in]       Windx2  Array of the indices along y-direction
                                where the interpolated data is in
                                between.
       \param[in]       Windx3  Array of the indices along z-direction
                                where the interpolated data is in
                                between.
       \param[in]        Mod1Y  Array of the actual values of the
                                producer (Mod1) variable.
       \param[in]      VarMask  Array of integer values with 0's or
                                1's to mask the consumer (Mod2)
                                variable, i.e. 
                                Mod2Y = mask(new value) + (1-mak)Mod2Y.
       \param[out]       Mod2Y  Array of the resulting interpolated
                                values to the consumer (Mod2).

      \return Integer values with 0 if finish successfully,
              otherwise if it fails.

*/
/*******************************************************************/
int DCT_Msk_fTriLinIntSparseProd ( const DCT_Data_Types Mod1DataType, const DCT_Integer Mod1npts2,
                               const DCT_Integer Mod1npts3, const DCT_Integer Mod2npts1,
                               const DCT_Integer Mod2npts2, const DCT_Integer Mod2npts3,
                               const float *Wx1, const float *Wx2, const float *Wx3,
                               const DCT_Integer *Windx1, const DCT_Integer *Windx2,
                               const DCT_Integer *Windx3, const DCT_VoidPointer Mod1Y,
                               const DCT_Integer *VarMask, float *Mod2Y)
{
/* ---------------------------------------------  Local Variables  */
  int  ierr = 1;
  float t, u, v;
  float oneminusT, oneminusU, oneminusV; 
  DCT_Integer ii, jj, kk, ind, indi;
  DCT_Integer Dim23, Dim23Mod2, Mod2indI;
  DCT_Integer *mask;
  
  float *fMod1Y, *foffset, *foffset2, fmask;
  double *dMod1Y, *doffset, *doffset2;
  long double *ldMod1Y, *ldoffset, *ldoffset2;
  
  float *dum, *dum2, *current;
  
/* ------------------------  BEGIN( DCT_Msk_fTriLinIntSparseProd ) */

   Dim23 = Mod1npts2*Mod1npts3;
   Dim23Mod2 = Mod2npts2*Mod2npts3;
   /* auxiliary array to perform the intermediate y-z planes */
   dum2 = (float *)malloc(sizeof(float)*(size_t)Dim23);
   DCTERROR( dum2 == (float *) NULL, "Memory Allocation Failed", return(1) );

   /* auxiliary array to perform the matrices product without a matrix */
   dum = (float *)malloc(sizeof(float)*(size_t)Mod1npts3);
   DCTERROR( dum == (float *) NULL, "Memory Allocation Failed", return(1) );
   
   /* Matrices product F_{int} = W^{(x)} F_o W^{(y)} without use any
     intermediate matrix */
   if (Mod1DataType == DCT_FLOAT ) {
      fMod1Y = (float *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Calculate the interpolation between two y-z planes */
         t = *(Wx1 + ii);
         oneminusT = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Dim23; /* i-th y-z plane */
         foffset = fMod1Y + indi;
         foffset2 = foffset + Dim23; /* (i+1)-th y-z plane*/
         /*** Plane y-z (varying j and k) ***/
         for (jj = 0; jj < Dim23; jj++ )
               *(dum2 + jj) = oneminusT * *(foffset + jj) + t * *(foffset2 + jj);


         Mod2indI = ii*Dim23Mod2;
         for (jj = 0; jj < Mod2npts2; jj++ ) {
            /* Calculates the interpolation along j sweeping k */
            u = *(Wx2 + jj);
            oneminusU = 1.0 - u;
            ind = *(Windx2 + jj);
            indi = ind*Mod1npts3;         /* j-th row */
            //indj = indi + Mod1npts3;   /* (j+1)-th row */
            foffset = dum2 + indi;
            foffset2 = foffset + Mod1npts3;
            for (kk = 0; kk < Mod1npts3 ; kk++ ) {
               *(dum + kk) = oneminusU * *(foffset + kk) + u * *(foffset2 + kk);
            }
         
            /* Calculates the interpolation along k sweeping k */
            indi = jj*Mod2npts3 + Mod2indI; /* (i,j)-th row */
            foffset = Mod2Y + indi;
            mask = VarMask + indi;
            for (kk = 0; kk < Mod2npts3 ; kk++ ) {
               v = *(Wx3 + kk);
               oneminusV = 1.0 - v;
               ind = *(Windx3 + kk);
               fmask = (float)*(mask + kk);
               current = dum + ind;
               *(foffset + kk) = fmask*( oneminusV * *(current) + v * *(current + 1) ) +
                            (1.0 - fmask) * *(foffset + kk);
                            
            }
         } /* End of for (jj = 0; jj < Mod2npts2; jj++ ) */

      } /* End of for (ii = 0; ii < Mod2npts1; ii++ ) */
      
   } else if (Mod1DataType == DCT_DOUBLE ) {
      dMod1Y = (double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Calculate the interpolation between two y-z planes */
         t = *(Wx1 + ii);
         oneminusT = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Dim23; /* i-th y-z plane */
         doffset = dMod1Y + indi;
         doffset2 = doffset + Dim23; /* (i+1)-th y-z plane*/
         /*** Plane y-z (varying j and k) ***/
         for (jj = 0; jj < Dim23; jj++ )
               *(dum2 + jj) = oneminusT * *(doffset + jj) + t * *(doffset2 + jj);


         Mod2indI = ii*Dim23Mod2;
         for (jj = 0; jj < Mod2npts2; jj++ ) {
            /* Calculates the interpolation along j sweeping k */
            u = *(Wx2 + jj);
            oneminusU = 1.0 - u;
            ind = *(Windx2 + jj);
            indi = ind*Mod1npts3;         /* j-th row */
            //indj = indi + Mod1npts3;   /* (j+1)-th row */
            foffset = dum2 + indi;
            foffset2 = foffset + Mod1npts3;
            for (kk = 0; kk < Mod1npts3 ; kk++ ) {
               *(dum + kk) = oneminusU * *(foffset + kk) + u * *(foffset2 + kk);
            }
         
            /* Calculates the interpolation along k sweeping k */
            indi = jj*Mod2npts3 + Mod2indI; /* (i,j)-th row */
            foffset = Mod2Y + indi;
            mask = VarMask + indi;
            for (kk = 0; kk < Mod2npts3 ; kk++ ) {
               v = *(Wx3 + kk);
               oneminusV = 1.0 - v;
               ind = *(Windx3 + kk);
               fmask = (float)*(mask + kk);
               current = dum + ind;
               *(foffset + kk) = fmask*( oneminusV * *(current) + v * *(current + 1) ) +
                            (1.0 - fmask) * *(foffset + kk);
            }
         } /* End of for (jj = 0; jj < Mod2npts2; jj++ ) */

      } /* End of for (ii = 0; ii < Mod2npts1; ii++ ) */
      
   }  else if (Mod1DataType == DCT_LONG_DOUBLE ) {
      ldMod1Y = (long double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Calculate the interpolation between two y-z planes */
         t = *(Wx1 + ii);
         oneminusT = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Dim23; /* i-th y-z plane */
         ldoffset = ldMod1Y + indi;
         ldoffset2 = ldoffset + Dim23; /* (i+1)-th y-z plane*/
         /*** Plane y-z (varying j and k) ***/
         for (jj = 0; jj < Dim23; jj++ )
               *(dum2 + jj) = oneminusT * *(ldoffset + jj) + t * *(ldoffset2 + jj);


         Mod2indI = ii*Dim23Mod2;
         for (jj = 0; jj < Mod2npts2; jj++ ) {
            /* Calculates the interpolation along j sweeping k */
            u = *(Wx2 + jj);
            oneminusU = 1.0 - u;
            ind = *(Windx2 + jj);
            indi = ind*Mod1npts3;         /* j-th row */
            //indj = indi + Mod1npts3;   /* (j+1)-th row */
            foffset = dum2 + indi;
            foffset2 = foffset + Mod1npts3;
            for (kk = 0; kk < Mod1npts3 ; kk++ ) {
               *(dum + kk) = oneminusU * *(foffset + kk) + u * *(foffset2 + kk);
            }
         
            /* Calculates the interpolation along k sweeping k */
            indi = jj*Mod2npts3 + Mod2indI; /* (i,j)-th row */
            foffset = Mod2Y + indi;
            mask = VarMask + indi;
            for (kk = 0; kk < Mod2npts3 ; kk++ ) {
               v = *(Wx3 + kk);
               oneminusV = 1.0 - v;
               ind = *(Windx3 + kk);
               fmask = (float)*(mask + kk);
               current = dum + ind;
               *(foffset + kk) = fmask*( oneminusV * *(current) + v * *(current + 1) ) +
                            (1.0 - fmask) * *(foffset + kk);
            }
         } /* End of for (jj = 0; jj < Mod2npts2; jj++ ) */

      } /* End of for (ii = 0; ii < Mod2npts1; ii++ ) */ 
      
   } /* End of if (Mod1DataType == DCT_FLOAT ) */
  
#ifdef MEMTRK
   TAU_TRACK_MEMORY_HERE();
#endif
  
   free( dum );
   free( dum2 );
   return(ierr=0);
  
/* --------------------------  END( DCT_Msk_fTriLinIntSparseProd ) */
}

/*******************************************************************/
/*                   DCT_Msk_dTriLinIntSparseProd                  */
/*                                                                 */
/*!    Calculates the trilinear interpolation using a separate
       sparse structures for the weights using double precision.
       The interpolation goes from Mod1 into Mod2, and this
       function uses a Mask Array.
       
       \param[in] Mod1DataType  Data type (float, double, long
                                double) of the producer model
                                (Mod1).
       \param[in]    Mod1npts2  Number of points along y-direction
                                of the producer (Mod1 discretization).
       \param[in]    Mod1npts3  Number of points along z-direction
                                of the producer (Mod1 discretization).
       \param[in]    Mod2npts1  Number of points along x-direction
                                of the consumer (Mod2 discretization).
       \param[in]    Mod2npts2  Number of points along y-direction
                                of the consumer (Mod2 discretization).
       \param[in]    Mod2npts3  Number of points along z-direction
                                of the consumer (Mod2 discretization).
       \param[in]          Wx1  Array of the weights calculated along
                                x-direction (t weights).
       \param[in]          Wx2  Array of the weights calculated along
                                y-direction (u weights).
       \param[in]          Wx3  Array of the weights calculated along
                                z-direction (u weights).
       \param[in]       Windx1  Array of the indices along x-direction
                                where the interpolated data is in
                                between.
       \param[in]       Windx2  Array of the indices along y-direction
                                where the interpolated data is in
                                between.
       \param[in]       Windx3  Array of the indices along z-direction
                                where the interpolated data is in
                                between.
       \param[in]        Mod1Y  Array of the actual values of the
                                producer (Mod1) variable.
       \param[in]      VarMask  Array of integer values with 0's or
                                1's to mask the consumer (Mod2)
                                variable, i.e. 
                                Mod2Y = mask(new value) + (1-mak)Mod2Y.
       \param[out]       Mod2Y  Array of the resulting interpolated
                                values to the consumer (Mod2).

      \return Integer values with 0 if finish successfully,
              otherwise if it fails.
              
      \todo Check all the Mask trails, specially this.

*/
/*******************************************************************/
int DCT_Msk_dTriLinIntSparseProd ( const DCT_Data_Types Mod1DataType, const DCT_Integer Mod1npts2,
                               const DCT_Integer Mod1npts3, const DCT_Integer Mod2npts1,
                               const DCT_Integer Mod2npts2, const DCT_Integer Mod2npts3,
                               const double *Wx1, const double *Wx2, const double *Wx3,
                               const DCT_Integer *Windx1, const DCT_Integer *Windx2,
                               const DCT_Integer *Windx3, const DCT_VoidPointer Mod1Y,
                               const DCT_Integer *VarMask, double *Mod2Y)
{
/* ---------------------------------------------  Local Variables  */
  int  ierr = 1;
  double t, u, v;
  double oneminusT, oneminusU, oneminusV; 
  DCT_Integer ii, jj, kk, ind, indi;
  DCT_Integer Dim23, Dim23Mod2, Mod2indI;
  DCT_Integer *mask;
  
  float *fMod1Y, *foffset, *foffset2;
  double *dMod1Y, *doffset, *doffset2, dmask;
  long double *ldMod1Y, *ldoffset, *ldoffset2;
  
  double *dum, *dum2, *current;
  
/* ------------------------  BEGIN( DCT_Msk_dTriLinIntSparseProd ) */

   Dim23 = Mod1npts2*Mod1npts3;
   Dim23Mod2 = Mod2npts2*Mod2npts3;
   /* auxiliary array to perform the intermediate y-z planes */
   dum2 = (double *)malloc(sizeof(double)*(size_t)Dim23);
   DCTERROR( dum2 == (double *) NULL, "Memory Allocation Failed", return(1) );

   /* auxiliary array to perform the matrices product without a matrix */
   dum = (double *)malloc(sizeof(double)*(size_t)Mod1npts3);
   DCTERROR( dum == (double *) NULL, "Memory Allocation Failed", return(1) );
   
   /* Matrices product F_{int} = W^{(x)} F_o W^{(y)} without use any
     intermediate matrix */
   if (Mod1DataType == DCT_FLOAT ) {
      fMod1Y = (float *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Calculate the interpolation between two y-z planes */
         t = *(Wx1 + ii);
         oneminusT = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Dim23; /* i-th y-z plane */
         foffset = fMod1Y + indi;
         foffset2 = foffset + Dim23; /* (i+1)-th y-z plane*/
         /*** Plane y-z (varying j and k) ***/
         for (jj = 0; jj < Dim23; jj++ )
               *(dum2 + jj) = oneminusT * *(foffset + jj) + t * *(foffset2 + jj);


         Mod2indI = ii*Dim23Mod2;
         for (jj = 0; jj < Mod2npts2; jj++ ) {
            /* Calculates the interpolation along j sweeping k */
            u = *(Wx2 + jj);
            oneminusU = 1.0 - u;
            ind = *(Windx2 + jj);
            indi = ind*Mod1npts3;         /* j-th row */
            //indj = indi + Mod1npts3;   /* (j+1)-th row */
            doffset = dum2 + indi;
            doffset2 = doffset + Mod1npts3;
            for (kk = 0; kk < Mod1npts3 ; kk++ ) {
               *(dum + kk) = oneminusU * *(doffset + kk) + u * *(doffset2 + kk);
            }
         
            /* Calculates the interpolation along k sweeping k */
            indi = jj*Mod2npts3 + Mod2indI; /* (i,j)-th row */
            doffset = Mod2Y + indi;
            mask = VarMask + indi;
            for (kk = 0; kk < Mod2npts3 ; kk++ ) {
               v = *(Wx3 + kk);
               oneminusV = 1.0 - v;
               ind = *(Windx3 + kk);
               dmask = (double)*(mask + kk);
               current = dum + ind;
               *(doffset + kk) = dmask*( oneminusV * *(current) + v * *(current + 1) ) +
                            (1.0 - dmask) * *(doffset + kk);
            }
         } /* End of for (jj = 0; jj < Mod2npts2; jj++ ) */

      } /* End of for (ii = 0; ii < Mod2npts1; ii++ ) */
      
   } else if (Mod1DataType == DCT_DOUBLE ) {
      dMod1Y = (double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Calculate the interpolation between two y-z planes */
         t = *(Wx1 + ii);
         oneminusT = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Dim23; /* i-th y-z plane */
         doffset = dMod1Y + indi;
         doffset2 = doffset + Dim23; /* (i+1)-th y-z plane*/
         /*** Plane y-z (varying j and k) ***/
         for (jj = 0; jj < Dim23; jj++ )
               *(dum2 + jj) = oneminusT * *(doffset + jj) + t * *(doffset2 + jj);


         Mod2indI = ii*Dim23Mod2;
         for (jj = 0; jj < Mod2npts2; jj++ ) {
            /* Calculates the interpolation along j sweeping k */
            u = *(Wx2 + jj);
            oneminusU = 1.0 - u;
            ind = *(Windx2 + jj);
            indi = ind*Mod1npts3;         /* j-th row */
            //indj = indi + Mod1npts3;   /* (j+1)-th row */
            doffset = dum2 + indi;
            doffset2 = doffset + Mod1npts3;
            for (kk = 0; kk < Mod1npts3 ; kk++ ) {
               *(dum + kk) = oneminusU * *(doffset + kk) + u * *(doffset2 + kk);
            }
         
            /* Calculates the interpolation along k sweeping k */
            indi = jj*Mod2npts3 + Mod2indI; /* (i,j)-th row */
            doffset = Mod2Y + indi;
            mask = VarMask + indi;
            for (kk = 0; kk < Mod2npts3 ; kk++ ) {
               v = *(Wx3 + kk);
               oneminusV = 1.0 - v;
               ind = *(Windx3 + kk);
               dmask = (double)*(mask + kk);
               current = dum + ind;
               *(doffset + kk) = dmask*( oneminusV * *(current) + v * *(current + 1) ) +
                            (1.0 - dmask) * *(doffset + kk);
            }
         } /* End of for (jj = 0; jj < Mod2npts2; jj++ ) */

      } /* End of for (ii = 0; ii < Mod2npts1; ii++ ) */
      
   }  else if (Mod1DataType == DCT_LONG_DOUBLE ) {
      ldMod1Y = (long double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Calculate the interpolation between two y-z planes */
         t = *(Wx1 + ii);
         oneminusT = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Dim23; /* i-th y-z plane */
         ldoffset = ldMod1Y + indi;
         ldoffset2 = ldoffset + Dim23; /* (i+1)-th y-z plane*/
         /*** Plane y-z (varying j and k) ***/
         for (jj = 0; jj < Dim23; jj++ )
               *(dum2 + jj) = oneminusT * *(ldoffset + jj) + t * *(ldoffset2 + jj);


         Mod2indI = ii*Dim23Mod2;
         for (jj = 0; jj < Mod2npts2; jj++ ) {
            /* Calculates the interpolation along j sweeping k */
            u = *(Wx2 + jj);
            oneminusU = 1.0 - u;
            ind = *(Windx2 + jj);
            indi = ind*Mod1npts3;         /* j-th row */
            //indj = indi + Mod1npts3;   /* (j+1)-th row */
            doffset = dum2 + indi;
            doffset2 = doffset + Mod1npts3;
            for (kk = 0; kk < Mod1npts3 ; kk++ ) {
               *(dum + kk) = oneminusU * *(doffset + kk) + u * *(doffset2 + kk);
            }
         
            /* Calculates the interpolation along k sweeping k */
            indi = jj*Mod2npts3 + Mod2indI; /* (i,j)-th row */
            doffset = Mod2Y + indi;
            mask = VarMask + indi;
            for (kk = 0; kk < Mod2npts3 ; kk++ ) {
               v = *(Wx3 + kk);
               oneminusV = 1.0 - v;
               ind = *(Windx3 + kk);
               dmask = (double)*(mask + kk);
               current = dum + ind;
               *(doffset + kk) = dmask*( oneminusV * *(current) + v * *(current + 1) ) +
                            (1.0 - dmask) * *(doffset + kk);
            }
         } /* End of for (jj = 0; jj < Mod2npts2; jj++ ) */

      } /* End of for (ii = 0; ii < Mod2npts1; ii++ ) */ 
      
   } /* End of if (Mod1DataType == DCT_FLOAT ) */
  
#ifdef MEMTRK
   TAU_TRACK_MEMORY_HERE();
#endif
  
   free( dum );
   free( dum2 );
   return(ierr=0);
  
/* --------------------------  END( DCT_Msk_dTriLinIntSparseProd ) */
}

/*******************************************************************/
/*                   DCT_Msk_ldTriLinIntSparseProd                 */
/*                                                                 */
/*!    Calculates the trilinear interpolation using a separate
       sparse structures for the weights using long double
       precision. The interpolation goes from Mod1 into Mod2,
       and this function uses a Mask Array.
       
       \param[in] Mod1DataType  Data type (float, double, long
                                double) of the producer model
                                (Mod1).
       \param[in]    Mod1npts2  Number of points along y-direction
                                of the producer (Mod1 discretization).
       \param[in]    Mod1npts3  Number of points along z-direction
                                of the producer (Mod1 discretization).
       \param[in]    Mod2npts1  Number of points along x-direction
                                of the consumer (Mod2 discretization).
       \param[in]    Mod2npts2  Number of points along y-direction
                                of the consumer (Mod2 discretization).
       \param[in]    Mod2npts3  Number of points along z-direction
                                of the consumer (Mod2 discretization).
       \param[in]          Wx1  Array of the weights calculated along
                                x-direction (t weights).
       \param[in]          Wx2  Array of the weights calculated along
                                y-direction (u weights).
       \param[in]          Wx3  Array of the weights calculated along
                                z-direction (u weights).
       \param[in]       Windx1  Array of the indices along x-direction
                                where the interpolated data is in
                                between.
       \param[in]       Windx2  Array of the indices along y-direction
                                where the interpolated data is in
                                between.
       \param[in]       Windx3  Array of the indices along z-direction
                                where the interpolated data is in
                                between.
       \param[in]        Mod1Y  Array of the actual values of the
                                producer (Mod1) variable.
       \param[in]      VarMask  Array of integer values with 0's or
                                1's to mask the consumer (Mod2)
                                variable, i.e. 
                                Mod2Y = mask(new value) + (1-mak)Mod2Y.
       \param[out]       Mod2Y  Array of the resulting interpolated
                                values to the consumer (Mod2).

      \return Integer values with 0 if finish successfully,
              otherwise if it fails.

*/
/*******************************************************************/
int DCT_Msk_ldTriLinIntSparseProd ( const DCT_Data_Types Mod1DataType, const DCT_Integer Mod1npts2,
                                const DCT_Integer Mod1npts3, const DCT_Integer Mod2npts1,
                                const DCT_Integer Mod2npts2, const DCT_Integer Mod2npts3,
                                const long double *Wx1, const long double *Wx2,
                                const long double *Wx3, const DCT_Integer *Windx1,
                                const DCT_Integer *Windx2, const DCT_Integer *Windx3,
                                const DCT_VoidPointer Mod1Y, const DCT_Integer *VarMask,
                                long double *Mod2Y)
{
/* ---------------------------------------------  Local Variables  */
  int  ierr = 1;
  long double t, u, v;
  long double oneminusT, oneminusU, oneminusV; 
  DCT_Integer ii, jj, kk, ind, indi;
  DCT_Integer Dim23, Dim23Mod2, Mod2indI;
  DCT_Integer *mask;
  
  float *fMod1Y, *foffset, *foffset2;
  double *dMod1Y, *doffset, *doffset2;
  long double *ldMod1Y, *ldoffset, *ldoffset2, ldmask;
  
  long double *dum, *dum2, *current;
  
/* -----------------------  BEGIN( DCT_Msk_ldTriLinIntSparseProd ) */

   Dim23 = Mod1npts2*Mod1npts3;
   Dim23Mod2 = Mod2npts2*Mod2npts3;
   /* auxiliary array to perform the intermediate y-z planes */
   dum2 = (long double *)malloc(sizeof(long double)*(size_t)Dim23);
   DCTERROR( dum2 == (long double *) NULL, "Memory Allocation Failed", return(1) );

   /* auxiliary array to perform the matrices product without a matrix */
   dum = (long double *)malloc(sizeof(long double)*(size_t)Mod1npts3);
   DCTERROR( dum == (long double *) NULL, "Memory Allocation Failed", return(1) );
   
   /* Matrices product F_{int} = W^{(x)} F_o W^{(y)} without use any
     intermediate matrix */
   if (Mod1DataType == DCT_FLOAT ) {
      fMod1Y = (float *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Calculate the interpolation between two y-z planes */
         t = *(Wx1 + ii);
         oneminusT = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Dim23; /* i-th y-z plane */
         foffset = fMod1Y + indi;
         foffset2 = foffset + Dim23; /* (i+1)-th y-z plane*/
         /*** Plane y-z (varying j and k) ***/
         for (jj = 0; jj < Dim23; jj++ )
               *(dum2 + jj) = oneminusT * *(foffset + jj) + t * *(foffset2 + jj);


         Mod2indI = ii*Dim23Mod2;
         for (jj = 0; jj < Mod2npts2; jj++ ) {
            /* Calculates the interpolation along j sweeping k */
            u = *(Wx2 + jj);
            oneminusU = 1.0 - u;
            ind = *(Windx2 + jj);
            indi = ind*Mod1npts3;         /* j-th row */
            //indj = indi + Mod1npts3;   /* (j+1)-th row */
            ldoffset = dum2 + indi;
            ldoffset2 = ldoffset + Mod1npts3;
            for (kk = 0; kk < Mod1npts3 ; kk++ ) {
               *(dum + kk) = oneminusU * *(ldoffset + kk) + u * *(ldoffset2 + kk);
            }
         
            /* Calculates the interpolation along k sweeping k */
            indi = jj*Mod2npts3 + Mod2indI; /* (i,j)-th row */
            ldoffset = Mod2Y + indi;
            mask = VarMask + indi;
            for (kk = 0; kk < Mod2npts3 ; kk++ ) {
               v = *(Wx3 + kk);
               oneminusV = 1.0 - v;
               ind = *(Windx3 + kk);
               ldmask = (long double)*(mask + kk);
               current = dum + ind;
               *(ldoffset + kk) = ldmask*( oneminusV * *(current) + v * *(current + 1) ) +
                            (1.0 - ldmask) * *(ldoffset + kk);
            }
         } /* End of for (jj = 0; jj < Mod2npts2; jj++ ) */

      } /* End of for (ii = 0; ii < Mod2npts1; ii++ ) */
      
   } else if (Mod1DataType == DCT_DOUBLE ) {
      dMod1Y = (double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Calculate the interpolation between two y-z planes */
         t = *(Wx1 + ii);
         oneminusT = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Dim23; /* i-th y-z plane */
         doffset = dMod1Y + indi;
         doffset2 = doffset + Dim23; /* (i+1)-th y-z plane*/
         /*** Plane y-z (varying j and k) ***/
         for (jj = 0; jj < Dim23; jj++ )
               *(dum2 + jj) = oneminusT * *(doffset + jj) + t * *(doffset2 + jj);


         Mod2indI = ii*Dim23Mod2;
         for (jj = 0; jj < Mod2npts2; jj++ ) {
            /* Calculates the interpolation along j sweeping k */
            u = *(Wx2 + jj);
            oneminusU = 1.0 - u;
            ind = *(Windx2 + jj);
            indi = ind*Mod1npts3;         /* j-th row */
            //indj = indi + Mod1npts3;   /* (j+1)-th row */
            ldoffset = dum2 + indi;
            ldoffset2 = ldoffset + Mod1npts3;
            for (kk = 0; kk < Mod1npts3 ; kk++ ) {
               *(dum + kk) = oneminusU * *(ldoffset + kk) + u * *(ldoffset2 + kk);
            }
         
            /* Calculates the interpolation along k sweeping k */
            indi = jj*Mod2npts3 + Mod2indI; /* (i,j)-th row */
            ldoffset = Mod2Y + indi;
            mask = VarMask + indi;
            for (kk = 0; kk < Mod2npts3 ; kk++ ) {
               v = *(Wx3 + kk);
               oneminusV = 1.0 - v;
               ind = *(Windx3 + kk);
               ldmask = (long double)*(mask + kk);
               current = dum + ind;
               *(ldoffset + kk) = ldmask*( oneminusV * *(current) + v * *(current + 1) ) +
                            (1.0 - ldmask) * *(ldoffset + kk);
            }
         } /* End of for (jj = 0; jj < Mod2npts2; jj++ ) */

      } /* End of for (ii = 0; ii < Mod2npts1; ii++ ) */
      
   }  else if (Mod1DataType == DCT_LONG_DOUBLE ) {
      ldMod1Y = (long double *) Mod1Y;
      for (ii = 0; ii < Mod2npts1; ii++ ){
         /* Calculate the interpolation between two y-z planes */
         t = *(Wx1 + ii);
         oneminusT = 1.0 - t;
         ind = *(Windx1 + ii);
         indi = ind*Dim23; /* i-th y-z plane */
         ldoffset = ldMod1Y + indi;
         ldoffset2 = ldoffset + Dim23; /* (i+1)-th y-z plane*/
         /*** Plane y-z (varying j and k) ***/
         for (jj = 0; jj < Dim23; jj++ )
               *(dum2 + jj) = oneminusT * *(ldoffset + jj) + t * *(ldoffset2 + jj);


         Mod2indI = ii*Dim23Mod2;
         for (jj = 0; jj < Mod2npts2; jj++ ) {
            /* Calculates the interpolation along j sweeping k */
            u = *(Wx2 + jj);
            oneminusU = 1.0 - u;
            ind = *(Windx2 + jj);
            indi = ind*Mod1npts3;         /* j-th row */
            //indj = indi + Mod1npts3;   /* (j+1)-th row */
            ldoffset = dum2 + indi;
            ldoffset2 = ldoffset + Mod1npts3;
            for (kk = 0; kk < Mod1npts3 ; kk++ ) {
               *(dum + kk) = oneminusU * *(ldoffset + kk) + u * *(ldoffset2 + kk);
            }
         
            /* Calculates the interpolation along k sweeping k */
            indi = jj*Mod2npts3 + Mod2indI; /* (i,j)-th row */
            ldoffset = Mod2Y + indi;
            mask = VarMask + indi;
            for (kk = 0; kk < Mod2npts3 ; kk++ ) {
               v = *(Wx3 + kk);
               oneminusV = 1.0 - v;
               ind = *(Windx3 + kk);
               ldmask = (long double)*(mask + kk);
               current = dum + ind;
               *(ldoffset + kk) = ldmask*( oneminusV * *(current) + v * *(current + 1) ) +
                            (1.0 - ldmask) * *(ldoffset + kk);
            }
         } /* End of for (jj = 0; jj < Mod2npts2; jj++ ) */

      } /* End of for (ii = 0; ii < Mod2npts1; ii++ ) */ 
      
   } /* End of if (Mod1DataType == DCT_FLOAT ) */
  
#ifdef MEMTRK
   TAU_TRACK_MEMORY_HERE();
#endif
  
   free( dum );
   free( dum2 );
   return(ierr=0);
  
/* -------------------------  END( DCT_Msk_ldTriLinIntSparseProd ) */
}

/*******************************************************************/
/*                        DCT_Trilinear_Interp                     */
/*                                                                 */
/*!     Call the suitable function that calculates the trilinear
        interpolation depending of the DCT_Data_Types and
        whether or not there is VarMask.

     \param[in]    Proddtype  Data type of elements stored in the
                              producer subdomain.
     \param[in]     ProdNpts  Pointer to an array of length dim,
                              with the number of point for every
                              producer tick marks array.
     \param[in]      ProdVal  Pointer to an array of the producer
                              subdomain values.
     \param[in]       Weight  Pointer to an array of length dim,
                              with pointers to the interpolation
                              weights.
     \param[in]    WeightInd  Pointer to an array of DCT_Integer
                              pointers, used to index the weights.
     \param[in]    Consdtype  Data type of elements stored in the
                              consumer subdomain.
     \param[in]      VarMask  Pointer to an array of mask values
                              to perfom Interpolation and unit
                              convertion. If mask entry is equal
                              to 1 the operation is made, and if
                              it is equal to 0, the output value
                              remains unaltered.
      \param[in]    ConsNpts  Pointer to an array of length dim,
                              with the number of point for every
                              consumer tick marks array.
      \param[in,out] ConsVal  Pointer to an array of the consumer
                              subdomain values.

      \return An integer error control value with 0, if success.

*/
/****************************************************************/
int DCT_Trilinear_Interp( DCT_Data_Types Proddtype, DCT_Integer *ProdNpts,
                          DCT_VoidPointer ProdVal, DCT_VoidPointer *Weight,
                          DCT_Integer **WeightInd, DCT_Data_Types Consdtype,
                          DCT_Integer *VarMask, DCT_Integer *ConsNpts,
                          DCT_VoidPointer ConsVal )
{
/* ---------------------------------------------  Local Variables  */
   int  ierr = 0;
   
/* ---------------------------------  BEGIN( DCT_Trilinear_Interp ) */

  if ( Consdtype == DCT_FLOAT ) {
    if ( VarMask == (DCT_Integer *)NULL) {
      ierr = DCT_fTriLinIntSparseProd ( Proddtype, ProdNpts[1], ProdNpts[2],
                                        ConsNpts[0], ConsNpts[1], ConsNpts[2],
                                        (float *)Weight[0], (float *)Weight[1],
                                        (float *)Weight[2], WeightInd[0],
                                        WeightInd[1], WeightInd[2], ProdVal,
                                        (float *)ConsVal );

    }
    else {
      /* 
ierr = DCT_Msk_fTriLinIntSparseProd ( ProdNpts, ProdVal, Weight, WeightInd,
                                            ConsNpts, ConsVal, Proddtype );
 */
    }
  } else if ( Consdtype == DCT_DOUBLE ) {
    if ( VarMask == (DCT_Integer *)NULL) {
      ierr = DCT_dTriLinIntSparseProd ( Proddtype, ProdNpts[1], ProdNpts[2],
                                        ConsNpts[0], ConsNpts[1], ConsNpts[2],
                                        (double *)Weight[0], (double *)Weight[1],
                                        (double *)Weight[2], WeightInd[0],
                                        WeightInd[1], WeightInd[2], ProdVal,
                                        (double *)ConsVal );
    }
    else {
      /* 
ierr = DCT_Msk_dTriLinIntSparseProd ( ProdNpts, ProdVal, Weight, WeightInd,
                                            ConsNpts, ConsVal, Proddtype );
 */
    }
  } else if ( Consdtype == DCT_LONG_DOUBLE ) {
    if ( VarMask == (DCT_Integer *)NULL) {
      ierr = DCT_ldTriLinIntSparseProd( Proddtype, ProdNpts[1], ProdNpts[2],
                                        ConsNpts[0], ConsNpts[1], ConsNpts[2],
                                        (long double *)Weight[0], (long double *)Weight[1],
                                        (long double *)Weight[2], WeightInd[0],
                                        WeightInd[1], WeightInd[2], ProdVal,
                                        (long double *)ConsVal );
    }
    else {
      /* 
ierr = DCT_Msk_ldTriLinIntSparseProd ( ProdNpts, ProdVal, Weight, WeightInd,
                                            ConsNpts, ConsVal, Proddtype );
 */
    }
  } /* End of if ( Consdtype == DCT_FLOAT ) */

   return (ierr);
/* -----------------------------------  END( DCT_Trilinear_Interp ) */
}

/*******************************************************************/
/*                     longdouble_UnitConvertion                   */
/*                                                                 */
/*!     Calculate the convertion as linear operation defined
        VarValue = factor*Values + trasl using long double
        precision.

     \param[in]         tdata  Pointer to a DCT_Consume_Plan
                               structure.
     \param[in,out] VarValues  Pointer to user's data array.
     \param[in]        factor  Value of the fator to multiply the
                               values.
     \param[in]         trasl  Value of the value to sum after to
                               multiply.
     \param[in]        VarDim  Number of points corresponding the
                               total variable dimension.
     \param[in]           dim  Integer with the space dimension
                               (2, 3 or 4).

     \return An integer error control value with 0, if success.

*/
/*******************************************************************/
int longdouble_UnitConvertion( DCT_Consume_Plan *tdata, long double *VarValues,
                               long double factor, long double trasl, DCT_Integer *VarDim,
                               DCT_Integer dim )
{
/* ---------------------------------------------  Local Variables  */
   int  ierr = 0;
   
   DCT_Integer      Npts1;
   DCT_Integer      Npts2;
   DCT_Integer      Npts3;
//   DCT_Integer      Npts4;
   long double     *filtdata;
   long double     *strdptr;
   
   int              stride, stride2;
   
   register int     ii, jj, kk;
   
/* ---------------------------  BEGIN( longdouble_UnitConvertion ) */

   filtdata  = (long double *) tdata->VarTempVal;
   
   /**** If no convertion is required ****/
   if ( ( factor == 1.0 ) && ( trasl == 0.0 ) ) {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = *(filtdata++);
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = *(filtdata++);
            }
         }
   
      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
               *(strdptr++) = *(filtdata++);
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
                  *(strdptr++) = *(filtdata++);
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                     *(strdptr++) = *(filtdata++);
               }
            }
         }
   
      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension", return(1) );
      }            
   }
   else if ( ( factor == 1.0 ) && ( trasl != 0.0 ) ) {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = *(filtdata++) + trasl;
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = *(filtdata++) + trasl;
            }
         }

      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
               *(strdptr++) = *(filtdata++) + trasl;
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
                  *(strdptr++) = *(filtdata++) + trasl;
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                     *(strdptr++) = *(filtdata++) + trasl;
               }
            }
         }

      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension",
                   return(1) );
      }            
   }
   else if ( ( factor != 1.0 ) && ( trasl == 0.0 ) ) {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = factor * *(filtdata++);
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = factor * *(filtdata++);
            }
         }

      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
               *(strdptr++) = factor * *(filtdata++);
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
                  *(strdptr++) = factor * *(filtdata++);
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                     *(strdptr++) = factor * *(filtdata++);
               }
            }
         }

      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension",
                   return(1) );
      }            
   }
   else {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = factor * *(filtdata++) + trasl;
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = factor * *(filtdata++) + trasl;
            }
         }

      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
               *(strdptr++) = factor * *(filtdata++) + trasl;
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
                  *(strdptr++) = factor * *(filtdata++) + trasl;
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                     *(strdptr++) = factor * *(filtdata++) + trasl;
               }
            }
         }

      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension",
                   return(1) );
      }            
   } /* End of if ( ( factor == 1.0 ) && ( trasl == 0.0 ) ) */

   return (ierr);
/* -----------------------------  END( longdouble_UnitConvertion ) */
}

/*******************************************************************/
/*                       double_UnitConvertion                     */
/*                                                                 */
/*!     Calculate the convertion as linear operation defined
        VarValue = factor*Values + trasl using double precision.

     \param[in]         tdata  Pointer to a DCT_Consume_Plan
                               structure.
     \param[in,out] VarValues  Pointer to user's data array.
     \param[in]        factor  Value of the fator to multiply the
                               values.
     \param[in]         trasl  Value of the value to sum after to
                               multiply.
     \param[in]        VarDim  Number of points corresponding the
                               total variable dimension.
     \param[in]           dim  Integer with the space dimension
                               (2, 3 or 4).

     \return An integer error control value with 0, if success.

*/
/*******************************************************************/
int double_UnitConvertion( DCT_Consume_Plan *tdata, double *VarValues, double factor,
                           double trasl, DCT_Integer *VarDim, DCT_Integer dim )
{
/* ---------------------------------------------  Local Variables  */
   int  ierr = 0;
   
   DCT_Integer      Npts1;
   DCT_Integer      Npts2;
   DCT_Integer      Npts3;
//   DCT_Integer      Npts4;
   double          *filtdata;
   double          *strdptr;
   
   int              stride, stride2;
   
   register int     ii, jj, kk;
   
/* -------------------------------  BEGIN( double_UnitConvertion ) */

   filtdata  = (double *) tdata->VarTempVal;
   
   /**** If no convertion is required ****/
   if ( ( factor == 1.0 ) && ( trasl == 0.0 ) ) {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = *(filtdata++);
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = *(filtdata++);
            }
         }
   
      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
               *(strdptr++) = *(filtdata++);
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
                  *(strdptr++) = *(filtdata++);
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                     *(strdptr++) = *(filtdata++);
               }
            }
         }
   
      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension", return(1) );
      }            
   }
   else if ( ( factor == 1.0 ) && ( trasl != 0.0 ) ) {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = *(filtdata++) + trasl;
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = *(filtdata++) + trasl;
            }
         }

      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
               *(strdptr++) = *(filtdata++) + trasl;
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
                  *(strdptr++) = *(filtdata++) + trasl;
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                     *(strdptr++) = *(filtdata++) + trasl;
               }
            }
         }

      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension",
                   return(1) );
      }            
   }
   else if ( ( factor != 1.0 ) && ( trasl == 0.0 ) ) {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = factor * *(filtdata++);
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = factor * *(filtdata++);
            }
         }

      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
               *(strdptr++) = factor * *(filtdata++);
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
                  *(strdptr++) = factor * *(filtdata++);
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                     *(strdptr++) = factor * *(filtdata++);
               }
            }
         }

      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension",
                   return(1) );
      }            
   }
   else {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = factor * *(filtdata++) + trasl;
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = factor * *(filtdata++) + trasl;
            }
         }

      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
               *(strdptr++) = factor * *(filtdata++) + trasl;
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
                  *(strdptr++) = factor * *(filtdata++) + trasl;
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                     *(strdptr++) = factor * *(filtdata++) + trasl;
               }
            }
         }

      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension",
                   return(1) );
      }            
   } /* End of if ( ( factor == 1.0 ) && ( trasl == 0.0 ) ) */

   return (ierr);
/* ---------------------------------  END( double_UnitConvertion ) */
}

/*******************************************************************/
/*                        float_UnitConvertion                     */
/*                                                                 */
/*!     Calculate the convertion as linear operation defined
        VarValue = factor*Values + trasl using float precision.

     \param[in]         tdata  Pointer to a DCT_Consume_Plan
                               structure.
     \param[in,out] VarValues  Pointer to user's data array.
     \param[in]        factor  Value of the fator to multiply the
                               values.
     \param[in]         trasl  Value of the value to sum after to
                               multiply.
     \param[in]        VarDim  Number of points corresponding the
                               total variable dimension.
     \param[in]           dim  Integer with the space dimension
                               (2, 3 or 4).

     \return An integer error control value with 0, if success.

*/
/*******************************************************************/
int float_UnitConvertion( DCT_Consume_Plan *tdata, float *VarValues, float factor,
                          float trasl, DCT_Integer *VarDim, DCT_Integer dim )
{
/* ---------------------------------------------  Local Variables  */
   int  ierr = 0;
   
   DCT_Integer      Npts1;
   DCT_Integer      Npts2;
   DCT_Integer      Npts3;
   //DCT_Integer      Npts4;
   float           *filtdata;
   float           *strdptr;
   
   int              stride, stride2;
   
   register int     ii, jj, kk;
   
/* --------------------------------  BEGIN( float_UnitConvertion ) */

   filtdata  = (float *) tdata->VarTempVal;
   
   /**** If no convertion is required ****/
   if ( ( factor == 1.0 ) && ( trasl == 0.0 ) ) {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = *(filtdata++);
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
               strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = *(filtdata++);
            }
         }
   
      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
            *(strdptr++) = *(filtdata++);
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
                  *(strdptr++) = *(filtdata++);
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
               strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                     *(strdptr++) = *(filtdata++);
               }
            }
         }
   
      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension", return(1) );
      }            
   }
   else if ( ( factor == 1.0 ) && ( trasl != 0.0 ) ) {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = *(filtdata++) + trasl;
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = *(filtdata++) + trasl;
            }
         }

      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
               *(strdptr++) = *(filtdata++) + trasl;
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
                  *(strdptr++) = *(filtdata++) + trasl;
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                     *(strdptr++) = *(filtdata++) + trasl;
               }
            }
         }

      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension",
                   return(1) );
      }            
   }
   else if ( ( factor != 1.0 ) && ( trasl == 0.0 ) ) {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = factor * *(filtdata++);
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = factor * *(filtdata++);
            }
         }

      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
               *(strdptr++) = factor * *(filtdata++);
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
                  *(strdptr++) = factor * *(filtdata++);
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                     *(strdptr++) = factor * *(filtdata++);
               }
            }
         }

      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension",
                   return(1) );
      }            
   }
   else {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = factor * *(filtdata++) + trasl;
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               *(strdptr++) = factor * *(filtdata++) + trasl;
            }
         }

      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
               *(strdptr++) = factor * *(filtdata++) + trasl;
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
                  *(strdptr++) = factor * *(filtdata++) + trasl;
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                     *(strdptr++) = factor * *(filtdata++) + trasl;
               }
            }
         }

      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension",
                   return(1) );
      }            
   } /* End of if ( ( factor == 1.0 ) && ( trasl == 0.0 ) ) */

   return (ierr);
/* ----------------------------------  END( float_UnitConvertion ) */
}

/*******************************************************************/
/*                   longdouble_Msk_UnitConvertion                 */
/*                                                                 */
/*!     Calculate the convertion as linear operation defined
        VarValue = 
             mask*(factor*Values + trasl) + (1-mask)*VarValue
        using long double precision. Where mask is defined
        inside tdata structure to determine if the conversion
        applies (mask = 1) or not (mask = 0).

     \param[in]         tdata  Pointer to a DCT_Consume_Plan
                               structure.
     \param[in,out] VarValues  Pointer to user's data array.
     \param[in]        factor  Value of the fator to multiply the
                               values.
     \param[in]         trasl  Value of the value to sum after to
                               multiply.
     \param[in]        VarDim  Number of points corresponding the
                               total variable dimension.
     \param[in]           dim  Integer with the space dimension
                               (2, 3 or 4).

     \return An integer error control value with 0, if success.

*/
/*******************************************************************/
int longdouble_Msk_UnitConvertion( DCT_Consume_Plan *tdata, long double *VarValues,
                               long double factor, long double trasl, DCT_Integer *VarDim,
                               DCT_Integer dim )
{
/* ---------------------------------------------  Local Variables  */
   int  ierr = 0;
   
   DCT_Integer      Npts1;
   DCT_Integer      Npts2;
   DCT_Integer      Npts3;
//   DCT_Integer      Npts4;
   long double     *filtdata;
   long double     *strdptr, ldmask;
   
   DCT_Integer     *mask;
   
   int              stride, stride2;
   
   register int     ii, jj, kk;
   
/* -----------------------  BEGIN( longdouble_Msk_UnitConvertion ) */

   filtdata  = (long double *) tdata->VarTempVal;
   mask      = tdata->VarMask;
   
   /**** If no convertion is required ****/
   if ( ( factor == 1.0 ) && ( trasl == 0.0 ) ) {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
            ldmask = (long double)*(mask++);
            *(strdptr) = ldmask * *(filtdata++) + (1.0 - ldmask) * *(strdptr);
            strdptr++;
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               ldmask = (long double)*(mask++);
               *(strdptr) = ldmask * *(filtdata++) + (1.0 - ldmask) * *(strdptr);
               strdptr++;
            }
         }
   
      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
            ldmask = (long double)*(mask++);
            *(strdptr) = ldmask * *(filtdata++) + (1.0 - ldmask) * *(strdptr);
            strdptr++;
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
               ldmask = (long double)*(mask++);
               *(strdptr) = ldmask * *(filtdata++) + (1.0 - ldmask) * *(strdptr);
               strdptr++;
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                  ldmask = (long double)*(mask++);
                  *(strdptr) = ldmask * *(filtdata++) + (1.0 - ldmask) * *(strdptr);
                  strdptr++;
               }
            }
         }
   
      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension", return(1) );
      }            
   }
   else if ( ( factor == 1.0 ) && ( trasl != 0.0 ) ) {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
            ldmask = (long double)*(mask++);
            *(strdptr) = ldmask * ( *(filtdata++) + trasl ) + (1.0 - ldmask) * *(strdptr);
            strdptr++;
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               ldmask = (long double)*(mask++);
               *(strdptr) = ldmask * ( *(filtdata++) + trasl ) + (1.0 - ldmask) * *(strdptr);
               strdptr++;
            }
         }

      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
            ldmask = (long double)*(mask++);
            *(strdptr) = ldmask * ( *(filtdata++) + trasl ) + (1.0 - ldmask) * *(strdptr);
            strdptr++;
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
               ldmask = (long double)*(mask++);
               *(strdptr) = ldmask * ( *(filtdata++) + trasl ) + (1.0 - ldmask) * *(strdptr);
               strdptr++;
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                  ldmask = (long double)*(mask++);
                  *(strdptr) = ldmask * ( *(filtdata++) + trasl ) + (1.0 - ldmask) * *(strdptr);
                  strdptr++;
               }
            }
         }

      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension",
                   return(1) );
      }            
   }
   else if ( ( factor != 1.0 ) && ( trasl == 0.0 ) ) {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
            ldmask = (long double)*(mask++);
            *(strdptr) = ldmask * ( factor * *(filtdata++) ) + (1.0 - ldmask) * *(strdptr);
            strdptr++;
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               ldmask = (long double)*(mask++);
               *(strdptr) = ldmask * ( factor * *(filtdata++) ) + (1.0 - ldmask) * *(strdptr);
               strdptr++;
            }
         }

      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
            ldmask = (long double)*(mask++);
            *(strdptr) = ldmask * ( factor * *(filtdata++) ) + (1.0 - ldmask) * *(strdptr);
            strdptr++;
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
               ldmask = (long double)*(mask++);
               *(strdptr) = ldmask * ( factor * *(filtdata++) ) + (1.0 - ldmask) * *(strdptr);
               strdptr++;
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                  ldmask = (long double)*(mask++);
                  *(strdptr) = ldmask * ( factor * *(filtdata++) ) + (1.0 - ldmask) * *(strdptr);
                  strdptr++;
               }
            }
         }

      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension",
                   return(1) );
      }            
   }
   else {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
            ldmask = (long double)*(mask++);
            *(strdptr) = ldmask * ( factor * *(filtdata++) + trasl )
                         + (1.0 - ldmask) * *(strdptr);
            strdptr++;
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               ldmask = (long double)*(mask++);
               *(strdptr) = ldmask * ( factor * *(filtdata++) + trasl )
                            + (1.0 - ldmask) * *(strdptr);
               strdptr++;
            }
         }

      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
            ldmask = (long double)*(mask++);
            *(strdptr) = ldmask * ( factor * *(filtdata++) + trasl )
                         + (1.0 - ldmask) * *(strdptr);
            strdptr++;
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
               ldmask = (long double)*(mask++);
               *(strdptr) = ldmask * ( factor * *(filtdata++) + trasl )
                            + (1.0 - ldmask) * *(strdptr);
               strdptr++;
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                  ldmask = (long double)*(mask++);
                  *(strdptr) = ldmask * ( factor * *(filtdata++) + trasl )
                               + (1.0 - ldmask) * *(strdptr);
                  strdptr++;
               }
            }
         }

      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension",
                   return(1) );
      }            
   } /* End of if ( ( factor == 1.0 ) && ( trasl == 0.0 ) ) */

   return (ierr);
/* -------------------------  END( longdouble_Msk_UnitConvertion ) */
}

/*******************************************************************/
/*                     double_Msk_UnitConvertion                   */
/*                                                                 */
/*!     Calculate the convertion as linear operation defined
        VarValue = 
             mask*(factor*Values + trasl) + (1-mask)*VarValue
        using double precision. Where mask is defined inside
        tdata structure to determine if the conversion applies
        (mask = 1) or not (mask = 0).

     \param[in]         tdata  Pointer to a DCT_Consume_Plan
                               structure.
     \param[in,out] VarValues  Pointer to user's data array.
     \param[in]        factor  Value of the fator to multiply the
                               values.
     \param[in]         trasl  Value of the value to sum after to
                               multiply.
     \param[in]        VarDim  Number of points corresponding the
                               total variable dimension.
     \param[in]           dim  Integer with the space dimension
                               (2, 3 or 4).

     \return An integer error control value with 0, if success.

*/
/*******************************************************************/
int double_Msk_UnitConvertion( DCT_Consume_Plan *tdata, double *VarValues, double factor,
                               double trasl, DCT_Integer *VarDim, DCT_Integer dim )
{
/* ---------------------------------------------  Local Variables  */
   int  ierr = 0;
   
   DCT_Integer      Npts1;
   DCT_Integer      Npts2;
   DCT_Integer      Npts3;
//   DCT_Integer      Npts4;
   double          *filtdata;
   double          *strdptr, dmask;
   
   DCT_Integer     *mask;
   
   int              stride, stride2;
   
   register int     ii, jj, kk;
   
/* ---------------------------  BEGIN( double_Msk_UnitConvertion ) */

   filtdata  = (double *) tdata->VarTempVal;
   mask      = tdata->VarMask;
   
   /**** If no convertion is required ****/
   if ( ( factor == 1.0 ) && ( trasl == 0.0 ) ) {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
            dmask = (double)*(mask++);
            *(strdptr) = dmask * *(filtdata++) + (1.0 - dmask) * *(strdptr);
            strdptr++;
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               dmask = (double)*(mask++);
               *(strdptr) = dmask * *(filtdata++) + (1.0 - dmask) * *(strdptr);
               strdptr++;
            }
         }
   
      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
            dmask = (double)*(mask++);
            *(strdptr) = dmask * *(filtdata++) + (1.0 - dmask) * *(strdptr);
            strdptr++;
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
               dmask = (double)*(mask++);
               *(strdptr) = dmask * *(filtdata++) + (1.0 - dmask) * *(strdptr);
               strdptr++;
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                  dmask = (double)*(mask++);
                  *(strdptr) = dmask * *(filtdata++) + (1.0 - dmask) * *(strdptr);
                  strdptr++;
               }
            }
         }
   
      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension", return(1) );
      }            
   }
   else if ( ( factor == 1.0 ) && ( trasl != 0.0 ) ) {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
            dmask = (double)*(mask++);
            *(strdptr) = dmask * ( *(filtdata++) + trasl ) + (1.0 - dmask) * *(strdptr);
            strdptr++;
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               dmask = (double)*(mask++);
               *(strdptr) = dmask * ( *(filtdata++) + trasl ) + (1.0 - dmask) * *(strdptr);
               strdptr++;
            }
         }

      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
            dmask = (double)*(mask++);
            *(strdptr) = dmask * ( *(filtdata++) + trasl ) + (1.0 - dmask) * *(strdptr);
            strdptr++;
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
               dmask = (double)*(mask++);
               *(strdptr) = dmask * ( *(filtdata++) + trasl ) + (1.0 - dmask) * *(strdptr);
               strdptr++;
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                  dmask = (double)*(mask++);
                  *(strdptr) = dmask * ( *(filtdata++) + trasl ) + (1.0 - dmask) * *(strdptr);
                  strdptr++;
               }
            }
         }

      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension",
                   return(1) );
      }            
   }
   else if ( ( factor != 1.0 ) && ( trasl == 0.0 ) ) {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
            dmask = (double)*(mask++);
            *(strdptr) = dmask * ( factor * *(filtdata++) ) + (1.0 - dmask) * *(strdptr);
            strdptr++;
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               dmask = (double)*(mask++);
               *(strdptr) = dmask * ( factor * *(filtdata++) ) + (1.0 - dmask) * *(strdptr);
               strdptr++;
            }
         }

      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
            dmask = (double)*(mask++);
            *(strdptr) = dmask * ( factor * *(filtdata++) ) + (1.0 - dmask) * *(strdptr);
            strdptr++;
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
               dmask = (double)*(mask++);
               *(strdptr) = dmask * ( factor * *(filtdata++) ) + (1.0 - dmask) * *(strdptr);
               strdptr++;
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                  dmask = (double)*(mask++);
                  *(strdptr) = dmask * ( factor * *(filtdata++) ) + (1.0 - dmask) * *(strdptr);
                  strdptr++;
               }
            }
         }

      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension",
                   return(1) );
      }            
   }
   else {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
            dmask = (double)*(mask++);
            *(strdptr) = dmask * ( factor * *(filtdata++) + trasl )
                         + (1.0 - dmask) * *(strdptr);
            strdptr++;
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               dmask = (double)*(mask++);
               *(strdptr) = dmask * ( factor * *(filtdata++) + trasl )
                            + (1.0 - dmask) * *(strdptr);
               strdptr++;
            }
         }

      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
            dmask = (double)*(mask++);
            *(strdptr) = dmask * ( factor * *(filtdata++) + trasl )
                         + (1.0 - dmask) * *(strdptr);
            strdptr++;
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
               dmask = (double)*(mask++);
               *(strdptr) = dmask * ( factor * *(filtdata++) + trasl )
                            + (1.0 - dmask) * *(strdptr);
               strdptr++;
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                  dmask = (double)*(mask++);
                  *(strdptr) = dmask * ( factor * *(filtdata++) + trasl )
                               + (1.0 - dmask) * *(strdptr);
                  strdptr++;
               }
            }
         }

      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension",
                   return(1) );
      }            
   } /* End of if ( ( factor == 1.0 ) && ( trasl == 0.0 ) ) */

   return (ierr);
/* -----------------------------  END( double_Msk_UnitConvertion ) */
}

/*******************************************************************/
/*                       float_Msk_UnitConvertion                  */
/*                                                                 */
/*!     Calculate the convertion as linear operation defined
        VarValue = 
             mask*(factor*Values + trasl) + (1-mask)*VarValue
        using float precision. Where mask is defined inside
        tdata structure to determine if the conversion applies
        (mask = 1) or not (mask = 0).

     \param[in]         tdata  Pointer to a DCT_Consume_Plan
                               structure.
     \param[in,out] VarValues  Pointer to user's data array.
     \param[in]        factor  Value of the fator to multiply the
                               values.
     \param[in]         trasl  Value of the value to sum after to
                               multiply.
     \param[in]        VarDim  Number of points corresponding the
                               total variable dimension.
     \param[in]           dim  Integer with the space dimension
                               (2, 3 or 4).

     \return An integer error control value with 0, if success.

*/
/*******************************************************************/
int float_Msk_UnitConvertion( DCT_Consume_Plan *tdata, float *VarValues, float factor,
                              float trasl, DCT_Integer *VarDim, DCT_Integer dim )
{
/* ---------------------------------------------  Local Variables  */
   int  ierr = 0;
   
   DCT_Integer      Npts1;
   DCT_Integer      Npts2;
   DCT_Integer      Npts3;
   //DCT_Integer      Npts4;
   float           *filtdata;
   float           *strdptr, fmask;
   
   DCT_Integer     *mask;
   
   int              stride, stride2;
   
   register int     ii, jj, kk;
   
/* ----------------------------  BEGIN( float_Msk_UnitConvertion ) */

   filtdata  = (float *) tdata->VarTempVal;
   mask      = tdata->VarMask;
   
   /**** If no convertion is required ****/
   if ( ( factor == 1.0 ) && ( trasl == 0.0 ) ) {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
            fmask = (float)*(mask++);
            *(strdptr) = fmask * *(filtdata++) + (1.0 - fmask) * *(strdptr);
            strdptr++;
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               fmask = (float)*(mask++);
               *(strdptr) = fmask * *(filtdata++) + (1.0 - fmask) * *(strdptr);
               strdptr++;
            }
         }
   
      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
            fmask = (float)*(mask++);
            *(strdptr) = fmask * *(filtdata++) + (1.0 - fmask) * *(strdptr);
            strdptr++;
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
               fmask = (float)*(mask++);
               *(strdptr) = fmask * *(filtdata++) + (1.0 - fmask) * *(strdptr);
               strdptr++;
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                  fmask = (float)*(mask++);
                  *(strdptr) = fmask * *(filtdata++) + (1.0 - fmask) * *(strdptr);
                  strdptr++;
               }
            }
         }
   
      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension", return(1) );
      }            
   }
   else if ( ( factor == 1.0 ) && ( trasl != 0.0 ) ) {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
            fmask = (float)*(mask++);
            *(strdptr) = fmask * ( *(filtdata++) + trasl ) + (1.0 - fmask) * *(strdptr);
            strdptr++;
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               fmask = (float)*(mask++);
               *(strdptr) = fmask * ( *(filtdata++) + trasl ) + (1.0 - fmask) * *(strdptr);
               strdptr++;
               
            }
         }

      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
               fmask = (float)*(mask++);
               *(strdptr) = fmask * ( *(filtdata++) + trasl ) + (1.0 - fmask) * *(strdptr);
               strdptr++;
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
               fmask = (float)*(mask++);
               *(strdptr) = fmask * ( *(filtdata++) + trasl ) + (1.0 - fmask) * *(strdptr);
               strdptr++;
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                  fmask = (float)*(mask++);
                  *(strdptr) = fmask * ( *(filtdata++) + trasl ) + (1.0 - fmask) * *(strdptr);
                  strdptr++;
               }
            }
         }

      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension",
                   return(1) );
      }            
   }
   else if ( ( factor != 1.0 ) && ( trasl == 0.0 ) ) {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
            fmask = (float)*(mask++);
            *(strdptr) = fmask * ( factor* *(filtdata++) ) + (1.0 - fmask) * *(strdptr);
            strdptr++;
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               fmask = (float)*(mask++);
               *(strdptr) = fmask * ( factor* *(filtdata++) ) + (1.0 - fmask) * *(strdptr);
               strdptr++;
            }
         }

      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
            fmask = (float)*(mask++);
            *(strdptr) = fmask * ( factor* *(filtdata++) ) + (1.0 - fmask) * *(strdptr);
            strdptr++;
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
               fmask = (float)*(mask++);
               *(strdptr) = fmask * ( factor* *(filtdata++) ) + (1.0 - fmask) * *(strdptr);
               strdptr++;
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                  fmask = (float)*(mask++);
                  *(strdptr) = fmask * ( factor* *(filtdata++) ) + (1.0 - fmask) * *(strdptr);
                  strdptr++;
               }
            }
         }

      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension",
                   return(1) );
      }            
   }
   else {
      if ( dim == 2) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         stride = VarDim[1] - Npts2;
         strdptr = VarValues + (tdata->VarIniPos[0])*VarDim[1] + tdata->VarIniPos[1];
         for ( jj = 0; jj < Npts2; jj++ ) {
            fmask = (float)*(mask++);
            *(strdptr) = fmask * ( factor* *(filtdata++) + trasl )
                         + (1.0 - fmask) * *(strdptr);
            strdptr++;
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride;
            for ( jj = 0; jj < Npts2; jj++ ) {
               fmask = (float)*(mask++);
               *(strdptr) = fmask * ( factor* *(filtdata++) + trasl )
                            + (1.0 - fmask) * *(strdptr);
               strdptr++;
            }
         }

      } else if (dim ==3) {
         Npts1 = tdata->VarNpts[0];
         Npts2 = tdata->VarNpts[1];
         Npts3 = tdata->VarNpts[2];
         stride = VarDim[2] - Npts3;
         stride2 = (VarDim[1] - Npts2)*VarDim[2];
         strdptr = VarValues + 
                   (tdata->VarIniPos[0]*VarDim[1] + tdata->VarIniPos[1])*VarDim[2] +
                   tdata->VarIniPos[2];
         for ( kk = 0; kk < Npts3; kk++ ) {
            fmask = (float)*(mask++);
            *(strdptr) = fmask * ( factor* *(filtdata++) + trasl )
                         + (1.0 - fmask) * *(strdptr);
            strdptr++;
         }
         
         for ( jj = 1; jj < Npts2; jj++ ) {
            strdptr += stride;
            for ( kk = 0; kk < Npts3; kk++ ) {
               fmask = (float)*(mask++);
               *(strdptr) = fmask * ( factor* *(filtdata++) + trasl )
                            + (1.0 - fmask) * *(strdptr);
               strdptr++;
            }
         }
         for ( ii = 1; ii < Npts1; ii++ ) {
         strdptr += stride2;
            for ( jj = 0; jj < Npts2; jj++ ) {
               strdptr += stride;
               for ( kk = 0; kk < Npts3; kk++ ) {
                  fmask = (float)*(mask++);
                  *(strdptr) = fmask * ( factor* *(filtdata++) + trasl )
                               + (1.0 - fmask) * *(strdptr);
                  strdptr++;
               }
            }
         }

      } else if (dim ==4) {
         DCTERROR( (dim ==4), 
                   "Internal Error, No unit convertion defined for given dimension",
                   return(1) );
      }            
   } /* End of if ( ( factor == 1.0 ) && ( trasl == 0.0 ) ) */

   return (ierr);
/* ------------------------------  END( float_Msk_UnitConvertion ) */
}

/*******************************************************************/
/*                       DCT_Convertion_Parameter                  */
/*                                                                 */
/*!     Assign the conversion parameters to be used later, during
        the conversion opeation.

     \param[in] ProdUnits  DCT_Units type of the producer side.
     \param[in]  ConsUnit  DCT_Units type of the consumer side.
     \param[out]   factor  Factor used to multiply in the
                           conversion.
     \param[out]   transl  Number used to sum (known as translation
                           factor) after the multiplication.

     \return An integer error control value with 0, if success.
     
     \todo Check the effectiveness when using long double precision.

*/
/*******************************************************************/
int DCT_Convertion_Parameter( DCT_Units ProdUnits, DCT_Units ConsUnit,
                                double *factor, double *transl )
{
/* ---------------------------------------------  Local Variables  */

/* ----------------------------  BEGIN( DCT_Convertion_Parameter ) */
   switch ( ProdUnits ) {
      case CENTIMETER:
        switch ( ConsUnit ) {
            case METER:
               *factor = 0.01;
               *transl = 0.0;
               break;
            case KILOMETER:
               *factor = 1.0E-5;
               *transl = 0.0;
               break;
            case INCH:
               *factor = 0.3937007874;
               *transl = 0.0;
               break;
            case FEET:
               *factor = 0.03280839895;
               *transl = 0.0;
               break;
            case MILES:
               *factor = 6.2137119224E-6;
               *transl = 0.0;
               break;
            default:
               DCTERROR( ConsUnit, "Incorrect pair of unit to be converted", return(1) );
        } /* End of switch ( ConsUnit ) */
        break;
      case METER:
        switch ( ConsUnit ) {
            case CENTIMETER:
               *factor = 100.0;
               *transl = 0.0;
               break;
            case KILOMETER:
               *factor = 0.001;
               *transl = 0.0;
               break;
            case INCH:
               *factor = 39.37007874;
               *transl = 0.0;
               break;
            case FEET:
               *factor = 3.280839895;
               *transl = 0.0;
               break;
            case MILES:
               *factor = 6.2137119224E-4;
               *transl = 0.0;
               break;
            default:
               DCTERROR( ConsUnit, "Incorrect pair of unit to be converted", return(1) );
        } /* End of switch ( ConsUnit ) */
        break;
      case KILOMETER:
        switch ( ConsUnit ) {
            case CENTIMETER:
               *factor = 100000.0;
               *transl = 0.0;
               break;
            case METER:
               *factor = 1000.0;
               *transl = 0.0;
               break;
            case INCH:
               *factor = 39370.07874;
               *transl = 0.0;
               break;
            case FEET:
               *factor = 3280.839895;
               *transl = 0.0;
               break;
            case MILES:
               *factor = 0.62137119224;
               *transl = 0.0;
               break;
            default:
               DCTERROR( ConsUnit, "Incorrect pair of unit to be converted", return(1) );
        } /* End of switch ( ConsUnit ) */
        break;
      case INCH:
        switch ( ConsUnit ) {
            case CENTIMETER:
               *factor = 2.54;
               *transl = 0.0;
               break;
            case METER:
               *factor = 0.0254;
               *transl = 0.0;
               break;
            case KILOMETER:
               *factor = 2.54E-5;
               *transl = 0.0;
               break;
            case FEET:
               *factor = 8.3333333333E-2;
               *transl = 0.0;
               break;
            case MILES:
               *factor = 1.5782828283E-5;
               *transl = 0.0;
               break;
            default:
               DCTERROR( ConsUnit, "Incorrect pair of unit to be converted", return(1) );
        } /* End of switch ( ConsUnit ) */
        break;
      case FEET:
        switch ( ConsUnit ) {
            case CENTIMETER:
               *factor = 30.48;
               *transl = 0.0;
               break;
            case METER:
               *factor = 0.3048;
               *transl = 0.0;
               break;
            case KILOMETER:
               *factor = 3.048E-4;
               *transl = 0.0;
               break;
            case INCH:
               *factor = 12.0;
               *transl = 0.0;
               break;
            case MILES:
               *factor = 1.8939393939E-4;
               *transl = 0.0;
               break;
            default:
               DCTERROR( ConsUnit, "Incorrect pair of unit to be converted", return(1) );
        } /* End of switch ( ConsUnit ) */
        break;
      case MILES:
        switch ( ConsUnit ) {
            case CENTIMETER:
               *factor = 160934.4;
               *transl = 0.0;
               break;
            case METER:
               *factor = 1609.344;
               *transl = 0.0;
               break;
            case KILOMETER:
               *factor = 1.609344;
               *transl = 0.0;
               break;
            case INCH:
               *factor = 63360.0;
               *transl = 0.0;
               break;
            case FEET:
               *factor = 5280.0;
               *transl = 0.0;
               break;
            default:
               DCTERROR( ConsUnit, "Incorrect pair of unit to be converted", return(1) );
        } /* End of switch ( ConsUnit ) */
        break;
   } /* End of switch ( ProdUnits ) */

   return( 0 );
/* ------------------------------  END( DCT_Convertion_Parameter ) */
}

/*******************************************************************/
/*                     DCT_Perfom_UnitConvertion                   */
/*                                                                 */
/*!          Determine the unit conversion to be performed

     \param[in]         tdata  Pointer to a DCT_Consume_Plan
                               structure.
     \param[in,out] VarValues  Pointer to user's data array.
     \param[in]         dtype  data type (DCT_FLOAT, DCT_DOUBLE,
                               etc.) corresponding to user's data.
     \param[in]      ConsUnit  Consumer's Unit type the received
                               values are converted to.
     \param[in]        VarDim  Number of points corresponding the
                               total variable dimension.
     \param[in]           dim  Integer with the space dimension
                               (2, 3 or 4).

     \return An integer error control value with 0, if success.

     \todo The calculation of factor and transl should be
           done once at register time and all the requirements
           to just only go and sweep the elements.

*/
/*******************************************************************/
int DCT_Perfom_UnitConvertion( DCT_Consume_Plan *tdata, DCT_VoidPointer VarValues,
                               DCT_Data_Types dtype, DCT_Units ConsUnit, DCT_Integer *VarDim,
                               DCT_Integer dim )
{
/* ---------------------------------------------  Local Variables  */
   int              ierr = 0;
   
   DCT_Units        ProdUnits;
   
   double           factor, transl;
   
/* ---------------------------  BEGIN( DCT_Perfom_UnitConvertion ) */

   ProdUnits = tdata->VarChUnits;

   /*********************  ATTENTION AS THE TODO SAYS THIS PART
               SHOUDL BE PERFORMED DURING REGISTRATION TIME ONLY ONCE
                                            ************************/
   /**** If no convertion is required ****/
   if ( ( ProdUnits == DCT_NO_UNITS ) || (ConsUnit == DCT_NO_UNITS) ||
        (ProdUnits == ConsUnit) ) {
      factor = 1.0;
      transl = 0.0;
   } else {
      ierr = DCT_Convertion_Parameter( ProdUnits, ConsUnit, &factor, &transl );
   } /* End of if ( ( ProdUnits == DCT_NO_UNITS ) || (ConsUnit == DCT_NO_UNITS) ||... */
   DCTERROR( ierr, "Error in pairing units", return(1) );
   
   switch ( dtype ) {
      case DCT_FLOAT:
         if ( tdata->VarMask == NULL )
            ierr = float_UnitConvertion( tdata, (float *) VarValues, (float) factor,
                                         (float) transl, VarDim, dim );
         else
            ierr = float_Msk_UnitConvertion( tdata, (float *) VarValues, (float) factor,
                                             (float) transl, VarDim, dim );
         break;
      case DCT_DOUBLE:
         if ( tdata->VarMask == NULL )
            ierr = double_UnitConvertion( tdata, (double *) VarValues, (double) factor,
                                          (double) transl, VarDim, dim );
         else
            ierr = double_Msk_UnitConvertion( tdata, (double *) VarValues, (double) factor,
                                              (double) transl, VarDim, dim );
         break;
      case DCT_LONG_DOUBLE:
         if ( tdata->VarMask == NULL )
            ierr = longdouble_UnitConvertion( tdata, (long double *) VarValues,
                                  (long double) factor, (long double) transl, VarDim, dim );
         else
            ierr = longdouble_Msk_UnitConvertion( tdata, (long double *) VarValues,
                                  (long double) factor, (long double) transl, VarDim, dim );
         break;
      default:
         DCTERROR( dtype, "Internal Error, No DCT_Data_Types defined", return(1) );
   } /* End of switch ( dtype ) */

   return (ierr);
/* -----------------------------  END( DCT_Perfom_UnitConvertion ) */
}

/*******************************************************************/
/*                          DCT_Perfom_Filter                      */
/*                                                                 */
/*!    Determine the filter or interpolation function to be
       performed

       \param[in,out] tdata  Pointer to a DCT_Consume_Plan
                             structure.
       \param[in]     dtype  data type (DCT_FLOAT, DCT_DOUBLE,
                             etc.) corresponding to consumer's
                             variable structure.
       \param[in]       dim  Integer with the space dimension
                             (2, 3 or 4).

       \return An integer error control value with 0, if success.

*/
/*******************************************************************/
int DCT_Perfom_Filter( DCT_Consume_Plan *tdata, DCT_Data_Types dtype,
                       DCT_Integer dim )
{
/* ---------------------------------------------  Local Variables  */
   int  ierr = 0;

   DCT_Data_Transformation Transf;
   
/* -----------------------------------  BEGIN( DCT_Perfom_Filter ) */

   Transf = tdata->VarTransf;

   switch (Transf) {
      case DCT_NO_INTERPOLATION:
         break;
      case DCT_LINEAR_INTERPOLATION:
         if ( dim == 2) {
            ierr = DCT_Bilinear_Interp( tdata->VarRcvdDType, tdata->VarRcvdNpts,
                                        tdata->VarRcvdVal, tdata->VarLabels, tdata->VarInt,
                                        dtype, tdata->VarMask, tdata->VarNpts, tdata->VarTempVal );

         } else if (dim ==3) {
            ierr = DCT_Trilinear_Interp( tdata->VarRcvdDType, tdata->VarRcvdNpts,
                                         tdata->VarRcvdVal, tdata->VarLabels, tdata->VarInt,
                                         dtype, tdata->VarMask, tdata->VarNpts, tdata->VarTempVal );
         } else if (dim ==4) {
            DCTERROR( (Transf) && (dim ==4), 
                      "Internal Error, No weight defined for given dimension", return(1) );
         }
         break;
      case DCT_QUAD_INTERPOLATION:
         if ( dim == 2) {
            DCTERROR( (Transf) && (dim ==2), 
                      "Internal Error, No weight defined for given dimension", return(1) );
         } else if (dim ==3) {
            DCTERROR( (Transf) && (dim ==3), 
                      "Internal Error, No weight defined for given dimension", return(1) );
         } else if (dim ==4) {
            DCTERROR( (Transf) && (dim ==4), 
                      "Internal Error, No weight defined for given dimension", return(1) );
         }
         break;
      case DCT_CUBIC_INTERPOLATION:
         if ( dim == 2) {
            DCTERROR( (Transf) && (dim ==2), 
                      "Internal Error, No weight defined for given dimension", return(1) );
         } else if (dim ==3) {
            DCTERROR( (Transf) && (dim ==3), 
                      "Internal Error, No weight defined for given dimension", return(1) );
         } else if (dim ==4) {
            DCTERROR( (Transf) && (dim ==4), 
                      "Internal Error, No weight defined for given dimension", return(1) );
         }
         break;
      default:
         DCTERROR( Transf, "Internal Error, wrong type of transformation", return(1) );
   } /* End of switch (Transf) */

   return (ierr);
/* -------------------------------------  END( DCT_Perfom_Filter ) */
}

