/*******************************************************************/
/*                 Distributed Coupling Toolkit (DCT)              */
/*!
   \file dct_comm_data.c
  
   \brief Contains all functions implementation about the
          \DCT_Vars data transfer, using a communication library.
  
    This file contains the functions to communicate the \DCT_Vars
    during the DCT Data Exchange Phase. The Send and Receive functions
    for each of the \DCT_Vars structures are implemented here.
    
    \warning The implementation is based on MPI. Some MPI function
             calls should be get rid from here, and a DCT
             interface must be defined instead.

    \date Date of Creation: April 05, 2010.  

    \author Dany De Cecchis: dcecchis@gmail.com
    \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.

*/
/*******************************************************************/

/* -----------------------------------------------  Include Files  */
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "dct.h"
#include "dctsys.h"

/* --------------------------------------------  Global Variables  */

/*******************************************************************/
/*                       DCT_Send_Field                            */
/*                                                                 */
/*!   This function sends (produces) the user values of the a
      variable that was set in the DCT_Field to be produced.

     \param[in,out] var  A pointer to a DCT_Field structure
                         with the data to be sent.

     \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Send_Field( DCT_Field *var )
{
/* ---------------------------------------------  Local Variables  */
   DCT_Error            ierr;
   
   DCT_Produce_Plan    *tdata;
   DCT_Request         *req;
   int                  nmsg;
   
   DCT_CPL_Vars        *CPLMasterT;  
   DCT_Hollow_Var      *HVar;
   
   DCT_Scalar           time;
   
   DCT_Produce_Plan   **indcs;
   
   register int         jj, nvar, vind, NOWnvar;
/* --------------------------------------  BEGIN( DCT_Send_Field ) */

   /* Check if the the function DCT_EndRegistration() was called */
   if (DCT_Step_Check < DCT_END_STATE) {
      ierr.Error_code  = DCT_INVALID_OPERATION;
      ierr.Error_msg   = (DCT_String) malloc((size_t)100);
      sprintf(ierr.Error_msg,
         "[DCT_Send_Field] Invalid Operation. The function DCT_EndRegistration() must be called  before send a variable.\n");
      return(ierr);
   } else if (var->VarCommit == DCT_FALSE){ /* Check if the variable was not commited */
      ierr.Error_code  = DCT_INVALID_OPERATION;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
         "[DCT_Send_Field] Invalid Operation. The DCT_Field <%s> is not commited\n",
         var->VarName);
      return(ierr);
   } else if (var->VarProduced != DCT_PRODUCE){ /* Check if the variable is to be produced */
      ierr.Error_code  = DCT_INVALID_OPERATION;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
         "[DCT_Send_Field] Invalid Operation. The DCT_Field <%s> is not to be produced\n",
          var->VarName);
      return(ierr);
   }
   
   time = var->VarModel->ModCurrentTime;

   /* If it is time to exchange, go on  */
   if ( ( time >= var->VarTimeIni ) && 
       ( (var->VarLastUpdate == DCT_TIME_UNSET) ||
         ( time >= (var->VarLastUpdate + var->VarFrequency) ) ) ) {

      /* Detects the varibles to be fed */
      nvar = (int) var->VarNumCpl; /* How many eschedules this var has */
      indcs = (DCT_Produce_Plan **) malloc( sizeof(DCT_Produce_Plan *)*(size_t)nvar );
      DCTERROR( indcs == (DCT_Produce_Plan **)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

      /* Master Couple List is plugged */
      CPLMasterT = &(DCT_CPL_Table.CPLMasterTable);
      NOWnvar = 0; /* Determine if a variable is being exchanged */

      for ( jj = 0; jj < nvar; jj++ ) {
         vind = (int)var->VarCplIndx[jj];
         HVar = CPLMasterT->CPLVarRemote[vind];
         /* DEtermine if it is time to exchange */
         if ( ( time >= HVar->VarTimeIni ) && 
              ( (HVar->VarLastUpdate == DCT_TIME_UNSET) ||
                ( time >= (HVar->VarLastUpdate + HVar->VarFrequency) ) ) ) {

            tdata = (DCT_Produce_Plan *) CPLMasterT->CPLDataComm[vind];
            req = tdata->reqsts;
            nmsg = tdata->Nmsg;
            if ( MPI_Startall( nmsg, req ) ) {
               ierr.Error_code  = DCT_FAILED_LIB_COMM;
               ierr.Error_msg   = (DCT_String) malloc((size_t)150);
               sprintf(ierr.Error_msg,
                  "[DCT_Send_Field] Error starting the shipment of DCT_Field <%s>\n",
                  var->VarName);
               return(ierr);
            }
            indcs[NOWnvar] = tdata; /* The request is registered */
            NOWnvar++;
   
            if ( HVar->VarLastUpdate == DCT_TIME_UNSET ) HVar->VarLastUpdate = HVar->VarTimeIni;
            else HVar->VarLastUpdate += HVar->VarFrequency;
   
         } /* End of if ( ( time >= HVar->VarTimeIni ) && 
              ( (HVar->VarLastUpdate == DCT_TIME_UNSET) ||
                ( time >= (HVar->VarLastUpdate + HVar->VarFrequency) ) ) ) */

      } /* End of for ( jj = 0; jj < nvar; jj++ ) { */
      var->VarLastUpdate = time;

      for ( jj=0; jj < NOWnvar; jj++ ) {
         tdata = indcs[jj];
         req = tdata->reqsts;
         nmsg = tdata->Nmsg;
         if ( MPI_Waitall( nmsg, req, MPI_STATUSES_IGNORE) ) {
            ierr.Error_code  = DCT_FAILED_LIB_COMM;
            ierr.Error_msg   = (DCT_String) malloc((size_t)150);
            sprintf(ierr.Error_msg,
               "[DCT_Send_Field] Error sending the DCT_Field <%s>\n",
               var->VarName);
            return(ierr);
         }
      }
      
      free ( indcs );
   } /* End of if ( ( time >= var->VarTimeIni ) && ... */

  ierr.Error_code = DCT_SUCCESS;
  ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
  return(ierr);
/* ----------------------------------------  END( DCT_Send_Field ) */
}

/*******************************************************************/
/*                       DCT_Send_3d_Var                           */
/*                                                                 */
/*                                                                 */
/*!   This function sends (produces) the user values of the a
      variable that was set in the DCT_3d_Var to be produced.

     \param[in,out] var  A pointer to a DCT_3d_Var structure
                         with the data to be sent.

     \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Send_3d_Var( DCT_3d_Var *var )
{
/* ---------------------------------------------  Local Variables  */
   DCT_Error            ierr;
   
   DCT_Produce_Plan    *tdata;
   DCT_Request         *req;
   int                  nmsg;
   
   DCT_CPL_Vars        *CPLMasterT;  
   DCT_Hollow_Var      *HVar;
   
   DCT_Scalar           time;
   
   DCT_Produce_Plan   **indcs;
   
   register int         jj, nvar, vind, NOWnvar;
/* -------------------------------------  BEGIN( DCT_Send_3d_Var ) */
  
   /* Check if the the function DCT_EndRegistration() was called */
   if (DCT_Step_Check < DCT_END_STATE) {
      ierr.Error_code  = DCT_INVALID_OPERATION;
      ierr.Error_msg   = (DCT_String) malloc((size_t)100);
      sprintf(ierr.Error_msg,
         "[DCT_Send_3d_Var] Invalid Operation. The function DCT_EndRegistration() must be called  before send a variable.\n");
      return(ierr);
   } else if (var->VarCommit == DCT_FALSE){ /* Check if the variable was not commited */
      ierr.Error_code  = DCT_INVALID_OPERATION;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
         "[DCT_Send_3d_Var] Invalid Operation. The DCT_3d_Var <%s> is not commited\n",
         var->VarName);
      return(ierr);
   } else if (var->VarProduced != DCT_PRODUCE){ /* Check if the variable is to be produced */
      ierr.Error_code  = DCT_INVALID_OPERATION;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
         "[DCT_Send_3d_Var] Invalid Operation. The DCT_3d_Var <%s> is not to be produced\n",
         var->VarName);
      return(ierr);
   }

   time = var->VarModel->ModCurrentTime;
  
   /* If it is time to exchange, go on  */
   if ( ( time >= var->VarTimeIni ) && 
       ( (var->VarLastUpdate == DCT_TIME_UNSET) ||
         ( time >= (var->VarLastUpdate + var->VarFrequency) ) ) ) {

      /* Detects the varibles to be fed */
      nvar = (int) var->VarNumCpl; /* How many eschedules this var has */
      indcs = (DCT_Produce_Plan **) malloc( sizeof(DCT_Produce_Plan *)*(size_t)nvar );
      DCTERROR( indcs == (DCT_Produce_Plan **)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

      /* Master Couple List is plugged */
      CPLMasterT = &(DCT_CPL_Table.CPLMasterTable);
      NOWnvar = 0; /* Determine if a variable is being exchanged */

      for ( jj = 0; jj < nvar; jj++ ) {
         vind = (int)var->VarCplIndx[jj];
         HVar = CPLMasterT->CPLVarRemote[vind];
         /* DEtermine if it is time to exchange */
         if ( ( time >= HVar->VarTimeIni ) && 
              ( (HVar->VarLastUpdate == DCT_TIME_UNSET) ||
                ( time >= (HVar->VarLastUpdate + HVar->VarFrequency) ) ) ) {

            tdata = (DCT_Produce_Plan *) CPLMasterT->CPLDataComm[vind];
            req = tdata->reqsts;
            nmsg = tdata->Nmsg;
            if ( MPI_Startall( nmsg, req ) ) {
               ierr.Error_code  = DCT_FAILED_LIB_COMM;
               ierr.Error_msg   = (DCT_String) malloc((size_t)150);
               sprintf(ierr.Error_msg,
                  "[DCT_Send_3d_Var] Error starting the shipment of DCT_3d_Var <%s>\n",
                  var->VarName);
               return(ierr);
            }
            indcs[NOWnvar] = tdata; /* The request is registered */
            NOWnvar++;
   
            if ( HVar->VarLastUpdate == DCT_TIME_UNSET ) HVar->VarLastUpdate = HVar->VarTimeIni;
            else HVar->VarLastUpdate += HVar->VarFrequency;
   
         } /* End of if ( ( time >= HVar->VarTimeIni ) && 
              ( (HVar->VarLastUpdate == DCT_TIME_UNSET) ||
                ( time >= (HVar->VarLastUpdate + HVar->VarFrequency) ) ) ) */

      } /* End of for ( jj = 0; jj < nvar; jj++ ) { */
      var->VarLastUpdate = time;

      for ( jj=0; jj < NOWnvar; jj++ ) {
         tdata = indcs[jj];
         req = tdata->reqsts;
         nmsg = tdata->Nmsg;
         if ( MPI_Waitall( nmsg, req, MPI_STATUSES_IGNORE) ) {
            ierr.Error_code  = DCT_FAILED_LIB_COMM;
            ierr.Error_msg   = (DCT_String) malloc((size_t)150);
            sprintf(ierr.Error_msg,
               "[DCT_Send_3d_Var] Error sending the DCT_3d_Var <%s>\n",
               var->VarName);
            return(ierr);
         }
      }

      free ( indcs );
   } /* End of if ( ( time >= var->VarTimeIni ) && ... */

  ierr.Error_code = DCT_SUCCESS;
  ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
  return(ierr);
  
/* ---------------------------------------  END( DCT_Send_3d_Var ) */
}

/*******************************************************************/
/*                       DCT_Send_4d_Var                           */
/*                                                                 */
/*                                                                 */
/*!   This function sends (produces) the user values of the a
      variable that was set in the DCT_4d_Var to be produced.

     \param[in,out] var  A pointer to a DCT_4d_Var structure
                         with the data to be sent.

     \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Send_4d_Var( DCT_4d_Var *var )
{
/* ---------------------------------------------  Local Variables  */
   DCT_Error            ierr;
   
   DCT_Produce_Plan    *tdata;
   DCT_Request         *req;
   int                  nmsg;
   
   DCT_CPL_Vars        *CPLMasterT;  
   DCT_Hollow_Var      *HVar;
   
   DCT_Scalar           time;
   
   DCT_Produce_Plan   **indcs;
   
   register int         jj, nvar, vind, NOWnvar;
/* -------------------------------------  BEGIN( DCT_Send_4d_Var ) */
  
   /* Check if the the function DCT_EndRegistration() was called */
   if (DCT_Step_Check < DCT_END_STATE) {
      ierr.Error_code  = DCT_INVALID_OPERATION;
      ierr.Error_msg   = (DCT_String) malloc((size_t)100);
      sprintf(ierr.Error_msg,
         "[DCT_Send_4d_Var] Invalid Operation. The function DCT_EndRegistration() must be called  before send a variable.\n");
      return(ierr);
   } else if (var->VarCommit == DCT_FALSE){ /* Check if the variable was not commited */
      ierr.Error_code  = DCT_INVALID_OPERATION;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
         "[DCT_Send_4d_Var] Invalid Operation. The DCT_4d_Var <%s> is not commited\n",
         var->VarName);
      return(ierr);
   } else if (var->VarProduced != DCT_PRODUCE){ /* Check if the variable is to be produced */
      ierr.Error_code  = DCT_INVALID_OPERATION;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
         "[DCT_Send_4d_Var] Invalid Operation. The DCT_4d_Var <%s> is not to be produced\n",
         var->VarName);
      return(ierr);
   }

   time = var->VarModel->ModCurrentTime;
  
   /* If it is time to exchange, go on  */
   if ( ( time >= var->VarTimeIni ) && 
       ( (var->VarLastUpdate == DCT_TIME_UNSET) ||
         ( time >= (var->VarLastUpdate + var->VarFrequency) ) ) ) {

      /* Detects the varibles to be fed */
      nvar = (int) var->VarNumCpl; /* How many eschedules this var has */
      indcs = (DCT_Produce_Plan **) malloc( sizeof(DCT_Produce_Plan *)*(size_t)nvar );
      DCTERROR( indcs == (DCT_Produce_Plan **)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

      /* Master Couple List is plugged */
      CPLMasterT = &(DCT_CPL_Table.CPLMasterTable);
      NOWnvar = 0; /* Determine if a variable is being exchanged */

      for ( jj = 0; jj < nvar; jj++ ) {
         vind = (int)var->VarCplIndx[jj];
         HVar = CPLMasterT->CPLVarRemote[vind];
         /* DEtermine if it is time to exchange */
         if ( ( time >= HVar->VarTimeIni ) && 
              ( (HVar->VarLastUpdate == DCT_TIME_UNSET) ||
                ( time >= (HVar->VarLastUpdate + HVar->VarFrequency) ) ) ) {

            tdata = (DCT_Produce_Plan *) CPLMasterT->CPLDataComm[vind];
            req = tdata->reqsts;
            nmsg = tdata->Nmsg;
            if ( MPI_Startall( nmsg, req ) ) {
               ierr.Error_code  = DCT_FAILED_LIB_COMM;
               ierr.Error_msg   = (DCT_String) malloc((size_t)150);
               sprintf(ierr.Error_msg,
                  "[DCT_Send_4d_Var] Error starting the shipment of DCT_4d_Var <%s>\n",
                  var->VarName);
               return(ierr);
            }
            indcs[NOWnvar] = tdata; /* The request is registered */
            NOWnvar++;
   
            if ( HVar->VarLastUpdate == DCT_TIME_UNSET ) HVar->VarLastUpdate = HVar->VarTimeIni;
            else HVar->VarLastUpdate += HVar->VarFrequency;
   
         } /* End of if ( ( time >= HVar->VarTimeIni ) && 
              ( (HVar->VarLastUpdate == DCT_TIME_UNSET) ||
                ( time >= (HVar->VarLastUpdate + HVar->VarFrequency) ) ) ) */

      } /* End of for ( jj = 0; jj < nvar; jj++ ) { */
      var->VarLastUpdate = time;

      for ( jj=0; jj < NOWnvar; jj++ ) {
         tdata = indcs[jj];
         req = tdata->reqsts;
         nmsg = tdata->Nmsg;
         if ( MPI_Waitall( nmsg, req, MPI_STATUSES_IGNORE) ) {
            ierr.Error_code  = DCT_FAILED_LIB_COMM;
            ierr.Error_msg   = (DCT_String) malloc((size_t)150);
            sprintf(ierr.Error_msg,
               "[DCT_Send_4d_Var] Error sending the DCT_4d_Var <%s>\n",
               var->VarName);
            return(ierr);
         }
      }

      free ( indcs );
   } /* End of if ( ( time >= var->VarTimeIni ) && ... */

  ierr.Error_code = DCT_SUCCESS;
  ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
  return(ierr);
  
/* ---------------------------------------  END( DCT_Send_4d_Var ) */
}

/*******************************************************************/
/*                        DCT_Recv_Field                           */
/*                                                                 */
/*!   This function receives (consumes) the user values of the a
      variable that was set in the DCT_Field to be consumed.
      This function perform all the afterwards operations:
      Data type cast, Interpolation/Filter, unit translation, etc.
      To have the data ready to use in the user array.

     \param[in,out] var  A pointer to a DCT_Field structure
                         with the data to be sent.

     \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Recv_Field( DCT_Field *var )
{
/* ---------------------------------------------  Local Variables  */
   DCT_Error            ierr;
   
   int                  nmsg;
   
   DCT_Consume_Plan    *tdata;
   DCT_Request         *req;
   
   DCT_CPL_Vars        *CPLMasterT;  
   DCT_Hollow_Var      *HVar;
   
   DCT_Scalar           time;
   
   register int         vind;
/* --------------------------------------  BEGIN( DCT_Recv_Field ) */
  
   /* Check if the the function DCT_EndRegistration() was called */
   if (DCT_Step_Check < DCT_END_STATE) {
      ierr.Error_code  = DCT_INVALID_OPERATION;
      ierr.Error_msg   = (DCT_String) malloc((size_t)100);
      sprintf(ierr.Error_msg,
         "[DCT_Recv_Field] Invalid Operation. The function DCT_EndRegistration() must be called  before receive a variable.\n");
      return(ierr);
   } else if (var->VarCommit == DCT_FALSE){ /* Check if the variable was not commited */
      ierr.Error_code  = DCT_INVALID_OPERATION;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
         "[DCT_Recv_Field] Invalid Operation. The DCT_Field <%s> is not commited\n",
         var->VarName);
      return(ierr);
   } else if (var->VarProduced != DCT_CONSUME){ /* Check if the variable is to be consumed */
      ierr.Error_code  = DCT_INVALID_OPERATION;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
         "[DCT_Recv_Field] Invalid Operation. The DCT_Field <%s> is not to be consumed\n",
         var->VarName);
      return(ierr);
   }

   time = var->VarModel->ModCurrentTime;

   /* If it is time to exchange, go on  */
   if ( ( time >= var->VarTimeIni ) && 
        ( (var->VarLastUpdate == DCT_TIME_UNSET) ||
          ( time >= (var->VarLastUpdate + var->VarFrequency) ) ) ) {

      /* Master Couple List is plugged */
      CPLMasterT = &(DCT_CPL_Table.CPLMasterTable);
      vind = (int) *(var->VarCplIndx);
      HVar = CPLMasterT->CPLVarRemote[vind];
      if ( ( time >= HVar->VarTimeIni ) && 
           ( (HVar->VarLastUpdate == DCT_TIME_UNSET) ||
             ( time >= (HVar->VarLastUpdate + HVar->VarFrequency) ) ) ) {

         tdata = (DCT_Consume_Plan *) CPLMasterT->CPLDataComm[vind];
         nmsg = tdata->Nmsg;
//          vtag = tdata->VarMsgTag;
         req = tdata->reqsts;

//          req = (DCT_Request *) malloc ( sizeof(DCT_Request)*(size_t)nmsg );
//          DCTERROR( req == (DCT_Request *)NULL, "Memory Allocation Failed",
//                         ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
       
         if ( MPI_Startall( nmsg, req ) ) {
            ierr.Error_code  = DCT_FAILED_LIB_COMM;
            ierr.Error_msg   = (DCT_String) malloc((size_t)150);
            sprintf(ierr.Error_msg,
               "[DCT_Recv_Field] Error starting the receipt of DCT_Field <%s>\n",
               var->VarName);
            return(ierr);
         }
//          for ( ii =0; ii < nmsg; ii++ ) {
//             Msg = tdata->Msg + ii;
//             if ( MPI_Irecv( Msg->IniPtr, Msg->Count, Msg->DType, Msg->Rank,
//                                                       vtag, DCT_Comm_World, (req+ii) ) ) {
//                ierr.Error_code  = DCT_FAILED_LIB_COMM;
//                ierr.Error_msg   = (DCT_String) malloc((size_t)150);
//                sprintf(ierr.Error_msg,
//                   "[DCT_Recv_Field] Error receiving the DCT_Field <%s>\n",
//                   var->VarName);
//                return(ierr);
//             }
//          }
         if ( HVar->VarLastUpdate == DCT_TIME_UNSET ) HVar->VarLastUpdate = HVar->VarTimeIni;
         else HVar->VarLastUpdate += HVar->VarFrequency;
         
         /* Make sure that the buffer can be changed */
         if ( MPI_Waitall( nmsg, req, MPI_STATUSES_IGNORE ) ) {
            ierr.Error_code  = DCT_FAILED_LIB_COMM;
            ierr.Error_msg   = (DCT_String) malloc((size_t)150);
            sprintf(ierr.Error_msg,
               "[DCT_Recv_Field] Error receiving the DCT_Field <%s>\n",
               var->VarName);
            return(ierr);
         }
//          free( req );
         
         /* The filter transformation */
         if ( DCT_Perfom_Filter( tdata, var->VarUserDataType, 2 ) )  {
            ierr.Error_code  = DCT_FAILED_TRANSFOR;
            ierr.Error_msg   = (DCT_String) malloc((size_t)150);
            sprintf(ierr.Error_msg,
               "[DCT_Recv_Field] Error performing transformation of DCT_Field <%s>\n",
               var->VarName);
            return(ierr);
         }
         
         /* The unit convertion */
         if ( DCT_Perfom_UnitConvertion( tdata, var->VarValues, var->VarUserDataType,
                                         var->VarUnits, var->VarDim, 2 ) )  {
            ierr.Error_code  = DCT_FAILED_TRANSFOR;
            ierr.Error_msg   = (DCT_String) malloc((size_t)150);
            sprintf(ierr.Error_msg,
               "[DCT_Recv_Field] Error performing unit convertion of DCT_Field <%s>\n",
               var->VarName);
            return(ierr);
         }

   
      } /* End of if ( ( time >= HVar->VarTimeIni ) && ... */
      var->VarLastUpdate = time;

   } /* End of if ( ( time >= var->VarTimeIni ) && ... */
   ierr.Error_code = DCT_SUCCESS;
   ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
   return(ierr);
  
/* ----------------------------------------  END( DCT_Recv_Field ) */
}

/*******************************************************************/
/*                      DCT_Recv_3d_Var                            */
/*                                                                 */
/*!   This function receives (consumes) the user values of the a
      variable that was set in the DCT_3d_Var to be consumed.
      This function perform all the afterwards operations:
      Data type cast, Interpolation/Filter, unit translation, etc.
      To have the data ready to use in the user array.

     \param[in,out] var  A pointer to a DCT_3d_Var structure
                         with the data to be sent.

     \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Recv_3d_Var( DCT_3d_Var *var )
{
/* ---------------------------------------------  Local Variables  */
   DCT_Error            ierr;

   int                  nmsg;
   
   DCT_Consume_Plan    *tdata;
   DCT_Request         *req;
   
   DCT_CPL_Vars        *CPLMasterT;  
   DCT_Hollow_Var      *HVar;
   
   DCT_Scalar           time;
   
   register int         vind;
/* -------------------------------------  BEGIN( DCT_Recv_3d_Var ) */
  
   /* Check if the the function DCT_EndRegistration() was called */
   if (DCT_Step_Check < DCT_END_STATE) {
      ierr.Error_code  = DCT_INVALID_OPERATION;
      ierr.Error_msg   = (DCT_String) malloc((size_t)100);
      sprintf(ierr.Error_msg,
         "[DCT_Recv_3d_Var] Invalid Operation. The function DCT_EndRegistration() must be called  before receive a variable.\n");
      return(ierr);
   } else if (var->VarCommit == DCT_FALSE){ /* Check if the variable was not commited */
      ierr.Error_code  = DCT_INVALID_OPERATION;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
         "[DCT_Recv_3d_Var] Invalid Operation. The DCT_3d_Var <%s> is not commited\n",
         var->VarName);
      return(ierr);
   } else if (var->VarProduced != DCT_CONSUME){ /* Check if the variable is to be consumed */
      ierr.Error_code  = DCT_INVALID_OPERATION;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
         "[DCT_Recv_3d_Var] Invalid Operation. The DCT_3d_Var <%s> is not to be consumed\n",
         var->VarName);
      return(ierr);
   }

   time = var->VarModel->ModCurrentTime;
  
/*                                                   BEGIN DEBUG */
//    printf("[DCT_Recv_3d_Var] Receiver %d, Variable %s. Time %lg >= %lg, %d *******\n",
//           DCT_procinfo.ProcDCTRank, var->VarName, time, var->VarLastUpdate + var->VarFrequency,
//           time >= (var->VarLastUpdate + var->VarFrequency) );
/*                                                     END DEBUG */
   /* If it is time to exchange, go on  */
   if ( ( time >= var->VarTimeIni ) && 
        ( (var->VarLastUpdate == DCT_TIME_UNSET) ||
          ( time >= (var->VarLastUpdate + var->VarFrequency) ) ) ) {

      /* Master Couple List is plugged */
      CPLMasterT = &(DCT_CPL_Table.CPLMasterTable);
      vind = (int) *(var->VarCplIndx);
      HVar = CPLMasterT->CPLVarRemote[vind];
      if ( ( time >= HVar->VarTimeIni ) && 
           ( (HVar->VarLastUpdate == DCT_TIME_UNSET) ||
             ( time >= (HVar->VarLastUpdate + HVar->VarFrequency) ) ) ) {

         tdata = (DCT_Consume_Plan *) CPLMasterT->CPLDataComm[vind];
         nmsg = tdata->Nmsg;
//          vtag = tdata->VarMsgTag;
         req = tdata->reqsts;
    
//          req = (DCT_Request *) malloc ( sizeof(DCT_Request)*(size_t)nmsg );
//          DCTERROR( req == (DCT_Request *)NULL, "Memory Allocation Failed",
//                         ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    
         if ( MPI_Startall( nmsg, req ) ) {
            ierr.Error_code  = DCT_FAILED_LIB_COMM;
            ierr.Error_msg   = (DCT_String) malloc((size_t)150);
            sprintf(ierr.Error_msg,
               "[DCT_Recv_3d_Var] Error starting the receipt of DCT_3d_Var <%s>\n",
               var->VarName);
            return(ierr);
         }
//          for ( ii =0; ii < nmsg; ii++ ) {
//             Msg = tdata->Msg + ii;
//             if ( MPI_Irecv( Msg->IniPtr, Msg->Count, Msg->DType, Msg->Rank,
//                                                       vtag, DCT_Comm_World, (req+ii) ) ) {
//                ierr.Error_code  = DCT_FAILED_LIB_COMM;
//                ierr.Error_msg   = (DCT_String) malloc((size_t)150);
//                sprintf(ierr.Error_msg,
//                   "[DCT_Recv_3d_Var] Error receiving the DCT_3d_Var <%s>\n",
//                   var->VarName);
//                return(ierr);
//             }
//          }
         if ( HVar->VarLastUpdate == DCT_TIME_UNSET ) HVar->VarLastUpdate = HVar->VarTimeIni;
         else HVar->VarLastUpdate = time;
//         else HVar->VarLastUpdate += HVar->VarFrequency;
/*                                                   BEGIN DEBUG */
//          printf("[DCT_Recv_3d_Var] Receiver %d, Time %g, MSG TAG sent: %d\n",
//                                         DCT_procinfo.ProcDCTRank, time, vtag );
//          printf("[DCT_Recv_3d_Var] Receiver %d, Remote var %s at Time %lg\n\n",
//                               DCT_procinfo.ProcDCTRank, HVar->VarName , HVar->VarLastUpdate );
/*                                                     END DEBUG */
    
         /* Make sure that the buffer can be changed */
         if ( MPI_Waitall( nmsg, req, MPI_STATUSES_IGNORE ) ) {
            ierr.Error_code  = DCT_FAILED_LIB_COMM;
            ierr.Error_msg   = (DCT_String) malloc((size_t)150);
            sprintf(ierr.Error_msg,
               "[DCT_Recv_3d_Var] Error receiving the DCT_3d_Var <%s>\n",
               var->VarName);
            return(ierr);
         }
//          free( req );

         /* The filter transformation */
         if ( DCT_Perfom_Filter( tdata, var->VarUserDataType, 3 ) )  {
            ierr.Error_code  = DCT_FAILED_TRANSFOR;
            ierr.Error_msg   = (DCT_String) malloc((size_t)150);
            sprintf(ierr.Error_msg,
               "[DCT_Recv_3d_Var] Error performing transformation of DCT_3d_Var <%s>\n",
               var->VarName);
            return(ierr);
         }
         
         /* The unit convertion */
         if ( DCT_Perfom_UnitConvertion( tdata, var->VarValues, var->VarUserDataType,
                                         var->VarUnits, var->VarDim, 3 ) )  {
            ierr.Error_code  = DCT_FAILED_TRANSFOR;
            ierr.Error_msg   = (DCT_String) malloc((size_t)150);
            sprintf(ierr.Error_msg,
               "[DCT_Recv_3d_Var] Error performing unit convertion of DCT_3d_Var <%s>\n",
               var->VarName);
            return(ierr);
         }

      } /* End of if ( ( time >= HVar->VarTimeIni ) && ... */
      var->VarLastUpdate = time;

   } /* End of if ( ( time >= var->VarTimeIni ) && ... */
   ierr.Error_code = DCT_SUCCESS;
   ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
   return(ierr);
  
/* ---------------------------------------  END( DCT_Recv_3d_Var ) */
}

/*******************************************************************/
/*                      DCT_Recv_4d_Var                            */
/*!   This function receives (consumes) the user values of the a
      variable that was set in the DCT_4d_Var to be consumed.
      This function perform all the afterwards operations:
      Data type cast, Interpolation/Filter, unit translation, etc.
      To have the data ready to use in the user array.

     \param[in,out] var  A pointer to a DCT_4d_Var structure
                         with the data to be sent.

     \return An error handler variable.

*/
/*******************************************************************/
DCT_Error DCT_Recv_4d_Var( DCT_4d_Var *var )
{
/* ---------------------------------------------  Local Variables  */
   DCT_Error            ierr;
   
   int                  nmsg;
   
   DCT_Consume_Plan    *tdata;
   DCT_Request         *req;
   
   DCT_CPL_Vars        *CPLMasterT;  
   DCT_Hollow_Var      *HVar;
   
   DCT_Scalar           time;
   
   register int         vind;
/* -------------------------------------  BEGIN( DCT_Recv_4d_Var ) */
  
   /* Check if the the function DCT_EndRegistration() was called */
   if (DCT_Step_Check < DCT_END_STATE) {
      ierr.Error_code  = DCT_INVALID_OPERATION;
      ierr.Error_msg   = (DCT_String) malloc((size_t)100);
      sprintf(ierr.Error_msg,
         "[DCT_Recv_4d_Var] Invalid Operation. The function DCT_EndRegistration() must be called  before receive a variable.\n");
      return(ierr);
   } else if (var->VarCommit == DCT_FALSE){ /* Check if the variable was not commited */
      ierr.Error_code  = DCT_INVALID_OPERATION;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
         "[DCT_Recv_4d_Var] Invalid Operation. The DCT_4d_Var <%s> is not commited\n",
         var->VarName);
      return(ierr);
   } else if (var->VarProduced != DCT_CONSUME){ /* Check if the variable is to be consumed */
      ierr.Error_code  = DCT_INVALID_OPERATION;
      ierr.Error_msg   = (DCT_String) malloc((size_t)150);
      sprintf(ierr.Error_msg,
         "[DCT_Recv_4d_Var] Invalid Operation. The DCT_4d_Var <%s> is not to be consumed\n",
         var->VarName);
      return(ierr);
   }

   time = var->VarModel->ModCurrentTime;
  
   /* If it is time to exchange, go on  */
   if ( ( time >= var->VarTimeIni ) && 
        ( (var->VarLastUpdate == DCT_TIME_UNSET) ||
          ( time >= (var->VarLastUpdate + var->VarFrequency) ) ) ) {

      /* Master Couple List is plugged */
      CPLMasterT = &(DCT_CPL_Table.CPLMasterTable);
      vind = (int) *(var->VarCplIndx);
      HVar = CPLMasterT->CPLVarRemote[vind];
      if ( ( time >= HVar->VarTimeIni ) && 
           ( (HVar->VarLastUpdate == DCT_TIME_UNSET) ||
             ( time >= (HVar->VarLastUpdate + HVar->VarFrequency) ) ) ) {

         tdata = (DCT_Consume_Plan *) CPLMasterT->CPLDataComm[vind];
         nmsg = tdata->Nmsg;
//          vtag = tdata->VarMsgTag;
         req = tdata->reqsts;
       
//          req = (DCT_Request *) malloc ( sizeof(DCT_Request)*(size_t)nmsg );
//          DCTERROR( req == (DCT_Request *)NULL, "Memory Allocation Failed",
//                         ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    
         if ( MPI_Startall( nmsg, req ) ) {
            ierr.Error_code  = DCT_FAILED_LIB_COMM;
            ierr.Error_msg   = (DCT_String) malloc((size_t)150);
            sprintf(ierr.Error_msg,
               "[DCT_Recv_4d_Var] Error starting the receipt of DCT_4d_Var <%s>\n",
               var->VarName);
            return(ierr);
         }
//          for ( ii =0; ii < nmsg; ii++ ) {
//             Msg = tdata->Msg + ii;
//             if ( MPI_Irecv( Msg->IniPtr, Msg->Count, Msg->DType, Msg->Rank,
//                                                       vtag, DCT_Comm_World, (req+ii) ) ) {
//                ierr.Error_code  = DCT_FAILED_LIB_COMM;
//                ierr.Error_msg   = (DCT_String) malloc((size_t)150);
//                sprintf(ierr.Error_msg,
//                   "[DCT_Recv_4d_Var] Error receiving the DCT_4d_Var <%s>\n",
//                   var->VarName);
//                return(ierr);
//             }
//          }
         if ( HVar->VarLastUpdate == DCT_TIME_UNSET ) HVar->VarLastUpdate = HVar->VarTimeIni;
         else HVar->VarLastUpdate += HVar->VarFrequency;
        
         /* Make sure that the buffer can be changed */
         if ( MPI_Waitall( nmsg, req, MPI_STATUSES_IGNORE ) ) {
            ierr.Error_code  = DCT_FAILED_LIB_COMM;
            ierr.Error_msg   = (DCT_String) malloc((size_t)150);
            sprintf(ierr.Error_msg,
               "[DCT_Recv_4d_Var] Error receiving the DCT_4d_Var <%s>\n",
               var->VarName);
            return(ierr);
         }
//          free( req );

         /* The filter transformation */
         if ( DCT_Perfom_Filter( tdata, var->VarUserDataType, 4 ) )  {
            ierr.Error_code  = DCT_FAILED_TRANSFOR;
            ierr.Error_msg   = (DCT_String) malloc((size_t)150);
            sprintf(ierr.Error_msg,
               "[DCT_Recv_4d_Var] Error performing transformation of DCT_4d_Var <%s>\n",
               var->VarName);
            return(ierr);
         }
         
         /* The unit convertion */
         if ( DCT_Perfom_UnitConvertion( tdata, var->VarValues, var->VarUserDataType,
                                         var->VarUnits, var->VarDim, 4 ) )  {
            ierr.Error_code  = DCT_FAILED_TRANSFOR;
            ierr.Error_msg   = (DCT_String) malloc((size_t)150);
            sprintf(ierr.Error_msg,
               "[DCT_Recv_4d_Var] Error performing unit convertion of DCT_4d_Var <%s>\n",
               var->VarName);
            return(ierr);
         }

      } /* End of if ( ( time >= HVar->VarTimeIni ) && ... */
      var->VarLastUpdate = time;

   } /* End of if ( ( time >= var->VarTimeIni ) && ... */
   ierr.Error_code = DCT_SUCCESS;
   ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
   return(ierr);
  
/* ---------------------------------------  END( DCT_Recv_4d_Var ) */
}
