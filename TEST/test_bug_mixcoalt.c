#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <mpi.h>
#ifdef MEMTRK
  #include <TAU.h>
#endif

// Johnny's model
extern int subflow(int, char**, MPI_Comm, MPI_Comm);
// Matthews' model
extern int resflow(int, char**, MPI_Comm, MPI_Comm);

int main(int argc, char *argv[])
{
  int numtasks, rank, rc, color;
  MPI_Comm lcomm;

  rc = MPI_Init(&argc, &argv);
   
  if(rc != MPI_SUCCESS) {
    printf("Error starting MPI program.\n");
    MPI_Abort(MPI_COMM_WORLD, rc);
  }

  rc  = MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
  rc |= MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if(rc != MPI_SUCCESS) {
    printf("Error asking rank and comm size.\n");
    MPI_Abort(MPI_COMM_WORLD, rc);
  }

  color = rank;

  rc = MPI_Comm_split(MPI_COMM_WORLD, color, 0, &lcomm);

  if(rank == 0) {
    printf("starting subflow\n");
    rc = subflow(argc, argv, lcomm, MPI_COMM_WORLD);
  } else { 
    printf("starting resflow\n");
    rc = resflow(argc, argv, lcomm, MPI_COMM_WORLD);
  }
  printf("finished running my model on rank %d\n",rank);
  if(rc) {
    printf("Error calling function in process %d.\n", rank);
    MPI_Abort(MPI_COMM_WORLD, rc);
  }

  MPI_Finalize();
  return 0;
}
