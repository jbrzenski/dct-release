/*******************************************************************/
/*                 Distributed Coupling Toolkit (DCT)              */
/*                                                                 */
/*!
    \file interp_tools.h                       
                                                            
    \brief   Header file for the tools to check interpolation      
    results                                               

    \date Created on Aug 10, 2011                
                                                            
    \author Dany De Cecchis: dcecchis@gmail.com
    \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.

*/
/*******************************************************************/
/*
      $Id: interp_tools.h,v 1.3 2011/10/09 00:51:01 dcecchis Exp $
*/

# ifndef INTERP_TOOLS_H
#   define INTERP_TOOLS_H

#include <dct.h>

int fprtdat( DCT_Integer, DCT_Integer, DCT_Scalar *, DCT_Scalar *, float *,
              char *, char *, char *, char * );

int dprtdat( DCT_Integer, DCT_Integer, DCT_Scalar *, DCT_Scalar *, double *,
              char *, char *, char *, char *);

int ldprtdat( DCT_Integer, DCT_Integer, DCT_Scalar *, DCT_Scalar *, long double *,
               char *, char *, char *, char *);
            
int dprtdatMatrix( DCT_Integer, DCT_Integer, double *, char *, char * );

/* Value initialization function */
int ffuncEval(DCT_Scalar *, DCT_Scalar *, float *, DCT_Integer, DCT_Integer );
int dfuncEval(DCT_Scalar *, DCT_Scalar *, double *, DCT_Integer, DCT_Integer );
int ldfuncEval(DCT_Scalar *, DCT_Scalar *, long double *, DCT_Integer, DCT_Integer );
int ffuncEval3D(DCT_Scalar *, DCT_Scalar *, DCT_Scalar *, float *, DCT_Integer,
                DCT_Integer, DCT_Integer );
int dfuncEval3D(DCT_Scalar *, DCT_Scalar *, DCT_Scalar *, double *, DCT_Integer,
                DCT_Integer, DCT_Integer );
int ldfuncEval3D(DCT_Scalar *, DCT_Scalar *, DCT_Scalar *, long double *, DCT_Integer,
                DCT_Integer, DCT_Integer );

/* Laplacian operator matrices */
int dlaplacian_matrix( double *, int, int );
int dlaplacian_bandmatrix( double *, int, int );
# endif /* INTERP_TOOLS_H */
