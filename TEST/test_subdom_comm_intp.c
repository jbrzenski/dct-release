/**************************************************************/
/*           test_subdom_subdom_comm_intp.c                   */
/*                                                            */
/*      This program is to test the communication and         */
/*      interpolation between to models A and B               */
/**************************************************************/
/*  Created on Jun 17, 2011                */
/*                                                            */
/**************************************************************/

/*
      $Id: test_subdom_comm_intp.c,v 1.4 2011/12/06 02:53:32 dcecchis Exp $
*/
/*****************************************************************/
/*
      $Log: test_subdom_comm_intp.c,v $
      Revision 1.4  2011/12/06 02:53:32  dcecchis
      The new registration process was implemented. This make that each process
      calculates its own preliminary subdomain calculation

      Revision 1.3  2011/07/13 02:23:04  dcecchis
      The test is modified to use the new message tag

      Revision 1.2  2011/07/13 02:10:18  dcecchis
      The message tag is change to fix the upper tag number limitation

      Revision 1.1  2011/07/12 22:40:17  dcecchis
      Tests files using only C code. This use data from files about temperature
      and wind stress.

*/
/*****************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <mpi.h>
#ifdef MEMTRK
  #include <TAU.h>
#endif
//#include "dct.h"

#define MAXNP  6
#define SPLIT  3

/******************************************************/
/*                    PROGRAM MAIN                    */
/*  Tests and Shows the use of the interface for      */
/*  creating, specifying and declaring a Field type   */
/******************************************************/
int main(int  argc, char *argv[])
{

/* ---------------------------------------  Variables Declaration  */
   int numtasks, color, rank, rc;
   
   MPI_Comm dom_comm;

/* ------------------------------------------------  BEGIN( main ) */


   /****************************************************************
    *****              the MPI system is initialized           *****
    ****************************************************************/
   rc = MPI_Init(&argc, &argv);
   if (rc != MPI_SUCCESS) {
      printf("Error starting MPI program. Terminating.\n");
      MPI_Abort(MPI_COMM_WORLD,rc);
   }

   rc  = MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
   rc |= MPI_Comm_rank(MPI_COMM_WORLD, &rank);
   if (rc != MPI_SUCCESS) {
      printf("Error asking rank and comm size. Terminating.\n");
      MPI_Abort(MPI_COMM_WORLD,rc);
   }
   // printf("This is the process %d of %d\n", rank, numtasks);
   
   
   if ( numtasks != MAXNP ) {
      printf("The number of processors does not agree. Terminating.\n");
      MPI_Abort(MPI_COMM_WORLD,rc);   
   }
   
   if ( rank < SPLIT ) color = 0;
   else            color = 1;
   
   rc  = MPI_Comm_split( MPI_COMM_WORLD, color, 0, &dom_comm );
   if ( rank < SPLIT )
      rc = test_sd_comm_a( rank, numtasks, dom_comm, MPI_COMM_WORLD );
   else 
      rc = test_sd_comm_b( rank, numtasks, dom_comm, MPI_COMM_WORLD );
   if ( rc ) {
      printf("Error calling sub domain function in process %d.\n", rank );
      MPI_Abort( MPI_COMM_WORLD, rc );
   }

   /****************************************************************
    *****              the MPI system is finalized             *****
    ****************************************************************/
   MPI_Finalize();
   return 0;

/* --------------------------------------------------  END( main ) */

}
