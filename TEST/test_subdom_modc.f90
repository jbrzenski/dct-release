SUBROUTINE test_subdom_modc ( model_comm, world_comm )

!===============================================================
!  FILE: test_subdom_modc.f90
!===============================================================
! Description: This file a dummy model a to test subdomain
!              mapping
!
!
! 20-Jul-2011: Created by: Dany De Cecchis
!---------------------------------------------------------------------
!$Id: test_subdom_modc.f90,v 1.5 2012/08/22 03:42:01 dcecchis Exp $
!
! DCT library module
USE DCT_FORTRAN
USE INTERP_TOOLS

IMPLICIT NONE

include "mpif.h"

! Global prpoblem size
INTEGER, PARAMETER    ::  Pdim = 2    ! Problem dimension
INTEGER, PARAMETER    ::  IMax = 26
INTEGER, PARAMETER    ::  JMax = 26

! Communication variables
INTEGER, INTENT(IN)                :: model_comm, world_comm

! Local Variables:
DOUBLE PRECISION                    :: time         ! Simulation Time
DOUBLE PRECISION                    :: dt           ! Time step
INTEGER                             :: i,j,k
INTEGER                             :: StepN, iterM
DOUBLE PRECISION                    :: coef
INTEGER                             :: nI, nJ, iniI, endI, iniJ, endJ
INTEGER                             :: pnI, pnJ, piniI, pendI, piniJ, pendJ

! DCT structures descriptors variabels
TYPE (DCT_FIELD)                    :: dctvx, dctvy, dctp
TYPE (DCT_MODEL)                    :: dmodel
TYPE (DCT_COUPLE)                   :: dcouple, dcouple2

integer, dimension(2)               :: npts= (/ IMax, JMax /) ! Global mesh point
!integer, dimension(2)               :: vnpts= (/ IMax-1, JMax-1/) ! Global var point

REAL, ALLOCATABLE, DIMENSION (:,:) :: cplvx, cplvy, cplp  ! Coupling Variables
DOUBLE PRECISION, ALLOCATABLE, DIMENSION (:) :: mx, my ! Coordinates of the
                                                           ! mesh points

DOUBLE PRECISION, ALLOCATABLE, DIMENSION ( :, :) :: grx, gry ! grid coordinates

DOUBLE PRECISION, ALLOCATABLE, DIMENSION (:) :: scx, scy ! Coordinates of the
                                                             ! variables points
! Model Variables
REAL, ALLOCATABLE, DIMENSION (:,:) :: vx, vy, p

integer                             :: err
CHARACTER(LEN=150)                  :: imesg
CHARACTER(LEN=3)                    :: FileProc

! MPI Variables
INTEGER, PARAMETER                  :: MNPS = 2  ! Model number of processes
INTEGER                             :: rank, nprocs
INTEGER, DIMENSION(MNPS)            :: lstranks
INTEGER, DIMENSION(Pdim,MNPS)       :: sdinipos ! Subdomain initial position
INTEGER, DIMENSION(Pdim,MNPS)       :: sdendpos ! Subdomain ending position

call MPI_COMM_SIZE(world_comm, nprocs,err)
call MPI_COMM_RANK(world_comm, rank,err)

!print *, 'I am process ', rank, ' in model A'

allocate( mx(Imax), my(Jmax) )

mx(1) = 0.0D+0
do i=2, Imax
   mx(i) = mx(1) + DBLE(i-1)*5.0D+0
enddo
my(1) = -35.0D+0
do j=2, Jmax
   my(j) = my(1) + DBLE(j-1)*5.0D+0
enddo

select case (rank)
!   case (11)
!      nI = 8
!      nJ = 4
!      iniI = 1
!      endI = 9
!      iniJ = 1
!      endJ = 5
!   case (10)
!      nI = 9
!      nJ = 4
!      iniI = 9
!      endI = 18
!      iniJ = 1
!      endJ = 5
!   case (9)
!      nI = 8
!      nJ = 4
!      iniI = 18
!      endI = 26
!      iniJ = 1
!      endJ = 5
!   case (8)
!      nI = 8
!      nJ = 7
!      iniI = 1
!      endI = 9
!      iniJ = 5
!      endJ = 12
!   case (7)
!      nI = 9
!      nJ = 7
!      iniI = 9
!      endI = 18
!      iniJ = 5
!      endJ = 12
!   case (6)
!      nI = 8
!      nJ = 7
!      iniI = 18
!      endI = 26
!      iniJ = 5
!      endJ = 12
!   case (5)
!      nI = 8
!      nJ = 7
!      iniI = 1
!      endI = 9
!      iniJ = 12
!      endJ = 19
!   case (4)
!      nI = 9
!      nJ = 7
!      iniI = 9
!      endI = 18
!      iniJ = 12
!      endJ = 19
!   case (3)
!      nI = 8
!      nJ = 7
!      iniI = 18
!      endI = 26
!      iniJ = 12
!      endJ = 19
!   case (2)
!      nI = 8
!      nJ = 7
!      iniI = 1
!      endI = 9
!      iniJ = 19
!      endJ = 26
   case (7)
      nI = 25
      nJ = 13
      iniI = 1
      endI = 26
      iniJ = 13
      endJ = 26
      pnI = 26
      pnJ = 13
      piniI = 1
      pendI = 26
      piniJ = 14
      pendJ = 26
   case (6)
      nI = 25
      nJ = 12
      iniI = 1
      endI = 26
      iniJ = 1
      endJ = 13
      pnI = 26
      pnJ = 13
      piniI = 1
      pendI = 26
      piniJ = 1
      pendJ = 13
end select
if ( rank < 10 ) then
   write(FileProc,'(I1)') rank
elseif ( rank < 100 ) then
   write(FileProc,'(I2)') rank
else
   write(FileProc,'(I3)') rank
endif
allocate( scx(nI), scy(nJ) )
allocate( vx(nI,nJ), vy(nI,nJ), p(pnI,pnJ) )

!! Setting the subdomain
! Rank (1)
lstranks(1) = 7
sdinipos(1,1) = 1
sdendpos(1,1) = 26
sdinipos(2,1) = 13
sdendpos(2,1) = 26

! Rank (0)
lstranks(2) = 6
sdinipos(1,2) = 1
sdendpos(1,2) = 26
sdinipos(2,2) = 1
sdendpos(2,2) = 13


! Variables ticks marks
!!! Scalar variables coordinates. The scalar variables are
!!! in the center point of the cell 
scx = ( mx( (iniI+1):endI ) + mx( iniI:(endI-1) ) )/2
scy = ( my( (iniJ+1):endJ ) + my( iniJ:(endJ-1) ) )/2


! Initializing the model
time=0.0D0
dt = 1.0D-3
iterM = 5

!***********************************************************************
!                                 Declaring the Coupling structures
!***********************************************************************
!!!!!!!! Variables allocation needed int he coupling process 
! The variables to connect with the coupling
! Coupling variables, where the user should dump or
! recover the values for coupling
allocate( cplvx(nI,nJ), cplvy(nI,nJ), cplp(pnI,pnJ) )

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!  *********************** Begin Registration Call *******************
call dct_beginregistration ( world_comm, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'calling Beginregistration'
end if
print *, "Despues de registro 3"
!!! ********************** The Model is registered  ******************** 
call dct_create_model (dmodel, "test_subdom_modc",&
                      & "Dummy model C to test comm and subdomain mapping",&
                      & MNPS, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'registering model'
end if

call dct_set_model_time (dmodel, Time, dt, DCT_TIME_NO_UNIT, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'model time'
end if

call dct_set_model_dom (dmodel, Pdim, 1, DCT_RECTILINEAR, mx, IMax,&
                       & DCT_F_ORDER, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'model domain dir 1'
end if

call dct_set_model_dom (dmodel, Pdim, 2, DCT_RECTILINEAR, my, JMax,&
                       & DCT_F_ORDER, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'model domain dir 2'
end if
print *, "Antes parlaout 3"
! call dct_set_model_parlayout (dmodel, DCT_DIST_RECTANGULAR, (/1 , 2/),&
!                             & lstranks, DCT_F_ORDER, err, imesg)
call dct_set_model_parlayout (dmodel, DCT_DIST_RECTANGULAR, (/1 , 2/),&
                            & DCT_F_ORDER, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'model parlayout'
end if
print *, "Despues de parlaout 3"
call dct_set_model_subdom (dmodel, lstranks, sdinipos, sdendpos,&
                          & DCT_F_ORDER, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'model parlayout'
end if
print *, "Despues de modelo 3"

!!!! ********************** Setting the Vx variable  ******************** 
call dct_create_field ( dctvx, "Vx",&
                         & "Dumm var Vx on model C",&
                         & DCT_NO_UNITS, DCT_CONSUME, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, imesg
  stop 'creating Vx'
end if

call dct_set_field_dims ( dctvx, nI, nJ, DCT_F_ORDER, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'dimensioning Vx'
end if

call dct_set_field_val_location ( dctvx, DCT_LOC_CENTERED, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'var location Vx'
end if

call dct_set_field_labels( dctvx, DCT_RECTILINEAR, scx , nI, &
                            &  DCT_RECTILINEAR, scy, nJ, &
                            &  DCT_F_ORDER, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'labeling Vx'
end if

call dct_set_field_time ( dctvx, DCT_TIME_NO_UNIT, 0.0D0, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'timing Vx'
end if

call dct_set_field_freq_consumption ( dctvx, dt, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'frequency Vx'
end if


call dct_set_field_values ( dctvx, DCT_REAL, cplvx, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'connecting Vx'
end if

!!!! ******************* Linking Vx to the model  ***************** 
call dct_set_model_var ( dmodel, dctvx, DCT_FIELD_TYPE, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'linking Vx'
end if
print *, "Despues de Vx 3"

!!!! ********************** Setting the Vy variable  ******************** 
call dct_create_field ( dctvy, "Vy",&
                         & "Dumm var Vy on model C",&
                         & DCT_NO_UNITS, DCT_CONSUME, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, imesg
  stop 'creating Vy'
end if

call dct_set_field_dims ( dctvy, nI, nJ, DCT_F_ORDER, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'dimensioning Vy'
end if

call dct_set_field_val_location ( dctvy, DCT_LOC_CENTERED, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'var location Vy'
end if

call dct_set_field_labels( dctvy, DCT_RECTILINEAR, scx , nI, &
                            &  DCT_RECTILINEAR, scy, nJ, &
                            &  DCT_F_ORDER, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'labeling Vy'
end if

call dct_set_field_time ( dctvy, DCT_TIME_NO_UNIT, 0.0D+0, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'timing Vy'
end if

call dct_set_field_freq_consumption ( dctvy, dt, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'frequency Vy'
end if


call dct_set_field_values ( dctvy, DCT_REAL, cplvy, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'connecting Vy'
end if

!!!! ******************* Linking Vy to the model  ***************** 
call dct_set_model_var ( dmodel, dctvy, DCT_FIELD_TYPE, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'linking Vy'
end if


!!!! ********************** Setting the p variable  ******************** 
call dct_create_field ( dctp, "p",&
                         & "Dumm var p on model C",&
                         & DCT_NO_UNITS, DCT_PRODUCE, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, imesg
  stop 'creating p'
end if

call dct_set_field_dims ( dctp, pnI, pnJ, DCT_F_ORDER, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'dimensioning p'
end if

call dct_set_field_val_location ( dctp, DCT_LOC_CORNERS, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'var location p'
end if

call dct_set_field_labels( dctp, DCT_RECTILINEAR, mx(piniI:pendI) , pnI, &
                            &  DCT_RECTILINEAR, my(piniJ:pendJ), pnJ, &
                            &  DCT_F_ORDER, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'labeling p'
end if

call dct_set_field_time ( dctp, DCT_TIME_NO_UNIT, 0.0D+0, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'timing p'
end if

call dct_set_field_freq_production ( dctp, dt, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'frequency p'
end if


call dct_set_field_values ( dctp, DCT_REAL, cplp, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'connecting p'
end if

!!!! ******************* Linking p to the model  ***************** 
call dct_set_model_var ( dmodel, dctp, DCT_FIELD_TYPE, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'linking p'
end if


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! ********************** The Couple is registered  ********************
call dct_create_couple ( dcouple, "SubDomCommTest3",&
                         & "Test of concurrent parallel and concurrent models",&
                         & dmodel, "test_subdom_modb", err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'creating coupling'
end if

!!!! ******************* Linking Vx to the couple  ***************** 
call dct_set_coupling_vars ( dcouple, dctvx, "Wx",&
                             & DCT_FIELD_TYPE, DCT_LINEAR_INTERPOLATION, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'coupling Vx'
end if

!!!! ******************* Linking Vy to the couple  ***************** 
call dct_set_coupling_vars ( dcouple, dctvy, "Wy",&
                             & DCT_FIELD_TYPE, DCT_LINEAR_INTERPOLATION, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'coupling Vy'
end if

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! ********************** The Couple is registered  ********************
call dct_create_couple ( dcouple2, "SubDomCommTest2",&
                         & "Test of concurrent parallel and concurrent models",&
                         & dmodel, "test_subdom_moda", err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'creating coupling2'
end if

!!!! ******************* Linking p to the couple  ***************** 
call dct_set_coupling_vars ( dcouple2, dctp, "pr",&
                             & DCT_FIELD_TYPE, DCT_LINEAR_INTERPOLATION, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'coupling p'
end if
print *, "Antes de final registro 3"
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!  ************************ End Registration Call ********************
call dct_endregistration ( err, imesg )
if (err /= DCT_SUCCESS) then
  print *, imesg
  stop 'calling Endregistration'
end if
!***********************************************************************
!                         End Declaring the Coupling structures
!***********************************************************************


! Mesh points to evaluate functions
allocate( grx( pnI, pnJ), gry( pnI, pnJ) )
do j = 1,pnJ
   grx(:,j) = mx(piniI:pendI)
enddo
do i = 1,pnI
   gry(i,:) = my(piniJ:pendJ)
enddo


!!!!!! Test using linear function...
!!!! Function constant
!vx = time + 1.0D0
!vx = rank
!vy = time - 1.0D-2*time + 0.5D0
!vy = rank
!coef = 0.6D-2
!p = - 1.5D0
!p = rank

!!!! Function linear
vx = -1.0D4   !grx + gry
vy = -1.0D4   !grx - 1.0D-2*gry  + time
coef = 0.6D-2
p = gry + coef*(grx + gry)


!!!! Whatever
!vx = grx*grx + gry*gry + time 
!vy = time*grx*grx - 1.0D-2*time*gry*gry
!coef = 0.6D-2
!t = (grx + gry)*(grx + gry) + coef*(grx*grx + gry*gry)


!!!!! Set the initial time to current time
call dct_update_model_time( dmodel, err, imesg )

cplvx = vx
print *,"Receiving vx in time: ", Time
call dct_recv_field ( dctvx, err, imesg)
if (err /= DCT_SUCCESS) then
   print *, "[", err, "]  ", imesg
   stop 'receiving vx'
end if
vx = cplvx

cplvy = vy
print *,"Receiving vy in time: ", Time
call dct_recv_field ( dctvy, err, imesg)
if (err /= DCT_SUCCESS) then
   print *, "[", err, "]  ", imesg
   stop 'receiving vy'
end if
vy = cplvy

cplp = p
print *,"Sending p in time: ", Time
call dct_send_field ( dctp, err, imesg)
if (err /= DCT_SUCCESS) then
   print *, "[", err, "]  ", imesg
   stop 'sending p'
end if

! The received varialbles are output into a file
!call WriteFileRealData( nI, nJ, scx, scy, cplvx, 'cx', 'cy', 'cplvx', &
!        & './SCRATCH/var_vx_'//trim(FileProc)//'.m' )
!call WriteFileRealData( nI, nJ, scx, scy, cplvy, 'cx', 'cy', 'cplvy', &
!        & './SCRATCH/var_vy_'//trim(FileProc)//'.m' )
!call WriteFileDoubleData( pnI, pnJ, mx(piniI:pendI), my(piniJ:pendJ), grx, &
!        & 'cx', 'cy', 'grx', './SCRATCH/var_grx_'//trim(FileProc)//'.m' )
!call WriteFileDoubleData( pnI, pnJ, mx(piniI:pendI), my(piniJ:pendJ), gry, &
!        & 'cx', 'cy', 'gry', './SCRATCH/var_gry_'//trim(FileProc)//'.m' )

! Entering the calculation loop
StepN=1
do while (StepN <= iterM)
   time = StepN*dt
   print *,"Model A. Step: ", StepN,". Time : ", time

   !!!!!! Test using linear function...
   !!!! Function constant
   !vx = time + 1.0D0
   !vy = time - 1.0D-2*time + 0.5D0
   coef = 0.6D-2
   p = time + p!*coef - 1.5D0
   
   !!!! Function linear
   !vx = (time + 1.0D0)*grx + (1.0D0 - time)*gry
   !vy = grx - 1.0D-2*gry  + time
   !coef = 0.6D-2
   !t = time*(grx - gry) + coef*(grx + gry)
   
   !!!! Whatever
   !vx = grx*grx + gry*gry + time 
   !vy = time*grx*grx - 1.0D-2*time*gry*gry
   !coef = 0.6D-2
   !t = (grx + gry)*(grx + gry) + coef*(grx*grx + gry*gry)
   
   
   !!!!! Set the initial time to current time
   call dct_update_model_time( dmodel, err, imesg )

   print *,"Receiving vy in time: ", Time
   call dct_recv_field ( dctvx, err, imesg)
   if (err /= DCT_SUCCESS) then
      print *, "[", err, "]  ", imesg
      stop 'receiving vx'
   end if
   vx = cplvx

   print *,"Receiving vy in time: ", Time
   call dct_recv_field ( dctvy, err, imesg)
   if (err /= DCT_SUCCESS) then
      print *, "[", err, "]  ", imesg
      stop 'receiving vy'
   end if
   vy = cplvy

   cplp = p
   print *,"Sending p in time: ", Time
   call dct_send_field ( dctp, err, imesg)
   if (err /= DCT_SUCCESS) then
      print *, "[", err, "]  ", imesg
      stop 'sending p'
   end if
   
   StepN=StepN+1

enddo

!******************************************************
!   Destroy the Coupling structures
!******************************************************
call dct_destroy_couple ( dcouple, err, imesg )
if (err /= DCT_SUCCESS) then
   print *, imesg
   stop 'destroying coupling'
end if
 
call dct_destroy_couple ( dcouple2, err, imesg )
if (err /= DCT_SUCCESS) then
   print *, imesg
   stop 'destroying coupling2'
end if
 
call dct_destroy_field ( dctvx, err, imesg )
if (err /= DCT_SUCCESS) then
   print *, imesg
   stop 'destroying Vx'
end if
 
call dct_destroy_field ( dctvy, err, imesg )
if (err /= DCT_SUCCESS) then
   print *, imesg
   stop 'destroying Vy'
end if
 
call dct_destroy_field ( dctp, err, imesg )
if (err /= DCT_SUCCESS) then
   print *, imesg
   stop 'destroying p'
end if
 
call dct_destroy_model ( dmodel, err, imesg )
if (err /= DCT_SUCCESS) then
   print *, imesg
   stop 'destroying model'
end if
 

call dct_finalized ( err, imesg )
if (err /= DCT_SUCCESS) then
   print *, imesg
   stop 'finalizing dct'
end if

!finalize system
deallocate( mx, my, scx, scy, vx, vy, p, cplvx, cplvy, cplp )

!CONTAINS

END SUBROUTINE test_subdom_modc


