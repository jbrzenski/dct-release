SUBROUTINE test_subdom_modb  ( model_comm, world_comm )

!===============================================================
!  FILE: test_subdom_modb.f90
!===============================================================
! Description: This file a dummy model b to test subdomain
!              mapping
!
!
! 20-Jul-2011: Created by: Dany De Cecchis
!---------------------------------------------------------------------
!$Id: test_subdom_modb.f90,v 1.7 2012/08/22 03:42:01 dcecchis Exp $
!
! DCT library module
USE DCT_FORTRAN
USE INTERP_TOOLS

IMPLICIT NONE

include "mpif.h"

! Global prpoblem size
INTEGER, PARAMETER    ::  Pdim = 2    ! Problem dimension
INTEGER, PARAMETER    ::  IMax = 11
INTEGER, PARAMETER    ::  JMax = 11

! Communication variables
INTEGER, INTENT(IN)                :: model_comm, world_comm

! Local Variables:
DOUBLE PRECISION                    :: time         ! Simulation Time
DOUBLE PRECISION                    :: dt           ! Time step
DOUBLE PRECISION                    :: coef
INTEGER                             :: i,j,k
INTEGER                             :: StepN, iterM
INTEGER                             :: nI, nJ, iniI, endI, iniJ, endJ

! DCT structures descriptors variabels
! Testing if one DCT vars can feed more than one var
! Then we have here two consumer variables of the same producer
TYPE (DCT_FIELD)                    :: dctwx, dctwy, dcttu
TYPE (DCT_MODEL)                    :: dmodel
TYPE (DCT_COUPLE)                   :: dcouple, dcouple2

integer, dimension(2)               :: npts= (/ IMax, JMax /) ! Global mesh point
!integer, dimension(2)               :: vnpts= (/ IMax-1, JMax-1/) ! Global var point

DOUBLE PRECISION, ALLOCATABLE, DIMENSION (:,:) :: cplwx, cplwy, cpltu  ! Coupling Variables
DOUBLE PRECISION, ALLOCATABLE, DIMENSION (:) :: mx, my ! Coordinates of the
                                                           ! mesh points

DOUBLE PRECISION, ALLOCATABLE, DIMENSION ( :, :) :: grx, gry ! grid coordinates

DOUBLE PRECISION, ALLOCATABLE, DIMENSION (:) :: scx, scy ! Coordinates of the
                                                             ! variables points
! Model Variables
DOUBLE PRECISION, ALLOCATABLE, DIMENSION ( :,: ) :: wx, wy, tu


integer                             :: err
CHARACTER(LEN=150)                  :: imesg
CHARACTER(LEN=3)                    :: FileProc


! MPI Variables
INTEGER, PARAMETER                  :: MNPS = 4  ! Model number of processes
INTEGER                             :: rank, nprocs
INTEGER, DIMENSION(MNPS)            :: lstranks
INTEGER, DIMENSION(Pdim,MNPS)       :: sdinipos ! Subdomain initial position
INTEGER, DIMENSION(Pdim,MNPS)       :: sdendpos ! Subdomain ending position

call MPI_COMM_SIZE(world_comm, nprocs,err)
call MPI_COMM_RANK(world_comm, rank,err)

!print *, 'I am process ', rank, ' in model B'

allocate( mx(Imax), my(Jmax) )

mx(1) = 0.0D+0
do i=2, Imax
   mx(i) = mx(1) + DBLE(i-1)*15.0D+0
enddo
my(1) = -40.0D+0
do j=2, Jmax
   my(j) = my(1) + DBLE(j-1)*10.0D+0
enddo

!! Setting the limits for each processor
select case (rank)
   case (2)
      nI = 5
      nJ = 6
      iniI = 6
      endI = 11
      iniJ = 5
      endJ = 11
   case (3)
      nI = 5
      nJ = 6
      iniI = 1
      endI = 6
      iniJ = 5
      endJ = 11
   case (4)
      nI = 5
      nJ = 4
      iniI = 6
      endI = 11
      iniJ = 1
      endJ = 5
   case (5)
      nI = 5
      nJ = 4
      iniI = 1
      endI = 6
      iniJ = 1
      endJ = 5
end select 
if ( rank < 10 ) then
   write(FileProc,'(I1)') rank
elseif ( rank < 100 ) then
   write(FileProc,'(I2)') rank
else
   write(FileProc,'(I3)') rank
endif
allocate( scx(nI), scy(nJ) )
allocate( wx(nI,nJ), wy(nI,nJ), tu(nI,nJ) )

!! Setting the subdomain
! Rank (2)
lstranks(1) = 2
sdinipos(1,1) = 6
sdendpos(1,1) = 11
sdinipos(2,1) = 5
sdendpos(2,1) = 11

! Rank (3)
lstranks(2) = 3
sdinipos(1,2) = 1
sdendpos(1,2) = 6
sdinipos(2,2) = 5
sdendpos(2,2) = 11

! Rank (4)
lstranks(3) = 4
sdinipos(1,3) = 6
sdendpos(1,3) = 11
sdinipos(2,3) = 1
sdendpos(2,3) = 5

! Rank (5)
lstranks(4) = 5
sdinipos(1,4) = 1
sdendpos(1,4) = 6
sdinipos(2,4) = 1
sdendpos(2,4) = 5


! Variables ticks marks
!!! Scalar variables coordinates. The scalar variables are
!!! in the center point of the cell 
scx = ( mx( (iniI+1):endI ) + mx( iniI:(endI-1) ) )/2
scy = ( my( (iniJ+1):endJ ) + my( iniJ:(endJ-1) ) )/2


! Mesh points to evaluate functions
allocate( grx(nI,nJ), gry(nI,nJ) )
do j = 1,nJ
   grx(:,j) = scx
enddo
do i = 1,nI
   gry(i,:) = scy
enddo

! Initializing the model time step
time=0.0D0
dt = 1.0D-3
iterM = 5

!***********************************************************************
!                                 Declaring the Coupling structures
!***********************************************************************
!!!!!!!! Variables allocation needed int he coupling process 
! The variables to connect with the coupling
! Coupling variables, where the user should dump or
! recover the values for coupling
allocate( cplwx(nI,nJ), cplwy(nI,nJ), cpltu(nI,nJ) ) 

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!  *********************** Begin Registration Call *******************
call dct_beginregistration ( world_comm, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'calling Beginregistration'
end if
print *, "Despues de registro 2"
!!! ********************** The Model is registered  ******************** 
call dct_create_model (dmodel, "test_subdom_modb",&
                      & "Dummy model B to test comm and subdomain mapping",&
                      & MNPS, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'registering model'
end if

call dct_set_model_time (dmodel, Time, dt, DCT_TIME_NO_UNIT, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'model time'
end if

call dct_set_model_dom (dmodel, Pdim, 1, DCT_RECTILINEAR, mx, IMax,&
                       & DCT_F_ORDER, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'model domain dir 1'
end if

call dct_set_model_dom (dmodel, Pdim, 2, DCT_RECTILINEAR, my, JMax,&
                       & DCT_F_ORDER, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'model domain dir 2'
end if
print *, "Antes parlaout 2"
! call dct_set_model_parlayout (dmodel, DCT_DIST_RECTANGULAR, (/2 , 2/),&
!                             & lstranks, DCT_F_ORDER, err, imesg)
call dct_set_model_parlayout (dmodel, DCT_DIST_RECTANGULAR, (/2 , 2/),&
                            & DCT_F_ORDER, err, imesg)

if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'model parlayout'
end if
print *, "Despues de parlaout 2"
call dct_set_model_subdom (dmodel, lstranks, sdinipos, sdendpos,&
                          & DCT_F_ORDER, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'model parlayout'
end if
print *, "Despues de modelo 2"

!!! ********************** Setting the Wx variable  ******************** 
call dct_create_field ( dctwx, "Wx",&
                         & "Dumm var Wx on model B",&
                         & DCT_NO_UNITS, DCT_PRODUCE, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, imesg
  stop 'creating Wx'
end if

call dct_set_field_dims ( dctwx, nI, nJ, DCT_F_ORDER, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'dimensioning Wx'
end if

call dct_set_field_val_location ( dctwx, DCT_LOC_CENTERED, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'var location Wx'
end if

call dct_set_field_labels( dctwx, DCT_RECTILINEAR, scx , nI, &
                            &  DCT_RECTILINEAR, scy, nJ, &
                            &  DCT_F_ORDER, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'labeling Wx'
end if

call dct_set_field_time ( dctwx, DCT_TIME_NO_UNIT, 0.0D+0, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'timing Wx'
end if

call dct_set_field_freq_production ( dctwx, dt, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'frequency Wx'
end if


call dct_set_field_values ( dctwx, DCT_DOUBLE_PRECISION, cplwx, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'connecting Wx'
end if

!!!! ******************* Linking Wx to the model  ***************** 
call dct_set_model_var ( dmodel, dctwx, DCT_FIELD_TYPE, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'linking Wx'
end if
print *, "Despues de Wx 2"
!!! ********************** Setting the Wy variable  ******************** 
call dct_create_field ( dctwy, "Wy",&
                         & "Dumm var Wy on model B",&
                         & DCT_NO_UNITS, DCT_PRODUCE, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, imesg
  stop 'creating Wy'
end if

call dct_set_field_dims ( dctwy, nI, nJ, DCT_F_ORDER, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'dimensioning Wy'
end if

call dct_set_field_val_location ( dctwy, DCT_LOC_CENTERED, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'var location Wy'
end if

call dct_set_field_labels( dctwy, DCT_RECTILINEAR, scx , nI, &
                            &  DCT_RECTILINEAR, scy, nJ, &
                            &  DCT_F_ORDER, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'labeling Wy'
end if

call dct_set_field_time ( dctwy, DCT_TIME_NO_UNIT, 0.0D+0, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'timing Wy'
end if

call dct_set_field_freq_production ( dctwy, dt, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'frequency Wy'
end if


call dct_set_field_values ( dctwy, DCT_DOUBLE_PRECISION, cplwy, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'connecting Wy'
end if

!!!! ******************* Linking Wy to the model  ***************** 
call dct_set_model_var ( dmodel, dctwy, DCT_FIELD_TYPE, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'linking Wy'
end if

!!! ********************** Setting the tu variable  ******************** 
call dct_create_field ( dcttu, "tu",&
                         & "Dumm var tu on model B",&
                         & DCT_NO_UNITS, DCT_CONSUME, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, imesg
  stop 'creating tu'
end if

call dct_set_field_dims ( dcttu, nI, nJ, DCT_F_ORDER, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'dimensioning tu'
end if

call dct_set_field_val_location ( dcttu, DCT_LOC_CENTERED, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'var location tu'
end if

call dct_set_field_labels( dcttu, DCT_RECTILINEAR, scx , nI, &
                            &  DCT_RECTILINEAR, scy, nJ, &
                            &  DCT_F_ORDER, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'labeling tu'
end if

call dct_set_field_time ( dcttu, DCT_TIME_NO_UNIT, 0.0D+0, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'timing tu'
end if

call dct_set_field_freq_consumption ( dcttu, dt, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'frequency tu'
end if


call dct_set_field_values ( dcttu, DCT_DOUBLE_PRECISION, cpltu, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'connecting tu'
end if

!!!! ******************* Linking tu to the model  ***************** 
call dct_set_model_var ( dmodel, dcttu, DCT_FIELD_TYPE, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'linking tu'
end if

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! ********************** The Couple is registered  ********************
call dct_create_couple ( dcouple, "SubDomCommTest",&
                        & "Test of concurrent parallel and concurrent models",&
                        & dmodel, "test_subdom_moda", err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'creating coupling'
end if

!!!! ******************* Linking Wx to the couple  ***************** 
call dct_set_coupling_vars ( dcouple, dctwx, "Vx",&
                             & DCT_FIELD_TYPE, DCT_LINEAR_INTERPOLATION, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'coupling Vx'
end if

!!!! ******************* Linking Wy to the couple  ***************** 
call dct_set_coupling_vars ( dcouple, dctwy, "Vy",&
                             & DCT_FIELD_TYPE, DCT_LINEAR_INTERPOLATION, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'coupling Vy'
end if

!!!! ******************* Linking tu to the couple  ***************** 
call dct_set_coupling_vars ( dcouple, dcttu, "t",&
                             & DCT_FIELD_TYPE, DCT_LINEAR_INTERPOLATION, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'coupling t'
end if

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! ********************** The Couple2 is registered  ********************
call dct_create_couple ( dcouple2, "SubDomCommTest3",&
                        & "Test of concurrent parallel and concurrent models",&
                        & dmodel, "test_subdom_modc", err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'creating coupling2'
end if

!!!! ******************* Linking Wx to the couple  ***************** 
call dct_set_coupling_vars ( dcouple2, dctwx, "Vx",&
                             & DCT_FIELD_TYPE, DCT_LINEAR_INTERPOLATION, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'coupling Vx'
end if

!!!! ******************* Linking Wy to the couple  ***************** 
call dct_set_coupling_vars ( dcouple2, dctwy, "Vy",&
                             & DCT_FIELD_TYPE, DCT_LINEAR_INTERPOLATION, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'coupling Vy'
end if
print *, "Antes de final registro 2"
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!  ************************ End Registration Call ********************
call dct_endregistration ( err, imesg )
if (err /= DCT_SUCCESS) then
  print *, imesg
  stop 'calling Endregistration'
end if
!***********************************************************************
!                         End Declaring the Coupling structures
!***********************************************************************

!!!!!! Test using linear function...
!!!! Function constant
!wx = time + 1.0D0
!wx = rank
!wy = time - 1.0D-2*time + 0.5D0
!wy = rank
!coef = 0.6D-2
!tu = rank ! time*coef - 1.5D0

!!!! Function linear
wx = grx + gry
wy = grx - 1.0D-2*gry  + time
!coef = 0.6D-2
tu = -1.0D4   ! gry + coef*(grx + gry)

!!!! Whatever
!wx = grx*grx + gry*gry + time 
!wy = time*grx*grx - 1.0D-2*time*gry*gry
!coef = 0.6D-2
!tu = (grx + gry)*(grx + gry) + coef*(grx*grx + gry*gry)


!!!!! Set the initial time to current time
call dct_update_model_time( dmodel, err, imesg )

cpltu = tu
print *,"Receiving tu (temperature) in time: ", Time
call dct_recv_field ( dcttu, err, imesg)
if (err /= DCT_SUCCESS) then
   print *, "[", err, "]  ", imesg
   stop 'receiving tu'
end if
tu = cpltu

cplwx = wx
print *,"Sending wx in time: ", Time
call dct_send_field ( dctwx, err, imesg)
if (err /= DCT_SUCCESS) then
   print *, "[", err, "]  ", imesg
   stop 'sending wx'
end if

cplwy = wy
print *,"Sending wy in time: ", Time
call dct_send_field ( dctwy, err, imesg)
if (err /= DCT_SUCCESS) then
   print *, "[", err, "]  ", imesg
   stop 'sending wy'
end if

! The received varialbles are output into a file
!call WriteFileDoubleData( nI, nJ, scx, scy, cpltu, 'bx', 'by', 'cpltu', &
!        & './SCRATCH/var_tu_'//trim(FileProc)//'.m' )
! The received varialbles are output into a file
!call WriteFileDoubleData( nI, nJ, scx, scy, cplwx, 'bx', 'by', 'cplwx', &
!        & './SCRATCH/var_wx_'//trim(FileProc)//'.m' )
! The received varialbles are output into a file
!call WriteFileDoubleData( nI, nJ, scx, scy, cplwy, 'bx', 'by', 'cplwy', &
!        & './SCRATCH/var_wy_'//trim(FileProc)//'.m' )

! Entering the calculation loop
StepN=1
do while ( StepN <= iterM )
   time = StepN*dt
   print *,"Model B. Step: ", StepN,". Time : ", time
   
   !!!!!! Test using linear function...
   !!!! Function constant
   wx = time + 1.0D0
   wy = time - 1.0D-2*time + 0.5D0
   coef = 0.6D-2
   tu = time*coef - 1.5D0
   
   !!!! Function linear
   !wx = (time + 1.0D0)*grx + (1.0D0 - time)*gry
   !wy = grx - 1.0D-2*gry  + time
   !coef = 0.6D-2
   !tu = time*(grx - gry) + coef*(grx + gry)
   
   !!!! Whatever
   !wx = grx*grx + gry*gry + time 
   !wy = time*grx*grx - 1.0D-2*time*gry*gry
   !coef = 0.6D-2
   !tu = (grx + gry)*(grx + gry) + coef*(grx*grx + gry*gry)
   
   
   !!!!! Set the initial time to current time
   call dct_update_model_time( dmodel, err, imesg )
   
   print *,"Receiving tu (temperature) in time: ", Time
   call dct_recv_field ( dcttu, err, imesg)
   if (err /= DCT_SUCCESS) then
      print *, "[", err, "]  ", imesg
      stop 'receiving tu'
   end if
   tu = cpltu

   cplwx = wx
   print *,"Sending wx in time: ", Time
   call dct_send_field ( dctwx, err, imesg)
   if (err /= DCT_SUCCESS) then
      print *, "[", err, "]  ", imesg
      stop 'sending wx'
   end if
   
   cplwy = wy
   print *,"Sending wy in time: ", Time
   call dct_send_field ( dctwy, err, imesg)
   if (err /= DCT_SUCCESS) then
      print *, "[", err, "]  ", imesg
      stop 'sending wy'
   end if
   
   StepN=StepN+1
   
enddo

!******************************************************
!   Destroy the Coupling structures
!******************************************************
call dct_destroy_couple ( dcouple, err, imesg )
if (err /= DCT_SUCCESS) then
   print *, imesg
   stop 'destroying coupling'
end if

call dct_destroy_couple ( dcouple2, err, imesg )
if (err /= DCT_SUCCESS) then
   print *, imesg
   stop 'destroying coupling2'
end if

call dct_destroy_field ( dctwx, err, imesg )
if (err /= DCT_SUCCESS) then
   print *, imesg
   stop 'destroying Wx'
end if

call dct_destroy_field ( dctwy, err, imesg )
if (err /= DCT_SUCCESS) then
   print *, imesg
   stop 'destroying Wy'
end if

call dct_destroy_field ( dcttu, err, imesg )
if (err /= DCT_SUCCESS) then
   print *, imesg
   stop 'destroying tu'
end if

call dct_destroy_model ( dmodel, err, imesg )
if (err /= DCT_SUCCESS) then
   print *, imesg
   stop 'destroying model'
end if

call dct_finalized ( err, imesg )
if (err /= DCT_SUCCESS) then
   print *, imesg
   stop 'finalizing dct'
end if
    


!finalize system
deallocate( mx, my, scx, scy, wx, wy, tu, cplwx, cplwy, cpltu )

! CONTAINS

  
END SUBROUTINE test_subdom_modb