/*******************************************************************/
/*                 Distributed Coupling Toolkit (DCT)              */
/*                                                                 */
/*!
    \file size_test.c                       
    \brief Silly program that prints out the size of DCT
    data estructures.

    This program just print out the size in bytes
    of all the data structures used in the DCT
    library                                         

    \author Dany De Cecchis: dcecchis@gmail.com
    \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.
*/
/*******************************************************************/

/* -----------------------------------------------  Include Files  */
#include <stdlib.h>
#include <stdio.h>
#include <dct.h>


/**************************************************************************
* subroutine print results in Matlab format
**************************************************************************/
int main(int argc, char *argv[])
{

   /* ---------------------------------------  Variables Declaration  */
   /* -------------------------------------------  Models Variables  */
//    DCT_Error  ierr;
//    DCT_Model  mod;
//    DCT_Couple cop;
//    DCT_Field  var;
//    DCT_3d_Var var3d;

   /* ------------------------------------------------ Program Begins */
   
   printf("DCT_Field Size in bytes: %d\n", (int)sizeof(DCT_Field) );
   printf("DCT_3d_Var Size in bytes: %d\n", (int)sizeof(DCT_3d_Var) );
   printf("DCT_Model Size in bytes: %d\n", (int)sizeof(DCT_Model) );
   printf("DCT_Couple Size in bytes: %d\n", (int)sizeof(DCT_Couple) );

   return 0;
}



