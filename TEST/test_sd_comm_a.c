#include <stdio.h>
#include <ctype.h>
#include <mpi.h>
#ifdef MEMTRK
  #include <TAU.h>
#endif
#include "dct.h"
#include "test_sd_comm_intp_tool.h"

#define     XRES    73
#define     YRES    37


int test_sd_comm_a( int rank, int numtasks, MPI_Comm dom_comm, MPI_Comm globalcomm )
{
/* ---------------------------------------  Variables Declaration  */
   int         ierr = 0;
   DCT_Error   dcterr = { DCT_SUCCESS, (DCT_String)NULL};
   
   DCT_Couple  couple;
   DCT_Model   model;
   DCT_Field   dcttemp;
   DCT_Field   dctwsx;
   DCT_Field   dctwsy;
   
   
   DCT_Scalar  xmarks[XRES];
   DCT_Scalar  ymarks[YRES];

   int         playdim[2] = { 1, 1 };
   DCT_Integer iniind[2] = { 0, 0 };
   DCT_Integer endind[2] = { (XRES-1), (YRES-1)};
   
   float       tini, dt, tend, tt;
   float     **data;
   float     **datax;
   float     **datay;
   float      *temp;
   float      *windx;
   float      *windy;
   int         ii, iniyr, endyr, nyr, nreads;
   int         maxdim, ntime;

/* --------------------------------------  BEGIN( test_sd_comm_a ) */

   printf("This is the process %d of %d\n", rank, numtasks);
   
   /********************************************************
    *********    Reading temeperature data files    ********
    ********************************************************/
   
   iniyr = 2010;
   endyr = 2010;
   maxdim = TNLATI*TNLONG; /* This is the grid resolution defined
                              in test_sd_comm_intp_tool.h         */
   nyr = endyr - iniyr + 1;
   nreads = 12*nyr;
   data = (float **) malloc( sizeof(float *)*(size_t)nreads );
   if ( data == (float **)NULL ) {
      fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
      fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
      return( 1 );
   }
   
   for ( ii=0; ii < nreads; ii++ ) {
      *(data+ii) = (float *) malloc( sizeof(float)*(size_t)maxdim );
      if ( *(data+ii) == (float *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
      }      
   }
   
   // Reading temperature */
   ierr = read_temperature( iniyr, endyr, data );
   if ( ierr ) {
      fflush( NULL );
      fprintf( stderr, "\nERROR[read temp]: Error in function read_temperature\n" );
      fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
      fflush( NULL);
      return ( ierr );
   }

   /********************************************************
    *********    Reading wind stress data files    *********
    ********************************************************/
   maxdim = WSNLATI*WSNLONG; /* This is the grid resolution defined
                                in test_sd_comm_intp_tool.h         */
   datax = (float **) malloc( sizeof(float *)*(size_t)12 );
   if ( datax == (float **)NULL ) {
      fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
      fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
      return( 1 );
   }
   datay = (float **) malloc( sizeof(float *)*(size_t)12 );
   if ( datay == (float **)NULL ) {
      fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
      fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
      return( 1 );
   }
   
   for ( ii=0; ii < 12; ii++ ) {
      *(datax+ii) = (float *) malloc( sizeof(float)*(size_t)maxdim );
      if ( *(datax+ii) == (float *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
      }      
      *(datay+ii) = (float *) malloc( sizeof(float)*(size_t)maxdim );
      if ( *(datay+ii) == (float *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
      }      
   }
   
   // Reading wind stress */
   ierr = read_wind_stress( datax, datay );
   if ( ierr ) {
      fflush( NULL );
      fprintf( stderr, "\nERROR[read wind stress]: Error in function read_wind_stress\n" );
      fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
      fflush( NULL);
      return ( ierr );
   }

   tini =  0.0;
   dt   =  1.0;
   tend = 11.0;
   
   /****************************************************************************
    ***********************   DCT_BeginRegistration Call   *********************
    ****************************************************************************/
   dcterr = DCT_BeginRegistration( globalcomm );
   DCTCHKERR( dcterr );

   /****************************************************************************
    **************************   Creating the DCT_Model  ***********************
    ****************************************************************************/
   dcterr = DCT_Create_Model( &model, "glb_model", "Test global model", 1 );
   DCTCHKERR( dcterr );

   dcterr = DCT_Set_Model_Time( &model, tini, dt, DCT_TIME_NO_UNIT );
   DCTCHKERR( dcterr );
   
   /* Generating the model ticks marks */
   xmarks[0] = -180.0;
   for ( ii = 1; ii < XRES; ii++ ) xmarks[ ii ] = xmarks[ ii-1 ]+ 5.0;
   ymarks[0] = -90.0;
   for ( ii = 1; ii < YRES; ii++ ) ymarks[ ii ] = ymarks[ ii-1 ]+ 5.0;
   
   dcterr = DCT_Set_Model_Dom( &model, 2, 1, DCT_RECTILINEAR, xmarks, XRES );
   DCTCHKERR( dcterr );
   dcterr = DCT_Set_Model_Dom( &model, 2, 2, DCT_RECTILINEAR, ymarks, YRES );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Model_ParLayout( &model, DCT_DIST_RECTANGULAR, playdim); /*, NULL );*/
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Model_SubDom( &model, &rank, iniind, endind );
   DCTCHKERR( dcterr );

   /****************************************************************************
    *************************   Creating the Temperature  **********************
    ****************************************************************************/
   temp = (float *)malloc(sizeof(float)*(size_t)((XRES-1)*(YRES-1)));
   if ( temp == (float *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }  
   /***** Control values ******/
   for ( ii = 0; ii < ((XRES-1)*(YRES-1)); ii++ ) *(temp + ii) = (float)ii;

   dcterr = DCT_Create_Field( &dcttemp, "glb_sst", "Temperature at Sea Surface",
                            CELSIUS, DCT_PRODUCE);
   DCTCHKERR( dcterr );

   dcterr = DCT_Set_Field_Dims( &dcttemp, (XRES-1), (YRES-1) );
   DCTCHKERR( dcterr );

   /* Generating the temperature ticks marks */
   xmarks[0] = -177.5;
   for ( ii=1; ii < (XRES-1); ii++ ) xmarks[ ii ] = xmarks[ ii-1 ]+ 5.0;
   ymarks[0] = -87.5;
   for ( ii=1; ii < (YRES-1); ii++ ) ymarks[ ii ] = ymarks[ ii-1 ]+ 5.0;
   dcterr = DCT_Set_Field_Labels( &dcttemp, DCT_RECTILINEAR, xmarks, (XRES-1),
                                  DCT_RECTILINEAR, ymarks, (YRES-1) );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Val_Location( &dcttemp, DCT_LOC_CENTERED );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Time( &dcttemp, DCT_TIME_NO_UNIT, tini );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Freq_Production( &dcttemp, dt );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Values( &dcttemp, DCT_FLOAT, temp );
   DCTCHKERR( dcterr );

   /****************************************************************************
    ***********************   Linking Temperature to Model *********************
    ****************************************************************************/
   dcterr =  DCT_Set_Model_Var( &model, &dcttemp, DCT_FIELD_TYPE );
   DCTCHKERR( dcterr );


   /****************************************************************************
    *************************   Creating the x-WindStress  *********************
    ****************************************************************************/
   windx = (float *)malloc(sizeof(float)*(size_t)((XRES-1)*(YRES)));
   if ( windx == (float *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }  
   dcterr = DCT_Create_Field( &dctwsx, "glb_windsx", "X-coordinate wind stress at Sea Surface",
                              DCT_NO_UNITS, DCT_PRODUCE);
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Dims( &dctwsx, (XRES-1), YRES );
   DCTCHKERR( dcterr );

   /* Generating the x-WindStress ticks marks */
   xmarks[0] = -177.5;
   for ( ii=1; ii < (XRES-1); ii++ ) xmarks[ ii ] = xmarks[ ii-1 ]+ 5.0;
   ymarks[0] = -90.0;
   for ( ii=1; ii < YRES; ii++ ) ymarks[ ii ] = ymarks[ ii-1 ]+ 5.0;
   dcterr = DCT_Set_Field_Labels( &dctwsx, DCT_RECTILINEAR, xmarks, (XRES-1),
                                  DCT_RECTILINEAR, ymarks, YRES );
   DCTCHKERR( dcterr );
   
//    dcterr = DCT_Set_Field_Val_Location( &dctwsx, DCT_LOC_CENTERED );
//    DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Time( &dctwsx, DCT_TIME_NO_UNIT, tini );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Freq_Production( &dctwsx, dt );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Values( &dctwsx, DCT_FLOAT, windx );
   DCTCHKERR( dcterr );

   /****************************************************************************
    ***********************   Linking x-WindStress to Model  *******************
    ****************************************************************************/
   dcterr =  DCT_Set_Model_Var( &model, &dctwsx, DCT_FIELD_TYPE );
   DCTCHKERR( dcterr );


   /****************************************************************************
    *************************   Creating the y-WindStress  *********************
    ****************************************************************************/
   windy = (float *)malloc(sizeof(float)*(size_t)(XRES*(YRES-1)));
   if ( windy == (float *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }  
   dcterr = DCT_Create_Field( &dctwsy, "glb_windsy", "Y-coordinate wind stress at Sea Surface",
                              DCT_NO_UNITS, DCT_PRODUCE);
   DCTCHKERR( dcterr );

   dcterr = DCT_Set_Field_Dims( &dctwsy, XRES, (YRES-1) );
   DCTCHKERR( dcterr );

   /* Generating the y-WindStress ticks marks */
   xmarks[0] = -180.0;
   for ( ii=1; ii < XRES; ii++ ) xmarks[ ii ] = xmarks[ ii-1 ]+ 5.0;
   ymarks[0] = -87.5;
   for ( ii=1; ii < (YRES-1); ii++ ) ymarks[ ii ] = ymarks[ ii-1 ]+ 5.0;
   dcterr = DCT_Set_Field_Labels( &dctwsy, DCT_RECTILINEAR, xmarks, XRES,
                                  DCT_RECTILINEAR, ymarks, (YRES-1) );
   DCTCHKERR( dcterr );
   
//    dcterr = DCT_Set_Field_Val_Location( &dctwsy, DCT_LOC_CENTERED );
//    DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Time( &dctwsy, DCT_TIME_NO_UNIT, tini );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Freq_Production( &dctwsy, dt );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Values( &dctwsy, DCT_FLOAT, windy );
   DCTCHKERR( dcterr );

   /****************************************************************************
    ***********************   Linking y-WindStress to Model  *******************
    ****************************************************************************/
   dcterr =  DCT_Set_Model_Var( &model, &dctwsy, DCT_FIELD_TYPE );
   DCTCHKERR( dcterr );


   /****************************************************************************
    ***************************   Creating the Couple  *************************
    ****************************************************************************/
   dcterr =  DCT_Create_Couple( &couple, "GlbReg_clp",
                        "Coupling beteween regional and global model", &model, "reg_model" );
   DCTCHKERR( dcterr );

   /****************************************************************************
    ********************   Linking Temperature to the Couple  ******************
    ****************************************************************************/
   dcterr =  DCT_Set_Coupling_Vars( &couple, &dcttemp, "reg_sst", DCT_FIELD_TYPE,
                                    DCT_LINEAR_INTERPOLATION );
   DCTCHKERR( dcterr );
   
   /****************************************************************************
    ********************  Linking x-WindStress to the Couple  ******************
    ****************************************************************************/
   dcterr =  DCT_Set_Coupling_Vars( &couple, &dctwsx, "reg_windsx", DCT_FIELD_TYPE,
                                    DCT_LINEAR_INTERPOLATION );
   DCTCHKERR( dcterr );
   
   /****************************************************************************
    ********************  Linking y-WindStress to the Couple  ******************
    ****************************************************************************/
   dcterr =  DCT_Set_Coupling_Vars( &couple, &dctwsy, "reg_windsy", DCT_FIELD_TYPE,
                                    DCT_LINEAR_INTERPOLATION );
   DCTCHKERR( dcterr );
   
   /****************************************************************************
    ************************   DCT_EndRegistration Call   **********************
    ****************************************************************************/
   dcterr = DCT_EndRegistration(  );
   DCTCHKERR( dcterr );
   
   /****************************************************************************
    ****************************************************************************
    ************************   Model Timesteping begins   **********************
    ****************************************************************************/
   tt = tini;
   ntime = 0;
   while ( tt <=  tend ) {
      printf("\t****** Model running time %g\n\t******\n", tt );
      dcterr =  DCT_Update_Model_Time( &model );
      DCTCHKERR( dcterr );
      
      // Feeding model temperature */
      ierr = feed_temperature( temp, data, ntime );
      if ( ierr ) {
         fflush( NULL );
         fprintf( stderr, "\nERROR[feeding temperature]: Error in function feed_temperature\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         fflush( NULL);
         return ( ierr );
      }
      // Feeding model x-wind stress */
      ierr = feed_xwinds( windx, datax, ntime );
      if ( ierr ) {
         fflush( NULL );
         fprintf( stderr, "\nERROR[feeding x-windstress]: Error in function feed_xwinds\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         fflush( NULL);
         return ( ierr );
      }
      // Feeding model y-wind stress */
      ierr = feed_ywinds( windy, datay, ntime );
      if ( ierr ) {
         fflush( NULL );
         fprintf( stderr, "\nERROR[feeding y-windstress]: Error in function feed_ywinds\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         fflush( NULL);
         return ( ierr );
      }
      
      printf("Sending temperature\n" );
      dcterr =  DCT_Send_Field( &dcttemp );
      DCTCHKERR( dcterr );
   
      printf("Sending x-wind stress\n" );
      dcterr =  DCT_Send_Field( &dctwsx );
      DCTCHKERR( dcterr );
   
      printf("Sending y-wind stress\n" );
      dcterr =  DCT_Send_Field( &dctwsy );
      DCTCHKERR( dcterr );
   
      tt += dt;
      ntime++;
   }
   
   /****************************************************************************
    *************************   Model Timesteping ends   ***********************
    ****************************************************************************
    ****************************************************************************/
   
   /****************************************************************************
    **********************   Destroying the DCT structures   *******************
    ****************************************************************************/
   dcterr = DCT_Destroy_Couple( &couple );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Destroy_Field( &dcttemp );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Destroy_Model( &model );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Finalized(  );
   DCTCHKERR( dcterr );
   
   for ( ii=0; ii < nreads; ii++ ) free ( data[ii] );
   free ( data );
   
   for ( ii=0; ii < 12; ii++ ) {
      free ( datax[ii] );
      free ( datay[ii] );   
   }
   free ( datax );
   free ( datay );
   free ( temp );
   free ( windx );
   free ( windy );
   
   return ( ierr );
/* ----------------------------------------  END( test_sd_comm_a ) */

}