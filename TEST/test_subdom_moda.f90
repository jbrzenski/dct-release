SUBROUTINE test_subdom_moda ( model_comm, world_comm )

!===============================================================
!  FILE: test_subdom_moda.f90
!===============================================================
! Description: This file a dummy model a to test subdomain
!              mapping
!
!
! 20-Jul-2011: Created by: Dany De Cecchis
!---------------------------------------------------------------------
!$Id: test_subdom_moda.f90,v 1.7 2012/08/22 03:42:01 dcecchis Exp $
!
! DCT library module
USE DCT_FORTRAN
USE INTERP_TOOLS

IMPLICIT NONE

include "mpif.h"

! Global prpoblem size
INTEGER, PARAMETER    ::  Pdim = 2    ! Problem dimension
INTEGER, PARAMETER    ::  IMax = 26
INTEGER, PARAMETER    ::  JMax = 26

! Communication variables
INTEGER, INTENT(IN)                :: model_comm, world_comm

! Local Variables:
DOUBLE PRECISION                    :: time         ! Simulation Time
DOUBLE PRECISION                    :: dt           ! Time step
INTEGER                             :: i,j,k
INTEGER                             :: StepN, iterM
DOUBLE PRECISION                    :: coef
INTEGER                             :: nI, nJ, iniI, endI, iniJ, endJ
INTEGER                             :: pnI, pnJ, piniI, pendI, piniJ, pendJ

! DCT structures descriptors variabels
TYPE (DCT_FIELD)                    :: dctvx, dctvy, dctt, dctpr
TYPE (DCT_MODEL)                    :: dmodel
TYPE (DCT_COUPLE)                   :: dcouple, dcouple2

integer, dimension(2)               :: npts= (/ IMax, JMax /) ! Global mesh point
!integer, dimension(2)               :: vnpts= (/ IMax-1, JMax-1/) ! Global var point

DOUBLE PRECISION, DIMENSION(2)      :: xo, xf, xx, yy
DOUBLE PRECISION, ALLOCATABLE, DIMENSION (:,:) :: cplvx, cplvy, cplt, cplpr  ! Coupling Variables
DOUBLE PRECISION, ALLOCATABLE, DIMENSION (:) :: mx, my ! Coordinates of the
                                                           ! mesh points

DOUBLE PRECISION, ALLOCATABLE, DIMENSION ( :, :) :: grx, gry ! grid coordinates

DOUBLE PRECISION, ALLOCATABLE, DIMENSION (:) :: scx, scy ! Coordinates of the
                                                             ! variables points
! Model Variables
DOUBLE PRECISION, ALLOCATABLE, DIMENSION (:,:) :: vx, vy, t, pr

integer                             :: err
CHARACTER(LEN=150)                  :: imesg
CHARACTER(LEN=3)                    :: FileProc

! MPI Variables
INTEGER, PARAMETER                  :: MNPS = 2  ! Model number of processes
INTEGER                             :: rank, nprocs
INTEGER, DIMENSION(MNPS)            :: lstranks
INTEGER, DIMENSION(Pdim,MNPS)       :: sdinipos ! Subdomain initial position
INTEGER, DIMENSION(Pdim,MNPS)       :: sdendpos ! Subdomain ending position

call MPI_COMM_SIZE(world_comm, nprocs,err)
call MPI_COMM_RANK(world_comm, rank,err)

!print *, 'I am process ', rank, ' in model A'

allocate( mx(Imax), my(Jmax) )

xo(1) = 0.0D+0
xo(2) = -35.0D+0
xf(1) = 125.0D+0
xf(2) = 90.0D+0
xx(1) = xo(1) + 2.5D+1
xx(2) = xf(1) - 2.5D+1
yy(1) = xo(2) + 2.5D+1
yy(2) = xf(2) - 2.5D+1
mx(1) = 0.0D+0
do i=2, Imax
   mx(i) = mx(1) + DBLE(i-1)*5.0D+0
enddo
my(1) = -35.0D+0
do j=2, Jmax
   my(j) = my(1) + DBLE(j-1)*5.0D+0
enddo

select case (rank)
!   case (11)
!      nI = 8
!      nJ = 4
!      iniI = 1
!      endI = 9
!      iniJ = 1
!      endJ = 5
!   case (10)
!      nI = 9
!      nJ = 4
!      iniI = 9
!      endI = 18
!      iniJ = 1
!      endJ = 5
!   case (9)
!      nI = 8
!      nJ = 4
!      iniI = 18
!      endI = 26
!      iniJ = 1
!      endJ = 5
!   case (8)
!      nI = 8
!      nJ = 7
!      iniI = 1
!      endI = 9
!      iniJ = 5
!      endJ = 12
!   case (7)
!      nI = 9
!      nJ = 7
!      iniI = 9
!      endI = 18
!      iniJ = 5
!      endJ = 12
!   case (6)
!      nI = 8
!      nJ = 7
!      iniI = 18
!      endI = 26
!      iniJ = 5
!      endJ = 12
!   case (5)
!      nI = 8
!      nJ = 7
!      iniI = 1
!      endI = 9
!      iniJ = 12
!      endJ = 19
!   case (4)
!      nI = 9
!      nJ = 7
!      iniI = 9
!      endI = 18
!      iniJ = 12
!      endJ = 19
!   case (3)
!      nI = 8
!      nJ = 7
!      iniI = 18
!      endI = 26
!      iniJ = 12
!      endJ = 19
!   case (2)
!      nI = 8
!      nJ = 7
!      iniI = 1
!      endI = 9
!      iniJ = 19
!      endJ = 26
   case (1)
      nI = 25
      nJ = 13
      iniI = 1
      endI = 26
      iniJ = 13
      endJ = 26
      pnI = 26
      pnJ = 14
      piniI = 1
      pendI = 26
      piniJ = 13
      pendJ = 26
   case (0)
      nI = 25
      nJ = 12
      iniI = 1
      endI = 26
      iniJ = 1
      endJ = 13
      pnI = 26
      pnJ = 12
      piniI = 1
      pendI = 26
      piniJ = 1
      pendJ = 12
end select
if ( rank < 10 ) then
   write(FileProc,'(I1)') rank
elseif ( rank < 100 ) then
   write(FileProc,'(I2)') rank
else
   write(FileProc,'(I3)') rank
endif

allocate( scx(nI), scy(nJ) )
allocate( vx(nI,nJ), vy(nI,nJ), t(nI,nJ), pr(pnI,pnJ) )

!! Setting the subdomain
! Rank (1)
lstranks(1) = 1
sdinipos(1,1) = 1
sdendpos(1,1) = 26
sdinipos(2,1) = 13
sdendpos(2,1) = 26

! Rank (0)
lstranks(2) = 0
sdinipos(1,2) = 1
sdendpos(1,2) = 26
sdinipos(2,2) = 1
sdendpos(2,2) = 13


! Variables ticks marks
!!! Scalar variables coordinates. The scalar variables are
!!! in the center point of the cell 
scx = ( mx( (iniI+1):endI ) + mx( iniI:(endI-1) ) )/2
scy = ( my( (iniJ+1):endJ ) + my( iniJ:(endJ-1) ) )/2


! Initializing the model
time=0.0D0
dt = 1.0D-3
iterM = 5

!***********************************************************************
!                                 Declaring the Coupling structures
!***********************************************************************
!!!!!!!! Variables allocation needed int he coupling process 
! The variables to connect with the coupling
! Coupling variables, where the user should dump or
! recover the values for coupling
allocate( cplvx(nI,nJ), cplvy(nI,nJ), cplt(nI,nJ), cplpr(pnI,pnJ) ) 

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!  *********************** Begin Registration Call *******************
call dct_beginregistration ( world_comm, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'calling Beginregistration'
end if
print *, "Despues de registro 1"
!!! ********************** The Model is registered  ******************** 
call dct_create_model (dmodel, "test_subdom_moda",&
                      & "Dummy model A to test comm and subdomain mapping",&
                      & MNPS, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'registering model'
end if

call dct_set_model_time (dmodel, Time, dt, DCT_TIME_NO_UNIT, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'model time'
end if

call dct_set_model_dom (dmodel, Pdim, 1, DCT_RECTILINEAR, mx, IMax,&
                       & DCT_F_ORDER, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'model domain dir 1'
end if

call dct_set_model_dom (dmodel, Pdim, 2, DCT_RECTILINEAR, my, JMax,&
                       & DCT_F_ORDER, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'model domain dir 2'
end if

!call dct_set_model_rspaced_dom ( dmodel, Pdim, xo, xf, (/ IMax, JMax/), DCT_F_ORDER,&
!                               & err, imesg )
!if (err /= DCT_SUCCESS) then
!  print *, "[", err, "]  ", imesg
!  stop 'model domain'
!end if
print *, "Antes parlaout 1"
! call dct_set_model_parlayout (dmodel, DCT_DIST_RECTANGULAR, (/1 , 2/),&
!                             & lstranks, DCT_F_ORDER, err, imesg)
call dct_set_model_parlayout (dmodel, DCT_DIST_RECTANGULAR, (/1 , 2/),&
                            & DCT_F_ORDER, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'model parlayout'
end if
print *, "Despues de parlaout 1"
call dct_set_model_subdom (dmodel, lstranks, sdinipos, sdendpos,&
                          & DCT_F_ORDER, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'model parlayout'
end if
print *, "Despues de modelo 1"

!!!! ********************** Setting the Vx variable  ******************** 
call dct_create_field ( dctvx, "Vx",&
                         & "Dumm var Vx on model A",&
                         & DCT_NO_UNITS, DCT_CONSUME, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, imesg
  stop 'creating Vx'
end if

call dct_set_field_dims ( dctvx, nI, nJ, DCT_F_ORDER, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'dimensioning Vx'
end if

call dct_set_field_val_location ( dctvx, DCT_LOC_CENTERED, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'var location Vx'
end if

call dct_set_field_labels( dctvx, DCT_RECTILINEAR, scx , nI, &
                            &  DCT_RECTILINEAR, scy, nJ, &
                            &  DCT_F_ORDER, err, imesg )
!call dct_set_field_labels( dctvx, DCT_CARTESIAN, xx , nI, &
!                            &  DCT_CARTESIAN, yy, nJ, &
!                            &  DCT_F_ORDER, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'labeling Vx'
end if

call dct_set_field_time ( dctvx, DCT_TIME_NO_UNIT, 0.0D+0, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'timing Vx'
end if

call dct_set_field_freq_consumption ( dctvx, dt, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'frequency Vx'
end if


call dct_set_field_values ( dctvx, DCT_DOUBLE_PRECISION, cplvx, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'connecting Vx'
end if

!!!! ******************* Linking Vx to the model  ***************** 
call dct_set_model_var ( dmodel, dctvx, DCT_FIELD_TYPE, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'linking Vx'
end if

print *, "Despues de Vx 1"
!!!! ********************** Setting the Vy variable  ******************** 
call dct_create_field ( dctvy, "Vy",&
                         & "Dumm var Vy on model A",&
                         & DCT_NO_UNITS, DCT_CONSUME, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, imesg
  stop 'creating Vy'
end if

call dct_set_field_dims ( dctvy, nI, nJ, DCT_F_ORDER, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'dimensioning Vy'
end if

call dct_set_field_val_location ( dctvy, DCT_LOC_CENTERED, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'var location Vy'
end if

call dct_set_field_labels( dctvy, DCT_RECTILINEAR, scx , nI, &
                            &  DCT_RECTILINEAR, scy, nJ, &
                            &  DCT_F_ORDER, err, imesg )
!call dct_set_field_labels( dctvy, DCT_CARTESIAN, xx , nI, &
!                            &  DCT_CARTESIAN, yy, nJ, &
!                            &  DCT_F_ORDER, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'labeling Vy'
end if

call dct_set_field_time ( dctvy, DCT_TIME_NO_UNIT, 0.0D0, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'timing Vy'
end if

call dct_set_field_freq_consumption ( dctvy, dt, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'frequency Vy'
end if


call dct_set_field_values ( dctvy, DCT_DOUBLE_PRECISION, cplvy, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'connecting Vy'
end if

!!!! ******************* Linking Vy to the model  ***************** 
call dct_set_model_var ( dmodel, dctvy, DCT_FIELD_TYPE, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'linking Vy'
end if


!!!! ********************** Setting the t variable  ******************** 
call dct_create_field ( dctt, "t",&
                         & "Dumm var t on model A",&
                         & DCT_NO_UNITS, DCT_PRODUCE, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, imesg
  stop 'creating t'
end if

call dct_set_field_dims ( dctt, nI, nJ, DCT_F_ORDER, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'dimensioning t'
end if

call dct_set_field_val_location ( dctt, DCT_LOC_CENTERED, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'var location t'
end if

call dct_set_field_labels( dctt, DCT_RECTILINEAR, scx , nI, &
                            &  DCT_RECTILINEAR, scy, nJ, &
                            &  DCT_F_ORDER, err, imesg )
!call dct_set_field_labels( dctt, DCT_CARTESIAN, xx , nI, &
!                            &  DCT_CARTESIAN, yy, nJ, &
!                            &  DCT_F_ORDER, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'labeling t'
end if

call dct_set_field_time ( dctt, DCT_TIME_NO_UNIT, 0.0D+0, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'timing t'
end if

call dct_set_field_freq_production ( dctt, dt, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'frequency t'
end if


call dct_set_field_values ( dctt, DCT_DOUBLE_PRECISION, cplt, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'connecting t'
end if

!!!! ******************* Linking t to the model  ***************** 
call dct_set_model_var ( dmodel, dctt, DCT_FIELD_TYPE, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'linking t'
end if


!!!! ********************** Setting the pr variable  ******************** 
call dct_create_field ( dctpr, "pr",&
                         & "Dumm var pr on model A",&
                         & DCT_NO_UNITS, DCT_CONSUME, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, imesg
  stop 'creating pr'
end if

call dct_set_field_dims ( dctpr, pnI, pnJ, DCT_F_ORDER, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'dimensioning pr'
end if

call dct_set_field_val_location ( dctpr, DCT_LOC_CORNERS, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'var location pr'
end if

call dct_set_field_labels( dctpr, DCT_RECTILINEAR, mx(piniI:pendI) , pnI, &
                            &  DCT_RECTILINEAR, my(piniJ:pendJ), pnJ, &
                            &  DCT_F_ORDER, err, imesg )
!call dct_set_field_labels( dctpr, DCT_CARTESIAN, (/ xo(1), xf(1)/) , pnI, &
!                            &  DCT_CARTESIAN, (/ xo(2), xf(2)/), pnJ, &
!                            &  DCT_F_ORDER, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'labeling pr'
end if

call dct_set_field_time ( dctpr, DCT_TIME_NO_UNIT, 0.0D+0, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'timing pr'
end if

call dct_set_field_freq_consumption ( dctpr, dt, err, imesg )
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'frequency pr'
end if


call dct_set_field_values ( dctpr, DCT_DOUBLE_PRECISION, cplpr, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'connecting pr'
end if

!!!! ******************* Linking pr to the model  ***************** 
call dct_set_model_var ( dmodel, dctpr, DCT_FIELD_TYPE, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'linking pr'
end if


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! ********************** The Couple is registered  ********************
call dct_create_couple ( dcouple, "SubDomCommTest",&
                         & "Test of concurrent parallel and concurrent models",&
                         & dmodel, "test_subdom_modb", err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'creating coupling'
end if

!!!! ******************* Linking Vx to the couple  ***************** 
call dct_set_coupling_vars ( dcouple, dctvx, "Wx",&
                             & DCT_FIELD_TYPE, DCT_LINEAR_INTERPOLATION, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'coupling Vx'
end if

!!!! ******************* Linking Vy to the couple  ***************** 
call dct_set_coupling_vars ( dcouple, dctvy, "Wy",&
                             & DCT_FIELD_TYPE, DCT_LINEAR_INTERPOLATION, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'coupling Vy'
end if

!!!! ******************* Linking t to the couple  ***************** 
call dct_set_coupling_vars ( dcouple, dctt, "tu",&
                             & DCT_FIELD_TYPE, DCT_LINEAR_INTERPOLATION, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'coupling t'
end if

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! ********************** The Couple2 is registered  ********************
call dct_create_couple ( dcouple2, "SubDomCommTest2",&
                         & "Test of concurrent parallel and concurrent models",&
                         & dmodel, "test_subdom_modc", err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'creating coupling2'
end if

!!!! ******************* Linking pr to the couple  ***************** 
call dct_set_coupling_vars ( dcouple2, dctpr, "p",&
                             & DCT_FIELD_TYPE, DCT_LINEAR_INTERPOLATION, err, imesg)
if (err /= DCT_SUCCESS) then
  print *, "[", err, "]  ", imesg
  stop 'coupling pr'
end if
print *, "Antes de final registro 1"

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!  ************************ End Registration Call ********************
call dct_endregistration ( err, imesg )
if (err /= DCT_SUCCESS) then
  print *, imesg
  stop 'calling Endregistration'
end if
!***********************************************************************
!                         End Declaring the Coupling structures
!***********************************************************************
print *, "Despues de regsitro 1"


! Mesh points to evaluate functions
allocate( grx(nI,nJ), gry(nI,nJ) )
do j = 1,nJ
   grx(:,j) = scx
enddo
do i = 1,nI
   gry(i,:) = scy
enddo


!!!!!! Test using linear function...
!!!! Function constant
!vx = rank
!vy = rank
!coef = 0.6D-2
!t = 6.5D-2
!pr = rank

!!!! Function linear
vx =  -1.0D4   !grx + gry
vy =  -1.0D4   !grx - 1.0D-2*gry  + time
coef = 0.6D-2
t =  gry + coef*(grx + gry)
pr = -1.0D4   ! gry + coef*(grx + gry)

!!!! Whatever
!vx = grx*grx + gry*gry + time 
!vy = time*grx*grx - 1.0D-2*time*gry*gry
!coef = 0.6D-2
!t = (grx + gry)*(grx + gry) + coef*(grx*grx + gry*gry)


!!!!! Set the initial time to current time
call dct_update_model_time( dmodel, err, imesg )

cplt = t
print *,"Sending t (temperature) in time: ", Time
call dct_send_field ( dctt, err, imesg)
if (err /= DCT_SUCCESS) then
   print *, "[", err, "]  ", imesg
   stop 'sending t'
end if

cplvx = vx
print *,"Receiving vx in time: ", Time
call dct_recv_field ( dctvx, err, imesg)
if (err /= DCT_SUCCESS) then
   print *, "[", err, "]  ", imesg
   stop 'receiving vx'
end if
vx = cplvx

cplvy = vy
print *,"Receiving vy in time: ", Time
call dct_recv_field ( dctvy, err, imesg)
if (err /= DCT_SUCCESS) then
   print *, "[", err, "]  ", imesg
   stop 'receiving vy'
end if
vy = cplvy

cplpr = pr
print *,"Receiving pr in time: ", Time
call dct_recv_field ( dctpr, err, imesg)
if (err /= DCT_SUCCESS) then
   print *, "[", err, "]  ", imesg
   stop 'receiving pr'
end if
pr = cplpr

! The received varialbles are output into a file
!call WriteFileDoubleData( nI, nJ, scx, scy, cplt, 'ax', 'ay', 'cplvx', &
!        & './SCRATCH/var_avx_'//trim(FileProc)//'.m' )
!call WriteFileDoubleData( nI, nJ, scx, scy, cplvx, 'ax', 'ay', 'cplvx', &
!        & './SCRATCH/var_avx_'//trim(FileProc)//'.m' )
!call WriteFileDoubleData( nI, nJ, scx, scy, cplvy, 'ax', 'ay', 'cplvy', &
!        & './SCRATCH/var_avy_'//trim(FileProc)//'.m' )
!call WriteFileDoubleData( pnI, pnJ, mx(piniI:pendI), my(piniJ:pendJ), cplpr,&
!        & 'ax', 'ay', 'cplpr', './SCRATCH/var_apr_'//trim(FileProc)//'.m' )


! Entering the calculation loop
StepN=1
do while (StepN <= iterM)
   time = StepN*dt
   print *,"Model A. Step: ", StepN,". Time : ", time

   !!!!!! Test using linear function...
   !!!! Function constant
   !vx = time + 1.0D0
   !vy = time - 1.0D-2*time + 0.5D0
   coef = 0.6D-2
   t = time + t!*coef - 1.5D0
   
   !!!! Function linear
   !vx = (time + 1.0D0)*grx + (1.0D0 - time)*gry
   !vy = grx - 1.0D-2*gry  + time
   !coef = 0.6D-2
   !t = time*(grx - gry) + coef*(grx + gry)
   
   !!!! Whatever
   !vx = grx*grx + gry*gry + time 
   !vy = time*grx*grx - 1.0D-2*time*gry*gry
   !coef = 0.6D-2
   !t = (grx + gry)*(grx + gry) + coef*(grx*grx + gry*gry)
   
   
   !!!!! Set the initial time to current time
   call dct_update_model_time( dmodel, err, imesg )


   cplt = t
   print *,"Sending t (temperature) in time: ", Time
   call dct_send_field ( dctt, err, imesg)
   if (err /= DCT_SUCCESS) then
      print *, "[", err, "]  ", imesg
      stop 'sending t'
   end if
   
   print *,"Receiving vx in time: ", Time
   call dct_recv_field ( dctvx, err, imesg)
   if (err /= DCT_SUCCESS) then
      print *, "[", err, "]  ", imesg
      stop 'receiving vx'
   end if
   vx = cplvx

   print *,"Receiving vy in time: ", Time
   call dct_recv_field ( dctvy, err, imesg)
   if (err /= DCT_SUCCESS) then
      print *, "[", err, "]  ", imesg
      stop 'receiving vy'
   end if
   vy = cplvy

   print *,"Receiving pr in time: ", Time
   call dct_recv_field ( dctpr, err, imesg)
   if (err /= DCT_SUCCESS) then
      print *, "[", err, "]  ", imesg
      stop 'receiving pr'
   end if
   pr = cplpr
   
   StepN=StepN+1

enddo

!******************************************************
!   Destroy the Coupling structures
!******************************************************
call dct_destroy_couple ( dcouple, err, imesg )
if (err /= DCT_SUCCESS) then
   print *, imesg
   stop 'destroying coupling'
end if
 
call dct_destroy_couple ( dcouple2, err, imesg )
if (err /= DCT_SUCCESS) then
   print *, imesg
   stop 'destroying coupling2'
end if
 
call dct_destroy_field ( dctvx, err, imesg )
if (err /= DCT_SUCCESS) then
   print *, imesg
   stop 'destroying Vx'
end if
 
call dct_destroy_field ( dctvy, err, imesg )
if (err /= DCT_SUCCESS) then
   print *, imesg
   stop 'destroying Vy'
end if
 
call dct_destroy_field ( dctt, err, imesg )
if (err /= DCT_SUCCESS) then
   print *, imesg
   stop 'destroying t'
end if
 
call dct_destroy_field ( dctpr, err, imesg )
if (err /= DCT_SUCCESS) then
   print *, imesg
   stop 'destroying pr'
end if
 
call dct_destroy_model ( dmodel, err, imesg )
if (err /= DCT_SUCCESS) then
   print *, imesg
   stop 'destroying model'
end if
 

call dct_finalized ( err, imesg )
if (err /= DCT_SUCCESS) then
   print *, imesg
   stop 'finalizing dct'
end if

!finalize system
deallocate( mx, my, scx, scy, vx, vy, t, pr, cplvx, cplvy, cplt, cplpr )

!CONTAINS

END SUBROUTINE test_subdom_moda


