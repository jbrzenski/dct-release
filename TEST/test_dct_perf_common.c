/*******************************************************************/
/*                Distributed Coupling Toolkit (DCT)               */
/*                                                                 */
/*!
   \file test_dct_perf_common.c                   

    Souce code file where the domain limits and other        
    common utils are defined for test_dct_perfom tests       
                                                           

   \date Created on Oct 10, 2011
    
    \author Dany De Cecchis: dcecchis@gmail.com
    \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.

*/
/*******************************************************************/

#include <stdlib.h>
#include "test_dct_perform.h"

/* Parameters */
struct Parms
{ 
  double cx;
  double cy;
} parms = {0.1, 0.1};

/****************************************************************************
 *  subroutine update
 ****************************************************************************/
void update(int nx, int ny, double *u1, double *u2)
{
   int offset, offset1, offset2, offset3;
   int ix, iy;
   int limsup, liminf;

   limsup = nx-1;
   liminf = ny-1;
   
   for (ix = 1; ix < limsup; ix++) {
      offset  = ix*ny;
      offset1 = offset + ny;
      offset2 = offset - ny;
      for (iy = 1; iy < liminf; iy++) {
         offset3 = offset + iy;
         *(u2 + offset3 ) = *(u1 + offset3 )  + 
         parms.cx * (*(u1 + offset1 + iy ) + *(u1 + offset2 + iy) - 
         2.0 * *(u1 + offset3)) +
         parms.cy * (*(u1 + offset3 + 1) + *(u1 + offset3 - 1) - 
         2.0 * *(u1 + offset3));
      }
   }
}

/*****************************************************************************
 *  subroutine inidat
 *****************************************************************************/
void inidat( int nx, int ny, int inix, int iniy, int gnx, int gny, double *u1)
{
   int ix, iy;
   double fact;

   for (ix = 0; ix < nx; ix++) 
      for (iy = 0; iy < ny; iy++) 
         *(u1 + ix*ny + iy) = ( (double)(inix + ix) * (double)(gnx - inix - ix - 1) * 
                                (double)(iniy + iy) * (double)(gny - iniy - iy - 1) );
}

