#include <mpi.h>
// #include <netcdf.h>
// #include "staggeredgrid2d.h"

#ifdef DCT
	extern "C" 
	{
		#include "dct.h"
	}
#endif

#define ERRCODE 3
#define ERR(e) { printf("Error: %s\n", nc_strerror(e)); exit(ERRCODE); }


//======================================================================
// 2D Problem parameters
bool NETCDF = 1;  // Output in NetCDF format?
double a = 0;     // West boundary
double b = 100;   // East boundary
double c = 0;     // South boundary
double d = 100;   // North boundary
double t = 5;     // Simulation time
u32 m = 100;      // Horizontal resolution
u32 n = 100;      // Vertical resolution
u32 k = 10000;    // Time resolution
double e = 75;    // Dispersion coefficient
double R = 1;     // Reaction rate
u16 order = 2;    // Order of spatial accuracy


// Initial condition (centers)
double ic(double, double)
{
	return 0;
}

// Horizontal edges
double u(double, double)
{
	return 0;
}

// Vertical edges
double v(double, double)
{
	return 0;
}

// Boundary condition (centers)
double bc(double x, double y)
{
	// Based on maximum pressure location
	if (x == 0 && y >= 0.5*(d-c)+c && y <= 0.75*(d-c)+c)
		return 1;
	else
		return 0;
}
//======================================================================


#ifndef DCT
	int main(int argc, char *argv[])
	{
#else
	int subflow(int argc, char *argv[], MPI_Comm lcomm, MPI_Comm gcomm)
	{
#endif


	argc = argc;
	argv = argv;
	
	double dx = (b-a)/m;
	double dy = (d-c)/n;
	double dt = t/k;


#ifdef DCT
//========================= DCT VARIABLES ==============================
    int numtasks, np[2] = {1, 1};
    int rank, iniind[2] = {0, 0}, endind[2] = {(m+2)-1, (n+2)-1};
    double t0 = 0;
    
    DC_Scalar centers_x[m+2]; 
    DC_Scalar centers_y[n+2];
    double P((m+2)*(n+2));
	 double U((m+2)*(n+2)); // (m+1)*n
	 double V((m+2)*(n+2)); // (n+1)*m
	
    DCT_Model  model;
    DCT_Couple couple;
    DCT_Field  dct_P;
    DCT_Field  dct_U;
    DCT_Field  dct_V;
    DCT_Error  dcterr;
    
    // Construct center's grid for DCT exchange
    centers_x[0] = a;
    centers_x[1] = a+dx/2;
    centers_x[m] = b-dx/2;
    centers_x[m+1] = b;
    centers_y[0] = c;
    centers_y[1] = c+dy/2;
    centers_y[n] = d-dy/2;
    centers_y[n+1] = d;
    
    for (u32 ii = 2; ii < m+1; ii++)
		  centers_x[ii] = centers_x[ii-1]+dx;
	
    for (u32 ii = 2; ii < n+1; ii++)
		  centers_y[ii] = centers_y[ii-1]+dy;
	
	
	MPI_Comm_size(gcomm, &numtasks);
	MPI_Comm_rank(gcomm, &rank);
	
	int mranks[] = {rank};


//======================== DCT REGISTRATION ============================
	lcomm = lcomm;
	dcterr = DCT_BeginRegistration(gcomm);
	DCTCHKERR(dcterr);
	
	// Create DCT_Model
	dcterr = DCT_Create_Model(&model, "subflow", "Model subflow", numtasks);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Set_Model_Time(&model, t0, dt, DCT_TIME_NO_UNIT);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Set_Model_Dom(&model, 2, 1, DCT_UNSTRUCTURED, &centers_x[0], m+2);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Set_Model_Dom(&model, 2, 2, DCT_UNSTRUCTURED, &centers_y[0], n+2);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Set_Model_ParLayout(&model, DCT_DIST_RECTANGULAR, np, NULL);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Set_Model_SubDom(&model, mranks, iniind, endind);
	DCTCHKERR(dcterr);
	
	// Create DCT_Field
	dcterr = DCT_Create_Field(&dct_P, "p_sub", "Pressure from subflow model",
							  DCT_NO_UNITS, DCT_CONSUME);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Create_Field(&dct_U, "u_sub", "u-velocity from subflow model",
							  DCT_NO_UNITS, DCT_CONSUME);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Create_Field(&dct_V, "v_sub", "v-velocity from subflow model",
							  DCT_NO_UNITS, DCT_CONSUME);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Set_Field_Dims(&dct_P, m+2, n+2);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Set_Field_Dims(&dct_U, m+2, n+2);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Set_Field_Dims(&dct_V, m+2, n+2);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Set_Field_Labels(&dct_P, DCT_UNSTRUCTURED, &centers_x[0], m+2,
								  DCT_UNSTRUCTURED, &centers_y[0], n+2);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Set_Field_Labels(&dct_U, DCT_UNSTRUCTURED, &centers_x[0], m+2,
								  DCT_UNSTRUCTURED, &centers_y[0], n+2);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Set_Field_Labels(&dct_V, DCT_UNSTRUCTURED, &centers_x[0], m+2,
								  DCT_UNSTRUCTURED, &centers_y[0], n+2);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Set_Field_Val_Location(&dct_P, DCT_LOC_CENTERED);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Set_Field_Val_Location(&dct_U, DCT_LOC_CENTERED);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Set_Field_Val_Location(&dct_V, DCT_LOC_CENTERED);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Set_Field_Time(&dct_P, DCT_TIME_NO_UNIT, t0);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Set_Field_Time(&dct_U, DCT_TIME_NO_UNIT, t0);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Set_Field_Time(&dct_V, DCT_TIME_NO_UNIT, t0);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Set_Field_Freq_Consumption(&dct_P, dt*100.0);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Set_Field_Freq_Consumption(&dct_U, dt*100.0);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Set_Field_Freq_Consumption(&dct_V, dt*100.0);
	DCTCHKERR(dcterr);
	
	// Link local variable to DCT field
	dcterr = DCT_Set_Field_Values(&dct_P, DCT_DOUBLE, &P[0]);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Set_Field_Values(&dct_U, DCT_DOUBLE, &U[0]);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Set_Field_Values(&dct_V, DCT_DOUBLE, &V[0]);
	DCTCHKERR(dcterr);
	
	// Link field to model
	dcterr =  DCT_Set_Model_Var(&model, &dct_P, DCT_FIELD_TYPE);
	DCTCHKERR(dcterr);
	
	dcterr =  DCT_Set_Model_Var(&model, &dct_U, DCT_FIELD_TYPE);
	DCTCHKERR(dcterr);
	
	dcterr =  DCT_Set_Model_Var(&model, &dct_V, DCT_FIELD_TYPE);
	DCTCHKERR(dcterr);
	
	// Create DCT_Couple
	dcterr =  DCT_Create_Couple(&couple, "cpl_sub_res", 
								"Coupling between subflow and resflow models",
								&model, "resflow");
	DCTCHKERR(dcterr);
	
	// Link field to couple
	dcterr =  DCT_Set_Coupling_Vars(&couple, &dct_P, "p_res", DCT_FIELD_TYPE,
                                    DCT_LINEAR_INTERPOLATION);
	DCTCHKERR(dcterr);
	
	dcterr =  DCT_Set_Coupling_Vars(&couple, &dct_U, "u_res", DCT_FIELD_TYPE,
                                    DCT_LINEAR_INTERPOLATION);
	DCTCHKERR(dcterr);
	
	dcterr =  DCT_Set_Coupling_Vars(&couple, &dct_V, "v_res", DCT_FIELD_TYPE,
                                    DCT_LINEAR_INTERPOLATION);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_EndRegistration();
	DCTCHKERR(dcterr);
//======================================================================
#endif


//============================= MOLE ===================================
    // Discretize the domain and impose conditions
//     StaggeredGrid2D* grid2D = new StaggeredGrid2D(a, b, c, d, m, n);
//     grid2D->assignCenters(&ic);
//     grid2D->imposeBCsCenters(&bc);
//     grid2D->assignEdges(&u, &v);
//     grid2D->imposeBCsEdges(&u, &v);
//     
//     // Get mimetic operators
//     Gradient* G = new Gradient(order, m, n, dx, dy);
//     Divergence* D = new Divergence(order, m, n, dx, dy);
//     Interpol* I = new Interpol(m, n, 0.5, 0.5);
//======================================================================


// 	vec R1(G->n_rows);
// 	vec R2(G->n_cols);
// 	vec R3(G->n_cols);


	// Time integration loop
	for (u32 tt = 0; tt <= k; tt++)
	{
      cout  << "Time integration..." << endl;

#ifdef DCT
//======================== UPDATE DCT FIELDS ===========================
      dcterr =  DCT_Update_Model_Time(&model);
      DCTCHKERR(dcterr);
   
      dcterr =  DCT_Recv_Field(&dct_P);
      DCTCHKERR(dcterr);
   
      dcterr =  DCT_Recv_Field(&dct_U);
      DCTCHKERR(dcterr);
   
      dcterr =  DCT_Recv_Field(&dct_V);
      DCTCHKERR(dcterr);
//======================================================================
#endif


		R1 = *G*grid2D->C;
		
		R1 = e*R1;
		
		R2 = *D*R1;
		
		R1 = *I*grid2D->C;
		
		R1 = R1%grid2D->E;
		
		R3 = *D*R1;
		
		R2 = (dt/R)*(R2-R3);
		
		grid2D->C += R2;

	// Spit out results in NetCDF format
// 		if (NETCDF&&(tt%500==1))
//       {
//          int ncid, x_dimid, y_dimid, varidD, retval, varidTime;
//          int dimids[2];
//       
//          if ((retval = nc_create("output.nc", NC_CLOBBER, &ncid)))
//             ERR(retval);
//       
//          double data_outD[n+2][m+2];  // Density
      
#ifdef DCT
         int varidP;
         double data_outP[n+2][m+2];  // Pressure
#endif
      
         for (u32 ii = 0; ii < n+2; ii++)
            for (u32 jj = 0; jj < m+2; jj++)
            {
//                data_outD[ii][jj] = grid2D->C[jj+ii*(m+2)];

#ifdef DCT
               data_outP[ii][jj] = P[jj+ii*(m+2)];
#endif
            }
      
//          if ((retval = nc_def_dim(ncid, "x", m+2, &x_dimid)))
//             ERR(retval);
//          if ((retval = nc_def_dim(ncid, "y", n+2, &y_dimid)))
//             ERR(retval);
      
         dimids[0] = x_dimid;
         dimids[1] = y_dimid;
      
//          if ((retval = nc_def_var(ncid, "Time", NC_DOUBLE, 0, dimids, &varidTime)))
//             ERR(retval);
// 
//          if ((retval = nc_def_var(ncid, "Density", NC_DOUBLE, 2, dimids, &varidD)))
//             ERR(retval);
// 
// #ifdef DCT
//          if ((retval = nc_def_var(ncid, "Pressure", NC_DOUBLE, 2, dimids, &varidP)))
//             ERR(retval);
// #endif
// 
//          if ((retval = nc_enddef(ncid)))
//             ERR(retval);
//          double currtime = tt*dt;
//          if ((retval = nc_put_var_double(ncid, varidD, &currtime)))
//             ERR(retval);
// 
//          if ((retval = nc_put_var_double(ncid, varidD, &data_outD[0][0])))
//             ERR(retval);
// 
// #ifdef DCT
//          if ((retval = nc_put_var_double(ncid, varidP, &data_outP[0][0])))
//             ERR(retval);
// #endif
// 
//          if ((retval = nc_close(ncid)))
//             ERR(retval);
//       }
	} /* End of for (u32 tt = 0; tt <= k; tt++) */


   R1.clear();
   R2.clear();
   R3.clear();



#ifdef DCT
//========================== DCT FINALIZE ==============================
// 	centers_x.clear();
// 	centers_y.clear();
// 	P.clear();
// 	U.clear();
// 	V.clear();
	
	dcterr = DCT_Destroy_Couple(&couple);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Destroy_Field(&dct_P);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Destroy_Field(&dct_U);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Destroy_Field(&dct_V);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Destroy_Model(&model);
	DCTCHKERR(dcterr);
	
	dcterr = DCT_Finalized();
	DCTCHKERR(dcterr);
//======================================================================
#endif


// 	delete grid2D;
// 	delete G;
// 	delete D;
// 	delete I;
    
	return 0;
}
