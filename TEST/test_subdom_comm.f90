PROGRAM test_subdom_comm

!===============================================================
!  FILE: test_subdom_comm.f90
!===============================================================
! Description: This file is the main program to call two model
!              dummies: test_subdom_moda and test_subdom_modb
!
! 20-Oct-2010: Created by: Dany De Cecchis
!---------------------------------------------------------------------
!
!$Id: test_subdom_comm.f90,v 1.3 2011/08/24 21:17:35 dcecchis Exp $
!

   implicit none
   
   include "mpif.h"
   
   !---------------------------------------------------- Variables
   INTEGER                :: rank, nprocs, ierr
   INTEGER                :: color, model_comm
   

   !----------------------------------------------- Program Begins
   
   call MPI_INIT(ierr)
   
   call MPI_COMM_SIZE(MPI_COMM_WORLD,nprocs,ierr)
   call MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)

   if ( nprocs /= 8 ) then
      if (rank==0) then
         print *, "[ errcod: ", ierr, "] This test must be run with only 8 processsors"
      endif
      call MPI_ABORT(MPI_COMM_WORLD, 999, ierr)
      stop 'wrong processor number'
   endif

   if (rank < 2) then
      color = 0
   else if (rank < 6) then
      color = 1
   else
      color = 2   
   endif

   call MPI_COMM_SPLIT(MPI_COMM_WORLD,color,0,model_comm,ierr)
   
   if (rank < 2) then
      
      call test_subdom_moda ( model_comm, MPI_COMM_WORLD )
   else if (rank < 6) then
      
      call test_subdom_modb ( model_comm, MPI_COMM_WORLD )
   else
      
      call test_subdom_modc ( model_comm, MPI_COMM_WORLD )
   endif


   call MPI_FINALIZE(ierr)
   
   !------------------------------------------------- Program Ends
  
END PROGRAM test_subdom_comm

