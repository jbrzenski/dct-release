/*******************************************************************/
/*                 Distributed Coupling Toolkit (DCT)              */
/*                                                                 */
/*!   \file  convertnetcdf.c
       \brief Program thats convert cartesian grids into
       NetCDF file.

       This program convert the cartesian grid into
       NetCDF file of the grid used as input int the
       package SCRIP.

       \date Created: Dec 9, 2011

       \author Dany De Cecchis: dcecchis@gmail.com
       \author Tony Drummond: LADrummond@lbl.gov
  
       \copyright GNU Public License.

*/
/*******************************************************************/

/* -----------------------------------------------  Include Files  */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <netcdf.h>

#include "test_dct_perform.h"

#define ERRCODE 2
#define CHCKERR( x, y ) if ( x ) {\
       fflush( NULL );\
       fprintf( stderr, "ERROR[%s]: %s\n", #x, y );\
       fprintf( stderr, "ERROR: in file %s (line: %d)\n", __FILE__ , __LINE__ );\
       fflush( NULL);\
       exit(ERRCODE);\
  }


int main(int argc, char *argv[])
{

/* ---------------------------------------  Variables Declaration  */
   int      nptsx, nptsy, nxm1, nym1;
   int      intrvsX, intrvsY;
   char    *grid_name, *grid_file_out;

   int      nc_grid_id, nc_gridsize_id, nc_gridrank_id, nc_gridcorn_id;
   int      nc_griddims_id, nc_grdcntrlat_id, nc_grdcntrlon_id, nc_grdimask_id;
   int      nc_grdcrnrlat_id, nc_grdcrnrlon_id;
   int      nc_dims2_id[2];
   int     *grid_imask;
   double  *grid_center_lat, *grid_center_lon;
   double  *grid_corner_lat, *grid_corner_lon;
   double  *tickmarks_lat, *tickmarks_lon;
   double  *cntr_tickmarks_lat, *cntr_tickmarks_lon;
   
   double   deltaX, deltaY, auxconst, auxconst1, auxconst2, auxconst3;
   
   int      grid_dims[2];
   int      len, grid_size, grid_corners, grid_rank;

   int      ii, jj, ind, ierr;

/* ------------------------------------------------ Program Begins */
   if ( argc != 5 ) {
      printf("[ERROR %s]: %s nptsx nptsy grid_file_out gridname\n", argv[0], argv[0] );
      printf("where nptsx nptsy are number of points in each direction,\n");
      printf("and grid_file_out the name of the output grid file and\n");
      printf("gridname is the name given to the grid.\n");
   }
   nptsx = atoi(argv[1]);
   nptsy = atoi(argv[2]);
   intrvsX = nptsx+1;
   intrvsY = nptsy+1;
   nxm1 = nptsx-1;
   nym1 = nptsy-1;

/* ------------------------------------------------------------------
     Variables defined to be used in the file
------------------------------------------------------------------ */

   grid_size = nptsx*nptsy;
   grid_dims[0] = intrvsX;
   grid_dims[1] = intrvsY;
   grid_corners = 4;
   grid_rank = 2;

   tickmarks_lon = (double *)malloc( sizeof(double)*(size_t)intrvsX );
   CHCKERR( tickmarks_lon == (double *)NULL, "Failed memory allocation" );
   tickmarks_lat = (double *)malloc( sizeof(double)*(size_t)intrvsY );
   CHCKERR( tickmarks_lat == (double *)NULL, "Failed memory allocation" );

   cntr_tickmarks_lon = (double *)malloc( sizeof(double)*(size_t)nptsx );
   CHCKERR( cntr_tickmarks_lon == (double *)NULL, "Failed memory allocation" );
   cntr_tickmarks_lat = (double *)malloc( sizeof(double)*(size_t)nptsy );
   CHCKERR( cntr_tickmarks_lat == (double *)NULL, "Failed memory allocation" );

   len = strlen( argv[3] );
   grid_file_out = (char *)malloc( sizeof(char)*(size_t)len+1 );
   CHCKERR( grid_file_out == (char *)NULL, "Failed memory allocation" );
   strcpy( grid_file_out, argv[3] );
   grid_file_out[len]='\0';

   len = strlen( argv[4] );
   grid_name = (char *)malloc( sizeof(char)*(size_t)len+1 );
   CHCKERR( grid_name == (char *)NULL, "Failed memory allocation" );
   strcpy( grid_name, argv[4] );
   grid_name[len]='\0';

   grid_center_lon = (double *)malloc( sizeof(double)*(size_t)grid_size );
   CHCKERR( grid_center_lon == (double *)NULL, "Failed memory allocation" );
   grid_center_lat = (double *)malloc( sizeof(double)*(size_t)grid_size );
   CHCKERR( grid_center_lat == (double *)NULL, "Failed memory allocation" );

   grid_imask = (int *)malloc( sizeof(int)*(size_t)grid_size );
   CHCKERR( grid_imask == (int *)NULL, "Failed memory allocation" );

   len = grid_corners*grid_size;
   grid_corner_lon = (double *)malloc( sizeof(double)*(size_t)len );
   CHCKERR( grid_corner_lon == (double *)NULL, "Failed memory allocation" );
   grid_corner_lat = (double *)malloc( sizeof(double)*(size_t)len );
   CHCKERR( grid_corner_lat == (double *)NULL, "Failed memory allocation" );

   /* Calculating the inverse of the number of intervals */
   deltaX = 1.0/(double)(nxm1);
   deltaY = 1.0/(double)(nym1);

//    for ( ii=0; ii < intrvsX; ii++ ) tickmarks_lon[ii] = INIX + deltaX*(double)ii;
//    tickmarks_lon[intrvsX] = ENDX;
//    for ( jj=0; jj < intrvsY; jj++ ) tickmarks_lat[jj] = INIY + deltaY*(double)jj;
//    tickmarks_lat[intrvsY] = ENDY;
   cntr_tickmarks_lon[0] = INIX;
   for ( ii=1; ii < nxm1; ii++ ) 
       cntr_tickmarks_lon[ii] = (INIX*(double)(nxm1-ii) + ENDX*(double)ii)*deltaX;
   cntr_tickmarks_lon[nxm1] = ENDX;
   
   cntr_tickmarks_lat[0] = INIY;
   for ( jj=1; jj < nym1; jj++ )
       cntr_tickmarks_lat[jj] = (INIY*(double)(nym1-jj) + ENDY*(double)jj)*deltaY;
   cntr_tickmarks_lat[nym1] = ENDY;

//    for ( ii=0; ii < intrvsX; ii++ )
//       cntr_tickmarks_lon[ii] = 0.5*(tickmarks_lon[ii]+tickmarks_lon[ii+1]);
//    for ( jj=0; jj < intrvsY; jj++ )
//       cntr_tickmarks_lat[jj] = 0.5*(tickmarks_lat[jj]+tickmarks_lat[jj+1]);
   
   /* Calculating a half of the interval in the grid */
   deltaX = (ENDX - INIX)/(double)(2*nxm1);
   deltaY = (ENDY - INIY)/(double)(2*nym1);
   tickmarks_lon[0] = INIX - deltaX;
   for ( ii=1; ii < nptsx; ii++ )
      tickmarks_lon[ii] = 0.5*(cntr_tickmarks_lon[ii-1] + cntr_tickmarks_lon[ii]);
   tickmarks_lon[nptsx] = ENDX + deltaX;

   tickmarks_lat[0] = INIY - deltaY;
   for ( jj=1; jj < nptsy; jj++ )
      tickmarks_lat[jj] = 0.5*(cntr_tickmarks_lat[jj-1] + cntr_tickmarks_lat[jj]);
   tickmarks_lat[nptsy] = ENDY + deltaY;

   ind=0;
   for ( ii=0; ii < nptsx; ii++ ) {
      auxconst = cntr_tickmarks_lon[ii];
      for ( jj=0; jj < nptsy; jj++ ) {
         grid_center_lon[ind] = auxconst;
         grid_center_lat[ind] = cntr_tickmarks_lat[jj];
         grid_imask[ind] = 1;
         ind++;
      }
   }

   ind=0;
   for ( ii=0; ii < nptsx; ii++ ) {
      auxconst = tickmarks_lon[ii];
      auxconst1 = tickmarks_lon[ii+1];
      for ( jj=0; jj < nptsy; jj++ ) {
         grid_corner_lon[ind] = auxconst;
         grid_corner_lon[ind+1] = auxconst1;
         grid_corner_lon[ind+2] = auxconst1;
         grid_corner_lon[ind+3] = auxconst;

         auxconst2 = tickmarks_lat[jj];
         auxconst3 = tickmarks_lat[jj+1];
         grid_corner_lat[ind] = auxconst2;
         grid_corner_lat[ind+1] = auxconst2;
         grid_corner_lat[ind+2] = auxconst3;
         grid_corner_lat[ind+3] = auxconst3;
         ind += 4;
      }
   }

/* ------------------------------------------------------------------
         set up attributes for netCDF file
------------------------------------------------------------------ */

      /***
       *** create netCDF dataset for this grid
       ***/
      ierr = nc_create( grid_file_out, NC_CLOBBER, &nc_grid_id);
      CHCKERR( ierr, "Error calling netcdf function" );

      ierr = nc_put_att_text( nc_grid_id, NC_GLOBAL, "title", strlen(grid_name), grid_name );
      CHCKERR( ierr, "Error calling netcdf function" );


      /***
       *** define grid size dimension
       ***/
      ierr = nc_def_dim( nc_grid_id, "grid_size", grid_size, &nc_gridsize_id );
      CHCKERR( ierr, "Error calling netcdf function" );

      /***
       *** define grid rank dimension
       ***/
      ierr = nc_def_dim( nc_grid_id, "grid_rank", grid_rank, &nc_gridrank_id );
      CHCKERR( ierr, "Error calling netcdf function" );

      /***
       *** define grid corner dimension
       ***/
      ierr = nc_def_dim( nc_grid_id, "grid_corners", grid_corners, &nc_gridcorn_id );
      CHCKERR( ierr, "Error calling netcdf function" );

      /***
       *** define grid dim size array
       ***/
      ierr = nc_def_var( nc_grid_id, "grid_dims", NC_INT, 1, &nc_gridrank_id, &nc_griddims_id );
      CHCKERR( ierr, "Error calling netcdf function" );

      /***
       *** define grid center latitude array
       ***/
      ierr = nc_def_var( nc_grid_id, "grid_center_lat", NC_DOUBLE, 1, &nc_gridsize_id,
                                                                    &nc_grdcntrlat_id );
      CHCKERR( ierr, "Error calling netcdf function" );

      ierr = nc_put_att_text( nc_grid_id, nc_grdcntrlat_id, "units", 7, "degrees" );
      CHCKERR( ierr, "Error calling netcdf function" );


      /***
       *** define grid center longitude array
       ***/
      ierr = nc_def_var( nc_grid_id, "grid_center_lon", NC_DOUBLE, 1, &nc_gridsize_id,
                                                              &nc_grdcntrlon_id );
      CHCKERR( ierr,"Error calling netcdf function" );

      ierr = nc_put_att_text( nc_grid_id, nc_grdcntrlon_id, "units", 7, "degrees" );
      CHCKERR( ierr, "Error calling netcdf function" );

      /***
       *** define grid mask
       ***/
      ierr = nc_def_var( nc_grid_id, "grid_imask", NC_INT, 1, &nc_gridsize_id,
                                                              &nc_grdimask_id );
      CHCKERR( ierr, "Error calling netcdf function" );

      ierr = nc_put_att_text( nc_grid_id, nc_grdimask_id, "units", 8, "unitless" );
      CHCKERR( ierr, "Error calling netcdf function" );

      /***
       *** define grid corner latitude array
       ***/

      nc_dims2_id[0] = nc_gridsize_id;
      nc_dims2_id[1] = nc_gridcorn_id;

      ierr = nc_def_var( nc_grid_id, "grid_corner_lat", NC_DOUBLE, 2, nc_dims2_id,
                                                                &nc_grdcrnrlat_id );
      CHCKERR( ierr, "Error calling netcdf function" );

      ierr = nc_put_att_text( nc_grid_id, nc_grdcrnrlat_id, "units", 7, "degrees" );
      CHCKERR( ierr, "Error calling netcdf function" );

      /***
       *** define grid corner longitude array
       ***/
      ierr = nc_def_var( nc_grid_id, "grid_corner_lon", NC_DOUBLE, 2, nc_dims2_id,
                                                              &nc_grdcrnrlon_id );
      CHCKERR( ierr, "Error calling netcdf function" );

      ierr = nc_put_att_text( nc_grid_id, nc_grdcrnrlon_id, "units", 7, "degrees" );
      CHCKERR( ierr, "Error calling netcdf function" );

      /***
       *** end definition stage
       ***/
      ierr = nc_enddef(nc_grid_id);
      CHCKERR( ierr, "Error calling netcdf function" );

/* ------------------------------------------------------------------
              write grid data
------------------------------------------------------------------ */
      ierr = nc_put_var_int(nc_grid_id, nc_griddims_id, grid_dims);
      CHCKERR( ierr, "Error calling netcdf function" );

      ierr = nc_put_var_int(nc_grid_id, nc_grdimask_id, grid_imask);
      CHCKERR( ierr, "Error calling netcdf function" );

      ierr = nc_put_var_double(nc_grid_id, nc_grdcntrlat_id, grid_center_lat);
      CHCKERR( ierr, "Error calling netcdf function" );

      ierr = nc_put_var_double(nc_grid_id, nc_grdcntrlon_id, grid_center_lon);
      CHCKERR( ierr, "Error calling netcdf function" );

      ierr = nc_put_var_double(nc_grid_id, nc_grdcrnrlat_id, grid_corner_lat);
      CHCKERR( ierr, "Error calling netcdf function" );

      ierr = nc_put_var_double(nc_grid_id, nc_grdcrnrlon_id, grid_corner_lon);
      CHCKERR( ierr, "Error calling netcdf function" );

      ierr = nc_close(nc_grid_id);
      CHCKERR( ierr, "Error closing netcdf file" );
/* ------------------------------------------------------------------
------------------------------------------------------------------ */

   free ( tickmarks_lat );
   free ( tickmarks_lon );
   free ( cntr_tickmarks_lat );
   free ( cntr_tickmarks_lon );
   free ( grid_file_out );
   free ( grid_name );
   free ( grid_center_lat );
   free ( grid_center_lon );
   free ( grid_imask );
   free ( grid_corner_lat );
   free ( grid_corner_lon );

   return 0;
}


