!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!               Distributed Coupling Toolkit (DCT)
!!
!>       \file interp_tools.F90
!!   
!!   \brief Module file of tools to check interpolation results.
!!
!!   In this file there are different tool subroutines that
!!   generates the values to be interpolated on the several
!!   mesh implementation, and output result to be plotted using
!!   Matlab.
!!   
!!   \date Date Created: Aug 10. 2011
!!
!!   \author Dany De Cecchis: dcecchis@gmail.com
!!   
!!   

!>    \brief Module that contains tools to check interpolation results.
!! 
!!   Contains several subroutines that
!!   generates the values to be interpolated on the several
!!   mesh implementation, and output result to be plotted using
!!   Matlab.
MODULE INTERP_TOOLS

!   USE NETCDF

IMPLICIT NONE
   private
   ! long integer type
   integer, parameter :: long = 8!selected_int_kind(10)

!-------------------------------- Interface
   interface WriteFileRealData
      module procedure WriteFileRealData_Int !, &
!                     & WriteFileRealData_Long
   end interface
   interface WriteFileDoubleData
      module procedure WriteFileDoubleData_Int !, &
!                     & WriteFileDoubleData_Long
   end interface
   public :: test_mct_get_bilin_interp
   public :: test_mct_get_bilin_interp_dist
   public :: WriteFileRealData
   public :: WriteFileDoubleData
   
   interface test_mct_get_bilin_interp
      module procedure test_mct_get_bilin_interp
   end interface

   interface Calculate_DirectionWeights
      module procedure Calculate_DirectionWeights_long,&
                     & Calculate_DirectionWeights_int
   end interface

!   interface WriteFileData
!      module procedure WriteFileRealData, WriteFileDoubleData
!   end interface

CONTAINS
!****************************************************************************
!>             \brief Generates the bilinear interpolation coefficients for
!!             equally spaced discretized domains
!!             
!!             This subroutine Generates the bilinear interpolation
!!             coefficients for equally spaced discretized domains between
!!             values in xends and yends (both domains).
!!
!!             The coefficients are stored in an aij sparse matrix format
!!             to be used in MCT. Mapping goes from A to B.
!!
SUBROUTINE test_mct_get_bilin_interp ( Apts, Bpts, xends, yends, num_elements, &
                                     & nRows, nColumns, rows, columns, weights, ierr)

   integer, intent(in), dimension(2) :: Apts, Bpts
   double precision, intent(in), dimension(2) :: xends, yends
   integer, intent(out) :: nRows, nColumns, num_elements
   integer, intent(out) :: ierr
   integer, dimension(:), pointer :: rows, columns
   double precision, dimension(:), pointer :: weights


! --------------------------------------------  Variables Declaration
   integer, allocatable, dimension(:) :: indX, indY     ! Indices of the lowest neighbor
   double precision, allocatable, dimension(:) :: wX, wY ! The langrangian coeff

   integer :: ii, jj, ind, irow, indI, indIm1
   integer :: strdA, strdAm1, strdB
   integer :: mod1x, mod1y, mod2x, mod2y
   double precision :: tt, ttm1, uu, uum1, temw

! -------------------------------  BEGIN( test_mct_get_bilin_interp )

   mod1x = Apts(1)
   mod1y = Apts(2)
   mod2x = Bpts(1)
   mod2y = Bpts(2)

   allocate ( indX(mod2x), indY(mod2y), wX(mod2x), wY(mod2y) )
   
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!! Calculating the coefficient in x-direction 
   call Calculate_DirectionWeights( xends, mod1x, mod2x, indX, wX )
!   print *, 'xends = ', xends
!   print *, 'mod1x = ', mod1x
!   print *, 'mod2x = ', mod2x
!   print *, ''
!   print *, '[ ii, indX, wX ] = '
!   do ii=1, mod2x
!      print *, '[ ', ii, ', ', indX(ii), ', ', wX(ii), ' ]'
!   end do


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!! Calculating the coefficient in y-direction 
   call Calculate_DirectionWeights( yends, mod1y, mod2y, indY, wY )
!   print *, 'yends = ', yends
!   print *, 'mod1x = ', mod1y
!   print *, 'mod2x = ', mod2y
!   print *, ''
!   print *, '[ jj, indY, wY ] = '
!   do jj=1, mod2y
!      print *, '[ ', jj, ', ', indY(jj), ', ', wY(jj), ' ]'
!   end do

   nRows = mod2x*mod2y
   nColumns = mod1x*mod1y
   num_elements = 4*nrows
   allocate ( rows(num_elements), columns(num_elements), weights(num_elements) )
   
   ind = 1
   do jj = 1,mod2y
      uu = wY(jj)
      uum1 = 1 - uu
      strdAm1 = indY(jj)*mod1x     !! upper row
      strdA = strdAm1 - mod1x      !! lower row
      strdB = (jj-1)*mod2x
      do ii = 1, mod2x
         tt = wX(ii)
         ttm1 = 1 - tt
         irow = ii + strdB   !! Linearized index in ModB to calculate weights
         indI = indX(ii)
         indIm1 = indI+1
         
         rows(ind) = irow
         columns(ind) = indI + strdA  ! Linearized in ModA lowest-left corner
         temw = 0.1D0*uum1*ttm1
         weights(ind) = 10.0D0*dabs((1+temw) - 1) ! This to avoid numbers less than epsilon
         ind = ind + 1

         rows(ind) = irow
         columns(ind) = indIm1 + strdA  ! Linearized in ModA lowest-right corner
         temw = 0.1D0*uum1*tt
         weights(ind) = 10.0D0*dabs((1+temw) - 1)
         ind = ind + 1
         
         rows(ind) = irow
         columns(ind) = indI + strdAm1  ! Linearized in ModA upper-left corner
         temw = 0.1D0*uu*ttm1
         weights(ind) = 10.0D0*dabs((1+temw) - 1)
         ind = ind + 1

         rows(ind) = irow
         columns(ind) = indIm1 + strdAm1  ! Linearized in ModA upper-right corner
         temw = 0.1D0*uu*tt
         weights(ind) = 10.0D0*dabs((1+temw) - 1)
         ind = ind + 1
      enddo
   enddo
   
!   print *, 'num_links = ', num_elements
!   print *, 'src_grid_size = ', nColumns
!   print *, 'dst_grid_size = ', nRows
!   print *, 'src_address = ', columns, ';'
!   print *, ''
!   print *, 'dst_address = ', rows, ';'
!   print *, ''
!   print *, 'remap_matrix = '
!   do ii=1, num_elements
!      print *, weights(ii)
!   end do
   
   deallocate( indX, indY, wX, wY, stat=ierr)
   if (ierr /= 0) then
      print *, "( errcod: ", ierr, ") Error deallocating memory. Terminating."
      print 1001,  __FILE__, __LINE__
      return
   endif
  
   ierr = 0

1001 format (1X,'ERROR: ', A, ' (', I6, ')' )
! ---------------------------------  END( test_mct_get_bilin_interp )
END SUBROUTINE test_mct_get_bilin_interp

!****************************************************************************
!>             \brief Generates the bilinear interpolation coefficients
!!             distributed among processes for equally spaced discretized
!!             domains
!!             
!!             This subroutine Generates the bilinear interpolation
!!             coefficients distributed among a number of processes
!!             arranged in a squared shape for equally spaced discretized
!!             domains between values in xends and yends (both domains).
!!
!!             The coefficients are stored in an aij sparse matrix format
!!             to be used in MCT. Mapping goes from A to B.
!!
SUBROUTINE test_mct_get_bilin_interp_dist ( Apts, Bpts, xends, yends, localsize, points, &
                                          & num_elements, nRows, nColumns, &
                                          & rows, columns, weights, ierr )

   integer(long), intent(in), dimension(2) :: Apts, Bpts
   double precision, intent(in), dimension(2) :: xends, yends
   integer(long), intent(in) :: localsize
   integer(long), dimension(:), intent(in) ::  points
   integer(long), intent(out) :: nRows, nColumns, num_elements
   integer, intent(out) :: ierr
   integer(long), dimension(:), pointer :: rows, columns
   double precision, dimension(:), pointer :: weights

! --------------------------------------------  Variables Declaration
   integer(long), allocatable, dimension(:) :: indX, indY     ! Indices of the lowest neighbor
   double precision, allocatable, dimension(:) :: wX, wY ! The langrangian coeff

   integer(long) :: ii, jj, pntm1, ind, index, irow, indI, indIm1
   integer(long) :: strdA, strdAm1
   integer(long) :: mod1x, mod1y, mod2x, mod2y
   double precision :: tt, ttm1, uu, uum1, temw

! --------------------------  BEGIN( test_mct_get_bilin_interp_dist )

   mod1x = Apts(1)
   mod1y = Apts(2)
   mod2x = Bpts(1)
   mod2y = Bpts(2)

   allocate ( indX(mod2x), indY(mod2y), wX(mod2x), wY(mod2y) )
   
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!! Calculating the coefficient in x-direction 
   call Calculate_DirectionWeights( xends, mod1x, mod2x, indX, wX )
!   print *, 'xends = ', xends
!   print *, 'mod1x = ', mod1x
!   print *, 'mod2x = ', mod2x
!   print *, ''
!   print *, '[ ii, indX, wX ] = '
!   do ii=1, mod2x
!      print *, '[ ', ii, ', ', indX(ii), ', ', wX(ii), ' ]'
!   end do


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!! Calculating the coefficient in y-direction 
   call Calculate_DirectionWeights( yends, mod1y, mod2y, indY, wY )
!   print *, 'yends = ', yends
!   print *, 'mod1x = ', mod1y
!   print *, 'mod2x = ', mod2y
!   print *, ''
!   print *, '[ jj, indY, wY ] = '
!   do jj=1, mod2y
!      print *, '[ ', jj, ', ', indY(jj), ', ', wY(jj), ' ]'
!   end do

   nRows = mod2x*mod2y
   nColumns = mod1x*mod1y
   num_elements = 4*localsize
   allocate ( rows(num_elements), columns(num_elements), weights(num_elements) )

!   print *, 'localsize = ', localsize
!   print *, 'num_links = ', num_elements
!   print *, 'src_grid_size = ', nColumns
!   print *, 'dst_grid_size = ', nRows
!   print *, 'points = ', points
   
   ind = 1
   do index = 1, localsize
      !! Transform from linear to cartesian
      irow = points(index)   !! Linearized index in ModB to calculate weights
      pntm1 =  irow - 1
      !! Model B's cartesian coordinates
      ii = mod(pntm1, mod2x) + 1
      jj = pntm1/mod2x + 1
      !print *, '( ', ii, ', ', jj, ') ==> ', irow

      strdAm1 = indY(jj)*mod1x     !! upper row
      strdA = strdAm1 - mod1x      !! lower row
      indI = indX(ii)
      indIm1 = indI+1

      tt = wX(ii)
      ttm1 = 1 - tt
      uu = wY(jj)
      uum1 = 1 - uu
      
      rows(ind) = irow
      columns(ind) = indI + strdA  ! Linearized in ModA lowest-left corner
      temw = 0.1D0*uum1*ttm1
      weights(ind) = 10.0D0*dabs((1+temw) - 1) ! This to avoid numbers less than epsilon
      ind = ind + 1

      rows(ind) = irow
      columns(ind) = indIm1 + strdA  ! Linearized in ModA lowest-right corner
      temw = 0.1D0*uum1*tt
      weights(ind) = 10.0D0*dabs((1+temw) - 1)
      ind = ind + 1
      
      rows(ind) = irow
      columns(ind) = indI + strdAm1  ! Linearized in ModA upper-left corner
      temw = 0.1D0*uu*ttm1
      weights(ind) = 10.0D0*dabs((1+temw) - 1)
      ind = ind + 1

      rows(ind) = irow
      columns(ind) = indIm1 + strdAm1  ! Linearized in ModA upper-right corner
      temw = 0.1D0*uu*tt
      weights(ind) = 10.0D0*dabs((1+temw) - 1)
      ind = ind + 1
   enddo
   
!   print *, 'src_address = ', columns, ';'
!   print *, ''
!   print *, 'dst_address = ', rows, ';'
!   print *, ''
!   print *, 'remap_matrix = '
!   do ii=1, num_elements
!      print *, weights(ii)
!   end do
   
   deallocate( indX, indY, wX, wY, stat=ierr)
   if (ierr /= 0) then
      print *, "( errcod: ", ierr, ") Error deallocating memory. Terminating."
      print 1001,  __FILE__, __LINE__
      return
   endif
  
   ierr = 0

1001 format (1X,'ERROR: ', A, ' (', I6, ')' )
! ----------------------------  END( test_mct_get_bilin_interp_dist )
END SUBROUTINE test_mct_get_bilin_interp_dist

SUBROUTINE Calculate_DirectionWeights_long( xends, mod1x, mod2x, indX, w1 )

   integer(long), intent(in) :: mod1x, mod2x
   double precision, intent(in), dimension(2) :: xends
   integer(long), intent(out), dimension(:) :: indX
   double precision, intent(out), dimension(:) :: w1

! --------------------------------------------  Variables Declaration
   integer(long) :: ii, ii1, lastA, lastB, indi, indi1
   double precision :: dh, dt, invdh, dxi, dxp, invnptsA, invnptsB
   double precision :: inival, endval

! -------------------------  BEGIN( Calculate_DirectionWeights_long )
   lastA = mod1x - 1
   lastB = mod2x - 1
   inival = xends(1)
   endval = xends(2)
   invnptsA = 1.0D0/dble(lastA)
   invnptsB = 1.0D0/dble(lastB)
   
   dh = (endval - inival)*invnptsA
   invdh = 1.0D0/dh   
   
   indi = 0
   dxi = inival  ! x_0
   do ii = 0, lastB
      ii1 = ii + 1
      indi1 = indi + 1
      dxp = (inival*dble( lastB - ii) + endval*dble(ii))*invnptsB !! x'
      dt = dxp - dxi ! (x' - x_i)
      do while ( (dt > dh) .and. ( indi1 < lastA) ) !! x' is beyond x_{i+1}
         indi = indi1
         indi1 = indi + 1
         dxi = (inival*dble( lastA - indi) + endval*dble(indi))*invnptsA !!! next x_i
         dt =  dabs(dxp - dxi) ! (x' - x_i)
      enddo
      w1(ii1) = dt * invdh ! (x' - x_{i})/(x_{i+1} - x_{i})
      indX(ii1) = indi1
   enddo

! ---------------------------  END( Calculate_DirectionWeights_long )
END SUBROUTINE Calculate_DirectionWeights_long

SUBROUTINE Calculate_DirectionWeights_int( xends, mod1x, mod2x, indX, w1 )

   integer, intent(in) :: mod1x, mod2x
   double precision, intent(in), dimension(2) :: xends
   integer, intent(out), dimension(:) :: indX
   double precision, intent(out), dimension(:) :: w1

! --------------------------------------------  Variables Declaration
   integer :: ii, ii1, lastA, lastB, indi, indi1
   double precision :: dh, dt, invdh, dxi, dxp, invnptsA, invnptsB
   double precision :: inival, endval

! --------------------------  BEGIN( Calculate_DirectionWeights_int )
   lastA = mod1x - 1
   lastB = mod2x - 1
   inival = xends(1)
   endval = xends(2)
   invnptsA = 1.0D0/dble(lastA)
   invnptsB = 1.0D0/dble(lastB)
   
   dh = (endval - inival)*invnptsA
   invdh = 1.0D0/dh   
   
   indi = 0
   dxi = inival  ! x_0
   do ii = 0, lastB
      ii1 = ii + 1
      indi1 = indi + 1
      dxp = (inival*dble( lastB - ii) + endval*dble(ii))*invnptsB !! x'
      dt = dxp - dxi ! (x' - x_i)
      do while ( (dt > dh) .and. ( indi1 < lastA) ) !! x' is beyond x_{i+1}
         indi = indi1
         indi1 = indi + 1
         dxi = (inival*dble( lastA - indi) + endval*dble(indi))*invnptsA !!! next x_i
         dt =  dabs(dxp - dxi) ! (x' - x_i)
      enddo
      w1(ii1) = dt * invdh ! (x' - x_{i})/(x_{i+1} - x_{i})
      indX(ii1) = indi1
   enddo

! ----------------------------  END( Calculate_DirectionWeights_int )
END SUBROUTINE Calculate_DirectionWeights_int

!****************************************************************************
!*  subroutine read_remap_matrix reads the remapping info from a SCRIP
!*             netCDF file
!****************************************************************************
!SUBROUTINE read_remap_matrix_double( fname, nRows, nColumns, num_elements, &
!                              rows, columns, weights, ierr )
!
!!   USE NETCDF
!
!   character(*), intent(in) :: fname
!   integer, intent(out) :: nRows, nColumns, num_elements, ierr
!   integer, dimension(:), pointer :: rows, columns
!   double precision, dimension(:), pointer :: weights
!
! ! ----------------------------------      NETCDF  Variables Declaration
!   character (len = *), parameter :: N_ELEMENTS = "num_links"
!   character (len = *), parameter :: SRC_GDIM = "src_grid_size"
!   character (len = *), parameter :: DST_GDIM = "dst_grid_size"
!   character (len = *), parameter :: SRC_INDEX = "src_address"
!   character (len = *), parameter :: DST_INDEX = "dst_address"
!   character (len = *), parameter :: RMAP_WGTH = "remap_matrix"
!   integer :: ncid, num_linksid, src_grid_dimid, dst_grid_dimid
!   integer :: varlinksid, src_indexid, dst_indexid
!   ! The cdfstart and cdfcount arrays will tell the netCDF library where to
!   ! read our data.
!   integer :: cdfstart(2), cdfcount(2)
!  
!   ! The interpolation netCDF dataset produced by SCRIP is opened
!   ierr = nf90_open( trim(fname), nf90_nowrite, ncid)
!   if (ierr /= nf90_noerr) then
!      print *, "NETD_CDF_ERRN( ", ierr, " ). ", trim(nf90_strerror(ierr))
!      print 1001,  __FILE__, __LINE__
!      ierr = 2
!      return
!   endif
!   ! open(mdev, file=trim(RemapMatrixFile), status="old")
!   
!   !! The id of dimensions are inquired
!   ierr = nf90_inq_dimid(ncid, N_ELEMENTS, num_linksid)
!   if (ierr /= nf90_noerr) then
!      print *, "NETD_CDF_ERRN( ", ierr, " ). ", trim(nf90_strerror(ierr))
!      print 1001,  __FILE__, __LINE__
!      ierr = 2
!      return
!   endif
!   ierr = nf90_inq_dimid(ncid, SRC_GDIM, src_grid_dimid)
!   if (ierr /= nf90_noerr) then
!      print *, "NETD_CDF_ERRN( ", ierr, " ). ", trim(nf90_strerror(ierr))
!      print 1001,  __FILE__, __LINE__
!      ierr = 2
!      return
!   endif
!   ierr = nf90_inq_dimid(ncid, DST_GDIM, dst_grid_dimid)
!   if (ierr /= nf90_noerr) then
!      print *, "NETD_CDF_ERRN( ", ierr, " ). ", trim(nf90_strerror(ierr))
!      print 1001,  __FILE__, __LINE__
!      ierr = 2
!      return
!   endif
!   !! The variables id are inquired
!   ierr = nf90_inq_varid( ncid, SRC_INDEX, src_indexid )
!   if (ierr /= nf90_noerr) then
!      print *, "NETD_CDF_ERRN( ", ierr, " ). ", trim(nf90_strerror(ierr))
!      print 1001,  __FILE__, __LINE__
!      ierr = 2
!      return
!   endif
!   ierr = nf90_inq_varid( ncid, DST_INDEX, dst_indexid )
!   if (ierr /= nf90_noerr) then
!      print *, "NETD_CDF_ERRN( ", ierr, " ). ", trim(nf90_strerror(ierr))
!      print 1001,  __FILE__, __LINE__
!      ierr = 2
!      return
!   endif
!   ierr = nf90_inq_varid( ncid, RMAP_WGTH, varlinksid )
!   if (ierr /= nf90_noerr) then
!      print *, "NETD_CDF_ERRN( ", ierr, " ). ", trim(nf90_strerror(ierr))
!      print 1001,  __FILE__, __LINE__
!      ierr = 2
!      return
!   endif
!
!
!   !! The dimensions are inquired
!   ierr = nf90_inquire_dimension(ncid, num_linksid, len = num_elements)
!   if (ierr /= nf90_noerr) then
!      print *, "NETD_CDF_ERRN( ", ierr, " ). ", trim(nf90_strerror(ierr))
!      print 1001,  __FILE__, __LINE__
!      ierr = 2
!      return
!   endif
!   ierr = nf90_inquire_dimension(ncid, src_grid_dimid, len = nColumns )
!   if (ierr /= nf90_noerr) then
!      print *, "NETD_CDF_ERRN( ", ierr, " ). ", trim(nf90_strerror(ierr))
!      print 1001,  __FILE__, __LINE__
!      ierr = 2
!      return
!   endif
!   ierr = nf90_inquire_dimension(ncid, dst_grid_dimid, len = nRows )
!   if (ierr /= nf90_noerr) then
!      print *, "NETD_CDF_ERRN( ", ierr, " ). ", trim(nf90_strerror(ierr))
!      print 1001,  __FILE__, __LINE__
!      ierr = 2
!      return
!   endif
!!   print *, N_ELEMENTS, ' = ', num_elements
!!   print *, SRC_GDIM, ' = ', nColumns
!!   print *, DST_GDIM, ' = ', nRows
!!   read(mdev,*) num_elements
!!   read(mdev,*) src_dims(1), src_dims(2)
!!   read(mdev,*) dst_dims(1), dst_dims(2)
!   
!   !! The variables used to store the interpolation coefficient
!   !! are allocated
!   allocate( rows(num_elements), columns(num_elements), &
!             weights(num_elements), stat=ierr)
!   if (ierr /= 0) then
!      print *, "( errcod: ", ierr, ") Error allocating memory. Terminating."
!      print 1001,  __FILE__, __LINE__
!      ierr = 3
!      return
!   endif
!
!!      do n=1, num_elements
!!         read(mdev,*) rows(n), columns(n), weights(n)
!!      end do
!!
!   !! The variables id are inquired
!   ierr = nf90_get_var( ncid, src_indexid, columns )
!   if (ierr /= nf90_noerr) then
!      print *, "NETD_CDF_ERRN( ", ierr, " ). ", trim(nf90_strerror(ierr))
!      print 1001,  __FILE__, __LINE__
!      ierr = 2
!      return
!   endif
!   ierr = nf90_get_var( ncid, dst_indexid, rows )
!   if (ierr /= nf90_noerr) then
!      print *, "NETD_CDF_ERRN( ", ierr, " ). ", trim(nf90_strerror(ierr))
!      print 1001,  __FILE__, __LINE__
!      ierr = 2
!      return
!   endif
!   cdfstart = (/ 1, 1 /)
!   cdfcount = (/ 1, num_elements /)
!   ierr = nf90_get_var( ncid, varlinksid, weights, start = cdfstart, &
!                                                 & count = cdfcount )
!   if (ierr /= nf90_noerr) then
!      print *, "NETD_CDF_ERRN( ", ierr, " ). ", trim(nf90_strerror(ierr))
!      print 1001,  __FILE__, __LINE__
!      ierr = 2
!      return
!   endif
!
!   ! The interpolation netCDF dataset is closed
!   ierr = nf90_close( ncid )
!   if (ierr /= nf90_noerr) then
!      print *, "NETD_CDF_ERRN( ", ierr, " ). ", trim(nf90_strerror(ierr))
!      print 1001,  __FILE__, __LINE__
!      ierr = 2
!      return
!   endif
!!      close(mdev)
!1001 format (1X,'ERROR: ', A, ' (', I6, ')' )
!END SUBROUTINE read_remap_matrix_double
!

! Subroutines to check the interpolation values

!**************************************************************************
! subroutine print results in Matlab format in real precision indices int
!**************************************************************************
SUBROUTINE WriteFileRealData_Int( nx, ny, x1, x2, uu, x1name, x2name, uname, filename )


   INTEGER, INTENT(IN)                             :: nx, ny
   DOUBLE PRECISION, DIMENSION(1:nx), INTENT(IN)   :: x1
   DOUBLE PRECISION, DIMENSION(1:ny), INTENT(IN)   :: x2
   REAL, DIMENSION(1:nx, 1:ny), INTENT(IN)         :: uu
   ! File name where the matrices are written
   CHARACTER (LEN=*), INTENT(IN)                   :: filename
   ! Name of the variable that stores the x-coordinate
   CHARACTER (LEN=*), INTENT(IN)                   :: x1name
   ! Name of the variable that stores the y-coordinate
   CHARACTER (LEN=*), INTENT(IN)                   :: x2name
   ! Name of the variable that stores the function values
   CHARACTER (LEN=*), INTENT(IN)                   :: uname

   INTEGER   ii, jj, nx1

   open(unit=10,file=filename,status='replace')

   write (10,*)
   write (10,*) x1name, ' = [ ', (x1(ii),ii=1,nx), '];'
   write (10,*)
   write (10,*)
   write (10,*) x2name, ' = [ ', (x2(ii),ii=1,ny), '];'
   write (10,*)
   write (10,*)
   write (10,*) uname, ' = [ '
   if ( nx > 1 ) then
      nx1 = nx - 1
      do ii=1,nx1
         write (10,*) (uu(ii,jj),jj=1,ny),' ; '
      enddo
   endif
   write (10,*) (uu(nx,jj),jj=1,ny),'];'

   close(10)

END SUBROUTINE WriteFileRealData_Int

!**************************************************************************
! subroutine print results in Matlab format in real precision indices long
!**************************************************************************
SUBROUTINE WriteFileRealData_Long( nx, ny, x1, x2, uu, x1name, x2name, uname, filename )


!   INTEGER(long), INTENT(IN)                             :: nx, ny
   INTEGER, INTENT(IN)                             :: nx, ny
   DOUBLE PRECISION, DIMENSION(1:nx), INTENT(IN)   :: x1
   DOUBLE PRECISION, DIMENSION(1:ny), INTENT(IN)   :: x2
   REAL, DIMENSION(1:nx, 1:ny), INTENT(IN)         :: uu
   ! File name where the matrices are written
   CHARACTER (LEN=*), INTENT(IN)                   :: filename
   ! Name of the variable that stores the x-coordinate
   CHARACTER (LEN=*), INTENT(IN)                   :: x1name
   ! Name of the variable that stores the y-coordinate
   CHARACTER (LEN=*), INTENT(IN)                   :: x2name
   ! Name of the variable that stores the function values
   CHARACTER (LEN=*), INTENT(IN)                   :: uname

   INTEGER(long) ii, jj, nx1

   open(unit=10,file=filename,status='replace')

   write (10,*)
   write (10,*) x1name, ' = [ ', (x1(ii),ii=1,nx), '];'
   write (10,*)
   write (10,*)
   write (10,*) x2name, ' = [ ', (x2(ii),ii=1,ny), '];'
   write (10,*)
   write (10,*)
   write (10,*) uname, ' = [ '
   if ( nx > 1 ) then
      nx1 = nx - 1
      do ii=1,nx1
         write (10,*) (uu(ii,jj),jj=1,ny),' ; '
      enddo
   endif
   write (10,*) (uu(nx,jj),jj=1,ny),'];'

   close(10)

END SUBROUTINE WriteFileRealData_Long

!**************************************************************************
! subroutine print results in Matlab format in double precision indices long
!**************************************************************************
SUBROUTINE WriteFileDoubleData_Int( nx, ny, x1, x2, uu, x1name, x2name, uname, filename )


   INTEGER, INTENT(IN)                       :: nx, ny
   DOUBLE PRECISION, DIMENSION(1:nx), INTENT(IN)   :: x1
   DOUBLE PRECISION, DIMENSION(1:ny), INTENT(IN)   :: x2
   DOUBLE PRECISION, DIMENSION(1:nx, 1:ny), INTENT(IN) :: uu
   ! File name where the matrices are written
   CHARACTER (LEN=*), INTENT(IN)                   :: filename
   ! Name of the variable that stores the x-coordinate
   CHARACTER (LEN=*), INTENT(IN)                   :: x1name
   ! Name of the variable that stores the y-coordinate
   CHARACTER (LEN=*), INTENT(IN)                   :: x2name
   ! Name of the variable that stores the function values
   CHARACTER (LEN=*), INTENT(IN)                   :: uname

   INTEGER   ii, jj, nx1

   open(unit=10,file=filename,status='replace')

   write (10,*)
   write (10,*) x1name, ' = [ ', (x1(ii),ii=1,nx), '];'
   write (10,*)
   write (10,*)
   write (10,*) x2name, ' = [ ', (x2(ii),ii=1,ny), '];'
   write (10,*)
   write (10,*)
   write (10,*) uname, ' = [ '
   if ( nx > 1 ) then
      nx1 = nx - 1
      do ii=1,nx1
         write (10,*) (uu(ii,jj),jj=1,ny),' ; '
      enddo
   endif
   write (10,*) (uu(nx,jj),jj=1,ny),'];'

   close(10)

END SUBROUTINE WriteFileDoubleData_Int

!**************************************************************************
! subroutine print results in Matlab format in double precision indices long
!**************************************************************************
SUBROUTINE WriteFileDoubleData_Long( nx, ny, x1, x2, uu, x1name, x2name, uname, filename )


!   INTEGER(long), INTENT(IN)                       :: nx, ny
   INTEGER, INTENT(IN)                       :: nx, ny
   DOUBLE PRECISION, DIMENSION(1:nx), INTENT(IN)   :: x1
   DOUBLE PRECISION, DIMENSION(1:ny), INTENT(IN)   :: x2
   DOUBLE PRECISION, DIMENSION(1:nx, 1:ny), INTENT(IN) :: uu
   ! File name where the matrices are written
   CHARACTER (LEN=*), INTENT(IN)                   :: filename
   ! Name of the variable that stores the x-coordinate
   CHARACTER (LEN=*), INTENT(IN)                   :: x1name
   ! Name of the variable that stores the y-coordinate
   CHARACTER (LEN=*), INTENT(IN)                   :: x2name
   ! Name of the variable that stores the function values
   CHARACTER (LEN=*), INTENT(IN)                   :: uname

   INTEGER   ii, jj, nx1

   open(unit=10,file=filename,status='replace')

   write (10,*)
   write (10,*) x1name, ' = [ ', (x1(ii),ii=1,nx), '];'
   write (10,*)
   write (10,*)
   write (10,*) x2name, ' = [ ', (x2(ii),ii=1,ny), '];'
   write (10,*)
   write (10,*)
   write (10,*) uname, ' = [ '
   if ( nx > 1 ) then
      nx1 = nx - 1
      do ii=1,nx1
         write (10,*) (uu(ii,jj),jj=1,ny),' ; '
      enddo
   endif
   write (10,*) (uu(nx,jj),jj=1,ny),'];'

   close(10)

END SUBROUTINE WriteFileDoubleData_Long

END MODULE INTERP_TOOLS
