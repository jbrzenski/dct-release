/**************************************************************/
/*              test_sd_comm_intp_tool.c                      */
/*                                                            */
/*      This is the source file used for the example test     */
/*      test_subdom_subdom_comm_intp.c where all the          */
/*      functions about the mesh sizes are implemented        */
/**************************************************************/
/*  Created on Jun 20, 2011                */
/*                                                            */
/**************************************************************/

/*
      $Id: test_sd_comm_intp_tool.c,v 1.1 2011/07/12 22:40:17 dcecchis Exp $
*/
/*****************************************************************/
/*
      $Log: test_sd_comm_intp_tool.c,v $
      Revision 1.1  2011/07/12 22:40:17  dcecchis
      Tests files using only C code. This use data from files about temperature
      and wind stress.

*/
/*****************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "test_sd_comm_intp_tool.h"

int read_temperature( int iniyr, int endyr, float **data )
{
/* ---------------------------------------  Variables Declaration  */
   int     len, ii, jj, kk, ll, inid, maxdim;
   char   *filename;
   FILE   *fp;
   float  *initprt;
   
/* ------------------------------------  BEGIN( read_temperature ) */
   
   if ( iniyr < endyr ) {
      fprintf( stderr,
          "\nERROR[parameter]: Error end year parameter smaller than initial year\n" );
      fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
      return( 1 );
   }
   
   len = strlen(TEMPDIR) + strlen(TEMPFILE) + 1;
   filename = (char *) malloc( sizeof(char)*(size_t)len );
   if ( filename == (char *)NULL ) {
      fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
      fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
      return( 1 );
   }
   
   strcpy( filename, TEMPDIR);
   strcat( filename, TEMPFILE);
   
   /** Opening the file **/
   fp = fopen(filename, "r");
   if ( fp ==(FILE *)NULL ) {
      fprintf( stderr, "\nERROR[opening file]: Error opening file %s\n", filename );
      fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
      return( 1 );
   }
   
   maxdim = TNLATI*TNLONG;
   /** Passing the previous data **/
   for ( ii=1850; ii < iniyr; ii++ ) {
      for ( jj=0; jj < 12; jj++ ) {
         fscanf( fp, "%d%*[^\n]%*c", &len  );
         if ( ii != len ) {
            fprintf( stderr, "\nERROR[reading file]: Error reading the file %s\n",
                             filename );
            fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
            return( 1 );
         }
         for ( kk=0; kk < maxdim; kk++ )
            fscanf( fp, "%*f" );
      }
   }
   
   /** Getting the data **/
   initprt = *data;
   inid = 0;
   for ( ii=iniyr; ii <= endyr; ii++ ) {
      for ( jj=0; jj < 12; jj++ ) {
         fscanf( fp, "%d%*[^\n]%*c", &len );
         if ( ii != len ) {
            fprintf( stderr, "\nERROR[reading file]: Error reading the file %s\n",
                             filename );
            fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
            return( 1 );
         }
         for ( ll=0; ll < TNLATI; ll++ ) {
            for ( kk=0; kk < TNLONG; kk++ )
               fscanf( fp, "%f", ( initprt + kk*TNLATI + TNLATI - ll -1 ) );
         }
         initprt = *(data + ++inid);
      }
      
   }
   
   /**  Closing the file **/
   fclose( fp );
   
   return (0);
/* --------------------------------------  END( read_temperature ) */
}

#define LWINDFN    15     /* Lenth of the wind stress file name */
int read_wind_stress( float **datax,  float **datay )
{
/* ---------------------------------------  Variables Declaration  */
   int     len, ii, kk, ll;
   int     nx, ny, lwsdir;
   char   *filename;
   FILE   *fp;
   char    filenamesx[12][LWINDFN] = { "tx-jan.dat",
                                       "tx-feb.dat",
                                       "tx-mar.dat",
                                       "tx-apr.dat",
                                       "tx-may.dat",
                                       "tx-jun.dat",
                                       "tx-jul.dat",
                                       "tx-aug.dat",
                                       "tx-sep.dat",
                                       "tx-oct.dat",
                                       "tx-nov.dat",
                                       "tx-dec.dat" };
   char    filenamesy[12][LWINDFN] = { "ty-jan.dat",
                                       "ty-feb.dat",
                                       "ty-mar.dat",
                                       "ty-apr.dat",
                                       "ty-may.dat",
                                       "ty-jun.dat",
                                       "ty-jul.dat",
                                       "ty-aug.dat",
                                       "ty-sep.dat",
                                       "ty-oct.dat",
                                       "ty-nov.dat",
                                       "ty-dec.dat" };
                                 
   float  *initprt;
   
/* ------------------------------------  BEGIN( read_wind_stress ) */
   
   lwsdir = strlen(WSDIR);
   len = lwsdir + LWINDFN + 1;
   filename = (char *) malloc( sizeof(char)*(size_t)len );
   if ( filename == (char *)NULL ) {
      fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
      fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
      return( 1 );
   }
   
   strcpy( filename, WSDIR);
   /** Getting the x-wind stress data **/
   initprt = *datax;
   for ( ii = 0; ii < 12; ii++ ) {
      strcpy( (filename+lwsdir), filenamesx[ii] );
   
      /** Opening the file **/
      fp = fopen(filename, "r");
      if ( fp ==(FILE *)NULL ) {
         fprintf( stderr, "\nERROR[opening file]: Error opening file %s\n", filename );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
      }

      fscanf( fp, "%d %d %*f %*f %*f %*f %*[^\n]%*c", &nx, &ny );
      if ( (nx != WSNLONG ) || (ny != WSNLATI) ) {
         fprintf( stderr, "\nERROR[reading file]: Error reading dimension from file %s\n",
                          filename );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
      }
      for ( ll=0; ll < WSNLATI; ll++ ) {
         for ( kk=0; kk < WSNLONG; kk++ )
            fscanf( fp, "%f", ( initprt + kk*WSNLATI + ll ) );
      }
      initprt = *(datax + ii);
   
      /**  Closing the file **/
      fclose( fp );
      
   }

   /** Getting the y-wind stress data **/
   initprt = *datay;
   for ( ii = 0; ii < 12; ii++ ) {
      strcpy( (filename+lwsdir), filenamesy[ii] );
   
      /** Opening the file **/
      fp = fopen(filename, "r");
      if ( fp ==(FILE *)NULL ) {
         fprintf( stderr, "\nERROR[opening file]: Error opening file %s\n", filename );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
      }

      fscanf( fp, "%d %d %*f %*f %*f %*f %*[^\n]%*c", &nx, &ny );
      if ( (nx != WSNLONG ) || (ny != WSNLATI) ) {
         fprintf( stderr, "\nERROR[reading file]: Error reading dimension from file %s\n",
                          filename );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
      }
      for ( ll=0; ll < WSNLATI; ll++ ) {
         for ( kk=0; kk < WSNLONG; kk++ )
            fscanf( fp, "%f", ( initprt + kk*WSNLATI + ll ) );
      }
      initprt = *(datay + ii);
   
      /**  Closing the file **/
      fclose( fp );
      
   }
   
   return (0);
/* --------------------------------------  END( read_wind_stress ) */
}


int feed_temperature( float *temp, float **data, int ntime )
{
/* ---------------------------------------  Variables Declaration  */
   int     ii, ntotal;

   float  *initprt;
   
/* ------------------------------------  BEGIN( feed_temperature ) */
   /* Gets the initial address */
   initprt = *(data + ntime);
   ntotal = TNLATI*TNLONG;
   
   /* Copy the corresponding time */
   for ( ii =0; ii < ntotal; ii++ ) *(temp + ii) = *(initprt + ii);
   
   return (0);
/* --------------------------------------  END( feed_temperature ) */
}

int feed_xwinds( float *windx, float **datax, int ntime )
{
/* ---------------------------------------  Variables Declaration  */
   int     ii, jj, ind;

   float  *initprt;
   float  *strided;
   
/* -----------------------------------------  BEGIN( feed_xwinds ) */
   /* Gets the initial address */
   initprt = *(datax + ntime);
   
   /* Copy the corresponding time */
   ind = 0;
   for ( ii =2; ii < WSNLONG; ii+=5 ) {
      strided = initprt + ii*WSNLATI;
      for ( jj =0; jj < WSNLATI; jj+=5 ) {
         *(windx + ind) = *(strided + jj );
         ind++;
      }
   }
   
   return (0);
/* -------------------------------------------  END( feed_xwinds ) */
}

int feed_ywinds( float *windy, float **datay, int ntime )
{
/* ---------------------------------------  Variables Declaration  */
   int     ii, jj, ind;

   float  *initprt;
   float  *strided;
   
/* -----------------------------------------  BEGIN( feed_ywinds ) */
   /* Gets the initial address */
   initprt = *(datay + ntime);
   
   /* Copy the corresponding time */
   ind = 0;
   for ( ii =0; ii < WSNLONG; ii+=5 ) {
      strided = initprt + ii*WSNLATI;
      for ( jj =2; jj < WSNLATI; jj+=5 ) {
         *(windy + ind) = *(strided + jj );
         ind++;
      }
   }
   
   return (0);
/* -------------------------------------------  END( feed_ywinds ) */
}


